XEP - XSL Formatting Engine for Paged Media
Trial Edition

Version 4.1 build 20050109
January 9th, 2005


CONTENTS

1. About the program
2. Installation
3. Deinstallation
4. Limitations of the evaluation version
5. Support
6. Contact info
7. Copyright notices


1. About the program

XEP  converts  XSL  Formatting  Objects  documents  into Adobe PDF or
PostScript format. The program is written in Java and requires a Java 
Virtual Machine.  Sun  JRE or JDK version 1.3 or later is recommended 
for best performance.

XEP  also runs under JDK/JRE 1.1.8, or Microsoft Java VM version 5.00 
or higher.  The performance  will not be as good as  with  the latest 
versions of Java VM. The software will not run on older Java machines.


2. Installation

 A. Ensure you have a Java VM  installed.  If you have multiple  VMs 
    on your  computer, pick  one of them.  The  installation  wizard 
    configures XEP to run  on  the  same  Java VM  that was used for 
    setup; you can change this later. 
    
 B. Run the setup from the jar file, using a Java VM of your choice.
    If your Java VM supports Java 2 (JDK/JRE 1.2 or higher), you can 
    run the jar by typing the following command on the prompt:
    
      java -jar setup-4.1-20050109-trial.jar
    
    Alternatively,  for Java machines  that don't support executable
    jar files, you can call Setup class from the jar, using standard
    invocation methods.  E.g. for Microsoft Java, the syntax will be
    as follows:
    
      jview /cp setup-4.1-20050109-trial.jar Setup
      
    The system will first display the  license  agreement  for  this
    edition of XEP; please read it carefully.  You should accept  it 
    in order to proceed.  Once you press the "Yes, I accept" button,
    you agree to be bound by the terms of this license agreement.
    
    After that, you will be prompted  for the target directory name. 
    Type the path in the edit box, or choose a directory by pressing
    "Browse" button.  On  systems with no Swing support,  the browse
    button will not be displayed: you have to type the path manually.
    
    The system copies all files to the specified  location  and sets
    up necessary configuration files. 

 C. Activate  your copy of  the software.  As a last  step XEP setup
    will  offer  you  to activate  XEP by  placing  appropriate  XML
    license  file in the root  directory of  XEP installation.  This
    file  could  be  included  in XEP distribution  or  send  to you
    separately  via  e-mail to the address  provided  during initial
    registration.

After activation, your XEP copy is ready to use. Please refer to the
PDF documentation, contained in the doc/ subdirectory of the  target
installation directory:

- intro.pdf           	  A Gentle Introduction to XEP, by Dave Pawson
- reference.pdf           XEP Reference Manual
- tutorial.pdf            a brief introduction into XSL FO


3. Deinstallation

The program places all its files into the target directory, and does
not modify any system settings.  To deinstall it, simply  remove the 
installation directory with all its contents.
    

4. Limitations of the Trial Edition

This edition of XEP is intended as a means for  interested  users to
access the formatting quality and the performance offered by XEP. It
has the following limitations with respect to the full version:

- Every page contains a label "XSL-FO/RenderX"  in  the  lower  left 
  corner, hyperlinked to the XSL home page at W3C and to the RenderX
  website at http://www.renderx.com. 

- Only PDF output is supported (no PostScript).


5. Support

RenderX provides no dedicated support for this  product  and  cannot
guarantee a fixed response time for your questions.  You are invited
to share your comments, ask questions and suggest improvements  in a 
public mailing list: xep-support@renderx.com.  To subscribe,  send a 
message to majordomo@renderx.com with words  "subscribe xep-support" 
in the body of the message.

When describing a problem, please take care to include the following 
data to help us better understand your case:

   - hardware and software platform used to run XEP  (processor type 
     and OS version);
   - Java VM vendor and version;
   - as much detail about XEP configuration as you can provide.


6. Contact info

Please write to info@renderx.com on  general  business  topics.  All 
correspondence  concerning  commercial  issues  should  be  sent  to
sales@renderx.com.


7. Copyright notices

Copyright (c) 1999-2004 RenderX Corporation.  All rights reserved.
Patents pending.

This product includes program code by Sun Microsystems, Inc. 
Copyright (c) 1999-2001 Sun Microsystems, Inc. All rights reserved.
URL: http://www.sun.com/

This product includes the SAXON XSLT Processor from Michael Kay. 
Copyright (c) 1998-2004 Michael Kay. All rights reserved.
URL: http://saxon.sourceforge.net/ 

This product includes the XT XSLT Processor from James Clark. 
Copyright (c) 1998, 1999 James Clark. All rights reserved.
URL: http://www.jclark.com/xml/xt.html

Documentation for this product is created in DocBook and formatted 
with DocBook XSL stylesheets from the DocBook Open Repository.
URL: http://docbook.sourceforge.net/

This product includes software developed by the Apache 
Software Foundation (http://www.apache.org/).
Copyright (c) 2000 The Apache Software Foundation. All rights reserved.
URL: http://www.apache.org/
