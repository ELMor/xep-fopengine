/*
 * HandlerAdapter - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.sax;

import java.util.Enumeration;

import com.renderx.util.Hashtable;
import com.renderx.util.Stack;

import org.xml.sax.AttributeList;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.DocumentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributeListImpl;
import org.xml.sax.helpers.AttributesImpl;

public class HandlerAdapter implements ContentHandler, DocumentHandler {
	private DocumentHandler dh = null;

	private ContentHandler ch = null;

	Stack scopes;

	Hashtable scope;

	private String getURI(String string) throws SAXException {
		Hashtable hashtable = scope;
		Enumeration enumeration = scopes.elements();
		for (;;) {
			if (hashtable.containsKey(string))
				return (String) hashtable.get(string);
			if (!enumeration.hasMoreElements())
				throw new SAXException("namespace prefix '" + string
						+ "' is not defined");
			hashtable = (Hashtable) enumeration.nextElement();
		}
	}

	private void startScope() {
		scopes.push(scope);
		scope = new Hashtable();
	}

	private void endScope() {
		scope = (Hashtable) scopes.pop();
	}

	public HandlerAdapter(DocumentHandler documenthandler) {
		scopes = new Stack();
		scope = new Hashtable();
		scope.put("", "");
		scope.put("xml", "http://www.w3.org/XML/1998/namespace");
		scope.put("xmlns", "");
		dh = documenthandler;
	}

	public HandlerAdapter(ContentHandler contenthandler) {
		scopes = new Stack();
		scope = new Hashtable();
		scope.put("", "");
		scope.put("xml", "http://www.w3.org/XML/1998/namespace");
		scope.put("xmlns", "");
		ch = contenthandler;
	}

	protected void setNamespace(String string, String string_0_)
			throws SAXException {
		scope.put(string, string_0_);
		ch.startPrefixMapping(string, string_0_);
	}

	public void setDocumentLocator(Locator locator) {
		if (dh != null)
			dh.setDocumentLocator(locator);
		else
			ch.setDocumentLocator(locator);
	}

	public void startDocument() throws SAXException {
		if (dh != null)
			dh.startDocument();
		else
			ch.startDocument();
	}

	public void endDocument() throws SAXException {
		if (dh != null)
			dh.endDocument();
		else
			ch.endDocument();
	}

	public void characters(char[] cs, int i, int i_1_) throws SAXException {
		if (dh != null)
			dh.characters(cs, i, i_1_);
		else
			ch.characters(cs, i, i_1_);
	}

	public void ignorableWhitespace(char[] cs, int i, int i_2_)
			throws SAXException {
		if (dh != null)
			dh.ignorableWhitespace(cs, i, i_2_);
		else
			ch.ignorableWhitespace(cs, i, i_2_);
	}

	public void processingInstruction(String string, String string_3_)
			throws SAXException {
		if (dh != null)
			dh.processingInstruction(string, string_3_);
		else
			ch.processingInstruction(string, string_3_);
	}

	public void startElement(String string, AttributeList attributelist)
			throws SAXException {
		if (ch != null) {
			int i = attributelist.getLength();
			startScope();
			for (int i_4_ = 0; i_4_ != i; i_4_++) {
				String string_5_ = attributelist.getName(i_4_);
				int i_6_ = string_5_.indexOf(':');
				if (i_6_ != -1) {
					if ("xmlns".equals(string_5_.substring(0, i_6_)))
						setNamespace(string_5_.substring(i_6_ + 1),
								attributelist.getValue(i_4_));
				} else if ("xmlns".equals(string_5_))
					setNamespace("", attributelist.getValue(i_4_));
			}
			Object object = null;
			Object object_7_ = null;
			Object object_8_ = null;
			Object object_9_ = null;
			int i_10_ = string.indexOf(':');
			String string_11_;
			String string_12_;
			String string_13_;
			if (i_10_ != -1) {
				String string_14_ = string.substring(0, i_10_);
				string_13_ = getURI(string_14_);
				string_11_ = string;
				string_12_ = string.substring(i_10_ + 1);
			} else {
				string_11_ = string_12_ = string;
				string_13_ = getURI("");
			}
			AttributesImpl attributesimpl = new AttributesImpl();
			for (int i_15_ = 0; i_15_ != i; i_15_++) {
				Object object_16_ = null;
				Object object_17_ = null;
				Object object_18_ = null;
				Object object_19_ = null;
				String string_20_ = attributelist.getName(i_15_);
				int i_21_ = string_20_.indexOf(':');
				String string_22_;
				String string_23_;
				String string_24_;
				if (i_21_ != -1) {
					String string_25_ = string_20_.substring(0, i_21_);
					string_24_ = getURI(string_25_);
					string_23_ = string_20_;
					string_22_ = string_20_.substring(i_21_ + 1);
				} else {
					string_22_ = string_23_ = string_20_;
					string_24_ = "";
				}
				attributesimpl.addAttribute(string_24_, string_22_, string_23_,
						attributelist.getType(i_15_), attributelist
								.getValue(i_15_));
			}
			ch.startElement(string_13_, string_12_, string_11_, attributesimpl);
		} else
			dh.startElement(string, attributelist);
	}

	public void endElement(String string) throws SAXException {
		if (ch != null) {
			Object object = null;
			Object object_26_ = null;
			Object object_27_ = null;
			Object object_28_ = null;
			int i = string.indexOf(':');
			String string_29_;
			String string_30_;
			String string_31_;
			if (i != -1) {
				String string_32_ = string.substring(0, i);
				string_31_ = getURI(string_32_);
				string_29_ = string;
				string_30_ = string.substring(i + 1);
			} else {
				string_29_ = string_30_ = string;
				string_31_ = getURI("");
			}
			ch.endElement(string_31_, string_30_, string_29_);
			Enumeration enumeration = scope.keys();
			while (enumeration.hasMoreElements())
				ch.endPrefixMapping((String) enumeration.nextElement());
			endScope();
		} else
			dh.endElement(string);
	}

	public void startPrefixMapping(String string, String string_33_)
			throws SAXException {
		if (ch != null)
			ch.startPrefixMapping(string, string_33_);
	}

	public void endPrefixMapping(String string) throws SAXException {
		if (ch != null)
			ch.endPrefixMapping(string);
	}

	public void startElement(String string, String string_34_,
			String string_35_, Attributes attributes) throws SAXException {
		if (dh != null) {
			int i = attributes.getLength();
			AttributeListImpl attributelistimpl = new AttributeListImpl();
			for (int i_36_ = 0; i_36_ != i; i_36_++)
				attributelistimpl.addAttribute(attributes.getQName(i_36_),
						attributes.getType(i_36_), attributes.getValue(i_36_));
			dh.startElement(string_35_, attributelistimpl);
		} else
			ch.startElement(string, string_34_, string_35_, attributes);
	}

	public void endElement(String string, String string_37_, String string_38_)
			throws SAXException {
		if (dh != null)
			dh.endElement(string_38_);
		else
			ch.endElement(string, string_37_, string_38_);
	}

	public void skippedEntity(String string) throws SAXException {
		if (ch != null)
			ch.skippedEntity(string);
	}
}