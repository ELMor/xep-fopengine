/*
 * JAXPTransformerAdapter - Decompiled by JODE Visit
 * http://jode.sourceforge.net/
 */
package com.renderx.sax;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;

import org.xml.sax.ContentHandler;
import org.xml.sax.DTDHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.XMLReader;

public class JAXPTransformerAdapter implements XMLReader {
	protected Transformer transformer = null;

	protected ContentHandler handler = null;

	protected ErrorHandler errhandler = null;

	protected DTDHandler dtdhandler = null;

	protected EntityResolver resolver = null;

	public JAXPTransformerAdapter(Transformer transformer) {
		this.transformer = transformer;
	}

	public void setContentHandler(ContentHandler contenthandler) {
		handler = contenthandler;
	}

	public ContentHandler getContentHandler() {
		return handler;
	}

	public void setErrorHandler(ErrorHandler errorhandler) {
		errhandler = errorhandler;
	}

	public ErrorHandler getErrorHandler() {
		return errhandler;
	}

	public void setDTDHandler(DTDHandler dtdhandler) {
		this.dtdhandler = dtdhandler;
	}

	public DTDHandler getDTDHandler() {
		return dtdhandler;
	}

	public void setEntityResolver(EntityResolver entityresolver) {
		resolver = entityresolver;
	}

	public EntityResolver getEntityResolver() {
		return resolver;
	}

	public void setProperty(String string, Object object)
			throws SAXNotSupportedException, SAXNotRecognizedException {
		throw new SAXNotRecognizedException(this.getClass().getName()
				+ " does not recognize property " + string);
	}

	public Object getProperty(String string) throws SAXNotSupportedException,
			SAXNotRecognizedException {
		throw new SAXNotRecognizedException(this.getClass().getName()
				+ " does not recognize property " + string);
	}

	public void setFeature(String string, boolean bool)
			throws SAXNotSupportedException, SAXNotRecognizedException {
		if (string.equals("http://xml.org/sax/features/namespaces")) {
			if (bool)
				return;
			throw new SAXNotSupportedException(this.getClass().getName()
					+ " does not support setting feature " + string + " to "
					+ bool);
		}
		if (string.equals("http://xml.org/sax/features/namespace-prefixes")) {
			if (!bool)
				return;
			throw new SAXNotSupportedException(this.getClass().getName()
					+ " does not support setting feature " + string + " to "
					+ bool);
		}
		throw new SAXNotRecognizedException(this.getClass().getName()
				+ " does not recognize feature " + string);
	}

	public boolean getFeature(String string) throws SAXNotRecognizedException {
		if (string.equals("http://xml.org/sax/features/namespaces"))
			return true;
		if (string.equals("http://xml.org/sax/features/namespace-prefixes"))
			return false;
		throw new SAXNotRecognizedException(this.getClass().getName()
				+ " does not recognize feature " + string);
	}

	public void parse(String string) throws SAXException {
		parse(new org.xml.sax.InputSource(string));
	}

	public void parse(org.xml.sax.InputSource inputsource) throws SAXException {
		try {
			XMLReader xmlreader = XMLReaderFactory.createXMLReader();
			transformer.transform(new SAXSource(xmlreader, inputsource),
					new SAXResult(handler));
		} catch (TransformerException transformerexception) {
			throw new SAXException(transformerexception);
		}
	}
}