/*
 * ConfigHandlerAdapter - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.sax;

import java.io.IOException;
import java.net.MalformedURLException;

import com.renderx.util.Hashtable;
import com.renderx.util.Stack;
import com.renderx.util.URLSpec;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class ConfigHandlerAdapter extends DefaultHandler {
	private final ConfigHandler handler;

	private final Stack contextStack = new Stack();

	private final URLSpec doc;

	private boolean insideHref = false;

	private String expectedElementName = null;

	private String expectedNamespaceURI = null;

	private int nestedFragments = 0;

	private Hashtable peekContext() throws SAXException {
		if (contextStack.isEmpty())
			throw new SAXException(
					"Context stack underflow; probably bad order of calls to SAX ContextHandler");
		return (Hashtable) contextStack.peek();
	}

	public ConfigHandlerAdapter(ConfigHandler confighandler, URLSpec urlspec) {
		handler = confighandler;
		doc = urlspec;
	}

	public void startDocument() throws SAXException {
		if (nestedFragments++ == 0) {
			Hashtable hashtable = new Hashtable();
			hashtable.put("base", doc);
			hashtable.put("document", doc.toString());
			contextStack.push(hashtable);
			handler.startDocument(hashtable);
		}
	}

	public void endDocument() throws SAXException {
		if (--nestedFragments == 0) {
			handler.endDocument(peekContext());
			contextStack.pop();
			if (!contextStack.isEmpty())
				throw new SAXException(
						"Context stack is not empty after the end of the document is reached; probably bad order of calls to SAX ContextHandler");
		}
	}

	public void startElement(String string, String string_0_, String string_1_,
			Attributes attributes) throws SAXException {
		if (insideHref)
			throw new SAXException(
					"Elements with 'href' attribute must be empty");
		Hashtable hashtable = peekContext();
		if (expectedElementName != null) {
			if (!expectedElementName.equals(string_0_))
				throw new SAXException("In remote fragment "
						+ hashtable.get("document") + ", root element '"
						+ string_0_
						+ "' does not match the referring element '"
						+ expectedElementName + "'");
			if (!expectedNamespaceURI.equals(string))
				throw new SAXException("In remote fragment "
						+ hashtable.get("document") + ", root namespace '"
						+ string
						+ "' does not match the referring element namespace '"
						+ expectedNamespaceURI + "'");
			expectedElementName = null;
			expectedNamespaceURI = null;
		}
		Hashtable hashtable_2_ = (Hashtable) hashtable.clone();
		contextStack.push(hashtable_2_);
		try {
			String string_3_ = attributes.getValue("href");
			if (string_3_ == null) {
				String string_4_ = (attributes.getValue(
						"http://www.w3.org/XML/1998/namespace", "base"));
				if (string_4_ != null) {
					URLSpec urlspec = new URLSpec((URLSpec) hashtable_2_
							.get("base"), string_4_);
					hashtable_2_.put("base", urlspec);
				}
				handler.startElement(string, string_0_, string_1_, attributes,
						hashtable_2_);
			} else {
				expectedElementName = string_0_;
				expectedNamespaceURI = string;
				URLSpec urlspec = new URLSpec((URLSpec) hashtable_2_
						.get("base"), string_3_);
				hashtable_2_.put("base", urlspec);
				hashtable_2_.put("document", urlspec.toString());
				XMLReader xmlreader = XMLReaderFactory.createXMLReader();
				xmlreader.setContentHandler(this);
				xmlreader.parse(new InputSource(urlspec));
				insideHref = true;
			}
		} catch (MalformedURLException malformedurlexception) {
			throw new SAXException(malformedurlexception);
		} catch (IOException ioexception) {
			throw new SAXException(ioexception);
		}
	}

	public void endElement(String string, String string_5_, String string_6_)
			throws SAXException {
		if (insideHref)
			insideHref = false;
		else
			handler.endElement(string, string_5_, string_6_, peekContext());
		contextStack.pop();
	}

	public void characters(char[] cs, int i, int i_7_) throws SAXException {
		if (insideHref)
			throw new SAXException(
					"Elements with 'href' attribute must be empty");
		handler.characters(cs, i, i_7_, peekContext());
	}

	public void processingInstruction(String string, String string_8_)
			throws SAXException {
		if (insideHref)
			throw new SAXException(
					"Elements with 'href' attribute must be empty");
		handler.processingInstruction(string, string_8_, peekContext());
	}
}