/*
 * NullSource - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.sax;

import java.io.StringReader;

public class NullSource extends org.xml.sax.InputSource {
	public NullSource(String string) {
		super(new StringReader("<void/>"));
		this.setSystemId(string);
	}

	public NullSource() {
		this("file:null");
	}
}