/*
 * CheckSignedXML - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.crypto;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.math.BigInteger;

import com.renderx.sax.Canonicalizer;
import com.renderx.sax.XMLReaderFactory;
import com.renderx.util.Base64InputStream;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public final class CheckSignedXML {
	private PGPSignature signature = null;

	private byte[] pgpHash = null;

	public void load(InputSource inputsource, XMLReader xmlreader)
			throws IOException, SAXException {
		signature = null;
		pgpHash = null;
		Canonicalizer canonicalizer = new Canonicalizer();
		xmlreader.setContentHandler(canonicalizer);
		xmlreader.parse(inputsource);
		MD5 md5 = new MD5();
		md5.update(canonicalizer.body);
		if (canonicalizer.signature != null) {
			String string = canonicalizer.signature.trim();
			if (string.length() < 5)
				throw new IOException("Invalid or corrupt signature");
			if (string.charAt(string.length() - 5) != '=')
				throw new IOException(
						"Signature incorrectly structured - no checksum");
			byte[] is = base64decode(string.substring(string.length() - 4));
			int i = (is[0] & 0xff) << 16 | (is[1] & 0xff) << 8 | is[2] & 0xff;
			byte[] is_0_ = base64decode(string
					.substring(0, string.length() - 5));
			if (i != PGPCRC.checksum(is_0_))
				throw new IOException("Incorrect signature checksum");
			signature = new PGPSignature(new DataInputStream(
					new ByteArrayInputStream(is_0_)));
			signature.addExtasToHash(md5);
			pgpHash = md5.digest();
		}
	}

	public byte[] base64decode(String string) throws IOException {
		Base64InputStream base64inputstream = new Base64InputStream(string);
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		try {
			byte[] is = new byte[1024];
			boolean bool = false;
			int i;
			while ((i = base64inputstream.read(is)) != -1)
				bytearrayoutputstream.write(is, 0, i);
			return bytearrayoutputstream.toByteArray();
		} finally {
			base64inputstream.close();
			bytearrayoutputstream.close();
		}
	}

	public boolean checkSignature(RSAPublicKey rsapublickey) {
		if (signature == null)
			return false;
		return signature.check(rsapublickey, pgpHash);
	}

	public static boolean check(InputSource inputsource, XMLReader xmlreader,
			String string) throws IOException, SAXException {
		int i = string.indexOf(':');
		RSAPublicKey rsapublickey = new RSAPublicKey(new BigInteger(string
				.substring(0, i), 16), new BigInteger(string.substring(i + 1),
				16));
		CheckSignedXML checksignedxml = new CheckSignedXML();
		checksignedxml.load(inputsource, xmlreader);
		return checksignedxml.checkSignature(rsapublickey);
	}

	public static void main(String[] strings) throws Exception {
		if (strings.length != 2) {
			System.err
					.println("Usage: java com.renderx.crypto.CheckSignedXML <XML file> <public key>\n");
			System.exit(1);
		}
		boolean bool = check(new InputSource(strings[0]), XMLReaderFactory
				.createXMLReader(), strings[1]);
		System.out.println(bool ? "PASSED" : "FAILED");
	}
}