/*
 * RSAPublicKey - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.crypto;

import java.math.BigInteger;

public class RSAPublicKey {
	protected BigInteger n_;

	protected BigInteger e_;

	private static final BigInteger newBigInteger(BigInteger biginteger) {
		return biginteger.pow(1);
	}

	public RSAPublicKey(BigInteger biginteger, BigInteger biginteger_0_) {
		n_ = newBigInteger(biginteger);
		e_ = newBigInteger(biginteger_0_);
	}

	public RSAPublicKey(RSAPublicKey rsapublickey_1_) {
		copy(rsapublickey_1_);
	}

	public void copy(Object object) {
		RSAPublicKey rsapublickey_2_ = (RSAPublicKey) object;
		n_ = newBigInteger(rsapublickey_2_.n_);
		e_ = newBigInteger(rsapublickey_2_.e_);
	}

	public boolean equals(Object object) {
		if (object instanceof RSAPublicKey) {
			RSAPublicKey rsapublickey_3_ = (RSAPublicKey) object;
			if (n_.equals(rsapublickey_3_.n_) && e_.equals(rsapublickey_3_.e_))
				return true;
		}
		return false;
	}

	public final byte[] id() {
		byte[] is = n_.toByteArray();
		byte[] is_4_ = new byte[8];
		System.arraycopy(is, is.length - 8, is_4_, 0, 8);
		return is_4_;
	}

	public final int bitLength() {
		return n_.bitLength();
	}

	public BigInteger encrypt(BigInteger biginteger) {
		return newBigInteger(biginteger).modPow(e_, n_);
	}

	public String toString() {
		return "n:" + n_ + "\ne:" + e_;
	}
}