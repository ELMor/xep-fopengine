/*
 * Hex - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.crypto;

public class Hex {
	private static final char[] hexDigits = { '0', '1', '2', '3', '4', '5',
			'6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	public static String toString(byte[] is) {
		char[] cs = new char[is.length * 2];
		int i = 0;
		for (int i_0_ = 0; i_0_ < is.length; i_0_++) {
			int i_1_ = is[i_0_];
			cs[i++] = hexDigits[i_1_ >>> 4 & 0xf];
			cs[i++] = hexDigits[i_1_ & 0xf];
		}
		return new String(cs);
	}

	public static byte[] stringToByteArray(String string) {
		int i = string.length();
		byte[] is = new byte[(i + 1) / 2];
		int i_2_ = 0;
		int i_3_ = 0;
		if (i % 2 == 1)
			is[i_3_++] = (byte) fromDigit(string.charAt(i_2_++));
		while (i_2_ < i)
			is[i_3_++] = (byte) (fromDigit(string.charAt(i_2_++)) << 4 | fromDigit(string
					.charAt(i_2_++)));
		return is;
	}

	public static int fromDigit(char c) {
		if (c >= '0' && c <= '9')
			return c - '0';
		if (c >= 'A' && c <= 'F')
			return c - 'A' + '\n';
		if (c >= 'a' && c <= 'f')
			return c - 'a' + '\n';
		throw new IllegalArgumentException("invalid hex digit '" + c + "'");
	}

	public static String shortToString(int i) {
		char[] cs = { hexDigits[i >>> 12 & 0xf], hexDigits[i >>> 8 & 0xf],
				hexDigits[i >>> 4 & 0xf], hexDigits[i & 0xf] };
		return new String(cs);
	}
}