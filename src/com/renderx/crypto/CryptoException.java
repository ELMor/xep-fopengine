/*
 * CryptoException - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.crypto;

public class CryptoException extends RuntimeException {
	public CryptoException() {
		/* empty */
	}

	public CryptoException(Exception exception) {
		super(exception.toString());
	}

	public CryptoException(String string) {
		super(string);
	}
}