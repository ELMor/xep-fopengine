/*
 * ISOLatin1 - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.crypto;

public class ISOLatin1 {
	public static byte[] toByteArray(String string, int i, int i_0_) {
		byte[] is = new byte[i_0_];
		for (int i_1_ = 0; i_1_ < i_0_; i_1_++)
			is[i_1_] = (byte) string.charAt(i + i_1_);
		return is;
	}

	public static byte[] toByteArrayLossless(String string, int i, int i_2_) {
		byte[] is = new byte[i_2_];
		for (int i_3_ = 0; i_3_ < i_2_; i_3_++) {
			char c = string.charAt(i + i_3_);
			if (c > '\u00ff')
				throw new IllegalArgumentException(
						"non-ISO-Latin-1 character in input: \\u"
								+ Hex.shortToString(c));
			is[i_3_] = (byte) c;
		}
		return is;
	}
}