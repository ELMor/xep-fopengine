/*
 * RC4 - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.crypto;

import junit.framework.Assert;
import junit.framework.TestCase;

public final class RC4 {
	private final int[] sBox = new int[256];

	private int x;

	private int y;

	public static final class Test extends TestCase {
		public void testRC4() {
			RC4 rc4 = new RC4("testkey".getBytes());
			byte[] is = "test message".getBytes();
			byte[] is_0_ = rc4.crypt(rc4.crypt(is));
			for (int i = 0; i < is.length; i++)
				Assert.assertEquals(is_0_[i], is[i]);
		}
	}

	public RC4(byte[] is) {
		makeKey(is);
	}

	public void crypt(byte[] is, int i, int i_1_, byte[] is_2_, int i_3_) {
		rc4(is, i, i_1_, is_2_, i_3_);
	}

	public byte[] crypt(byte[] is) {
		byte[] is_4_ = new byte[is.length];
		rc4(is, 0, is.length, is_4_, 0);
		return is_4_;
	}

	private void rc4(byte[] is, int i, int i_5_, byte[] is_6_, int i_7_) {
		for (int i_8_ = 0; i_8_ < i_5_; i_8_++) {
			x = x + 1 & 0xff;
			y = sBox[x] + y & 0xff;
			int i_9_ = sBox[x];
			sBox[x] = sBox[y];
			sBox[y] = i_9_;
			int i_10_ = sBox[x] + sBox[y] & 0xff;
			is_6_[i_7_++] = (byte) (is[i++] ^ sBox[i_10_]);
		}
	}

	private void makeKey(byte[] is) throws CryptoException {
		byte[] is_11_ = (byte[]) is.clone();
		if (is_11_ == null)
			throw new CryptoException("Null user key");
		int i = is_11_.length;
		if (i == 0)
			throw new CryptoException("Invalid user key length");
		x = y = 0;
		for (int i_12_ = 0; i_12_ < 256; i_12_++)
			sBox[i_12_] = i_12_;
		int i_13_ = 0;
		int i_14_ = 0;
		for (int i_15_ = 0; i_15_ < 256; i_15_++) {
			i_14_ = (is_11_[i_13_] & 0xff) + sBox[i_15_] + i_14_ & 0xff;
			int i_16_ = sBox[i_15_];
			sBox[i_15_] = sBox[i_14_];
			sBox[i_14_] = i_16_;
			i_13_ = (i_13_ + 1) % i;
		}
	}
}