/*
 * PGPSignature - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.crypto;

import java.io.DataInput;
import java.io.EOFException;
import java.io.IOException;
import java.math.BigInteger;

public final class PGPSignature {
	private static final byte[] PADDING = { 0, 48, 32, 48, 12, 6, 8, 42, -122,
			72, -122, -9, 13, 2, 5, 5, 0, 4, 16 };

	private static final int SIGNATURE_TYPE = 2;

	private BigInteger number;

	private byte[] extra;

	private byte[] keyId;

	private int pkeAlgorithm = 1;

	private int hashAlgorithm = 1;

	private byte[] hashId;

	public PGPSignature(DataInput datainput) throws IOException {
		int i;
		int i_0_;
		try {
			PGPHeader pgpheader = new PGPHeader(datainput);
			i = pgpheader.len;
			i_0_ = pgpheader.type;
		} catch (EOFException eofexception) {
			throw new EOFException("no signature");
		}
		if (i_0_ == 2)
			read(datainput, i);
		else
			throw new CryptoException(
					"Packet is not a signature. Unknown packet type (" + i_0_
							+ ")");
	}

	public void read(DataInput datainput, int i) throws IOException {
		datainput.readByte();
		int i_1_ = datainput.readByte() & 0xff;
		if (i_1_ != 5 && i_1_ != 7)
			throw new CryptoException(
					"The length of the extra data field is incorrect.");
		datainput.readFully(extra = new byte[i_1_]);
		datainput.readFully(keyId = new byte[8]);
		pkeAlgorithm = datainput.readByte();
		hashAlgorithm = datainput.readByte();
		datainput.readFully(hashId = new byte[2]);
		number = readMPI(datainput);
	}

	public static BigInteger readMPI(DataInput datainput) throws IOException {
		int i = datainput.readShort() & 0xffff;
		byte[] is = new byte[(i + 7) / 8];
		datainput.readFully(is);
		return new BigInteger(1, is);
	}

	public boolean check(RSAPublicKey rsapublickey, byte[] is) {
		if (is[0] != hashId[0] || is[1] != hashId[1])
			return false;
		byte[] is_2_ = rsapublickey.id();
		int i = is_2_.length;
		if (i == keyId.length) {
			for (int i_3_ = 0; i_3_ < i; i_3_++) {
				if (is_2_[i_3_] != keyId[i_3_])
					return false;
			}
		}
		BigInteger biginteger = rsapublickey.encrypt(number);
		BigInteger biginteger_4_ = bigIntFromHash(rsapublickey, is);
		return biginteger.equals(biginteger_4_);
	}

	public void addExtasToHash(MD5 md5) {
		md5.update(extra);
	}

	public static BigInteger bigIntFromHash(RSAPublicKey rsapublickey, byte[] is) {
		if (is.length != 16)
			throw new Error(
					"The code for hashes of lengths other than 16 is not in place yet.");
		int i = rsapublickey.bitLength() / 8;
		int i_5_ = i - (19 + is.length);
		byte[] is_6_ = new byte[i];
		is_6_[0] = (byte) 0;
		is_6_[1] = (byte) 1;
		for (int i_7_ = 2; i_7_ < i_5_; i_7_++)
			is_6_[i_7_] = (byte) -1;
		System.arraycopy(PADDING, 0, is_6_, i_5_, 19);
		System.arraycopy(is, 0, is_6_, i_5_ + 19, is.length);
		return new BigInteger(is_6_);
	}
}