/*
 * EPS - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pslib;

import java.io.IOException;

import com.renderx.graphics.EPSImage;
import com.renderx.util.SeekableInput;

public class EPS {
	EPSImage eps = null;

	String shortname;

	double[] bbox;

	public EPS(EPSImage epsimage) {
		this.eps = epsimage;
		shortname = this.eps.toDisplayString();
		int i = shortname.lastIndexOf('/');
		if (i != -1)
			shortname = shortname.substring(i + 1);
		bbox = this.eps.hiResBoundingBox;
		if (bbox == null)
			bbox = this.eps.boundingBox;
	}

	public void writeEPS(PSOutputStream psoutputstream) throws IOException {
		psoutputstream.println("%%BeginDocument: " + shortname);
		SeekableInput seekableinput = this.eps.openSeekableImageStream();
		try {
			seekableinput.seek(this.eps.psOffset);
			byte[] is = new byte[8192];
			int i;
			for (long l = this.eps.psLength; l > 0L; l -= (long) i) {
				i = l < (long) is.length ? (int) l : is.length;
				int i_0_ = seekableinput.read(is, 0, i);
				psoutputstream.write(is, 0, i_0_);
				if (i_0_ < i)
					break;
			}
		} finally {
			seekableinput.close();
		}
		psoutputstream.println("\n%%EndDocument");
	}
}