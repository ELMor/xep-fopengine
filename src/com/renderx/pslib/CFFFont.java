/*
 * CFFFont - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pslib;

import java.io.IOException;

import com.renderx.fonts.FontDescriptor;
import com.renderx.fonts.Metric;
import com.renderx.fonts.TTMetric;
import com.renderx.util.SeekableInput;
import com.renderx.util.SeekableInputOffsetFilter;

public class CFFFont {
	FontDescriptor fdesc;

	public CFFFont(FontDescriptor fontdescriptor) {
		fdesc = fontdescriptor;
	}

	public void write(PSOutputStream psoutputstream) throws IOException {
		Metric metric = fdesc.record.getMetric();
		psoutputstream.println("%!PS-Adobe-3.0 Resource-FontSet");
		psoutputstream
				.println("%%DocumentNeededResources: ProcSet (FontSetInit)");
		psoutputstream.println("%%Title: (FontSet/" + metric.fontName + ")");
		psoutputstream.println("%%Version: 1.000");
		psoutputstream.println("%%EndComments");
		psoutputstream.println("%%IncludeResource: ProcSet (FontSetInit)");
		psoutputstream.println("%%BeginResource: FontSet (" + metric.fontName
				+ ")");
		psoutputstream.println("/FontSetInit /ProcSet findresource begin");
		SeekableInput seekableinput = fdesc.record.openSeekableFontStream();
		SeekableInputOffsetFilter seekableinputoffsetfilter = new SeekableInputOffsetFilter(
				seekableinput, ((TTMetric) metric).cff_offset);
		try {
			long l = ((TTMetric) metric).cff_length;
			String string = "/" + metric.fontName + " " + l + " StartData";
			psoutputstream.println("%%BeginData: "
					+ (l + (long) string.length() + 1L) + " Binary Bytes");
			psoutputstream.println(string);
			byte[] is = new byte[16384];
			while (l > 0L) {
				int i = l < (long) is.length ? (int) l : is.length;
				l -= (long) i;
				seekableinputoffsetfilter.read(is, 0, i);
				psoutputstream.write(is, 0, i);
			}
			psoutputstream.println();
		} catch (Exception exception) {
			psoutputstream.doc.errorHandler.exception(("Broken font file '"
					+ fdesc.record.fontfile + "'."), exception);
		} finally {
			seekableinputoffsetfilter.close();
			seekableinput.close();
		}
		psoutputstream.println("%%EndData");
		psoutputstream.println("%%EndResource");
	}
}