/*
 * ASCII85_Encode - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pslib;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ASCII85_Encode extends FilterOutputStream {
	private static final int[] power85 = { 1, 85, 7225, 614125, 52200625 };

	private static final int charsPerRow = 64;

	private int offset = 0;

	private int count = 0;

	private long[] inbytes = new long[4];

	public ASCII85_Encode(OutputStream outputstream) {
		super(outputstream);
	}

	public void write(int i) throws IOException {
		inbytes[count++] = (long) (i & 0xff);
		if (count == 4)
			writeTuple();
	}

	public void write(byte[] is) throws IOException {
		write(is, 0, is.length);
	}

	public void write(byte[] is, int i, int i_0_) throws IOException {
		for (int i_1_ = i; i_1_ < i + i_0_; i_1_++)
			write(is[i_1_]);
	}

	public void EOD() throws IOException {
		if (count > 0)
			writeTuple();
		super.write(126);
		super.write(62);
	}

	private void writeTuple() throws IOException {
		if (offset > 64) {
			super.write(10);
			offset = 0;
		}
		long l = 0L;
		for (int i = 0; i < 4; i++) {
			l <<= 8;
			l |= inbytes[i];
		}
		if (l == 0L) {
			super.write(122);
			offset++;
		} else {
			for (int i = 0; i <= count; i++)
				super
						.write((byte) (int) (33L + l / (long) power85[4 - i]
								% 85L));
			offset += 5;
		}
		count = 0;
		inbytes[0] = inbytes[1] = inbytes[2] = inbytes[3] = 0L;
	}
}