/*
 * Image - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pslib;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

import com.renderx.fonts.FontCatalog;
import com.renderx.fonts.FontConfigurationException;
import com.renderx.graphics.BitmapImage;
import com.renderx.graphics.EPSImage;
import com.renderx.graphics.ImageFactory;
import com.renderx.graphics.ImageFormatException;
import com.renderx.graphics.UnregisteredMIMETypeException;
import com.renderx.graphics.VectorImage;
import com.renderx.graphics.vector.Contour;
import com.renderx.graphics.vector.ExternalImage;
import com.renderx.graphics.vector.GraphicGroup;
import com.renderx.graphics.vector.GraphicObject;
import com.renderx.graphics.vector.ImageTree;
import com.renderx.graphics.vector.OpaqueColor;
import com.renderx.graphics.vector.PaintSpec;
import com.renderx.graphics.vector.Path;
import com.renderx.graphics.vector.StrokeSpec;
import com.renderx.graphics.vector.Subpath;
import com.renderx.graphics.vector.Text;
import com.renderx.util.DefaultErrorHandler;
import com.renderx.util.DeflaterOutputStream;
import com.renderx.util.ErrorHandler;
import com.renderx.util.URLCache;
import com.renderx.util.URLSpec;
import com.renderx.util.User;

import junit.framework.Assert;
import junit.framework.TestCase;

public class Image {
	static final int MAX_SOURCE_LENGTH = 220;

	com.renderx.graphics.Image image = null;

	final String id;

	final String source;

	final boolean isInstream;

	boolean clippingPending = false;

	VectorImage lastSubobj;

	double[] clipMatrix = { 1.0, 0.0, 0.0, 1.0, 0.0, 0.0 };

	double[] contentMatrix = { 1.0, 0.0, 0.0, 1.0, 0.0, 0.0 };

	ImageTree imageTree = null;

	public static final class Test extends TestCase {
		ErrorHandler errorHandler = new DefaultErrorHandler();

		URLCache urlcache = new URLCache();

		FontCatalog fontCatalog;

		ImageFactory imageFactory;

		public void setUp() {
			try {
				fontCatalog = new FontCatalog(urlcache, errorHandler);
				imageFactory = new ImageFactory(fontCatalog, urlcache,
						errorHandler);
			} catch (IOException ioexception) {
				Assert.fail("Cann't create fontCatalog or imageFactory: "
						+ ioexception);
			} catch (FontConfigurationException fontconfigurationexception) {
				Assert.fail("Cannot create font catalog: "
						+ fontconfigurationexception);
			}
		}

		public void test_write(String string, String string_0_, int i)
				throws IOException, ImageFormatException,
				UnregisteredMIMETypeException {
			ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
			String string_1_ = string_0_;
			if (string_0_.equals("tiff"))
				string_1_ = "tif";
			else if (string_0_.equals("jpeg"))
				string_1_ = "jpg";
			PSDocument psdocument = new PSDocument(bytearrayoutputstream,
					new File(User.getProperty("user.dir")), imageFactory,
					fontCatalog, errorHandler);
			psdocument.setLanguageLevel(i);
			String string_2_ = "com/renderx/pslib/TEST_DATA/" + string + "."
					+ string_1_;
			String string_3_ = "com/renderx/pslib/TEST_DATA/" + string
					+ ".test" + i;
			Image image = new Image(
					(psdocument.imageFactory.makeImage(new URLSpec("resource:"
							+ string_2_), "image/" + string_0_)), "_" + string
							+ ".test" + i);
			image.write(psdocument.out);
			psdocument.out.close();
			byte[] is = bytearrayoutputstream.toByteArray();
			InputStream inputstream = ClassLoader
					.getSystemResourceAsStream(string_3_);
			ByteArrayOutputStream bytearrayoutputstream_4_ = new ByteArrayOutputStream();
			byte[] is_5_ = new byte[1024];
			int i_6_;
			while ((i_6_ = inputstream.read(is_5_, 0, 1024)) >= 0)
				bytearrayoutputstream_4_.write(is_5_, 0, i_6_);
			bytearrayoutputstream_4_.close();
			inputstream.close();
			byte[] is_7_ = bytearrayoutputstream_4_.toByteArray();
			Assert.assertEquals("Size isn't valid: ", is_7_.length, is.length);
			for (int i_8_ = 0; i_8_ < is_7_.length; i_8_++)
				Assert.assertEquals(is_7_[i_8_], is[i_8_]);
		}

		public void test_write2_gif() throws IOException, ImageFormatException,
				UnregisteredMIMETypeException {
			test_write("test_gif", "gif", 2);
		}

		public void test_write3_gif() throws IOException, ImageFormatException,
				UnregisteredMIMETypeException {
			test_write("test_gif", "gif", 3);
		}

		public void test_write2_tiff() throws IOException,
				ImageFormatException, UnregisteredMIMETypeException {
			test_write("test_tiff", "tiff", 2);
		}

		public void test_write3_tiff() throws IOException,
				ImageFormatException, UnregisteredMIMETypeException {
			test_write("test_tiff", "tiff", 3);
		}

		public void test_write2_png() throws IOException, ImageFormatException,
				UnregisteredMIMETypeException {
			test_write("test_png", "png", 2);
		}

		public void test_write3_png() throws IOException, ImageFormatException,
				UnregisteredMIMETypeException {
			test_write("test_png", "png", 3);
		}

		public void test_write2_jpeg() throws IOException,
				ImageFormatException, UnregisteredMIMETypeException {
			test_write("test_jpeg", "jpeg", 2);
		}

		public void test_write3_jpeg() throws IOException,
				ImageFormatException, UnregisteredMIMETypeException {
			test_write("test_jpeg", "jpeg", 3);
		}
	}

	class SubTreeVectorImage extends VectorImage {
		ImageTree imageTree = null;

		public void parse() throws IOException, ImageFormatException {
			/* empty */
		}

		public SubTreeVectorImage(ImageTree imagetree) throws IOException {
			imageTree = imagetree;
			this.source = new URLSpec("data:,");
		}

		public ImageTree getImageTree() throws IOException,
				ImageFormatException {
			return imageTree;
		}
	}

	public Image(com.renderx.graphics.Image image_9_, String string)
			throws InternalException, IOException, ImageFormatException {
		id = string;
		this.image = image_9_;
		isInstream = image_9_.source.scheme != 3;
		if (isInstream)
			source = null;
		else {
			String string_10_ = image_9_.source.toString();
			if (string_10_.length() > 220)
				string_10_ = "..."
						+ string_10_.substring(string_10_.length() + 3 - 220);
			source = string_10_;
		}
		if (!(image_9_ instanceof BitmapImage)) {
			if (image_9_ instanceof VectorImage) {
				VectorImage vectorimage = (VectorImage) image_9_;
				if (vectorimage instanceof EPSImage)
					return;
				if (vectorimage.supportsVectorRendering) {
					imageTree = vectorimage.getImageTree();
					return;
				}
				if (vectorimage.hasBitmapPreview) {
					this.image = vectorimage.getBitmapPreview();
					return;
				}
				if (vectorimage.supportsRasterizing) {
					this.image = vectorimage.rasterize(300.0);
					return;
				}
			}
			throw new ImageFormatException("Unsupported format "
					+ image_9_.mimetype + " for image '"
					+ image_9_.toDisplayString() + "'");
		}
	}

	void write(PSOutputStream psoutputstream) throws IOException,
			ImageFormatException {
		if (isInstream)
			psoutputstream.println("%RNDRXBeginInstreamImage");
		else
			psoutputstream.println("%RNDRXBeginExternalImage: " + source);
		try {
			if (this.image instanceof BitmapImage)
				writeBitmapImage(psoutputstream, (BitmapImage) this.image);
			else if (this.image instanceof VectorImage)
				writeVectorImage(psoutputstream, (VectorImage) this.image);
			else
				throw new InternalException(
						"Invalid image type: neither bitmap nor vector");
		} finally {
			if (isInstream)
				psoutputstream.println("%RNDRXEndInstreamImage");
			else
				psoutputstream.println("%RNDRXEndExternalImage");
		}
	}

	void writeVectorImage(PSOutputStream psoutputstream, VectorImage vectorimage)
			throws IOException, ImageFormatException {
		if (vectorimage instanceof EPSImage)
			writeEPSImage(psoutputstream, new EPS((EPSImage) vectorimage));
		else if (vectorimage.supportsVectorRendering)
			writeImageTree(psoutputstream, imageTree);
		else
			throw new InternalException(
					"Unsupported image type, erroneously passed here");
	}

	void writeEPSImage(PSOutputStream psoutputstream, EPS eps)
			throws IOException {
		psoutputstream.println("/ImageForm" + id + " <<");
		psoutputstream.println("/FormType 1");
		psoutputstream.println("/BBox [" + (float) eps.bbox[0] + " "
				+ (float) eps.bbox[1] + " " + (float) eps.bbox[2] + " "
				+ (float) eps.bbox[3] + "]");
		psoutputstream.println("/Matrix [1 0 0 1 " + (float) -eps.bbox[0] + " "
				+ (float) -eps.bbox[1] + " ]");
		psoutputstream.println("/PaintProc {");
		psoutputstream.println("BeginEPSF");
		eps.writeEPS(psoutputstream);
		psoutputstream.println("EndEPSF");
		psoutputstream.println("} bind >> def");
	}

	void writeBitmapImage(PSOutputStream psoutputstream, BitmapImage bitmapimage)
			throws IOException {
		boolean bool = (bitmapimage.canCopyData
				&& bitmapimage.compressionMethod != 0 && (bitmapimage.compressionMethod != 2 || psoutputstream.doc.LANGUAGE_LEVEL != 2));
		if (!bool && !bitmapimage.canExpandData)
			psoutputstream.doc.errorHandler
					.error("cannot use neither raw nor expanded bitmap data in file '"
							+ bitmapimage.toDisplayString() + "'.");
		else {
			psoutputstream.println("/ImageForm" + id + " <<");
			psoutputstream.println("/FormType 1");
			psoutputstream.println("/BBox [0 0 1 1]");
			psoutputstream.println("/Matrix [" + (float) bitmapimage.width
					+ " 0 0 " + (float) bitmapimage.height + " 0 0 ]");
			psoutputstream.println("/PaintProc {");
			psoutputstream.println("pop");
			psoutputstream.println("gsave");
			psoutputstream
					.println((psoutputstream.doc.LANGUAGE_LEVEL == 3 ? "ImageData"
							+ id + " 0 setfileposition"
							: "userdict /i 0 put")
							+ "             % rewind our image data");
			switch (bitmapimage.colorSpace) {
			case 1:
				psoutputstream.println("/DeviceRGB setcolorspace");
				break;
			case 2:
			case 3:
				psoutputstream.println("/DeviceCMYK setcolorspace");
				break;
			case 4:
			case 5:
				psoutputstream.println("/DeviceGray setcolorspace");
				break;
			case 6: {
				int i = bitmapimage.colorTable.length / 3;
				psoutputstream.println("[/Indexed /DeviceRGB " + (i - 1));
				psoutputstream.print("<");
				for (int i_11_ = 0; i_11_ < bitmapimage.colorTable.length; i_11_++) {
					if (i_11_ != 0) {
						if (i_11_ % 30 == 0)
							psoutputstream.println();
						else if (i_11_ % 3 == 0)
							psoutputstream.print(" ");
					}
					psoutputstream.writeHexByte(bitmapimage.colorTable[i_11_]);
				}
				psoutputstream.println(">");
				psoutputstream.println("] setcolorspace");
				break;
			}
			}
			psoutputstream.println("<< /ImageType 1");
			psoutputstream.println("/Width " + bitmapimage.pxWidth);
			psoutputstream.println("/Height " + bitmapimage.pxHeight);
			psoutputstream.println("/ImageMatrix [ " + bitmapimage.pxWidth
					+ " 0 0 -" + bitmapimage.pxHeight + " 0 "
					+ bitmapimage.pxHeight + " ]");
			psoutputstream.println("/BitsPerComponent "
					+ bitmapimage.bitsPerComponent);
			if (psoutputstream.doc.LANGUAGE_LEVEL == 3)
				psoutputstream.println("/DataSource ImageData" + id);
			else {
				psoutputstream.println("/DataSource {");
				psoutputstream.println("ImageData" + id + " i get");
				psoutputstream.println("/i i 1 add store ");
				psoutputstream.println("} bind");
			}
			switch (bitmapimage.colorSpace) {
			case 4:
				psoutputstream.println("/Decode [0 1]");
				break;
			case 5:
				psoutputstream.println("/Decode [1 0]");
				break;
			case 1:
				psoutputstream.println("/Decode [0 1 0 1 0 1]");
				break;
			case 2:
				psoutputstream.println("/Decode [0 1 0 1 0 1 0 1]");
				break;
			case 3:
				psoutputstream.println("/Decode [1 0 1 0 1 0 1 0]");
				break;
			case 6:
				psoutputstream.println("/Decode [0 "
						+ ((1 << bitmapimage.bitsPerComponent) - 1) + "]");
				break;
			}
			psoutputstream.println(">> image");
			psoutputstream.println();
			psoutputstream.println("grestore");
			psoutputstream.println("} bind");
			psoutputstream.println(">> def");
			psoutputstream.println("/ImageData" + id);
			psoutputstream.print("currentfile /ASCII85Decode filter ");
			if (bool) {
				switch (bitmapimage.compressionMethod) {
				case 7:
					psoutputstream.print("<</EarlyChange 0");
					writePredictor(psoutputstream, bitmapimage, true);
					psoutputstream.print(">> /LZWDecode");
					break;
				case 8:
					writePredictor(psoutputstream, bitmapimage, false);
					psoutputstream.print("/LZWDecode");
					break;
				case 2:
					writePredictor(psoutputstream, bitmapimage, false);
					psoutputstream.print("/FlateDecode");
					break;
				case 4:
					psoutputstream.print("<</K 0 /Columns "
							+ bitmapimage.pxWidth + " /Rows "
							+ bitmapimage.pxHeight + ">> /CCITTFaxDecode");
					break;
				case 5:
					psoutputstream.print("<</K 1 /Columns "
							+ bitmapimage.pxWidth + " /Rows "
							+ bitmapimage.pxHeight + ">> /CCITTFaxDecode");
					break;
				case 6:
					psoutputstream.print("<</K -1 /Columns "
							+ bitmapimage.pxWidth + " /Rows "
							+ bitmapimage.pxHeight + ">> /CCITTFaxDecode");
					break;
				case 3:
					psoutputstream.print("/DCTDecode");
					break;
				case 1:
					psoutputstream.print("/RunLengthDecode");
					break;
				}
			} else
				psoutputstream
						.print(psoutputstream.doc.LANGUAGE_LEVEL == 2 ? "/RunLengthDecode"
								: "/FlateDecode");
			psoutputstream.print(" filter ");
			psoutputstream
					.println(psoutputstream.doc.LANGUAGE_LEVEL == 3 ? "/ReusableStreamDecode filter"
							: "CreateDataArray");
			try {
				ASCII85_Encode ascii85_encode = new ASCII85_Encode(
						psoutputstream);
				if (bool)
					bitmapimage.copyData(ascii85_encode);
				else if (psoutputstream.doc.LANGUAGE_LEVEL == 3) {
					DeflaterOutputStream deflateroutputstream = new DeflaterOutputStream(
							ascii85_encode);
					bitmapimage.expandData(deflateroutputstream);
					deflateroutputstream.finish();
				} else {
					RunLength_Encode runlength_encode = new RunLength_Encode(
							ascii85_encode);
					bitmapimage.expandData(runlength_encode);
					runlength_encode.EOD();
				}
				ascii85_encode.EOD();
			} catch (Exception exception) {
				psoutputstream.doc.errorHandler
						.exception(("Cannot insert image '"
								+ this.image.toDisplayString() + "'."),
								exception);
			}
			psoutputstream.println("\ndef");
		}
	}

	private void writePredictor(PSOutputStream psoutputstream,
			BitmapImage bitmapimage, boolean bool) throws IOException {
		if (bitmapimage.predictor != 0) {
			psoutputstream.print(bool ? " " : "<<");
			switch (bitmapimage.predictor) {
			case 1:
				psoutputstream.print("/Predictor 2");
				break;
			case 2:
				psoutputstream.print("/Predictor 15");
				break;
			}
			psoutputstream.print(" /Columns " + bitmapimage.pxWidth);
			if (bitmapimage.bitsPerComponent != 8)
				psoutputstream.print(" /BitsPerComponent "
						+ bitmapimage.bitsPerComponent);
			switch (bitmapimage.colorSpace) {
			case 1:
				psoutputstream.print(" /Colors 3");
				break;
			case 2:
			case 3:
				psoutputstream.print(" /Colors 4");
				break;
			}
			if (!bool)
				psoutputstream.print(">> ");
		}
	}

	void writeImageTree(PSOutputStream psoutputstream, ImageTree imagetree)
			throws IOException {
		psoutputstream.println("/ImageForm" + id + " <<");
		psoutputstream.println("/FormType 1");
		psoutputstream.println("/BBox [" + (float) imagetree.startX + " "
				+ (float) imagetree.startY + " "
				+ ((float) imagetree.startX + (float) imagetree.width) + " "
				+ ((float) imagetree.startY + (float) imagetree.height) + "]");
		psoutputstream.println("/Matrix [1 0 0 1 0 0]");
		psoutputstream.println("/PaintProc {");
		psoutputstream.doc.setCharSpacing(0.0F);
		psoutputstream.doc.setWordSpacing(0.0F);
		PaintGraphicObject(psoutputstream, imagetree.root);
		psoutputstream.println("} bind >> def");
	}

	void PaintGraphicObject(PSOutputStream psoutputstream,
			GraphicObject graphicobject) throws IOException {
		if (graphicobject instanceof GraphicGroup) {
			GraphicGroup graphicgroup = (GraphicGroup) graphicobject;
			boolean bool = false;
			boolean bool_12_ = false;
			psoutputstream.doc.setFontSize(0.0F);
			double[] ds = contentMatrix;
			double[] ds_13_ = contentMatrix;
			if (!bool
					&& !clippingPending
					&& (!graphicgroup.isIdentityTransform() || graphicgroup.clippath != null)) {
				psoutputstream.println("gsave");
				bool = true;
			}
			if (!graphicgroup.isIdentityTransform() && !clippingPending) {
				printMatrix(psoutputstream, graphicgroup.matrix);
				ds_13_ = graphicgroup.multipleMatrix(contentMatrix,
						graphicgroup.matrix);
			}
			if (graphicgroup.clippath != null && !clippingPending) {
				bool_12_ = !clippingPending;
				clippingPending = true;
				GraphicGroup graphicgroup_14_ = graphicgroup.clippath;
				graphicgroup.clippath = null;
				graphicgroup.matrix = new double[] { 1.0, 0.0, 0.0, 1.0, 0.0,
						0.0 };
				double[] ds_15_ = calcBBox(imageTree.startX, imageTree.startY,
						imageTree.width, imageTree.height, graphicgroup
								.reverseMatrix(ds_13_), graphicgroup);
				ImageTree imagetree = new ImageTree(ds_15_[0], ds_15_[1],
						ds_15_[2] - ds_15_[0], ds_15_[3] - ds_15_[1]);
				imagetree.root = graphicgroup;
				SubTreeVectorImage subtreevectorimage = new SubTreeVectorImage(
						imagetree);
				subtreevectorimage.supportsVectorRendering = true;
				lastSubobj = subtreevectorimage;
				PaintGraphicObject(psoutputstream, graphicgroup_14_);
			} else if (clippingPending) {
				double[] ds_16_ = clipMatrix;
				double[] ds_17_ = (graphicgroup.isIdentityTransform() ? clipMatrix
						: graphicgroup.multipleMatrix(clipMatrix,
								graphicgroup.matrix));
				Enumeration enumeration = graphicgroup.children.elements();
				while (enumeration.hasMoreElements()) {
					clipMatrix = ds_17_;
					GraphicObject graphicobject_18_ = (GraphicObject) enumeration
							.nextElement();
					if (graphicobject_18_ instanceof Contour) {
						psoutputstream.println("gsave");
						if (!graphicgroup.isIdentityTransform(clipMatrix))
							printMatrix(psoutputstream, clipMatrix);
						PaintGraphicObject(psoutputstream, graphicobject_18_);
						if (!graphicgroup.isIdentityTransform(clipMatrix))
							printMatrix(psoutputstream, graphicgroup
									.reverseMatrix(clipMatrix));
						psoutputstream.doc.placeImage(lastSubobj);
						psoutputstream.println("grestore");
					} else if (graphicobject_18_ instanceof GraphicGroup)
						PaintGraphicObject(psoutputstream, graphicobject_18_);
				}
				clipMatrix = ds_16_;
			} else {
				Enumeration enumeration = graphicgroup.children.elements();
				while (enumeration.hasMoreElements()) {
					contentMatrix = ds_13_;
					PaintGraphicObject(psoutputstream,
							((GraphicObject) enumeration.nextElement()));
				}
				contentMatrix = ds;
			}
			if (bool_12_)
				clippingPending = false;
			if (bool)
				psoutputstream.println("grestore");
		} else if (graphicobject instanceof Contour) {
			Contour contour = (Contour) graphicobject;
			boolean bool = contour.fill != null;
			boolean bool_19_ = contour.stroke != null
					&& contour.stroke.paint != null;
			if (bool || bool_19_ || clippingPending) {
				if (contour instanceof Path) {
					Path path = (Path) contour;
					psoutputstream.println("newpath");
					Enumeration enumeration = path.subs.elements();
					while (enumeration.hasMoreElements())
						PaintSubpath(psoutputstream, (Subpath) enumeration
								.nextElement());
					if (bool && !clippingPending) {
						if (bool_19_)
							psoutputstream.println("gsave");
						PaintFillSpec(psoutputstream, path.fill);
						psoutputstream.println(path.fillRule == 0 ? "fill"
								: "eofill");
						if (bool_19_)
							psoutputstream.println("grestore");
					}
					if (bool_19_ && !clippingPending) {
						PaintStrokeSpec(psoutputstream, path.stroke);
						psoutputstream.println("stroke");
					}
				} else if (graphicobject instanceof Text) {
					Text text = (Text) contour;
					psoutputstream.println("" + (float) text.x + " "
							+ (float) text.y + " moveto");
					psoutputstream.doc
							.setFontStretch((float) text.font.stretch);
					psoutputstream.doc.u28.selectFont(text.font.family,
							text.font.weight, text.font.style,
							text.font.variant);
					if (clippingPending) {
						int i = psoutputstream.doc.textRenderMode;
						psoutputstream.doc.textRenderMode = 2;
						psoutputstream.doc.setFontSize((float) text.font.size);
						psoutputstream.doc.u28.processText(new String(
								text.value));
						psoutputstream.doc.textRenderMode = i;
					} else {
						if (bool) {
							if (bool_19_)
								psoutputstream.println("gsave");
							PaintFillSpec(psoutputstream, text.fill);
							int i = psoutputstream.doc.textRenderMode;
							psoutputstream.doc.textRenderMode = 0;
							psoutputstream.doc
									.setFontSize((float) text.font.size);
							psoutputstream.doc.u28.processText(new String(
									text.value));
							psoutputstream.doc.textRenderMode = i;
							if (bool_19_)
								psoutputstream.println("grestore");
						}
						if (bool_19_) {
							PaintStrokeSpec(psoutputstream, text.stroke);
							int i = psoutputstream.doc.textRenderMode;
							psoutputstream.doc.textRenderMode = 1;
							psoutputstream.doc
									.setFontSize((float) text.font.size);
							psoutputstream.doc.u28.processText(new String(
									text.value));
							psoutputstream.doc.textRenderMode = i;
							psoutputstream.println("stroke");
						}
					}
				}
				if (clippingPending)
					psoutputstream.println(contour.clipRule == 0 ? "clip"
							: "eoclip");
			}
		} else if (graphicobject instanceof ExternalImage) {
			ExternalImage externalimage = (ExternalImage) graphicobject;
			psoutputstream.doc.placeImage(externalimage.image);
		}
	}

	void PaintSubpath(PSOutputStream psoutputstream, Subpath subpath)
			throws IOException {
		psoutputstream.println("" + (float) subpath.startX + " "
				+ (float) subpath.startY + " moveto");
		Enumeration enumeration = subpath.segs.elements();
		while (enumeration.hasMoreElements()) {
			Subpath.Segment segment = (Subpath.Segment) enumeration
					.nextElement();
			if (segment instanceof Subpath.Line) {
				Subpath.Line line = (Subpath.Line) segment;
				psoutputstream.println("" + (float) line.xend + " "
						+ (float) line.yend + " lineto");
			} else if (segment instanceof Subpath.Curve) {
				Subpath.Curve curve = (Subpath.Curve) segment;
				psoutputstream.println("" + (float) curve.cpx1 + " "
						+ (float) curve.cpy1 + " " + (float) curve.cpx2 + " "
						+ (float) curve.cpy2 + " " + (float) curve.xend + " "
						+ (float) curve.yend + " curveto");
			}
		}
		if (subpath.closed)
			psoutputstream.println("closepath");
	}

	void PaintStrokeSpec(PSOutputStream psoutputstream, StrokeSpec strokespec)
			throws IOException {
		psoutputstream.println("" + (float) strokespec.thickness
				+ " setlinewidth ");
		psoutputstream.println("" + strokespec.lineCap + " setlinecap");
		psoutputstream.println("" + strokespec.lineJoin + " setlinejoin");
		if (strokespec.lineJoin == 0)
			psoutputstream.println("" + (float) strokespec.miterLimit
					+ " setmiterlimit");
		PaintFillSpec(psoutputstream, strokespec.paint);
		if (strokespec.dashArray == null)
			psoutputstream.println("[] 0 setdash");
		else {
			psoutputstream.print("[");
			for (int i = 0; i < strokespec.dashArray.length; i++) {
				if (i > 0)
					psoutputstream.print(" ");
				psoutputstream.print("" + (float) strokespec.dashArray[i]);
			}
			psoutputstream.println("] " + (float) strokespec.dashPhase
					+ " setdash");
		}
	}

	void PaintFillSpec(PSOutputStream psoutputstream, PaintSpec paintspec)
			throws IOException {
		if (paintspec instanceof OpaqueColor.Grayscale) {
			OpaqueColor.Grayscale grayscale = (OpaqueColor.Grayscale) paintspec;
			psoutputstream.doc.setGrayColor((float) grayscale.g);
		} else if (paintspec instanceof OpaqueColor.RGB) {
			OpaqueColor.RGB rgb = (OpaqueColor.RGB) paintspec;
			psoutputstream.doc.setRGBColor((float) rgb.r, (float) rgb.g,
					(float) rgb.b);
		} else if (paintspec instanceof OpaqueColor.CMYK) {
			OpaqueColor.CMYK cmyk = (OpaqueColor.CMYK) paintspec;
			psoutputstream.doc.setCMYKColor((float) cmyk.c, (float) cmyk.m,
					(float) cmyk.y, (float) cmyk.k);
		} else if (paintspec instanceof OpaqueColor.SpotColor) {
			OpaqueColor.SpotColor spotcolor = (OpaqueColor.SpotColor) paintspec;
			float[] fs = { 0.0F };
			if (spotcolor.altcolor != null) {
				if (spotcolor.altcolor instanceof OpaqueColor.Grayscale) {
					OpaqueColor.Grayscale grayscale = (OpaqueColor.Grayscale) spotcolor.altcolor;
					fs = new float[] { (float) grayscale.g };
				} else if (spotcolor.altcolor instanceof OpaqueColor.RGB) {
					OpaqueColor.RGB rgb = (OpaqueColor.RGB) spotcolor.altcolor;
					fs = new float[] { (float) rgb.r, (float) rgb.g,
							(float) rgb.b };
				} else if (spotcolor.altcolor instanceof OpaqueColor.CMYK) {
					OpaqueColor.CMYK cmyk = (OpaqueColor.CMYK) spotcolor.altcolor;
					fs = new float[] { (float) cmyk.c, (float) cmyk.m,
							(float) cmyk.y, (float) cmyk.k };
				}
			}
			psoutputstream.doc.setSpotColor((float) spotcolor.tint,
					spotcolor.colorant, fs);
		} else if (paintspec instanceof OpaqueColor.Registration) {
			OpaqueColor.Registration registration = (OpaqueColor.Registration) paintspec;
			psoutputstream.doc.setSpotColor((float) registration.tint, "All",
					new float[] { 0.0F });
		}
	}

	private void printMatrix(PSOutputStream psoutputstream, double[] ds)
			throws IOException {
		if (ds[0] == 1.0 && ds[1] == 0.0 && ds[2] == 0.0 && ds[3] == 1.0)
			psoutputstream.println("" + (float) ds[4] + " " + (float) ds[5]
					+ " translate");
		else if (ds[1] == 0.0 && ds[2] == 0.0 && ds[4] == 0.0 && ds[5] == 0.0)
			psoutputstream.println("" + (float) ds[0] + " " + (float) ds[3]
					+ " scale");
		else
			psoutputstream.println("[" + (float) ds[0] + " " + (float) ds[1]
					+ " " + (float) ds[2] + " " + (float) ds[3] + " "
					+ (float) ds[4] + " " + (float) ds[5] + "] concat");
	}

	private double[] calcBBox(double d, double d_20_, double d_21_,
			double d_22_, double[] ds, GraphicGroup graphicgroup) {
		double[] ds_23_ = { 0.0, 0.0, 0.0, 0.0 };
		double[] ds_24_ = graphicgroup.transformCoordinates(ds, d, d_20_);
		if (ds_24_[0] < ds_23_[0])
			ds_23_[0] = ds_24_[0];
		else if (ds_24_[0] > ds_23_[2])
			ds_23_[2] = ds_24_[0];
		if (ds_24_[1] < ds_23_[1])
			ds_23_[1] = ds_24_[1];
		else if (ds_24_[1] > ds_23_[3])
			ds_23_[3] = ds_24_[1];
		ds_24_ = graphicgroup.transformCoordinates(ds, d, d_22_);
		if (ds_24_[0] < ds_23_[0])
			ds_23_[0] = ds_24_[0];
		else if (ds_24_[0] > ds_23_[2])
			ds_23_[2] = ds_24_[0];
		if (ds_24_[1] < ds_23_[1])
			ds_23_[1] = ds_24_[1];
		else if (ds_24_[1] > ds_23_[3])
			ds_23_[3] = ds_24_[1];
		ds_24_ = graphicgroup.transformCoordinates(ds, d_21_, d_22_);
		if (ds_24_[0] < ds_23_[0])
			ds_23_[0] = ds_24_[0];
		else if (ds_24_[0] > ds_23_[2])
			ds_23_[2] = ds_24_[0];
		if (ds_24_[1] < ds_23_[1])
			ds_23_[1] = ds_24_[1];
		else if (ds_24_[1] > ds_23_[3])
			ds_23_[3] = ds_24_[1];
		ds_24_ = graphicgroup.transformCoordinates(ds, d_21_, d_20_);
		if (ds_24_[0] < ds_23_[0])
			ds_23_[0] = ds_24_[0];
		else if (ds_24_[0] > ds_23_[2])
			ds_23_[2] = ds_24_[0];
		if (ds_24_[1] < ds_23_[1])
			ds_23_[1] = ds_24_[1];
		else if (ds_24_[1] > ds_23_[3])
			ds_23_[3] = ds_24_[1];
		return ds_23_;
	}
}