/*
 * T1Font - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pslib;

import java.io.IOException;

import com.renderx.fonts.CharMetrics;
import com.renderx.fonts.Encoding;
import com.renderx.fonts.FontDescriptor;
import com.renderx.fonts.Metric;
import com.renderx.fonts.PFxFont;
import com.renderx.util.SeekableInput;

public class T1Font {
	FontDescriptor fdesc;

	public T1Font(FontDescriptor fontdescriptor) {
		fdesc = fontdescriptor;
	}

	public void write(PSOutputStream psoutputstream) {
		if (fdesc.record.fontfile == null)
			psoutputstream.doc.errorHandler
					.error("No PFA/PFB file found for font "
							+ fdesc.record.getMetric().fontName);
		else {
			try {
				SeekableInput seekableinput = fdesc.record
						.openSeekableFontStream();
				try {
					PFxFont pfxfont = new PFxFont(seekableinput);
					if (fdesc.record.subset) {
						Metric metric = fdesc.record.getMetric();
						pfxfont.newSubset();
						for (int i = 0; i < 65536; i++) {
							if (fdesc.used.checkGlyph((char) i)) {
								CharMetrics charmetrics = metric.ucm((char) i);
								if (charmetrics != null)
									pfxfont.addGlyph(charmetrics.n,
											((Encoding) fdesc.encodingTable
													.get(0)));
							}
						}
						pfxfont.endSubset();
					}
					psoutputstream.print(pfxfont.head);
					if (fdesc.record.subset)
						psoutputstream.writeHexArray(pfxfont.subsetbody);
					else
						psoutputstream.writeHexArray(pfxfont.body);
					psoutputstream.println("\n" + pfxfont.tail);
				} finally {
					seekableinput.close();
				}
			} catch (IOException ioexception) {
				psoutputstream.doc.errorHandler
						.exception("Cannot read font file '"
								+ fdesc.record.fontfile + "'", ioexception);
			}
		}
	}
}