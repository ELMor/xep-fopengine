/*
 * Info - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pslib;

import java.io.IOException;
import java.util.Enumeration;

import com.renderx.util.Hashtable;

public class Info extends Hashtable {
	public void set(String string, String string_0_) {
		if (string != null && string_0_ != null)
			this.put(string, string_0_);
	}

	public void writeHeaderComment(PSOutputStream psoutputstream, String string)
			throws IOException {
		String string_1_ = (String) this.get(string);
		if (string_1_ != null) {
			psoutputstream.print("%%" + string + ": ");
			string_1_ = Strings.normalize_space(string_1_);
			int i = 251 - string.length();
			if (string_1_.length() > i)
				string_1_ = string_1_.substring(0, i - 3) + "...";
			psoutputstream.println(string_1_);
		}
	}

	public void writePDFMarks(PSOutputStream psoutputstream) throws IOException {
		Enumeration enumeration = this.keys();
		if (enumeration.hasMoreElements()) {
			psoutputstream.print("[");
			while (enumeration.hasMoreElements()) {
				String string = (String) enumeration.nextElement();
				String string_2_ = (String) this.get(string);
				psoutputstream.print("/" + string + " ");
				psoutputstream.print_annotation(string_2_, 255);
				psoutputstream.println();
			}
			psoutputstream.println("/DOCINFO pdfmark");
		}
	}
}