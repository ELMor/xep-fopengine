/*
 * PSOutputStream - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pslib;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.renderx.util.URLUtil;

public class PSOutputStream extends BufferedOutputStream {
	PSDocument doc;

	PSOutputStream(PSDocument psdocument, OutputStream outputstream)
			throws IOException {
		super(outputstream);
		doc = psdocument;
	}

	public void print(char c) throws IOException {
		this.write((byte) (c & '\u00ff'));
	}

	public void print(int i) throws IOException {
		this.write((byte) (i & 0xff));
	}

	public void print(String string) throws IOException {
		int i = string.length();
		for (int i_0_ = 0; i_0_ < i; i_0_++)
			print(string.charAt(i_0_));
	}

	public void println() throws IOException {
		print('\n');
	}

	public void println(String string) throws IOException {
		print(string);
		println();
	}

	public void writeHexByte(int i) throws IOException {
		print(hexByte(i));
	}

	public String hexByte(int i) {
		return ("" + Integer.toHexString(i >> 4 & 0xf) + Integer
				.toHexString(i & 0xf));
	}

	public void writeHexArray(byte[] is) throws IOException {
		if (is != null) {
			for (int i = 0; i < is.length; i++) {
				print(Integer.toHexString(is[i] >> 4 & 0xf));
				print(Integer.toHexString(is[i] & 0xf));
				if ((i + 1) % 32 == 0)
					print('\n');
			}
		}
	}

	public void print_annotation(String string, int i) throws IOException {
		print(Strings.quote_string(string, i, doc.UNICODE_ANNOTATIONS));
	}

	void createAction(String string, boolean bool) throws IOException {
		print("/Action ");
		string = string.trim();
		if (string.toLowerCase().startsWith("file:")) {
			String string_1_ = string.substring(5);
			if (string_1_.startsWith("//"))
				string_1_ = string_1_.substring(2);
			String string_2_ = null;
			int i = string_1_.lastIndexOf('#');
			if (i != -1) {
				string_2_ = string_1_.substring(i + 1).trim();
				string_1_ = string_1_.substring(0, i).trim();
			}
			if (string_1_.length() != 0) {
				println("<</Subtype /GoToR /NewWindow "
						+ (bool ? "true" : "false"));
				if (string_2_ == null || string_2_.length() == 0)
					println("/Dest [0 /Fit]");
				else {
					print("/Dest ");
					print_annotation(URLUtil.urlDecode(string_2_), 65535);
					println();
				}
				print("/File ");
				print_annotation(URLUtil.urlDecode(string_1_), 65535);
				println(">>");
				return;
			}
		}
		println("<</Subtype /URI /URI ");
		print_annotation(string, 65535);
		println(">>");
	}
}