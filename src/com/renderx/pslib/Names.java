/*
 * Names - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pslib;

import java.io.IOException;
import java.util.Enumeration;
import java.util.StringTokenizer;

import com.renderx.util.Hashtable;

public class Names {
	private Hashtable targets = new Hashtable();

	private Hashtable usedTargets = new Hashtable();

	private Hashtable mangled2normal = new Hashtable();

	private Hashtable normal2mangled = new Hashtable();

	static class Destination {
		int page = -1;

		float x = 0.0F;

		float y = 0.0F;

		Destination(int i, float f, float f_0_) {
			page = i;
			x = f;
			y = f_0_;
		}
	}

	void addDestination(String string, int i, float f, float f_1_) {
		if (!targets.containsKey(string))
			targets.put(string, new Destination(i, f, f_1_));
	}

	Destination getDestination(String string) {
		return (Destination) targets.get(string);
	}

	void markUsed(String string) {
		usedTargets.put(string, string);
	}

	void write(PSOutputStream psoutputstream, boolean bool) throws IOException {
		Enumeration enumeration = bool ? usedTargets.keys() : targets.keys();
		while (enumeration.hasMoreElements()) {
			String string = (String) enumeration.nextElement();
			Destination destination = (Destination) targets.get(string);
			if (destination != null) {
				psoutputstream.print("[/Dest /" + string);
				psoutputstream.print(" /Page " + destination.page
						+ " /View [/XYZ " + destination.x + " " + destination.y
						+ " 0]");
				psoutputstream.println(" /DEST pdfmark");
			}
		}
	}

	String makePSName(String string) {
		String string_2_ = (String) normal2mangled.get(string);
		if (string_2_ != null)
			return string_2_;
		if (new StringTokenizer(string,
				"$+-_.:0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
				.hasMoreTokens()) {
			string_2_ = "";
			for (int i = 0; i < string.length(); i++) {
				char c = string.charAt(i);
				if ("$+-_.:0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
						.indexOf(c) != -1)
					string_2_ += c;
			}
		} else
			string_2_ = string;
		if (string_2_.length() > 127)
			string_2_ = string_2_.substring(0, 127);
		if (string_2_.length() == 0 || mangled2normal.get(string_2_) != null) {
			String string_3_ = string_2_;
			if (string_3_.length() > 118)
				string_3_ = string_3_.substring(0, 118);
			int i = 0;
			for (;;) {
				String string_4_ = "x" + Integer.toHexString(i);
				string_2_ = string_3_ + string_4_;
				if (mangled2normal.get(string_2_) == null)
					break;
				i++;
			}
		}
		if (mangled2normal.get(string_2_) != null)
			throw new InternalException(
					"Cannot generate a unique PostScript name from string "
							+ string);
		normal2mangled.put(string, string_2_);
		mangled2normal.put(string_2_, string);
		return string_2_;
	}
}