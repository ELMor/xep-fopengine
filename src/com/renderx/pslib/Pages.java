/*
 * Pages - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pslib;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.Enumeration;
import java.util.StringTokenizer;

import com.renderx.util.Array;
import com.renderx.util.Hashtable;
import com.renderx.util.List;

class Pages {
	int currentNumber = 0;

	Page currentPage = null;

	PSDocument doc = null;

	public boolean USE_TEMP_FILE = true;

	private static int filenumber = 0;

	File tempFile = null;

	RandomAccessFile raf = null;

	private Array pageArray = new Array();

	static class PageBoundaries {
		Hashtable boundaries = new Hashtable();

		public PageBoundaries(Hashtable hashtable) {
			boundaries = (Hashtable) hashtable.clone();
		}

		public boolean isBleeds() {
			return boundaries.containsKey("BLEED");
		}

		public boolean isMediaBoxOffset() {
			return boundaries.containsKey("CROP_OFFSET");
		}

		public boolean isBleedMarkWidth() {
			return boundaries.containsKey("BLEED_MARK_WIDTH");
		}

		public boolean isCropMarkWidth() {
			return boundaries.containsKey("CROP_MARK_WIDTH");
		}

		public float[] getBleeds() {
			float[] fs = new float[4];
			for (int i = 0; i < fs.length; i++)
				fs[i] = 0.0F;
			if (isBleeds())
				fs = (float[]) boundaries.get("BLEED");
			return fs;
		}

		public float[] getMediaBoxOffset() {
			float[] fs = new float[4];
			for (int i = 0; i < fs.length; i++)
				fs[i] = 0.0F;
			if (isMediaBoxOffset())
				fs = (float[]) boundaries.get("CROP_OFFSET");
			float[] fs_0_ = getBleeds();
			for (int i = 0; i < fs.length; i++) {
				if (fs[i] < fs_0_[i])
					fs[i] = fs_0_[i];
			}
			return fs;
		}

		public float getBleedMarkWidth() {
			return (!isBleedMarkWidth() ? 0.0F : new Float((String) boundaries
					.get("BLEED_MARK_WIDTH")).floatValue());
		}

		public float getCropMarkWidth() {
			return (!isCropMarkWidth() ? 0.0F : new Float((String) boundaries
					.get("CROP_MARK_WIDTH")).floatValue());
		}

		public boolean isPageOffset() {
			return isMediaBoxOffset() || isBleeds();
		}
	}

	class Page {
		private ByteArrayOutputStream accumulator = null;

		public PSOutputStream contentStream = null;

		public String id = null;

		private long dataOffset = 0L;

		private long dataLength = 0L;

		Hashtable pagedevice = new Hashtable();

		Hashtable pageboundaries = new Hashtable();

		List printermarks = new List();

		class RAFOutputStream extends OutputStream {
			public void write(int i) throws IOException {
				raf.write((byte) i);
			}

			public void write(byte[] is) throws IOException {
				raf.write(is);
			}

			public void write(byte[] is, int i, int i_1_) throws IOException {
				raf.write(is, i, i_1_);
			}
		}

		Page(String string) throws IOException {
			id = string;
			if (USE_TEMP_FILE) {
				dataOffset = raf.getFilePointer();
				contentStream = new PSOutputStream(doc, new RAFOutputStream());
			} else {
				accumulator = new ByteArrayOutputStream();
				contentStream = new PSOutputStream(doc, accumulator);
			}
		}

		public void writePageSetup(PSOutputStream psoutputstream)
				throws IOException {
			psoutputstream.println("%%BeginPageSetup");
			Enumeration enumeration = pagedevice.keys();
			if (enumeration.hasMoreElements()) {
				psoutputstream
						.println("XEPPS_DocumentPageDevice dup length dict copy");
				while (enumeration.hasMoreElements()) {
					String string = (String) enumeration.nextElement();
					String string_2_ = (String) pagedevice.get(string);
					psoutputstream.println("dup /" + string + " " + string_2_
							+ " put");
				}
				psoutputstream.println("SafeSetPageDevice");
			} else
				psoutputstream
						.println("XEPPS_DocumentPageDevice SafeSetPageDevice");
			psoutputstream.println("%%EndPageSetup");
		}

		public void writePageTrailer(PSOutputStream psoutputstream)
				throws IOException {
			psoutputstream.println("showpage");
			psoutputstream.println("%%PageTrailer");
		}

		public void endPage() throws IOException {
			contentStream.flush();
			if (USE_TEMP_FILE)
				dataLength = raf.getFilePointer() - dataOffset;
		}

		void writePrinterMarks(PSOutputStream psoutputstream)
				throws IOException {
			PageBoundaries pageboundaries = new PageBoundaries(
					this.pageboundaries);
			PageBoundaries pageboundaries_3_ = new PageBoundaries(doc
					.getPageBoundaries());
			if (pageboundaries.isPageOffset()
					|| pageboundaries_3_.isPageOffset()) {
				float[] fs = pageboundaries_3_.getBleeds();
				if (pageboundaries.isBleeds())
					fs = pageboundaries.getBleeds();
				float[] fs_4_ = pageboundaries_3_.getMediaBoxOffset();
				if (pageboundaries.isMediaBoxOffset())
					fs_4_ = pageboundaries.getMediaBoxOffset();
				String string = doc.getPageDeviceEntry("PageSize");
				if (pagedevice.containsKey("PageSize"))
					string = (String) pagedevice.get("PageSize");
				StringTokenizer stringtokenizer = new StringTokenizer(string);
				String string_5_ = stringtokenizer.nextToken();
				string_5_ = string_5_.substring(1);
				String string_6_ = stringtokenizer.nextToken();
				string_6_ = string_6_.substring(0, string_6_.length() - 1);
				float f = new Float(string_5_).floatValue();
				float f_7_ = new Float(string_6_).floatValue();
				psoutputstream.println("gsave");
				psoutputstream.println("newpath");
				psoutputstream.println("0 0 moveto");
				psoutputstream.println("0 " + string_6_ + " lineto");
				psoutputstream.println(string_5_ + " " + string_6_ + " lineto");
				psoutputstream.println(string_5_ + " " + 0 + " lineto");
				psoutputstream.println("closepath");
				psoutputstream.println("clippath");
				psoutputstream.println("newpath");
				psoutputstream.println("" + (fs_4_[0] - fs[0]) + " "
						+ (fs_4_[1] - fs[1]) + " moveto");
				psoutputstream.println("" + (fs_4_[0] - fs[0]) + " "
						+ (f_7_ - fs_4_[3] + fs[3]) + " lineto");
				psoutputstream.println("" + (f - fs_4_[2] + fs[2]) + " "
						+ (f_7_ - fs_4_[3] + fs[3]) + " lineto");
				psoutputstream.println("" + (f - fs_4_[2] + fs[2]) + " "
						+ (fs_4_[1] - fs[1]) + " lineto");
				psoutputstream.println("closepath");
				psoutputstream.println("clip");
				psoutputstream.println("" + fs_4_[0] + " " + fs_4_[1]
						+ " translate");
			}
		}

		void write(PSOutputStream psoutputstream) throws IOException {
			contentStream.flush();
			writePageSetup(psoutputstream);
			writePrinterMarks(psoutputstream);
			if (USE_TEMP_FILE) {
				byte[] is = new byte[16384];
				raf.seek(dataOffset);
				int i;
				for (/**/; dataLength > 0L; dataLength -= (long) i) {
					i = dataLength < 16384L ? (int) dataLength : 16384;
					raf.readFully(is, 0, i);
					psoutputstream.write(is, 0, i);
				}
			} else {
				accumulator.writeTo(psoutputstream);
				accumulator.close();
			}
			writePageTrailer(psoutputstream);
		}
	}

	static synchronized File getTempFile(File file) {
		Object object = null;
		File file_8_;
		do
			file_8_ = new File(file, (Integer.toHexString((int) System
					.currentTimeMillis())
					+ "_" + Integer.toHexString(++filenumber) + ".tmp"));
		while (file_8_.exists());
		return file_8_;
	}

	Pages(PSDocument psdocument, File file) {
		doc = psdocument;
		USE_TEMP_FILE = file != null;
		if (USE_TEMP_FILE) {
			try {
				tempFile = getTempFile(file);
				raf = new RandomAccessFile(tempFile, "rw");
			} catch (IOException ioexception) {
				psdocument.errorHandler.exception(
						"Cannot create temporary file " + tempFile.toString(),
						ioexception);
				USE_TEMP_FILE = false;
			}
		}
	}

	int length() {
		return pageArray.length();
	}

	void insertBlankPage(int i) {
		pageArray.put(i - 1, null);
	}

	void write(PSOutputStream psoutputstream) throws IOException {
		int i = 0;
		while (i < pageArray.length()) {
			Page page = (Page) pageArray.get(i++);
			if (page != null) {
				psoutputstream.println("%%Page: " + page.id + " " + i);
				page.write(psoutputstream);
			} else {
				psoutputstream.println("%%Page: " + i + " " + i);
				psoutputstream.println("%%BeginPageSetup");
				psoutputstream.println("%%EndPageSetup");
				psoutputstream.println("showpage");
				psoutputstream.println("%%PageTrailer");
			}
		}
		if (USE_TEMP_FILE) {
			raf.close();
			tempFile.delete();
		}
	}

	void beginPage(int i, String string) throws IOException {
		currentPage = new Page(string);
		pageArray.put(i - 1, currentPage);
		currentNumber = i;
	}

	void endPage() throws IOException {
		currentPage.endPage();
		currentPage = null;
	}
}