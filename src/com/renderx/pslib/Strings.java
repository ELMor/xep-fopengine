/*
 * Strings - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pslib;

import java.util.StringTokenizer;

public class Strings {
	private static final String pdfEncodingCharset = "\u02c6\u02d9\u02dd\u02db\u02da\u02dc\u2022\u2020\u2021\u2026\u2014\u2013\u0192\u2044\u2039\u203a\u2212\u2030\u201e\u201c\u201d\u2018\u2019\u201a\u2122\ufb01\ufb02\u0141\u0152\u0160\u0178\u017d\u0131\u0142\u0153\u0161\u017e\u20ac\t\r\n\u00a0\u00a1\u00a2\u00a3\u00a4\u00a5\u00a6\u00a7\u00a8\u00a9\u00aa\u00ab\u00ac\u00ad\u00ae\u00af\u00b0\u00b1\u00b2\u00b3\u00b4\u00b5\u00b6\u00b7\u00b8\u00b9\u00ba\u00bb\u00bc\u00bd\u00be\u00bf\u00c0\u00c1\u00c2\u00c3\u00c4\u00c5\u00c6\u00c7\u00c8\u00c9\u00ca\u00cb\u00cc\u00cd\u00ce\u00cf\u00d0\u00d1\u00d2\u00d3\u00d4\u00d5\u00d6\u00d7\u00d8\u00d9\u00da\u00db\u00dc\u00dd\u00de\u00df\u00e0\u00e1\u00e2\u00e3\u00e4\u00e5\u00e6\u00e7\u00e8\u00e9\u00ea\u00eb\u00ec\u00ed\u00ee\u00ef\u00f0\u00f1\u00f2\u00f3\u00f4\u00f5\u00f6\u00f7\u00f8\u00f9\u00fa\u00fb\u00fc\u00fd\u00fe\u00ff !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";

	public static String quote_string(String string, int i, boolean bool) {
		if (bool) {
			StringTokenizer stringtokenizer = (new StringTokenizer(
					string,
					"\u02c6\u02d9\u02dd\u02db\u02da\u02dc\u2022\u2020\u2021\u2026\u2014\u2013\u0192\u2044\u2039\u203a\u2212\u2030\u201e\u201c\u201d\u2018\u2019\u201a\u2122\ufb01\ufb02\u0141\u0152\u0160\u0178\u017d\u0131\u0142\u0153\u0161\u017e\u20ac\t\r\n\u00a0\u00a1\u00a2\u00a3\u00a4\u00a5\u00a6\u00a7\u00a8\u00a9\u00aa\u00ab\u00ac\u00ad\u00ae\u00af\u00b0\u00b1\u00b2\u00b3\u00b4\u00b5\u00b6\u00b7\u00b8\u00b9\u00ba\u00bb\u00bc\u00bd\u00be\u00bf\u00c0\u00c1\u00c2\u00c3\u00c4\u00c5\u00c6\u00c7\u00c8\u00c9\u00ca\u00cb\u00cc\u00cd\u00ce\u00cf\u00d0\u00d1\u00d2\u00d3\u00d4\u00d5\u00d6\u00d7\u00d8\u00d9\u00da\u00db\u00dc\u00dd\u00de\u00df\u00e0\u00e1\u00e2\u00e3\u00e4\u00e5\u00e6\u00e7\u00e8\u00e9\u00ea\u00eb\u00ec\u00ed\u00ee\u00ef\u00f0\u00f1\u00f2\u00f3\u00f4\u00f5\u00f6\u00f7\u00f8\u00f9\u00fa\u00fb\u00fc\u00fd\u00fe\u00ff !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"));
			bool = stringtokenizer.hasMoreTokens();
		}
		if (bool)
			i /= 2;
		if (string.length() > i) {
			string = string.substring(0, i - 3);
			string += "...";
		}
		return (bool ? unicode_quote_string(string)
				: pdfencoding_quote_string(string));
	}

	public static String pdfencoding_quote_string(String string) {
		String string_0_ = "(";
		for (int i = 0; i < string.length(); i++) {
			if (i % 128 == 127)
				string_0_ += "\\\n";
			char c = string.charAt(i);
			switch (c) {
			case '\\':
				string_0_ += "\\\\";
				break;
			case '(':
				string_0_ += "\\(";
				break;
			case ')':
				string_0_ += "\\)";
				break;
			case '\u02c6':
				string_0_ += '\032';
				break;
			case '\u02d9':
				string_0_ += '\033';
				break;
			case '\u02dd':
				string_0_ += '\034';
				break;
			case '\u02db':
				string_0_ += '\035';
				break;
			case '\u02da':
				string_0_ += '\036';
				break;
			case '\u02dc':
				string_0_ += '\037';
				break;
			case '\u2022':
				string_0_ += '\u0080';
				break;
			case '\u2020':
				string_0_ += '\u0081';
				break;
			case '\u2021':
				string_0_ += '\u0082';
				break;
			case '\u2026':
				string_0_ += '\u0083';
				break;
			case '\u2014':
				string_0_ += '\u0084';
				break;
			case '\u2013':
				string_0_ += '\u0085';
				break;
			case '\u0192':
				string_0_ += '\u0086';
				break;
			case '\u2044':
				string_0_ += '\u0087';
				break;
			case '\u2039':
				string_0_ += '\u0088';
				break;
			case '\u203a':
				string_0_ += '\u0089';
				break;
			case '\u2212':
				string_0_ += '\u008a';
				break;
			case '\u2030':
				string_0_ += '\u008b';
				break;
			case '\u201e':
				string_0_ += '\u008c';
				break;
			case '\u201c':
				string_0_ += '\u008d';
				break;
			case '\u201d':
				string_0_ += '\u008e';
				break;
			case '\u2018':
				string_0_ += '\u008f';
				break;
			case '\u2019':
				string_0_ += '\u0090';
				break;
			case '\u201a':
				string_0_ += '\u0091';
				break;
			case '\u2122':
				string_0_ += '\u0092';
				break;
			case '\ufb01':
				string_0_ += '\u0093';
				break;
			case '\ufb02':
				string_0_ += '\u0094';
				break;
			case '\u0141':
				string_0_ += '\u0095';
				break;
			case '\u0152':
				string_0_ += '\u0096';
				break;
			case '\u0160':
				string_0_ += '\u0097';
				break;
			case '\u0178':
				string_0_ += '\u0098';
				break;
			case '\u017d':
				string_0_ += '\u0099';
				break;
			case '\u0131':
				string_0_ += '\u009a';
				break;
			case '\u0142':
				string_0_ += '\u009b';
				break;
			case '\u0153':
				string_0_ += '\u009c';
				break;
			case '\u0161':
				string_0_ += '\u009d';
				break;
			case '\u017e':
				string_0_ += '\u009e';
				break;
			case '\u0100':
			case '\u0102':
			case '\u0104':
				string_0_ += 'A';
				break;
			case '\u0101':
			case '\u0103':
			case '\u0105':
				string_0_ += 'a';
				break;
			case '\u0106':
			case '\u0108':
			case '\u010a':
			case '\u010c':
				string_0_ += 'C';
				break;
			case '\u0107':
			case '\u0109':
			case '\u010b':
			case '\u010d':
				string_0_ += 'c';
				break;
			case '\u010e':
				string_0_ += 'D';
				break;
			case '\u0110':
				string_0_ += '\u00d0';
				break;
			case '\u010f':
			case '\u0111':
				string_0_ += 'd';
				break;
			case '\u0112':
			case '\u0114':
			case '\u0116':
			case '\u0118':
			case '\u011a':
				string_0_ += 'E';
				break;
			case '\u0113':
			case '\u0115':
			case '\u0117':
			case '\u0119':
			case '\u011b':
				string_0_ += 'e';
				break;
			case '\u011c':
			case '\u011e':
			case '\u0120':
			case '\u0122':
				string_0_ += 'G';
				break;
			case '\u011d':
			case '\u011f':
			case '\u0121':
			case '\u0123':
				string_0_ += 'g';
				break;
			case '\u0124':
			case '\u0126':
				string_0_ += 'H';
				break;
			case '\u0125':
			case '\u0127':
				string_0_ += 'h';
				break;
			case '\u0128':
			case '\u012a':
			case '\u012c':
			case '\u012e':
			case '\u0130':
				string_0_ += 'I';
				break;
			case '\u0129':
			case '\u012b':
			case '\u012d':
			case '\u012f':
				string_0_ += 'i';
				break;
			case '\u0132':
				string_0_ += "IJ";
				break;
			case '\u0134':
				string_0_ += 'J';
				break;
			case '\u0133':
				string_0_ += "ij";
				break;
			case '\u0135':
				string_0_ += 'j';
				break;
			case '\u0136':
				string_0_ += 'K';
				break;
			case '\u0137':
			case '\u0138':
				string_0_ += 'k';
				break;
			case '\u0139':
			case '\u013b':
			case '\u013d':
			case '\u013f':
				string_0_ += 'L';
				break;
			case '\u013a':
			case '\u013c':
			case '\u013e':
			case '\u0140':
				string_0_ += 'l';
				break;
			case '\u0143':
			case '\u0145':
			case '\u0147':
			case '\u014a':
				string_0_ += 'N';
				break;
			case '\u0144':
			case '\u0146':
			case '\u0148':
			case '\u014b':
				string_0_ += 'n';
				break;
			case '\u0149':
				string_0_ += "'n";
				break;
			case '\u014c':
			case '\u014e':
			case '\u0150':
				string_0_ += 'O';
				break;
			case '\u014d':
			case '\u014f':
			case '\u0151':
				string_0_ += 'o';
				break;
			case '\u0154':
			case '\u0156':
			case '\u0158':
				string_0_ += 'R';
				break;
			case '\u0155':
			case '\u0157':
			case '\u0159':
				string_0_ += 'r';
				break;
			case '\u015a':
			case '\u015c':
			case '\u015e':
				string_0_ += 'S';
				break;
			case '\u015b':
			case '\u015d':
			case '\u015f':
			case '\u017f':
				string_0_ += 's';
				break;
			case '\u0162':
			case '\u0164':
			case '\u0166':
				string_0_ += 'T';
				break;
			case '\u0163':
			case '\u0165':
			case '\u0167':
				string_0_ += 't';
				break;
			case '\u0168':
			case '\u016a':
			case '\u016c':
			case '\u016e':
			case '\u0170':
			case '\u0172':
				string_0_ += 'U';
				break;
			case '\u0169':
			case '\u016b':
			case '\u016d':
			case '\u016f':
			case '\u0171':
			case '\u0173':
				string_0_ += 'u';
				break;
			case '\u0174':
				string_0_ += 'W';
				break;
			case '\u0175':
				string_0_ += 'w';
				break;
			case '\u0176':
				string_0_ += 'Y';
				break;
			case '\u0177':
				string_0_ += 'y';
				break;
			case '\u0179':
			case '\u017b':
				string_0_ += 'Z';
				break;
			case '\u017a':
			case '\u017c':
				string_0_ += 'z';
				break;
			case '\u00a0':
				string_0_ += ' ';
				break;
			case '\t':
				string_0_ += "\\t";
				break;
			case '\n':
			case '\r':
				string_0_ += "\\n";
				break;
			default:
				if (' ' <= c && c < '\u0080' || '\u00a0' < c && c <= '\u00ff')
					string_0_ += c;
				else
					string_0_ += '\u0080';
			}
		}
		return string_0_ + ")";
	}

	public static String unicode_quote_string(String string) {
		String string_1_ = "<FEFF";
		for (int i = 0; i < string.length(); i++) {
			if (i % 32 == 31)
				string_1_ += '\n';
			int i_2_ = string.charAt(i);
			int i_3_ = i_2_ >> 8 & 0xff;
			int i_4_ = i_2_ & 0xff;
			if (i_3_ < 16)
				string_1_ += "0";
			string_1_ += Integer.toHexString(i_3_);
			if (i_4_ < 16)
				string_1_ += "0";
			string_1_ += Integer.toHexString(i_4_);
		}
		return string_1_ + ">";
	}

	public static String normalize_space(String string) {
		if (string == null)
			return null;
		StringTokenizer stringtokenizer = new StringTokenizer(string);
		String string_5_ = "";
		while (stringtokenizer.hasMoreTokens())
			string_5_ += " " + stringtokenizer.nextToken();
		return string_5_.trim();
	}
}