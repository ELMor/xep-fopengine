/*
 * Bookmarks - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pslib;

import java.io.IOException;

import com.renderx.util.Array;
import com.renderx.util.Hashtable;

public class Bookmarks {
	Array toplevel = new Array();

	Hashtable byId = new Hashtable();

	class Bookmark {
		String text;

		String name = null;

		String url = null;

		boolean open = false;

		boolean newWindow = false;

		Array children = new Array();

		Bookmark(String string, String string_0_, String string_1_,
				boolean bool, boolean bool_2_) {
			text = string;
			name = string_0_;
			url = string_1_;
			open = bool;
			newWindow = bool_2_;
		}

		void write(PSOutputStream psoutputstream) throws IOException {
			psoutputstream.print("[");
			if (children.length() > 0)
				psoutputstream.println("/Count " + (open ? "" : "-")
						+ children.length());
			if (url != null)
				psoutputstream.createAction(url, newWindow);
			else
				psoutputstream.println("/Dest /" + name);
			psoutputstream.println("/Border [0 0 0 [0]]");
			psoutputstream.print("/Title ");
			psoutputstream.print_annotation(text, 255);
			psoutputstream.println();
			psoutputstream.println("/OUT pdfmark");
			for (int i = 0; i < children.length(); i++)
				((Bookmark) children.get(i)).write(psoutputstream);
		}
	}

	public boolean isEmpty() {
		return toplevel.length() == 0;
	}

	void addBookmark(String string, int i, int i_3_, String string_4_,
			String string_5_, boolean bool, boolean bool_6_) {
		if (byId.get(new Integer(i)) == null) {
			Bookmark bookmark = new Bookmark(string, string_4_, string_5_,
					bool, bool_6_);
			Bookmark bookmark_7_ = null;
			if (i_3_ != 0)
				bookmark_7_ = (Bookmark) byId.get(new Integer(i_3_));
			if (bookmark_7_ == null)
				toplevel.put(toplevel.length(), bookmark);
			else
				bookmark_7_.children.put(bookmark_7_.children.length(),
						bookmark);
			byId.put(new Integer(i), bookmark);
		}
	}

	void write(PSOutputStream psoutputstream) throws IOException {
		if (toplevel.length() != 0) {
			for (int i = 0; i < toplevel.length(); i++)
				((Bookmark) toplevel.get(i)).write(psoutputstream);
		}
	}
}