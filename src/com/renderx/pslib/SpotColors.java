/*
 * SpotColors - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pslib;

import java.io.IOException;
import java.util.Enumeration;

import com.renderx.util.Hashtable;

public class SpotColors {
	int colorantsCount = -1;

	Hashtable colorants = new Hashtable();

	public class SpotColor {
		String colorant;

		float[] altcolor;

		SpotColor(String string, float[] fs) {
			colorant = string;
			altcolor = fs;
			if (fs == null)
				altcolor = new float[] { 0.0F };
		}

		public int hashCode() {
			return colorant.hashCode();
		}

		public boolean equals(Object object) {
			if (!(object instanceof SpotColor))
				return false;
			SpotColor spotcolor_0_ = (SpotColor) object;
			if (!spotcolor_0_.colorant.equals(colorant))
				return false;
			if (spotcolor_0_.altcolor.length != altcolor.length)
				return false;
			for (int i = 0; i < altcolor.length; i++) {
				if (spotcolor_0_.altcolor[i] != altcolor[i])
					return false;
			}
			return true;
		}
	}

	SpotColors() {
		/* empty */
	}

	String getPSSpotColorName(String string, float[] fs) {
		SpotColor spotcolor = new SpotColor(string, fs);
		return (String) colorants.get(spotcolor);
	}

	void addSpotColor(String string, float[] fs) {
		colorantsCount++;
		SpotColor spotcolor = new SpotColor(string, fs);
		colorants.put(spotcolor, "CS" + colorantsCount);
	}

	void write(PSOutputStream psoutputstream) throws IOException {
		Enumeration enumeration = colorants.keys();
		while (enumeration.hasMoreElements()) {
			SpotColor spotcolor = (SpotColor) enumeration.nextElement();
			String string = (String) colorants.get(spotcolor);
			psoutputstream.print("/" + string + " ");
			psoutputstream.print("[ /Separation (" + spotcolor.colorant + ") ");
			writeAltColorTransformProcedure(psoutputstream, spotcolor.altcolor);
			psoutputstream.print(" ] ");
			psoutputstream.print("/ColorSpace ");
			psoutputstream.println("defineresource");
		}
	}

	private void writeAltColorTransformProcedure(PSOutputStream psoutputstream,
			float[] fs) throws IOException {
		float[] fs_1_ = fs;
		if (fs == null)
			fs_1_ = new float[] { 0.0F };
		String string = "";
		switch (fs_1_.length) {
		case 1:
			string = "DeviceGray";
			break;
		case 3:
			string = "DeviceRGB";
			break;
		case 4:
			string = "DeviceCMYK";
			break;
		default:
			string = "DeviceGray";
		}
		psoutputstream.print("/" + string);
		int i = fs_1_.length;
		psoutputstream.print(" { ");
		if (i == 1 || i == 3) {
			for (int i_2_ = 0; i_2_ < i; i_2_++) {
				if (i_2_ < i - 1)
					psoutputstream.print("dup dup " + fs_1_[i_2_]
							+ " mul 1 add exch sub exch ");
				else
					psoutputstream.print("dup " + fs_1_[i_2_]
							+ " mul 1 add exch sub");
			}
		} else {
			for (int i_3_ = 0; i_3_ < i; i_3_++) {
				if (i_3_ < i - 1)
					psoutputstream.print("dup " + fs_1_[i_3_] + " mul exch ");
				else
					psoutputstream.print("" + fs_1_[i_3_] + " mul");
			}
		}
		psoutputstream.print(" }");
	}
}