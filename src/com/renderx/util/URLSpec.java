/*
 * URLSpec - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class URLSpec {
	public static final int INVALID_SCHEME = 0;

	public static final int SCHEME_DATA = 1;

	public static final int SCHEME_RESOURCE = 2;

	public static final int SCHEME_OTHER = 3;

	public final int scheme;

	public DataURL data = null;

	public String resource = null;

	public URL url = null;

	public URLSpec(String string) throws MalformedURLException {
		this(null, string);
	}

	public URLSpec(URLSpec urlspec_0_, String string)
			throws MalformedURLException {
		if (urlspec_0_ != null)
			urlspec_0_ = urlspec_0_.getBase();
		int i = string.indexOf(':');
		int i_1_ = string.indexOf('/');
		if (i != -1 && (i_1_ == -1 || i_1_ > i)) {
			String string_2_ = string.substring(0, i);
			if (string_2_.equalsIgnoreCase("data")) {
				scheme = 1;
				data = new DataURL(urlspec_0_, string);
			} else if (string_2_.equalsIgnoreCase("resource")) {
				scheme = 2;
				resource = string.substring("resource:".length());
			} else {
				scheme = 3;
				url = getOtherURL(null, string);
			}
		} else if (urlspec_0_ == null || urlspec_0_.scheme == 1) {
			scheme = 3;
			url = getOtherURL(null, string);
		} else if (urlspec_0_.scheme == 2) {
			scheme = 2;
			resource = appendResource(urlspec_0_.resource, string);
		} else {
			scheme = 3;
			url = getOtherURL(urlspec_0_.url, string);
		}
	}

	private static URL getOtherURL(URL url, String string)
			throws MalformedURLException {
		try {
			return new URL(url, string);
		} catch (MalformedURLException malformedurlexception) {
			File file = new File(string);
			if (file.exists())
				return URLUtil.toURL(file);
			throw new MalformedURLException(
					"Invalid URL or non-existent file: " + string);
		}
	}

	private static String appendResource(String string, String string_3_) {
		if (string_3_.startsWith("/"))
			return string_3_;
		int i = string.lastIndexOf('/');
		if (i == -1)
			return string_3_;
		return string.substring(0, i + 1) + string_3_;
	}

	public String toString() {
		switch (scheme) {
		case 1:
			return data.toString();
		case 2:
			return "resource:" + resource;
		case 3:
			if (url.getProtocol().equalsIgnoreCase("file")) {
				String string = url.getHost();
				String string_4_ = url.getFile();
				if (string_4_.startsWith("//")
						&& (string == null || string.length() == 0)) {
					String string_5_ = url.toExternalForm().substring(5);
					if (!string_5_.startsWith("////"))
						return "file://" + string_5_;
				}
			}
			return url.toExternalForm();
		default:
			throw new RuntimeException(
					"Invalid structure in URLSpec: incorrect value for scheme switch ("
							+ scheme + ")");
		}
	}

	public InputStream openStream() throws IOException {
		switch (scheme) {
		case 1:
			return data.openStream();
		case 3:
			return url.openStream();
		case 2: {
			String string = resource.trim();
			if (string.startsWith("/"))
				string = string.substring(1);
			ClassLoader classloader = URLUtil.class.getClassLoader();
			if (classloader != null)
				return classloader.getResourceAsStream(string);
			return ClassLoader.getSystemResourceAsStream(string);
		}
		default:
			throw new RuntimeException(
					"Invalid structure in URLSpec: incorrect value for scheme switch ("
							+ scheme + ")");
		}
	}

	public String getProtocol() {
		switch (scheme) {
		case 2:
			return "resource";
		case 1:
			return "data";
		case 3:
			return url.getProtocol();
		default:
			throw new RuntimeException(
					"Invalid structure in URLSpec: incorrect value for scheme switch ("
							+ scheme + ")");
		}
	}

	public String getContentType() throws IOException {
		switch (scheme) {
		case 2:
			return null;
		case 1:
			return data.getContentType();
		case 3:
			if ("file".equalsIgnoreCase(url.getProtocol()))
				return null;
			return url.openConnection().getContentType();
		default:
			throw new RuntimeException(
					"Invalid structure in URLSpec: incorrect value for scheme switch ("
							+ scheme + ")");
		}
	}

	public URLSpec getBase() {
		if (scheme != 1)
			return this;
		return data.baseURL == null ? null : data.baseURL.getBase();
	}
}