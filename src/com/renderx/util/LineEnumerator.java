/*
 * LineEnumerator - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.io.IOException;
import java.io.InputStream;

public class LineEnumerator {
	private static final int BUFSIZE = 8192;

	private final InputStream input;

	private final byte[] buffer = new byte[8192];

	private int offset = 0;

	private int length = 0;

	private int nextchar = 0;

	public LineEnumerator(InputStream inputstream) throws IOException {
		input = inputstream;
		nextchar = read();
	}

	public boolean hasMoreLines() throws IOException {
		return nextchar != -1;
	}

	public String nextLine() throws IOException {
		StringBuffer stringbuffer = new StringBuffer();
		for (/**/; nextchar != -1 && nextchar != 10 && nextchar != 13; nextchar = read())
			stringbuffer.append((char) nextchar);
		nextchar = read();
		return stringbuffer.toString();
	}

	private int read() throws IOException {
		if (offset >= length) {
			offset = 0;
			length = input.read(buffer);
			if (length <= 0)
				return -1;
		}
		return buffer[offset++];
	}
}