/*
 * UnicodeTable - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

public class UnicodeTable {
	public Array ranges = new Array(256);

	public void put(char c, Object object) {
		int i = c >> 8 & 0xff;
		int i_0_ = c & 0xff;
		Array array = (Array) ranges.get(i);
		if (array == null)
			ranges.put(i, array = new Array(256));
		array.put(i_0_, object);
	}

	public Object get(char c) {
		Array array = (Array) ranges.get(c >> 8 & 0xff);
		return array == null ? null : array.get(c & 0xff);
	}
}