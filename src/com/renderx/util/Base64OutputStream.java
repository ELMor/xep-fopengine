/*
 * Base64OutputStream - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

public class Base64OutputStream extends OutputStream {
	public static final String base64charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	private int bits = 0;

	private int bitCount = 0;

	private int charCount = 0;

	private boolean endOfFile = false;

	private Writer writer = null;

	public Base64OutputStream(Writer writer) {
		this.writer = writer;
	}

	public void flush() throws IOException {
		writer.flush();
	}

	public void close() throws IOException {
		EOD();
		flush();
	}

	public void write(int i) throws IOException {
		if (endOfFile)
			throw new IOException(
					"Attempt to write to a Base64OutputStream after EOD was sent");
		bitCount += 8;
		bits |= (i & 0xff) << 32 - bitCount;
		while (bitCount >= 6) {
			if (++charCount > 64) {
				charCount = 1;
				writer.write('\n');
			}
			int i_0_ = bits >> 26 & 0x3f;
			bits <<= 6;
			bitCount -= 6;
			writer
					.write("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
							.charAt(i_0_));
		}
	}

	public void EOD() throws IOException {
		if (!endOfFile) {
			endOfFile = true;
			if (bitCount != 0) {
				writer
						.write("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
								.charAt(bits >> 26 & 0x3f));
				switch (bitCount) {
				case 2:
					writer.write("==");
					break;
				case 4:
					writer.write("=");
					break;
				default:
					throw new RuntimeException(
							"Internal error: unexpected bit count [" + bitCount
									+ "] in Base64OutputStream");
				}
			}
		}
	}
}