/*
 * User - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.util.Enumeration;
import java.util.Properties;

public class User {
	private static Properties up = new Properties();

	public static Properties getProperties() {
		Properties properties = new Properties();
		try {
			Properties properties_0_ = System.getProperties();
			Enumeration enumeration = properties_0_.propertyNames();
			while (enumeration.hasMoreElements()) {
				String string = (String) enumeration.nextElement();
				String string_1_ = properties_0_.getProperty(string);
				if (string_1_ != null)
					properties.put(string, string_1_);
			}
		} catch (SecurityException securityexception) {
			/* empty */
		}
		Enumeration enumeration = up.propertyNames();
		while (enumeration.hasMoreElements()) {
			String string = (String) enumeration.nextElement();
			properties.put(string, up.getProperty(string));
		}
		return properties;
	}

	public static void setProperties(Properties properties) {
		up = (Properties) properties.clone();
		try {
			System.setProperties(up);
		} catch (SecurityException securityexception) {
			/* empty */
		}
	}

	public static String getProperty(String string) {
		String string_2_ = up.getProperty(string);
		if (string_2_ == null) {
			try {
				string_2_ = System.getProperty(string);
			} catch (SecurityException securityexception) {
				/* empty */
			}
		}
		return string_2_;
	}
}