/*
 * Magic - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.io.IOException;
import java.util.Enumeration;
import java.util.StringTokenizer;

import com.renderx.sax.InputSource;
import com.renderx.sax.XMLReaderFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class Magic {
	private static List magdir = new List();

	private static Hashtable extab = new Hashtable();

	private static boolean initialized = false;

	private static class Config extends DefaultHandler {
		private static final String uri = "http://www.renderx.com/util/magic";

		private Stack clstack = new Stack();

		private List checklist = null;

		private Config() {
			/* empty */
		}

		public void startElement(String string, String string_0_,
				String string_1_, Attributes attributes) throws SAXException {
			if ("".equals(string)
					|| "http://www.renderx.com/util/magic".equals(string)) {
				if (string_0_.equals("entry")) {
					Entry entry = new Entry(attributes.getValue("mime-type"));
					Magic.magdir.snoc(entry);
					checklist = entry.checklist;
					String string_2_ = attributes.getValue("extensions");
					if (string_2_ != null) {
						StringTokenizer stringtokenizer = new StringTokenizer(
								string_2_, ",");
						while (stringtokenizer.hasMoreTokens())
							Magic.extab.put(stringtokenizer.nextToken().trim()
									.toLowerCase(), entry);
					}
				} else if (string_0_.equals("match")) {
					Check check = new Check();
					check.offset = Long
							.parseLong(attributes.getValue("offset"));
					String string_3_ = attributes.getValue("bytes");
					if (string_3_ != null) {
						StringTokenizer stringtokenizer = new StringTokenizer(
								string_3_, ",");
						check.bytes = new byte[stringtokenizer.countTokens()];
						for (int i = 0; i != check.bytes.length; i++)
							check.bytes[i] = Short.decode(
									stringtokenizer.nextToken()).byteValue();
					} else {
						String string_4_ = attributes.getValue("string");
						if (string_4_ != null)
							check.bytes = string_4_.getBytes();
						else
							throw new SAXException(
									"either string or bytes must be specified for each match");
					}
					checklist.snoc(check);
					clstack.push(checklist);
					checklist = check.checklist;
				} else if (!string_0_.equals("magic"))
					throw new SAXException("unexpected element: '" + string_0_
							+ "'");
			}
		}

		public void endElement(String string, String string_5_, String string_6_) {
			if ("".equals(string)
					|| "http://www.renderx.com/util/magic".equals(string)) {
				if (string_5_.equals("format"))
					checklist = null;
				else if (string_5_.equals("match"))
					checklist = (List) clstack.pop();
			}
		}
	}

	private static class Check {
		long offset;

		byte[] bytes;

		List checklist = new List();

		private Check() {
			/* empty */
		}
	}

	private static class Entry {
		String mimetype;

		List checklist = new List();

		Entry(String string) {
			mimetype = string;
		}
	}

	public static class UnknownMIMETypeException extends Exception {
		public String path;

		public UnknownMIMETypeException() {
			/* empty */
		}

		public UnknownMIMETypeException(String string) {
			super("cannot determine mime type for file '" + string + "'");
			path = string;
		}
	}

	public static synchronized void init() {
		if (!initialized) {
			String string = User.getProperty("com.renderx.util.magic");
			if (string == null)
				string = "resource:com/renderx/util/magic.xml";
			try {
				XMLReader xmlreader = XMLReaderFactory.createXMLReader();
				xmlreader.setContentHandler(new Config());
				xmlreader.parse(new InputSource(string));
			} catch (RuntimeException runtimeexception) {
				throw runtimeexception;
			} catch (Exception exception) {
				throw new RuntimeException(
						"error initializing com.renderx.util.Magic:"
								+ exception.toString());
			}
			initialized = true;
		}
	}

	private static boolean matches(SeekableInput seekableinput, List list) {
		if (list.isEmpty())
			return true;
		Enumeration enumeration = list.elements();
		while_9_: while (enumeration.hasMoreElements()) {
			Check check = (Check) enumeration.nextElement();
			byte[] is = new byte[check.bytes.length];
			try {
				seekableinput.seek(check.offset);
				seekableinput.readFully(is);
			} catch (IOException ioexception) {
				continue;
			}
			for (int i = 0; i != is.length; i++) {
				if (is[i] != check.bytes[i])
					continue while_9_;
			}
			return matches(seekableinput, check.checklist);
		}
		return false;
	}

	public static String MIME(String string, SeekableInput seekableinput)
			throws IOException, UnknownMIMETypeException {
		int i = string.lastIndexOf('.');
		String string_7_ = (i != -1 ? string.substring(++i, string.length())
				.toLowerCase() : "");
		Entry entry = (Entry) extab.get(string_7_);
		String string_8_ = null;
		if (entry != null) {
			if (matches(seekableinput, entry.checklist))
				return entry.mimetype;
			string_8_ = entry.mimetype;
		}
		Enumeration enumeration = magdir.elements();
		while (enumeration.hasMoreElements()) {
			entry = (Entry) enumeration.nextElement();
			if (matches(seekableinput, entry.checklist))
				return entry.mimetype;
		}
		if (string_8_ != null)
			return string_8_;
		throw new UnknownMIMETypeException(string);
	}

	public static void main(String[] strings) {
		init();
		for (int i = 0; i != strings.length; i++) {
			String string = strings[i];
			try {
				System.out.println(string + ": "
						+ MIME(string, new SeekableFileInputStream(string)));
			} catch (Exception exception) {
				System.err.println("error processing " + string + ": "
						+ exception);
			}
		}
	}
}