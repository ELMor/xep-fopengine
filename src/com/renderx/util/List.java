/*
 * List - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.util.Enumeration;
import java.util.NoSuchElementException;

public class List implements Cloneable {
	private Link head;

	private Link tail;

	private int length;

	private class Enumerator implements Enumeration {
		private Link cur = head.next;

		public final Object nextElement() {
			if (cur == tail.next.next)
				throw new NoSuchElementException(
						"attempt to access element past the end of a list");
			Object object = cur.data;
			cur = cur.next;
			return object;
		}

		public final boolean hasMoreElements() {
			return cur != tail.next.next;
		}
	}

	public final class Mark {
		Link link = tail.next;

		int length = List.this.length;

		Mark() {
			/* empty */
		}
	}

	static final class Link {
		Object data;

		Link next;
	}

	public List() {
		clear();
	}

	public final List clear() {
		head = new Link();
		head.next = null;
		tail = new Link();
		tail.next = head;
		length = 0;
		return this;
	}

	protected List newList() {
		try {
			return ((List) super.clone()).clear();
		} catch (CloneNotSupportedException clonenotsupportedexception) {
			throw new RuntimeException(clonenotsupportedexception.getMessage());
		}
	}

	public Object clone() {
		List list_0_ = newList();
		if (isPair()) {
			Link link = head.next;
			for (;;) {
				list_0_.snoc(link.data);
				if (link == tail.next)
					break;
				link = link.next;
			}
		}
		return list_0_;
	}

	public final List enolc() {
		List list_1_ = newList();
		if (isPair()) {
			Link link = head.next;
			for (;;) {
				list_1_.cons(link.data);
				if (link == tail.next)
					break;
				link = link.next;
			}
		}
		return list_1_;
	}

	public final List reverse() {
		return length > 1 ? enolc() : this;
	}

	public final List unshift(Object object) {
		return cons(object);
	}

	public final List cons(Object object) {
		Link link = new Link();
		head.data = object;
		link.next = head;
		head = link;
		length++;
		return this;
	}

	public final List prepend(List list_2_) {
		if (list_2_.isPair()) {
			list_2_.tail.next.next = head.next;
			head.next = list_2_.head.next;
			length += list_2_.length;
		}
		return this;
	}

	public final List append(Object object) {
		return snoc(object);
	}

	public final List snoc(Object object) {
		Link link = new Link();
		link.data = object;
		link.next = tail.next.next;
		tail.next.next = link;
		tail.next = link;
		length++;
		return this;
	}

	public final List append(List list_3_) {
		if (list_3_.isPair()) {
			tail.next.next = list_3_.head.next;
			tail.next = list_3_.tail.next;
			length += list_3_.length;
		}
		return this;
	}

	public final Object shift() {
		if (!isPair())
			throw new NoSuchElementException("list is not a pair");
		head = head.next;
		length--;
		return head.data;
	}

	public final boolean isEmpty() {
		return !isPair();
	}

	public final boolean isPair() {
		return head.next != tail.next.next;
	}

	public final int length() {
		return length;
	}

	public final Object car() {
		if (!isPair())
			throw new NoSuchElementException("list is not a pair");
		return head.next.data;
	}

	public final Object last() {
		if (!isPair())
			throw new NoSuchElementException("list is not a pair");
		return tail.next.data;
	}

	public final List cdr() {
		List list_4_ = newList();
		if (!isPair())
			throw new NoSuchElementException("list is not a pair");
		list_4_.head.next = head.next.next;
		list_4_.tail.next = tail.next;
		list_4_.length = length - 1;
		return list_4_;
	}

	public final List setCar(Object object) {
		if (!isPair())
			throw new NoSuchElementException("list is not a pair");
		head.next.data = object;
		return this;
	}

	public final List setLast(Object object) {
		if (!isPair())
			throw new NoSuchElementException("list is not a pair");
		tail.next.data = object;
		return this;
	}

	public final List setCdr(List list_5_) {
		if (!isPair())
			throw new NoSuchElementException("list is not a pair");
		head.next.next = list_5_.head.next;
		length = list_5_.length + 1;
		return this;
	}

	public final void foreach(Applicator applicator) {
		Enumeration enumeration = elements();
		while (enumeration.hasMoreElements())
			applicator.f(enumeration.nextElement());
	}

	public final List map(List list_6_, Applicator applicator) {
		Enumeration enumeration = elements();
		while (enumeration.hasMoreElements())
			list_6_.append(applicator.f(enumeration.nextElement()));
		return list_6_;
	}

	public final Mark mark() {
		return new Mark();
	}

	public final List insert(Mark mark, Object object) {
		Link link = new Link();
		link.next = mark.link.next;
		link.data = object;
		mark.link.next = link;
		if (tail.next == mark.link)
			tail.next = link;
		length++;
		return this;
	}

	public final List cut(Mark mark) {
		tail.next = mark.link;
		length = mark.length;
		return this;
	}

	public final List hcut(Mark mark) {
		tail.next = mark.link;
		length = 0;
		for (Link link = head.next; link != tail.next.next; link = link.next)
			length++;
		return this;
	}

	public final List insert(Link link, List list_7_) {
		Link link_8_ = link.next;
		list_7_.tail.next.next = link_8_;
		link.next = list_7_.head.next;
		if (tail.next == link)
			tail.next = list_7_.tail.next;
		length += list_7_.length;
		return this;
	}

	public String toString() {
		return isPair() ? cdr().addToString(car().toString()) : "()";
	}

	private String addToString(String string) {
		if (isPair())
			return cdr().addToString(string + " " + car());
		return "(" + string + ")";
	}

	public final Enumeration elements() {
		return new Enumerator();
	}
}