/*
 * URLUtil - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

public class URLUtil {
	public static URL toURL(File file) throws MalformedURLException {
		String string = file.getAbsolutePath().replace(File.separatorChar, '/');
		if (!string.startsWith("/"))
			string = "/" + string;
		if (file.isDirectory() && !string.endsWith("/"))
			string += "/";
		return new URL("file", "", string);
	}

	public static File toFile(URL url) {
		if (url == null || !"file".equalsIgnoreCase(url.getProtocol()))
			throw new RuntimeException(
					"Incorrect argument to URLSpec.toFile(): " + url);
		String string = "";
		String string_0_ = url.getHost();
		if (string_0_ != null && string_0_.trim().length() > 0)
			string = "//" + string_0_;
		string += url.getFile();
		return new File(urlDecode(string).replace('/', File.separatorChar));
	}

	public static File toFile(URLSpec urlspec) {
		if (urlspec == null || !"file".equalsIgnoreCase(urlspec.getProtocol()))
			throw new RuntimeException(
					"Incorrect argument to URLSpec.toFile(): " + urlspec);
		return toFile(urlspec.url);
	}

	public static String urlEncode(String string) {
		char[] cs = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A',
				'B', 'C', 'D', 'E', 'F' };
		Object object = null;
		byte[] is;
		try {
			is = string.getBytes("UTF-8");
		} catch (UnsupportedEncodingException unsupportedencodingexception) {
			is = string.getBytes();
		}
		StringBuffer stringbuffer = new StringBuffer();
		for (int i = 0; i < is.length; i++) {
			char c = (char) is[i];
			if ("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.!~*'()"
					.indexOf(c) != -1)
				stringbuffer.append(c);
			else {
				stringbuffer.append('%');
				stringbuffer.append(cs[c >> 4 & 0xf]);
				stringbuffer.append(cs[c & 0xf]);
			}
		}
		return stringbuffer.toString();
	}

	public static String urlDecode(String string) {
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			if (c == '%' && i + 2 < string.length()) {
				int i_1_ = ("0123456789ABCDEF".indexOf(Character
						.toUpperCase(string.charAt(i + 1))));
				int i_2_ = ("0123456789ABCDEF".indexOf(Character
						.toUpperCase(string.charAt(i + 2))));
				if (i_1_ != -1 && i_2_ != -1) {
					bytearrayoutputstream.write((i_1_ << 4) + i_2_);
					i += 2;
					continue;
				}
			}
			bytearrayoutputstream.write((byte) c);
		}
		Object object = null;
		String string_3_;
		try {
			string_3_ = new String(bytearrayoutputstream.toByteArray(), "UTF-8");
		} catch (UnsupportedEncodingException unsupportedencodingexception) {
			string_3_ = new String(bytearrayoutputstream.toByteArray());
		}
		return string_3_;
	}
}