/*
 * ErrorHandler - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

public interface ErrorHandler {
	public void info(String string);

	public void warning(String string);

	public void error(String string);

	public void exception(String string, Exception exception);
}