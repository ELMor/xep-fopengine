/*
 * XMLLicense - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.io.IOException;
import java.io.StringWriter;
import java.security.Security;
import java.util.Enumeration;

import com.renderx.sax.Canonicalizer;
import com.renderx.sax.Fork;
import com.renderx.sax.Serializer;
import com.renderx.sax.XMLReaderFactory;

import cryptix.pgp.CRC;
import cryptix.pgp.DecryptException;
import cryptix.pgp.PacketFactory;
import cryptix.pgp.Passphrase;
import cryptix.pgp.SecretKeyRing;
import cryptix.pgp.Signature;
import cryptix.provider.Cryptix;
import cryptix.security.MD5;
import cryptix.security.rsa.SecretKey;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.AttributesImpl;

public class XMLLicense {
	private static class SigningFilter extends Fork {
		int level = 0;

		final SecretKey secretKey;

		SigningFilter(ContentHandler contenthandler, SecretKey secretkey) {
			super(new Canonicalizer(), contenthandler);
			secretKey = secretkey;
		}

		public void startElement(String string, String string_0_,
				String string_1_, Attributes attributes) throws SAXException {
			level++;
			super.startElement(string, string_0_, string_1_, attributes);
		}

		public void endElement(String string, String string_2_, String string_3_)
				throws SAXException {
			level--;
			if (level == 0) {
				h1.endElement(string, string_2_, string_3_);
				h1.endDocument();
				Canonicalizer canonicalizer = (Canonicalizer) h1;
				Object object = null;
				String string_4_;
				try {
					string_4_ = makeSignature(canonicalizer.body);
				} catch (IOException ioexception) {
					throw new SAXException(ioexception);
				}
				String string_5_ = "signature";
				String string_6_ = null;
				if (string_3_ != null) {
					String string_7_ = string_3_.substring(0, (string_3_
							.length() - string_2_.length()));
					string_6_ = string_7_ + string_5_;
				}
				h2.startElement(string, string_5_, string_6_,
						new AttributesImpl());
				h2.characters(string_4_.toCharArray(), 0, string_4_.length());
				h2.endElement(string, string_5_, string_6_);
				h1 = null;
			}
			super.endElement(string, string_2_, string_3_);
		}

		private String makeSignature(byte[] is) throws IOException {
			MD5 md5 = new MD5();
			md5.add(is);
			Signature signature = new Signature(secretKey, md5);
			byte[] is_8_ = PacketFactory.save(signature);
			byte[] is_9_ = new byte[3];
			int i = CRC.checksum(is_8_);
			is_9_[0] = (byte) (i >>> 16);
			is_9_[1] = (byte) (i >>> 8);
			is_9_[2] = (byte) i;
			return base64encode(is_8_) + "=" + base64encode(is_9_);
		}

		private static final String base64encode(byte[] is) throws IOException {
			StringWriter stringwriter = new StringWriter();
			Base64OutputStream base64outputstream = new Base64OutputStream(
					stringwriter);
			base64outputstream.write(is);
			base64outputstream.close();
			return stringwriter.toString();
		}
	}

	public static void sign(InputSource inputsource, XMLReader xmlreader,
			ContentHandler contenthandler, String string, String string_10_,
			String string_11_) throws IOException, SAXException,
			DecryptException, PGPException {
		SecretKeyRing secretkeyring = new SecretKeyRing(string_10_);
		SecretKey secretkey = secretkeyring.getKey(string, new Passphrase(
				string_11_));
		if (secretkey == null)
			throw new PGPException("cannot retrieve a secret key for '"
					+ string + "' from secret key ring '" + string_10_ + "'");
		xmlreader
				.setContentHandler(new SigningFilter(contenthandler, secretkey));
		xmlreader.parse(inputsource);
	}

	public static void main(String[] strings) {
		Args args = new Args(strings);
		if (args.arguments.length() != 1) {
			System.err
					.println("call: java com.renderx.util.License [-Dsecring=<path to secring.pgp>] [-Daddress=<signer's address>] passphrase\n");
			System.exit(1);
		}
		String string = "RenderX";
		String string_12_ = "secring.pgp";
		String string_13_ = (String) args.arguments.car();
		Enumeration enumeration = args.options.keys();
		while (enumeration.hasMoreElements()) {
			String string_14_ = (String) enumeration.nextElement();
			if (string_14_.equals("address"))
				string = (String) args.options.get("address");
			else if (string_14_.equals("secring"))
				string_12_ = (String) args.options.get("secring");
			else
				System.err.println("illegal option '" + string_14_
						+ "', skipping");
		}
		java.security.Provider provider = Security.getProvider("Cryptix");
		if (provider == null)
			Security.addProvider(new Cryptix());
		try {
			sign(new InputSource(System.in),
					XMLReaderFactory.createXMLReader(), new Serializer(
							System.out), string, string_12_, string_13_);
		} catch (Exception exception) {
			System.err.println("error signing the license: "
					+ exception.getMessage());
			System.exit(2);
		}
	}
}