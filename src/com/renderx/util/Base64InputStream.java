/*
 * Base64InputStream - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;

public class Base64InputStream extends InputStream {
	public static final String base64charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	private int bits = 0;

	private int bitCount = 0;

	private boolean endOfFile = false;

	private Reader reader = null;

	public Base64InputStream(Reader reader) {
		this.reader = reader;
	}

	public Base64InputStream(String string) {
		this(new StringReader(string));
	}

	public boolean markSupported() {
		return false;
	}

	public long skip(long l) throws IOException {
		long l_0_ = 0L;
		while (l-- > 0L && read() != -1)
			l_0_++;
		return l_0_;
	}

	public int read() throws IOException {
		if (endOfFile)
			return -1;
		while (bitCount < 8) {
			int i = reader.read();
			if (i == -1 || i == 61) {
				endOfFile = true;
				return -1;
			}
			if (i > 32) {
				int i_1_ = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
						.indexOf((char) i);
				if (i_1_ == -1)
					throw new IOException("Invalid character '" + (char) i
							+ "' (hex " + Integer.toHexString(i)
							+ ") in Base64 data");
				bitCount += 6;
				bits |= i_1_ << 32 - bitCount;
			}
		}
		int i = bits >> 24 & 0xff;
		bits <<= 8;
		bitCount -= 8;
		return i;
	}
}