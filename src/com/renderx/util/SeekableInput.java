/*
 * SeekableInput - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.io.DataInput;
import java.io.IOException;

public interface SeekableInput extends DataInput {
	public int read() throws IOException;

	public int read(byte[] is) throws IOException;

	public int read(byte[] is, int i, int i_0_) throws IOException;

	public void close() throws IOException;

	public long readUnsignedInt() throws IOException;

	public long length() throws IOException;

	public long getFilePointer() throws IOException;

	public void seek(long l) throws IOException;
}