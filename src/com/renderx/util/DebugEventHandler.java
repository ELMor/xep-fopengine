/*
 * DebugEventHandler - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

public interface DebugEventHandler {
	public void message(String string);

	public boolean isDebugged(String string);

	public void debug(String string, String string_0_);
}