/*
 * LZWDecoderStream - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class LZWDecoderStream extends FilterOutputStream {
	int initialCodeSize = -1;

	boolean LSBfirst = true;

	int earlyCodeSwitch = 0;

	int CLEAR = 0;

	int EOD = 0;

	int code = 0;

	int codeSize = -1;

	int codeMask = 0;

	int oldCode = -1;

	boolean stop = false;

	byte[] currentChar = null;

	byte[] prefix = null;

	int bits = 0;

	int bitCount = 0;

	int codetableLength = 0;

	final Array codetable = new Array();

	byte[] buffer = new byte[65536];

	int bufferCounter = 0;

	boolean countData = false;

	int bytesToDecode = 0;

	boolean overflowPending = false;

	public LZWDecoderStream(OutputStream outputstream, int i, boolean bool,
			boolean bool_0_, int i_1_) {
		super(outputstream);
		LSBfirst = bool;
		earlyCodeSwitch = bool_0_ ? 1 : 0;
		if (i_1_ == -1)
			countData = false;
		else {
			countData = true;
			bytesToDecode = i_1_;
		}
		initialCodeSize = i + 1;
		CLEAR = 1 << i;
		EOD = CLEAR + 1;
		for (int i_2_ = 0; i_2_ <= EOD; i_2_++)
			codetable.put(i_2_, new byte[] { (byte) i_2_ });
		clear();
	}

	public void write(byte[] is) throws IOException {
		for (int i = 0; i < is.length; i++)
			write(is[i] & 0xff);
	}

	public void write(byte[] is, int i, int i_3_) throws IOException {
		for (int i_4_ = 0; i_4_ < i_3_; i_4_++)
			write(is[i + i_4_] & 0xff);
	}

	public void write(int i) throws IOException {
		if (!stop) {
			if (LSBfirst)
				bits |= i << bitCount;
			else
				bits = bits << 8 | i;
			bitCount += 8;
			while (bitCount >= codeSize) {
				if (LSBfirst) {
					code = bits & codeMask;
					bits >>= codeSize;
				} else
					code = bits >> bitCount - codeSize;
				bitCount -= codeSize;
				bits &= (1 << bitCount) - 1;
				dispatch();
				if (code == EOD) {
					stop = true;
					flush();
					break;
				}
			}
		}
	}

	void clear() {
		overflowPending = false;
		codeSize = initialCodeSize;
		codeMask = (1 << codeSize) - 1;
		oldCode = -1;
		currentChar = null;
		prefix = null;
		codetableLength = EOD + 1;
	}

	void dispatch() throws IOException {
		if (code == CLEAR)
			clear();
		else if (code != EOD) {
			if (overflowPending)
				throw new IOException(
						"LZWDecoder: LZW overflow - codeword length > 12");
			if (code > codetableLength)
				throw new IOException(
						"LZWDecoder: unexpected code in data stream " + code
								+ "(codetable length is " + codetableLength
								+ ")");
			if (code < codetableLength) {
				currentChar = (byte[]) codetable.get(code);
				if (oldCode != -1) {
					codetable.put(codetableLength, addByte(prefix,
							currentChar[0]));
					codetableLength++;
				}
			} else {
				currentChar = addByte(prefix, prefix[0]);
				codetable.put(codetableLength, currentChar);
				codetableLength++;
			}
			bufferedWrite(currentChar);
			if (countData) {
				bytesToDecode -= currentChar.length;
				if (bytesToDecode <= 0) {
					code = EOD;
					return;
				}
			}
			prefix = currentChar;
			oldCode = code;
			if (codetableLength + earlyCodeSwitch >> codeSize != 0) {
				if (codeSize == 12)
					overflowPending = true;
				else
					codeSize++;
				codeMask = (1 << codeSize) - 1;
			}
		}
	}

	public void flush() throws IOException {
		out.write(buffer, 0, bufferCounter);
		bufferCounter = 0;
		out.flush();
	}

	byte[] addByte(byte[] is, byte i) {
		byte[] is_5_ = new byte[is.length + 1];
		System.arraycopy(is, 0, is_5_, 0, is.length);
		is_5_[is.length] = i;
		return is_5_;
	}

	void bufferedWrite(byte[] is) throws IOException {
		if (65536 - bufferCounter < is.length) {
			out.write(buffer, 0, bufferCounter);
			bufferCounter = 0;
		}
		System.arraycopy(is, 0, buffer, bufferCounter, is.length);
		bufferCounter += is.length;
	}
}