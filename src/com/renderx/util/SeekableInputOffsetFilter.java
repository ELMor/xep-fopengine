/*
 * SeekableInputOffsetFilter - Decompiled by JODE Visit
 * http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.io.IOException;

public class SeekableInputOffsetFilter implements SeekableInput {
	private final SeekableInput in;

	private final long offset;

	public SeekableInputOffsetFilter(SeekableInput seekableinput, long l)
			throws IOException {
		seekableinput.seek(l);
		in = seekableinput;
		if (l > seekableinput.length())
			throw new IllegalArgumentException(
					"Offset value is out of parent stream bounds");
		offset = l;
	}

	public long length() throws IOException {
		return in.length() - offset;
	}

	public void seek(long l) throws IOException {
		in.seek(l + offset);
	}

	public long getFilePointer() throws IOException {
		return in.getFilePointer() - offset;
	}

	public int read() throws IOException {
		return in.read();
	}

	public int read(byte[] is) throws IOException {
		return in.read(is);
	}

	public int read(byte[] is, int i, int i_0_) throws IOException {
		return in.read(is, i, i_0_);
	}

	public void close() throws IOException {
		in.close();
	}

	public long readUnsignedInt() throws IOException {
		return in.readUnsignedInt();
	}

	public void readFully(byte[] is) throws IOException {
		in.readFully(is);
	}

	public void readFully(byte[] is, int i, int i_1_) throws IOException {
		in.readFully(is, i, i_1_);
	}

	public int skipBytes(int i) throws IOException {
		return in.skipBytes(i);
	}

	public boolean readBoolean() throws IOException {
		return in.readBoolean();
	}

	public byte readByte() throws IOException {
		return in.readByte();
	}

	public int readUnsignedByte() throws IOException {
		return in.readUnsignedByte();
	}

	public short readShort() throws IOException {
		return in.readShort();
	}

	public int readUnsignedShort() throws IOException {
		return in.readUnsignedShort();
	}

	public char readChar() throws IOException {
		return in.readChar();
	}

	public int readInt() throws IOException {
		return in.readInt();
	}

	public long readLong() throws IOException {
		return in.readLong();
	}

	public float readFloat() throws IOException {
		return in.readFloat();
	}

	public double readDouble() throws IOException {
		return in.readDouble();
	}

	public String readLine() throws IOException {
		return in.readLine();
	}

	public String readUTF() throws IOException {
		return in.readUTF();
	}
}