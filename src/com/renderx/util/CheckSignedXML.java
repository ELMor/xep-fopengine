/*
 * CheckSignedXML - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import com.renderx.sax.XMLReaderFactory;

import org.xml.sax.InputSource;

public final class CheckSignedXML {
	public static void main(String[] strings) throws Exception {
		if (strings.length != 1) {
			System.err
					.println("Usage: java com.renderx.crypto.CheckSignedXML <public key>\n");
			System.exit(1);
		}
		if (com.renderx.crypto.CheckSignedXML.check(new InputSource(System.in),
				XMLReaderFactory.createXMLReader(), strings[0])) {
			System.out.println("OK");
			System.exit(0);
		} else {
			System.out.println("ERROR");
			System.exit(1);
		}
	}
}