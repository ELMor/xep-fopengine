/*
 * DefaultErrorHandler - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

public class DefaultErrorHandler implements ErrorHandler {
	public void info(String string) {
		System.err.println("\n" + string);
	}

	public void warning(String string) {
		System.err.println("\nWarning: " + string);
	}

	public void error(String string) {
		System.err.println("\nError: " + string);
	}

	public void exception(String string, Exception exception) {
		System.err.println("\nError: " + string);
		System.err.println("Exception caught:" + exception.toString());
		System.err.println("Exception stack trace follows:");
		exception.printStackTrace();
	}
}