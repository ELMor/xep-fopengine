/*
 * NewListNotOverriddenException - Decompiled by JODE Visit
 * http://jode.sourceforge.net/
 */
package com.renderx.util;

public class NewListNotOverriddenException extends RuntimeException {
	public NewListNotOverriddenException() {
		/* empty */
	}

	public NewListNotOverriddenException(String string) {
		super(string);
	}
}