/*
 * MIMEType - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.StringTokenizer;

import junit.framework.Assert;
import junit.framework.TestCase;

public class MIMEType {
	public String type = null;

	public String subtype = null;

	public Hashtable parameters = null;

	public static class Test extends TestCase {
		public void testParserBasic() throws MIMETypeParseException {
			MIMEType mimetype = new MIMEType("text/plain");
			Assert.assertEquals("text", mimetype.type);
			Assert.assertEquals("plain", mimetype.subtype);
			mimetype = new MIMEType("Image/SVG+XML");
			Assert.assertEquals("image", mimetype.type);
			Assert.assertEquals("svg+xml", mimetype.subtype);
			mimetype = new MIMEType("image/PNG; qs=0.7");
			Assert.assertEquals("image", mimetype.type);
			Assert.assertEquals("png", mimetype.subtype);
			Assert.assertEquals("0.7", mimetype.parameters.get("qs"));
			mimetype = new MIMEType("image/CGM; Version=4; ProfileId=WebCGM");
			Assert.assertEquals("image", mimetype.type);
			Assert.assertEquals("cgm", mimetype.subtype);
			Assert.assertEquals("4", mimetype.parameters.get("version"));
			Assert.assertEquals("WebCGM", mimetype.parameters.get("profileid"));
		}

		public void testParserExtended() throws MIMETypeParseException {
			MIMEType mimetype = (new MIMEType(
					"image/CGM; Version='4'; ProfileId=\"WebCGM\"; Custom-Data=Extra-Data"));
			Assert.assertEquals("image", mimetype.type);
			Assert.assertEquals("cgm", mimetype.subtype);
			Assert.assertEquals("4", mimetype.parameters.get("version"));
			Assert.assertEquals("WebCGM", mimetype.parameters.get("profileid"));
			Assert.assertEquals("Extra-Data", mimetype.parameters
					.get("custom-data"));
			String string = " I wonder what happens with punctuations... Are they stripped? preserved? ";
			String string_0_ = "Every decent MIME type (like this one) _must_ have a slogan!";
			mimetype = new MIMEType(" image / X-strange ; SLOGAN = '"
					+ string_0_ + "'  ;" + "Motto = \"" + string + "\" ; ");
			Assert.assertEquals("image", mimetype.type);
			Assert.assertEquals("x-strange", mimetype.subtype);
			Assert.assertEquals(string_0_, mimetype.parameters.get("slogan"));
			Assert.assertEquals(string, mimetype.parameters.get("motto"));
		}

		public void testMatch() throws MIMETypeParseException {
			MIMEType mimetype = new MIMEType("text/plain");
			MIMEType mimetype_1_ = new MIMEType("TEXT/PLAIN");
			Assert.assertTrue(mimetype.matches(mimetype_1_));
			Assert.assertTrue(mimetype_1_.matches(mimetype));
			MIMEType mimetype_2_ = new MIMEType("text/xml");
			MIMEType mimetype_3_ = new MIMEType("image/plain");
			Assert.assertFalse(mimetype_2_.matches(mimetype));
			Assert.assertFalse(mimetype_1_.matches(mimetype_2_));
			Assert.assertFalse(mimetype_3_.matches(mimetype_1_));
			Assert.assertFalse(mimetype.matches(mimetype_3_));
			MIMEType mimetype_4_ = new MIMEType("Text/Plain; charset=koi8");
			Assert.assertTrue(mimetype.matches(mimetype_4_));
			Assert.assertTrue(mimetype_4_.matches(mimetype));
			MIMEType mimetype_5_ = new MIMEType("Text/Plain; charset=KOI8");
			Assert.assertTrue(mimetype_4_.matches(mimetype_5_));
			Assert.assertTrue(mimetype_5_.matches(mimetype_4_));
			MIMEType mimetype_6_ = new MIMEType(
					"Text/Plain; charset=windows-1251");
			Assert.assertTrue(mimetype.matches(mimetype_6_));
			Assert.assertTrue(mimetype_6_.matches(mimetype));
			Assert.assertFalse(mimetype_6_.matches(mimetype_4_));
			Assert.assertFalse(mimetype_4_.matches(mimetype_6_));
			MIMEType mimetype_7_ = (new MIMEType(
					"image/CGM; ProfileId=\"WebCGM\"; copyright=\"(c) 1889-2005 The Corporation\""));
			MIMEType mimetype_8_ = new MIMEType(
					"image/CGM; ProfileId=\"WEBcgm\"; license=GPL");
			Assert.assertTrue(mimetype_7_.matches(mimetype_8_));
			Assert.assertTrue(mimetype_8_.matches(mimetype_7_));
		}
	}

	public MIMEType(String string) throws MIMETypeParseException {
		StringTokenizer stringtokenizer = new StringTokenizer(string, ";");
		if (!stringtokenizer.hasMoreTokens())
			throw new MIMETypeParseException("\"" + string
					+ "\" is not a valid MIME type");
		String string_9_ = stringtokenizer.nextToken().trim();
		int i = string_9_.indexOf('/');
		if (i == -1 || i == 0 || i == string_9_.length() - 1)
			throw new MIMETypeParseException("\"" + string_9_
					+ "\" is not a valid MIME type");
		type = string_9_.substring(0, i).trim().toLowerCase();
		subtype = string_9_.substring(i + 1).trim().toLowerCase();
		if (stringtokenizer.hasMoreTokens()) {
			parameters = new Hashtable();
			while (stringtokenizer.hasMoreTokens()) {
				string_9_ = stringtokenizer.nextToken().trim();
				i = string_9_.indexOf('=');
				if (i != -1 && i != 0) {
					String string_10_ = string_9_.substring(0, i).trim()
							.toLowerCase();
					String string_11_ = string_9_.substring(i + 1).trim();
					if ((string_11_.charAt(0) == '\"' && string_11_
							.charAt(string_11_.length() - 1) == '\"')
							|| (string_11_.charAt(0) == '\'' && (string_11_
									.charAt(string_11_.length() - 1) == '\'')))
						string_11_ = string_11_.substring(1, string_11_
								.length() - 1);
					parameters.put(string_10_, string_11_);
				}
			}
		}
	}

	public String toString() {
		String string = type + "/" + subtype;
		if (parameters != null) {
			Enumeration enumeration = parameters.keys();
			while (enumeration.hasMoreElements()) {
				String string_12_ = (String) enumeration.nextElement();
				String string_13_ = (String) parameters.get(string_12_);
				string += "; " + string_12_ + "=" + string_13_;
			}
		}
		return string;
	}

	public boolean matches(MIMEType mimetype_14_) {
		if (!type.equals(mimetype_14_.type))
			return false;
		if (!subtype.equals(mimetype_14_.subtype))
			return false;
		if (parameters == null || mimetype_14_.parameters == null)
			return true;
		Enumeration enumeration = parameters.keys();
		while (enumeration.hasMoreElements()) {
			String string = (String) enumeration.nextElement();
			String string_15_ = (String) parameters.get(string);
			String string_16_ = (String) mimetype_14_.parameters.get(string);
			if (string_16_ != null && !string_16_.equalsIgnoreCase(string_15_))
				return false;
		}
		return true;
	}

	public static void main(String[] strings) {
		char[] cs = new char[8192];
		int i = 0;
		for (;;) {
			try {
				InputStreamReader inputstreamreader = new InputStreamReader(
						System.in);
				i = inputstreamreader.read(cs);
			} catch (IOException ioexception) {
				System.err.println(ioexception);
				System.exit(1);
			}
			if (i < 0)
				System.exit(0);
			try {
				System.out.println(new MIMEType(new String(cs, 0, i))
						.toString());
			} catch (MIMETypeParseException mimetypeparseexception) {
				System.err.println(mimetypeparseexception.toString());
			}
		}
	}
}