/*
 * Array - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

public class Array implements Cloneable {
	protected static final int INITIAL_LENGTH = 4;

	protected Object[] data;

	private int length = 0;

	public Array() {
		this(4);
	}

	public Array(int i) {
		data = new Object[i];
		for (int i_0_ = 0; i_0_ != i; i_0_++)
			data[i_0_] = null;
	}

	protected void resize(int i) {
		Object[] objects = new Object[(i + 1) * 2];
		System.arraycopy(data, 0, objects, 0, data.length);
		for (int i_1_ = data.length; i_1_ != objects.length; i_1_++)
			objects[i_1_] = null;
		data = objects;
	}

	public final int length() {
		return length;
	}

	public final Object get(int i) {
		return data.length <= i || i < 0 ? null : data[i];
	}

	public final void put(int i, Object object) {
		if (i < 0)
			throw new ArrayIndexOutOfBoundsException(i);
		if (data.length <= i)
			resize(i);
		data[i] = object;
		if (i >= length)
			length = i + 1;
	}

	public Object clone() {
		try {
			Array array_2_ = (Array) super.clone();
			array_2_.data = datacopy();
			return array_2_;
		} catch (CloneNotSupportedException clonenotsupportedexception) {
			return null;
		}
	}

	public Object[] datacopy() {
		Object[] objects = new Object[length];
		System.arraycopy(data, 0, objects, 0, length);
		return objects;
	}
}