/*
 * KeyView - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.util;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Security;

import cryptix.pgp.PacketFactory;
import cryptix.provider.Cryptix;

public final class KeyView {
	public static final void main(String[] strings) {
		java.security.Provider provider = Security.getProvider("Cryptix");
		if (provider == null)
			Security.addProvider(new Cryptix());
		if (strings.length < 1)
			System.out.println("Usage: java KeyView keyring-name");
		else {
			try {
				System.out.println("- contents of " + strings[0] + " -");
				FileInputStream fileinputstream;
				viewPackets(new DataInputStream(
						fileinputstream = new FileInputStream(strings[0])));
				fileinputstream.close();
			} catch (FileNotFoundException filenotfoundexception) {
				System.out.println("File : " + strings[0] + " Not found.");
			} catch (Exception exception) {
				exception.printStackTrace(System.out);
			}
		}
	}

	private static final void viewPackets(DataInputStream datainputstream)
			throws IOException {
		int i = 1;
		cryptix.pgp.Packet packet;
		while ((packet = PacketFactory.read(datainputstream)) != null) {
			System.out.println("*- packet " + i++ + " : "
					+ packet.getClass().getName());
			System.out.println(packet);
		}
		System.out.println("- Done -");
	}
}