/*
 * OneByteTTFont - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import com.renderx.fonts.Encoding;
import com.renderx.fonts.FontDescriptor;

public class OneByteTTFont extends OneByteFont {
	OneByteTTFont(IndirectObject indirectobject, FontDescriptor fontdescriptor,
			int i) {
		super(indirectobject, fontdescriptor, i);
	}

	String getSubType() {
		return "TrueType";
	}

	void writeEncoding(PDFOutputStream pdfoutputstream, Encoding encoding) {
		if (this.encoding == 0)
			pdfoutputstream.println("/Encoding /WinAnsiEncoding");
		else if (this.encoding == 1)
			pdfoutputstream.println("/Encoding /MacRomanEncoding");
	}
}