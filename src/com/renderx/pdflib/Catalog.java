/*
 * Catalog - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

public class Catalog extends PDFObject {
	public Pages pages;

	public Outlines outlines;

	public Names names;

	public static final int MODE_AUTO = 0;

	public static final int MODE_OUTLINES = 1;

	public static final int MODE_THUMBNAILS = 2;

	public static final int MODE_NONE = 3;

	public static final int MODE_FULLSCREEN = 4;

	public int pageMode = 0;

	String initialDestination = null;

	final String getType() {
		return "Catalog";
	}

	Catalog(IndirectObject indirectobject) {
		super(indirectobject);
		outlines = null;
		names = null;
	}

	void setViewMode(String string) throws PDFException {
		if ("auto".equals(string))
			pageMode = 0;
		else if ("show-bookmarks".equals(string))
			pageMode = 1;
		else if ("show-thumbnails".equals(string))
			pageMode = 2;
		else if ("show-none".equals(string))
			pageMode = 3;
		else if ("full-screen".equals(string))
			pageMode = 4;
		else
			throw new PDFException("Wrong value for view mode: " + string);
	}

	void setOpenAction(IndirectObject indirectobject, float f, float f_0_,
			String string) throws PDFException {
		if (string == null)
			string = "auto";
		string = string.trim();
		if ("fit".equals(string))
			initialDestination = "[" + indirectobject.toStringR() + " /Fit]";
		else if ("fit-width".equals(string))
			initialDestination = ("[" + indirectobject.toStringR() + " /FitH "
					+ PDFOutputStream.pdf_float_coarse((double) f_0_) + "]");
		else if ("fit-height".equals(string))
			initialDestination = ("[" + indirectobject.toStringR() + " /FitV "
					+ PDFOutputStream.pdf_float_coarse((double) f) + "]");
		else {
			initialDestination = ("[" + indirectobject.toStringR() + " /XYZ "
					+ PDFOutputStream.pdf_float_coarse((double) f) + " "
					+ PDFOutputStream.pdf_float_coarse((double) f_0_) + " 0]");
			if (!"auto".equals(string)) {
				float f_1_ = 0.0F;
				try {
					if (string.endsWith("%"))
						f_1_ = Float.valueOf(
								string.substring(0, string.length() - 1))
								.floatValue() / 100.0F;
					else
						f_1_ = Float.valueOf(string).floatValue();
				} catch (NumberFormatException numberformatexception) {
					throw new PDFException("Wrong value for zoom factor: "
							+ string);
				}
				initialDestination = ("[" + indirectobject.toStringR()
						+ " /XYZ "
						+ PDFOutputStream.pdf_float_coarse((double) f) + " "
						+ PDFOutputStream.pdf_float_coarse((double) f_0_) + " "
						+ PDFOutputStream.pdf_float((double) f_1_) + "]");
			}
		}
	}

	void writeBody(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.println("/Pages " + pages.getID().toStringR());
		pdfoutputstream.println("/PageLabels << /Nums  [");
		boolean bool = false;
		int i = 1;
		for (int i_2_ = 0; i_2_ <= pages.getPagesCount(); i_2_++) {
			Page page = (Page) pages.pages.get(new Integer(i_2_));
			String string = page.number;
			int i_3_ = i_2_ + 1;
			if (page.number != null) {
				try {
					i_3_ = Integer.parseInt(page.number.trim());
					if (i_3_ <= 0)
						throw new NumberFormatException();
				} catch (NumberFormatException numberformatexception) {
					pdfoutputstream.println(" " + i_2_ + " <</P ("
							+ page.number + ")>>");
					bool = false;
					continue;
				}
			}
			if (!bool || i != i_3_ - i_2_) {
				bool = true;
				i = i_3_ - i_2_;
				pdfoutputstream.println(" " + i_2_ + " <</S/D"
						+ (i_3_ == 1 ? "" : "/St " + i_3_) + ">>");
			}
		}
		pdfoutputstream.println("] >>");
		if (pageMode == 0)
			pageMode = outlines == null ? 3 : 1;
		switch (pageMode) {
		case 3:
			pdfoutputstream.println("/PageMode /UseNone");
			break;
		case 1:
			pdfoutputstream.println("/PageMode /UseOutlines");
			break;
		case 2:
			pdfoutputstream.println("/PageMode /UseThumbs");
			break;
		case 4:
			pdfoutputstream.println("/PageMode /FullScreen");
			break;
		}
		if (initialDestination != null)
			pdfoutputstream.println("/OpenAction " + initialDestination);
		if (outlines != null)
			pdfoutputstream
					.println("/Outlines " + outlines.getID().toStringR());
		if (names != null)
			pdfoutputstream.println("/Names " + names.getID().toStringR());
	}

	void addPage(Page page) {
		pages.addPage(page);
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<writepages>");
		pages.write(pdfoutputstream);
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("</writepages>");
	}
}