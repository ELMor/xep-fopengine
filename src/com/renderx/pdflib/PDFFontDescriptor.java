/*
 * PDFFontDescriptor - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.util.StringTokenizer;

import com.renderx.fonts.FontDescriptor;
import com.renderx.fonts.Metric;

public class PDFFontDescriptor extends PDFObject {
	PDFFontFile fontFile;

	FontDescriptor descriptor;

	boolean enableSubset;

	int flags;

	String fontName;

	PDFFontDescriptor(IndirectObject indirectobject,
			FontDescriptor fontdescriptor, String string, int i, boolean bool) {
		super(indirectobject);
		descriptor = fontdescriptor;
		StringTokenizer stringtokenizer = new StringTokenizer(string);
		fontName = "";
		while (stringtokenizer.hasMoreTokens()) {
			StringBuffer stringbuffer = new StringBuffer();
			PDFFontDescriptor pdffontdescriptor_0_ = this;
			pdffontdescriptor_0_.fontName = stringbuffer.append(
					pdffontdescriptor_0_.fontName).append(
					stringtokenizer.nextToken()).toString();
		}
		flags = i;
		enableSubset = bool;
	}

	String getType() {
		return "FontDescriptor";
	}

	void writeBody(PDFOutputStream pdfoutputstream) {
		Metric metric = descriptor.record.getMetric();
		pdfoutputstream.println("/FontName /" + fontName);
		pdfoutputstream.println("/Ascent "
				+ PDFOutputStream.pdf_float((double) metric.ascender));
		pdfoutputstream.println("/Descent "
				+ PDFOutputStream.pdf_float((double) metric.descender));
		pdfoutputstream.println("/CapHeight "
				+ PDFOutputStream.pdf_float((double) metric.capHeight));
		pdfoutputstream.println("/Flags " + flags);
		pdfoutputstream.println("/FontBBox ["
				+ PDFOutputStream.pdf_float((double) metric.fontBBox[0]) + " "
				+ PDFOutputStream.pdf_float((double) metric.fontBBox[1]) + " "
				+ PDFOutputStream.pdf_float((double) metric.fontBBox[2]) + " "
				+ PDFOutputStream.pdf_float((double) metric.fontBBox[3]) + "]");
		pdfoutputstream.println("/ItalicAngle "
				+ PDFOutputStream.pdf_float((double) metric.italicAngle));
		pdfoutputstream.println("/StemV "
				+ PDFOutputStream.pdf_float((double) metric.stdVW));
		if (metric.stdHW != 1.4E-45F)
			pdfoutputstream.println("/StemH "
					+ PDFOutputStream.pdf_float((double) metric.stdHW));
		if (metric.xHeight != 1.4E-45F)
			pdfoutputstream.println("/XHeight "
					+ PDFOutputStream.pdf_float((double) metric.xHeight));
		if (descriptor.record.embed) {
			if (descriptor.record.datatype == 2)
				fontFile = new TTFontFile(new IndirectObject(
						pdfoutputstream.lastObjectNumber++), descriptor,
						enableSubset);
			else if (descriptor.record.datatype == 3
					|| descriptor.record.datatype == 4)
				fontFile = new CFFFontFile(new IndirectObject(
						pdfoutputstream.lastObjectNumber++), descriptor,
						enableSubset);
			else if (descriptor.record.datatype == 1)
				fontFile = new T1FontFile(new IndirectObject(
						pdfoutputstream.lastObjectNumber++), descriptor,
						enableSubset);
			pdfoutputstream.println("/" + fontFile.pdfFontType() + " "
					+ fontFile.getID().toStringR());
		}
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		if (descriptor.record.embed)
			fontFile.write(pdfoutputstream);
	}
}