/*
 * GenericHintRecord - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;

import com.renderx.pdflib.parser.PDFInstance;
import com.renderx.util.Hashtable;

public class GenericHintRecord extends AbstractHintRecord {
	public int firstID;

	public long firstLocation;

	public int numobjs;

	public long lengthGroup;

	public GenericHintRecord(Hashtable hashtable, Hashtable hashtable_0_,
			PDFInstance pdfinstance) {
		numobjs = hashtable.size();
		int i = 2147483647;
		lengthGroup = 0L;
		Enumeration enumeration = hashtable.keys();
		while (enumeration.hasMoreElements()) {
			Integer integer = (Integer) enumeration.nextElement();
			if (i > pdfinstance.newID(integer).intValue())
				i = pdfinstance.newID(integer).intValue();
			lengthGroup += ((Long) hashtable_0_.get(integer)).longValue();
		}
	}

	byte[] getBytes() throws IOException {
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		bytearrayoutputstream.write(this.int2FourBytes(firstID));
		bytearrayoutputstream.write(this.long2FourBytes(firstLocation));
		bytearrayoutputstream.write(this.int2FourBytes(numobjs));
		bytearrayoutputstream.write(this.long2FourBytes(lengthGroup));
		return bytearrayoutputstream.toByteArray();
	}
}