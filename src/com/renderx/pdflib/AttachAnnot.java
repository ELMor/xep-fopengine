/*
 * AttachAnnot - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

class AttachAnnot extends Annot {
	int icon;

	String filename;

	String contents;

	String mimetype;

	String title;

	AttachAnnot(IndirectObject indirectobject, float f, float f_0_, float f_1_,
			float f_2_, int i, float f_3_, float f_4_, String string,
			String string_5_, String string_6_, String string_7_) {
		super(indirectobject, f, f_0_, f_1_, f_2_);
		filename = string;
		contents = string_5_;
		mimetype = string_6_;
		title = string_7_;
	}

	String getSubType() {
		return "Link";
	}

	void writeAnnotBody(PDFOutputStream pdfoutputstream) {
		if (icon != 2)
			pdfoutputstream.println("/Name /" + Annot.icon_names[icon] + "\n");
		if (title != null) {
			pdfoutputstream.println("/T ");
			pdfoutputstream.print_annotation(title);
			pdfoutputstream.println("\n");
		}
		if (contents != null) {
			pdfoutputstream.println("/Contents ");
			pdfoutputstream.print_annotation(contents);
			pdfoutputstream.println("\n");
		}
		int i = 28;
		pdfoutputstream.println("/F " + i + "\n");
		pdfoutputstream.println("/FS ");
		pdfoutputstream.begin_dict();
		pdfoutputstream.println("/Type/FileSpec\n");
		pdfoutputstream.println("/F (" + filename + ")\n");
		pdfoutputstream.println("/EF << /F " + obj_id.toStringR() + ">>\n");
		pdfoutputstream.end_dict();
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		/* empty */
	}
}