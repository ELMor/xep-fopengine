/*
 * TTFont - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import com.renderx.fonts.FontDescriptor;

public class TTFont extends PDFFont {
	int encoding;

	private CMap cmap;

	T0DescendantFont t0DescendantFont;

	TTFont(IndirectObject indirectobject, FontDescriptor fontdescriptor, int i) {
		super(indirectobject, fontdescriptor);
		encoding = i;
		if (enableSubset && fontdescriptor.record.subset
				&& fontdescriptor.record.embed)
			this.subsetName();
	}

	String getSubType() {
		return "Type0";
	}

	void writeAddInfo(PDFOutputStream pdfoutputstream) {
		String string = "Identity-H";
		t0DescendantFont = new T0DescendantFont(new IndirectObject(
				pdfoutputstream.lastObjectNumber++), descriptor, enableSubset);
		pdfoutputstream.println("/Encoding/" + string);
		pdfoutputstream.println("/DescendantFonts ["
				+ t0DescendantFont.getID().toStringR() + "]");
		if (!descriptor.record.embed && descriptor.record.getMetric().isCIDFont)
			cmap = null;
		else {
			cmap = new CMap(new IndirectObject(
					pdfoutputstream.lastObjectNumber++), descriptor, null,
					enableSubset);
			pdfoutputstream.println("/ToUnicode " + cmap.getID().toStringR());
		}
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		if (cmap != null)
			cmap.write(pdfoutputstream);
		t0DescendantFont.write(pdfoutputstream);
	}
}