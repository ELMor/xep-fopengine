/*
 * PDFImage - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.io.IOException;
import java.util.Enumeration;

import com.renderx.graphics.BitmapImage;
import com.renderx.graphics.Image;
import com.renderx.graphics.ImageFormatException;
import com.renderx.graphics.VectorImage;
import com.renderx.graphics.vector.Contour;
import com.renderx.graphics.vector.ExternalImage;
import com.renderx.graphics.vector.GraphicGroup;
import com.renderx.graphics.vector.GraphicObject;
import com.renderx.graphics.vector.ImageTree;
import com.renderx.graphics.vector.OpaqueColor;
import com.renderx.graphics.vector.PaintSpec;
import com.renderx.graphics.vector.Path;
import com.renderx.graphics.vector.StrokeSpec;
import com.renderx.graphics.vector.Subpath;
import com.renderx.graphics.vector.Text;
import com.renderx.pdflib.parser.PDFDictionary;
import com.renderx.pdflib.parser.PDFInstance;
import com.renderx.pdflib.parser.PDFNumeric;
import com.renderx.pdflib.parser.PDFStream;
import com.renderx.pdflib.parser.PDFUniversalObject;
import com.renderx.pdflib.parser.Rectangle;
import com.renderx.util.Hashtable;

public class PDFImage extends PDFObject {
	Image image;

	ImageTree imageTree;

	int length;

	IndirectObject length_id;

	IndirectObject cspace_id = null;

	PDFDictionary resources = null;

	Resources formResources = null;

	boolean clippingPending = false;

	Hashtable subTrees = new Hashtable();

	Hashtable subIds = new Hashtable();

	String lastSubobjName;

	double[] clipMatrix = { 1.0, 0.0, 0.0, 1.0, 0.0, 0.0 };

	double[] contentMatrix = { 1.0, 0.0, 0.0, 1.0, 0.0, 0.0 };

	int subcount = 0;

	class SubTreeVectorImage extends VectorImage {
		ImageTree imageTree = null;

		public void parse() throws IOException, ImageFormatException {
			/* empty */
		}

		public SubTreeVectorImage(ImageTree imagetree) {
			imageTree = imagetree;
		}

		public ImageTree getImageTree() throws IOException,
				ImageFormatException {
			return imageTree;
		}
	}

	PDFImage(IndirectObject indirectobject, Image image,
			PDFOutputStream pdfoutputstream) {
		super(indirectobject);
		this.image = image;
		imageTree = null;
		try {
			if (!(this.image instanceof BitmapImage)
					&& !(this.image instanceof PDFInstance)) {
				if (this.image instanceof VectorImage) {
					VectorImage vectorimage = (VectorImage) this.image;
					if (vectorimage.supportsVectorRendering)
						imageTree = vectorimage.getImageTree();
					else if (vectorimage.hasBitmapPreview)
						this.image = vectorimage.getBitmapPreview();
					else if (vectorimage.supportsRasterizing)
						this.image = vectorimage.rasterize(300.0);
					else
						throw new PDFException("Unsupported format "
								+ image.mimetype + " for image '"
								+ image.toDisplayString() + "'");
				}
			}
		} catch (Exception exception) {
			this.image = null;
			pdfoutputstream.exception(("Cannot parse image '"
					+ image.toDisplayString() + "'"), exception);
		}
	}

	double getWidth() {
		return image == null ? 0.0 : image.width;
	}

	double getHeight() {
		return image == null ? 0.0 : image.height;
	}

	String getType() {
		return "XObject";
	}

	void writeBody(PDFOutputStream pdfoutputstream) {
		if (image != null) {
			length_id = new IndirectObject(pdfoutputstream.lastObjectNumber++);
			if (image instanceof BitmapImage) {
				pdfoutputstream.println("/Subtype/Image");
				BitmapImage bitmapimage = (BitmapImage) image;
				pdfoutputstream.println("/Width " + bitmapimage.pxWidth);
				pdfoutputstream.println("/Height " + bitmapimage.pxHeight);
				switch (bitmapimage.colorSpace) {
				case 1:
					pdfoutputstream.println("/ColorSpace /DeviceRGB");
					break;
				case 2:
					pdfoutputstream.println("/ColorSpace /DeviceCMYK");
					break;
				case 3:
					pdfoutputstream.println("/ColorSpace /DeviceCMYK");
					pdfoutputstream.println("/Decode [1 0 1 0 1 0 1 0]");
					break;
				case 4:
					pdfoutputstream.println("/ColorSpace /DeviceGray");
					break;
				case 5:
					pdfoutputstream.println("/ColorSpace /DeviceGray");
					pdfoutputstream.println("/Decode [1 0]");
					break;
				case 6: {
					int i = bitmapimage.colorTable.length / 3;
					cspace_id = new IndirectObject(
							pdfoutputstream.lastObjectNumber++);
					pdfoutputstream.lastObjectNumber++;
					pdfoutputstream.println("/ColorSpace [/Indexed /DeviceRGB "
							+ (i - 1) + " " + cspace_id.toStringR() + "]");
					break;
				}
				}
				pdfoutputstream.println("/BitsPerComponent "
						+ bitmapimage.bitsPerComponent);
				if (bitmapimage.transparentColor != null) {
					pdfoutputstream.print("/Mask [");
					for (int i = 0; i < bitmapimage.transparentColor.length; i++) {
						if (i > 0)
							pdfoutputstream.print(" ");
						int i_0_ = bitmapimage.transparentColor[i] & 0xff;
						pdfoutputstream.print("" + i_0_ + " " + i_0_);
					}
					pdfoutputstream.println("]");
				}
			}
		}
	}

	void writeTail(PDFOutputStream pdfoutputstream) {
		if (image == null)
			pdfoutputstream.println(">>\nendobj");
		else if (image instanceof BitmapImage) {
			BitmapImage bitmapimage = (BitmapImage) image;
			if (bitmapimage.canCopyData)
				copyBitmapData(pdfoutputstream);
			else
				expandBitmapData(pdfoutputstream);
		} else if (image instanceof PDFInstance) {
			PDFInstance pdfinstance = (PDFInstance) image;
			try {
				pdfinstance.open();
				try {
					copyContent(pdfoutputstream);
				} finally {
					pdfinstance.close();
				}
			} catch (Exception exception) {
				pdfoutputstream.exception(
						"Cannot copy the contents of a PDF image", exception);
			}
		} else if (image instanceof VectorImage && imageTree != null)
			writeImageTree(pdfoutputstream);
	}

	private void copyContent(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.println("/Subtype/Form");
		PDFInstance pdfinstance = (PDFInstance) image;
		pdfinstance.converObjects.clear();
		pdfinstance.pendingObjects.clear();
		Object object = null;
		PDFDictionary pdfdictionary;
		try {
			try {
				Rectangle rectangle = pdfinstance.getMediaBox();
				pdfdictionary = pdfinstance.getDictionary(pdfinstance
						.getPage(0));
				pdfoutputstream.println("/BBox ["
						+ PDFOutputStream.pdf_float((double) rectangle.x1)
						+ " "
						+ PDFOutputStream.pdf_float((double) rectangle.y1)
						+ " "
						+ PDFOutputStream.pdf_float((double) rectangle.x2)
						+ " "
						+ PDFOutputStream.pdf_float((double) rectangle.y2)
						+ "]");
				float f = 1.0F / rectangle.getWidth();
				float f_1_ = 1.0F / rectangle.getHeight();
				float[] fs = new float[6];
				switch (pdfinstance.angle) {
				case 0:
					fs[0] = f;
					fs[1] = 0.0F;
					fs[2] = 0.0F;
					fs[3] = f_1_;
					fs[4] = -rectangle.x1 * f;
					fs[5] = -rectangle.y1 * f_1_;
					break;
				case 90:
					fs[0] = 0.0F;
					fs[1] = -f;
					fs[2] = f_1_;
					fs[3] = 0.0F;
					fs[4] = -rectangle.y1 * f_1_;
					fs[5] = rectangle.x2 * f;
					break;
				case 180:
					fs[0] = -f;
					fs[1] = 0.0F;
					fs[2] = 0.0F;
					fs[3] = -f_1_;
					fs[4] = rectangle.x2 * f;
					fs[5] = rectangle.y2 * f_1_;
					break;
				case 270:
					fs[0] = 0.0F;
					fs[1] = f;
					fs[2] = -f_1_;
					fs[3] = 0.0F;
					fs[4] = rectangle.y2 * f_1_;
					fs[5] = -rectangle.x1 * f;
					break;
				}
				pdfoutputstream.println("/Matrix ["
						+ PDFOutputStream.pdf_float((double) fs[0]) + " "
						+ PDFOutputStream.pdf_float((double) fs[1]) + " "
						+ PDFOutputStream.pdf_float((double) fs[2]) + " "
						+ PDFOutputStream.pdf_float((double) fs[3]) + " "
						+ PDFOutputStream.pdf_float((double) fs[4]) + " "
						+ PDFOutputStream.pdf_float((double) fs[5]) + "]");
				if (pdfdictionary.dict.containsKey("/Filter"))
					pdfoutputstream.println("/Filter "
							+ pdfdictionary.dict.get("/Filter").toString());
				else if (pdfoutputstream.compression == true)
					pdfoutputstream.println("/Filter /FlateDecode");
				try {
					resources = pdfinstance.getResources();
				} catch (ClassCastException classcastexception) {
					throw new PDFExternalFileException("Broken pdf file "
							+ pdfinstance.toDisplayString());
				}
			} catch (IOException ioexception) {
				pdfoutputstream.exception("", ioexception);
				pdfoutputstream.println(">>\nendobj");
				return;
			}
		} catch (ImageFormatException imageformatexception) {
			pdfoutputstream.exception("", imageformatexception);
			pdfoutputstream.println(">>\nendobj");
			return;
		}
		try {
			pdfoutputstream.println("/Resources");
			resources.write(pdfoutputstream, pdfinstance);
			pdfoutputstream.println("/Length " + length_id.toStringR());
			pdfoutputstream.println(">>");
		} catch (IOException ioexception) {
			pdfoutputstream.exception("", ioexception);
			pdfoutputstream.println(">>\nendobj");
			return;
		}
		pdfoutputstream.println("stream");
		try {
			try {
				int i = pdfoutputstream.size();
				if (pdfdictionary.dict.containsKey("/Filter"))
					pdfoutputstream.startCompressedStream();
				else
					pdfoutputstream.startStream();
				pdfinstance.getContent(pdfoutputstream);
				if (pdfdictionary.dict.containsKey("/Filter"))
					pdfoutputstream.finishCompressedStream();
				else
					pdfoutputstream.finishStream();
				length = pdfoutputstream.size() - i;
			} catch (IOException ioexception) {
				pdfoutputstream.exception("", ioexception);
			}
		} catch (ImageFormatException imageformatexception) {
			pdfoutputstream.exception("", imageformatexception);
		}
		pdfoutputstream.println("endstream");
		pdfoutputstream.println("endobj");
	}

	private void writeImageTree(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.println("/Subtype/Form");
		pdfoutputstream.doc.currentImage = this;
		IndirectObject indirectobject = new IndirectObject(
				pdfoutputstream.lastObjectNumber++);
		formResources = new Resources(indirectobject);
		pdfoutputstream.println("/BBox ["
				+ PDFOutputStream.pdf_float(imageTree.startX)
				+ " "
				+ PDFOutputStream.pdf_float(imageTree.startY)
				+ " "
				+ PDFOutputStream.pdf_float(imageTree.startX + imageTree.width)
				+ " "
				+ PDFOutputStream
						.pdf_float(imageTree.startY + imageTree.height) + "]");
		pdfoutputstream.println("/Matrix ["
				+ PDFOutputStream.pdf_float(1.0 / imageTree.width) + " 0 0 "
				+ PDFOutputStream.pdf_float(1.0 / imageTree.height) + " 0 0]");
		pdfoutputstream.println("/Length " + length_id.toStringR());
		pdfoutputstream.println("/Resources " + indirectobject.toStringR());
		if (pdfoutputstream.compression == true)
			pdfoutputstream.println("/Filter /FlateDecode");
		pdfoutputstream.println(">>");
		pdfoutputstream.println("stream");
		try {
			int i = pdfoutputstream.size();
			pdfoutputstream.startStream();
			pdfoutputstream.doc.setCharSpacing(0.0F);
			pdfoutputstream.doc.setWordSpacing(0.0F);
			PaintGraphicObject(pdfoutputstream, imageTree.root);
			pdfoutputstream.finishStream();
			length = pdfoutputstream.size() - i;
		} catch (IOException ioexception) {
			pdfoutputstream.exception("", ioexception);
		}
		pdfoutputstream.println("endstream");
		pdfoutputstream.println("endobj");
	}

	void PaintGraphicObject(PDFOutputStream pdfoutputstream,
			GraphicObject graphicobject) throws IOException {
		if (graphicobject instanceof GraphicGroup) {
			GraphicGroup graphicgroup = (GraphicGroup) graphicobject;
			boolean bool = false;
			boolean bool_2_ = false;
			double[] ds = contentMatrix;
			double[] ds_3_ = contentMatrix;
			if (!bool
					&& !clippingPending
					&& (!graphicgroup.isIdentityTransform() || graphicgroup.clippath != null)) {
				pdfoutputstream.println("q");
				bool = true;
			}
			if (!graphicgroup.isIdentityTransform() && !clippingPending) {
				printMatrix(pdfoutputstream, graphicgroup.matrix);
				ds_3_ = graphicgroup.multipleMatrix(contentMatrix,
						graphicgroup.matrix);
			}
			if (graphicgroup.clippath != null && !clippingPending) {
				bool_2_ = !clippingPending;
				clippingPending = true;
				GraphicGroup graphicgroup_4_ = graphicgroup.clippath;
				graphicgroup.clippath = null;
				graphicgroup.matrix = new double[] { 1.0, 0.0, 0.0, 1.0, 0.0,
						0.0 };
				IndirectObject indirectobject = new IndirectObject(
						pdfoutputstream.lastObjectNumber++);
				lastSubobjName = "Img" + subcount;
				formResources.addImage("/" + lastSubobjName + " "
						+ indirectobject.toStringR());
				double[] ds_5_ = calcBBox(imageTree.startX, imageTree.startY,
						imageTree.width, imageTree.height, graphicgroup
								.reverseMatrix(ds_3_), graphicgroup);
				ImageTree imagetree = new ImageTree(ds_5_[0], ds_5_[1],
						ds_5_[2] - ds_5_[0], ds_5_[3] - ds_5_[1]);
				imagetree.root = graphicgroup;
				subTrees.put(lastSubobjName, imagetree);
				subIds.put(lastSubobjName, indirectobject);
				subcount++;
				PaintGraphicObject(pdfoutputstream, graphicgroup_4_);
			} else if (clippingPending) {
				double[] ds_6_ = clipMatrix;
				double[] ds_7_ = (graphicgroup.isIdentityTransform() ? clipMatrix
						: graphicgroup.multipleMatrix(clipMatrix,
								graphicgroup.matrix));
				Enumeration enumeration = graphicgroup.children.elements();
				while (enumeration.hasMoreElements()) {
					clipMatrix = ds_7_;
					GraphicObject graphicobject_8_ = (GraphicObject) enumeration
							.nextElement();
					if (graphicobject_8_ instanceof Contour) {
						pdfoutputstream.println("q");
						if (!graphicgroup.isIdentityTransform(clipMatrix))
							printMatrix(pdfoutputstream, clipMatrix);
						PaintGraphicObject(pdfoutputstream, graphicobject_8_);
						if (!graphicgroup.isIdentityTransform(clipMatrix))
							printMatrix(pdfoutputstream, graphicgroup
									.reverseMatrix(clipMatrix));
						ImageTree imagetree = (ImageTree) subTrees
								.get(lastSubobjName);
						printMatrix(pdfoutputstream, new double[] {
								imagetree.width, 0.0, 0.0, imagetree.height,
								0.0, 0.0 });
						pdfoutputstream.println("/" + lastSubobjName + " Do");
						pdfoutputstream.println("Q");
					} else if (graphicobject_8_ instanceof GraphicGroup)
						PaintGraphicObject(pdfoutputstream, graphicobject_8_);
				}
				clipMatrix = ds_6_;
			} else {
				Enumeration enumeration = graphicgroup.children.elements();
				while (enumeration.hasMoreElements()) {
					contentMatrix = ds_3_;
					PaintGraphicObject(pdfoutputstream,
							((GraphicObject) enumeration.nextElement()));
				}
				contentMatrix = ds;
			}
			if (bool_2_)
				clippingPending = false;
			if (bool)
				pdfoutputstream.println("Q");
		} else if (graphicobject instanceof Contour) {
			Contour contour = (Contour) graphicobject;
			boolean bool = contour.fill != null;
			boolean bool_9_ = contour.stroke != null
					&& contour.stroke.paint != null;
			if (bool || bool_9_ || clippingPending) {
				if (bool)
					PaintFillSpec(pdfoutputstream, contour.fill,
							contour.fillOpacity);
				if (bool_9_)
					PaintStrokeSpec(pdfoutputstream, contour.stroke,
							contour.strokeOpacity);
				if (contour instanceof Path) {
					Enumeration enumeration = ((Path) contour).subs.elements();
					while (enumeration.hasMoreElements())
						PaintSubpath(pdfoutputstream, (Subpath) enumeration
								.nextElement());
					if (clippingPending) {
						pdfoutputstream.println(contour.clipRule == 0 ? "W"
								: "W*");
						pdfoutputstream.println("n");
					} else if (bool && bool_9_) {
						if (contour.strokeOpacity != 1.0) {
							pdfoutputstream.println(contour.fillRule == 0 ? "f"
									: "f*");
							enumeration = ((Path) contour).subs.elements();
							while (enumeration.hasMoreElements())
								PaintSubpath(pdfoutputstream,
										((Subpath) enumeration.nextElement()));
							pdfoutputstream.println("S");
						} else
							pdfoutputstream.println(contour.fillRule == 0 ? "B"
									: "B*");
					} else if (bool)
						pdfoutputstream.println(contour.fillRule == 0 ? "f"
								: "f*");
					else if (bool_9_)
						pdfoutputstream.println("S");
				} else if (contour instanceof Text) {
					Text text = (Text) contour;
					pdfoutputstream.startText();
					pdfoutputstream.doc.setFontSize((float) text.font.size);
					pdfoutputstream.doc.setHorzScale((float) text.font.stretch);
					pdfoutputstream.doc.u28.selectFont(text.font.family,
							text.font.weight, text.font.style,
							text.font.variant);
					if (clippingPending)
						pdfoutputstream.println("7 Tr");
					else if (bool && bool_9_) {
						if (contour.strokeOpacity != 1.0) {
							pdfoutputstream.println("0 Tr");
							pdfoutputstream.doc.setTextPos((float) text.x,
									(float) text.y);
							pdfoutputstream.doc.u28.processText(new String(
									text.value));
							pdfoutputstream.endText();
							pdfoutputstream.startText();
							pdfoutputstream.println("1 Tr");
						} else
							pdfoutputstream.println("2 Tr");
					} else if (bool_9_)
						pdfoutputstream.println("1 Tr");
					else if (bool)
						pdfoutputstream.println("0 Tr");
					pdfoutputstream.doc.setTextPos((float) text.x,
							(float) text.y);
					pdfoutputstream.doc.u28.processText(new String(text.value));
					pdfoutputstream.endText();
				}
			}
		} else if (graphicobject instanceof ExternalImage) {
			ExternalImage externalimage = (ExternalImage) graphicobject;
			pdfoutputstream.doc.placeImage(externalimage.image);
		}
	}

	void PaintSubpath(PDFOutputStream pdfoutputstream, Subpath subpath)
			throws IOException {
		pdfoutputstream.println("" + PDFOutputStream.pdf_float(subpath.startX)
				+ " " + PDFOutputStream.pdf_float(subpath.startY) + " m");
		Enumeration enumeration = subpath.segs.elements();
		while (enumeration.hasMoreElements()) {
			Subpath.Segment segment = (Subpath.Segment) enumeration
					.nextElement();
			if (segment instanceof Subpath.Line) {
				Subpath.Line line = (Subpath.Line) segment;
				pdfoutputstream.println(""
						+ PDFOutputStream.pdf_float(line.xend) + " "
						+ PDFOutputStream.pdf_float(line.yend) + " l");
			} else if (segment instanceof Subpath.Curve) {
				Subpath.Curve curve = (Subpath.Curve) segment;
				pdfoutputstream.println(""
						+ PDFOutputStream.pdf_float(curve.cpx1) + " "
						+ PDFOutputStream.pdf_float(curve.cpy1) + " "
						+ PDFOutputStream.pdf_float(curve.cpx2) + " "
						+ PDFOutputStream.pdf_float(curve.cpy2) + " "
						+ PDFOutputStream.pdf_float(curve.xend) + " "
						+ PDFOutputStream.pdf_float(curve.yend) + " c");
			}
		}
		if (subpath.closed)
			pdfoutputstream.println("h");
	}

	void PaintFillSpec(PDFOutputStream pdfoutputstream, PaintSpec paintspec,
			double d) throws IOException {
		pdfoutputstream.doc.setOpacityFill((float) d);
		if (paintspec instanceof OpaqueColor.Grayscale) {
			OpaqueColor.Grayscale grayscale = (OpaqueColor.Grayscale) paintspec;
			pdfoutputstream.doc.setGrayFill((float) grayscale.g);
		} else if (paintspec instanceof OpaqueColor.RGB) {
			OpaqueColor.RGB rgb = (OpaqueColor.RGB) paintspec;
			pdfoutputstream.doc.setRGBColorFill((float) rgb.r, (float) rgb.g,
					(float) rgb.b);
		} else if (paintspec instanceof OpaqueColor.CMYK) {
			OpaqueColor.CMYK cmyk = (OpaqueColor.CMYK) paintspec;
			pdfoutputstream.doc.setCMYKColorFill((float) cmyk.c,
					(float) cmyk.m, (float) cmyk.y, (float) cmyk.k);
		} else if (paintspec instanceof OpaqueColor.SpotColor) {
			OpaqueColor.SpotColor spotcolor = (OpaqueColor.SpotColor) paintspec;
			float[] fs = { 0.0F };
			if (spotcolor.altcolor != null) {
				if (spotcolor.altcolor instanceof OpaqueColor.Grayscale) {
					OpaqueColor.Grayscale grayscale = (OpaqueColor.Grayscale) spotcolor.altcolor;
					fs = new float[] { (float) grayscale.g };
				} else if (spotcolor.altcolor instanceof OpaqueColor.RGB) {
					OpaqueColor.RGB rgb = (OpaqueColor.RGB) spotcolor.altcolor;
					fs = new float[] { (float) rgb.r, (float) rgb.g,
							(float) rgb.b };
				} else if (spotcolor.altcolor instanceof OpaqueColor.CMYK) {
					OpaqueColor.CMYK cmyk = (OpaqueColor.CMYK) spotcolor.altcolor;
					fs = new float[] { (float) cmyk.c, (float) cmyk.m,
							(float) cmyk.y, (float) cmyk.k };
				}
			}
			pdfoutputstream.doc.setSpotColorFill((float) spotcolor.tint,
					spotcolor.colorant, fs);
		} else if (paintspec instanceof OpaqueColor.Registration) {
			OpaqueColor.Registration registration = (OpaqueColor.Registration) paintspec;
			pdfoutputstream.doc.setSpotColorFill((float) registration.tint,
					"All", new float[] { 0.0F });
		}
	}

	void PaintStrokeSpec(PDFOutputStream pdfoutputstream,
			StrokeSpec strokespec, double d) throws IOException {
		pdfoutputstream.doc.setOpacityStroke((float) d);
		if (strokespec.paint instanceof OpaqueColor.Grayscale) {
			OpaqueColor.Grayscale grayscale = (OpaqueColor.Grayscale) strokespec.paint;
			pdfoutputstream.doc.setGrayStroke((float) grayscale.g);
		} else if (strokespec.paint instanceof OpaqueColor.RGB) {
			OpaqueColor.RGB rgb = (OpaqueColor.RGB) strokespec.paint;
			pdfoutputstream.doc.setRGBColorStroke((float) rgb.r, (float) rgb.g,
					(float) rgb.b);
		} else if (strokespec.paint instanceof OpaqueColor.CMYK) {
			OpaqueColor.CMYK cmyk = (OpaqueColor.CMYK) strokespec.paint;
			pdfoutputstream.doc.setCMYKColorStroke((float) cmyk.c,
					(float) cmyk.m, (float) cmyk.y, (float) cmyk.k);
		} else if (strokespec.paint instanceof OpaqueColor.SpotColor) {
			OpaqueColor.SpotColor spotcolor = (OpaqueColor.SpotColor) strokespec.paint;
			float[] fs = { 0.0F };
			if (spotcolor.altcolor != null) {
				if (spotcolor.altcolor instanceof OpaqueColor.Grayscale) {
					OpaqueColor.Grayscale grayscale = (OpaqueColor.Grayscale) spotcolor.altcolor;
					fs = new float[] { (float) grayscale.g };
				} else if (spotcolor.altcolor instanceof OpaqueColor.RGB) {
					OpaqueColor.RGB rgb = (OpaqueColor.RGB) spotcolor.altcolor;
					fs = new float[] { (float) rgb.r, (float) rgb.g,
							(float) rgb.b };
				} else if (spotcolor.altcolor instanceof OpaqueColor.CMYK) {
					OpaqueColor.CMYK cmyk = (OpaqueColor.CMYK) spotcolor.altcolor;
					fs = new float[] { (float) cmyk.c, (float) cmyk.m,
							(float) cmyk.y, (float) cmyk.k };
				}
			}
			pdfoutputstream.doc.setSpotColorStroke((float) spotcolor.tint,
					spotcolor.colorant, fs);
		} else if (strokespec.paint instanceof OpaqueColor.Registration) {
			OpaqueColor.Registration registration = (OpaqueColor.Registration) strokespec.paint;
			pdfoutputstream.doc.setSpotColorStroke((float) registration.tint,
					"All", new float[] { 0.0F });
		}
		pdfoutputstream.println(""
				+ PDFOutputStream.pdf_float(strokespec.thickness) + " w ");
		pdfoutputstream.println("" + strokespec.lineCap + " J");
		pdfoutputstream.println("" + strokespec.lineJoin + " j");
		if (strokespec.lineJoin == 0)
			pdfoutputstream.println(""
					+ PDFOutputStream.pdf_float(strokespec.miterLimit) + " M");
		if (strokespec.dashArray == null)
			pdfoutputstream.println("[] 0 d");
		else {
			pdfoutputstream.print("[");
			for (int i = 0; i < strokespec.dashArray.length; i++) {
				if (i > 0)
					pdfoutputstream.print(" ");
				pdfoutputstream.print(""
						+ PDFOutputStream.pdf_float(strokespec.dashArray[i]));
			}
			pdfoutputstream.println("] "
					+ PDFOutputStream.pdf_float(strokespec.dashPhase) + " d");
		}
	}

	private void copyBitmapData(PDFOutputStream pdfoutputstream) {
		BitmapImage bitmapimage = (BitmapImage) image;
		switch (bitmapimage.compressionMethod) {
		case 7:
			pdfoutputstream.println("/Filter /LZWDecode");
			pdfoutputstream.print("/DecodeParms <</EarlyChange 0");
			writePredictor(pdfoutputstream, bitmapimage, true);
			pdfoutputstream.println(">>");
			break;
		case 8:
			pdfoutputstream.println("/Filter /LZWDecode");
			writePredictor(pdfoutputstream, bitmapimage, false);
			break;
		case 2:
			pdfoutputstream.println("/Filter /FlateDecode");
			writePredictor(pdfoutputstream, bitmapimage, false);
			break;
		case 4:
			pdfoutputstream.println("/Filter /CCITTFaxDecode");
			pdfoutputstream.println("/DecodeParms <</K 0 /Columns "
					+ bitmapimage.pxWidth + " /Rows " + bitmapimage.pxHeight
					+ ">>");
			break;
		case 5:
			pdfoutputstream.println("/Filter /CCITTFaxDecode");
			pdfoutputstream.println("/DecodeParms <</K 1 /Columns "
					+ bitmapimage.pxWidth + " /Rows " + bitmapimage.pxHeight
					+ ">>");
			break;
		case 6:
			pdfoutputstream.println("/Filter /CCITTFaxDecode");
			pdfoutputstream.println("/DecodeParms <</K -1 /Columns "
					+ bitmapimage.pxWidth + " /Rows " + bitmapimage.pxHeight
					+ ">>");
			break;
		case 3:
			pdfoutputstream.println("/Filter /DCTDecode");
			break;
		case 1:
			pdfoutputstream.println("/Filter /RunLengthDecode");
			break;
		default:
			expandBitmapData(pdfoutputstream);
			return;
		}
		pdfoutputstream.println("/Length " + length_id.toStringR());
		pdfoutputstream.println(">>");
		pdfoutputstream.println("stream");
		int i = pdfoutputstream.size();
		pdfoutputstream.startCompressedStream();
		try {
			bitmapimage.copyData(pdfoutputstream.getCurrentOutputStream());
		} catch (IOException ioexception) {
			pdfoutputstream.exception(("Image '"
					+ bitmapimage.toDisplayString() + "' is not correct"),
					ioexception);
		} catch (ImageFormatException imageformatexception) {
			pdfoutputstream.exception(("Image '"
					+ bitmapimage.toDisplayString() + "' has broken format"),
					imageformatexception);
		}
		pdfoutputstream.finishCompressedStream();
		length = pdfoutputstream.size() - i;
		pdfoutputstream.println("endstream");
		pdfoutputstream.println("endobj");
	}

	private void expandBitmapData(PDFOutputStream pdfoutputstream) {
		if (pdfoutputstream.compression)
			pdfoutputstream.println("/Filter [/FlateDecode]");
		pdfoutputstream.println("/Length " + length_id.toStringR());
		pdfoutputstream.println(">>");
		pdfoutputstream.println("stream");
		int i = pdfoutputstream.size();
		pdfoutputstream.startStream();
		try {
			((BitmapImage) image).expandData(pdfoutputstream
					.getCurrentOutputStream());
		} catch (IOException ioexception) {
			pdfoutputstream
					.exception(
							("Image '" + image.toDisplayString() + "' cannot be processed"),
							ioexception);
		} catch (ImageFormatException imageformatexception) {
			pdfoutputstream
					.exception(
							("Image '" + image.toDisplayString() + "' has unrecognizable format"),
							imageformatexception);
		}
		pdfoutputstream.finishStream();
		length = pdfoutputstream.size() - i;
		pdfoutputstream.println("endstream");
		pdfoutputstream.println("endobj");
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		if (image != null) {
			pdfoutputstream.printLength(length_id, length);
			if (cspace_id != null) {
				pdfoutputstream.beginStream(cspace_id);
				pdfoutputstream.print(((BitmapImage) image).colorTable);
				pdfoutputstream.endStream();
			}
			if (formResources != null)
				formResources.write(pdfoutputstream);
			Enumeration enumeration = subTrees.keys();
			while (enumeration.hasMoreElements()) {
				String string = (String) enumeration.nextElement();
				ImageTree imagetree = (ImageTree) subTrees.get(string);
				SubTreeVectorImage subtreevectorimage = new SubTreeVectorImage(
						imagetree);
				subtreevectorimage.supportsVectorRendering = true;
				PDFImage pdfimage_10_ = new PDFImage((IndirectObject) subIds
						.get(string), subtreevectorimage, pdfoutputstream);
				pdfimage_10_.write(pdfoutputstream);
			}
			if (image instanceof PDFInstance) {
				try {
					PDFInstance pdfinstance = (PDFInstance) image;
					pdfinstance.open();
					String string = "0";
					Integer integer = new Integer(0);
					try {
						while (!pdfinstance.pendingObjects.isEmpty()) {
							string = (String) pdfinstance.pendingObjects
									.shift();
							integer = ((Integer) pdfinstance.converObjects
									.get(string));
							PDFUniversalObject pdfuniversalobject = pdfinstance
									.getObject(new Integer(string).intValue());
							if (pdfuniversalobject.objdata instanceof PDFStream)
								pdfuniversalobject.objdata = pdfinstance
										.getStream(pdfuniversalobject);
							pdfuniversalobject.setID(integer.intValue());
							pdfuniversalobject.write(pdfoutputstream,
									pdfinstance);
						}
					} catch (ImageFormatException imageformatexception) {
						pdfoutputstream.warning(imageformatexception
								.getMessage());
						PDFUniversalObject pdfuniversalobject = new PDFUniversalObject(
								integer.intValue());
						pdfuniversalobject.objdata = new PDFNumeric("0", true);
						pdfuniversalobject.write(pdfoutputstream, pdfinstance);
					} finally {
						while (!pdfinstance.pendingObjects.isEmpty()) {
							string = (String) pdfinstance.pendingObjects
									.shift();
							integer = ((Integer) pdfinstance.converObjects
									.get(string));
							PDFUniversalObject pdfuniversalobject = new PDFUniversalObject(
									integer.intValue());
							pdfuniversalobject.write(pdfoutputstream,
									pdfinstance);
						}
						pdfinstance.close();
					}
				} catch (IOException ioexception) {
					pdfoutputstream.exception("", ioexception);
				}
			}
		}
	}

	private void writePredictor(PDFOutputStream pdfoutputstream,
			BitmapImage bitmapimage, boolean bool) {
		if (bitmapimage.predictor != 0) {
			pdfoutputstream.print(bool ? " " : "/DecodeParms <<");
			switch (bitmapimage.predictor) {
			case 1:
				pdfoutputstream.print("/Predictor 2");
				break;
			case 2:
				pdfoutputstream.print("/Predictor 15");
				break;
			}
			pdfoutputstream.print(" /Columns " + bitmapimage.pxWidth);
			if (bitmapimage.bitsPerComponent != 8)
				pdfoutputstream.print(" /BitsPerComponent "
						+ bitmapimage.bitsPerComponent);
			switch (bitmapimage.colorSpace) {
			case 1:
				pdfoutputstream.print(" /Colors 3");
				break;
			case 2:
			case 3:
				pdfoutputstream.print(" /Colors 4");
				break;
			}
			if (!bool)
				pdfoutputstream.println(">>");
		}
	}

	private void printMatrix(PDFOutputStream pdfoutputstream, double[] ds) {
		pdfoutputstream.println("" + PDFOutputStream.pdf_float(ds[0]) + " "
				+ PDFOutputStream.pdf_float(ds[1]) + " "
				+ PDFOutputStream.pdf_float(ds[2]) + " "
				+ PDFOutputStream.pdf_float(ds[3]) + " "
				+ PDFOutputStream.pdf_float(ds[4]) + " "
				+ PDFOutputStream.pdf_float(ds[5]) + " cm");
	}

	private double[] calcBBox(double d, double d_11_, double d_12_,
			double d_13_, double[] ds, GraphicGroup graphicgroup) {
		double[] ds_14_ = { 0.0, 0.0, 0.0, 0.0 };
		double[] ds_15_ = graphicgroup.transformCoordinates(ds, d, d_11_);
		if (ds_15_[0] < ds_14_[0])
			ds_14_[0] = ds_15_[0];
		else if (ds_15_[0] > ds_14_[2])
			ds_14_[2] = ds_15_[0];
		if (ds_15_[1] < ds_14_[1])
			ds_14_[1] = ds_15_[1];
		else if (ds_15_[1] > ds_14_[3])
			ds_14_[3] = ds_15_[1];
		ds_15_ = graphicgroup.transformCoordinates(ds, d, d_13_);
		if (ds_15_[0] < ds_14_[0])
			ds_14_[0] = ds_15_[0];
		else if (ds_15_[0] > ds_14_[2])
			ds_14_[2] = ds_15_[0];
		if (ds_15_[1] < ds_14_[1])
			ds_14_[1] = ds_15_[1];
		else if (ds_15_[1] > ds_14_[3])
			ds_14_[3] = ds_15_[1];
		ds_15_ = graphicgroup.transformCoordinates(ds, d_12_, d_13_);
		if (ds_15_[0] < ds_14_[0])
			ds_14_[0] = ds_15_[0];
		else if (ds_15_[0] > ds_14_[2])
			ds_14_[2] = ds_15_[0];
		if (ds_15_[1] < ds_14_[1])
			ds_14_[1] = ds_15_[1];
		else if (ds_15_[1] > ds_14_[3])
			ds_14_[3] = ds_15_[1];
		ds_15_ = graphicgroup.transformCoordinates(ds, d_12_, d_11_);
		if (ds_15_[0] < ds_14_[0])
			ds_14_[0] = ds_15_[0];
		else if (ds_15_[0] > ds_14_[2])
			ds_14_[2] = ds_15_[0];
		if (ds_15_[1] < ds_14_[1])
			ds_14_[1] = ds_15_[1];
		else if (ds_15_[1] > ds_14_[3])
			ds_14_[3] = ds_15_[1];
		return ds_14_;
	}
}