/*
 * PDFFont - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.util.StringTokenizer;

import com.renderx.fonts.AFMMetric;
import com.renderx.fonts.FontDescriptor;
import com.renderx.fonts.Metric;

public abstract class PDFFont extends PDFObject {
	static final int FIXEDWIDTH = 1;

	static final int SERIF = 2;

	static final int SYMBOL = 4;

	static final int SCRIPT = 8;

	static final int ADOBESTANDARD = 32;

	static final int ITALIC = 64;

	static final int SMALLCAPS = 131072;

	static final int FORCEBOLD = 262144;

	static final int LAST_MODE = 7;

	boolean enableSubset;

	FontDescriptor descriptor;

	int encoding;

	String fontName;

	String subsetName;

	String getType() {
		return "Font";
	}

	abstract String getSubType();

	public PDFFont(IndirectObject indirectobject, FontDescriptor fontdescriptor) {
		super(indirectobject);
		descriptor = fontdescriptor;
		StringTokenizer stringtokenizer = new StringTokenizer(
				fontdescriptor.record.getMetric().fontName);
		fontName = "";
		while (stringtokenizer.hasMoreTokens()) {
			StringBuffer stringbuffer = new StringBuffer();
			PDFFont pdffont_0_ = this;
			pdffont_0_.fontName = stringbuffer.append(pdffont_0_.fontName)
					.append(stringtokenizer.nextToken()).toString();
		}
		subsetName = "";
		enableSubset = true;
	}

	void setEnableSubset(boolean bool) {
		enableSubset = bool;
	}

	boolean getEnableSubset() {
		return enableSubset;
	}

	void writeBody(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.println("/Subtype/" + getSubType());
		writeAddInfo(pdfoutputstream);
		pdfoutputstream.println("/BaseFont/" + subsetName + fontName);
	}

	void subsetName() {
		subsetName = "";
		int i = 1193046;
		for (int i_1_ = 0; i_1_ < 6; i_1_++) {
			int i_2_ = i >> 4 * i_1_ & 0xf;
			StringBuffer stringbuffer = new StringBuffer();
			PDFFont pdffont_3_ = this;
			pdffont_3_.subsetName = stringbuffer.append(pdffont_3_.subsetName)
					.append((char) (65 + i_2_)).toString();
		}
		StringBuffer stringbuffer = new StringBuffer();
		PDFFont pdffont_4_ = this;
		pdffont_4_.subsetName = stringbuffer.append(pdffont_4_.subsetName)
				.append("+").toString();
	}

	int make_fontflags() {
		Metric metric = descriptor.record.getMetric();
		int i = 0;
		if (metric.isFixedPitch)
			i |= 0x1;
		if (metric instanceof AFMMetric
				&& ((AFMMetric) metric).encodingScheme
						.equals("AdobeStandardEncoding"))
			i |= 0x20;
		else
			i |= 0x4;
		if (metric.italicAngle < 0.0F)
			i |= 0x40;
		if (metric.weight != null && metric.weight.equalsIgnoreCase("Bold"))
			i |= 0x40000;
		return i;
	}

	abstract void writeAddInfo(PDFOutputStream pdfoutputstream);

	abstract void writeSubObjects(PDFOutputStream pdfoutputstream);
}