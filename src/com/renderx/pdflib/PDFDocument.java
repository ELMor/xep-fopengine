/*
 * PDFDocument - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.util.Enumeration;

import com.renderx.fonts.FontCatalog;
import com.renderx.fonts.FontConfigurationException;
import com.renderx.fonts.FontDescriptor;
import com.renderx.fonts.FontRecord;
import com.renderx.fonts.U28;
import com.renderx.graphics.Image;
import com.renderx.graphics.ImageFactory;
import com.renderx.graphics.ImageFormatException;
import com.renderx.graphics.UnregisteredMIMETypeException;
import com.renderx.sax.Serializer;
import com.renderx.sax.XMLReaderFactory;
import com.renderx.util.Base64OutputStream;
import com.renderx.util.DefaultErrorHandler;
import com.renderx.util.ErrorHandler;
import com.renderx.util.Hashtable;
import com.renderx.util.List;
import com.renderx.util.URLCache;
import com.renderx.util.URLSpec;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.AttributesImpl;

public class PDFDocument {
	public static final String DEFAULT_PDF_VERSION = "1.4";

	int NONE = 0;

	int FILL_WINDING = 0;

	int FILL_EVENODD = 1;

	int fillRule;

	PDFOutputStream outputPDF;

	boolean printText;

	boolean initializationFinished;

	boolean dropUnusedDestinations;

	Page firstPage;

	Page currentPage;

	PDFImage currentImage;

	Links links;

	Fonts fonts;

	Images images;

	SpotColors spotColors;

	ExtGStates gStates;

	private float text_x;

	private float text_y;

	private float hscale = 1.0F;

	private float wspace = 0.0F;

	private float cspace = 0.0F;

	private boolean newpos = true;

	private float fontSize = 0.0F;

	private float fontSlant = 0.0F;

	boolean unsupportedOpacityWarning = false;

	private Hashtable destStorage = null;

	private Hashtable usedDestinations = null;

	String initialDestination = null;

	String initialZoom = null;

	String viewMode = null;

	String pdfVersion = "1.4";

	private final Hashtable pageboundaries = new Hashtable();

	private Hashtable currentPageBoundaries = null;

	private final List printermarks = new List();

	private List currentPrinterMarks = null;

	final ImageFactory imageFactory;

	final FontCatalog fontCatalog;

	final ErrorHandler errorHandler;

	final U28 u28;

	class ImageSizeSetterHandler implements ContentHandler {
		private final ContentHandler parent;

		private final String height;

		private final String width;

		boolean firstElement = true;

		ImageSizeSetterHandler(ContentHandler contenthandler, String string,
				String string_0_) {
			parent = contenthandler;
			height = string_0_;
			width = string;
		}

		public void startDocument() throws SAXException {
			parent.startDocument();
			AttributesImpl attributesimpl = new AttributesImpl();
			parent.startPrefixMapping("svg", "http://www.w3.org/2000/svg");
			attributesimpl.addAttribute("", "width", "width", "CDATA", width);
			attributesimpl
					.addAttribute("", "height", "height", "CDATA", height);
			parent.startElement("http://www.w3.org/2000/svg", "svg", "svg:svg",
					attributesimpl);
		}

		public void endDocument() throws SAXException {
			parent.endElement("http://www.w3.org/2000/svg", "svg", "svg:svg");
			parent.endPrefixMapping("svg");
			parent.endDocument();
		}

		public void setDocumentLocator(Locator locator) {
			parent.setDocumentLocator(locator);
		}

		public void startPrefixMapping(String string, String string_1_)
				throws SAXException {
			parent.startPrefixMapping(string, string_1_);
		}

		public void endPrefixMapping(String string) throws SAXException {
			parent.endPrefixMapping(string);
		}

		public void startElement(String string, String string_2_,
				String string_3_, Attributes attributes) throws SAXException {
			parent.startElement(string, string_2_, string_3_, attributes);
		}

		public void endElement(String string, String string_4_, String string_5_)
				throws SAXException {
			parent.endElement(string, string_4_, string_5_);
		}

		public void characters(char[] cs, int i, int i_6_) throws SAXException {
			parent.characters(cs, i, i_6_);
		}

		public void ignorableWhitespace(char[] cs, int i, int i_7_)
				throws SAXException {
			parent.ignorableWhitespace(cs, i, i_7_);
		}

		public void processingInstruction(String string, String string_8_)
				throws SAXException {
			parent.processingInstruction(string, string_8_);
		}

		public void skippedEntity(String string) throws SAXException {
			parent.skippedEntity(string);
		}
	}

	protected class PDFU28 extends U28 {
		PDFU28() {
			super(fontCatalog);
		}

		protected void flushTextBuffer() {
			if (itxt != 0) {
				if (!fontSpecified) {
					if (isOneByteText(curFont.record))
						setFont(curFont, fontSize, fontEncoding);
					else
						setFont(curFont, fontSize);
					fontSpecified = true;
				}
				String string = new String(txt, 0, itxt);
				itxt = 0;
				if (newpos) {
					showXY(string, text_x, text_y, hscale);
					newpos = false;
				} else
					show(string);
			}
		}

		protected boolean isOneByteText(FontRecord fontrecord) {
			switch (fontrecord.datatype) {
			case 2:
				if (fontrecord.embed)
					return false;
				return true;
			case 1:
				return true;
			case 3:
				return true;
			case 4:
				return false;
			default:
				throw new RuntimeException("Invalid data type in font record");
			}
		}

		protected boolean isProcessAsUnicode(FontRecord fontrecord) {
			return false;
		}
	}

	class NamedDestRecord {
		IndirectObject page;

		float x;

		float y;

		NamedDestRecord(IndirectObject indirectobject, float f, float f_9_) {
			page = indirectobject;
			x = f;
			y = f_9_;
		}
	}

	public void setInitialDestination(String string) {
		initialDestination = string;
	}

	public void setInitialZoom(String string) {
		initialZoom = string;
	}

	public void setViewMode(String string) {
		viewMode = string;
	}

	public void setPDFVersion(String string) {
		int i = Integer.valueOf(string.substring(0, "1.4".indexOf('.')))
				.intValue();
		int i_10_ = Integer.valueOf(string.substring("1.4".indexOf('.') + 1))
				.intValue();
		int i_11_ = string.indexOf('.');
		try {
			i = Integer.valueOf(string.substring(0, i_11_)).intValue();
			i_10_ = Integer.valueOf(string.substring(i_11_ + 1)).intValue();
		} catch (Exception exception) {
			outputPDF.warning("Unrecognized PDF Version number: " + string
					+ "; revert to default value: " + "1.4" + ".");
			return;
		}
		if (i == 1 && i_10_ < 3) {
			outputPDF.warning("Unsupported PDF Version: " + string
					+ "; revert to the lowest possible value: 1.3.");
			pdfVersion = "1.3";
		} else
			pdfVersion = String.valueOf(i).concat(".").concat(
					String.valueOf(i_10_));
	}

	public int getPDFVersion_major() {
		int i = pdfVersion.indexOf('.');
		return Integer.valueOf(pdfVersion.substring(0, i)).intValue();
	}

	public int getPDFVersion_minor() {
		int i = pdfVersion.indexOf('.');
		return Integer.valueOf(pdfVersion.substring(i + 1)).intValue();
	}

	public void setPageBoundaryEntry(String string, Object object) {
		currentPageBoundaries.put(string, object);
	}

	public void setPrinterMarkEntry(String string) {
		currentPrinterMarks.append(string);
	}

	protected PDFDocument(String string) throws IOException,
			FontConfigurationException {
		this(new FileOutputStream(string));
	}

	protected PDFDocument(OutputStream outputstream) throws IOException,
			FontConfigurationException {
		this(outputstream, new URLCache(), new DefaultErrorHandler());
	}

	private PDFDocument(OutputStream outputstream, URLCache urlcache,
			ErrorHandler errorhandler) throws IOException,
			FontConfigurationException {
		this(outputstream, new FontCatalog(urlcache, errorhandler), urlcache,
				errorhandler);
	}

	private PDFDocument(OutputStream outputstream, FontCatalog fontcatalog,
			URLCache urlcache, ErrorHandler errorhandler) throws IOException {
		this(outputstream,
				new ImageFactory(fontcatalog, urlcache, errorhandler),
				fontcatalog, errorhandler);
	}

	public PDFDocument(String string, ImageFactory imagefactory,
			FontCatalog fontcatalog, ErrorHandler errorhandler)
			throws IOException {
		this(new FileOutputStream(string), imagefactory, fontcatalog,
				errorhandler);
	}

	public PDFDocument(OutputStream outputstream, ImageFactory imagefactory,
			FontCatalog fontcatalog, ErrorHandler errorhandler)
			throws IOException {
		fontCatalog = fontcatalog;
		imageFactory = imagefactory;
		errorHandler = errorhandler;
		outputPDF = new PDFOutputStream(outputstream, this);
		outputPDF.lastObjectNumber = 1;
		initializationFinished = false;
		printText = true;
		outputPDF.trailer = new PDFTrailer();
		outputPDF.trailer.catalog = new Catalog(new IndirectObject(
				outputPDF.lastObjectNumber++));
		outputPDF.trailer.catalog.pages = new Pages(new IndirectObject(
				outputPDF.lastObjectNumber++));
		outputPDF.trailer.info = new Info(new IndirectObject(
				outputPDF.lastObjectNumber++));
		links = new Links();
		fonts = new Fonts();
		images = new Images();
		spotColors = new SpotColors();
		gStates = new ExtGStates();
		outputPDF.compression = true;
		firstPage = null;
		currentPage = null;
		currentImage = null;
		currentPageBoundaries = pageboundaries;
		currentPrinterMarks = printermarks;
		u28 = new PDFU28();
	}

	public void startDocument() {
		if (!initializationFinished) {
			if (PDFOutputStream.dump != null)
				PDFOutputStream.dump.println("<pdf compression=\""
						+ outputPDF.compression + "\"" + "linearization=\""
						+ outputPDF.getLinearize() + "\"" + "encryption=\""
						+ outputPDF.getUseEncryption() + "\"" + "/>");
			outputPDF.println("%PDF-" + pdfVersion);
			outputPDF.println("%\u00e2\u00e3\u00cf\u00d3");
			initializationFinished = true;
		} else
			errorHandler.error("Document already started!");
	}

	public void setTMPDIR(File file) {
		if (file != null) {
			if (!file.exists()) {
				errorHandler
						.error("Directory "
								+ file
								+ " does not exist; disk caching in PDF generator disabled.");
				file = null;
			} else if (!file.isDirectory()) {
				errorHandler
						.error("File "
								+ file
								+ " is not a directory; disk caching in PDF generator disabled.");
				file = null;
			} else if (!file.canWrite()) {
				errorHandler
						.error("Directory "
								+ file
								+ " is not writable; disk caching in PDF generator disabled.");
				file = null;
			}
		}
		outputPDF.tmpdir = file;
		outputPDF.USE_TEMP_FILE = file != null;
	}

	public void setCompression(boolean bool) {
		outputPDF.compression = bool;
	}

	public void setLinearize(boolean bool) {
		outputPDF.setLinearize(bool);
	}

	public void setUnicodeAnnotations(boolean bool) {
		outputPDF.unicodeAnnotations = bool;
	}

	public void setDestinationTreatment(boolean bool) {
		dropUnusedDestinations = bool;
		if (bool) {
			destStorage = new Hashtable();
			usedDestinations = new Hashtable();
		} else {
			destStorage = null;
			usedDestinations = null;
		}
	}

	public void setEncryption(String string, String string_12_, int i) {
		outputPDF.setUseEncryption(string, string_12_, i);
	}

	public void setInfo(String string, String string_13_) {
		outputPDF.trailer.info.set(string, string_13_);
	}

	public void beginPage(float f, float f_14_, String string) {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<page>");
		currentImage = null;
		currentPage = new Page(
				new IndirectObject(outputPDF.lastObjectNumber++), f, f_14_,
				string);
		currentPage
				.addContentID(new IndirectObject(outputPDF.lastObjectNumber));
		outputPDF.lastObjectNumber = (outputPDF.beginStream(new IndirectObject(
				outputPDF.lastObjectNumber)).num);
		currentPage.addResources(new IndirectObject(
				outputPDF.lastObjectNumber++));
		if (firstPage == null)
			firstPage = currentPage;
		currentPageBoundaries = currentPage.pageboundaries;
		currentPrinterMarks = currentPage.printermarks;
		if (pageboundaries.containsKey("CROP_OFFSET"))
			setPageBoundaryEntry("CROP_OFFSET", (float[]) pageboundaries
					.get("CROP_OFFSET"));
		if (pageboundaries.containsKey("BLEED"))
			setPageBoundaryEntry("BLEED", (float[]) pageboundaries.get("BLEED"));
		if (pageboundaries.containsKey("CROP_MARK_WIDTH"))
			setPageBoundaryEntry("CROP_MARK_WIDTH", ((String) pageboundaries
					.get("CROP_MARK_WIDTH")));
		if (pageboundaries.containsKey("BLEED_MARK_WIDTH"))
			setPageBoundaryEntry("BLEED_MARK_WIDTH", ((String) pageboundaries
					.get("BLEED_MARK_WIDTH")));
	}

	public void endPage() {
		restore();
		float[] fs = currentPage.getBleeds();
		float[] fs_15_ = currentPage.getCropOffsets();
		float f = currentPage.getCropMarkWidth();
		float f_16_ = currentPage.getBleedMarkWidth();
		save();
		PDFOutputStream pdfoutputstream = outputPDF;
		StringBuffer stringbuffer = new StringBuffer();
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_17_ = stringbuffer.append(
				PDFOutputStream.pdf_float((double) -fs_15_[0])).append(" ");
		if (this != null) {
			/* empty */
		}
		pdfoutputstream.println(stringbuffer_17_.append(
				PDFOutputStream.pdf_float((double) -fs_15_[1])).append(" m")
				.toString());
		PDFOutputStream pdfoutputstream_18_ = outputPDF;
		StringBuffer stringbuffer_19_ = new StringBuffer();
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_20_ = stringbuffer_19_.append(
				PDFOutputStream.pdf_float((double) -fs_15_[0])).append(" ");
		if (this != null) {
			/* empty */
		}
		pdfoutputstream_18_.println(stringbuffer_20_.append(
				PDFOutputStream
						.pdf_float((double) (currentPage.height + fs_15_[3])))
				.append(" l").toString());
		PDFOutputStream pdfoutputstream_21_ = outputPDF;
		StringBuffer stringbuffer_22_ = new StringBuffer();
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_23_ = stringbuffer_22_.append(
				PDFOutputStream
						.pdf_float((double) (currentPage.width + fs_15_[2])))
				.append(" ");
		if (this != null) {
			/* empty */
		}
		pdfoutputstream_21_.println(stringbuffer_23_.append(
				PDFOutputStream
						.pdf_float((double) (currentPage.height + fs_15_[3])))
				.append(" l").toString());
		PDFOutputStream pdfoutputstream_24_ = outputPDF;
		StringBuffer stringbuffer_25_ = new StringBuffer();
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_26_ = stringbuffer_25_.append(
				PDFOutputStream
						.pdf_float((double) (currentPage.width + fs_15_[2])))
				.append(" ");
		if (this != null) {
			/* empty */
		}
		pdfoutputstream_24_.println(stringbuffer_26_.append(
				PDFOutputStream.pdf_float((double) -fs_15_[1])).append(" l")
				.toString());
		outputPDF.println("h");
		PDFOutputStream pdfoutputstream_27_ = outputPDF;
		StringBuffer stringbuffer_28_ = new StringBuffer();
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_29_ = stringbuffer_28_.append(
				PDFOutputStream.pdf_float((double) -fs[0])).append(" ");
		if (this != null) {
			/* empty */
		}
		pdfoutputstream_27_.println(stringbuffer_29_.append(
				PDFOutputStream.pdf_float((double) -fs[1])).append(" m")
				.toString());
		PDFOutputStream pdfoutputstream_30_ = outputPDF;
		StringBuffer stringbuffer_31_ = new StringBuffer();
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_32_ = stringbuffer_31_
				.append(
						PDFOutputStream
								.pdf_float((double) (currentPage.width + fs[2])))
				.append(" ");
		if (this != null) {
			/* empty */
		}
		pdfoutputstream_30_.println(stringbuffer_32_.append(
				PDFOutputStream.pdf_float((double) -fs[1])).append(" l")
				.toString());
		PDFOutputStream pdfoutputstream_33_ = outputPDF;
		StringBuffer stringbuffer_34_ = new StringBuffer();
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_35_ = stringbuffer_34_
				.append(
						PDFOutputStream
								.pdf_float((double) (currentPage.width + fs[2])))
				.append(" ");
		if (this != null) {
			/* empty */
		}
		pdfoutputstream_33_.println(stringbuffer_35_.append(
				PDFOutputStream
						.pdf_float((double) (currentPage.height + fs[3])))
				.append(" l").toString());
		PDFOutputStream pdfoutputstream_36_ = outputPDF;
		StringBuffer stringbuffer_37_ = new StringBuffer();
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_38_ = stringbuffer_37_.append(
				PDFOutputStream.pdf_float((double) -fs[0])).append(" ");
		if (this != null) {
			/* empty */
		}
		pdfoutputstream_36_.println(stringbuffer_38_.append(
				PDFOutputStream
						.pdf_float((double) (currentPage.height + fs[3])))
				.append(" l").toString());
		outputPDF.println("h");
		outputPDF.println("W n");
		if (f != 0.0F || f_16_ != 0.0F) {
			if (f != 0.0F) {
				float[] fs_39_ = { 0.0F, 0.0F, 0.0F, 0.0F };
				drawMarks(fs_15_, fs_39_, f, currentPage.width,
						currentPage.height);
			}
			if (f_16_ != 0.0F)
				drawMarks(fs_15_, fs, f_16_, currentPage.width,
						currentPage.height);
		}
		Enumeration enumeration = printermarks.elements();
		if (!currentPage.printermarks.isEmpty())
			enumeration = currentPage.printermarks.elements();
		while (enumeration.hasMoreElements()) {
			String string = (String) enumeration.nextElement();
			Object object = null;
			XMLReader xmlreader;
			try {
				xmlreader = XMLReaderFactory.createXMLReader();
			} catch (SAXException saxexception) {
				outputPDF.error("Cannot create XML reader: "
						+ saxexception.getMessage());
				continue;
			}
			StringWriter stringwriter = new StringWriter();
			Base64OutputStream base64outputstream = new Base64OutputStream(
					stringwriter);
			try {
				StringBuffer stringbuffer_40_ = new StringBuffer();
				if (this != null) {
					/* empty */
				}
				String string_41_ = stringbuffer_40_.append(
						PDFOutputStream.pdf_float((double) ((currentPage.width)
								+ fs_15_[0] + fs_15_[2]))).append("pt")
						.toString();
				StringBuffer stringbuffer_42_ = new StringBuffer();
				if (this != null) {
					/* empty */
				}
				String string_43_ = stringbuffer_42_.append(
						PDFOutputStream
								.pdf_float((double) ((currentPage.height)
										+ fs_15_[1] + fs_15_[3]))).append("pt")
						.toString();
				xmlreader.setContentHandler(new ImageSizeSetterHandler(
						new Serializer(base64outputstream), string_41_,
						string_43_));
			} catch (IOException ioexception) {
				outputPDF
						.error("Cannot constructs serializer from an output stream: "
								+ ioexception.getMessage());
				continue;
			}
			stringwriter.write("data:image/svg+xml;base64,");
			try {
				xmlreader.parse(string);
				base64outputstream.close();
				stringwriter.close();
			} catch (IOException ioexception) {
				outputPDF.warning("Invalid URL or non-existent file: " + string
						+ "; setting ignored");
				continue;
			} catch (SAXException saxexception) {
				outputPDF.error("Cannot parse a document: "
						+ saxexception.getMessage());
				continue;
			}
			String string_44_ = stringwriter.toString();
			try {
				save();
				transform(1.0F, 0.0F, 0.0F, 1.0F, -fs_15_[0], -fs_15_[1]);
				placeImage(new URLSpec(string_44_), "image/svg+xml");
				restore();
			} catch (MalformedURLException malformedurlexception) {
				outputPDF.warning("Invalid URL or non-existent file: "
						+ string_44_);
			} catch (PDFExternalFileException pdfexternalfileexception) {
				outputPDF.warning("Cannot insert image: "
						+ pdfexternalfileexception.getMessage());
			}
		}
		restore();
		outputPDF.endStream();
		outputPDF.trailer.catalog.pages.addPage(currentPage);
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("</page>");
		currentPage = null;
		currentPageBoundaries = pageboundaries;
		currentPrinterMarks = printermarks;
	}

	public void drawMarks(float[] fs, float[] fs_45_, float f, float f_46_,
			float f_47_) {
		save();
		setRGBColor(0.0F, 0.0F, 0.0F);
		setLineWidth(f);
		moveTo(-fs_45_[0], -fs_45_[1]);
		lineTo(-fs[0], -fs_45_[1]);
		stroke();
		restore();
		save();
		setRGBColor(0.0F, 0.0F, 0.0F);
		setLineWidth(f);
		moveTo(-fs_45_[0], -fs_45_[1]);
		lineTo(-fs_45_[0], -fs[1]);
		stroke();
		restore();
		save();
		setRGBColor(0.0F, 0.0F, 0.0F);
		setLineWidth(f);
		moveTo(-fs_45_[0], f_47_ + fs_45_[3]);
		lineTo(-fs[0], f_47_ + fs_45_[3]);
		stroke();
		restore();
		save();
		setRGBColor(0.0F, 0.0F, 0.0F);
		setLineWidth(f);
		moveTo(-fs_45_[0], f_47_ + fs_45_[3]);
		lineTo(-fs_45_[0], f_47_ + fs[3]);
		stroke();
		restore();
		save();
		setRGBColor(0.0F, 0.0F, 0.0F);
		setLineWidth(f);
		moveTo(f_46_ + fs_45_[2], f_47_ + fs_45_[3]);
		lineTo(f_46_ + fs[2], f_47_ + fs_45_[3]);
		stroke();
		restore();
		save();
		setRGBColor(0.0F, 0.0F, 0.0F);
		setLineWidth(f);
		moveTo(f_46_ + fs_45_[2], f_47_ + fs_45_[3]);
		lineTo(f_46_ + fs_45_[2], f_47_ + fs[3]);
		stroke();
		restore();
		save();
		setRGBColor(0.0F, 0.0F, 0.0F);
		setLineWidth(f);
		moveTo(f_46_ + fs_45_[2], -fs_45_[1]);
		lineTo(f_46_ + fs[2], -fs_45_[1]);
		stroke();
		restore();
		save();
		setRGBColor(0.0F, 0.0F, 0.0F);
		setLineWidth(f);
		moveTo(f_46_ + fs_45_[2], -fs_45_[1]);
		lineTo(f_46_ + fs_45_[2], -fs[1]);
		stroke();
		restore();
	}

	public void endDocument() {
		if (outputPDF.trailer.catalog.pages.getPagesCount() == -1) {
			beginPage(100.0F, 100.0F, "1");
			endPage();
		}
		outputPDF.trailer.info.write(outputPDF);
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<writeimages>");
		images.write(outputPDF);
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("</writeimages>");
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<writefonts>");
		if (u28.fontStateTable != null)
			fonts.write(outputPDF, u28.fontStateTable);
		else
			fonts.write(outputPDF);
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("</writefonts>");
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<writespotcolors>");
		spotColors.write(outputPDF);
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("</writespotcolors>");
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<writeextgstates>");
		gStates.write(outputPDF);
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("</writeextgstates>");
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<writelinks>");
		links.write(outputPDF, outputPDF.trailer.catalog.pages);
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("</writelinks>");
		if (dropUnusedDestinations) {
			Enumeration enumeration = usedDestinations.keys();
			while (enumeration.hasMoreElements()) {
				String string = (String) enumeration.nextElement();
				NamedDestRecord nameddestrecord = (NamedDestRecord) destStorage
						.get(string);
				if (nameddestrecord != null)
					addNamedDestination(string, nameddestrecord.x,
							nameddestrecord.y, nameddestrecord.page);
			}
		}
		if (viewMode != null) {
			try {
				outputPDF.trailer.catalog.setViewMode(viewMode);
			} catch (PDFException pdfexception) {
				errorHandler.warning(pdfexception.getMessage()
						+ "; parameter ignored");
			}
		}
		NamedDestRecord nameddestrecord = null;
		if (initialDestination != null) {
			nameddestrecord = (NamedDestRecord) destStorage
					.get(initialDestination);
			if (nameddestrecord == null)
				errorHandler
						.warning("Initial destination '" + initialDestination
								+ "' not found; parameter ignored");
		}
		if (nameddestrecord != null || initialZoom != null) {
			if (nameddestrecord == null)
				nameddestrecord = new NamedDestRecord(firstPage.getID(), 0.0F,
						firstPage.height);
			try {
				outputPDF.trailer.catalog.setOpenAction(nameddestrecord.page,
						nameddestrecord.x, nameddestrecord.y, initialZoom);
			} catch (PDFException pdfexception) {
				errorHandler.warning(pdfexception.getMessage()
						+ "; parameter ignored");
			}
		}
		outputPDF.trailer.catalog.write(outputPDF);
		if (outputPDF.trailer.catalog.outlines != null) {
			if (PDFOutputStream.dump != null)
				PDFOutputStream.dump.println("<writeoutlines>");
			outputPDF.trailer.catalog.outlines
					.finish(outputPDF.trailer.catalog.pages);
			outputPDF.trailer.catalog.outlines.write(outputPDF);
			if (PDFOutputStream.dump != null)
				PDFOutputStream.dump.println("</writeoutlines>");
		}
		if (outputPDF.trailer.catalog.names != null) {
			if (PDFOutputStream.dump != null)
				PDFOutputStream.dump.println("<writenames>");
			outputPDF.trailer.catalog.names.write(outputPDF);
			if (PDFOutputStream.dump != null)
				PDFOutputStream.dump.println("</writenames>");
		}
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<writetail>");
		printTail();
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("</writetail>");
		outputPDF.finish();
		initializationFinished = false;
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("</pdf>");
		destStorage = null;
		usedDestinations = null;
	}

	public void transform(float f, float f_48_, float f_49_, float f_50_,
			float f_51_, float f_52_) {
		outputPDF.endText();
		PDFOutputStream pdfoutputstream = outputPDF;
		StringBuffer stringbuffer = new StringBuffer();
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_53_ = stringbuffer.append(
				PDFOutputStream.pdf_float((double) f)).append(" ");
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_54_ = stringbuffer_53_.append(
				PDFOutputStream.pdf_float((double) f_48_)).append(" ");
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_55_ = stringbuffer_54_.append(
				PDFOutputStream.pdf_float((double) f_49_)).append(" ");
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_56_ = stringbuffer_55_.append(
				PDFOutputStream.pdf_float((double) f_50_)).append(" ");
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_57_ = stringbuffer_56_.append(
				PDFOutputStream.pdf_float((double) f_51_)).append(" ");
		if (this != null) {
			/* empty */
		}
		pdfoutputstream.println(stringbuffer_57_.append(
				PDFOutputStream.pdf_float((double) f_52_)).append(" cm")
				.toString());
	}

	public void scale(float f, float f_58_) {
		transform(f, 0.0F, 0.0F, f_58_, 0.0F, 0.0F);
	}

	public void rotate(float f) {
		if (f != 0.0F) {
			double d = (double) f * 3.141592653589793 / 180.0;
			float f_59_ = (float) Math.cos(d);
			float f_60_ = (float) Math.sin(d);
			transform(f_59_, f_60_, -f_60_, f_59_, 0.0F, 0.0F);
		}
	}

	public void translate(float f, float f_61_) {
		transform(1.0F, 0.0F, 0.0F, 1.0F, f, f_61_);
	}

	public void moveTo(float f, float f_62_) {
		outputPDF.endText();
		PDFOutputStream pdfoutputstream = outputPDF;
		StringBuffer stringbuffer = new StringBuffer();
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_63_ = stringbuffer.append(
				PDFOutputStream.pdf_float((double) f)).append(" ");
		if (this != null) {
			/* empty */
		}
		pdfoutputstream.println(stringbuffer_63_.append(
				PDFOutputStream.pdf_float((double) f_62_)).append(" m")
				.toString());
	}

	public void lineTo(float f, float f_64_) {
		outputPDF.endText();
		PDFOutputStream pdfoutputstream = outputPDF;
		StringBuffer stringbuffer = new StringBuffer();
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_65_ = stringbuffer.append(
				PDFOutputStream.pdf_float((double) f)).append(" ");
		if (this != null) {
			/* empty */
		}
		pdfoutputstream.println(stringbuffer_65_.append(
				PDFOutputStream.pdf_float((double) f_64_)).append(" l")
				.toString());
	}

	public void curveTo(float f, float f_66_, float f_67_, float f_68_,
			float f_69_, float f_70_) {
		outputPDF.endText();
		if (f_67_ == f_69_ && f_68_ == f_70_) {
			PDFOutputStream pdfoutputstream = outputPDF;
			StringBuffer stringbuffer = new StringBuffer();
			if (this != null) {
				/* empty */
			}
			StringBuffer stringbuffer_71_ = stringbuffer.append(
					PDFOutputStream.pdf_float((double) f)).append(" ");
			if (this != null) {
				/* empty */
			}
			StringBuffer stringbuffer_72_ = stringbuffer_71_.append(
					PDFOutputStream.pdf_float((double) f_66_)).append(" ");
			if (this != null) {
				/* empty */
			}
			StringBuffer stringbuffer_73_ = stringbuffer_72_.append(
					PDFOutputStream.pdf_float((double) f_69_)).append(" ");
			if (this != null) {
				/* empty */
			}
			pdfoutputstream.println(stringbuffer_73_.append(
					PDFOutputStream.pdf_float((double) f_70_)).append(" y")
					.toString());
		} else {
			PDFOutputStream pdfoutputstream = outputPDF;
			StringBuffer stringbuffer = new StringBuffer();
			if (this != null) {
				/* empty */
			}
			StringBuffer stringbuffer_74_ = stringbuffer.append(
					PDFOutputStream.pdf_float((double) f)).append(" ");
			if (this != null) {
				/* empty */
			}
			StringBuffer stringbuffer_75_ = stringbuffer_74_.append(
					PDFOutputStream.pdf_float((double) f_66_)).append(" ");
			if (this != null) {
				/* empty */
			}
			StringBuffer stringbuffer_76_ = stringbuffer_75_.append(
					PDFOutputStream.pdf_float((double) f_67_)).append(" ");
			if (this != null) {
				/* empty */
			}
			StringBuffer stringbuffer_77_ = stringbuffer_76_.append(
					PDFOutputStream.pdf_float((double) f_68_)).append(" ");
			if (this != null) {
				/* empty */
			}
			StringBuffer stringbuffer_78_ = stringbuffer_77_.append(
					PDFOutputStream.pdf_float((double) f_69_)).append(" ");
			if (this != null) {
				/* empty */
			}
			pdfoutputstream.println(stringbuffer_78_.append(
					PDFOutputStream.pdf_float((double) f_70_)).append(" c")
					.toString());
		}
	}

	public void rectangle(float f, float f_79_, float f_80_, float f_81_) {
		outputPDF.endText();
		PDFOutputStream pdfoutputstream = outputPDF;
		StringBuffer stringbuffer = new StringBuffer();
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_82_ = stringbuffer.append(
				PDFOutputStream.pdf_float((double) f)).append(" ");
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_83_ = stringbuffer_82_.append(
				PDFOutputStream.pdf_float((double) f_79_)).append(" ");
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_84_ = stringbuffer_83_.append(
				PDFOutputStream.pdf_float((double) f_80_)).append(" ");
		if (this != null) {
			/* empty */
		}
		pdfoutputstream.println(stringbuffer_84_.append(
				PDFOutputStream.pdf_float((double) f_81_)).append(" re")
				.toString());
	}

	public void setFont(FontDescriptor fontdescriptor, float f) {
		setFont(fontdescriptor, f, NONE);
	}

	public void setFont(FontDescriptor fontdescriptor, float f, int i) {
		fontSize = f;
		if (f != 0.0F) {
			fontSlant = (float) fontdescriptor.record.extraSlantAngle;
			outputPDF.startText();
			String string = fonts.getPDFFontName(fontdescriptor, i);
			if (string == null) {
				fonts.addFont(fontdescriptor, i, new IndirectObject(
						outputPDF.lastObjectNumber++));
				string = fonts.getPDFFontName(fontdescriptor, i);
			}
			if (PDFOutputStream.dump != null)
				PDFOutputStream.dump.println("<font name=\"" + string + "\"/>");
			PDFOutputStream pdfoutputstream = outputPDF;
			StringBuffer stringbuffer = new StringBuffer().append("/").append(
					string).append(" ");
			if (this != null) {
				/* empty */
			}
			pdfoutputstream.println(stringbuffer.append(
					PDFOutputStream.pdf_float((double) f)).append(" Tf")
					.toString());
			if (currentPage != null)
				currentPage
						.addFont(string, fonts.getFontOID(fontdescriptor, i));
			else if (currentImage != null && currentImage.formResources != null)
				currentImage.formResources.addFont("/" + string + " "
						+ fonts.getFontOID(fontdescriptor, i).toStringR());
		}
	}

	public void setHorzScale(float f) {
		if (f <= 0.0F) {
			errorHandler.warning("Wrong font stretch: " + f + "!");
			f = 1.0F;
		}
		hscale = f;
		newpos = true;
		if (cspace != 0.0F)
			resetCharSpacing();
		if (wspace != 0.0F)
			resetWordSpacing();
	}

	public void setTextPos(float f, float f_85_) {
		text_x = f;
		text_y = f_85_;
		newpos = true;
	}

	public void setCharSpacing(float f) {
		cspace = f;
		resetCharSpacing();
	}

	private void resetCharSpacing() {
		PDFOutputStream pdfoutputstream = outputPDF;
		StringBuffer stringbuffer = new StringBuffer();
		if (this != null) {
			/* empty */
		}
		pdfoutputstream.println(stringbuffer.append(
				PDFOutputStream.pdf_float((double) (cspace / hscale))).append(
				" Tc").toString());
	}

	public void setWordSpacing(float f) {
		wspace = f;
		resetWordSpacing();
	}

	public void resetWordSpacing() {
		PDFOutputStream pdfoutputstream = outputPDF;
		StringBuffer stringbuffer = new StringBuffer();
		if (this != null) {
			/* empty */
		}
		pdfoutputstream.println(stringbuffer.append(
				PDFOutputStream.pdf_float((double) (wspace / hscale))).append(
				" Tw").toString());
	}

	public void setFontSize(float f) {
		fontSize = f;
		u28.resetFont();
	}

	public void processText(String string) throws IOException {
		u28.processText(string);
	}

	public void selectFont(String string, int i, String string_86_,
			String string_87_) {
		u28.selectFont(string, i, string_86_, string_87_);
	}

	public void showXY(String string, float f, float f_88_, float f_89_) {
		if (string != null && string.length() != 0 && fontSize != 0.0F) {
			outputPDF.setTextMatrix(f_89_, (float) Math
					.tan((double) fontSlant * 0.017453292519943295), f, f_88_);
			show(string);
		}
	}

	public void show(String string) {
		if (string != null && string.length() != 0 && fontSize != 0.0F) {
			outputPDF.printString(string);
			outputPDF.println("Tj");
		}
	}

	public void save() {
		outputPDF.endText();
		outputPDF.println("q");
	}

	public void restore() {
		outputPDF.endText();
		outputPDF.println("Q");
	}

	public void setBorderStyle(String string, float f) {
		/* empty */
	}

	public void setDash(float f, float f_90_, float f_91_) {
		if (!((double) f_90_ < 0.0) && !((double) f_91_ < 0.0)) {
			if ((double) f_90_ == 0.0 && (double) f_91_ == 0.0)
				outputPDF.println("[] 0 d");
			else {
				PDFOutputStream pdfoutputstream = outputPDF;
				StringBuffer stringbuffer = new StringBuffer().append("[");
				if (this != null) {
					/* empty */
				}
				StringBuffer stringbuffer_92_ = stringbuffer.append(
						PDFOutputStream.pdf_float((double) f_90_)).append(" ");
				if (this != null) {
					/* empty */
				}
				StringBuffer stringbuffer_93_ = stringbuffer_92_.append(
						PDFOutputStream.pdf_float((double) f_91_)).append("] ");
				if (this != null) {
					/* empty */
				}
				pdfoutputstream.println(stringbuffer_93_.append(
						PDFOutputStream.pdf_float((double) f)).append(" d")
						.toString());
			}
		}
	}

	public void setLineCap(int i) {
		if (i >= 0 && i <= 2)
			outputPDF.println("" + i + " J");
	}

	public void setLineWidth(float f) {
		if (!((double) f <= 0.0)) {
			PDFOutputStream pdfoutputstream = outputPDF;
			StringBuffer stringbuffer = new StringBuffer();
			if (this != null) {
				/* empty */
			}
			pdfoutputstream.println(stringbuffer.append(
					PDFOutputStream.pdf_float((double) f)).append(" w")
					.toString());
		}
	}

	public void placeImage(URLSpec urlspec, String string) {
		try {
			placeImage(imageFactory.makeImage(urlspec, string));
		} catch (IOException ioexception) {
			errorHandler.exception("Failed to read image file", ioexception);
		} catch (ImageFormatException imageformatexception) {
			errorHandler.exception("Cannot parse image file",
					imageformatexception);
		} catch (UnregisteredMIMETypeException unregisteredmimetypeexception) {
			errorHandler.error("Unknown MIME type: " + string);
		}
	}

	void placeImage(Image image) {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<placeimage src=\"" + image.source
					+ "\" type=\"" + image.mimetype + "\"/>");
		String string = images.getPDFImageName(image);
		if (string == null) {
			images.addImage(new IndirectObject(outputPDF.lastObjectNumber++),
					image, outputPDF);
			string = images.getPDFImageName(image);
		}
		PDFImage pdfimage = images.getImageDesc(image);
		save();
		outputPDF.println("0 Tw 0 Tc");
		PDFOutputStream pdfoutputstream = outputPDF;
		StringBuffer stringbuffer = new StringBuffer().append("");
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_94_ = stringbuffer
				.append(
						PDFOutputStream.pdf_float((double) (float) pdfimage
								.getWidth())).append(" 0 0 ");
		if (this != null) {
			/* empty */
		}
		pdfoutputstream.println(stringbuffer_94_.append(
				PDFOutputStream
						.pdf_float((double) (float) pdfimage.getHeight()))
				.append(" 0 0  cm").toString());
		outputPDF.println("/" + string + " Do");
		restore();
		if (currentPage != null)
			currentPage.addImage(string, images.getImageOID(image));
		else if (currentImage != null && currentImage.formResources != null)
			currentImage.formResources.addImage("/" + string + " "
					+ images.getImageOID(image).toStringR());
	}

	public void setCMYKColor(float f, float f_95_, float f_96_, float f_97_) {
		setCMYKColorFill(f, f_95_, f_96_, f_97_);
		setCMYKColorStroke(f, f_95_, f_96_, f_97_);
	}

	public void setCMYKColorFill(float f, float f_98_, float f_99_, float f_100_) {
		PDFOutputStream pdfoutputstream = outputPDF;
		StringBuffer stringbuffer = new StringBuffer();
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_101_ = stringbuffer.append(
				PDFOutputStream.pdf_float((double) f)).append(" ");
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_102_ = stringbuffer_101_.append(
				PDFOutputStream.pdf_float((double) f_98_)).append(" ");
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_103_ = stringbuffer_102_.append(
				PDFOutputStream.pdf_float((double) f_99_)).append(" ");
		if (this != null) {
			/* empty */
		}
		pdfoutputstream.println(stringbuffer_103_.append(
				PDFOutputStream.pdf_float((double) f_100_)).append(" k")
				.toString());
	}

	public void setCMYKColorStroke(float f, float f_104_, float f_105_,
			float f_106_) {
		PDFOutputStream pdfoutputstream = outputPDF;
		StringBuffer stringbuffer = new StringBuffer();
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_107_ = stringbuffer.append(
				PDFOutputStream.pdf_float((double) f)).append(" ");
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_108_ = stringbuffer_107_.append(
				PDFOutputStream.pdf_float((double) f_104_)).append(" ");
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_109_ = stringbuffer_108_.append(
				PDFOutputStream.pdf_float((double) f_105_)).append(" ");
		if (this != null) {
			/* empty */
		}
		pdfoutputstream.println(stringbuffer_109_.append(
				PDFOutputStream.pdf_float((double) f_106_)).append(" K")
				.toString());
	}

	public void setRGBColor(float f, float f_110_, float f_111_) {
		setRGBColorFill(f, f_110_, f_111_);
		setRGBColorStroke(f, f_110_, f_111_);
	}

	public void setRGBColorFill(float f, float f_112_, float f_113_) {
		PDFOutputStream pdfoutputstream = outputPDF;
		StringBuffer stringbuffer = new StringBuffer();
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_114_ = stringbuffer.append(
				PDFOutputStream.pdf_float((double) f)).append(" ");
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_115_ = stringbuffer_114_.append(
				PDFOutputStream.pdf_float((double) f_112_)).append(" ");
		if (this != null) {
			/* empty */
		}
		pdfoutputstream.println(stringbuffer_115_.append(
				PDFOutputStream.pdf_float((double) f_113_)).append(" rg")
				.toString());
	}

	public void setRGBColorStroke(float f, float f_116_, float f_117_) {
		PDFOutputStream pdfoutputstream = outputPDF;
		StringBuffer stringbuffer = new StringBuffer();
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_118_ = stringbuffer.append(
				PDFOutputStream.pdf_float((double) f)).append(" ");
		if (this != null) {
			/* empty */
		}
		StringBuffer stringbuffer_119_ = stringbuffer_118_.append(
				PDFOutputStream.pdf_float((double) f_116_)).append(" ");
		if (this != null) {
			/* empty */
		}
		pdfoutputstream.println(stringbuffer_119_.append(
				PDFOutputStream.pdf_float((double) f_117_)).append(" RG")
				.toString());
	}

	public void setGray(float f) {
		setGrayStroke(f);
		setGrayFill(f);
	}

	void setGrayFill(float f) {
		PDFOutputStream pdfoutputstream = outputPDF;
		StringBuffer stringbuffer = new StringBuffer();
		if (this != null) {
			/* empty */
		}
		pdfoutputstream.println(stringbuffer.append(
				PDFOutputStream.pdf_float((double) f)).append(" g").toString());
	}

	void setGrayStroke(float f) {
		PDFOutputStream pdfoutputstream = outputPDF;
		StringBuffer stringbuffer = new StringBuffer();
		if (this != null) {
			/* empty */
		}
		pdfoutputstream.println(stringbuffer.append(
				PDFOutputStream.pdf_float((double) f)).append(" G").toString());
	}

	public void setSpotColor(float f, String string, float[] fs) {
		setSpotColorFill(f, string, fs);
		setSpotColorStroke(f, string, fs);
	}

	public void setSpotColorFill(float f, String string, float[] fs) {
		addSpotColor(f, string, fs);
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<spot color name=\""
					+ spotColors.getPDFSpotColorName(string, fs) + "\"/>");
		PDFOutputStream pdfoutputstream = outputPDF;
		StringBuffer stringbuffer = new StringBuffer().append("/").append(
				spotColors.getPDFSpotColorName(string, fs)).append(" cs ");
		if (this != null) {
			/* empty */
		}
		pdfoutputstream.println(stringbuffer.append(
				PDFOutputStream.pdf_float((double) f)).append(" scn")
				.toString());
	}

	public void setSpotColorStroke(float f, String string, float[] fs) {
		addSpotColor(f, string, fs);
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<spot color name=\""
					+ spotColors.getPDFSpotColorName(string, fs) + "\"/>");
		PDFOutputStream pdfoutputstream = outputPDF;
		StringBuffer stringbuffer = new StringBuffer().append("/").append(
				spotColors.getPDFSpotColorName(string, fs)).append(" CS ");
		if (this != null) {
			/* empty */
		}
		pdfoutputstream.println(stringbuffer.append(
				PDFOutputStream.pdf_float((double) f)).append(" SCN")
				.toString());
	}

	public void addSpotColor(float f, String string, float[] fs) {
		String string_120_ = spotColors.getPDFSpotColorName(string, fs);
		if (string_120_ == null) {
			spotColors.addSpotColor(f, string, fs, new IndirectObject(
					outputPDF.lastObjectNumber++));
			string_120_ = spotColors.getPDFSpotColorName(string, fs);
		}
		if (currentPage != null)
			currentPage.addSpotColor(string_120_, spotColors.getSpotColorOID(
					string, fs));
		else if (currentImage != null && currentImage.formResources != null)
			currentImage.formResources.addSpotColor("/" + string_120_ + " "
					+ spotColors.getSpotColorOID(string, fs).toStringR());
	}

	void setOpacityFill(float f) {
		if (getPDFVersion_major() == 1 && getPDFVersion_minor() < 4) {
			if (!unsupportedOpacityWarning)
				outputPDF.warning("PDF version " + pdfVersion
						+ " does not support opacity; feature ignored.");
			unsupportedOpacityWarning = true;
		} else {
			String string = gStates.getAISName(false);
			String string_121_ = gStates.getFillOpacityName(f);
			if (string == null) {
				gStates.addAIS(false, new IndirectObject(
						outputPDF.lastObjectNumber++));
				string = gStates.getAISName(false);
			}
			if (string_121_ == null) {
				gStates.addFillOpacity(f, new IndirectObject(
						outputPDF.lastObjectNumber++));
				string_121_ = gStates.getFillOpacityName(f);
			}
			if (currentPage != null) {
				currentPage.addAIS(string, gStates.getAIS_OID(false));
				currentPage.addFillOpacity(string_121_, gStates
						.getFillOpacityOID(f));
			} else if (currentImage != null
					&& currentImage.formResources != null) {
				currentImage.formResources.addAIS("/" + string + " "
						+ gStates.getAIS_OID(false).toStringR());
				currentImage.formResources.addFillOpacity("/" + string_121_
						+ " " + gStates.getFillOpacityOID(f).toStringR());
			}
			if (PDFOutputStream.dump != null)
				PDFOutputStream.dump.println("<opacity value for fill=\""
						+ string_121_ + "\"/>");
			outputPDF.println("/" + string + " gs");
			outputPDF.println("/" + string_121_ + " gs");
		}
	}

	void setOpacityStroke(float f) {
		if (getPDFVersion_major() == 1 && getPDFVersion_minor() < 4) {
			if (!unsupportedOpacityWarning)
				outputPDF.warning("PDF version " + pdfVersion
						+ " does not support opacity; feature ignored.");
			unsupportedOpacityWarning = true;
		} else {
			String string = gStates.getAISName(false);
			String string_122_ = gStates.getStrokeOpacityName(f);
			if (string == null) {
				gStates.addAIS(false, new IndirectObject(
						outputPDF.lastObjectNumber++));
				string = gStates.getAISName(false);
			}
			if (string_122_ == null) {
				gStates.addStrokeOpacity(f, new IndirectObject(
						outputPDF.lastObjectNumber++));
				string_122_ = gStates.getStrokeOpacityName(f);
			}
			if (currentPage != null) {
				currentPage.addAIS(string, gStates.getAIS_OID(false));
				currentPage.addStrokeOpacity(string_122_, gStates
						.getStrokeOpacityOID(f));
			} else if (currentImage != null
					&& currentImage.formResources != null) {
				currentImage.formResources.addAIS("/" + string + " "
						+ gStates.getAIS_OID(false).toStringR());
				currentImage.formResources.addFillOpacity("/" + string_122_
						+ " " + gStates.getStrokeOpacityOID(f).toStringR());
			}
			if (PDFOutputStream.dump != null)
				PDFOutputStream.dump.println("<opacity value for opacity=\""
						+ string_122_ + "\"/>");
			outputPDF.println("/" + string + " gs");
			outputPDF.println("/" + string_122_ + " gs");
		}
	}

	void set_fillrule(String string) {
		if (string.equals("winding"))
			fillRule = FILL_WINDING;
		else if (string.equals("evenodd"))
			fillRule = FILL_EVENODD;
	}

	public void clip() {
		switch (fillRule) {
		case 0:
			outputPDF.println("W");
			break;
		case 1:
			outputPDF.println("W*");
			break;
		}
	}

	public void endPath() {
		outputPDF.println("n");
	}

	public void closePath() {
		outputPDF.println("h");
	}

	public void fill() {
		switch (fillRule) {
		case 0:
			outputPDF.println("f");
			break;
		case 1:
			outputPDF.println("f*");
			break;
		}
	}

	public void stroke() {
		outputPDF.println("S");
	}

	public void fillStroke() {
		outputPDF.println("");
		switch (fillRule) {
		case 0:
			outputPDF.println("B");
			break;
		case 1:
			outputPDF.println("B*");
			break;
		}
	}

	public void addLocalLink(float f, float f_123_, float f_124_, float f_125_,
			int i, float f_126_, float f_127_, String string) {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<locallink page=\"" + i + "\"/>");
		IndirectObject indirectobject = new IndirectObject(
				outputPDF.lastObjectNumber);
		links.addLink(new LocalLink(new IndirectObject(
				outputPDF.lastObjectNumber), f, f_123_, f_124_, f_125_, i,
				f_126_, f_127_, string));
		currentPage.addLinkID(indirectobject);
	}

	public void addLocalLink(String string, float f, float f_128_,
			float f_129_, float f_130_, String string_131_) {
		markDestination(string);
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<locallink dest_id=\"" + string
					+ "\"/>");
		IndirectObject indirectobject = new IndirectObject(
				outputPDF.lastObjectNumber);
		links.addLink(new LocalLink(new IndirectObject(
				outputPDF.lastObjectNumber), string, f, f_128_, f_129_, f_130_,
				string_131_));
		currentPage.addLinkID(indirectobject);
		outputPDF.lastObjectNumber++;
	}

	public void addPDFLink(float f, float f_132_, float f_133_, float f_134_,
			int i, String string) {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<pdflink page=\"" + i + "\" file=\""
					+ string + "\"/>");
		IndirectObject indirectobject = new IndirectObject(
				outputPDF.lastObjectNumber);
		links.addLink(new PDFLink(
				new IndirectObject(outputPDF.lastObjectNumber), f, f_132_,
				f_133_, f_134_, i, string));
		currentPage.addLinkID(indirectobject);
	}

	public void addWebLink(float f, float f_135_, float f_136_, float f_137_,
			String string, boolean bool) {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<weblink url=\"" + string + "\"/>");
		IndirectObject indirectobject = new IndirectObject(
				outputPDF.lastObjectNumber);
		links.addLink(new WebLink(indirectobject, f, f_135_, f_136_, f_137_, 0,
				string, bool));
		currentPage.addLinkID(indirectobject);
		outputPDF.lastObjectNumber++;
	}

	public void addTextAnnotation(float f, float f_138_, float f_139_,
			float f_140_, String string, String string_141_, float[] fs) {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<textannotation txt=\"" + string
					+ "\"/>");
		IndirectObject indirectobject = new IndirectObject(
				outputPDF.lastObjectNumber);
		TextAnnot textannot = new TextAnnot(indirectobject, f, f_138_, f_139_,
				f_140_);
		textannot.title = string_141_;
		textannot.contents = string;
		if (fs != null && fs.length == 3) {
			textannot.border_red = fs[0];
			textannot.border_green = fs[1];
			textannot.border_blue = fs[2];
		}
		links.addLink(textannot);
		currentPage.addLinkID(indirectobject);
		outputPDF.lastObjectNumber++;
	}

	public void addLocalBookmark(String string, int i, int i_142_, int i_143_,
			float f, float f_144_, boolean bool) {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<localbookmark label=\"" + string
					+ "\"/>");
		if (outputPDF.trailer.catalog.outlines == null)
			outputPDF.trailer.catalog.outlines = new Outlines(
					new IndirectObject(outputPDF.lastObjectNumber++));
		outputPDF.trailer.catalog.outlines.addBookmark(new LocalBookmark(
				string, i, i_142_, i_143_, f, f_144_, bool), outputPDF);
	}

	public void addLocalBookmark(String string, String string_145_, int i,
			int i_146_, boolean bool) {
		markDestination(string);
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<localbookmark label=\""
					+ string_145_ + "\"/>");
		if (outputPDF.trailer.catalog.outlines == null)
			outputPDF.trailer.catalog.outlines = new Outlines(
					new IndirectObject(outputPDF.lastObjectNumber++));
		outputPDF.trailer.catalog.outlines.addBookmark(new LocalBookmark(
				string, string_145_, i, i_146_, bool), outputPDF);
	}

	public void addWebBookmark(String string, int i, int i_147_,
			String string_148_, boolean bool, boolean bool_149_) {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<webbookmark label=\"" + string
					+ "\"/>");
		if (outputPDF.trailer.catalog.outlines == null)
			outputPDF.trailer.catalog.outlines = new Outlines(
					new IndirectObject(outputPDF.lastObjectNumber++));
		outputPDF.trailer.catalog.outlines.addBookmark(new ExternalBookmark(
				string, i, i_147_, string_148_, bool, bool_149_), outputPDF);
	}

	public void addPDFBookmark(String string, int i, int i_150_,
			String string_151_, boolean bool, boolean bool_152_) {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<pdfbookmark label=\"" + string
					+ "\"/>");
		if (outputPDF.trailer.catalog.outlines == null)
			outputPDF.trailer.catalog.outlines = new Outlines(
					new IndirectObject(outputPDF.lastObjectNumber++));
		outputPDF.trailer.catalog.outlines.addBookmark(new ExternalBookmark(
				string, i, i_150_, string_151_, bool, bool_152_), outputPDF);
	}

	public void addNamedDestination(String string, float f, float f_153_) {
		if (currentPage == null || currentPage.getID() == null)
			errorHandler
					.error("Named destination cannot be added, page is not initialized!");
		else if (dropUnusedDestinations)
			destStorage.put(string, new NamedDestRecord(currentPage.getID(), f,
					f_153_));
		else
			addNamedDestination(string, f, f_153_, currentPage.getID());
	}

	public void addNamedDestination(String string, float f, float f_154_,
			IndirectObject indirectobject) {
		if (outputPDF.trailer.catalog.names == null)
			outputPDF.trailer.catalog.names = new Names(new IndirectObject(
					outputPDF.lastObjectNumber++));
		if (outputPDF.trailer.catalog.names.dests == null)
			outputPDF.trailer.catalog.names.dests = new NameTree(
					new IndirectObject(outputPDF.lastObjectNumber++));
		outputPDF.trailer.catalog.names.dests.addName(string, indirectobject,
				f, f_154_);
	}

	void markDestination(String string) {
		if (dropUnusedDestinations)
			usedDestinations.put(string, string);
	}

	public void printTail() {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<tail/>");
		outputPDF.trailer.encrypt = null;
		if (outputPDF.getUseEncryption()) {
			outputPDF.trailer.encrypt = outputPDF.getEncryptObject();
			outputPDF.trailer.encrypt.write(outputPDF);
		}
		outputPDF.trailer.xref.addObjectOffset(new IndirectObject(0), 0L);
		outputPDF.trailer.write(outputPDF);
	}
}