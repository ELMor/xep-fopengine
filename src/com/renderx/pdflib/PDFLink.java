/*
 * PDFLink - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

class PDFLink extends Annot {
	static final int RETAIN = 0;

	static final int FITPAGE = 1;

	static final int FITWIDTH = 2;

	static final int FITHEIGHT = 3;

	static final int FITBBOX = 4;

	static String annot_type = "Link";

	String filename;

	String contents;

	int dest_type;

	int dest_page;

	PDFLink(IndirectObject indirectobject, float f, float f_0_, float f_1_,
			float f_2_, int i, String string) {
		super(indirectobject, f, f_0_, f_1_, f_2_);
	}

	String getSubType() {
		return "Link";
	}

	void writeAnnotBody(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.println("/A ");
		pdfoutputstream.begin_dict();
		pdfoutputstream.println("/Type/Action /S /GoToR\n");
		if (dest_type == 0)
			pdfoutputstream.println("/D [" + (dest_page - 1)
					+ " 0 R /XYZ null null 0]\n");
		else if (dest_type == 1)
			pdfoutputstream.println("/D [" + (dest_page - 1) + " 0 R /Fit]\n");
		else if (dest_type == 2)
			pdfoutputstream.println("/D [" + (dest_page - 1)
					+ " 0 R /FitH 2000]\n");
		else if (dest_type == 3)
			pdfoutputstream.println("/D [" + (dest_page - 1)
					+ " 0 R /FitV 0]\n");
		else if (dest_type == 4)
			pdfoutputstream.println("/D [" + (dest_page - 1) + " 0 R/FitB]\n");
		pdfoutputstream.println("/F ");
		pdfoutputstream.begin_dict();
		pdfoutputstream.println("/Type/FileSpec\n");
		pdfoutputstream.println("/F (" + filename + ")\n");
		pdfoutputstream.end_dict();
		pdfoutputstream.end_dict();
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		/* empty */
	}
}