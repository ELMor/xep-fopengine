/*
 * HintStream - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class HintStream extends PDFObject {
	int length;

	PageOffsetHintRecord pageOffset;

	SharedObjectsHintRecord sharedObjects;

	GenericHintRecord outlines;

	GenericHintRecord namedDestinations;

	GenericHintRecord pageLabel;

	private ByteArrayOutputStream baos;

	byte[] bytes;

	HintStream(IndirectObject indirectobject) {
		super(indirectobject);
	}

	void setType() {
		/* empty */
	}

	void writeBody(PDFOutputStream pdfoutputstream) {
		baos = new ByteArrayOutputStream();
		long l = 0L;
		try {
			byte[] is = pageOffset.getBytes();
			int i = 0;
			if (is != null) {
				l += (long) is.length;
				baos.write(is);
				i = is.length;
			}
			byte[] is_0_ = sharedObjects.getBytes();
			if (is_0_ != null) {
				pdfoutputstream.write(("/S " + l + "\n").getBytes());
				baos.write(is_0_);
				l += (long) is_0_.length;
				i += is_0_.length;
			}
			if (outlines != null) {
				byte[] is_1_ = outlines.getBytes();
				if (is_1_ != null) {
					baos.write(is_1_);
					pdfoutputstream.write(("/O " + l + "\n").getBytes());
					i += is_1_.length;
					l += (long) is_1_.length;
				}
			}
			if (namedDestinations != null) {
				byte[] is_2_ = namedDestinations.getBytes();
				if (is_2_ != null) {
					baos.write(is_2_);
					pdfoutputstream.write(("/E " + l + "\n").getBytes());
					i += is_2_.length;
					l += (long) is_2_.length;
				}
			}
			pdfoutputstream.write(("/Length " + i + "\n").getBytes());
			if (pdfoutputstream.compression)
				pdfoutputstream.write("/Filter /FlateDecode\n".getBytes());
		} catch (IOException ioexception) {
			throw new PDFException(ioexception);
		}
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		/* empty */
	}

	void writeTail(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.println(">>");
		pdfoutputstream.println("stream");
		int i = pdfoutputstream.size();
		pdfoutputstream.startStream();
		try {
			pdfoutputstream.write(baos.toByteArray());
		} catch (IOException ioexception) {
			throw new PDFException(ioexception);
		}
		pdfoutputstream.finishStream();
		length = pdfoutputstream.size() - i;
		pdfoutputstream.println("endstream");
		pdfoutputstream.println("endobj");
	}
}