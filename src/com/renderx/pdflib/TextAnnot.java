/*
 * TextAnnot - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

class TextAnnot extends Annot {
	int icon = 6;

	String contents = "";

	String title = "";

	boolean open = false;

	TextAnnot(IndirectObject indirectobject, float f, float f_0_, float f_1_,
			float f_2_) {
		super(indirectobject, f, f_0_, f_1_, f_2_);
	}

	String getSubType() {
		return "Text";
	}

	void writeAnnotBody(PDFOutputStream pdfoutputstream) {
		if (icon != 6)
			pdfoutputstream.println("/Name /" + Annot.icon_names[icon] + "\n");
		if (open)
			pdfoutputstream.println("/Open true\n");
		pdfoutputstream.println("/Contents ");
		if (contents != null) {
			pdfoutputstream.print_annotation(contents);
			pdfoutputstream.println("\n");
		} else
			pdfoutputstream.println("()");
		if (title != null) {
			pdfoutputstream.println("/T ");
			pdfoutputstream.print_annotation(title);
			pdfoutputstream.println();
		}
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		/* empty */
	}
}