/*
 * Page - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import com.renderx.util.Hashtable;
import com.renderx.util.List;

public class Page extends PDFObject {
	IndirectObject parent_id;

	int resourcesCount;

	Hashtable resourceID;

	int contentsCount;

	Hashtable contentID;

	int annotsCount;

	Hashtable annotID;

	Hashtable pageboundaries;

	List printermarks;

	Resources resources;

	float width;

	float height;

	String number;

	Page(IndirectObject indirectobject, float f, float f_0_, String string) {
		super(indirectobject);
		width = f;
		height = f_0_;
		resourceID = new Hashtable();
		resourcesCount = -1;
		contentID = new Hashtable();
		contentsCount = -1;
		annotID = new Hashtable();
		annotsCount = -1;
		pageboundaries = new Hashtable();
		printermarks = new List();
		number = string;
	}

	String getType() {
		return "Page";
	}

	void addResources(IndirectObject indirectobject) {
		resources = new Resources(indirectobject);
	}

	void addResourcesID(IndirectObject indirectobject) {
		resourcesCount++;
		resourceID.put(new Integer(resourcesCount), indirectobject);
	}

	void addContentID(IndirectObject indirectobject) {
		contentsCount++;
		contentID.put(new Integer(contentsCount), indirectobject);
	}

	void addLinkID(IndirectObject indirectobject) {
		annotsCount++;
		annotID.put(new Integer(annotsCount), indirectobject);
	}

	void setSize(int i, int i_1_) {
		/* empty */
	}

	void addFont(String string, IndirectObject indirectobject) {
		resources.addFont("/" + string + " " + indirectobject.toStringR());
	}

	void addImage(String string, IndirectObject indirectobject) {
		resources.addImage("/" + string + " " + indirectobject.toStringR());
	}

	void addSpotColor(String string, IndirectObject indirectobject) {
		resources.addSpotColor("/" + string + " " + indirectobject.toStringR());
	}

	void addFillOpacity(String string, IndirectObject indirectobject) {
		resources.addFillOpacity("/" + string + " "
				+ indirectobject.toStringR());
	}

	void addStrokeOpacity(String string, IndirectObject indirectobject) {
		resources.addStrokeOpacity("/" + string + " "
				+ indirectobject.toStringR());
	}

	void addAIS(String string, IndirectObject indirectobject) {
		resources.addAIS("/" + string + " " + indirectobject.toStringR());
	}

	void writeBody(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.println("/Parent " + parent_id.toStringR());
		float[] fs = getBleeds();
		float[] fs_2_ = getCropOffsets();
		pdfoutputstream
				.println("/MediaBox ["
						+ PDFOutputStream.pdf_float((double) -fs_2_[0])
						+ " "
						+ PDFOutputStream.pdf_float((double) -fs_2_[1])
						+ " "
						+ PDFOutputStream
								.pdf_float((double) (width + fs_2_[2]))
						+ " "
						+ PDFOutputStream
								.pdf_float((double) (height + fs_2_[3])) + "]");
		pdfoutputstream
				.println("/CropBox ["
						+ PDFOutputStream.pdf_float((double) -fs_2_[0])
						+ " "
						+ PDFOutputStream.pdf_float((double) -fs_2_[1])
						+ " "
						+ PDFOutputStream
								.pdf_float((double) (width + fs_2_[2]))
						+ " "
						+ PDFOutputStream
								.pdf_float((double) (height + fs_2_[3])) + "]");
		pdfoutputstream.println("/BleedBox ["
				+ PDFOutputStream.pdf_float((double) -fs[0]) + " "
				+ PDFOutputStream.pdf_float((double) -fs[1]) + " "
				+ PDFOutputStream.pdf_float((double) (width + fs[2])) + " "
				+ PDFOutputStream.pdf_float((double) (height + fs[3])) + "]");
		pdfoutputstream.println("/TrimBox [0 0 "
				+ PDFOutputStream.pdf_float((double) width) + " "
				+ PDFOutputStream.pdf_float((double) height) + "]");
		pdfoutputstream.println("/ArtBox [0 0 "
				+ PDFOutputStream.pdf_float((double) width) + " "
				+ PDFOutputStream.pdf_float((double) height) + "]");
		for (int i = contentsCount; i >= 0; i--) {
			IndirectObject indirectobject = (IndirectObject) contentID
					.get(new Integer(i));
			contentID.put(new Integer(i + 1), indirectobject);
		}
		contentsCount++;
		contentID.put(new Integer(0), new IndirectObject(
				pdfoutputstream.lastObjectNumber));
		pdfoutputstream.println("/Resources " + resources.getID().toStringR());
		pdfoutputstream.printVector("Contents", contentID, contentsCount);
		pdfoutputstream.printVector("Annots", annotID, annotsCount);
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		resources.write(pdfoutputstream);
		float[] fs = getBleeds();
		pdfoutputstream.lastObjectNumber = (pdfoutputstream
				.beginStream(new IndirectObject(
						pdfoutputstream.lastObjectNumber)).num);
		pdfoutputstream.println("q");
		pdfoutputstream.println(PDFOutputStream.pdf_float((double) -fs[0])
				+ " " + PDFOutputStream.pdf_float((double) -fs[1]) + " m");
		pdfoutputstream.println(PDFOutputStream
				.pdf_float((double) (width + fs[2]))
				+ " " + PDFOutputStream.pdf_float((double) -fs[1]) + " l");
		pdfoutputstream.println(PDFOutputStream
				.pdf_float((double) (width + fs[2]))
				+ " "
				+ PDFOutputStream.pdf_float((double) (height + fs[3]))
				+ " l");
		pdfoutputstream.println(PDFOutputStream.pdf_float((double) -fs[0])
				+ " " + PDFOutputStream.pdf_float((double) (height + fs[3]))
				+ " l");
		pdfoutputstream.println("h");
		pdfoutputstream.println("W n");
		pdfoutputstream.endStream();
	}

	public float[] getBleeds() {
		float[] fs = new float[4];
		for (int i = 0; i < fs.length; i++)
			fs[i] = 0.0F;
		if (pageboundaries.containsKey("BLEED"))
			fs = (float[]) pageboundaries.get("BLEED");
		return fs;
	}

	public float[] getCropOffsets() {
		float[] fs = new float[4];
		for (int i = 0; i < fs.length; i++)
			fs[i] = 0.0F;
		if (pageboundaries.containsKey("CROP_OFFSET"))
			fs = (float[]) pageboundaries.get("CROP_OFFSET");
		float[] fs_3_ = getBleeds();
		for (int i = 0; i < fs.length; i++) {
			if (fs[i] < fs_3_[i])
				fs[i] = fs_3_[i];
		}
		return fs;
	}

	public float getBleedMarkWidth() {
		return (pageboundaries.containsKey("BLEED_MARK_WIDTH") ? new Float(
				(String) pageboundaries.get("BLEED_MARK_WIDTH")).floatValue()
				: 0.0F);
	}

	public float getCropMarkWidth() {
		return (pageboundaries.containsKey("CROP_MARK_WIDTH") ? new Float(
				(String) pageboundaries.get("CROP_MARK_WIDTH")).floatValue()
				: 0.0F);
	}
}