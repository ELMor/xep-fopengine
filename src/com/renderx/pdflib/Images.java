/*
 * Images - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import com.renderx.graphics.Image;
import com.renderx.util.Array;
import com.renderx.util.Hashtable;

public class Images {
	private final Hashtable imageName = new Hashtable();

	private final Hashtable imageOID = new Hashtable();

	private final Hashtable imageDesc = new Hashtable();

	private final Array imageList = new Array();

	Images() {
		/* empty */
	}

	void addImage(IndirectObject indirectobject, Image image,
			PDFOutputStream pdfoutputstream) {
		PDFImage pdfimage = new PDFImage(indirectobject, image, pdfoutputstream);
		imageName.put(image, "I" + imageList.length());
		imageList.put(imageList.length(), image);
		imageOID.put(image, indirectobject);
		imageDesc.put(image, pdfimage);
	}

	String getPDFImageName(Image image) {
		return (String) imageName.get(image);
	}

	IndirectObject getImageOID(Image image) {
		return (IndirectObject) imageOID.get(image);
	}

	PDFImage getImageDesc(Image image) {
		return (PDFImage) imageDesc.get(image);
	}

	void write(PDFOutputStream pdfoutputstream) {
		for (int i = 0; i < imageList.length(); i++) {
			Image image = (Image) imageList.get(i);
			PDFImage pdfimage = (PDFImage) imageDesc.get(image);
			pdfimage.write(pdfoutputstream);
		}
	}
}