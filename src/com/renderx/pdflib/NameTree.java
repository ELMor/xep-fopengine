/*
 * NameTree - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.util.Enumeration;

import com.renderx.util.Hashtable;

public class NameTree extends PDFObject {
	Hashtable names = new Hashtable();

	NamedDestination[] sortbuffer = null;

	Subtree[] kids = null;

	public static final int NODESIZE = 16;

	static class Target {
		IndirectObject page;

		float x;

		float y;

		Target(IndirectObject indirectobject, float f, float f_0_) {
			page = indirectobject;
			x = f;
			y = f_0_;
		}
	}

	class Subtree extends PDFObject {
		int start = -1;

		int end = -1;

		Subtree[] kids = null;

		Subtree(IndirectObject indirectobject, int i, int i_1_) {
			super(indirectobject);
			start = i;
			end = i_1_;
		}

		void writeBody(PDFOutputStream pdfoutputstream) {
			pdfoutputstream.print("/Limits [");
			pdfoutputstream.print_annotation(sortbuffer[start].name);
			pdfoutputstream.print(" ");
			pdfoutputstream.print_annotation(sortbuffer[end].name);
			pdfoutputstream.println("]");
			kids = NameTree.this.enumerateKids(pdfoutputstream, start, end);
		}

		void writeSubObjects(PDFOutputStream pdfoutputstream) {
			if (kids != null) {
				for (int i = 0; i < kids.length; i++)
					kids[i].write(pdfoutputstream);
				kids = null;
			}
		}
	}

	static class NamedDestination extends PDFObject {
		String name;

		Target target;

		NamedDestination(IndirectObject indirectobject, String string,
				Target target) {
			super(indirectobject);
			name = string;
			this.target = target;
		}

		void writeBody(PDFOutputStream pdfoutputstream) {
			pdfoutputstream.print("/D [" + target.page.toStringR() + " /XYZ "
					+ PDFOutputStream.pdf_float_coarse((double) target.x) + " "
					+ PDFOutputStream.pdf_float_coarse((double) target.y)
					+ " 0]");
		}

		void writeSubObjects(PDFOutputStream pdfoutputstream) {
			/* empty */
		}
	}

	NameTree(IndirectObject indirectobject) {
		super(indirectobject);
	}

	public void addName(String string, IndirectObject indirectobject, float f,
			float f_2_) {
		if (!names.containsKey(string))
			names.put(string, new Target(indirectobject, f, f_2_));
	}

	void writeBody(PDFOutputStream pdfoutputstream) {
		sortbuffer = new NamedDestination[names.size()];
		int i = 0;
		Enumeration enumeration = names.keys();
		while (enumeration.hasMoreElements()) {
			String string = (String) enumeration.nextElement();
			sortbuffer[i++] = new NamedDestination(new IndirectObject(
					pdfoutputstream.lastObjectNumber++), string, (Target) names
					.get(string));
		}
		String string = sortbuffer[0].name;
		for (i = 1; i < sortbuffer.length; i++) {
			String string_3_ = sortbuffer[i].name;
			if (string.compareTo(string_3_) < 0)
				string = string_3_;
		}
		sortTargets(0, sortbuffer.length - 1, string);
		kids = enumerateKids(pdfoutputstream, 0, sortbuffer.length - 1);
	}

	private Subtree[] enumerateKids(PDFOutputStream pdfoutputstream, int i,
			int i_4_) {
		if (i_4_ - i < 16) {
			pdfoutputstream.println("/Names [");
			for (int i_5_ = i; i_5_ <= i_4_; i_5_++) {
				pdfoutputstream.print_annotation(sortbuffer[i_5_].name);
				pdfoutputstream.println(" "
						+ sortbuffer[i_5_].getID().toStringR());
			}
			pdfoutputstream.println("]");
			return null;
		}
		int i_6_ = 16;
		int i_7_;
		for (i_7_ = (i_4_ - i + 16) / 16; i_7_ > 16; i_7_ = (i_7_ + 16 - 1) / 16)
			i_6_ *= 16;
		pdfoutputstream.print("/Kids [");
		Subtree[] subtrees = new Subtree[i_7_];
		for (int i_8_ = 0; i_8_ < i_7_; i_8_++) {
			int i_9_ = i + i_6_ - 1;
			if (i_9_ > i_4_)
				i_9_ = i_4_;
			subtrees[i_8_] = new Subtree(new IndirectObject(
					pdfoutputstream.lastObjectNumber++), i, i_9_);
			i += i_6_;
			pdfoutputstream.print(" " + subtrees[i_8_].getID().toStringR());
		}
		pdfoutputstream.println("]");
		return subtrees;
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		if (kids != null) {
			for (int i = 0; i < kids.length; i++)
				kids[i].write(pdfoutputstream);
			kids = null;
		}
		for (int i = 0; i < sortbuffer.length; i++)
			sortbuffer[i].write(pdfoutputstream);
		sortbuffer = null;
	}

	private void sortTargets(int i, int i_10_, String string) {
		if (i < i_10_) {
			if (i == i_10_ - 1) {
				if (sortbuffer[i].name.compareTo(sortbuffer[i_10_].name) > 0) {
					NamedDestination nameddestination = sortbuffer[i];
					sortbuffer[i] = sortbuffer[i_10_];
					sortbuffer[i_10_] = nameddestination;
				}
			} else {
				String string_11_ = null;
				boolean bool = false;
				int i_12_;
				for (i_12_ = i; i_12_ <= i_10_; i_12_++) {
					string_11_ = sortbuffer[i_12_].name;
					if (!string_11_.equals(string))
						break;
				}
				if (i_12_ <= i_10_) {
					int i_13_ = i;
					int i_14_;
					for (i_14_ = i_10_; i_13_ <= i_14_; i_14_--) {
						for (/**/; sortbuffer[i_13_].name
								.compareTo(string_11_) <= 0; i_13_++) {
							/* empty */
						}
						for (/**/; sortbuffer[i_14_].name
								.compareTo(string_11_) > 0; i_14_--) {
							/* empty */
						}
						if (i_13_ >= i_14_)
							break;
						NamedDestination nameddestination = sortbuffer[i_13_];
						sortbuffer[i_13_] = sortbuffer[i_14_];
						sortbuffer[i_14_] = nameddestination;
						i_13_++;
					}
					sortTargets(i, i_14_, string_11_);
					sortTargets(i_13_, i_10_, string);
				}
			}
		}
	}
}