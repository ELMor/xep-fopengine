/*
 * Info - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.util.Date;
import java.util.GregorianCalendar;

import com.renderx.util.User;

public class Info extends PDFObject {
	String Keywords;

	String Subject;

	String Title;

	String Creator;

	String Author;

	Info(IndirectObject indirectobject) {
		super(indirectobject);
	}

	void set(String string, String string_0_) {
		if (string != null && string_0_ != null) {
			if (string.equals("Keywords"))
				Keywords = new String(string_0_);
			else if (string.equals("Subject"))
				Subject = new String(string_0_);
			else if (string.equals("Title"))
				Title = new String(string_0_);
			else if (string.equals("Creator"))
				Creator = new String(string_0_);
			else if (string.equals("Author"))
				Author = new String(string_0_);
		}
	}

	void writeBody(PDFOutputStream pdfoutputstream) {
		if (Keywords != null) {
			pdfoutputstream.print("/Keywords ");
			pdfoutputstream.print_annotation(Keywords);
			pdfoutputstream.println();
		}
		if (Subject != null) {
			pdfoutputstream.print("/Subject ");
			pdfoutputstream.print_annotation(Subject);
			pdfoutputstream.println();
		}
		if (Title != null) {
			pdfoutputstream.print("/Title ");
			pdfoutputstream.print_annotation(Title);
			pdfoutputstream.println();
		}
		if (Creator != null) {
			pdfoutputstream.print("/Creator ");
			pdfoutputstream.print_annotation(Creator);
			pdfoutputstream.println();
		}
		if (Author != null) {
			pdfoutputstream.print("/Author ");
			pdfoutputstream.print_annotation(Author);
			pdfoutputstream.println();
		}
		String string = "D:19700101000000";
		if (!"true".equals(User.getProperty("com.renderx.pdflib.nodate"))) {
			GregorianCalendar gregoriancalendar = new GregorianCalendar();
			gregoriancalendar.setTime(new Date());
			string = new String("D:" + new Integer(gregoriancalendar.get(1))
					+ addzero(gregoriancalendar.get(2) + 1)
					+ addzero(gregoriancalendar.get(5))
					+ addzero(gregoriancalendar.get(11))
					+ addzero(gregoriancalendar.get(12))
					+ addzero(gregoriancalendar.get(13)));
		}
		pdfoutputstream.print("/CreationDate ");
		pdfoutputstream.print_annotation(string);
		pdfoutputstream.println();
		pdfoutputstream.print("/ModDate ");
		pdfoutputstream.print_annotation(string);
		pdfoutputstream.println();
		pdfoutputstream.print("/Producer ");
		pdfoutputstream
				.print_annotation("XEP PDF Generator \u2013 RenderX, Inc.");
	}

	private static String addzero(int i) {
		return i < 10 ? "0" + i : Integer.toString(i);
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		/* empty */
	}
}