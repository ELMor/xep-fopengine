/*
 * SpotColors - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.util.Enumeration;

import com.renderx.util.Hashtable;

public class SpotColors {
	int colorantsCount = -1;

	Hashtable colorants = new Hashtable();

	Hashtable colorantsOID = new Hashtable();

	private class SpotColor {
		String colorant;

		float[] altcolor;

		SpotColor(String string, float[] fs) {
			colorant = string;
			altcolor = fs;
			if (fs == null)
				altcolor = new float[] { 0.0F };
		}

		public int hashCode() {
			return colorant.hashCode();
		}

		public boolean equals(Object object) {
			if (!(object instanceof SpotColor))
				return false;
			SpotColor spotcolor_0_ = (SpotColor) object;
			if (!spotcolor_0_.colorant.equals(colorant))
				return false;
			if (spotcolor_0_.altcolor.length != altcolor.length)
				return false;
			for (int i = 0; i < altcolor.length; i++) {
				if (spotcolor_0_.altcolor[i] != altcolor[i])
					return false;
			}
			return true;
		}
	}

	SpotColors() {
		/* empty */
	}

	String getPDFSpotColorName(String string, float[] fs) {
		SpotColor spotcolor = new SpotColor(string, fs);
		return (String) colorants.get(spotcolor);
	}

	void addSpotColor(float f, String string, float[] fs,
			IndirectObject indirectobject) {
		colorantsCount++;
		SpotColor spotcolor = new SpotColor(string, fs);
		colorants.put(spotcolor, "CS" + colorantsCount);
		colorantsOID.put(spotcolor, indirectobject);
	}

	IndirectObject getSpotColorOID(String string, float[] fs) {
		SpotColor spotcolor = new SpotColor(string, fs);
		return (IndirectObject) colorantsOID.get(spotcolor);
	}

	void write(PDFOutputStream pdfoutputstream) {
		Enumeration enumeration = colorants.keys();
		while (enumeration.hasMoreElements()) {
			SpotColor spotcolor = (SpotColor) enumeration.nextElement();
			PDFSeparateColorSpace pdfseparatecolorspace = new PDFSeparateColorSpace(
					((IndirectObject) colorantsOID.get(spotcolor)),
					spotcolor.colorant, spotcolor.altcolor);
			pdfseparatecolorspace.write(pdfoutputstream);
		}
	}
}