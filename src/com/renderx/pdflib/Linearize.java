/*
 * Linearize - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;

import com.renderx.fonts.FontCatalog;
import com.renderx.fonts.FontConfigurationException;
import com.renderx.graphics.ImageFactory;
import com.renderx.graphics.ImageFormatException;
import com.renderx.pdflib.parser.PDFArray;
import com.renderx.pdflib.parser.PDFDictionary;
import com.renderx.pdflib.parser.PDFIndirectObject;
import com.renderx.pdflib.parser.PDFInstance;
import com.renderx.pdflib.parser.PDFStream;
import com.renderx.pdflib.parser.PDFString;
import com.renderx.pdflib.parser.PDFUniversalObject;
import com.renderx.util.Array;
import com.renderx.util.DefaultErrorHandler;
import com.renderx.util.ErrorHandler;
import com.renderx.util.Hashtable;
import com.renderx.util.List;
import com.renderx.util.SeekableFileInputStream;
import com.renderx.util.SeekableInput;
import com.renderx.util.URLCache;
import com.renderx.util.URLSpec;

import junit.framework.Assert;
import junit.framework.TestCase;

public class Linearize {
	private final FontCatalog fontCatalog;

	private final ImageFactory imageFactory;

	private final ErrorHandler errorHandler;

	OutputStream output;

	PDFInstance source;

	boolean compressed;

	boolean encrypted;

	Encrypt encrypt;

	Hashtable objs;

	Hashtable lengths;

	Hashtable streamLengths;

	Array pages;

	Hashtable pagesh;

	Hashtable firstPageSubObjects;

	Hashtable unicalSubObjects;

	Hashtable unicalSubObjectsList;

	Hashtable sharedSubObjects;

	Hashtable nonPageObjects;

	Hashtable outlines;

	Hashtable names;

	long firstPageOffset;

	long firstOutlineOffset;

	long firstNamedDestinationOffset;

	int linearize_id;

	int catalog_id;

	int info_id;

	int pages_id;

	int outlines_id;

	int names_id;

	int counter;

	long filesize;

	long tr2itemnumber;

	long tr2sizewhioutIndirectObjects;

	long xrefOffset;

	long headerSize;

	private static final class NullOutputStream extends OutputStream {
		private NullOutputStream() {
			/* empty */
		}

		public void write(int i) {
			/* empty */
		}

		public void write(byte[] is) {
			/* empty */
		}

		public void write(byte[] is, int i, int i_0_) {
			/* empty */
		}
	}

	public static class Test extends TestCase {
		String testPDFFileName = "jUnitTestSimple.pdf";

		String testPDFFileName2 = "jUnitTest2pages.pdf";

		String testPDFFileName3 = "jUnitTest2pagesandimagefor12.pdf";

		String testPDFFileName4 = "jUnitTest3pagesandimagefor23.pdf";

		String rezPDFFileName = "jUnitTestLinearized.pdf";

		ErrorHandler errorHandler = new DefaultErrorHandler();

		URLCache urlcache;

		FontCatalog fontCatalog;

		ImageFactory imageFactory;

		public void setUp() {
			try {
				urlcache = new URLCache(errorHandler);
				fontCatalog = new FontCatalog(urlcache, errorHandler);
				imageFactory = new ImageFactory(fontCatalog, urlcache,
						errorHandler);
			} catch (IOException ioexception) {
				Assert.fail("Cannot create font catalog or image factory: "
						+ ioexception);
			} catch (FontConfigurationException fontconfigurationexception) {
				Assert.fail("Cannot create font catalog: "
						+ fontconfigurationexception);
			}
		}

		void createTestPDF() throws IOException {
			PDFDocument pdfdocument = new PDFDocument(testPDFFileName,
					imageFactory, fontCatalog, errorHandler);
			pdfdocument.setCompression(false);
			pdfdocument.startDocument();
			pdfdocument.beginPage(100.0F, 100.0F, "1");
			pdfdocument.setRGBColor(0.0F, 0.0F, 0.0F);
			pdfdocument.setLineWidth(10.0F);
			pdfdocument.moveTo(10.0F, 10.0F);
			pdfdocument.lineTo(50.0F, 10.0F);
			pdfdocument.lineTo(50.0F, 50.0F);
			pdfdocument.lineTo(10.0F, 50.0F);
			pdfdocument.closePath();
			pdfdocument.fill();
			pdfdocument.endPage();
			pdfdocument.endDocument();
		}

		void createTestPDF2() throws IOException {
			PDFDocument pdfdocument = new PDFDocument(testPDFFileName2,
					imageFactory, fontCatalog, errorHandler);
			pdfdocument.setCompression(false);
			pdfdocument.startDocument();
			pdfdocument.beginPage(100.0F, 100.0F, "1");
			pdfdocument.setRGBColor(0.0F, 0.0F, 0.0F);
			pdfdocument.setLineWidth(10.0F);
			pdfdocument.moveTo(10.0F, 10.0F);
			pdfdocument.lineTo(50.0F, 10.0F);
			pdfdocument.lineTo(50.0F, 50.0F);
			pdfdocument.lineTo(10.0F, 50.0F);
			pdfdocument.fill();
			pdfdocument.closePath();
			pdfdocument.endPage();
			pdfdocument.beginPage(100.0F, 100.0F, "2");
			pdfdocument.setRGBColor(0.0F, 100.0F, 0.0F);
			pdfdocument.moveTo(20.0F, 10.0F);
			pdfdocument.lineTo(60.0F, 10.0F);
			pdfdocument.lineTo(60.0F, 50.0F);
			pdfdocument.lineTo(20.0F, 50.0F);
			pdfdocument.closePath();
			pdfdocument.fill();
			pdfdocument.endPage();
			pdfdocument.endDocument();
		}

		void createTestPDF3() throws IOException {
			PDFDocument pdfdocument = new PDFDocument(testPDFFileName3,
					imageFactory, fontCatalog, errorHandler);
			resourceToFile("com/renderx/pdflib/TEST_DATA/test_gif.gif", "1.gif");
			pdfdocument.setCompression(false);
			pdfdocument.startDocument();
			pdfdocument.beginPage(100.0F, 100.0F, "1");
			pdfdocument.setRGBColor(0.0F, 0.0F, 0.0F);
			pdfdocument.setLineWidth(10.0F);
			pdfdocument.moveTo(10.0F, 10.0F);
			pdfdocument.lineTo(50.0F, 10.0F);
			pdfdocument.lineTo(50.0F, 50.0F);
			pdfdocument.lineTo(10.0F, 50.0F);
			pdfdocument.closePath();
			pdfdocument.fill();
			pdfdocument.placeImage(new URLSpec("1.gif"), "image/gif");
			pdfdocument.endPage();
			pdfdocument.beginPage(100.0F, 100.0F, "2");
			pdfdocument.setRGBColor(0.0F, 100.0F, 0.0F);
			pdfdocument.moveTo(20.0F, 10.0F);
			pdfdocument.lineTo(60.0F, 10.0F);
			pdfdocument.lineTo(60.0F, 50.0F);
			pdfdocument.lineTo(20.0F, 50.0F);
			pdfdocument.closePath();
			pdfdocument.fill();
			pdfdocument.placeImage(new URLSpec("1.gif"), "image/gif");
			pdfdocument.endPage();
			pdfdocument.endDocument();
		}

		void createTestPDF4() throws IOException {
			PDFDocument pdfdocument = new PDFDocument(testPDFFileName4,
					imageFactory, fontCatalog, errorHandler);
			resourceToFile("com/renderx/pdflib/TEST_DATA/test_gif.gif", "1.gif");
			resourceToFile("com/renderx/pdflib/TEST_DATA/test_jpeg.jpg",
					"1.jpg");
			pdfdocument.setCompression(false);
			pdfdocument.startDocument();
			pdfdocument.beginPage(100.0F, 100.0F, "1");
			pdfdocument.setRGBColor(0.0F, 0.0F, 0.0F);
			pdfdocument.setLineWidth(10.0F);
			pdfdocument.moveTo(10.0F, 10.0F);
			pdfdocument.lineTo(50.0F, 10.0F);
			pdfdocument.lineTo(50.0F, 50.0F);
			pdfdocument.lineTo(10.0F, 50.0F);
			pdfdocument.closePath();
			pdfdocument.fill();
			pdfdocument.endPage();
			pdfdocument.beginPage(100.0F, 100.0F, "2");
			pdfdocument.setRGBColor(0.0F, 100.0F, 0.0F);
			pdfdocument.moveTo(20.0F, 10.0F);
			pdfdocument.lineTo(60.0F, 10.0F);
			pdfdocument.lineTo(60.0F, 50.0F);
			pdfdocument.lineTo(20.0F, 50.0F);
			pdfdocument.closePath();
			pdfdocument.fill();
			pdfdocument.placeImage(new URLSpec("1.gif"), "image/gif");
			pdfdocument.endPage();
			pdfdocument.beginPage(100.0F, 100.0F, "3");
			pdfdocument.setRGBColor(0.0F, 0.0F, 100.0F);
			pdfdocument.moveTo(20.0F, 10.0F);
			pdfdocument.lineTo(60.0F, 10.0F);
			pdfdocument.lineTo(60.0F, 50.0F);
			pdfdocument.lineTo(20.0F, 50.0F);
			pdfdocument.closePath();
			pdfdocument.fill();
			pdfdocument.placeImage(new URLSpec("1.gif"), "image/gif");
			pdfdocument.endPage();
			pdfdocument.endDocument();
		}

		Linearize createTestLinearize() {
			Linearize linearize = null;
			try {
				try {
					createTestPDF();
					SeekableFileInputStream seekablefileinputstream = new SeekableFileInputStream(
							testPDFFileName);
					FileOutputStream fileoutputstream = new FileOutputStream(
							rezPDFFileName);
					linearize = new Linearize(seekablefileinputstream,
							fileoutputstream, fontCatalog, urlcache,
							errorHandler);
				} catch (IOException ioexception) {
					Assert.assertTrue(false);
				}
			} catch (ImageFormatException imageformatexception) {
				Assert.assertTrue(false);
			}
			return linearize;
		}

		Linearize createTestLinearize2() {
			Linearize linearize = null;
			try {
				try {
					createTestPDF2();
					SeekableFileInputStream seekablefileinputstream = new SeekableFileInputStream(
							testPDFFileName2);
					FileOutputStream fileoutputstream = new FileOutputStream(
							rezPDFFileName);
					linearize = new Linearize(seekablefileinputstream,
							fileoutputstream, fontCatalog, urlcache,
							errorHandler);
				} catch (IOException ioexception) {
					Assert.fail("Error:" + ioexception);
				}
			} catch (ImageFormatException imageformatexception) {
				Assert.assertTrue(false);
			}
			return linearize;
		}

		Linearize createTestLinearize3() {
			Linearize linearize = null;
			try {
				try {
					createTestPDF3();
					SeekableFileInputStream seekablefileinputstream = new SeekableFileInputStream(
							testPDFFileName3);
					FileOutputStream fileoutputstream = new FileOutputStream(
							rezPDFFileName);
					linearize = new Linearize(seekablefileinputstream,
							fileoutputstream, fontCatalog, urlcache,
							errorHandler);
				} catch (IOException ioexception) {
					Assert.fail("Error:" + ioexception);
				}
			} catch (ImageFormatException imageformatexception) {
				Assert.assertTrue(false);
			}
			return linearize;
		}

		Linearize createTestLinearize4() {
			Linearize linearize = null;
			try {
				try {
					createTestPDF4();
					SeekableFileInputStream seekablefileinputstream = new SeekableFileInputStream(
							testPDFFileName4);
					FileOutputStream fileoutputstream = new FileOutputStream(
							rezPDFFileName);
					linearize = new Linearize(seekablefileinputstream,
							fileoutputstream, fontCatalog, urlcache,
							errorHandler);
				} catch (IOException ioexception) {
					Assert.fail("Error:" + ioexception);
				}
			} catch (ImageFormatException imageformatexception) {
				Assert.assertTrue(false);
			}
			return linearize;
		}

		public void testAddHashtableEmpty(Linearize linearize) {
			Hashtable hashtable = new Hashtable();
			Hashtable hashtable_1_ = new Hashtable();
			Hashtable hashtable_2_ = linearize.addHashtable(hashtable,
					hashtable_1_);
			if (hashtable_2_.size() != 0)
				Assert.fail("Size of rezult Hashtable " + hashtable_2_.size());
		}

		public void testAddHashtableDifferent(Linearize linearize) {
			Hashtable hashtable = new Hashtable();
			hashtable.put("1", "");
			hashtable.put("2", "");
			Hashtable hashtable_3_ = new Hashtable();
			hashtable_3_.put("3", "");
			hashtable_3_.put("4", "");
			Hashtable hashtable_4_ = linearize.addHashtable(hashtable,
					hashtable_3_);
			for (int i = 1; i < 5; i++) {
				if (!hashtable_4_.containsKey("" + i))
					Assert.fail("Can not find key " + i);
			}
			if (hashtable_4_.size() != 4)
				Assert.fail("Size of rezult Hashtable " + hashtable_4_.size());
		}

		public void testAddHashtableSame(Linearize linearize) {
			Hashtable hashtable = new Hashtable();
			hashtable.put("1", "");
			hashtable.put("2", "");
			Hashtable hashtable_5_ = new Hashtable();
			hashtable_5_.put("2", "");
			Hashtable hashtable_6_ = linearize.addHashtable(hashtable,
					hashtable_5_);
			if (!hashtable_6_.containsKey("1")
					|| !hashtable_6_.containsKey("2")
					|| hashtable_6_.size() != 2)
				Assert.assertTrue(false);
			hashtable = new Hashtable();
			hashtable.put("1", "");
			hashtable_5_ = new Hashtable();
			hashtable_5_.put("1", "");
			hashtable_5_.put("2", "");
			hashtable_6_ = linearize.addHashtable(hashtable, hashtable_5_);
			for (int i = 1; i < 2; i++) {
				if (!hashtable_6_.containsKey("" + i))
					Assert.fail("Can not find key " + i);
			}
			if (hashtable_6_.size() != 2)
				Assert.fail("Size of rezult Hashtable " + hashtable_6_.size());
		}

		public void testAddHashtable() {
			Linearize linearize = createTestLinearize();
			testAddHashtableEmpty(linearize);
			testAddHashtableDifferent(linearize);
			testAddHashtableSame(linearize);
		}

		public void testGetSubObjectsFromArray() {
			Linearize linearize = createTestLinearize();
			linearize.objs = new Hashtable();
			PDFUniversalObject pdfuniversalobject = new PDFUniversalObject(1);
			PDFArray pdfarray = new PDFArray();
			pdfarray.array.put(0, new PDFIndirectObject("2"));
			pdfarray.array.put(1, new PDFString("3"));
			pdfarray.array.put(2, new PDFIndirectObject("3"));
			pdfuniversalobject.objdata = pdfarray;
			if (pdfuniversalobject.getIndirectObjects().isEmpty())
				Assert.fail("Test PDFArray don`t contains indirect objects!");
			linearize.objs.put(new Integer(1), pdfuniversalobject);
			PDFUniversalObject pdfuniversalobject_7_ = new PDFUniversalObject(2);
			pdfuniversalobject_7_.objdata = new PDFString("1");
			linearize.objs.put(new Integer(pdfuniversalobject_7_.obj_id),
					pdfuniversalobject_7_);
			pdfuniversalobject_7_ = new PDFUniversalObject(3);
			pdfuniversalobject_7_.objdata = new PDFString("1");
			linearize.objs.put(new Integer(pdfuniversalobject_7_.obj_id),
					pdfuniversalobject_7_);
			Hashtable hashtable = linearize.getSubObjects(1);
			if (hashtable.size() != 2)
				Assert.fail("Number of subobjects is  " + hashtable.size());
			for (int i = 2; i < 4; i++) {
				if (!hashtable.containsKey(new Integer(i)))
					Assert.fail("Can not find indirect object " + i);
			}
		}

		public void testGetSubObjectsFromDictionary() {
			Linearize linearize = createTestLinearize();
			linearize.objs = new Hashtable();
			PDFUniversalObject pdfuniversalobject = new PDFUniversalObject(1);
			PDFArray pdfarray = new PDFArray();
			pdfarray.array.put(0, new PDFIndirectObject("2"));
			pdfarray.array.put(1, new PDFString("3"));
			pdfarray.array.put(2, new PDFIndirectObject("3"));
			PDFIndirectObject pdfindirectobject = new PDFIndirectObject("4");
			PDFDictionary pdfdictionary = new PDFDictionary();
			pdfdictionary.dict.put("/1", pdfarray);
			pdfdictionary.dict.put("/2", pdfindirectobject);
			pdfdictionary.dict.put("/3", new PDFString("3"));
			pdfuniversalobject.objdata = pdfdictionary;
			if (pdfuniversalobject.getIndirectObjects().isEmpty())
				Assert.fail("Test PDFArray don`t contains indirect objects!");
			linearize.objs.put(new Integer(1), pdfuniversalobject);
			PDFUniversalObject pdfuniversalobject_8_ = new PDFUniversalObject(2);
			pdfuniversalobject_8_.objdata = new PDFString("1");
			linearize.objs.put(new Integer(2), pdfuniversalobject_8_);
			pdfuniversalobject_8_ = new PDFUniversalObject(3);
			pdfuniversalobject_8_.objdata = new PDFString("1");
			linearize.objs.put(new Integer(3), pdfuniversalobject_8_);
			pdfuniversalobject_8_ = new PDFUniversalObject(4);
			pdfuniversalobject_8_.objdata = new PDFIndirectObject("5");
			linearize.objs.put(new Integer(4), pdfuniversalobject_8_);
			pdfuniversalobject_8_ = new PDFUniversalObject(5);
			pdfuniversalobject_8_.objdata = new PDFString("1");
			linearize.objs.put(new Integer(5), pdfuniversalobject_8_);
			Hashtable hashtable = null;
			try {
				hashtable = linearize.getSubObjects(1);
			} catch (Exception exception) {
				exception.printStackTrace();
				Assert.fail("Error " + exception);
			}
			if (hashtable.size() != 4)
				Assert.fail("Number of subobjects is  " + hashtable.size());
			for (int i = 2; i < 6; i++) {
				if (!hashtable.containsKey(new Integer(i)))
					Assert.fail("Can not find key " + i);
			}
		}

		public void testGetSubObjectsFromIndirectObject() {
			Linearize linearize = createTestLinearize();
			linearize.objs = new Hashtable();
			PDFUniversalObject pdfuniversalobject = new PDFUniversalObject(1);
			PDFIndirectObject pdfindirectobject = new PDFIndirectObject("2");
			if (pdfindirectobject.getIndirectObjects().isEmpty())
				Assert
						.fail("Test PDFIndirectObject don`t contains indirect objects!");
			pdfuniversalobject.objdata = pdfindirectobject;
			if (pdfuniversalobject.getIndirectObjects().isEmpty())
				Assert
						.fail("Test PDFUniversalObject don`t contains indirect objects!");
			linearize.objs.put(new Integer(1), pdfuniversalobject);
			PDFUniversalObject pdfuniversalobject_9_ = new PDFUniversalObject(2);
			pdfuniversalobject_9_.objdata = new PDFString("1");
			linearize.objs.put(new Integer(2), pdfuniversalobject_9_);
			Hashtable hashtable = linearize.getSubObjects(1);
			if (hashtable.size() != 1)
				Assert.fail("Number of subobjects is  " + hashtable.size());
			if (!hashtable.containsKey(new Integer(2)))
				Assert.fail("Indirect object 2 not founded.");
		}

		public void testReadPages() {
			Linearize linearize = createTestLinearize();
			try {
				linearize.readObjects();
			} catch (Exception exception) {
				exception.printStackTrace(System.err);
				Assert.fail("Error in reading objects " + exception);
			}
			linearize.readPages();
			if (linearize.pages.length() != 1)
				Assert
						.fail("Wrong number of pages "
								+ linearize.pages.length());
			if (((Integer) linearize.pages.get(0)).intValue() != 4)
				Assert.fail("Wrong id of page " + linearize.pages.get(0));
		}

		public void testSeparateObjects() {
			Linearize linearize = createTestLinearize();
			try {
				linearize.readObjects();
			} catch (Exception exception) {
				Assert.fail("Error in reading objects " + exception);
			}
			linearize.readPages();
			linearize.separateSubobjects();
			if (linearize.firstPageSubObjects.size() != 5)
				Assert.fail("Wrong number of first page subobjects "
						+ linearize.firstPageSubObjects.size());
			for (int i = 5; i < 10; i++) {
				if (!linearize.firstPageSubObjects.containsKey(new Integer(i)))
					Assert.fail("Can not find first page subobject " + i);
			}
			if (linearize.unicalSubObjects.size() != 0)
				Assert.fail("Wrong number of unical subobjects "
						+ linearize.unicalSubObjects.size());
			if (linearize.sharedSubObjects.size() != 0)
				Assert.fail("Wrong number of shared subobjects  "
						+ linearize.sharedSubObjects.size());
			if (linearize.nonPageObjects.size() != 2)
				Assert.fail("Wrong number of non page objects "
						+ linearize.nonPageObjects.size());
			for (int i = 2; i < 4; i++) {
				if (!linearize.nonPageObjects.containsKey(new Integer(i)))
					Assert.fail("Can not find non page object " + i);
			}
		}

		public void testRenumberObjects() {
			Linearize linearize = createTestLinearize();
			try {
				linearize.readObjects();
			} catch (Exception exception) {
				Assert.fail("Error in reading objects " + exception);
			}
			linearize.readPages();
			linearize.separateSubobjects();
			linearize.renumberObjects();
		}

		public Linearize createAndRenumber() {
			Linearize linearize = createTestLinearize();
			try {
				linearize.readObjects();
			} catch (Exception exception) {
				Assert.fail("Error in reading objects " + exception);
			}
			linearize.readPages();
			linearize.separateSubobjects();
			linearize.renumberObjects();
			return linearize;
		}

		public Linearize createAndRenumber2() {
			Linearize linearize = createTestLinearize2();
			try {
				linearize.readObjects();
			} catch (Exception exception) {
				Assert.fail("Error in reading objects " + exception);
			}
			linearize.readPages();
			linearize.separateSubobjects();
			linearize.renumberObjects();
			return linearize;
		}

		public Linearize createAndRenumber3() {
			Linearize linearize = createTestLinearize3();
			try {
				linearize.readObjects();
			} catch (Exception exception) {
				Assert.fail("Error in reading objects " + exception);
			}
			linearize.readPages();
			linearize.separateSubobjects();
			linearize.renumberObjects();
			return linearize;
		}

		public Linearize createAndRenumber4() {
			Linearize linearize = createTestLinearize4();
			try {
				linearize.readObjects();
			} catch (Exception exception) {
				Assert.fail("Error in reading objects " + exception);
			}
			linearize.readPages();
			linearize.separateSubobjects();
			linearize.renumberObjects();
			return linearize;
		}

		public void testcreateLinearizedDictionary() {
			Linearize linearize = createAndRenumber();
			Linearized linearized = linearize
					.createLinearizedDictionary(new IndirectObject(1));
			if (linearized.firstPageID != 5)
				Assert.fail("Wrong firstPageID " + linearized.firstPageID);
			if (linearized.numberOfPages != 1)
				Assert.fail("Wrong numberOfPages " + linearized.numberOfPages);
		}

		public void testcreateHintStreamDictionary() {
			Linearize linearize = createAndRenumber();
			PDFDocument pdfdocument = null;
			try {
				pdfdocument = linearize.createDestination();
			} catch (Exception exception) {
				Assert.fail("Error in createDestination " + exception);
			}
			try {
				linearize.calcSizeOfObjects();
			} catch (Exception exception) {
				Assert.fail("Error in calcSizeOfObjects " + exception);
			}
			HintStream hintstream = (linearize.createFirstHintStream(
					new IndirectObject(linearize.counter++), pdfdocument));
			Linearized linearized = (linearize
					.createLinearizedDictionary(new IndirectObject(
							linearize.counter++)));
			PDFTrailer pdftrailer = linearize.createFirstTrailer(linearized,
					null);
			linearize.appendLinearizedData(linearized,
					pdftrailer.xref.xrefSize, hintstream);
			if (hintstream.pageOffset.leastNumber != 6)
				Assert.fail("Wrong least number of Page Hint Table "
						+ hintstream.pageOffset.leastNumber);
		}

		public void testFirstPageWithImagePDF() {
			Linearize linearize = createAndRenumber3();
			if (linearize.firstPageSubObjects.size() != 9)
				Assert.fail("Wrong number of first page subobjects "
						+ linearize.firstPageSubObjects.size());
			if (linearize.unicalSubObjects.size() != 1)
				Assert.fail("Wrong number of unical subobjects "
						+ linearize.unicalSubObjects.size());
			Hashtable hashtable = (Hashtable) linearize.unicalSubObjects
					.get(new Integer(1));
			if (hashtable.size() != 5)
				Assert.fail("Wrong number of unical subobjects for page 1 - "
						+ hashtable.size());
			Enumeration enumeration = hashtable.keys();
			while (enumeration.hasMoreElements()) {
				Integer integer = (Integer) enumeration.nextElement();
				if (integer.intValue() < 10 || integer.intValue() > 19)
					Assert
							.fail("Wrong number of unical subobject for page 1 - "
									+ integer);
				if (linearize.source.newID(integer).intValue() > 6)
					Assert
							.fail("Wrong new number of unical subobject for page 1 - "
									+ integer
									+ " -> "
									+ linearize.source.newID(integer));
			}
			if (linearize.sharedSubObjects.size() != 0)
				Assert.fail("Wrong number of shared subobjects  "
						+ linearize.sharedSubObjects.size());
			if (linearize.nonPageObjects.size() != 2)
				Assert.fail("Wrong number of non page objects "
						+ linearize.nonPageObjects.size());
			for (int i = 2; i < 4; i++) {
				if (!linearize.nonPageObjects.containsKey(new Integer(i)))
					Assert.fail("Can not find non page object " + i);
			}
		}

		public void testPDFWithSharedObjects() {
			Linearize linearize = createAndRenumber4();
			if (linearize.firstPageSubObjects.size() != 5)
				Assert.fail("Wrong number of first page subobjects "
						+ linearize.firstPageSubObjects.size());
			for (int i = 5; i < 8; i++) {
				if (!linearize.firstPageSubObjects.containsKey(new Integer(i)))
					Assert.fail("Can not find first page subobject " + i);
			}
			if (linearize.unicalSubObjects.size() != 2)
				Assert.fail("Wrong number of unical subobjects "
						+ linearize.unicalSubObjects.size());
			Hashtable hashtable = (Hashtable) linearize.unicalSubObjects
					.get(new Integer(1));
			if (hashtable.size() != 5)
				Assert.fail("Wrong number of unical subobjects for page 1 - "
						+ hashtable.size());
			Enumeration enumeration = hashtable.keys();
			while (enumeration.hasMoreElements()) {
				Integer integer = (Integer) enumeration.nextElement();
				if (integer.intValue() < 9 || integer.intValue() > 23)
					Assert
							.fail("Wrong number of unical subobject for page 1 - "
									+ integer);
				if (linearize.source.newID(integer).intValue() > 6)
					Assert
							.fail("Wrong new number of unical subobject for page 1 - "
									+ integer
									+ " -> "
									+ linearize.source.newID(integer));
			}
			hashtable = (Hashtable) linearize.unicalSubObjects.get(new Integer(
					2));
			if (hashtable.size() != 5)
				Assert.fail("Wrong number of unical subobjects for page 2 - "
						+ linearize.unicalSubObjects.size());
			enumeration = hashtable.keys();
			while (enumeration.hasMoreElements()) {
				Integer integer = (Integer) enumeration.nextElement();
				if (integer.intValue() < 14 || integer.intValue() > 25)
					Assert
							.fail("Wrong number of unical subobject for page 2 - "
									+ integer);
				if (linearize.source.newID(integer).intValue() < 8
						|| linearize.source.newID(integer).intValue() > 12)
					Assert
							.fail("Wrong new number of unical subobject for page 2 - "
									+ integer
									+ " -> "
									+ linearize.source.newID(integer));
			}
			if (linearize.sharedSubObjects.size() != 4)
				Assert.fail("Wrong number of shared subobjects  "
						+ linearize.sharedSubObjects.size());
			if (linearize.nonPageObjects.size() != 2)
				Assert.fail("Wrong number of non page objects "
						+ linearize.nonPageObjects.size());
			for (int i = 2; i < 4; i++) {
				if (!linearize.nonPageObjects.containsKey(new Integer(i)))
					Assert.fail("Can not find non page object " + i);
			}
		}

		public void test2PagePDF() {
			Linearize linearize = createAndRenumber2();
			if (linearize.firstPageSubObjects.size() != 5)
				Assert.fail("Wrong number of first page subobjects "
						+ linearize.firstPageSubObjects.size());
			if (linearize.unicalSubObjects.size() != 1)
				Assert.fail("Wrong number of unical subobjects "
						+ linearize.unicalSubObjects.size());
			Hashtable hashtable = (Hashtable) linearize.unicalSubObjects
					.get(new Integer(1));
			if (hashtable.size() != 5)
				Assert.fail("Wrong number of unical subobjects for page 1 - "
						+ hashtable.size());
			Enumeration enumeration = hashtable.keys();
			while (enumeration.hasMoreElements()) {
				Integer integer = (Integer) enumeration.nextElement();
				if (integer.intValue() < 9 || integer.intValue() > 15)
					Assert
							.fail("Wrong number of unical subobject for page 1 - "
									+ integer);
				if (linearize.source.newID(integer).intValue() < 2
						|| linearize.source.newID(integer).intValue() > 6)
					Assert
							.fail("Wrong new number of unical subobject for page 1 - "
									+ integer
									+ " -> "
									+ linearize.source.newID(integer));
			}
			if (linearize.sharedSubObjects.size() != 0)
				Assert.fail("Wrong number of shared subobjects  "
						+ linearize.sharedSubObjects.size());
			if (linearize.nonPageObjects.size() != 2)
				Assert.fail("Wrong number of non page objects "
						+ linearize.nonPageObjects.size());
			for (int i = 2; i < 4; i++) {
				if (!linearize.nonPageObjects.containsKey(new Integer(i)))
					Assert.fail("Can not find non page object " + i);
			}
		}

		private void resourceToFile(String string, String string_10_)
				throws IOException {
			InputStream inputstream = ClassLoader
					.getSystemResourceAsStream(string);
			FileOutputStream fileoutputstream = new FileOutputStream(string_10_);
			byte[] is = new byte[1024];
			int i;
			while ((i = inputstream.read(is, 0, 1024)) >= 0)
				fileoutputstream.write(is, 0, i);
			fileoutputstream.close();
		}
	}

	protected Linearize(SeekableInput seekableinput, OutputStream outputstream)
			throws IOException, ImageFormatException,
			FontConfigurationException {
		this(seekableinput, outputstream, new DefaultErrorHandler());
	}

	private Linearize(SeekableInput seekableinput, OutputStream outputstream,
			ErrorHandler errorhandler) throws IOException,
			ImageFormatException, FontConfigurationException {
		this(seekableinput, outputstream, new URLCache(errorhandler),
				errorhandler);
	}

	private Linearize(SeekableInput seekableinput, OutputStream outputstream,
			URLCache urlcache, ErrorHandler errorhandler) throws IOException,
			ImageFormatException, FontConfigurationException {
		this(seekableinput, outputstream, new FontCatalog(urlcache,
				errorhandler), urlcache, errorhandler);
	}

	private Linearize(SeekableInput seekableinput, OutputStream outputstream,
			FontCatalog fontcatalog, URLCache urlcache,
			ErrorHandler errorhandler) throws IOException, ImageFormatException {
		this(seekableinput, outputstream, new ImageFactory(fontcatalog,
				urlcache, errorhandler), fontcatalog, errorhandler);
	}

	public Linearize(SeekableInput seekableinput, OutputStream outputstream,
			ImageFactory imagefactory, FontCatalog fontcatalog,
			ErrorHandler errorhandler) throws IOException, ImageFormatException {
		imageFactory = imagefactory;
		fontCatalog = fontcatalog;
		errorHandler = errorhandler;
		source = new PDFInstance();
		source.parse(seekableinput);
		source.renumber = false;
		source.converObjects.put("0", new Integer(0));
		pagesh = new Hashtable();
		output = outputstream;
		encrypt = null;
	}

	public void setUseCompression(boolean bool) {
		compressed = bool;
	}

	public void setUseEncryption(Encrypt encrypt) {
		encrypted = true;
		this.encrypt = encrypt;
	}

	public void setUseEncryption(String string, String string_11_, int i) {
		encrypted = true;
		encrypt = new Encrypt(new IndirectObject(0), string, string_11_, i,
				output.toString());
	}

	Hashtable addHashtable(Hashtable hashtable, Hashtable hashtable_12_) {
		Enumeration enumeration = hashtable_12_.keys();
		while (enumeration.hasMoreElements()) {
			Object object = enumeration.nextElement();
			hashtable.put(object, hashtable_12_.get(object));
		}
		return hashtable;
	}

	Hashtable getSubObjects(int i) {
		PDFUniversalObject pdfuniversalobject = (PDFUniversalObject) objs
				.get(new Integer(i));
		if (pdfuniversalobject == null)
			throw new PDFException("Can not find object " + i);
		List list = pdfuniversalobject.getIndirectObjects();
		Hashtable hashtable = new Hashtable();
		while (!list.isEmpty()) {
			Integer integer = (Integer) list.shift();
			PDFUniversalObject pdfuniversalobject_13_ = (PDFUniversalObject) objs
					.get(integer);
			if (!pdfuniversalobject_13_.getType().equals("/Pages")
					&& !pdfuniversalobject_13_.getType().equals("/Page")) {
				hashtable.put(integer, "");
				hashtable = addHashtable(hashtable, getSubObjects(integer
						.intValue()));
			}
		}
		return hashtable;
	}

	void separateSubobjects() {
		firstPageSubObjects = new Hashtable();
		unicalSubObjects = new Hashtable();
		unicalSubObjectsList = new Hashtable();
		sharedSubObjects = new Hashtable();
		nonPageObjects = new Hashtable();
		firstPageSubObjects = getSubObjects(((Integer) pages.get(0)).intValue());
		Hashtable hashtable = new Hashtable();
		for (int i = 1; i < pages.length(); i++)
			hashtable.put(new Integer(i),
					getSubObjects(((Integer) pages.get(i)).intValue()));
		for (int i = 1; i < pages.length(); i++) {
			Hashtable hashtable_14_ = (Hashtable) hashtable.get(new Integer(i));
			Enumeration enumeration = hashtable_14_.keys();
			Hashtable hashtable_15_ = new Hashtable();
			Hashtable hashtable_16_ = new Hashtable();
			while (enumeration.hasMoreElements()) {
				Integer integer = (Integer) enumeration.nextElement();
				if (!firstPageSubObjects.containsKey(integer)
						&& !outlines.containsKey(integer)
						&& !names.containsKey(integer)) {
					boolean bool = true;
					for (int i_17_ = 1; i_17_ < pages.length() && bool; i_17_++) {
						if (i_17_ != i) {
							Hashtable hashtable_18_ = ((Hashtable) hashtable
									.get(new Integer(i_17_)));
							Enumeration enumeration_19_ = hashtable_18_.keys();
							while (enumeration_19_.hasMoreElements() && bool) {
								Integer integer_20_ = (Integer) enumeration_19_
										.nextElement();
								if (integer.intValue() == integer_20_
										.intValue())
									bool = false;
							}
						}
					}
					if (bool)
						hashtable_15_.put(integer, "");
					else
						hashtable_16_.put(integer, "");
				}
			}
			unicalSubObjects.put(new Integer(i), hashtable_15_);
			unicalSubObjectsList = addHashtable(unicalSubObjectsList,
					hashtable_15_);
			sharedSubObjects = addHashtable(sharedSubObjects, hashtable_16_);
		}
		Enumeration enumeration = objs.keys();
		while (enumeration.hasMoreElements()) {
			Integer integer = (Integer) enumeration.nextElement();
			if (!firstPageSubObjects.containsKey(integer)
					&& !outlines.containsKey(integer)
					&& !names.containsKey(integer)
					&& !unicalSubObjectsList.containsKey(integer)
					&& !sharedSubObjects.containsKey(integer)
					&& !pagesh.containsKey(integer)
					&& !((PDFUniversalObject) objs.get(integer)).getType()
							.equals("/Catalog")) {
				if (((PDFUniversalObject) objs.get(integer)).objdata instanceof PDFDictionary) {
					if (!((PDFDictionary) ((PDFUniversalObject) objs
							.get(integer)).objdata).dict
							.containsKey("/Linearized")
							&& !((PDFDictionary) ((PDFUniversalObject) objs
									.get(integer)).objdata).dict
									.containsKey("/S"))
						nonPageObjects.put(integer, "");
				} else
					nonPageObjects.put(integer, "");
			}
		}
	}

	Array getPages(int i) {
		PDFUniversalObject pdfuniversalobject = (PDFUniversalObject) objs
				.get(new Integer(i));
		Array array = (((PDFArray) ((PDFDictionary) pdfuniversalobject.objdata).dict
				.get("/Kids")).array);
		Array array_21_ = new Array();
		int i_22_ = 0;
		int i_23_ = array.length();
		for (int i_24_ = 0; i_24_ < i_23_; i_24_++) {
			PDFUniversalObject pdfuniversalobject_25_ = getObject((PDFIndirectObject) array
					.get(i_24_));
			String string = pdfuniversalobject_25_.getType();
			if (string.equals("/Page")) {
				array_21_
						.put(i_22_, new Integer(pdfuniversalobject_25_.obj_id));
				i_22_++;
				pagesh.put(new Integer(pdfuniversalobject_25_.obj_id), "");
			} else if (string.equals("/Pages")) {
				Array array_26_ = getPages(pdfuniversalobject_25_.obj_id);
				int i_27_ = array_26_.length();
				for (int i_28_ = 0; i_28_ < i_27_; i_28_++) {
					array_21_.put(i_22_, array_26_.get(i_28_));
					i_22_++;
				}
			}
		}
		return array_21_;
	}

	void getOutlines(int i) {
		if (i != 0) {
			PDFUniversalObject pdfuniversalobject = (PDFUniversalObject) objs
					.get(new Integer(i));
			outlines.put(new Integer(i), "");
			if (pdfuniversalobject != null) {
				if (((PDFDictionary) pdfuniversalobject.objdata).dict
						.containsKey("/First")) {
					int i_29_ = ((PDFIndirectObject) ((PDFDictionary) pdfuniversalobject.objdata).dict
							.get("/First")).intValue();
					getOutlines(i_29_);
				}
				if (((PDFDictionary) pdfuniversalobject.objdata).dict
						.containsKey("/Next")) {
					int i_30_ = ((PDFIndirectObject) ((PDFDictionary) pdfuniversalobject.objdata).dict
							.get("/Next")).intValue();
					getOutlines(i_30_);
				}
			}
		}
	}

	void getNamedDestinations(int i) {
		PDFUniversalObject pdfuniversalobject = (PDFUniversalObject) objs
				.get(new Integer(i));
		names.put(new Integer(i), "");
		if (pdfuniversalobject != null) {
			if (((PDFDictionary) pdfuniversalobject.objdata).dict
					.containsKey("/Dests")) {
				int i_31_ = ((PDFIndirectObject) ((PDFDictionary) pdfuniversalobject.objdata).dict
						.get("/Dests")).intValue();
				getNamedDestinations(i_31_);
			}
			if (((PDFDictionary) pdfuniversalobject.objdata).dict
					.containsKey("/Kids")) {
				PDFArray pdfarray = (PDFArray) ((PDFDictionary) pdfuniversalobject.objdata).dict
						.get("/Kids");
				for (int i_32_ = 0; i_32_ < pdfarray.array.length(); i_32_++) {
					PDFIndirectObject pdfindirectobject = (PDFIndirectObject) pdfarray.array
							.get(i_32_);
					getNamedDestinations(pdfindirectobject.intValue());
				}
			}
			if (((PDFDictionary) pdfuniversalobject.objdata).dict
					.containsKey("/Names")) {
				PDFArray pdfarray = (PDFArray) ((PDFDictionary) pdfuniversalobject.objdata).dict
						.get("/Names");
				for (int i_33_ = 0; i_33_ < pdfarray.array.length() / 2; i_33_++) {
					PDFIndirectObject pdfindirectobject = ((PDFIndirectObject) pdfarray.array
							.get(2 * i_33_ + 1));
					names.put(new Integer(pdfindirectobject.intValue()), "");
				}
			}
		}
	}

	void readPages() {
		PDFUniversalObject pdfuniversalobject = (PDFUniversalObject) objs
				.get(new Integer(catalog_id));
		pages_id = ((PDFIndirectObject) ((PDFDictionary) pdfuniversalobject.objdata).dict
				.get("/Pages")).intValue();
		pages = getPages(pages_id);
		outlines = new Hashtable();
		names = new Hashtable();
		if (((PDFDictionary) pdfuniversalobject.objdata).dict
				.containsKey("/Outlines")) {
			outlines_id = ((PDFIndirectObject) ((PDFDictionary) pdfuniversalobject.objdata).dict
					.get("/Outlines")).intValue();
			getOutlines(outlines_id);
		}
		if (((PDFDictionary) pdfuniversalobject.objdata).dict
				.containsKey("/Names")) {
			names_id = ((PDFIndirectObject) ((PDFDictionary) pdfuniversalobject.objdata).dict
					.get("/Names")).intValue();
			getNamedDestinations(names_id);
		}
	}

	PDFUniversalObject getObject(PDFIndirectObject pdfindirectobject) {
		return getObject(pdfindirectobject.intValue());
	}

	PDFUniversalObject getObject(int i) {
		return (PDFUniversalObject) objs.get(new Integer(i));
	}

	void readObjects() throws IOException, ImageFormatException {
		Enumeration enumeration = source.xRefTable.obj_offset.keys();
		objs = new Hashtable();
		while (enumeration.hasMoreElements()) {
			Integer integer = (Integer) enumeration.nextElement();
			PDFUniversalObject pdfuniversalobject = source.getObject(integer
					.intValue());
			objs.put(integer, pdfuniversalobject);
		}
		catalog_id = source.xRefTable.getRoot();
		info_id = source.xRefTable.getInfo();
	}

	void renumberObjects() {
		counter = 1;
		for (int i = 1; i < pages.length(); i++) {
			Integer integer = (Integer) pages.get(i);
			counter = source.addNewID(integer, counter);
			Hashtable hashtable = (Hashtable) unicalSubObjects.get(new Integer(
					i));
			if (hashtable != null) {
				Enumeration enumeration = hashtable.keys();
				while (enumeration.hasMoreElements())
					counter = source.addNewID((Integer) enumeration
							.nextElement(), counter);
			}
		}
		Enumeration enumeration = sharedSubObjects.keys();
		while (enumeration.hasMoreElements())
			counter = source.addNewID((Integer) enumeration.nextElement(),
					counter);
		counter = source.addNewID(pages_id, counter);
		counter = source.addNewID(info_id, counter);
		enumeration = nonPageObjects.keys();
		while (enumeration.hasMoreElements()) {
			Integer integer = (Integer) enumeration.nextElement();
			counter = source.addNewID(integer, counter);
		}
		linearize_id = counter++;
		counter = source.addNewID(catalog_id, counter);
		counter = source.addNewID((Integer) pages.get(0), counter);
		enumeration = outlines.keys();
		while (enumeration.hasMoreElements())
			counter = source.addNewID((Integer) enumeration.nextElement(),
					counter);
		enumeration = names.keys();
		while (enumeration.hasMoreElements())
			counter = source.addNewID((Integer) enumeration.nextElement(),
					counter);
		enumeration = firstPageSubObjects.keys();
		while (enumeration.hasMoreElements())
			counter = source.addNewID((Integer) enumeration.nextElement(),
					counter);
		firstPageSubObjects = addHashtable(firstPageSubObjects, outlines);
		firstPageSubObjects = addHashtable(firstPageSubObjects, names);
	}

	void calcSizeOfObjects() throws IOException, ImageFormatException {
		Enumeration enumeration = objs.keys();
		PDFDocument pdfdocument = new PDFDocument(new NullOutputStream(),
				imageFactory, fontCatalog, errorHandler);
		pdfdocument.setCompression(compressed);
		if (encrypted) {
			pdfdocument.outputPDF.encrypt = encrypt;
			pdfdocument.outputPDF.useEncryption = true;
		}
		lengths = new Hashtable();
		streamLengths = new Hashtable();
		source.renumber = true;
		PDFTrailer pdftrailer = new PDFTrailer();
		tr2itemnumber = 0L;
		while (enumeration.hasMoreElements()) {
			Integer integer = (Integer) enumeration.nextElement();
			Integer integer_34_ = source.newID(integer);
			if (integer_34_ != null) {
				PDFUniversalObject pdfuniversalobject = (PDFUniversalObject) objs
						.get(integer);
				if (!firstPageSubObjects.containsKey(new Integer(
						pdfuniversalobject.obj_id))
						&& pdfuniversalobject.obj_id != catalog_id)
					tr2itemnumber++;
				pdfuniversalobject.setID(integer_34_.intValue());
				if (pdfuniversalobject.objdata instanceof PDFStream)
					pdfuniversalobject.objdata = source
							.getStream(pdfuniversalobject);
				int i = pdfdocument.outputPDF.size();
				pdfuniversalobject.write(pdfdocument.outputPDF, source);
				lengths.put(integer, new Long((long) (pdfdocument.outputPDF
						.size() - i)));
				if (pdfuniversalobject.objdata instanceof PDFStream)
					streamLengths
							.put(
									integer,
									new Long(
											((PDFStream) pdfuniversalobject.objdata).stream_size));
			}
		}
		Linearize linearize_35_ = this;
		long l = tr2itemnumber;
		if (pdftrailer != null) {
			/* empty */
		}
		linearize_35_.tr2sizewhioutIndirectObjects = (l * PDFXRefTable.itemSize
				+ (long) (("" + tr2itemnumber).length() * 2) + 62L);
		filesize = (long) pdfdocument.outputPDF.size();
		pdfdocument.outputPDF.close();
	}

	PDFDocument createDestination() throws IOException, ImageFormatException {
		PDFDocument pdfdocument = new PDFDocument(output, imageFactory,
				fontCatalog, errorHandler);
		pdfdocument.setCompression(compressed);
		pdfdocument.startDocument();
		return pdfdocument;
	}

	Linearized createLinearizedDictionary(IndirectObject indirectobject) {
		Linearized linearized = new Linearized(indirectobject);
		linearized.firstPageID = source.newID((Integer) pages.get(0))
				.intValue();
		linearized.numberOfPages = pagesh.size();
		return linearized;
	}

	void calcFirstLocations() {
		PDFTrailer pdftrailer = new PDFTrailer();
		PDFXRefTable pdfxreftable = pdftrailer.xref;
		if (pdftrailer != null) {
			/* empty */
		}
		long l = PDFXRefTable.bodySize;
		if (pdftrailer != null) {
			/* empty */
		}
		pdfxreftable.xrefSize = l + PDFXRefTable.itemSize
				* (long) (firstPageSubObjects.size() + 3);
		headerSize = Linearized.smallHeaderSize + pdftrailer.xref.xrefSize;
		firstPageOffset = (headerSize + ((Long) lengths.get(new Integer(
				catalog_id))).longValue());
		firstOutlineOffset = firstPageOffset
				+ ((Long) lengths.get(pages.get(0))).longValue();
		firstNamedDestinationOffset = firstOutlineOffset;
		Enumeration enumeration = outlines.keys();
		while (enumeration.hasMoreElements()) {
			Integer integer = (Integer) enumeration.nextElement();
			firstNamedDestinationOffset += ((Long) lengths.get(source
					.newID(integer))).longValue();
		}
	}

	PDFTrailer createFirstTrailer(Linearized linearized, byte[] is) {
		PDFTrailer pdftrailer = new PDFTrailer();
		PDFXRefTable pdfxreftable = pdftrailer.xref;
		if (pdftrailer != null) {
			/* empty */
		}
		long l = PDFXRefTable.bodySize;
		if (pdftrailer != null) {
			/* empty */
		}
		pdfxreftable.xrefSize = l + PDFXRefTable.itemSize
				* (long) (firstPageSubObjects.size() + 3);
		if (encrypted) {
			PDFXRefTable pdfxreftable_36_ = pdftrailer.xref;
			long l_37_ = pdfxreftable_36_.xrefSize;
			if (pdftrailer != null) {
				/* empty */
			}
			pdfxreftable_36_.xrefSize = l_37_ + PDFXRefTable.itemSize;
		}
		headerSize = Linearized.smallHeaderSize + pdftrailer.xref.xrefSize;
		long l_38_ = headerSize;
		if (encrypted) {
			pdftrailer.xref.addObjectOffset(new IndirectObject(
					encrypt.obj_id.num), l_38_);
			l_38_ += (long) is.length;
		}
		pdftrailer.xref.addObjectOffset(new IndirectObject(source
				.newID(catalog_id)), l_38_);
		l_38_ += ((Long) lengths.get(new Integer(catalog_id))).longValue();
		pdftrailer.xref.addObjectOffset(new IndirectObject(source
				.newID((Integer) pages.get(0))), l_38_);
		l_38_ += ((Long) lengths.get(pages.get(0))).longValue();
		Enumeration enumeration = firstPageSubObjects.keys();
		int i = 2147483647;
		Array array = new Array();
		while (enumeration.hasMoreElements()) {
			Integer integer = (Integer) enumeration.nextElement();
			Long var_long = (Long) lengths.get(integer);
			if (source.newID(integer).intValue() < i)
				i = source.newID(integer).intValue();
			array.put(source.newID(integer).intValue(), integer);
		}
		for (int i_39_ = i; i_39_ < array.length(); i_39_++) {
			pdftrailer.xref.addObjectOffset(new IndirectObject(i_39_), l_38_);
			l_38_ += ((Long) lengths.get(array.get(i_39_))).longValue();
		}
		pdftrailer.catalog_id = source.newID(catalog_id);
		pdftrailer.info_id = source.newID(info_id);
		pdftrailer.encrypt = encrypt;
		linearized.fileLength = filesize + headerSize
				+ tr2sizewhioutIndirectObjects;
		return pdftrailer;
	}

	HintStream createFirstHintStream(IndirectObject indirectobject,
			PDFDocument pdfdocument) {
		calcFirstLocations();
		HintStream hintstream = new HintStream(indirectobject);
		hintstream.pageOffset = new PageOffsetHintRecord(pages,
				firstPageSubObjects, lengths, unicalSubObjects);
		hintstream.pageOffset.firstObjectLocation = firstPageOffset;
		hintstream.sharedObjects = new SharedObjectsHintRecord(pages,
				sharedSubObjects, firstPageSubObjects, lengths, source);
		if (outlines.size() > 0) {
			hintstream.outlines = new GenericHintRecord(outlines, lengths,
					source);
			hintstream.outlines.firstLocation = firstOutlineOffset;
		}
		if (names.size() > 0) {
			hintstream.namedDestinations = new GenericHintRecord(names,
					lengths, source);
			hintstream.namedDestinations.firstLocation = firstNamedDestinationOffset;
		}
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		DataOutputStream dataoutputstream = pdfdocument.outputPDF.output;
		pdfdocument.outputPDF.output = new DataOutputStream(
				bytearrayoutputstream);
		pdfdocument.outputPDF.currentOS = pdfdocument.outputPDF.output;
		hintstream.write(pdfdocument.outputPDF);
		hintstream.bytes = bytearrayoutputstream.toByteArray();
		pdfdocument.outputPDF.output = dataoutputstream;
		pdfdocument.outputPDF.currentOS = pdfdocument.outputPDF.output;
		hintstream.length = hintstream.bytes.length;
		return hintstream;
	}

	void appendLinearizedData(Linearized linearized, long l,
			HintStream hintstream) {
		linearized.hintStreamObjectOffset = ((int) headerSize + ((Long) lengths
				.get(new Integer(catalog_id))).intValue());
		linearized.hintStreamObjectLength = hintstream.length;
	}

	long calcfirstPageEndOffset(HintStream hintstream, byte[] is) {
		long l = headerSize + (long) hintstream.length;
		l += (long) ((Long) lengths.get(pages.get(0))).intValue();
		l += (long) ((Long) lengths.get(new Integer(catalog_id))).intValue();
		Enumeration enumeration = firstPageSubObjects.keys();
		while (enumeration.hasMoreElements()) {
			Integer integer = (Integer) enumeration.nextElement();
			l += (long) ((Long) lengths.get(integer)).intValue();
		}
		if (encrypted)
			l += (long) is.length;
		return l;
	}

	byte[] createEncryptObj(PDFDocument pdfdocument) {
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		DataOutputStream dataoutputstream = pdfdocument.outputPDF.output;
		pdfdocument.outputPDF.output = new DataOutputStream(
				bytearrayoutputstream);
		pdfdocument.outputPDF.currentOS = pdfdocument.outputPDF.output;
		encrypt.write(pdfdocument.outputPDF);
		byte[] is = bytearrayoutputstream.toByteArray();
		pdfdocument.outputPDF.output = dataoutputstream;
		pdfdocument.outputPDF.currentOS = pdfdocument.outputPDF.output;
		return is;
	}

	void writeLinearized() throws IOException, ImageFormatException {
		PDFDocument pdfdocument = createDestination();
		HintStream hintstream = createFirstHintStream(new IndirectObject(
				counter++), pdfdocument);
		Linearized linearized = createLinearizedDictionary(new IndirectObject(
				source.newID(catalog_id).intValue() - 1));
		linearized.firstPageEndOffset = firstPageOffset;
		byte[] is = null;
		if (encrypted) {
			encrypt.obj_id = new IndirectObject(counter++);
			is = createEncryptObj(pdfdocument);
			pdfdocument.outputPDF.encrypt = encrypt;
			pdfdocument.outputPDF.useEncryption = true;
		}
		PDFTrailer pdftrailer = createFirstTrailer(linearized, is);
		appendLinearizedData(linearized, pdftrailer.xref.xrefSize, hintstream);
		pdftrailer.xref.addObjectOffset(linearized.obj_id,
				(long) pdfdocument.outputPDF.size());
		pdftrailer.xref.addObjectOffset(hintstream.obj_id,
				(headerSize
						+ ((Long) lengths.get(new Integer(catalog_id)))
								.longValue() + (encrypted ? (long) is.length
						: 0L)));
		for (int i = source.newID(catalog_id).intValue() + 1; i < hintstream.obj_id.num; i++) {
			long l = (((Long) pdftrailer.xref.offsets.get(i)).longValue() + (long) hintstream.length);
			pdftrailer.xref.offsets.put(i, new Long(l));
		}
		pdftrailer.prev = headerSize + (long) hintstream.length;
		if (encrypted)
			pdftrailer.prev += (long) is.length;
		Enumeration enumeration = lengths.keys();
		while (enumeration.hasMoreElements())
			pdftrailer.prev += ((Long) lengths.get(enumeration.nextElement()))
					.longValue();
		linearized.fileLength += (long) (hintstream.length
				+ ("" + source.newID(info_id) + " 0 R").length() + (""
				+ source.newID(catalog_id) + " 0 R").length());
		linearized.mainXRefFirstLineOffset = (headerSize
				+ (long) hintstream.length + filesize + 8L + (long) ("" + tr2itemnumber)
				.length());
		if (encrypted) {
			linearized.mainXRefFirstLineOffset += (long) is.length;
			linearized.fileLength += (long) is.length;
		}
		linearized.firstPageEndOffset = calcfirstPageEndOffset(hintstream, is);
		linearized.write(pdfdocument.outputPDF);
		long l = (long) pdfdocument.outputPDF.size();
		xrefOffset = (long) pdfdocument.outputPDF.size();
		pdftrailer.xref.offset = 0L;
		pdftrailer.write(pdfdocument.outputPDF);
		for (long l_40_ = (long) pdfdocument.outputPDF.size() - l; l_40_ < pdftrailer.xref.xrefSize; l_40_++)
			pdfdocument.outputPDF.print(" ");
		if (encrypted)
			pdfdocument.outputPDF.write(is);
		PDFUniversalObject pdfuniversalobject = (PDFUniversalObject) objs
				.get(new Integer(catalog_id));
		pdfuniversalobject.setID(source.newID(catalog_id).intValue());
		pdfuniversalobject.write(pdfdocument.outputPDF, source);
		pdfdocument.outputPDF.write(hintstream.bytes);
		pdfuniversalobject = (PDFUniversalObject) objs.get(pages.get(0));
		pdfuniversalobject.setID(source.newID(pages.get(0)).intValue());
		pdfuniversalobject.write(pdfdocument.outputPDF, source);
		writeObjectOrderedByNewID(firstPageSubObjects, pdfdocument.outputPDF,
				source);
		for (int i = 1; i < pages.length(); i++) {
			pdfuniversalobject = (PDFUniversalObject) objs.get(pages.get(i));
			pdfuniversalobject.setID(source.newID(pages.get(i)).intValue());
			pdfuniversalobject.write(pdfdocument.outputPDF, source);
			writeObjectOrderedByNewID(((Hashtable) unicalSubObjects
					.get(new Integer(i))), pdfdocument.outputPDF, source);
		}
		writeObjectOrderedByNewID(sharedSubObjects, pdfdocument.outputPDF,
				source);
		writeObjectOrderedByNewID(nonPageObjects, pdfdocument.outputPDF, source);
		pdfdocument.outputPDF.trailer.catalog.obj_id = new IndirectObject(
				source.newID(catalog_id));
		pdfdocument.outputPDF.trailer.info.obj_id = new IndirectObject(source
				.newID(info_id));
		pdfdocument.outputPDF.trailer.xref.offset = xrefOffset;
		Array array = pdfdocument.outputPDF.trailer.xref.offsets;
		Array array_41_ = new Array();
		for (int i = 1; i < linearize_id; i++)
			array_41_.put(i, array.get(i));
		source.close();
		source = null;
		pdfdocument.outputPDF.trailer.xref.offsets = array_41_;
		if (pdfdocument.outputPDF.useEncryption)
			pdfdocument.outputPDF.useEncryption = false;
		pdfdocument.printTail();
	}

	void writeObjectOrderedByNewID(Hashtable hashtable,
			PDFOutputStream pdfoutputstream, PDFInstance pdfinstance) {
		if (hashtable != null) {
			Enumeration enumeration = hashtable.keys();
			int i = 2147483647;
			Array array = new Array();
			while (enumeration.hasMoreElements()) {
				Integer integer = (Integer) enumeration.nextElement();
				PDFUniversalObject pdfuniversalobject = (PDFUniversalObject) objs
						.get(integer);
				pdfuniversalobject.setID(source.newID(integer).intValue());
				if (pdfuniversalobject.obj_id < i)
					i = pdfuniversalobject.obj_id;
				array.put(pdfuniversalobject.obj_id, pdfuniversalobject);
			}
			for (int i_42_ = i; i_42_ < array.length(); i_42_++) {
				PDFUniversalObject pdfuniversalobject = (PDFUniversalObject) array
						.get(i_42_);
				pdfuniversalobject.write(pdfoutputstream, pdfinstance);
			}
		}
	}

	String printHashtable(Hashtable hashtable) {
		Enumeration enumeration = hashtable.keys();
		String string = "";
		while (enumeration.hasMoreElements()) {
			Object object = enumeration.nextElement();
			string += object + " " + hashtable.get(object) + "\n";
		}
		return string;
	}

	long getLength(int i) {
		return getLength(new Integer(i));
	}

	long getLength(Integer integer) {
		return ((Long) lengths.get(integer)).longValue();
	}

	void linearize() throws IOException, ImageFormatException {
		readObjects();
		readPages();
		separateSubobjects();
		renumberObjects();
		calcSizeOfObjects();
		writeLinearized();
	}

	public static void main(String[] strings) throws Exception {
		SeekableFileInputStream seekablefileinputstream = new SeekableFileInputStream(
				strings[0]);
		FileOutputStream fileoutputstream = new FileOutputStream(strings[1]);
		Linearize linearize = new Linearize(seekablefileinputstream,
				fileoutputstream);
		linearize.linearize();
		seekablefileinputstream.close();
		fileoutputstream.close();
	}
}