/*
 * PDFException - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

public class PDFException extends RuntimeException {
	public PDFException() {
		/* empty */
	}

	public PDFException(Exception exception) {
		super(exception.toString());
	}

	public PDFException(String string) {
		super(string);
	}
}