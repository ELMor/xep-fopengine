/*
 * Pages - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import com.renderx.util.Hashtable;

public class Pages extends PDFObject {
	int pagesCount;

	Hashtable pages;

	Pages[] kids = null;

	IndirectObject parent_id = null;

	int start = -1;

	int end = -1;

	public static final int NODESIZE = 10;

	String getType() {
		return "Pages";
	}

	Pages(IndirectObject indirectobject) {
		super(indirectobject);
		pagesCount = -1;
		this.pages = new Hashtable();
	}

	Pages(IndirectObject indirectobject, IndirectObject indirectobject_0_,
			Hashtable hashtable, int i, int i_1_) {
		super(indirectobject);
		pagesCount = i_1_ - i;
		this.pages = hashtable;
		parent_id = indirectobject_0_;
		start = i;
		end = i_1_;
	}

	int getPagesCount() {
		return pagesCount;
	}

	void addPage(Page page) {
		pagesCount++;
		this.pages.put(new Integer(pagesCount), page);
	}

	void writeBody(PDFOutputStream pdfoutputstream) {
		pdfoutputstream.println("/Count " + (pagesCount + 1));
		if (parent_id != null)
			pdfoutputstream.println("/Parent " + parent_id.toStringR());
		if (start == -1)
			kids = enumerateKids(pdfoutputstream, 0, this.pages.size() - 1);
		else
			kids = enumerateKids(pdfoutputstream, start, end);
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		if (kids != null) {
			for (int i = 0; i < kids.length; i++)
				kids[i].write(pdfoutputstream);
			kids = null;
		}
		if (start == -1) {
			for (int i = 0; i < this.pages.size(); i++)
				((Page) this.pages.get(new Integer(i))).write(pdfoutputstream);
			this.pages = null;
		}
	}

	private Pages[] enumerateKids(PDFOutputStream pdfoutputstream, int i,
			int i_2_) {
		if (i_2_ - i < 10) {
			pdfoutputstream.print("/Kids [");
			for (int i_3_ = i; i_3_ <= i_2_; i_3_++) {
				Page page = (Page) this.pages.get(new Integer(i_3_));
				pdfoutputstream.print(page.getID().toStringR() + " ");
				page.parent_id = this.getID();
				this.pages.put(new Integer(i_3_), page);
			}
			pdfoutputstream.println("]");
			return null;
		}
		int i_4_ = 10;
		int i_5_;
		for (i_5_ = (i_2_ - i + 10) / 10; i_5_ > 10; i_5_ = (i_5_ + 10 - 1) / 10)
			i_4_ *= 10;
		pdfoutputstream.print("/Kids [");
		Pages[] pageses = new Pages[i_5_];
		for (int i_6_ = 0; i_6_ < i_5_; i_6_++) {
			int i_7_ = i + i_4_ - 1;
			if (i_7_ > i_2_)
				i_7_ = i_2_;
			pageses[i_6_] = new Pages(new IndirectObject(
					pdfoutputstream.lastObjectNumber++), this.getID(),
					this.pages, i, i_7_);
			i += i_4_;
			pdfoutputstream.print(" " + pageses[i_6_].getID().toStringR());
		}
		pdfoutputstream.println("]");
		return pageses;
	}
}