/*
 * Lexer - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib.parser;

import java.io.IOException;

import com.renderx.pdflib.PDFOutputStream;
import com.renderx.util.SeekableFileInputStream;
import com.renderx.util.SeekableInput;

public class Lexer {
	SeekableInput raf;

	char lastchar;

	boolean lastcharUsed;

	Lexer(SeekableInput seekableinput) throws IOException {
		raf = seekableinput;
		lastcharUsed = true;
	}

	public void seek(long l) throws IOException {
		raf.seek(l);
	}

	Token nextToken() throws IOException {
		if (lastcharUsed)
			read();
		skipSpaces();
		while (lastchar == '%') {
			skipComment();
			if (PDFOutputStream.dump != null)
				PDFOutputStream.dumpprintln("<skipComment/>");
		}
		if (lastchar == '/') {
			lastcharUsed = false;
			return readName();
		}
		if (lastchar == '(') {
			lastcharUsed = true;
			return readString();
		}
		if (lastchar == '[') {
			lastcharUsed = true;
			return new Token((byte) 5, null);
		}
		if (lastchar == ']') {
			lastcharUsed = true;
			return new Token((byte) 6, null);
		}
		if (lastchar == '>') {
			if (read() == '>') {
				lastcharUsed = true;
				return new Token((byte) 8, null);
			}
			throw new IOException("Broken pdf file. Dictionary is not closed.");
		}
		if (lastchar == '<') {
			read();
			if (lastchar == '<') {
				lastcharUsed = true;
				return new Token((byte) 7, null);
			}
			lastcharUsed = true;
			return readHEXString();
		}
		if (ParseUtils.isNumber(lastchar)) {
			lastcharUsed = false;
			return readNumeric();
		}
		lastcharUsed = false;
		String string = readTillTerminal();
		if (string.equals("R"))
			return new Token((byte) 4, null);
		if (string.startsWith("stream"))
			return new Token((byte) 11, null);
		if (string.startsWith("startxref"))
			return new Token((byte) 12, null);
		if (string.startsWith("endobj"))
			return new Token((byte) 10, null);
		if (string.startsWith("true"))
			return new Token((byte) 13, string);
		if (string.startsWith("false"))
			return new Token((byte) 13, string);
		if (string.startsWith("null"))
			return new Token((byte) 13, string);
		if (string.startsWith("obj"))
			return new Token((byte) 9, string);
		throw new IOException("Broken pdf file. Wrong token '" + string + "'");
	}

	private Token readName() throws IOException {
		String string = "/";
		read();
		for (/**/; (!ParseUtils.isSpace(lastchar) && lastchar != '/'
				&& lastchar != '[' && lastchar != ']' && lastchar != '('
				&& lastchar != '<' && lastchar != '>'); lastchar = read())
			string += lastchar;
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("<readName value=\"" + string + "\"/>");
		return new Token((byte) 1, string);
	}

	private String decodeString(String string) {
		if (string == null)
			return null;
		if (string.length() == 0)
			return "";
		StringBuffer stringbuffer = new StringBuffer();
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			if (c != 92)
				stringbuffer.append(c);
			else if (++i == string.length())
				stringbuffer.append(c);
			else {
				switch (c = string.charAt(i)) {
				case 'n':
					stringbuffer.append('\n');
					break;
				case 'r':
					stringbuffer.append('\r');
					break;
				case 't':
					stringbuffer.append('\t');
					break;
				case 'b':
					stringbuffer.append('\010');
					break;
				case 'f':
					stringbuffer.append('\014');
					break;
				case '(':
				case ')':
				case '\\':
					stringbuffer.append(c);
					break;
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7': {
					int i_0_ = c - 48;
					for (int i_1_ = 0; i_1_ < 2 && i + 1 < string.length(); i++) {
						int i_2_ = string.charAt(i + 1);
						if (i_2_ < 48 || 55 < i_2_)
							break;
						i_0_ *= 8;
						i_0_ += i_2_ - 48;
						i_1_++;
					}
					stringbuffer.append((char) i_0_);
					break;
				}
				}
			}
		}
		return stringbuffer.toString();
	}

	private Token readString() throws IOException {
		String string = "";
		read();
		char c = 'z';
		while (lastchar != ')' || lastchar == ')' && c == '\\') {
			string += lastchar;
			c = lastchar;
			read();
		}
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("<readString value=\"" + string
					+ "\"/>");
		return new Token((byte) 2, decodeString(string));
	}

	private int hexval(int i) {
		switch (i) {
		case 48:
		case 49:
		case 50:
		case 51:
		case 52:
		case 53:
		case 54:
		case 55:
		case 56:
		case 57:
			return i - 48;
		case 65:
		case 66:
		case 67:
		case 68:
		case 69:
		case 70:
			return i - 97 + 10;
		case 97:
		case 98:
		case 99:
		case 100:
		case 101:
		case 102:
			return i - 97 + 10;
		default:
			return -1;
		}
	}

	public Token readHEXString() throws IOException {
		String string = "";
		read();
		while (lastchar != '>') {
			string += lastchar;
			read();
		}
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("<readHEXString value=\"" + string
					+ "\"/>");
		String string_3_ = "";
		if (string.length() % 2 != 0)
			string += "0";
		int i = string.length();
		int i_4_ = -1;
		for (int i_5_ = 0; i_5_ < i; i_5_++) {
			int i_6_;
			if ((i_6_ = hexval(string.charAt(i_5_))) != -1) {
				if (i_4_ == -1)
					i_4_ = i_6_;
				else {
					string_3_ += (char) ((i_4_ << 4) + i_6_);
					i_4_ = -1;
				}
			}
		}
		return new Token((byte) 2, string_3_);
	}

	public void skipComment() throws IOException {
		read();
		while (lastchar != '\n' && lastchar != '\r')
			read();
		skipSpaces();
	}

	public void skipSpaces() throws IOException {
		if (ParseUtils.isSpace(lastchar)) {
			while (ParseUtils.isSpace(read())) {
				/* empty */
			}
		}
	}

	public void skipEndl() throws IOException {
		if (lastchar == '\r') {
			read();
			if (lastchar != '\n') {
				long l = raf.getFilePointer();
				raf.seek(l - 1L);
				lastchar = '\r';
			}
		}
	}

	public Token readNumeric() throws IOException {
		String string = "" + lastchar;
		read();
		boolean bool = true;
		while (!ParseUtils.isTerminal(lastchar)) {
			if (!ParseUtils.isDigit(lastchar))
				bool = false;
			string += lastchar;
			read();
		}
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("<numeric value=\"" + string + "\"/>");
		if (bool)
			return new Token((byte) 3, string);
		return new Token((byte) 13, string);
	}

	public String readTillTerminal() throws IOException {
		String string = "" + lastchar;
		while (!ParseUtils.isTerminal(read()))
			string += lastchar;
		skipEndl();
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("<tillTerminal value=\"" + string
					+ "\"/>");
		return string;
	}

	char read() throws IOException {
		lastchar = (char) raf.readUnsignedByte();
		return lastchar;
	}

	public static void main(String[] strings) throws Exception {
		Lexer lexer = new Lexer(new SeekableFileInputStream(strings[0]));
		Token token;
		while ((token = lexer.nextToken()).type < 10)
			System.out.println(token);
		System.out.println(token);
	}
}