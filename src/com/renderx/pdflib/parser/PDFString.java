/*
 * PDFString - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib.parser;

import java.io.IOException;

import com.renderx.pdflib.PDFOutputStream;
import com.renderx.util.List;

public class PDFString extends PDFElement {
	String text;

	int lenstr;

	public PDFString(String string) {
		text = string;
		lenstr = text.length();
	}

	String getString() {
		return text;
	}

	public void quote(PDFOutputStream pdfoutputstream) {
		for (int i = 0; i < lenstr; i++) {
			int i_0_ = (byte) text.charAt(i);
			if (0 <= i_0_ && i_0_ < 32) {
				pdfoutputstream.print('\\');
				pdfoutputstream.print(48 + (i_0_ >> 6 & 0x7));
				pdfoutputstream.print(48 + (i_0_ >> 3 & 0x7));
				pdfoutputstream.print(48 + (i_0_ & 0x7));
			} else if (i_0_ == 40 || i_0_ == 41 || i_0_ == 92) {
				pdfoutputstream.print('\\');
				pdfoutputstream.print(i_0_);
			} else
				pdfoutputstream.print(i_0_);
		}
	}

	public void quote(PDFOutputStream pdfoutputstream, byte i) {
		if (0 <= i && i < 32) {
			pdfoutputstream.print('\\');
			pdfoutputstream.print(48 + (i >> 6 & 0x7));
			pdfoutputstream.print(48 + (i >> 3 & 0x7));
			pdfoutputstream.print(48 + (i & 0x7));
		} else if (i == 40 || i == 41 || i == 92) {
			pdfoutputstream.print('\\');
			pdfoutputstream.print(i);
		} else
			pdfoutputstream.print(i);
	}

	public void quote(PDFOutputStream pdfoutputstream, byte[] is) {
		if (is != null) {
			for (int i = 0; i < is.length; i++)
				quote(pdfoutputstream, is[i]);
		}
	}

	public void unicode_quote(PDFOutputStream pdfoutputstream) {
		int i = text.length();
		byte[] is = new byte[i * 2 + 2];
		is[0] = (byte) -2;
		is[1] = (byte) -1;
		for (int i_1_ = 0; i_1_ < i; i_1_++) {
			int i_2_ = text.charAt(i_1_);
			is[i_1_ * 2 + 2] = (byte) (i_2_ >> 8 & 0xff);
			is[i_1_ * 2 + 3] = (byte) (i_2_ & 0xff);
		}
		quote(pdfoutputstream,
				(pdfoutputstream.getUseEncryption() ? pdfoutputstream
						.encryptArray(is) : is));
	}

	public void pdfencoding_quote(PDFOutputStream pdfoutputstream) {
		byte[] is = new byte[lenstr];
		for (int i = 0; i < lenstr; i++) {
			char c = text.charAt(i);
			switch (c) {
			case '\u02c6':
				is[i] = (byte) 26;
				break;
			case '\u02d9':
				is[i] = (byte) 27;
				break;
			case '\u02dd':
				is[i] = (byte) 28;
				break;
			case '\u02db':
				is[i] = (byte) 29;
				break;
			case '\u02da':
				is[i] = (byte) 30;
				break;
			case '\u02dc':
				is[i] = (byte) 31;
				break;
			case '\u2022':
				is[i] = (byte) -128;
				break;
			case '\u2020':
				is[i] = (byte) -127;
				break;
			case '\u2021':
				is[i] = (byte) -126;
				break;
			case '\u2026':
				is[i] = (byte) -125;
				break;
			case '\u2014':
				is[i] = (byte) -124;
				break;
			case '\u2013':
				is[i] = (byte) -123;
				break;
			case '\u0192':
				is[i] = (byte) -122;
				break;
			case '\u2044':
				is[i] = (byte) -121;
				break;
			case '\u2039':
				is[i] = (byte) -120;
				break;
			case '\u203a':
				is[i] = (byte) -119;
				break;
			case '\u2212':
				is[i] = (byte) -118;
				break;
			case '\u2030':
				is[i] = (byte) -117;
				break;
			case '\u201e':
				is[i] = (byte) -116;
				break;
			case '\u201c':
				is[i] = (byte) -115;
				break;
			case '\u201d':
				is[i] = (byte) -114;
				break;
			case '\u2018':
				is[i] = (byte) -113;
				break;
			case '\u2019':
				is[i] = (byte) -112;
				break;
			case '\u201a':
				is[i] = (byte) -111;
				break;
			case '\u2122':
				is[i] = (byte) -110;
				break;
			case '\ufb01':
				is[i] = (byte) -109;
				break;
			case '\ufb02':
				is[i] = (byte) -108;
				break;
			case '\u0141':
				is[i] = (byte) -107;
				break;
			case '\u0152':
				is[i] = (byte) -106;
				break;
			case '\u0160':
				is[i] = (byte) -105;
				break;
			case '\u0178':
				is[i] = (byte) -104;
				break;
			case '\u017d':
				is[i] = (byte) -103;
				break;
			case '\u0131':
				is[i] = (byte) -102;
				break;
			case '\u0142':
				is[i] = (byte) -101;
				break;
			case '\u0153':
				is[i] = (byte) -100;
				break;
			case '\u0161':
				is[i] = (byte) -99;
				break;
			case '\u017e':
				is[i] = (byte) -98;
				break;
			case '\u20ac':
				is[i] = (byte) -96;
				break;
			case '\u0100':
			case '\u0102':
			case '\u0104':
				is[i] = (byte) 65;
				break;
			case '\u0101':
			case '\u0103':
			case '\u0105':
				is[i] = (byte) 97;
				break;
			case '\u0106':
			case '\u0108':
			case '\u010a':
			case '\u010c':
				is[i] = (byte) 67;
				break;
			case '\u0107':
			case '\u0109':
			case '\u010b':
			case '\u010d':
				is[i] = (byte) 99;
				break;
			case '\u010e':
				is[i] = (byte) 68;
				break;
			case '\u0110':
				is[i] = (byte) -48;
				break;
			case '\u010f':
			case '\u0111':
				is[i] = (byte) 100;
				break;
			case '\u0112':
			case '\u0114':
			case '\u0116':
			case '\u0118':
			case '\u011a':
				is[i] = (byte) 69;
				break;
			case '\u0113':
			case '\u0115':
			case '\u0117':
			case '\u0119':
			case '\u011b':
				is[i] = (byte) 101;
				break;
			case '\u011c':
			case '\u011e':
			case '\u0120':
			case '\u0122':
				is[i] = (byte) 71;
				break;
			case '\u011d':
			case '\u011f':
			case '\u0121':
			case '\u0123':
				is[i] = (byte) 103;
				break;
			case '\u0124':
			case '\u0126':
				is[i] = (byte) 72;
				break;
			case '\u0125':
			case '\u0127':
				is[i] = (byte) 104;
				break;
			case '\u0128':
			case '\u012a':
			case '\u012c':
			case '\u012e':
			case '\u0130':
				is[i] = (byte) 73;
				break;
			case '\u0129':
			case '\u012b':
			case '\u012d':
			case '\u012f':
				is[i] = (byte) 105;
				break;
			case '\u0132':
			case '\u0134':
				is[i] = (byte) 74;
				break;
			case '\u0133':
			case '\u0135':
				is[i] = (byte) 106;
				break;
			case '\u0136':
				is[i] = (byte) 75;
				break;
			case '\u0137':
			case '\u0138':
				is[i] = (byte) 107;
				break;
			case '\u0139':
			case '\u013b':
			case '\u013d':
			case '\u013f':
				is[i] = (byte) 76;
				break;
			case '\u013a':
			case '\u013c':
			case '\u013e':
			case '\u0140':
				is[i] = (byte) 108;
				break;
			case '\u0143':
			case '\u0145':
			case '\u0147':
			case '\u014a':
				is[i] = (byte) 78;
				break;
			case '\u0144':
			case '\u0146':
			case '\u0148':
			case '\u0149':
			case '\u014b':
				is[i] = (byte) 110;
				break;
			case '\u014c':
			case '\u014e':
			case '\u0150':
				is[i] = (byte) 79;
				break;
			case '\u014d':
			case '\u014f':
			case '\u0151':
				is[i] = (byte) 111;
				break;
			case '\u0154':
			case '\u0156':
			case '\u0158':
				is[i] = (byte) 82;
				break;
			case '\u0155':
			case '\u0157':
			case '\u0159':
				is[i] = (byte) 114;
				break;
			case '\u015a':
			case '\u015c':
			case '\u015e':
				is[i] = (byte) 83;
				break;
			case '\u015b':
			case '\u015d':
			case '\u015f':
			case '\u017f':
				is[i] = (byte) 115;
				break;
			case '\u0162':
			case '\u0164':
			case '\u0166':
				is[i] = (byte) 84;
				break;
			case '\u0163':
			case '\u0165':
			case '\u0167':
				is[i] = (byte) 116;
				break;
			case '\u0168':
			case '\u016a':
			case '\u016c':
			case '\u016e':
			case '\u0170':
			case '\u0172':
				is[i] = (byte) 85;
				break;
			case '\u0169':
			case '\u016b':
			case '\u016d':
			case '\u016f':
			case '\u0171':
			case '\u0173':
				is[i] = (byte) 117;
				break;
			case '\u0174':
				is[i] = (byte) 87;
				break;
			case '\u0175':
				is[i] = (byte) 119;
				break;
			case '\u0176':
				is[i] = (byte) 89;
				break;
			case '\u0177':
				is[i] = (byte) 121;
				break;
			case '\u0179':
			case '\u017b':
				is[i] = (byte) 90;
				break;
			case '\u017a':
			case '\u017c':
				is[i] = (byte) 122;
				break;
			case '\u00a0':
				is[i] = (byte) 32;
				break;
			case '\t':
			case '\n':
			case '\r':
				is[i] = (byte) c;
				break;
			default:
				if (' ' <= c && c < '\u0080' || '\u00a0' < c && c <= '\u00ff')
					is[i] = (byte) c;
				else
					is[i] = (byte) -128;
			}
		}
		quote(pdfoutputstream,
				(pdfoutputstream.getUseEncryption() ? pdfoutputstream
						.encryptArray(is) : is));
	}

	public void write(PDFOutputStream pdfoutputstream, PDFInstance pdfinstance)
			throws IOException {
		lenstr = text.length();
		byte[] is = new byte[lenstr];
		for (int i = 0; i < lenstr; i++)
			is[i] = (byte) text.charAt(i);
		pdfoutputstream.write(40);
		quote(pdfoutputstream,
				(pdfoutputstream.getUseEncryption() ? pdfoutputstream
						.encryptArray(is) : is));
		pdfoutputstream.write(41);
	}

	List getIndirectObjects() {
		return new List();
	}

	public String toString() {
		return text;
	}
}