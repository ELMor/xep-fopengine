/*
 * PDFInstance - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib.parser;

import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;

import com.renderx.fonts.FontCatalog;
import com.renderx.fonts.FontConfigurationException;
import com.renderx.graphics.Image;
import com.renderx.graphics.ImageFactory;
import com.renderx.graphics.ImageFormatException;
import com.renderx.pdflib.PDFOutputStream;
import com.renderx.util.Array;
import com.renderx.util.DefaultErrorHandler;
import com.renderx.util.ErrorHandler;
import com.renderx.util.Hashtable;
import com.renderx.util.List;
import com.renderx.util.SeekableInput;
import com.renderx.util.URLCache;
import com.renderx.util.URLSpec;

import junit.framework.Assert;
import junit.framework.TestCase;

public class PDFInstance extends Image {
	Lexer input = null;

	public XRefTable xRefTable = null;

	int root_obj = 0;

	int pages_obj = 0;

	public int countPages = 0;

	public boolean renumber = true;

	public int angle = 0;

	private boolean isLZW = false;

	Hashtable pagestoobj = new Hashtable();

	Hashtable pagesparent = new Hashtable();

	public Hashtable converObjects = new Hashtable();

	public List pendingObjects = new List();

	Hashtable lastPageFonts = new Hashtable();

	Hashtable lastPageImages = new Hashtable();

	public static final class Test extends TestCase {
		ErrorHandler errorHandler = new DefaultErrorHandler();

		URLCache urlcache;

		FontCatalog fontCatalog;

		ImageFactory imageFactory;

		class NameHashtable {
			Hashtable ht;

			NameHashtable(String string) throws Exception {
				DataInputStream datainputstream = (new DataInputStream(
						ClassLoader.getSystemResourceAsStream(string)));
				ht = new Hashtable();
				String string_0_;
				while ((string_0_ = datainputstream.readLine()) != null)
					ht.put(string_0_.substring(0, string_0_.indexOf(" ")),
							string_0_.substring(string_0_.indexOf(" ") + 1));
				datainputstream.close();
			}

			boolean equals(Hashtable hashtable) {
				if (ht.size() != hashtable.size())
					return false;
				Enumeration enumeration = ht.keys();
				while (enumeration.hasMoreElements()) {
					String string = (String) enumeration.nextElement();
					if (!hashtable.containsKey(string))
						return false;
					String string_1_ = (String) ht.get(string);
					String string_2_ = (String) hashtable.get(string);
					if (!string_1_.equals(string_2_))
						return false;
				}
				return true;
			}
		}

		public void setUp() {
			try {
				urlcache = new URLCache(errorHandler);
				fontCatalog = new FontCatalog(urlcache, errorHandler);
				imageFactory = new ImageFactory(fontCatalog, urlcache,
						errorHandler);
			} catch (IOException ioexception) {
				Assert.fail("Cannot create font catalog or image factory: "
						+ ioexception);
			} catch (FontConfigurationException fontconfigurationexception) {
				Assert.fail("Cannot create font catalog: "
						+ fontconfigurationexception);
			}
		}

		public void testMediaBoxSimple() {
			try {
				resourceToFile(
						"com/renderx/pdflib/parser/TEST_DATA/mediaBoxSimple.pdf",
						"1.tst");
				PDFInstance pdfinstance = ((PDFInstance) imageFactory
						.makeImage(new URLSpec("1.tst"), "application/pdf"));
				pdfinstance.open();
				Rectangle rectangle = pdfinstance.getMediaBox();
				NameHashtable namehashtable = (new NameHashtable(
						"com/renderx/pdflib/parser/TEST_DATA/mediaBoxSimple.test"));
				Assert.assertTrue(((String) namehashtable.ht.get("MediaBox"))
						.equals(rectangle.toString()));
				Object object = null;
				Object object_3_ = null;
			} catch (Exception exception) {
				exception.printStackTrace(System.err);
				Assert.assertTrue(false);
			}
		}

		public void testMediaBoxComplicaded() {
			try {
				resourceToFile(
						"com/renderx/pdflib/parser/TEST_DATA/mediaBoxComplicaded.pdf",
						"1.tst");
				PDFInstance pdfinstance = ((PDFInstance) imageFactory
						.makeImage(new URLSpec("1.tst"), "application/pdf"));
				pdfinstance.open();
				Rectangle rectangle = pdfinstance.getMediaBox();
				NameHashtable namehashtable = (new NameHashtable(
						"com/renderx/pdflib/parser/TEST_DATA/mediaBoxComplicaded.test"));
				Assert.assertTrue(((String) namehashtable.ht.get("MediaBox"))
						.equals(rectangle.toString()));
				Object object = null;
				Object object_4_ = null;
			} catch (Exception exception) {
				exception.printStackTrace(System.err);
				Assert.assertTrue(false);
			}
		}

		public void testMediaBoxMoreComplicaded() {
			try {
				resourceToFile(
						"com/renderx/pdflib/parser/TEST_DATA/mediaBoxMoreComplicaded.pdf",
						"1.tst");
				PDFInstance pdfinstance = ((PDFInstance) imageFactory
						.makeImage(new URLSpec("1.tst"), "application/pdf"));
				pdfinstance.open();
				Rectangle rectangle = pdfinstance.getMediaBox();
				NameHashtable namehashtable = (new NameHashtable(
						"com/renderx/pdflib/parser/TEST_DATA/mediaBoxMoreComplicaded.test"));
				Assert.assertTrue(((String) namehashtable.ht.get("MediaBox"))
						.equals(rectangle.toString()));
				Object object = null;
				Object object_5_ = null;
			} catch (Exception exception) {
				exception.printStackTrace(System.err);
				Assert.assertTrue(false);
			}
		}

		private void resourceToFile(String string, String string_6_)
				throws IOException {
			InputStream inputstream = ClassLoader
					.getSystemResourceAsStream(string);
			FileOutputStream fileoutputstream = new FileOutputStream(string_6_);
			byte[] is = new byte[1024];
			int i;
			while ((i = inputstream.read(is, 0, 1024)) >= 0)
				fileoutputstream.write(is, 0, i);
			fileoutputstream.close();
		}
	}

	public PDFInstance() throws IOException {
		mimetype = "application/pdf";
	}

	public void parse() throws IOException, ImageFormatException {
		try {
			parse(this.openSeekableImageStream());
		} finally {
			close();
		}
	}

	public void parse(SeekableInput seekableinput) throws IOException,
			ImageFormatException {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("<parsepdf>");
		open(seekableinput);
		init();
		Rectangle rectangle = getMediaBox();
		if (angle == 0 || angle == 180) {
			width = (double) rectangle.getWidth();
			height = (double) rectangle.getHeight();
		} else {
			width = (double) rectangle.getHeight();
			height = (double) rectangle.getWidth();
		}
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("</parsepdf>");
	}

	private void init() throws IOException, ImageFormatException {
		checkHeader();
		xRefTable = new XRefTable(input.raf);
		readRoot(xRefTable.getRoot());
		readPages(pages_obj, -1);
	}

	private void checkHeader() throws IOException, ImageFormatException {
		input.seek(0L);
		byte[] is = new byte[6];
		input.raf.readFully(is);
		if (is[0] != 37 || is[1] != 80 || is[2] != 68 || is[3] != 70
				|| is[4] != 45 || is[5] != 49)
			throw new ImageFormatException("Wrong PDF file header");
	}

	public void readRoot(int i) throws IOException, ImageFormatException {
		PDFDictionary pdfdictionary = getDictionary(getObject(i));
		if (pdfdictionary.dict.containsKey("/Pages") && pages_obj == 0)
			pages_obj = ParseUtils
					.getIntFromIndirectObject((PDFElement) pdfdictionary.dict
							.get("/Pages"));
	}

	public void readPages(int i, int i_7_) throws IOException,
			ImageFormatException {
		if (i_7_ != -1)
			pagesparent.put(new Integer(i), new Integer(i_7_));
		PDFDictionary pdfdictionary = getDictionary(getObject(i));
		if (pdfdictionary.dict.containsKey("/Type")
				&& ((PDFName) pdfdictionary.dict.get("/Type")).value
						.equals("/Page")) {
			pagestoobj.put(new Integer(countPages), new Integer(i));
			countPages++;
		} else if (pdfdictionary.dict.containsKey("/Kids")) {
			PDFArray pdfarray = (PDFArray) pdfdictionary.dict.get("/Kids");
			for (int i_8_ = 0; i_8_ < pdfarray.array.length(); i_8_++)
				readPages(ParseUtils
						.getIntFromIndirectObject((PDFElement) pdfarray.array
								.get(i_8_)), i);
		}
	}

	public PDFDictionary getResources() throws IOException,
			ImageFormatException {
		return getResources(0);
	}

	public PDFDictionary getResources(int i) throws IOException,
			ImageFormatException {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("<getResources page=\"" + i + "\"/>");
		return getResourcesByObject(ParseUtils.getIntFromHash(pagestoobj, i), i);
	}

	public PDFDictionary getResourcesByObject(int i, int i_9_)
			throws IOException, ImageFormatException {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("<getResources page_obj=\"" + i
					+ "\"/>");
		input.raf.seek(xRefTable.getOffset(i));
		PDFElement pdfelement = ElementParser.parse(input);
		if (!(pdfelement instanceof PDFDictionary))
			throw new ImageFormatException("Broken page " + i_9_ + "(object "
					+ ParseUtils.getIntFromHash(pagestoobj, i_9_) + ")");
		PDFDictionary pdfdictionary = (PDFDictionary) pdfelement;
		Object object = null;
		PDFDictionary pdfdictionary_10_;
		if (pdfdictionary.dict.containsKey("/Resources"))
			pdfdictionary_10_ = ((PDFDictionary) readTillUnIndirectObject((PDFElement) pdfdictionary.dict
					.get("/Resources")));
		else {
			if (pagesparent.get(new Integer(i)) != null)
				return (getResourcesByObject(ParseUtils.getIntFromHash(
						pagesparent, i), i_9_));
			throw new ImageFormatException("Broken pdf file "
					+ this.toDisplayString()
					+ ". None of /ArtBox, /CropBox, or /MediaBox for page "
					+ i_9_ + " (object "
					+ ParseUtils.getIntFromHash(pagestoobj, i_9_)
					+ ") is found.");
		}
		return pdfdictionary_10_;
	}

	public Rectangle getMediaBox() throws IOException, ImageFormatException {
		return getMediaBox(0);
	}

	public Rectangle getMediaBox(int i) throws IOException,
			ImageFormatException {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("<getMediaBox page=\"" + i + "\"/>");
		return getMediaBoxByObject(ParseUtils.getIntFromHash(pagestoobj, i), i);
	}

	public Rectangle getMediaBoxByObject(int i, int i_11_) throws IOException,
			ImageFormatException {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("<getMediaBoxByObject page_obj=\"" + i
					+ "\"/>");
		input.raf.seek(xRefTable.getOffset(i));
		PDFElement pdfelement = ElementParser.parse(input);
		if (!(pdfelement instanceof PDFDictionary))
			throw new ImageFormatException("Broken page " + i_11_ + "(object "
					+ ParseUtils.getIntFromHash(pagestoobj, i_11_) + ")");
		PDFDictionary pdfdictionary = (PDFDictionary) pdfelement;
		if (pdfdictionary.dict.containsKey("/Rotate")) {
			PDFElement pdfelement_12_ = (PDFElement) pdfdictionary.dict
					.get("/Rotate");
			if (!(pdfelement_12_ instanceof PDFNumeric))
				throw new ImageFormatException("Broken pdf file "
						+ this.toDisplayString()
						+ ". Wrong rotate paremeter for page " + i_11_
						+ " (object "
						+ ParseUtils.getIntFromHash(pagestoobj, i_11_) + ").");
			angle = new Integer(((PDFNumeric) pdfelement_12_).value).intValue();
			angle = angle % 360;
			angle = angle < 0 ? 360 + angle : angle;
			if (angle != 0 && angle != 90 && angle != 180 && angle != 270)
				throw new ImageFormatException("Broken pdf file "
						+ this.toDisplayString()
						+ ". Wrong rotate paremeter for page " + i_11_
						+ " (object "
						+ ParseUtils.getIntFromHash(pagestoobj, i_11_) + ").");
		}
		PDFArray pdfarray;
		if (pdfdictionary.dict.containsKey("/ArtBox"))
			pdfarray = ((PDFArray) readTillUnIndirectObject((PDFElement) pdfdictionary.dict
					.get("/ArtBox")));
		else if (pdfdictionary.dict.containsKey("/CropBox"))
			pdfarray = (PDFArray) readTillUnIndirectObject((PDFElement) pdfdictionary.dict
					.get("/CropBox"));
		else if (pdfdictionary.dict.containsKey("/MediaBox"))
			pdfarray = (PDFArray) readTillUnIndirectObject((PDFElement) pdfdictionary.dict
					.get("/MediaBox"));
		else {
			if (pagesparent.get(new Integer(i)) != null)
				return getMediaBoxByObject(ParseUtils.getIntFromHash(
						pagesparent, i), i_11_);
			throw new ImageFormatException("Broken pdf file "
					+ this.toDisplayString()
					+ ". None of /ArtBox, /CropBox, or /MediaBox for page "
					+ i_11_ + " (object "
					+ ParseUtils.getIntFromHash(pagestoobj, i_11_)
					+ ") is found.");
		}
		return new Rectangle(new Float(readTillUnIndirectObject(
				(PDFElement) pdfarray.array.get(0)).toString()).floatValue(),
				new Float(readTillUnIndirectObject(
						(PDFElement) pdfarray.array.get(1)).toString())
						.floatValue(), new Float(readTillUnIndirectObject(
						(PDFElement) pdfarray.array.get(2)).toString())
						.floatValue(), new Float(readTillUnIndirectObject(
						(PDFElement) pdfarray.array.get(3)).toString())
						.floatValue());
	}

	public void getContent(OutputStream outputstream) throws IOException,
			ImageFormatException {
		getContent(0, outputstream);
	}

	public void parsePageHeader() throws IOException, ImageFormatException {
		parsePageHeader(0);
	}

	public PDFUniversalObject getPage(int i) throws IOException,
			ImageFormatException {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("<getPage number=\"" + i + "\">");
		PDFUniversalObject pdfuniversalobject = getObject(ParseUtils
				.getIntFromHash(pagestoobj, i));
		PDFDictionary pdfdictionary = getDictionary(pdfuniversalobject);
		String string = null;
		if (pdfdictionary.dict.containsKey("/Contents")) {
			PDFElement pdfelement = (PDFElement) pdfdictionary.dict
					.get("/Contents");
			if (pdfelement instanceof PDFArray)
				string = checkFilters(pdfelement);
			else if (pdfelement instanceof PDFIndirectObject) {
				PDFElement pdfelement_13_ = readTillUnIndirectObject(pdfelement);
				if (pdfelement_13_ instanceof PDFDictionary) {
					PDFDictionary pdfdictionary_14_ = getDictionary(pdfelement_13_);
					if (pdfdictionary_14_.dict.containsKey("/Filter"))
						string = pdfdictionary_14_.dict.get("/Filter")
								.toString();
				} else if (pdfelement_13_ instanceof PDFArray)
					string = checkFilters(pdfelement_13_);
			}
		}
		do {
			if (string != null && !string.equals("/FlateDecode")) {
				if (string.equals("/LZWDecode"))
					pdfdictionary.dict.put("/Filter", string);
				else if (string.equals("/ASCIIHexDecode")
						|| string.equals("/ASCII85Decode")
						|| string.equals("/RunLengthDecode"))
					break;
			}
		} while (false);
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("</getPage>");
		return pdfuniversalobject;
	}

	public String checkFilters(PDFElement pdfelement) throws IOException,
			ImageFormatException {
		boolean bool = false;
		for (int i = 0; i < ((PDFArray) pdfelement).array.length(); i++) {
			PDFElement pdfelement_15_ = readTillUnIndirectObject((PDFElement) ((PDFArray) pdfelement).array
					.get(i));
			PDFDictionary pdfdictionary = getDictionary(pdfelement_15_);
			boolean bool_16_ = false;
			if (pdfdictionary.dict.containsKey("/Filter")) {
				PDFElement pdfelement_17_ = readTillUnIndirectObject((PDFElement) pdfdictionary.dict
						.get("/Filter"));
				if (pdfelement_17_ instanceof PDFName) {
					PDFName pdfname = (PDFName) pdfelement_17_;
					bool_16_ = pdfname.value.equals("/LZWDecode");
				} else if (pdfelement_17_ instanceof PDFArray) {
					PDFArray pdfarray = (PDFArray) pdfelement_17_;
					for (int i_18_ = 0; i_18_ < pdfarray.array.length()
							&& !bool_16_; i_18_++) {
						Object object = pdfarray.array.get(i_18_);
						if (!(object instanceof PDFName))
							throw new ImageFormatException(
									"Filter array contains objects that are not names");
						bool_16_ = ((PDFName) object).value
								.equals("/LZWDecode");
					}
				}
			}
			if (i == 0)
				bool = bool_16_;
			else if (bool != bool_16_)
				throw new ImageFormatException(
						"LZW and non-LZW compression mixed in the content array of a page");
		}
		if (bool)
			return "/LZWDecode";
		return null;
	}

	public PDFUniversalObject getObject(int i) throws IOException,
			ImageFormatException {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("<readObject number=\"" + i + "\">");
		long l = xRefTable.getOffset(i);
		input.raf.seek(l);
		input.lastcharUsed = true;
		PDFUniversalObject pdfuniversalobject = new PDFUniversalObject(i);
		input.nextToken();
		input.nextToken();
		input.nextToken();
		PDFElement pdfelement = ElementParser.parse(input);
		pdfuniversalobject.objdata = pdfelement;
		if (pdfelement instanceof PDFStream) {
			PDFStream pdfstream = (PDFStream) pdfelement;
			if (pdfstream.dict.containsKey("/Length"))
				pdfstream.streamLength = readLength((PDFElement) pdfstream.dict
						.get("/Length"));
			else
				throw new ImageFormatException("Object " + i
						+ " with stream has not length");
		}
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("</readObject>");
		return pdfuniversalobject;
	}

	public void parsePageHeader(int i) throws IOException, ImageFormatException {
		PDFUniversalObject pdfuniversalobject = getObject(ParseUtils
				.getIntFromHash(pagestoobj, i));
	}

	public void getContent(int i, OutputStream outputstream)
			throws IOException, ImageFormatException {
		PDFUniversalObject pdfuniversalobject = getObject(ParseUtils
				.getIntFromHash(pagestoobj, i));
		PDFDictionary pdfdictionary = getDictionary(pdfuniversalobject);
		Array array = new Array(0);
		if (pdfdictionary.dict.containsKey("/Contents")) {
			PDFElement pdfelement = (PDFElement) pdfdictionary.dict
					.get("/Contents");
			if (pdfelement instanceof PDFArray)
				array = composeArray(pdfelement, outputstream);
			else if (pdfelement instanceof PDFIndirectObject) {
				PDFUniversalObject pdfuniversalobject_19_ = getObject(((PDFIndirectObject) pdfelement)
						.intValue());
				if (pdfuniversalobject_19_.objdata instanceof PDFDictionary) {
					PDFStream pdfstream = getStream(pdfuniversalobject_19_);
					pdfstream.decompress();
					array.put(0, pdfstream);
					if (pdfstream.minFilter().equals("/LZWDecode"))
						isLZW = true;
				} else if (pdfuniversalobject_19_.objdata instanceof PDFArray)
					array = composeArray(pdfuniversalobject_19_.objdata,
							outputstream);
			} else
				throw new ImageFormatException("Broken pdf file "
						+ this.toDisplayString()
						+ ". Wrong page contents value");
		}
		if (isLZW)
			copyLZWStrips(outputstream, array);
		else {
			for (int i_20_ = 0; i_20_ < array.length(); i_20_++) {
				PDFStream pdfstream = (PDFStream) array.get(i_20_);
				outputstream.write(pdfstream.array);
			}
		}
	}

	Array composeArray(PDFElement pdfelement, OutputStream outputstream)
			throws IOException, ImageFormatException {
		Array array = new Array(0);
		int i = 0;
		for (int i_21_ = 0; i_21_ < ((PDFArray) pdfelement).array.length(); i_21_++) {
			PDFElement pdfelement_22_ = (PDFElement) ((PDFArray) pdfelement).array
					.get(i_21_);
			if (pdfelement_22_ instanceof PDFIndirectObject) {
				PDFUniversalObject pdfuniversalobject = getObject(((PDFIndirectObject) ((PDFArray) pdfelement).array
						.get(i_21_)).intValue());
				PDFStream pdfstream = getStream(pdfuniversalobject);
				pdfstream.decompress();
				if (pdfstream.minFilter().equals("/LZWDecode"))
					isLZW = true;
				array.put(i, pdfstream);
				i++;
			} else
				throw new ImageFormatException("Broken pdf file "
						+ this.toDisplayString()
						+ ". Wrong page contents array");
		}
		return array;
	}

	private void copyLZWStrips(OutputStream outputstream, Array array)
			throws IOException, ImageFormatException {
		byte[] is = new byte[8192];
		byte i = 0;
		int i_23_ = 0;
		for (int i_24_ = 0; i_24_ < array.length(); i_24_++) {
			int i_25_ = ((PDFStream) array.get(i_24_)).array.length;
			if (i_25_ < 4)
				throw new ImageFormatException(
						"An LZW-encoded strip is too short, only "
								+ i_25_
								+ " bytes long - data format error in PDF file "
								+ this.toDisplayString());
			if (is.length < i_25_)
				is = new byte[i_25_];
			is = ((PDFStream) array.get(i_24_)).array;
			int i_26_ = ((is[0] & 0xff) << 1) + ((is[1] & 0xff) >> 7);
			if (i_26_ != 256)
				throw new ImageFormatException(
						"An LZW-encoded strip does not start with ClearCode pattern (100000000) - data format error in PDF image "
								+ this.toDisplayString());
			long l = (long) ((is[i_25_ - 4] << 24 & ~0xffffff)
					+ (is[i_25_ - 3] << 16 & 0xff0000)
					+ (is[i_25_ - 2] << 8 & 0xff00) + (is[i_25_ - 1] & 0xff));
			int i_27_;
			for (i_27_ = 0; i_27_ < 24; i_27_++) {
				if ((l >> i_27_ & 0x1ffL) == 257L)
					break;
			}
			if (i_27_ == 24)
				throw new ImageFormatException(
						"An LZW-encoded strip does not end with EOI pattern (100000001) - data format error in PDF file "
								+ this.toDisplayString());
			if (i_23_ != 0) {
				i |= (0xff & is[0]) >>> i_23_;
				is[0] <<= 8 - i_23_;
				for (int i_28_ = 0; i_28_ < i_25_ - 1; i_28_++) {
					is[i_28_] |= (0xff & is[i_28_ + 1]) >>> i_23_;
					is[i_28_ + 1] <<= 8 - i_23_;
				}
				outputstream.write(i);
			}
			int i_29_ = 8 * i_25_ - 9 - i_27_ - (8 - i_23_) % 8;
			i_25_ = i_29_ / 8;
			i_23_ = i_29_ % 8;
			i = (byte) (is[i_25_] & (byte) (255 << 8 - i_23_));
			outputstream.write(is, 0, i_25_);
		}
		byte i_30_ = (byte) (i | (byte) (128 >>> i_23_));
		byte i_31_ = (byte) (128 >>> i_23_);
		outputstream.write(i_30_);
		outputstream.write(i_31_);
	}

	public PDFDictionary getDictionary(PDFUniversalObject pdfuniversalobject)
			throws ImageFormatException {
		Object object = null;
		if (!(pdfuniversalobject.objdata instanceof PDFDictionary))
			throw new ImageFormatException("Broken page tree object "
					+ pdfuniversalobject.obj_id);
		return (PDFDictionary) pdfuniversalobject.objdata;
	}

	public PDFDictionary getDictionary(PDFElement pdfelement)
			throws ImageFormatException {
		if (!(pdfelement instanceof PDFDictionary))
			throw new ImageFormatException("Broken page dictionary "
					+ pdfelement);
		return (PDFDictionary) pdfelement;
	}

	public PDFElement readTillUnIndirectObject(PDFElement pdfelement)
			throws IOException, ImageFormatException {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("<readTillUnIndirectObject/>");
		if (!(pdfelement instanceof PDFIndirectObject))
			return pdfelement;
		PDFUniversalObject pdfuniversalobject;
		for (pdfuniversalobject = getObject(((PDFIndirectObject) pdfelement)
				.intValue()); pdfuniversalobject.objdata instanceof PDFIndirectObject; pdfuniversalobject = getObject(((PDFIndirectObject) pdfuniversalobject.objdata)
				.intValue())) {
			/* empty */
		}
		return pdfuniversalobject.objdata;
	}

	long readLength(PDFElement pdfelement) throws IOException,
			ImageFormatException {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("<readLength/>");
		PDFElement pdfelement_32_ = readTillUnIndirectObject(pdfelement);
		if (pdfelement_32_ instanceof PDFNumeric
				&& ((PDFNumeric) pdfelement_32_).isUnsignedInteger)
			return ParseUtils.readDigits(((PDFNumeric) pdfelement_32_).value);
		throw new ImageFormatException("Wrong Length element " + pdfelement_32_);
	}

	public PDFStream getStream(PDFUniversalObject pdfuniversalobject)
			throws IOException, ImageFormatException {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("<getStream>");
		PDFDictionary pdfdictionary = getDictionary(pdfuniversalobject);
		PDFStream pdfstream = (PDFStream) pdfuniversalobject.objdata;
		pdfstream.filters = readTillUnIndirectObject((PDFElement) pdfstream.dict
				.get("/Filter"));
		pdfstream.array = getStream(pdfstream.streamOffset,
				pdfstream.streamLength);
		String string = null;
		if (pdfstream.minFilter().equals("/LZWDecode"))
			isLZW = true;
		if (string == null)
			string = pdfstream.minFilter();
		else if (!string.equals(string))
			throw new ImageFormatException(
					"Different filters in contents of one page is not supported!");
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("</getStream>");
		return pdfstream;
	}

	byte[] getStream(long l, long l_33_) throws IOException {
		input.raf.seek(l);
		byte[] is = new byte[(int) l_33_];
		input.raf.readFully(is);
		return is;
	}

	public Integer newID(Object object) {
		return newID((Integer) object);
	}

	public Integer newID(Integer integer) {
		return (Integer) converObjects.get("" + integer);
	}

	public Integer newID(int i) {
		return (Integer) converObjects.get("" + i);
	}

	public int addNewID(String string, Integer integer) {
		if (!converObjects.containsKey(string)) {
			converObjects.put("" + string, integer);
			return integer.intValue() + 1;
		}
		return integer.intValue();
	}

	public int addNewID(int i, int i_34_) {
		return addNewID("" + i, new Integer(i_34_));
	}

	public int addNewID(Integer integer, int i) {
		return addNewID("" + integer, new Integer(i));
	}

	public void open() throws IOException {
		open(this.openSeekableImageStream());
	}

	public void open(SeekableInput seekableinput) throws IOException {
		input = new Lexer(seekableinput);
	}

	public void close() throws IOException {
		input.raf.close();
		input = null;
	}
}