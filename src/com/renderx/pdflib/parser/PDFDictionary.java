/*
 * PDFDictionary - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib.parser;

import java.io.IOException;
import java.util.Enumeration;

import com.renderx.pdflib.PDFOutputStream;
import com.renderx.util.Hashtable;
import com.renderx.util.List;

public class PDFDictionary extends PDFElement {
	public Hashtable dict = new Hashtable();

	public void writeDict(PDFOutputStream pdfoutputstream,
			PDFInstance pdfinstance) throws IOException {
		Enumeration enumeration = dict.keys();
		pdfoutputstream.write(60);
		pdfoutputstream.write(60);
		while (enumeration.hasMoreElements()) {
			String string = (String) enumeration.nextElement();
			PDFElement pdfelement = (PDFElement) dict.get(string);
			pdfoutputstream.write(string.getBytes());
			pdfoutputstream.write(32);
			pdfelement.write(pdfoutputstream, pdfinstance);
			pdfoutputstream.write(10);
		}
		pdfoutputstream.write(62);
		pdfoutputstream.write(62);
	}

	public void write(PDFOutputStream pdfoutputstream, PDFInstance pdfinstance)
			throws IOException {
		writeDict(pdfoutputstream, pdfinstance);
	}

	public List getIndirectObjects() {
		List list = new List();
		Enumeration enumeration = dict.keys();
		while (enumeration.hasMoreElements()) {
			List list_0_ = ((PDFElement) dict.get((String) enumeration
					.nextElement())).getIndirectObjects();
			if (list_0_ != null)
				list.append(list_0_);
		}
		return list;
	}

	public String getType() {
		if (dict.containsKey("/Type"))
			return ((PDFElement) dict.get("/Type")).toString();
		return "";
	}

	public String toString() {
		String string = "<<";
		Enumeration enumeration = dict.keys();
		while (enumeration.hasMoreElements()) {
			String string_1_ = (String) enumeration.nextElement();
			PDFElement pdfelement = (PDFElement) dict.get(string_1_);
			string += string_1_ + "='";
			string += pdfelement + "' ";
		}
		string += ">>";
		return string;
	}
}