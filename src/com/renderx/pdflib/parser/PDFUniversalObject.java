/*
 * PDFUniversalObject - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib.parser;

import java.io.IOException;

import com.renderx.pdflib.IndirectObject;
import com.renderx.pdflib.PDFIOException;
import com.renderx.pdflib.PDFOutputStream;
import com.renderx.util.List;

public class PDFUniversalObject {
	public int obj_id;

	public PDFElement objdata = null;

	public PDFUniversalObject(int i) {
		obj_id = i;
	}

	public String getType() {
		if (objdata instanceof PDFDictionary)
			return ((PDFDictionary) objdata).getType();
		return "";
	}

	int getID() {
		return obj_id;
	}

	public void setID(int i) {
		obj_id = i;
	}

	void writeBody(PDFOutputStream pdfoutputstream) throws PDFIOException {
		/* empty */
	}

	public void write(PDFOutputStream pdfoutputstream, PDFInstance pdfinstance)
			throws PDFIOException {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("<UniversalObject number=\"" + obj_id
					+ "\">");
		if (PDFOutputStream.dump != null && objdata instanceof PDFDictionary
				&& ((PDFDictionary) objdata).dict.containsKey("/Type"))
			PDFOutputStream.dump.println("<type name=\""
					+ ((PDFDictionary) objdata).dict.get("/Type") + "\"/>");
		pdfoutputstream.trailer.xref.addObjectOffset(
				new IndirectObject(obj_id), (long) pdfoutputstream.size());
		pdfoutputstream.generateCurrentKey(getID());
		writeHead(pdfoutputstream);
		try {
			if (objdata != null) {
				if (objdata instanceof PDFStream)
					((PDFStream) objdata).write(pdfoutputstream, pdfinstance,
							obj_id);
				else
					objdata.write(pdfoutputstream, pdfinstance);
			}
		} catch (IOException ioexception) {
			pdfoutputstream.exception("", ioexception);
		}
		writeTail(pdfoutputstream);
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dump.println("</UniversalObject>");
	}

	public void writeHead(PDFOutputStream pdfoutputstream)
			throws PDFIOException {
		pdfoutputstream.println(obj_id + " 0 obj");
	}

	void writeTail(PDFOutputStream pdfoutputstream) throws PDFIOException {
		pdfoutputstream.println("\nendobj");
	}

	public List getIndirectObjects() {
		if (objdata != null)
			return objdata.getIndirectObjects();
		return new List();
	}
}