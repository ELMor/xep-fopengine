/*
 * PDFIndirectObject - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib.parser;

import java.io.IOException;

import com.renderx.pdflib.PDFOutputStream;
import com.renderx.util.List;

public class PDFIndirectObject extends PDFElement {
	public String str;

	public PDFIndirectObject(String string) {
		str = string;
	}

	void write(PDFOutputStream pdfoutputstream, PDFInstance pdfinstance)
			throws IOException {
		if (pdfinstance.renumber) {
			Integer integer = (Integer) pdfinstance.converObjects.get(str);
			if (integer == null) {
				integer = new Integer(pdfoutputstream.lastObjectNumber++);
				pdfinstance.converObjects.put(str, integer);
				pdfinstance.pendingObjects.append(str);
			}
			pdfoutputstream.write(new String(integer.toString() + " 0 R ")
					.getBytes());
		} else
			pdfoutputstream.write(new String(str + " 0 R ").getBytes());
	}

	public List getIndirectObjects() {
		List list = new List();
		list.append(new Integer(str));
		return list;
	}

	public String toString() {
		return "{" + str + " R}";
	}

	public String toStringR() {
		return str + " R";
	}

	public int intValue() {
		return new Integer(str).intValue();
	}
}