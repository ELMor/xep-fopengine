/*
 * ElementParser - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib.parser;

import java.io.IOException;

import com.renderx.graphics.ImageFormatException;
import com.renderx.pdflib.PDFOutputStream;
import com.renderx.util.Array;
import com.renderx.util.SeekableFileInputStream;
import com.renderx.util.Stack;

class ElementParser {
	public static synchronized PDFElement parse(Lexer lexer)
			throws IOException, ImageFormatException {
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("<parse>");
		Token token = lexer.nextToken();
		if (token.type == 8 || token.type == 6 || token.type == 4
				|| token.type == 12 || token.type == 11 || token.type == 10)
			throw new IOException("Wrong PDF file.");
		Stack stack = new Stack();
		Object object = null;
		PDFElement pdfelement;
		try {
			for (/**/; token.type != 12 && token.type != 11
					&& token.type != 10; token = lexer.nextToken()) {
				if (token.type == 9) {
					if (stack.length() != 2) {
						stack.pop();
						throw new ImageFormatException(
								"Wrong PDF file: too many tokens before object "
										+ stack.pop() + ".");
					}
				} else if (token.type == 2) {
					if (PDFOutputStream.dump != null)
						PDFOutputStream.dumpprintln("<pushPDFString/>");
					stack.push(new PDFString(token.value));
				} else if (token.type == 1) {
					if (PDFOutputStream.dump != null)
						PDFOutputStream.dumpprintln("<pushPDFName/>");
					stack.push(new PDFName(token.value));
				} else if (token.type == 13) {
					if (PDFOutputStream.dump != null)
						PDFOutputStream.dumpprintln("<pushPDFSimple/>");
					stack.push(new PDFSimple(token.value));
				} else if (token.type == 3) {
					if (PDFOutputStream.dump != null)
						PDFOutputStream.dumpprintln("<pushPDFNumeric/>");
					stack.push(new PDFNumeric(token.value, true));
				} else if (token.type == 4) {
					if (PDFOutputStream.dump != null)
						PDFOutputStream.dumpprintln("<pushPDFIndirectObject/>");
					PDFNumeric pdfnumeric = (PDFNumeric) stack.pop();
					PDFNumeric pdfnumeric_0_ = (PDFNumeric) stack.pop();
					if (pdfnumeric.isUnsignedInteger
							|| pdfnumeric_0_.isUnsignedInteger)
						stack.push(new PDFIndirectObject(pdfnumeric_0_.value));
				} else if (token.type == 6) {
					if (PDFOutputStream.dump != null)
						PDFOutputStream.dumpprintln("<pushPDFArray/>");
					Object object_1_ = stack.pop();
					Array array = new Array();
					int i = 0;
					while (!(object_1_ instanceof Token)) {
						array.put(i, (PDFElement) object_1_);
						object_1_ = stack.pop();
						i++;
					}
					int i_2_ = array.length();
					for (int i_3_ = 0; i_3_ < i_2_; i_3_++) {
						/* empty */
					}
					PDFArray pdfarray = new PDFArray();
					pdfarray.array = new Array(array.length());
					for (int i_4_ = 0; i_4_ < i_2_; i_4_++)
						pdfarray.array.put(i_4_, array.get(i - i_4_ - 1));
					stack.push(pdfarray);
				} else if (token.type == 8) {
					if (PDFOutputStream.dump != null)
						PDFOutputStream.dumpprintln("<pushPDFDictionary/>");
					Object object_5_ = stack.pop();
					PDFDictionary pdfdictionary = new PDFDictionary();
					for (/**/; !(object_5_ instanceof Token); object_5_ = stack
							.pop()) {
						PDFName pdfname = (PDFName) stack.pop();
						pdfdictionary.dict.put(pdfname.value,
								(PDFElement) object_5_);
					}
					stack.push(pdfdictionary);
				} else {
					if (PDFOutputStream.dump != null)
						PDFOutputStream.dumpprintln("<push" + token + "/>");
					stack.push(token);
				}
			}
			if (token.type == 11) {
				pdfelement = (PDFElement) stack.pop();
				PDFStream pdfstream = new PDFStream((PDFDictionary) pdfelement);
				pdfstream.streamOffset = lexer.raf.getFilePointer();
				stack.push(pdfstream);
			}
			pdfelement = (PDFElement) stack.pop();
		} catch (ClassCastException classcastexception) {
			classcastexception.printStackTrace();
			throw new IOException("Broken PDF file!");
		}
		if (PDFOutputStream.dump != null)
			PDFOutputStream.dumpprintln("</parse>");
		return pdfelement;
	}

	public static void main(String[] strings) throws Exception {
		PDFElement pdfelement = parse(new Lexer(new SeekableFileInputStream(
				strings[0])));
		System.out.println(pdfelement.toString());
	}
}