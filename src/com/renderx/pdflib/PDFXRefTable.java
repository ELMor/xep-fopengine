/*
 * PDFXRefTable - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

import com.renderx.util.Array;

public class PDFXRefTable {
	Array offsets;

	public long offset;

	public static long bodySize = 280L;

	public static long itemSize = 20L;

	public long xrefSize = 0L;

	public PDFXRefTable() {
		offsets = new Array();
		offset = -1L;
	}

	public void addObjectOffset(IndirectObject indirectobject, long l) {
		offsets.put(indirectobject.num, new Long(l));
	}

	public Long getOffset(IndirectObject indirectobject) {
		return getOffset(indirectobject.num);
	}

	public Long getOffset(int i) {
		return (Long) offsets.get(i);
	}

	void write(PDFOutputStream pdfoutputstream) {
		int i;
		for (i = 0; i < offsets.length() && offsets.get(i) == null; i++) {
			/* empty */
		}
		if (offset == -1L)
			offset = (long) pdfoutputstream.size();
		pdfoutputstream.println("xref");
		pdfoutputstream.println(i + " " + (offsets.length() - i));
		int i_0_ = offsets.length();
		for (int i_1_ = i; i_1_ < i_0_; i_1_++) {
			String string = "00000000000";
			Long var_long = (Long) offsets.get(i_1_);
			if (var_long == null)
				throw new PDFException(
						"Pdflib internal error. Broken xref. Please report to manufacturer!.");
			String string_2_ = var_long.toString();
			string = string.substring(0, 10 - string_2_.length());
			if (i_1_ != 0)
				pdfoutputstream.println(string + string_2_ + " 00000 n ");
			else
				pdfoutputstream.println("0000000000 65535 f ");
		}
	}
}