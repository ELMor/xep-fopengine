/*
 * LocalLink - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.pdflib;

class LocalLink extends Annot {
	Destination dest;

	LocalLink(IndirectObject indirectobject, float f, float f_0_, float f_1_,
			float f_2_, int i, float f_3_, float f_4_, String string) {
		super(indirectobject, f, f_0_, f_1_, f_2_);
		dest = new Destination(i, f_3_, f_4_);
	}

	LocalLink(IndirectObject indirectobject, String string, float f,
			float f_5_, float f_6_, float f_7_, String string_8_) {
		super(indirectobject, f, f_5_, f_6_, f_7_);
		dest = new Destination(string);
	}

	String getSubType() {
		return "Link";
	}

	int getPageNumber() {
		return dest.page_number;
	}

	void setPageID(IndirectObject indirectobject) {
		dest.page_id = indirectobject;
	}

	void writeAnnotBody(PDFOutputStream pdfoutputstream) {
		dest.write(pdfoutputstream);
	}

	void writeSubObjects(PDFOutputStream pdfoutputstream) {
		/* empty */
	}
}