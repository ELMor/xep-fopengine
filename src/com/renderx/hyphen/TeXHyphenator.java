/*
 * TeXHyphenator - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.hyphen;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Enumeration;
import java.util.StringTokenizer;

import com.renderx.util.DefaultErrorHandler;
import com.renderx.util.ErrorHandler;
import com.renderx.util.Hashtable;
import com.renderx.util.List;

public class TeXHyphenator implements Hyphenator {
	private final Hashtable exceptions = new Hashtable();

	private final List[] entrytab = new List[256];

	private final ErrorHandler errHandler;

	private static class Scanner {
		static final short EOF = 0;

		static final short LBRAC = 1;

		static final short RBRAC = 2;

		static final short PATTERNS = 3;

		static final short EXCEPTIONS = 4;

		static final short PATTERN = 5;

		char[] pattern = new char[0];

		int patlen;

		private Reader in;

		private ErrorHandler errHandler;

		private int cc = 10;

		private int cc1 = -1;

		private int prevlno = -1;

		private int lno = 0;

		private int cno = 0;

		private static final Hashtable acctab = new Hashtable();

		private static final int cp2i(int i, int i_0_) {
			return (i << 8) + i_0_;
		}

		Scanner(Reader reader, ErrorHandler errorhandler) throws IOException {
			in = reader;
			errHandler = errorhandler;
			read();
		}

		short getSym() {
			for (;;) {
				if (isSpace())
					read();
				else {
					switch (cc) {
					case 37:
						do {
							read();
							if (isNL())
								break;
						} while (cc != -1);
						break;
					case 92:
						read();
						if (isLetter()) {
							patlen = 0;
							do
								cc2pat();
							while (isLetter());
							String string = new String(pattern, 0, patlen);
							if (string.equals("patterns"))
								return (short) 3;
							if (string.equals("hyphenation"))
								return (short) 4;
							if (string.equals("endinput"))
								return (short) 0;
							warning("shamelessly skipping \\" + string);
							break;
						}
					/* fall through */
					case 123:
						read();
						return (short) 1;
					case 125:
						read();
						return (short) 2;
					case -1:
						return (short) 0;
					default:
						if (isPatChar()) {
							patlen = 0;
							do
								cc2pat();
							while (isPatChar());
							if (patlen > 1)
								return (short) 5;
						} else
							read();
					}
				}
			}
		}

		private void read() {
			if (cc != -1) {
				if (isNL()) {
					lno++;
					cno = 0;
				}
				try {
					if (cc1 != -1) {
						cc = cc1;
						cc1 = -1;
					} else
						cc = in.read();
					switch (cc) {
					case 94:
						cc1 = in.read();
						if (cc1 == 94) {
							cc1 = -1;
							int i = in.read();
							if ((cc = hexval(i)) == -1)
								cc = i + 64 & 0x7f;
							else {
								cc1 = in.read();
								if ((i = hexval(cc1)) != -1) {
									cc1 = -1;
									cc *= 16;
									cc += i;
								}
							}
						}
						break;
					case 92:
						cc1 = in.read();
						switch (cc1) {
						case 34:
						case 39:
						case 46:
						case 61:
						case 72:
						case 94:
						case 96:
						case 97:
						case 98:
						case 99:
						case 100:
						case 105:
						case 107:
						case 108:
						case 111:
						case 114:
						case 115:
						case 117:
						case 118:
						case 126: {
							int i = cc1 << 8;
							int i_1_ = -1;
							int i_2_ = in.read();
							if ((i_2_ == 32 && cc1 != 108 && cc1 != 111 && cc1 != 105)
									|| i_2_ == 123 || i_2_ == 92) {
								i_1_ = i_2_;
								i_2_ = in.read();
								if (i_1_ == 123 && i_2_ == 125)
									i_1_ = -1;
								i_2_ = 32;
							}
							cc1 = -1;
							i += i_2_;
							if (i_1_ == 123)
								in.read();
							Object object = acctab.get(new Integer(i));
							cc = (object != null ? ((Integer) object)
									.intValue() : i_2_);
							break;
						}
						}
						break;
					}
				} catch (IOException ioexception) {
					error(ioexception.toString());
					cc = -1;
				}
				cno++;
			}
		}

		private int hexval(int i) {
			switch (i) {
			case 48:
			case 49:
			case 50:
			case 51:
			case 52:
			case 53:
			case 54:
			case 55:
			case 56:
			case 57:
				return i - 48;
			case 97:
			case 98:
			case 99:
			case 100:
			case 101:
			case 102:
				return i - 97 + 10;
			default:
				return -1;
			}
		}

		private void cc2pat() {
			if (patlen == pattern.length) {
				char[] cs = new char[patlen * 2 + 1];
				System.arraycopy(pattern, 0, cs, 0, patlen);
				pattern = cs;
			}
			pattern[patlen++] = Character.toLowerCase((char) cc);
			read();
		}

		private boolean isLetter() {
			if (cc == -1)
				return false;
			return Character.isLetter((char) cc);
		}

		private boolean isSpace() {
			if (cc == -1)
				return false;
			return Character.isSpaceChar((char) cc) || isNL();
		}

		private boolean isNL() {
			if (cc == 10)
				return true;
			if (cc == 13) {
				if (cc1 == -1) {
					try {
						cc = in.read();
					} catch (IOException ioexception) {
						error(ioexception.toString());
						cc = -1;
					}
				} else {
					cc = cc1;
					cc1 = -1;
				}
				if (cc != 10)
					cc1 = cc;
				return true;
			}
			return false;
		}

		private boolean isPatChar() {
			if (cc == -1)
				return false;
			char c = (char) cc;
			return Character.isLetterOrDigit(c) || c == '.' || c == '-';
		}

		private void error(String string) {
			if (prevlno != lno) {
				prevlno = lno;
				errHandler.error("(" + lno + "," + cno + "): " + string);
			}
		}

		private void warning(String string) {
			if (prevlno != lno) {
				prevlno = lno;
				errHandler.warning("(" + lno + "," + cno + "): " + string);
			}
		}

		static {
			acctab.put(new Integer(cp2i(96, 97)), new Integer(224));
			acctab.put(new Integer(cp2i(96, 101)), new Integer(232));
			acctab.put(new Integer(cp2i(96, 105)), new Integer(236));
			acctab.put(new Integer(cp2i(96, 111)), new Integer(242));
			acctab.put(new Integer(cp2i(96, 117)), new Integer(249));
			acctab.put(new Integer(cp2i(96, 119)), new Integer(7809));
			acctab.put(new Integer(cp2i(96, 121)), new Integer(7923));
			acctab.put(new Integer(cp2i(39, 97)), new Integer(225));
			acctab.put(new Integer(cp2i(39, 99)), new Integer(263));
			acctab.put(new Integer(cp2i(39, 101)), new Integer(233));
			acctab.put(new Integer(cp2i(39, 103)), new Integer(501));
			acctab.put(new Integer(cp2i(39, 105)), new Integer(237));
			acctab.put(new Integer(cp2i(39, 107)), new Integer(7729));
			acctab.put(new Integer(cp2i(39, 108)), new Integer(314));
			acctab.put(new Integer(cp2i(39, 109)), new Integer(7743));
			acctab.put(new Integer(cp2i(39, 110)), new Integer(324));
			acctab.put(new Integer(cp2i(39, 111)), new Integer(243));
			acctab.put(new Integer(cp2i(39, 112)), new Integer(7765));
			acctab.put(new Integer(cp2i(39, 114)), new Integer(341));
			acctab.put(new Integer(cp2i(39, 115)), new Integer(347));
			acctab.put(new Integer(cp2i(39, 117)), new Integer(250));
			acctab.put(new Integer(cp2i(39, 119)), new Integer(7811));
			acctab.put(new Integer(cp2i(39, 121)), new Integer(253));
			acctab.put(new Integer(cp2i(39, 122)), new Integer(378));
			acctab.put(new Integer(cp2i(94, 97)), new Integer(226));
			acctab.put(new Integer(cp2i(94, 99)), new Integer(265));
			acctab.put(new Integer(cp2i(94, 101)), new Integer(234));
			acctab.put(new Integer(cp2i(94, 103)), new Integer(285));
			acctab.put(new Integer(cp2i(94, 104)), new Integer(293));
			acctab.put(new Integer(cp2i(94, 105)), new Integer(238));
			acctab.put(new Integer(cp2i(94, 106)), new Integer(309));
			acctab.put(new Integer(cp2i(94, 111)), new Integer(244));
			acctab.put(new Integer(cp2i(94, 115)), new Integer(349));
			acctab.put(new Integer(cp2i(94, 117)), new Integer(251));
			acctab.put(new Integer(cp2i(94, 119)), new Integer(373));
			acctab.put(new Integer(cp2i(94, 121)), new Integer(375));
			acctab.put(new Integer(cp2i(94, 122)), new Integer(7825));
			acctab.put(new Integer(cp2i(34, 97)), new Integer(228));
			acctab.put(new Integer(cp2i(34, 101)), new Integer(235));
			acctab.put(new Integer(cp2i(34, 104)), new Integer(7719));
			acctab.put(new Integer(cp2i(34, 105)), new Integer(239));
			acctab.put(new Integer(cp2i(34, 111)), new Integer(246));
			acctab.put(new Integer(cp2i(34, 116)), new Integer(7831));
			acctab.put(new Integer(cp2i(34, 117)), new Integer(252));
			acctab.put(new Integer(cp2i(34, 119)), new Integer(7813));
			acctab.put(new Integer(cp2i(34, 120)), new Integer(7821));
			acctab.put(new Integer(cp2i(34, 121)), new Integer(255));
			acctab.put(new Integer(cp2i(72, 111)), new Integer(337));
			acctab.put(new Integer(cp2i(72, 117)), new Integer(369));
			acctab.put(new Integer(cp2i(126, 97)), new Integer(227));
			acctab.put(new Integer(cp2i(126, 101)), new Integer(7869));
			acctab.put(new Integer(cp2i(126, 105)), new Integer(297));
			acctab.put(new Integer(cp2i(126, 110)), new Integer(241));
			acctab.put(new Integer(cp2i(126, 111)), new Integer(245));
			acctab.put(new Integer(cp2i(126, 117)), new Integer(361));
			acctab.put(new Integer(cp2i(126, 118)), new Integer(7805));
			acctab.put(new Integer(cp2i(126, 121)), new Integer(7929));
			acctab.put(new Integer(cp2i(117, 97)), new Integer(259));
			acctab.put(new Integer(cp2i(117, 101)), new Integer(277));
			acctab.put(new Integer(cp2i(117, 103)), new Integer(287));
			acctab.put(new Integer(cp2i(117, 105)), new Integer(301));
			acctab.put(new Integer(cp2i(117, 111)), new Integer(335));
			acctab.put(new Integer(cp2i(117, 117)), new Integer(365));
			acctab.put(new Integer(cp2i(118, 97)), new Integer(462));
			acctab.put(new Integer(cp2i(118, 32)), new Integer(711));
			acctab.put(new Integer(cp2i(118, 99)), new Integer(269));
			acctab.put(new Integer(cp2i(118, 100)), new Integer(271));
			acctab.put(new Integer(cp2i(118, 101)), new Integer(283));
			acctab.put(new Integer(cp2i(118, 103)), new Integer(487));
			acctab.put(new Integer(cp2i(118, 105)), new Integer(464));
			acctab.put(new Integer(cp2i(118, 106)), new Integer(496));
			acctab.put(new Integer(cp2i(118, 107)), new Integer(489));
			acctab.put(new Integer(cp2i(118, 108)), new Integer(318));
			acctab.put(new Integer(cp2i(118, 110)), new Integer(328));
			acctab.put(new Integer(cp2i(118, 111)), new Integer(466));
			acctab.put(new Integer(cp2i(118, 114)), new Integer(345));
			acctab.put(new Integer(cp2i(118, 115)), new Integer(353));
			acctab.put(new Integer(cp2i(118, 116)), new Integer(357));
			acctab.put(new Integer(cp2i(118, 117)), new Integer(468));
			acctab.put(new Integer(cp2i(118, 122)), new Integer(382));
			acctab.put(new Integer(cp2i(99, 99)), new Integer(231));
			acctab.put(new Integer(cp2i(99, 100)), new Integer(7697));
			acctab.put(new Integer(cp2i(99, 103)), new Integer(291));
			acctab.put(new Integer(cp2i(99, 104)), new Integer(7721));
			acctab.put(new Integer(cp2i(99, 107)), new Integer(311));
			acctab.put(new Integer(cp2i(99, 108)), new Integer(316));
			acctab.put(new Integer(cp2i(99, 110)), new Integer(326));
			acctab.put(new Integer(cp2i(99, 114)), new Integer(343));
			acctab.put(new Integer(cp2i(99, 115)), new Integer(351));
			acctab.put(new Integer(cp2i(99, 116)), new Integer(355));
			acctab.put(new Integer(cp2i(100, 97)), new Integer(7841));
			acctab.put(new Integer(cp2i(100, 98)), new Integer(7685));
			acctab.put(new Integer(cp2i(100, 100)), new Integer(7693));
			acctab.put(new Integer(cp2i(100, 101)), new Integer(7865));
			acctab.put(new Integer(cp2i(100, 104)), new Integer(7717));
			acctab.put(new Integer(cp2i(100, 105)), new Integer(7883));
			acctab.put(new Integer(cp2i(100, 107)), new Integer(7731));
			acctab.put(new Integer(cp2i(100, 108)), new Integer(7735));
			acctab.put(new Integer(cp2i(100, 109)), new Integer(7747));
			acctab.put(new Integer(cp2i(100, 110)), new Integer(7751));
			acctab.put(new Integer(cp2i(100, 111)), new Integer(7885));
			acctab.put(new Integer(cp2i(100, 114)), new Integer(7771));
			acctab.put(new Integer(cp2i(100, 115)), new Integer(7779));
			acctab.put(new Integer(cp2i(100, 116)), new Integer(7789));
			acctab.put(new Integer(cp2i(100, 117)), new Integer(7909));
			acctab.put(new Integer(cp2i(100, 118)), new Integer(7807));
			acctab.put(new Integer(cp2i(100, 119)), new Integer(7817));
			acctab.put(new Integer(cp2i(100, 121)), new Integer(7925));
			acctab.put(new Integer(cp2i(100, 122)), new Integer(7827));
			acctab.put(new Integer(cp2i(46, 99)), new Integer(267));
			acctab.put(new Integer(cp2i(46, 101)), new Integer(279));
			acctab.put(new Integer(cp2i(46, 103)), new Integer(289));
			acctab.put(new Integer(cp2i(46, 108)), new Integer(320));
			acctab.put(new Integer(cp2i(46, 122)), new Integer(380));
			acctab.put(new Integer(cp2i(114, 97)), new Integer(229));
			acctab.put(new Integer(cp2i(114, 117)), new Integer(367));
			acctab.put(new Integer(cp2i(114, 119)), new Integer(7832));
			acctab.put(new Integer(cp2i(114, 121)), new Integer(7833));
			acctab.put(new Integer(cp2i(107, 97)), new Integer(261));
			acctab.put(new Integer(cp2i(107, 101)), new Integer(281));
			acctab.put(new Integer(cp2i(107, 105)), new Integer(303));
			acctab.put(new Integer(cp2i(107, 111)), new Integer(491));
			acctab.put(new Integer(cp2i(107, 117)), new Integer(371));
			acctab.put(new Integer(cp2i(97, 97)), new Integer(229));
			acctab.put(new Integer(cp2i(97, 101)), new Integer(230));
			acctab.put(new Integer(cp2i(105, 32)), new Integer(305));
			acctab.put(new Integer(cp2i(108, 32)), new Integer(322));
			acctab.put(new Integer(cp2i(111, 32)), new Integer(248));
			acctab.put(new Integer(cp2i(111, 101)), new Integer(339));
			acctab.put(new Integer(cp2i(115, 115)), new Integer(223));
		}
	}

	TeXHyphenator(Reader reader, ErrorHandler errorhandler) throws IOException {
		errHandler = errorhandler;
		for (int i = 0; i != 256; i++)
			entrytab[i] = new List();
		Scanner scanner = new Scanner(reader, errHandler);
		int i = 0;
		while_4_: for (;;) {
			short i_3_ = scanner.getSym();
			switch (i_3_) {
			case 0:
				break while_4_;
			case 3:
			case 4:
				if (scanner.getSym() != 1)
					scanner.error("'{' expected");
				i = i_3_ == 3 ? 2 : 1;
				break;
			case 2:
				i = 0;
				break;
			case 5:
				switch (i) {
				case 2:
					readPattern(scanner);
					break;
				case 1:
					readException(scanner);
					break;
				}
				break;
			default:
				scanner.error("problem parsing input");
			}
		}
	}

	public String hyphenate(String string, int i, int i_4_) {
		if (i < 1)
			i = 1;
		if (i_4_ < 1)
			i_4_ = 1;
		if (string.length() < i_4_ + i)
			return string;
		char[] cs = new char[string.length() + 1];
		cs[cs.length - 1] = '\0';
		string.getChars(0, string.length(), cs, 0);
		char[] cs_5_ = new char[cs.length * 2 - 1];
		int i_6_ = -2147483648;
		int i_7_ = 0;
		int i_8_ = 0;
		boolean bool = false;
		for (;;) {
			if (bool) {
				if (Character.isLetter(cs[i_7_]))
					i_7_++;
				else {
					int i_9_ = i_7_ - i_6_;
					String string_10_ = new String(cs, i_6_, i_9_)
							.toLowerCase();
					int[] is = (int[]) exceptions.get(string_10_);
					if (is == null) {
						char[] cs_11_ = new char[i_9_ + 2];
						is = new int[cs_11_.length + 1];
						cs_11_[0] = cs_11_[cs_11_.length - 1] = '.';
						for (int i_12_ = 0; i_12_ != i_9_; i_12_++)
							cs_11_[1 + i_12_] = Character.toLowerCase(cs[i_6_
									+ i_12_]);
						for (int i_13_ = 0; i_13_ != i_9_; i_13_++) {
							int i_14_ = cs_11_[i_13_] % 256;
							List list = entrytab[i_14_];
							int i_15_ = i_13_;
							Enumeration enumeration = list.elements();
							while (enumeration.hasMoreElements()) {
								list = (List) enumeration.nextElement();
								if (((Character) list.car()).charValue() == cs_11_[i_15_]) {
									list = list.cdr();
									int[] is_16_ = (int[]) list.car();
									for (int i_17_ = 0; i_17_ != is_16_.length; i_17_++) {
										if (is_16_[i_17_] > is[i_13_ + i_17_])
											is[i_13_ + i_17_] = is_16_[i_17_];
									}
									if (++i_15_ == cs_11_.length)
										break;
									enumeration = list.cdr().elements();
								}
							}
						}
						int[] is_18_ = new int[i_9_];
						System.arraycopy(is, 2, is_18_, 0, i_9_);
						is = is_18_;
					}
					if (i + i_4_ <= i_9_) {
						for (int i_19_ = 0; i_19_ != i - 1; i_19_++)
							cs_5_[i_8_++] = cs[i_6_++];
						for (int i_20_ = i - 1; i_20_ != i_9_ - i_4_; i_20_++) {
							cs_5_[i_8_++] = cs[i_6_++];
							if (is[i_20_] % 2 == 1)
								cs_5_[i_8_++] = '\u00ad';
						}
						for (int i_21_ = i_9_ - i_4_; i_21_ != i_9_; i_21_++)
							cs_5_[i_8_++] = cs[i_6_++];
					} else {
						for (int i_22_ = 0; i_22_ != i_9_; i_22_++)
							cs_5_[i_8_++] = cs[i_6_++];
					}
					bool = false;
				}
			} else {
				if (Character.isLetter(cs[i_7_])) {
					i_6_ = i_7_;
					bool = true;
				} else {
					if (cs[i_7_] == 0)
						break;
					cs_5_[i_8_++] = cs[i_7_];
					if (cs[i_7_] == '-' || cs[i_7_] == '\u2010')
						cs_5_[i_8_++] = '\u200b';
				}
				i_7_++;
			}
		}
		return new String(cs_5_, 0, i_8_);
	}

	void readPattern(Scanner scanner) {
		List list = null;
		List list_23_ = (entrytab[(scanner.pattern[Character
				.isDigit(scanner.pattern[0]) ? 1 : 0] % '\u0100')]);
		int[] is = new int[scanner.patlen + 1];
		int i = 0;
		int i_24_ = 0;
		Enumeration enumeration = list_23_.elements();
		while_6_: do {
			do {
				if (Character.isDigit(scanner.pattern[i])) {
					is[i_24_++] = scanner.pattern[i++] - 48;
					if (i == scanner.patlen)
						break while_6_;
				} else
					is[i_24_++] = 0;
				while_5_: do {
					do {
						if (!enumeration.hasMoreElements()) {
							int[] is_25_ = new int[i_24_ + 1];
							for (int i_26_ = 0; i_26_ != is_25_.length; i_26_++)
								is_25_[i_26_] = 0;
							list = new List().snoc(
									new Character(scanner.pattern[i])).snoc(
									is_25_);
							list_23_.snoc(list);
							list_23_ = list;
							enumeration = list_23_.elements();
							enumeration.nextElement();
							enumeration.nextElement();
							break while_5_;
						}
						list = (List) enumeration.nextElement();
					} while (((Character) list.car()).charValue() != scanner.pattern[i]);
					list_23_ = list;
					enumeration = list_23_.elements();
					enumeration.nextElement();
					enumeration.nextElement();
				} while (false);
			} while (++i != scanner.patlen);
			is[i_24_++] = 0;
		} while (false);
		System.arraycopy(is, 0, (int[]) list.cdr().car(), 0, i_24_);
	}

	void readException(Scanner scanner) {
		int i = 0;
		int i_27_ = scanner.patlen;
		for (;;) {
			if (i_27_ == i)
				return;
			if (scanner.pattern[i] == '-')
				i++;
			else {
				if (scanner.pattern[i_27_ - 1] != '-')
					break;
				i_27_--;
			}
		}
		int[] is = new int[i_27_ - i];
		int i_28_ = 0;
		for (int i_29_ = i; i_29_ != i_27_; i_29_++) {
			if (scanner.pattern[i_29_] == '-')
				is[i_28_ - 1] = 1;
			else {
				scanner.pattern[i_28_] = scanner.pattern[i_29_];
				is[i_28_] = 0;
				i_28_++;
			}
		}
		exceptions.put(new String(scanner.pattern, 0, i_28_), is);
	}

	public static void main(String[] strings) throws Exception {
		if (strings.length != 1 && strings.length != 2) {
			System.err.println("Usage: java " + TeXHyphenator.class.getName()
					+ " table.tex [encoding]");
			System.exit(1);
		}
		FileInputStream fileinputstream = new FileInputStream(strings[0]);
		BufferedReader bufferedreader = new BufferedReader(
				new InputStreamReader(fileinputstream,
						(strings.length == 1 ? "ISO-8859-1" : strings[1])));
		TeXHyphenator texhyphenator = new TeXHyphenator(bufferedreader,
				new DefaultErrorHandler());
		bufferedreader.close();
		char[] cs = new char[8192];
		int i = 0;
		for (;;) {
			try {
				InputStreamReader inputstreamreader = new InputStreamReader(
						System.in);
				i = inputstreamreader.read(cs);
			} catch (IOException ioexception) {
				System.err.println(ioexception);
				System.exit(1);
			}
			if (i < 0)
				System.exit(0);
			StringTokenizer stringtokenizer = new StringTokenizer(new String(
					cs, 0, i));
			while (stringtokenizer.hasMoreTokens()) {
				System.out.print(texhyphenator.hyphenate(
						stringtokenizer.nextToken(), 1, 1).replace('\u00ad',
						'-'));
				if (stringtokenizer.hasMoreTokens())
					System.out.print(" ");
			}
			System.out.println();
		}
	}
}