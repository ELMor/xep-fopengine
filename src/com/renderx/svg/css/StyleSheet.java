/*
 * StyleSheet - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.svg.css;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Vector;

import com.renderx.util.Array;
import com.renderx.util.DefaultErrorHandler;
import com.renderx.util.ErrorHandler;
import com.renderx.util.URLSpec;

public class StyleSheet {
	private Vector ruleTable = new Vector();

	private Vector styleTable = new Vector();

	ErrorHandler errorHandler = new DefaultErrorHandler();

	class Rule extends Array {
		Selector _selector;

		Rule(Selector selector) {
			_selector = selector;
		}
	}

	public StyleSheet(String string, ErrorHandler errorhandler) {
		if (errorhandler != null)
			setErrorHandler(errorhandler);
		parse(new Lexer(string));
	}

	public void setErrorHandler(ErrorHandler errorhandler) {
		errorHandler = errorhandler;
	}

	private void parse(Lexer lexer) {
		lexer.nextIgnoreSpaces();
		while (lexer.getType() == 28) {
			try {
				String string = parseImport(lexer);
				if (string.length() != 0)
					parse(new Lexer(load(new URLSpec(string).openStream())));
			} catch (CSSParseException cssparseexception) {
				errorForward(lexer, cssparseexception);
			} catch (Exception exception) {
				errorHandler.warning("CSS parser: " + exception.getMessage());
			}
		}
		while (lexer.getType() != 0) {
			try {
				RuleSet ruleset = new RuleSet(lexer, errorHandler);
				styleTable.addElement(ruleset.getStyleDeclaration());
				for (int i = 0; i < ruleset.length(); i++)
					addRule((Selector) ruleset.get(i));
			} catch (CSSParseException cssparseexception) {
				errorForward(lexer, cssparseexception);
			}
		}
	}

	private String parseImport(Lexer lexer) throws CSSParseException {
		StringBuffer stringbuffer = new StringBuffer();
		lexer.nextIgnoreSpaces();
		while (lexer.getType() != 8) {
			switch (lexer.getType()) {
			case 0:
				throw new CSSParseException(
						"Unexpected end of style declaration");
			case -1:
				throw new CSSParseException("Expected @import directive");
			default:
				stringbuffer.append(lexer.getStringValue());
				lexer.nextIgnoreSpaces();
			}
		}
		lexer.nextIgnoreSpaces();
		return parseURL(stringbuffer.toString());
	}

	protected static String parseURL(String string) {
		String string_0_ = string.trim();
		if ((string_0_.startsWith("url(") || string_0_.startsWith("URL("))
				&& string_0_.charAt(string_0_.length() - 1) == ')')
			string_0_ = string_0_.substring(4, string_0_.length() - 1).trim();
		if (string_0_.length() > 2
				&& ((string_0_.charAt(0) == '\'' && string_0_.charAt(string_0_
						.length() - 1) == '\'') || (string_0_.charAt(0) == '\"' && string_0_
						.charAt(string_0_.length() - 1) == '\"')))
			string_0_ = string_0_.substring(1, string_0_.length() - 1);
		return string_0_;
	}

	public int indexOfRule(Selector selector) {
		for (int i = 0; i < ruleTable.size(); i++) {
			if (((Rule) ruleTable.elementAt(i))._selector.equals(selector))
				return i;
		}
		return -1;
	}

	private void addRule(Selector selector) {
		for (int i = 0; i < ruleTable.size(); i++) {
			Selector selector_1_ = ((Rule) ruleTable.elementAt(i))._selector;
			if (selector.equals(selector_1_)) {
				Rule rule = (Rule) ruleTable.elementAt(i);
				rule.put(rule.length(), new Integer(styleTable.size() - 1));
				ruleTable.setElementAt(rule, i);
				return;
			}
			if (selector.getSpecificity() < selector_1_.getSpecificity()) {
				Rule rule = new Rule(selector);
				rule.put(rule.length(), new Integer(styleTable.size() - 1));
				ruleTable.insertElementAt(rule, i);
				return;
			}
		}
		Rule rule = new Rule(selector);
		rule.put(rule.length(), new Integer(styleTable.size() - 1));
		ruleTable.addElement(rule);
	}

	public void addRule(Selector selector, Style style) {
		styleTable.addElement(style);
		addRule(selector);
	}

	public Selector getSelector(int i) {
		return ((Rule) ruleTable.elementAt(i))._selector;
	}

	public Style getStyle(int i) {
		Style style = new Style();
		Rule rule = (Rule) ruleTable.elementAt(i);
		for (int i_2_ = 0; i_2_ < rule.length(); i_2_++) {
			Style style_3_ = (Style) styleTable.elementAt(((Integer) rule
					.get(i_2_)).intValue());
			Enumeration enumeration = style_3_.keys();
			while (enumeration.hasMoreElements()) {
				Object object = enumeration.nextElement();
				style.put(object, style_3_.get(object));
			}
		}
		return style;
	}

	public int size() {
		return ruleTable.size();
	}

	private void errorForward(Lexer lexer, CSSParseException cssparseexception) {
		int i = 0;
		for (;;) {
			switch (lexer.getType()) {
			case 0:
				return;
			case 2:
				i--;
			/* fall through */
			case 8:
				if (i == 0) {
					lexer.nextIgnoreSpaces();
					return;
				}
			/* fall through */
			case -1:
				break;
			case 1:
				i++;
				break;
			}
			lexer.nextIgnoreSpaces();
		}
	}

	public static String load(InputStream inputstream) throws IOException {
		StringBuffer stringbuffer = new StringBuffer();
		for (;;) {
			int i = inputstream.read();
			if (i == -1)
				return stringbuffer.toString();
			if (i == 13 || i == 10)
				stringbuffer.append(' ');
			else
				stringbuffer.append((char) i);
		}
	}

	public static void main(String[] strings) throws IOException {
		if (strings.length != 1) {
			System.err
					.println("call: java com.renderx.svg.css.StyleSheet file_CSS1_style\n");
			System.exit(1);
		}
		StyleSheet stylesheet = new StyleSheet(load(new FileInputStream(
				strings[0])), null);
		for (int i = 0; i < stylesheet.size(); i++)
			System.out.println(stylesheet.getSelector(i).nodelist.toString()
					+ " --> " + stylesheet.getStyle(i) + " spec="
					+ stylesheet.getSelector(i).getSpecificity());
	}
}