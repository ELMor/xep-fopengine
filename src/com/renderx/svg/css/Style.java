/*
 * Style - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.svg.css;

import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Hashtable;

import com.renderx.util.DefaultErrorHandler;
import com.renderx.util.ErrorHandler;

public class Style extends Hashtable {
	public static final String IMPORTANT = "important";

	private int current;

	private final Lexer lexer;

	static final class Expression {
		String value = null;

		boolean important = false;

		public String toString() {
			return value.toString() + (important ? " !important" : "");
		}
	}

	public Style() {
		lexer = null;
	}

	public Style(Lexer lexer, ErrorHandler errorhandler) {
		this.lexer = lexer;
		current = this.lexer.getType();
		parse(errorhandler);
	}

	public Style(String string, ErrorHandler errorhandler) {
		lexer = new Lexer(string);
		current = lexer.nextIgnoreSpaces();
		parse(errorhandler);
	}

	public Enumeration properties() {
		return this.keys();
	}

	public String getValue(String string) {
		Expression expression = (Expression) this.get(string);
		return expression == null ? null : expression.toString();
	}

	public String getCleanValue(String string) {
		Expression expression = (Expression) this.get(string);
		return expression == null ? null : expression.value;
	}

	public boolean isImportant(String string) {
		Expression expression = (Expression) this.get(string);
		return expression == null ? false : expression.important;
	}

	private void parse(ErrorHandler errorhandler) {
		for (;;) {
			switch (current) {
			case 0:
			case 2:
				return;
			case 8:
				current = lexer.nextIgnoreSpaces();
				continue;
			case 20:
				break;
			default:
				errorForward(new CSSParseException("Expected identifier"),
						errorhandler);
				continue;
			}
			String string = lexer.getStringValue();
			Expression expression = new Expression();
			try {
				if ((current = lexer.nextIgnoreSpaces()) != 16)
					throw new CSSParseException("Expected symbol ':'");
				if ((current = lexer.nextIgnoreSpaces()) == -1)
					throw new CSSParseException("Expected expression");
				expression.value = parseExpression();
				if (current == 23) {
					if ((current = lexer.nextIgnoreSpaces()) == -1)
						throw new CSSParseException("Expected expression");
					if (lexer.getStringValue().equalsIgnoreCase("important"))
						expression.important = true;
					else
						throw new CSSParseException("Expected important symbol");
					current = lexer.nextIgnoreSpaces();
				}
				if (current == 8 || current == 2 || current == 0)
					this.put(string, expression);
				else
					throw new CSSParseException("Expected end of expression");
			} catch (CSSParseException cssparseexception) {
				errorForward(cssparseexception, errorhandler);
			}
		}
	}

	private String parseExpression() throws CSSParseException {
		StringBuffer stringbuffer = new StringBuffer(parseTerm());
		for (;;) {
			boolean bool = false;
			if (current == 6 || current == 10) {
				bool = true;
				stringbuffer.append(lexer.getStringValue());
				current = lexer.nextIgnoreSpaces();
			}
			switch (current) {
			case 0:
			case 2:
			case 8:
			case 23:
				if (bool)
					throw new CSSParseException("Unexpected end of expression");
				return stringbuffer.toString();
			default:
				if (!bool)
					stringbuffer.append(' ');
				stringbuffer.append(parseTerm());
			}
		}
	}

	private String parseTerm() throws CSSParseException {
		String string = "";
		switch (current) {
		case 4:
		case 5:
			string = lexer.getStringValue();
			current = lexer.nextIgnoreSpaces();
		/* fall through */
		default:
			switch (current) {
			case 27:
				string += lexer.getStringValue();
				current = lexer.nextToken();
				if (current != 51 && current != 20)
					throw new CSSParseException("Error in a hexcolor operand");
				string += lexer.getStringValue();
				current = lexer.nextIgnoreSpaces();
				break;
			case 19:
			case 20:
			case 51:
				string += lexer.getStringValue();
				current = lexer.nextIgnoreSpaces();
				break;
			case 52:
				string += parseFunction();
				break;
			default:
				throw new CSSParseException("Error in an operand");
			}
			return string;
		}
	}

	private String parseFunction() throws CSSParseException {
		int i = 1;
		String string = lexer.getStringValue();
		while (i > 0) {
			switch (lexer.nextToken()) {
			case -1:
			case 0:
				throw new CSSParseException("Error in a function");
			case 17:
			case 18:
				break;
			case 15:
				i--;
				string += lexer.getStringValue();
				break;
			case 14:
				i++;
			/* fall through */
			default:
				string += lexer.getStringValue();
			}
		}
		current = lexer.nextIgnoreSpaces();
		return string;
	}

	private void errorForward(CSSParseException cssparseexception,
			ErrorHandler errorhandler) {
		int i = 1;
		for (;;) {
			switch (current) {
			case 0:
				return;
			case 8:
				if (i > 1)
					break;
			/* fall through */
			case 2:
				if (--i == 0)
					return;
			/* fall through */
			case -1:
				break;
			case 1:
				i++;
				break;
			}
			current = lexer.nextIgnoreSpaces();
		}
	}

	public void merge(Style style_0_) {
		if (style_0_ != null) {
			Enumeration enumeration = style_0_.properties();
			while (enumeration.hasMoreElements()) {
				String string = (String) enumeration.nextElement();
				Expression expression = (Expression) style_0_.get(string);
				Expression expression_1_ = (Expression) this.get(string);
				if (expression_1_ == null || !expression_1_.important
						|| expression.important)
					this.put(string, expression);
			}
		}
	}

	public static void main(String[] strings) throws Exception {
		char[] cs = new char[8192];
		boolean bool = false;
		for (;;) {
			InputStreamReader inputstreamreader = new InputStreamReader(
					System.in);
			int i = inputstreamreader.read(cs);
			if (i < 0)
				System.exit(0);
			Style style = new Style(new String(cs, 0, i),
					new DefaultErrorHandler());
			Enumeration enumeration = style.properties();
			while (enumeration.hasMoreElements()) {
				String string = (String) enumeration.nextElement();
				String string_2_ = style.getValue(string);
				System.err.println(string + "=\"" + string_2_ + "\"");
			}
		}
	}
}