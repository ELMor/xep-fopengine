/*
 * Selector - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.svg.css;

import java.util.Enumeration;

import com.renderx.util.List;

public class Selector {
	private static final String FIRST_LETER = "first-letter";

	private static final String FIRST_LINE = "first-line";

	private static final String LINK = "link";

	private static final String VISITED = "visited";

	private static final String ACTIVE = "active";

	private final Lexer lexer;

	private int current;

	private short cntId = 0;

	private short cntCl = 0;

	private short cntEl = 0;

	public List nodelist;

	public class Node {
		public String elementName = null;

		public String className = null;

		public String pseudoClassName = null;

		public String pseudoElementName = null;

		public String elementID = null;

		public boolean equals(Node node_0_) {
			return (node_0_ != null
					&& (elementName == null ? elementName == node_0_.elementName
							: elementName.equals(node_0_.elementName))
					&& (elementID == null ? elementID == node_0_.elementID
							: elementID.equals(node_0_.elementID))
					&& (className == null ? className == node_0_.className
							: className.equals(node_0_.className))
					&& (pseudoClassName == null ? pseudoClassName == node_0_.pseudoClassName
							: pseudoClassName.equals(node_0_.pseudoClassName)) && (pseudoElementName == null ? pseudoElementName == node_0_.pseudoElementName
					: pseudoElementName.equals(node_0_.pseudoElementName)));
		}

		public String toString() {
			return ((elementName == null ? "" : elementName)
					+ (elementID == null ? "" : '#' + elementID)
					+ (className == null ? "" : '.' + className)
					+ (pseudoClassName == null ? "" : ':' + pseudoClassName) + (pseudoElementName == null ? ""
					: ':' + pseudoElementName));
		}
	}

	public Selector(Lexer lexer) throws CSSParseException {
		nodelist = new List();
		this.lexer = lexer;
		current = this.lexer.getType();
		parse();
	}

	public Selector(String string) throws CSSParseException {
		nodelist = new List();
		lexer = new Lexer(string);
		current = lexer.nextIgnoreSpaces();
		parse();
	}

	public int getSpecificity() {
		int i = Math.max(Integer.toString(cntId).length(), Math.max(Integer
				.toString(cntCl).length(), Integer.toString(cntEl).length()));
		return (cntId * (int) Math.pow(10.0, (double) (2 * i)) + cntCl
				* (int) Math.pow(10.0, (double) i) + cntEl);
	}

	private void parse() throws CSSParseException {
		int i = 60;
		Node node = null;
		for (;;) {
			switch (i) {
			case 60:
				if (node != null)
					nodelist.cons(node);
				node = new Node();
				switch (current) {
				case 20:
					i = 58;
					node.elementName = lexer.getStringValue();
					cntEl++;
					break;
				case 27:
					i = 27;
					break;
				case 7:
					i = 7;
					break;
				case 16:
					i = 16;
					break;
				default:
					throw new CSSParseException("Expected selector");
				}
				current = lexer.nextToken();
				break;
			case 27:
				if (current == 20) {
					i = 57;
					node.elementID = lexer.getStringValue();
					current = lexer.nextToken();
					cntId++;
				} else
					throw new CSSParseException("Invalid HASH ident");
				break;
			case 7:
				if (current == 20) {
					i = 56;
					node.className = lexer.getStringValue();
					current = lexer.nextToken();
					cntCl++;
				} else
					throw new CSSParseException("Invalid CLASS ident");
				break;
			case 16:
				if (current == 20 && isPseudoClass(lexer.getStringValue())) {
					i = 59;
					node.pseudoClassName = lexer.getStringValue();
					cntCl++;
				} else if (current == 20
						&& isPseudoElement(lexer.getStringValue())) {
					i = 54;
					node.pseudoElementName = lexer.getStringValue();
					cntEl++;
				} else
					throw new CSSParseException(
							"Invalid pseudo-class/pseudo-element ident");
				current = lexer.nextIgnoreSpaces();
				break;
			case 58:
				if (current == 27) {
					i = 27;
					current = lexer.nextToken();
					break;
				}
			/* fall through */
			case 57:
				if (current == 7) {
					i = 7;
					current = lexer.nextToken();
					break;
				}
			/* fall through */
			case 56:
				switch (current) {
				case 16:
					i = 16;
					current = lexer.nextToken();
					break;
				case 17:
				case 18:
					i = 59;
					current = lexer.nextToken();
					break;
				case 0:
				case 1:
				case 6:
					i = 53;
					break;
				default:
					throw new CSSParseException("Invalid selector name");
				}
				break;
			case 59:
				switch (current) {
				case 0:
				case 1:
				case 6:
					i = 53;
					break;
				default:
					i = 60;
					current = lexer.skipSpaces();
				}
				break;
			case 54:
				current = lexer.skipSpaces();
				switch (current) {
				case 0:
				case 1:
				case 6:
					i = 53;
					break;
				default:
					throw new CSSParseException("Expected end of selector");
				}
				break;
			}
			if (i == 53) {
				nodelist.cons(node);
				break;
			}
		}
	}

	public boolean equals(Selector selector_1_) {
		if (selector_1_.nodelist.length() != nodelist.length())
			return false;
		Enumeration enumeration = nodelist.elements();
		Enumeration enumeration_2_ = selector_1_.nodelist.elements();
		boolean bool = true;
		while (enumeration.hasMoreElements()) {
			if (!((Node) enumeration.nextElement())
					.equals((Node) enumeration_2_.nextElement()))
				return false;
		}
		return true;
	}

	public String toString() {
		return nodelist.reverse().toString();
	}

	private boolean isPseudoElement(String string) {
		return (string.equalsIgnoreCase("first-letter") || string
				.equalsIgnoreCase("first-line"));
	}

	private boolean isPseudoClass(String string) {
		return (string.equalsIgnoreCase("link")
				|| string.equalsIgnoreCase("visited") || string
				.equalsIgnoreCase("active"));
	}
}