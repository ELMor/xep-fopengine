/*
 * Lexer - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.svg.css;

import java.io.InputStreamReader;

import junit.framework.Assert;
import junit.framework.TestCase;

public class Lexer {
	private String buffer;

	private int type = -1;

	private int current;

	private int parse;

	private int start;

	public static final class Test extends TestCase {
		public void testLexer() {
			Lexer lexer = new Lexer("abc abc a abc-40 a&");
			Assert.assertTrue(lexer.nextToken() == 20);
			Assert.assertTrue(lexer.nextToken() == 17);
			Assert.assertTrue(lexer.nextToken() == 20);
			Assert.assertTrue(lexer.nextToken() == 17);
			Assert.assertTrue(lexer.nextToken() == 20);
			Assert.assertTrue(lexer.nextToken() == 17);
			Assert.assertTrue(lexer.nextToken() == -1);
			lexer = new Lexer("33 .2 33.2 33.% 33.2%pc 33a.2 33.2.a");
			Assert.assertTrue(lexer.nextToken() == 51);
			Assert.assertTrue(lexer.nextToken() == 17);
			Assert.assertTrue(lexer.nextToken() == 51);
			Assert.assertTrue(lexer.nextToken() == 17);
			Assert.assertTrue(lexer.nextToken() == 51);
			Assert.assertTrue(lexer.nextToken() == 17);
			Assert.assertTrue(lexer.nextToken() == 51);
			Assert.assertTrue(lexer.nextToken() == 17);
			Assert.assertTrue(lexer.nextToken() == 51);
			Assert.assertTrue(lexer.nextToken() == 17);
			Assert.assertTrue(lexer.nextToken() == -1);
			Assert.assertTrue(lexer.nextToken() == 17);
			Assert.assertTrue(lexer.nextToken() == -1);
			lexer = new Lexer("/* 12345abcd */");
			Assert.assertTrue(lexer.nextToken() == 18);
			lexer = new Lexer("/* 12345abcd ");
			Assert.assertTrue(lexer.nextToken() == -1);
			lexer = new Lexer("\"abcd\\'dcba\"");
			Assert.assertTrue(lexer.nextToken() == 19);
			lexer = new Lexer("\"abcd'dcba\"");
			Assert.assertTrue(lexer.nextToken() == -1);
			lexer = new Lexer("abcd");
			lexer.nextToken();
			Assert.assertTrue(lexer.getStringValue().equals("abcd"));
		}
	}

	public Lexer(String string) {
		buffer = string;
		parse = 0;
		start = 0;
		if (buffer.length() == 0)
			current = -1;
		else
			current = nextChar();
	}

	public int getType() {
		return type;
	}

	public int getStartPosition() {
		return start;
	}

	private int nextChar() {
		if (parse < buffer.length())
			return current = buffer.charAt(parse++);
		if (parse == buffer.length())
			parse++;
		return current = -1;
	}

	public String getStringValue() {
		return buffer.substring(start, parse - 1);
	}

	public int nextToken() {
		boolean bool = false;
		start = parse - 1;
		switch (current) {
		case -1:
			type = 0;
			break;
		case 123:
			nextChar();
			type = 1;
			break;
		case 125:
			nextChar();
			type = 2;
			break;
		case 61:
			nextChar();
			type = 3;
			break;
		case 43:
			nextChar();
			type = 4;
			break;
		case 45:
			nextChar();
			type = 5;
			break;
		case 44:
			nextChar();
			type = 6;
			break;
		case 59:
			nextChar();
			type = 8;
			break;
		case 40:
			nextChar();
			type = 14;
			break;
		case 41:
			nextChar();
			type = 15;
			break;
		case 58:
			nextChar();
			type = 16;
			break;
		case 34:
		case 39:
			type = string((char) current);
			break;
		case 9:
		case 10:
		case 12:
		case 13:
		case 32:
			do
				nextChar();
			while (isSpace((char) current));
			type = 17;
			break;
		case 47:
			nextChar();
			if (current != 42)
				type = 10;
			else {
				nextChar();
				start = parse - 1;
				for (;;) {
					if (current != -1 && current != 42)
						nextChar();
					else {
						do
							nextChar();
						while (current != -1 && current == 42);
						if (current == -1 || current == 47)
							break;
					}
				}
				if (current == -1)
					type = -1;
				else {
					nextChar();
					type = 18;
				}
			}
			break;
		case 35:
			nextChar();
			type = 27;
			break;
		case 64: {
			nextChar();
			int i;
			if ((i = isIdent(parse - 1)) > 0
					&& "import".equalsIgnoreCase(buffer.substring(parse - 1,
							(parse + i - 1)))) {
				parse += i - 1;
				type = 28;
			} else
				type = -1;
			nextChar();
			break;
		}
		case 33:
			nextChar();
			type = 23;
			break;
		case 48:
		case 49:
		case 50:
		case 51:
		case 52:
		case 53:
		case 54:
		case 55:
		case 56:
		case 57:
			nextChar();
			while (current >= 48 && current <= 57)
				nextChar();
			if (current == 46) {
				nextChar();
				if (current < 48 || current > 57) {
					nextChar();
					return type = -1;
				}
			}
			while (isNotnum(current))
				nextChar();
			if (current == 46) {
				nextChar();
				return type = -1;
			}
			type = 51;
			break;
		case 46:
			nextChar();
			if (current >= 48 && current <= 57) {
				while (isNotnum(current))
					nextChar();
				if (current == 46) {
					nextChar();
					return type = -1;
				}
				type = 51;
			} else
				type = 7;
			break;
		default: {
			int i;
			if ((i = isIdent(start)) > 0) {
				parse += i - 1;
				nextChar();
				if (current == 40) {
					nextChar();
					type = 52;
				} else
					type = 20;
			} else {
				nextChar();
				type = -1;
			}
		}
		}
		return type;
	}

	public int nextIgnoreSpaces() {
		boolean bool = true;
		while (bool) {
			switch (nextToken()) {
			case 17:
			case 18:
				break;
			default:
				bool = false;
			}
		}
		return type;
	}

	public int skipSpaces() {
		if (type == 17 || type == 18)
			return nextIgnoreSpaces();
		return type;
	}

	private int isIdent(int i) {
		if (i >= buffer.length())
			return 0;
		int i_0_ = i;
		boolean bool = false;
		int i_1_;
		if ((i_1_ = isNmstrt(i_0_)) > 0) {
			while ((i_1_ = isNmchar(++i_0_)) > 0)
				i_0_ += i_1_ - 1;
		}
		return i_0_ - i;
	}

	private int isNmstrt(int i) {
		if (i >= buffer.length())
			return 0;
		int i_2_ = i;
		boolean bool = false;
		char c = Character.toLowerCase(buffer.charAt(i_2_));
		if (c >= 'a' && c <= 'z' || c >= '\u00a1' && c <= '\u00ff')
			return ++i_2_ - i;
		int i_3_;
		if ((i_3_ = isUnicode(i_2_)) > 0)
			i_2_ += i_3_;
		return i_2_ - i;
	}

	private int isNmchar(int i) {
		if (i >= buffer.length())
			return 0;
		int i_4_ = i;
		boolean bool = false;
		char c = Character.toLowerCase(buffer.charAt(i_4_));
		if (c >= 'a' && c <= 'z' || c >= '0' && c <= '9' || c >= '\u00a1'
				&& c <= '\u00ff' || c == '-' || c == '_')
			return ++i_4_ - i;
		int i_5_;
		if ((i_5_ = isUnicode(i_4_)) > 0)
			i_4_ += i_5_;
		return i_4_ - i;
	}

	private int isUnicode(int i) {
		if (i >= buffer.length())
			return 0;
		int i_6_ = i;
		if (buffer.charAt(i_6_) == '\\') {
			for (/**/; (i_6_ < buffer.length() && i_6_ - i < 5 && ((buffer
					.charAt(i_6_ + 1) >= '0' && buffer.charAt(i_6_ + 1) <= '9') || (Character
					.toLowerCase(buffer.charAt(i_6_ + 1)) >= 'a' && (Character
					.toLowerCase(buffer.charAt(i_6_ + 1)) <= 'z')))); i_6_++) {
				/* empty */
			}
		}
		return i_6_ - i;
	}

	private boolean isNotnum(int i) {
		char c = Character.toLowerCase((char) i);
		return i >= 0
				&& (c >= 'a' && c <= 'z' || c >= '0' && c <= '9'
						|| c >= '\u00a1' && c <= '\u00ff' || c == '-'
						|| c == '^' || c == '%');
	}

	private int string(char c) {
		start = parse - 1;
		nextChar();
		for (;;) {
			switch (current) {
			case -1:
				return -1;
			case 92:
				nextChar();
			/* fall through */
			default:
				nextChar();
				if (c == current) {
					nextChar();
					return 19;
				}
			}
		}
	}

	private static boolean isSpace(char c) {
		return c == ' ' || c == '\t' || c == '\r' || c == '\n' || c == '\014';
	}

	public static void main(String[] strings) throws Exception {
		char[] cs = new char[8192];
		boolean bool = false;
		for (;;) {
			InputStreamReader inputstreamreader = new InputStreamReader(
					System.in);
			int i = inputstreamreader.read(cs);
			if (i < 0)
				System.exit(0);
			Lexer lexer = new Lexer(new String(cs, 0, i).trim());
			for (;;) {
				lexer.nextToken();
				if (lexer.getType() == 0)
					break;
				System.err.println("[" + lexer.getType() + "] \""
						+ lexer.getStringValue() + "\"");
			}
		}
	}
}