/*
 * RuleSet - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.svg.css;

import com.renderx.util.Array;
import com.renderx.util.ErrorHandler;

public class RuleSet extends Array {
	private Lexer lexer;

	private int current;

	private Style style;

	public RuleSet(Lexer lexer, ErrorHandler errorhandler)
			throws CSSParseException {
		this.lexer = lexer;
		current = this.lexer.getType();
		parse(errorhandler);
	}

	public Style getStyleDeclaration() {
		return style;
	}

	private void parse(ErrorHandler errorhandler) throws CSSParseException {
		parseSelectorList();
		if (current != 1)
			throw new CSSParseException("Expected symbol {");
		current = lexer.nextIgnoreSpaces();
		style = new Style(lexer, errorhandler);
		if ((current = lexer.getType()) != 2)
			throw new CSSParseException("Unexpected end of style declaration");
		current = lexer.nextIgnoreSpaces();
	}

	private void parseSelectorList() throws CSSParseException {
		this.put(this.length(), new Selector(lexer));
		while ((current = lexer.getType()) == 6) {
			current = lexer.nextIgnoreSpaces();
			this.put(this.length(), new Selector(lexer));
		}
	}
}