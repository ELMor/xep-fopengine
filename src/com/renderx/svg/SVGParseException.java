/*
 * SVGParseException - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.svg;

import org.xml.sax.SAXException;

public class SVGParseException extends SAXException {
	public SVGParseException() {
		super("");
	}

	public SVGParseException(String string) {
		super(string);
	}
}