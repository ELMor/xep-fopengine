/*
 * SVGElement - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.svg;

import com.renderx.util.Hashtable;

public class SVGElement {
	public static final String SVGURI = "http://www.w3.org/2000/svg";

	public static final String XLINKURI = "http://www.w3.org/1999/xlink";

	public static final String XMLURI = "http://www.w3.org/XML/1998/namespace";

	public boolean transparent = false;

	public boolean viewport = false;

	public boolean hasText = false;

	public String name = null;

	public int id = -1;

	public static final int INVALID_ELEMENT = -1;

	public static final int CIRCLE = 1;

	public static final int CLIP_PATH = 2;

	public static final int DEFS = 3;

	public static final int ELLIPSE = 4;

	public static final int G = 5;

	public static final int IMAGE = 6;

	public static final int LINE = 7;

	public static final int LINEAR_GRADIENT = 8;

	public static final int MARKER = 9;

	public static final int PATH = 10;

	public static final int POLYGON = 11;

	public static final int POLYLINE = 12;

	public static final int PATTERN = 13;

	public static final int RADIAL_GRADIENT = 14;

	public static final int RECT = 15;

	public static final int STOP = 16;

	public static final int SVG = 17;

	public static final int TEXT = 18;

	public static final int TEXT_PATH = 19;

	public static final int TSPAN = 20;

	public static final int A = 21;

	public static final int STYLE = 22;

	public static final int USE = 23;

	public static final int TREF = 24;

	public static final int SWITCH_ = 25;

	public static final int SYMBOL = 26;

	public static final int F_NONE = 0;

	public static final int F_TRANSPARENT = 2;

	public static final int F_HAS_TEXT = 4;

	public static final int F_ESTABLISHES_VIEWPORT = 8;

	public static final Hashtable nametab = new Hashtable();

	public static SVGElement $circle = null;

	public static SVGElement $clipPath = null;

	public static SVGElement $defs = null;

	public static SVGElement $ellipse = null;

	public static SVGElement $g = null;

	public static SVGElement $image = null;

	public static SVGElement $line = null;

	public static SVGElement $linearGradient = null;

	public static SVGElement $marker = null;

	public static SVGElement $path = null;

	public static SVGElement $pattern = null;

	public static SVGElement $polygon = null;

	public static SVGElement $polyline = null;

	public static SVGElement $radialGradient = null;

	public static SVGElement $rect = null;

	public static SVGElement $stop = null;

	public static SVGElement $svg = null;

	public static SVGElement $text = null;

	public static SVGElement $textPath = null;

	public static SVGElement $tspan = null;

	public static SVGElement $a = null;

	public static SVGElement $style = null;

	public static SVGElement $use = null;

	public static SVGElement $tref = null;

	public static SVGElement $switch_ = null;

	public static SVGElement $symbol = null;

	private static SVGElement createElement(String string, int i, int i_0_) {
		SVGElement svgelement = new SVGElement(string, i);
		svgelement.transparent = (i_0_ & 0x2) != 0;
		svgelement.hasText = (i_0_ & 0x4) != 0;
		svgelement.viewport = (i_0_ & 0x8) != 0;
		nametab.put(string, svgelement);
		return svgelement;
	}

	protected SVGElement(String string, int i) {
		name = string;
		id = i;
	}

	public static SVGElement get(String string, String string_1_) {
		if (!string.equals("http://www.w3.org/2000/svg"))
			return null;
		return (SVGElement) nametab.get(string_1_);
	}

	static {
		$circle = createElement("circle", 1, 0);
		$clipPath = createElement("clipPath", 2, 0);
		$defs = createElement("defs", 3, 0);
		$ellipse = createElement("ellipse", 4, 0);
		$g = createElement("g", 5, 0);
		$image = createElement("image", 6, 0);
		$line = createElement("line", 7, 0);
		$linearGradient = createElement("linearGradient", 8, 0);
		$marker = createElement("marker", 9, 0);
		$path = createElement("path", 10, 0);
		$pattern = createElement("pattern", 13, 0);
		$polygon = createElement("polygon", 11, 0);
		$polyline = createElement("polyline", 12, 0);
		$radialGradient = createElement("radialGradient", 14, 0);
		$rect = createElement("rect", 15, 0);
		$stop = createElement("stop", 16, 0);
		$svg = createElement("svg", 17, 0);
		$text = createElement("text", 18, 4);
		$textPath = createElement("textPath", 19, 0);
		$tspan = createElement("tspan", 20, 4);
		$a = createElement("a", 21, 2);
		$style = createElement("style", 22, 0);
		$use = createElement("use", 23, 0);
		$tref = createElement("tref", 24, 0);
		$switch_ = createElement("switch", 25, 0);
		$symbol = createElement("symbol", 26, 0);
	}
}