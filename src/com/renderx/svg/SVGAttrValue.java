/*
 * SVGAttrValue - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.svg;

import java.util.Enumeration;

import com.renderx.graphics.SpotColorList;
import com.renderx.graphics.vector.OpaqueColor;
import com.renderx.graphics.vector.PaintSpec;
import com.renderx.util.Hashtable;
import com.renderx.util.List;

public abstract class SVGAttrValue {
	public static final double PIXEL_SIZE = 0.6;

	public static class Paint extends Color {
		public Paint() {
			/* empty */
		}

		public Paint(String string) throws SVGParseException {
			parse(string);
		}

		public String toString() {
			return super.toString();
		}

		public String getTypeName() {
			return "Paint";
		}

		public void parse(String string) throws SVGParseException {
			if (string.trim().startsWith("url"))
				super.parse("none");
			super.parse(string);
		}

		public PaintSpec getPaintSpec(OpaqueColor opaquecolor)
				throws SVGParseException {
			return super.getColorSpec(opaquecolor);
		}
	}

	public static final class PathData extends SVGAttrValue {
		public static final Hashtable patterns = new Hashtable();

		public final List chunks;

		public static class Chunk {
			public char command;

			public double[] params = null;

			char[] patt = null;

			public Chunk(char c) throws SVGParseException {
				command = c;
				patt = ((char[]) SVGAttrValue.PathData.patterns
						.get(new Character(c)));
				if (patt == null)
					throw new SVGParseException(
							"Invalid command in path data: '" + c + "'");
				params = new double[patt.length];
			}

			public String toString() {
				String string = "" + command;
				if (params != null) {
					for (int i = 0; i < params.length; i++)
						string += " " + params[i];
				}
				return string;
			}
		}

		public PathData() {
			chunks = new List();
		}

		public PathData(String string) throws SVGParseException {
			chunks = new List();
			parse(string);
		}

		public void parse(String string) throws SVGParseException {
			PathLexer pathlexer = new PathLexer(string);
			char c = '\0';
			Token token = new Token();
			while (pathlexer.nextToken(token) != 1) {
				Object object = null;
				Chunk chunk;
				if (token.type == 13) {
					chunk = new Chunk(token.stringValue.charAt(0));
					switch (chunk.command) {
					case 'M':
						c = 'L';
						break;
					case 'm':
						c = 'l';
						break;
					default:
						c = chunk.command;
					}
					if (chunk.params.length != 0)
						pathlexer.nextToken(token);
				} else {
					if (c == 0)
						throw new SVGParseException(
								"Invalid path data: no command preceding numeric values");
					chunk = new Chunk(c);
					if (chunk.params.length == 0)
						throw new SVGParseException(
								"Invalid path data: no numeric values can follow '"
										+ c + "'");
				}
				for (int i = 0; i < chunk.params.length; i++) {
					if (i > 0)
						pathlexer.nextToken(token);
					switch (chunk.patt[i]) {
					case 'A':
						chunk.params[i] = new SVGAttrValue.Angle(token).angle;
						break;
					case 'L':
						chunk.params[i] = new SVGAttrValue.Length(token).len;
						break;
					case 'N':
						chunk.params[i] = new SVGAttrValue.Numeric(token).num;
						break;
					}
				}
				chunks.append(chunk);
			}
		}

		public final String toString() {
			String string = "";
			Enumeration enumeration = chunks.elements();
			while (enumeration.hasMoreElements()) {
				string += enumeration.nextElement();
				string += " ";
			}
			return string.trim();
		}

		public final String getTypeName() {
			return "PathData";
		}

		static {
			patterns.put(new Character('M'), new char[] { 'L', 'L' });
			patterns.put(new Character('m'), new char[] { 'L', 'L' });
			patterns.put(new Character('Z'), new char[0]);
			patterns.put(new Character('z'), new char[0]);
			patterns.put(new Character('L'), new char[] { 'L', 'L' });
			patterns.put(new Character('l'), new char[] { 'L', 'L' });
			patterns.put(new Character('V'), new char[] { 'L' });
			patterns.put(new Character('v'), new char[] { 'L' });
			patterns.put(new Character('H'), new char[] { 'L' });
			patterns.put(new Character('h'), new char[] { 'L' });
			patterns.put(new Character('C'), new char[] { 'L', 'L', 'L', 'L',
					'L', 'L' });
			patterns.put(new Character('c'), new char[] { 'L', 'L', 'L', 'L',
					'L', 'L' });
			patterns.put(new Character('S'), new char[] { 'L', 'L', 'L', 'L' });
			patterns.put(new Character('s'), new char[] { 'L', 'L', 'L', 'L' });
			patterns.put(new Character('Q'), new char[] { 'L', 'L', 'L', 'L' });
			patterns.put(new Character('q'), new char[] { 'L', 'L', 'L', 'L' });
			patterns.put(new Character('T'), new char[] { 'L', 'L' });
			patterns.put(new Character('t'), new char[] { 'L', 'L' });
			patterns.put(new Character('A'), new char[] { 'L', 'L', 'A', 'N',
					'N', 'L', 'L' });
			patterns.put(new Character('a'), new char[] { 'L', 'L', 'A', 'N',
					'N', 'L', 'L' });
		}
	}

	public static final class TransformList extends SVGAttrValue {
		public static final Hashtable patterns = new Hashtable();

		public final List transforms;

		public static class Transform {
			public String type = null;

			public double[] params = null;

			Transform() {
				/* empty */
			}

			public String toString() {
				if (params == null || type == null)
					return "<ERROR: UNINITIALIZED TRANSFORM>";
				String string = type + "(";
				for (int i = 0; i < params.length; i++) {
					if (i > 0)
						string += " ";
					string += "" + params[i];
				}
				string += ")";
				return string;
			}

			public boolean read(Lexer lexer) throws SVGParseException {
				Token token = new Token();
				do
					lexer.nextToken(token);
				while (token.type == 8);
				if (token.type == 1)
					return false;
				if (token.type != 13)
					throw new SVGParseException(
							"Unexpected token in transform-list (transform name expected): "
									+ token.getTypeName());
				String[] strings = (String[]) SVGAttrValue.TransformList.patterns
						.get(token.stringValue);
				if (strings == null)
					throw new SVGParseException(
							"Unrecognized string token in transform-list: "
									+ token.stringValue);
				type = token.stringValue;
				lexer.nextToken(token);
				if (token.type == 1)
					throw new SVGParseException(
							"Premature end of data in transform-list (opening parenthesis expected)");
				if (token.type != 2)
					throw new SVGParseException(
							"Unexpected token in transform-list (opening parenthesis expected): "
									+ token.getTypeName());
				params = new double[strings.length];
				boolean bool = false;
				for (int i = 0; i < strings.length; i++) {
					while (lexer.nextToken(token) == 8) {
						/* empty */
					}
					switch (token.type) {
					case 15:
						switch (strings[i].charAt(0)) {
						case 'A':
							params[i] = new SVGAttrValue.Angle(token).angle;
							break;
						case 'L':
							params[i] = new SVGAttrValue.Length(token).len;
							break;
						case 'N':
							params[i] = new SVGAttrValue.Numeric(token).num;
							break;
						}
						break;
					case 3:
						if (strings[i].charAt(1) != '?')
							throw new SVGParseException(
									"Too few arguments in transform-list of type "
											+ type);
						for (/**/; i < strings.length; i++)
							params[i] = (strings[i].charAt(0) == 'N' ? params[i - 1]
									: 0.0);
						break;
					case 1:
						throw new SVGParseException(
								"Premature end of data in transform-list (number or closing parenthesis expected)");
					default:
						throw new SVGParseException(
								"Unexpected token found in transform-list (closing parenthesis expected): "
										+ token.getTypeName());
					}
				}
				if (token.type != 3)
					lexer.nextToken(token);
				if (token.type == 1)
					throw new SVGParseException(
							"Premature end of data in transform-list (closing parenthesis expected)");
				if (token.type != 3)
					throw new SVGParseException(
							"Unexpected token in transform-list (closing parenthesis expected)");
				return true;
			}
		}

		public TransformList() {
			transforms = new List();
		}

		public TransformList(String string) throws SVGParseException {
			transforms = new List();
			parse(string);
		}

		public final String getTypeName() {
			return "TransformList";
		}

		public final String toString() {
			String string = "";
			Enumeration enumeration = transforms.elements();
			while (enumeration.hasMoreElements()) {
				string += enumeration.nextElement();
				string += " ";
			}
			return string.trim();
		}

		public void parse(String string) throws SVGParseException {
			Lexer lexer = new Lexer(string);
			for (;;) {
				Transform transform = new Transform();
				if (!transform.read(lexer))
					break;
				transforms.append(transform);
			}
		}

		static {
			patterns.put("matrix", new String[] { "N+", "N+", "N+", "N+", "L+",
					"L+" });
			patterns.put("rotate", new String[] { "A+", "L?", "L?" });
			patterns.put("translate", new String[] { "L+", "L?" });
			patterns.put("scale", new String[] { "N+", "N?" });
			patterns.put("skewX", new String[] { "A+" });
			patterns.put("skewY", new String[] { "A+" });
		}
	}

	public static class Color extends SVGAttrValue {
		double[] samples = null;

		String sampleStr = null;

		public static final int INVALID_COLOR = -2147483648;

		public static final int NONE_COLOR = 0;

		public static final int CURRENT_COLOR = 1;

		public static final int GRAYSCALE_COLOR = 2;

		public static final int RGB_COLOR = 3;

		public static final int CMYK_COLOR = 4;

		public static final int SPOT_COLOR = 5;

		public static final int REGISTRATION_COLOR = 6;

		public int type = -2147483648;

		public static final Hashtable predefinedColors = new Hashtable();

		public Color() {
			/* empty */
		}

		public Color(String string) throws SVGParseException {
			parse(string);
		}

		public String getTypeName() {
			return "Color";
		}

		public String toString() {
			switch (type) {
			case 0:
				return "none";
			case 1:
				return "current";
			case 2:
				return "#GRAY(" + samples[0] + ")";
			case 3:
				return ("#RGB(" + samples[0] + "," + samples[1] + ","
						+ samples[2] + ")");
			case 4:
				return ("#CMYK(" + samples[0] + "," + samples[1] + ","
						+ samples[2] + "," + samples[3] + ")");
			case 5:
				return ("#SpotColor(" + sampleStr + "," + samples[0] + ","
						+ samples[1] + "," + samples[2] + "," + samples[3]
						+ "," + samples[4] + ")");
			case 6:
				return "#Registration(" + samples[0] + ")";
			default:
				return "<Internal error: wrong color type>";
			}
		}

		public void parse(String string) throws SVGParseException {
			parse(new Lexer(string), true);
		}

		private void parse(Lexer lexer, boolean bool) throws SVGParseException {
			Token token = new Token();
			lexer.nextToken(token);
			if (token.type == 1) {
				type = 0;
				samples = null;
			} else if (token.type == 12) {
				int i = (int) Math.round(token.numValue);
				double d = (double) (i >> 16 & 0xff) / 255.0;
				double d_0_ = (double) (i >> 8 & 0xff) / 255.0;
				double d_1_ = (double) (i & 0xff) / 255.0;
				if (d == d_0_ && d_0_ == d_1_) {
					type = 2;
					samples = new double[] { d };
				} else {
					type = 3;
					samples = new double[] { d, d_0_, d_1_ };
				}
			} else if (token.type == 13) {
				if ("none".equals(token.stringValue)) {
					type = 0;
					samples = null;
				} else if ("currentColor".equals(token.stringValue)) {
					type = 1;
					samples = null;
				} else if ("url".equals(token.stringValue) && bool) {
					if (lexer.nextToken(token) != 2)
						throw new SVGParseException(
								"Unexpected token "
										+ token.getTypeName()
										+ " in url() function: left parenthesis expected");
					String string = "";
					while (token.type != 3) {
						lexer.nextToken(token);
						if (token.type == 1)
							throw new SVGParseException(
									"No closing parenthesis in url() function");
						string += " " + token.stringValue;
					}
					string = string.trim();
					parse(lexer, false);
				} else if ("rgb".equals(token.stringValue)) {
					if (lexer.nextToken(token) != 2)
						throw new SVGParseException(
								"Unexpected token "
										+ token.getTypeName()
										+ " in rgb() color specifier: left parenthesis expected");
					type = 3;
					samples = new double[3];
					samples[0] = getNextRGBComponent(lexer);
					if (lexer.nextToken(token) != 8)
						throw new SVGParseException(
								"Unexpected token "
										+ token.getTypeName()
										+ " in rgb() color specification : comma expected");
					samples[1] = getNextRGBComponent(lexer);
					if (lexer.nextToken(token) != 8)
						throw new SVGParseException(
								"Unexpected token "
										+ token.getTypeName()
										+ " in rgb() color specification : comma expected");
					samples[2] = getNextRGBComponent(lexer);
					if (lexer.nextToken(token) != 3)
						throw new SVGParseException(
								"Unexpected token "
										+ token.getTypeName()
										+ " in rgb() color specification : right parenthesis expected");
				} else {
					samples = (double[]) predefinedColors
							.get(token.stringValue);
					if (samples == null)
						throw new SVGParseException("Invalid color name ("
								+ token.stringValue
								+ ") or unsupported color specification");
					type = samples.length == 1 ? 2 : 3;
				}
			} else
				throw new SVGParseException("Data type mismatch: found "
						+ token.getTypeName() + " where color is expected");
			parseICC(lexer);
		}

		private void parseICC(Lexer lexer) throws SVGParseException {
			Token token = new Token();
			if (lexer.nextToken(token) == 13
					&& "icc-color".equals(token.stringValue)
					&& lexer.nextToken(token) == 2
					&& lexer.nextToken(token) == 13) {
				if ("#Grayscale".equals(token.stringValue)) {
					if (lexer.nextToken(token) == 8
							&& lexer.nextToken(token) == 15) {
						double d = getIntensity(token);
						if (lexer.nextToken(token) == 3) {
							type = 2;
							samples = new double[] { d };
						}
					}
				} else if ("#Registration".equals(token.stringValue)) {
					if (lexer.nextToken(token) == 8
							&& lexer.nextToken(token) == 15) {
						double d = getIntensity(token);
						if (lexer.nextToken(token) == 3) {
							type = 6;
							samples = new double[] { d };
						}
					}
				} else if ("#CMYK".equals(token.stringValue)) {
					if (lexer.nextToken(token) == 8
							&& lexer.nextToken(token) == 15) {
						double d = getIntensity(token);
						if (lexer.nextToken(token) == 8
								&& lexer.nextToken(token) == 15) {
							double d_2_ = getIntensity(token);
							if (lexer.nextToken(token) == 8
									&& lexer.nextToken(token) == 15) {
								double d_3_ = getIntensity(token);
								if (lexer.nextToken(token) == 8
										&& lexer.nextToken(token) == 15) {
									double d_4_ = getIntensity(token);
									if (lexer.nextToken(token) == 3) {
										type = 4;
										samples = new double[] { d, d_2_, d_3_,
												d_4_ };
									}
								}
							}
						}
					}
				} else if ("#SpotColor".equals(token.stringValue)) {
					if (lexer.nextToken(token) == 8) {
						int i = lexer.nextToken(token);
						if (i == 13 || i == 14) {
							sampleStr = token.stringValue;
							if (lexer.nextToken(token) == 8
									&& lexer.nextToken(token) == 15) {
								double d = getIntensity(token);
								if (lexer.nextToken(token) == 3) {
									type = 5;
									double[] ds = SpotColorList.dflt
											.getCMYK(sampleStr);
									if (ds == null)
										samples = new double[] { d, 0.0 };
									else
										samples = new double[] { d, ds[0],
												ds[1], ds[2], ds[3] };
								} else if (lexer.nextToken(token) == 13) {
									if ("#CMYK".equals(token.stringValue)) {
										if (lexer.nextToken(token) == 8
												&& lexer.nextToken(token) == 15) {
											double d_5_ = getIntensity(token);
											if (lexer.nextToken(token) == 8
													&& (lexer.nextToken(token) == 15)) {
												double d_6_ = getIntensity(token);
												if (lexer.nextToken(token) == 8
														&& (lexer
																.nextToken(token) == 15)) {
													double d_7_ = getIntensity(token);
													if ((lexer.nextToken(token) == 8)
															&& (lexer
																	.nextToken(token)) == 15) {
														double d_8_ = (getIntensity(token));
														if ((lexer
																.nextToken(token)) == 3) {
															type = 5;
															samples = (new double[] {
																	d, d_5_,
																	d_6_, d_7_,
																	d_8_ });
														}
													}
												}
											}
										}
									} else if ("#Grayscale"
											.equals(token.stringValue)) {
										if (lexer.nextToken(token) == 8
												&& lexer.nextToken(token) == 15) {
											double d_9_ = getIntensity(token);
											if (lexer.nextToken(token) == 3) {
												type = 5;
												samples = new double[] { d,
														d_9_ };
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		private static final double getIntensity(Token token) {
			double d = token.numValue;
			if (token.stringValue != null && token.stringValue.equals("%"))
				d /= 100.0;
			if (d < 0.0)
				return 0.0;
			if (d > 1.0)
				return 1.0;
			return d;
		}

		private double getNextRGBComponent(Lexer lexer)
				throws SVGParseException {
			Token token = new Token();
			if (lexer.nextToken(token) != 15)
				throw new SVGParseException(
						"Unexpected token "
								+ token.getTypeName()
								+ " in argument to rgb()': expected number or percentage");
			if (token.stringValue == null)
				return token.numValue / 255.0;
			if (token.stringValue.equals("%"))
				return token.numValue / 100.0;
			throw new SVGParseException("Unexpected measurement unit "
					+ token.stringValue
					+ " in argument to rgb()': expected number or percentage");
		}

		public OpaqueColor getColorSpec(OpaqueColor opaquecolor)
				throws SVGParseException {
			switch (type) {
			case 0:
				return null;
			case 1:
				return opaquecolor;
			case 2:
				return new OpaqueColor.Grayscale(samples[0]);
			case 3:
				return new OpaqueColor.RGB(samples[0], samples[1], samples[2]);
			case 4:
				return new OpaqueColor.CMYK(samples[0], samples[1], samples[2],
						samples[3]);
			case 6:
				return new OpaqueColor.Registration(samples[0]);
			case 5:
				if (samples.length == 2)
					return new OpaqueColor.SpotColor(samples[0], sampleStr,
							samples[1]);
				if (samples.length == 5)
					return new OpaqueColor.SpotColor(samples[0], sampleStr,
							samples[1], samples[2], samples[3], samples[4]);
			/* fall through */
			default:
				throw new SVGParseException(
						"Internal error: invalid color type in SVGAttrValue.Color");
			}
		}

		static {
			predefinedColors.put("aliceblue", new double[] {
					0.9411764705882353, 0.9725490196078431, 1.0 });
			predefinedColors.put("antiquewhite",
					new double[] { 0.9803921568627451, 0.9215686274509803,
							0.8431372549019608 });
			predefinedColors.put("aqua", new double[] { 0.0, 1.0, 1.0 });
			predefinedColors.put("aquamarine", new double[] {
					0.4980392156862745, 1.0, 0.8313725490196079 });
			predefinedColors.put("azure", new double[] { 0.9411764705882353,
					1.0, 1.0 });
			predefinedColors.put("beige", new double[] { 0.9607843137254902,
					0.9607843137254902, 0.8627450980392157 });
			predefinedColors.put("bisque", new double[] { 1.0,
					0.8941176470588236, 0.7686274509803922 });
			predefinedColors.put("black", new double[] { 0.0 });
			predefinedColors.put("blanchedalmond", new double[] { 1.0,
					0.9215686274509803, 0.803921568627451 });
			predefinedColors.put("blue", new double[] { 0.0, 0.0, 1.0 });
			predefinedColors.put("blueviolet",
					new double[] { 0.5411764705882353, 0.16862745098039217,
							0.8862745098039215 });
			predefinedColors.put("brown", new double[] { 0.6470588235294118,
					0.16470588235294117, 0.16470588235294117 });
			predefinedColors.put("burlywood",
					new double[] { 0.8705882352941177, 0.7215686274509804,
							0.5294117647058824 });
			predefinedColors.put("cadetblue",
					new double[] { 0.37254901960784315, 0.6196078431372549,
							0.6274509803921569 });
			predefinedColors.put("chartreuse", new double[] {
					0.4980392156862745, 1.0, 0.0 });
			predefinedColors.put("chocolate",
					new double[] { 0.8235294117647058, 0.4117647058823529,
							0.11764705882352941 });
			predefinedColors.put("coral", new double[] { 1.0,
					0.4980392156862745, 0.3137254901960784 });
			predefinedColors.put("cornflowerblue",
					new double[] { 0.39215686274509803, 0.5843137254901961,
							0.9294117647058824 });
			predefinedColors.put("cornsilk", new double[] { 1.0,
					0.9725490196078431, 0.8627450980392157 });
			predefinedColors.put("crimson", new double[] { 0.8627450980392157,
					0.0784313725490196, 0.23529411764705882 });
			predefinedColors.put("cyan", new double[] { 0.0, 1.0, 1.0 });
			predefinedColors.put("darkblue", new double[] { 0.0, 0.0,
					0.5450980392156862 });
			predefinedColors.put("darkcyan", new double[] { 0.0,
					0.5450980392156862, 0.5450980392156862 });
			predefinedColors.put("darkgoldenrod", new double[] {
					0.7215686274509804, 0.5254901960784314,
					0.043137254901960784 });
			predefinedColors.put("darkgray",
					new double[] { 0.6627450980392157 });
			predefinedColors.put("darkgreen", new double[] { 0.0,
					0.39215686274509803, 0.0 });
			predefinedColors.put("darkgrey",
					new double[] { 0.6627450980392157 });
			predefinedColors.put("darkkhaki",
					new double[] { 0.7411764705882353, 0.7176470588235294,
							0.4196078431372549 });
			predefinedColors.put("darkmagenta", new double[] {
					0.5450980392156862, 0.0, 0.5450980392156862 });
			predefinedColors.put("darkolivegreen",
					new double[] { 0.3333333333333333, 0.4196078431372549,
							0.1843137254901961 });
			predefinedColors.put("darkorange", new double[] { 1.0,
					0.5490196078431373, 0.0 });
			predefinedColors.put("darkorchid", new double[] { 0.6,
					0.19607843137254902, 0.8 });
			predefinedColors.put("darkred", new double[] { 0.5450980392156862,
					0.0, 0.0 });
			predefinedColors.put("darksalmon",
					new double[] { 0.9137254901960784, 0.5882352941176471,
							0.47843137254901963 });
			predefinedColors.put("darkseagreen",
					new double[] { 0.5607843137254902, 0.7372549019607844,
							0.5607843137254902 });
			predefinedColors.put("darkslateblue",
					new double[] { 0.2823529411764706, 0.23921568627450981,
							0.5450980392156862 });
			predefinedColors.put("darkslategray", new double[] {
					0.1843137254901961, 0.30980392156862746,
					0.30980392156862746 });
			predefinedColors.put("darkslategrey", new double[] {
					0.1843137254901961, 0.30980392156862746,
					0.30980392156862746 });
			predefinedColors.put("darkturquoise", new double[] { 0.0,
					0.807843137254902, 0.8196078431372549 });
			predefinedColors.put("darkviolet", new double[] {
					0.5803921568627451, 0.0, 0.8274509803921568 });
			predefinedColors.put("deeppink", new double[] { 1.0,
					0.0784313725490196, 0.5764705882352941 });
			predefinedColors.put("deepskyblue", new double[] { 0.0,
					0.7490196078431373, 1.0 });
			predefinedColors
					.put("dimgray", new double[] { 0.4117647058823529 });
			predefinedColors
					.put("dimgrey", new double[] { 0.4117647058823529 });
			predefinedColors.put("dodgerblue", new double[] {
					0.11764705882352941, 0.5647058823529412, 1.0 });
			predefinedColors.put("firebrick", new double[] {
					0.6980392156862745, 0.13333333333333333,
					0.13333333333333333 });
			predefinedColors.put("floralwhite", new double[] { 1.0,
					0.9803921568627451, 0.9411764705882353 });
			predefinedColors.put("forestgreen", new double[] {
					0.13333333333333333, 0.5450980392156862,
					0.13333333333333333 });
			predefinedColors.put("fuchsia", new double[] { 1.0, 0.0, 1.0 });
			predefinedColors.put("gainsboro",
					new double[] { 0.8627450980392157 });
			predefinedColors.put("ghostwhite", new double[] {
					0.9725490196078431, 0.9725490196078431, 1.0 });
			predefinedColors.put("gold", new double[] { 1.0,
					0.8431372549019608, 0.0 });
			predefinedColors.put("goldenrod",
					new double[] { 0.8549019607843137, 0.6470588235294118,
							0.12549019607843137 });
			predefinedColors.put("gray", new double[] { 0.5019607843137255 });
			predefinedColors.put("grey", new double[] { 0.5019607843137255 });
			predefinedColors.put("green", new double[] { 0.0,
					0.5019607843137255, 0.0 });
			predefinedColors.put("greenyellow", new double[] {
					0.6784313725490196, 1.0, 0.1843137254901961 });
			predefinedColors.put("honeydew", new double[] { 0.9411764705882353,
					1.0, 0.9411764705882353 });
			predefinedColors.put("hotpink", new double[] { 1.0,
					0.4117647058823529, 0.7058823529411765 });
			predefinedColors.put("indianred", new double[] { 0.803921568627451,
					0.3607843137254902, 0.3607843137254902 });
			predefinedColors.put("indigo", new double[] { 0.29411764705882354,
					0.0, 0.5098039215686274 });
			predefinedColors.put("ivory", new double[] { 1.0, 1.0,
					0.9411764705882353 });
			predefinedColors.put("khaki", new double[] { 0.9411764705882353,
					0.9019607843137255, 0.5490196078431373 });
			predefinedColors.put("lavender", new double[] { 0.9019607843137255,
					0.9019607843137255, 0.9803921568627451 });
			predefinedColors.put("lavenderblush", new double[] { 1.0,
					0.9411764705882353, 0.9607843137254902 });
			predefinedColors.put("lawngreen", new double[] {
					0.48627450980392156, 0.9882352941176471, 0.0 });
			predefinedColors.put("lemonchiffon", new double[] { 1.0,
					0.9803921568627451, 0.803921568627451 });
			predefinedColors.put("lightblue",
					new double[] { 0.6784313725490196, 0.8470588235294118,
							0.9019607843137255 });
			predefinedColors.put("lightcoral",
					new double[] { 0.9411764705882353, 0.5019607843137255,
							0.5019607843137255 });
			predefinedColors.put("lightcyan", new double[] {
					0.8784313725490196, 1.0, 1.0 });
			predefinedColors.put("lightgoldenrodyellow",
					new double[] { 0.9803921568627451, 0.9803921568627451,
							0.8235294117647058 });
			predefinedColors.put("lightgray",
					new double[] { 0.8274509803921568 });
			predefinedColors.put("lightgreen",
					new double[] { 0.5647058823529412, 0.9333333333333333,
							0.5647058823529412 });
			predefinedColors.put("lightgrey",
					new double[] { 0.8274509803921568 });
			predefinedColors.put("lightpink", new double[] { 1.0,
					0.7137254901960784, 0.7568627450980392 });
			predefinedColors.put("lightsalmon", new double[] { 1.0,
					0.6274509803921569, 0.47843137254901963 });
			predefinedColors.put("lightseagreen",
					new double[] { 0.12549019607843137, 0.6980392156862745,
							0.6666666666666666 });
			predefinedColors
					.put("lightskyblue", new double[] { 0.5294117647058824,
							0.807843137254902, 0.9803921568627451 });
			predefinedColors.put("lightslategray", new double[] {
					0.4666666666666667, 0.5333333333333333, 0.6 });
			predefinedColors.put("lightslategrey", new double[] {
					0.4666666666666667, 0.5333333333333333, 0.6 });
			predefinedColors.put("lightsteelblue",
					new double[] { 0.6901960784313725, 0.7686274509803922,
							0.8705882352941177 });
			predefinedColors.put("lightyellow", new double[] { 1.0, 1.0,
					0.8784313725490196 });
			predefinedColors.put("lime", new double[] { 0.0, 1.0, 0.0 });
			predefinedColors.put("limegreen",
					new double[] { 0.19607843137254902, 0.803921568627451,
							0.19607843137254902 });
			predefinedColors.put("linen", new double[] { 0.9803921568627451,
					0.9411764705882353, 0.9019607843137255 });
			predefinedColors.put("magenta", new double[] { 1.0, 0.0, 1.0 });
			predefinedColors.put("maroon", new double[] { 0.5019607843137255,
					0.0, 0.0 });
			predefinedColors.put("mediumaquamarine", new double[] { 0.4,
					0.803921568627451, 0.6666666666666666 });
			predefinedColors.put("mediumblue", new double[] { 0.0, 0.0,
					0.803921568627451 });
			predefinedColors.put("mediumorchid",
					new double[] { 0.7294117647058823, 0.3333333333333333,
							0.8274509803921568 });
			predefinedColors.put("mediumpurple",
					new double[] { 0.5764705882352941, 0.4392156862745098,
							0.8588235294117647 });
			predefinedColors.put("mediumseagreen", new double[] {
					0.23529411764705882, 0.7019607843137254,
					0.44313725490196076 });
			predefinedColors.put("mediumslateblue",
					new double[] { 0.4823529411764706, 0.40784313725490196,
							0.9333333333333333 });
			predefinedColors.put("mediumspringgreen", new double[] { 0.0,
					0.9803921568627451, 0.6039215686274509 });
			predefinedColors.put("mediumturquoise", new double[] {
					0.2823529411764706, 0.8196078431372549, 0.8 });
			predefinedColors.put("mediumvioletred",
					new double[] { 0.7803921568627451, 0.08235294117647059,
							0.5215686274509804 });
			predefinedColors.put("midnightblue", new double[] {
					0.09803921568627451, 0.09803921568627451,
					0.4392156862745098 });
			predefinedColors.put("mintcream", new double[] {
					0.9607843137254902, 1.0, 0.9803921568627451 });
			predefinedColors.put("mistyrose", new double[] { 1.0,
					0.8941176470588236, 0.8823529411764706 });
			predefinedColors.put("moccasin", new double[] { 1.0,
					0.8941176470588236, 0.7098039215686275 });
			predefinedColors.put("navajowhite", new double[] { 1.0,
					0.8705882352941177, 0.6784313725490196 });
			predefinedColors.put("navy", new double[] { 0.0, 0.0,
					0.5019607843137255 });
			predefinedColors.put("oldlace", new double[] { 0.9921568627450981,
					0.9607843137254902, 0.9019607843137255 });
			predefinedColors.put("olive", new double[] { 0.5019607843137255,
					0.5019607843137255, 0.0 });
			predefinedColors.put("olivedrab",
					new double[] { 0.4196078431372549, 0.5568627450980392,
							0.13725490196078433 });
			predefinedColors.put("orange", new double[] { 1.0,
					0.6470588235294118, 0.0 });
			predefinedColors.put("orangered", new double[] { 1.0,
					0.27058823529411763, 0.0 });
			predefinedColors.put("orchid", new double[] { 0.8549019607843137,
					0.4392156862745098, 0.8392156862745098 });
			predefinedColors.put("palegoldenrod",
					new double[] { 0.9333333333333333, 0.9098039215686274,
							0.6666666666666666 });
			predefinedColors.put("palegreen", new double[] { 0.596078431372549,
					0.984313725490196, 0.596078431372549 });
			predefinedColors.put("paleturquoise",
					new double[] { 0.6862745098039216, 0.9333333333333333,
							0.9333333333333333 });
			predefinedColors.put("palevioletred",
					new double[] { 0.8588235294117647, 0.4392156862745098,
							0.5764705882352941 });
			predefinedColors.put("papayawhip", new double[] { 1.0,
					0.9372549019607843, 0.8352941176470589 });
			predefinedColors.put("peachpuff", new double[] { 1.0,
					0.8549019607843137, 0.7254901960784313 });
			predefinedColors.put("peru", new double[] { 0.803921568627451,
					0.5215686274509804, 0.24705882352941178 });
			predefinedColors.put("pink", new double[] { 1.0,
					0.7529411764705882, 0.796078431372549 });
			predefinedColors.put("plum", new double[] { 0.8666666666666667,
					0.6274509803921569, 0.8666666666666667 });
			predefinedColors.put("powderblue",
					new double[] { 0.6901960784313725, 0.8784313725490196,
							0.9019607843137255 });
			predefinedColors.put("purple", new double[] { 0.5019607843137255,
					0.0, 0.5019607843137255 });
			predefinedColors.put("red", new double[] { 1.0, 0.0, 0.0 });
			predefinedColors.put("rosybrown",
					new double[] { 0.7372549019607844, 0.5607843137254902,
							0.5607843137254902 });
			predefinedColors.put("royalblue",
					new double[] { 0.2549019607843137, 0.4117647058823529,
							0.8823529411764706 });
			predefinedColors.put("saddlebrown", new double[] {
					0.5450980392156862, 0.27058823529411763,
					0.07450980392156863 });
			predefinedColors.put("salmon", new double[] { 0.9803921568627451,
					0.5019607843137255, 0.4470588235294118 });
			predefinedColors.put("sandybrown",
					new double[] { 0.9568627450980393, 0.6431372549019608,
							0.3764705882352941 });
			predefinedColors.put("seagreen", new double[] { 0.1803921568627451,
					0.5450980392156862, 0.3411764705882353 });
			predefinedColors.put("seashell", new double[] { 1.0,
					0.9607843137254902, 0.9333333333333333 });
			predefinedColors.put("sienna", new double[] { 0.6274509803921569,
					0.3215686274509804, 0.17647058823529413 });
			predefinedColors.put("silver", new double[] { 0.7529411764705882 });
			predefinedColors.put("skyblue", new double[] { 0.5294117647058824,
					0.807843137254902, 0.9215686274509803 });
			predefinedColors.put("slateblue",
					new double[] { 0.41568627450980394, 0.35294117647058826,
							0.803921568627451 });
			predefinedColors.put("slategray",
					new double[] { 0.4392156862745098, 0.5019607843137255,
							0.5647058823529412 });
			predefinedColors.put("slategrey",
					new double[] { 0.4392156862745098, 0.5019607843137255,
							0.5647058823529412 });
			predefinedColors.put("snow", new double[] { 1.0,
					0.9803921568627451, 0.9803921568627451 });
			predefinedColors.put("springgreen", new double[] { 0.0, 1.0,
					0.4980392156862745 });
			predefinedColors.put("steelblue",
					new double[] { 0.27450980392156865, 0.5098039215686274,
							0.7058823529411765 });
			predefinedColors.put("tan", new double[] { 0.8235294117647058,
					0.7058823529411765, 0.5490196078431373 });
			predefinedColors.put("teal", new double[] { 0.0,
					0.5019607843137255, 0.5019607843137255 });
			predefinedColors.put("thistle", new double[] { 0.8470588235294118,
					0.7490196078431373, 0.8470588235294118 });
			predefinedColors.put("tomato", new double[] { 1.0,
					0.38823529411764707, 0.2784313725490196 });
			predefinedColors.put("turquoise",
					new double[] { 0.25098039215686274, 0.8784313725490196,
							0.8156862745098039 });
			predefinedColors.put("violet", new double[] { 0.9333333333333333,
					0.5098039215686274, 0.9333333333333333 });
			predefinedColors.put("wheat", new double[] { 0.9607843137254902,
					0.8705882352941177, 0.7019607843137254 });
			predefinedColors.put("white", new double[] { 1.0 });
			predefinedColors.put("whitesmoke",
					new double[] { 0.9607843137254902 });
			predefinedColors.put("yellow", new double[] { 1.0, 1.0, 0.0 });
			predefinedColors.put("yellowgreen",
					new double[] { 0.6039215686274509, 0.803921568627451,
							0.19607843137254902 });
		}
	}

	public static class Numeric extends SVGAttrValue {
		public double num = 0.0;

		public Numeric() {
			/* empty */
		}

		public Numeric(String string) throws SVGParseException {
			parse(string);
		}

		Numeric(Token token) throws SVGParseException {
			this();
			create(token);
		}

		public String toString() {
			return String.valueOf(num);
		}

		public final String getTypeName() {
			return "Number";
		}

		void create(Token token) throws SVGParseException {
			if (token.type == 15) {
				num = token.numValue;
				if (token.stringValue != null)
					throw new SVGParseException("Unexpected unit: "
							+ token.stringValue);
			} else
				throw new SVGParseException("Data type mismatch: found "
						+ token.getTypeName() + " where number is expected");
		}

		public void parse(String string) throws SVGParseException {
			create(Lexer.getSingleToken(string));
		}
	}

	public static class Angle extends SVGAttrValue {
		public double angle = 0.0;

		public Angle() {
			/* empty */
		}

		public Angle(String string) throws SVGParseException {
			parse(string);
		}

		Angle(Token token) throws SVGParseException {
			this();
			create(token);
		}

		public String toString() {
			return String.valueOf(this.angle) + "deg";
		}

		public final String getTypeName() {
			return "Angle";
		}

		void create(Token token) throws SVGParseException {
			if (token.type == 15) {
				if (token.stringValue == null
						|| token.stringValue.equals("deg"))
					this.angle = token.numValue * 3.141592653589793 / 180.0;
				else if (token.stringValue.equals("grad"))
					this.angle = token.numValue * 3.141592653589793 / 200.0;
				else if (token.stringValue.equals("rad"))
					this.angle = token.numValue;
				else
					throw new SVGParseException("Invalid angle unit: "
							+ token.stringValue);
			} else
				throw new SVGParseException("Data type mismatch: found "
						+ token.getTypeName() + " where angle is expected");
		}

		public void parse(String string) throws SVGParseException {
			create(Lexer.getSingleToken(string));
		}
	}

	public static class Length extends SVGAttrValue {
		public static final int ABSOLUTE = 0;

		public static final int RELATIVE_EM = 1;

		public static final int RELATIVE_EX = 2;

		public static final int RELATIVE_PERCENTS = 3;

		public double len = 0.0;

		public int base = 0;

		public Length() {
			/* empty */
		}

		public Length(String string) throws SVGParseException {
			parse(string);
		}

		Length(Token token) throws SVGParseException {
			this();
			create(token);
		}

		public String toString() {
			switch (base) {
			case 0:
				return String.valueOf(len) + "pt";
			case 1:
				return String.valueOf(len) + "em";
			case 2:
				return String.valueOf(len) + "ex";
			case 3:
				return String.valueOf(len * 100.0) + "%";
			default:
				return "<ERROR! INCORRECT BASE FOR LENGTH>";
			}
		}

		public final String getTypeName() {
			return "Length";
		}

		void create(Token token) throws SVGParseException {
			if (token.type == 15) {
				len = token.numValue;
				if (token.stringValue == null || token.stringValue.equals("px"))
					len *= 0.6;
				else if (token.stringValue.equals("in"))
					len *= 72.0;
				else if (token.stringValue.equals("pc"))
					len *= 12.0;
				else if (token.stringValue.equals("mm"))
					len *= 2.834645669291339;
				else if (token.stringValue.equals("cm"))
					len *= 28.346456692913385;
				else if (token.stringValue.equals("em"))
					base = 1;
				else if (token.stringValue.equals("ex"))
					base = 2;
				else if (token.stringValue.equals("%")) {
					base = 3;
					len /= 100.0;
				} else if (!token.stringValue.equals("pt"))
					throw new SVGParseException("Invalid length unit: "
							+ token.stringValue);
			} else
				throw new SVGParseException("Data type mismatch: found "
						+ token.getTypeName() + " where length is expected");
		}

		public void parse(String string) throws SVGParseException {
			create(Lexer.getSingleToken(string));
		}
	}

	public static class Enum extends SVGAttrValue {
		public String word = null;

		public Enum() {
			/* empty */
		}

		public Enum(String string) throws SVGParseException {
			parse(string);
		}

		public String toString() {
			return word;
		}

		public String getTypeName() {
			return "Enum";
		}

		public void parse(String string) throws SVGParseException {
			Token token = Lexer.getSingleToken(string);
			if (token.type != 13)
				throw new SVGParseException("Data type mismatch: found "
						+ token.getTypeName() + " where a keyword is expected");
			word = token.stringValue;
		}
	}

	public static class Literal extends SVGAttrValue {
		public String word = null;

		public Literal() {
			/* empty */
		}

		public Literal(String string) throws SVGParseException {
			parse(string);
		}

		public String toString() {
			return word;
		}

		public String getTypeName() {
			return "Literal";
		}

		public void parse(String string) throws SVGParseException {
			word = string;
		}
	}

	public abstract String toString();

	public abstract String getTypeName();

	public abstract void parse(String string) throws SVGParseException;
}