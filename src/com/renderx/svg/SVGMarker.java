/*
 * SVGMarker - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.svg;

import com.renderx.graphics.vector.GraphicGroup;

public class SVGMarker {
	public final String id;

	public final GraphicGroup body;

	public final double width;

	public final double height;

	public final boolean useStrokeWidth;

	public final boolean autoRotate;

	public final double orientation;

	public SVGMarker(String string, GraphicGroup graphicgroup, double d,
			double d_0_, String string_1_, String string_2_) {
		id = string;
		body = graphicgroup;
		width = d;
		height = d_0_;
		useStrokeWidth = !"userSpaceOnUse".equals(string_1_);
		autoRotate = "auto".equals(string_2_);
		double d_3_ = 0.0;
		if (!autoRotate) {
			try {
				d_3_ = new SVGAttrValue.Angle(string_2_).angle;
			} catch (SVGParseException svgparseexception) {
				/* empty */
			}
		}
		orientation = d_3_;
	}
}