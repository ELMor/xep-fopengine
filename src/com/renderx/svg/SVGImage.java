/*
 * SVGImage - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.svg;

import java.io.IOException;
import java.io.InputStream;
import java.util.StringTokenizer;
import java.util.zip.GZIPInputStream;

import com.renderx.graphics.ImageFormatException;
import com.renderx.graphics.XMLVectorImage;
import com.renderx.graphics.vector.ImageTree;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class SVGImage extends XMLVectorImage {
	private static final int[] gzip_magic = { 31, 139 };

	static class ParseTerminationException extends SAXException {
		public ParseTerminationException() {
			super("");
		}
	}

	class SizeReader extends DefaultHandler {
		final String svgURI = "http://www.w3.org/2000/svg";

		public void startElement(String string, String string_0_,
				String string_1_, Attributes attributes) throws SAXException {
			if (!"http://www.w3.org/2000/svg".equals(string))
				throw new SAXException("Root element in an SVG image "
						+ SVGImage.this.toDisplayString()
						+ " is not in the SVG namespace ("
						+ "http://www.w3.org/2000/svg" + ")");
			if (!"svg".equals(string_0_))
				throw new SAXException(
						"Root element is not 'svg' in SVG image "
								+ SVGImage.this.toDisplayString());
			try {
				width = parseLength(attributes.getValue("", "width"));
				height = parseLength(attributes.getValue("", "height"));
			} catch (SVGParseException svgparseexception) {
				String string_2_ = attributes.getValue("", "viewBox");
				if (string_2_ == null)
					throw svgparseexception;
				StringTokenizer stringtokenizer = new StringTokenizer(
						string_2_, ", \t\n\r");
				if (!stringtokenizer.hasMoreTokens())
					throw new SVGParseException(
							"Invalid viewBox attribute: no data");
				double d = new SVGAttrValue.Length(stringtokenizer.nextToken()).len;
				if (!stringtokenizer.hasMoreTokens())
					throw new SVGParseException(
							"Invalid viewBox attribute: required 4 numbers, only 1 found");
				double d_3_ = new SVGAttrValue.Length(stringtokenizer
						.nextToken()).len;
				if (!stringtokenizer.hasMoreTokens())
					throw new SVGParseException(
							"Invalid viewBox attribute: required 4 numbers, only 2 found");
				width = new SVGAttrValue.Length(stringtokenizer.nextToken()).len;
				if (!stringtokenizer.hasMoreTokens())
					throw new SVGParseException(
							"Invalid viewBox attribute: required 4 numbers, only 3 found");
				height = new SVGAttrValue.Length(stringtokenizer.nextToken()).len;
				if (stringtokenizer.hasMoreTokens())
					throw new SVGParseException(
							"Invalid viewBox attribute: extra data after 4 numbers");
			}
			throw new ParseTerminationException();
		}

		private double parseLength(String string) throws SAXException {
			if (string == null)
				throw new SVGParseException(
						"Missing either 'height' or 'width' on root element of SVG image "
								+ SVGImage.this.toDisplayString());
			SVGAttrValue.Length length = new SVGAttrValue.Length(string);
			if (length.base == 1)
				throw new SVGParseException(
						"Em units are not supported in dimensions of a top-level 'svg' element");
			if (length.base == 2)
				throw new SVGParseException(
						"Ex units are not supported in dimensions of a top-level 'svg' element");
			if (length.base == 3)
				throw new SVGParseException(
						"Percentages are not supported in dimensions of a top-level 'svg' element");
			return length.len;
		}
	}

	public InputStream openImageStream() throws IOException {
		InputStream inputstream = super.openImageStream();
		boolean bool = false;
		try {
			bool = (gzip_magic[0] == inputstream.read() && gzip_magic[1] == inputstream
					.read());
		} finally {
			inputstream.close();
		}
		return (bool ? (InputStream) new GZIPInputStream(super
				.openImageStream()) : super.openImageStream());
	}

	public void parse(XMLReader xmlreader, InputSource inputsource)
			throws IOException, ImageFormatException, SAXException {
		try {
			xmlreader.setContentHandler(new SizeReader());
			xmlreader.parse(inputsource);
		} catch (ParseTerminationException parseterminationexception) {
			/* empty */
		}
		supportsVectorRendering = true;
	}

	public ImageTree getImageTree(XMLReader xmlreader, InputSource inputsource)
			throws IOException, ImageFormatException, SAXException {
		return (new Parser(factory, factory.fontCatalog, factory.errHandler)
				.parse(xmlreader, inputsource, source.getBase(), width, height));
	}
}