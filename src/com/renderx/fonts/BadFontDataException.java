/*
 * BadFontDataException - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.fonts;

public class BadFontDataException extends RuntimeException {
	public BadFontDataException(String string) {
		super(string);
	}
}