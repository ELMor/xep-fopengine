/*
 * FontFileFormatError - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.fonts;

public class FontFileFormatError extends Exception {
	public FontFileFormatError() {
		/* empty */
	}

	public FontFileFormatError(String string) {
		super(string);
	}
}