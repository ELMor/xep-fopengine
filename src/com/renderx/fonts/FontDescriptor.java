/*
 * FontDescriptor - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.fonts;

import com.renderx.util.Array;

public class FontDescriptor {
	public final FontRecord record;

	public final UsedGlyphs used;

	public final Array encodingTable;

	public boolean canExtend = true;

	public boolean hasUnmappedChars = false;

	public static final int LATIN1 = 0;

	public static final int WINANSI = 0;

	public static final int MACROMAN = 1;

	public FontDescriptor(FontRecord fontrecord, Array array,
			UsedGlyphs usedglyphs) {
		record = fontrecord;
		fontrecord.getMetric();
		if (!fontrecord.embed && fontrecord.datatype == 2) {
			canExtend = false;
			Encoding encoding = new Encoding();
			Encoding encoding_0_ = new Encoding();
			for (int i = 0; i < 256; i++) {
				char c = Encoding.WINANSI[i];
				encoding.assignCode(c, fontrecord.getMetric().ucm(c));
				char c_1_ = Encoding.MACROMAN[i];
				encoding_0_.assignCode(c_1_, fontrecord.getMetric().ucm(c_1_));
			}
			encodingTable = new Array();
			encodingTable.put(0, encoding);
			encodingTable.put(1, encoding_0_);
		} else if (array == null || array.length() == 0) {
			Encoding encoding = new Encoding();
			for (int i = 0; i < 256; i++) {
				char c = Encoding.LATIN1[i];
				encoding.assignCode(c, fontrecord.getMetric().ucm(c));
			}
			encodingTable = new Array();
			encodingTable.put(0, encoding);
		} else
			encodingTable = array;
		used = usedglyphs;
	}
}