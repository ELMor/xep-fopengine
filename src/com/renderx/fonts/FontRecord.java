/*
 * FontRecord - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.fonts;

import java.io.IOException;
import java.io.InputStream;

import com.renderx.util.Array;
import com.renderx.util.ErrorHandler;
import com.renderx.util.Hashtable;
import com.renderx.util.SeekableInput;
import com.renderx.util.URLCache;
import com.renderx.util.URLSpec;
import com.renderx.util.UnicodeData;
import com.renderx.util.UnicodeTable;

public class FontRecord implements Cloneable {
	public final String familyName;

	private Metric metric = null;

	public boolean embed = false;

	public boolean subset = true;

	public boolean makeSmallCaps = false;

	public double smallCapSizeRatio = 0.8;

	public double extraSlantAngle = 0.0;

	public static final int NONE = 0;

	public static final int TYPE1 = 1;

	public static final int TRUETYPE = 2;

	public static final int OPENTYPE_CFF = 3;

	public static final int OPENTYPE_CFF_CID = 4;

	public int datatype = 0;

	public URLSpec metricfile = null;

	public URLSpec glyphlistfile = null;

	public URLSpec fontfile = null;

	public UnicodeTable ligatures = null;

	public int subfont = 1;

	private final ErrorHandler errHandler;

	private final URLCache cache;

	FontRecord cloneRecord() {
		try {
			return (FontRecord) super.clone();
		} catch (CloneNotSupportedException clonenotsupportedexception) {
			return null;
		}
	}

	FontRecord(String string, Hashtable hashtable, URLCache urlcache,
			ErrorHandler errorhandler) throws FontConfigurationException {
		familyName = string;
		cache = urlcache;
		errHandler = errorhandler;
		metricfile = (URLSpec) hashtable.get("ttf");
		if (metricfile == null)
			metricfile = (URLSpec) hashtable.get("otf");
		if (metricfile == null)
			metricfile = (URLSpec) hashtable.get("ttc");
		if (metricfile != null) {
			datatype = 2;
			fontfile = metricfile;
			String string_0_ = (String) hashtable.get("subfont");
			if (string_0_ != null) {
				try {
					subfont = Integer.parseInt(string_0_);
				} catch (NumberFormatException numberformatexception) {
					throw new FontConfigurationException(
							"Invalid subfont attribute '" + string_0_
									+ "'; should be a positive integer");
				}
			}
		} else {
			metricfile = (URLSpec) hashtable.get("afm");
			if (metricfile != null) {
				datatype = 1;
				glyphlistfile = (URLSpec) hashtable.get("glyph-list");
				fontfile = (URLSpec) hashtable.get("pfa");
				if (fontfile == null)
					fontfile = (URLSpec) hashtable.get("pfb");
			} else
				throw new FontConfigurationException(
						"No metric information present for font " + metricfile);
		}
		embed = getBooleanProperty((String) hashtable.get("embed"),
				fontfile != null);
		subset = getBooleanProperty((String) hashtable.get("subset"), true);
		if (embed && fontfile == null) {
			embed = false;
			error("No outline file for font metric '" + metricfile
					+ "'; font cannot be embedded");
		}
		String string_1_ = (String) hashtable.get("slant-angle");
		if (string_1_ != null) {
			try {
				extraSlantAngle = new Double(string_1_).doubleValue();
			} catch (NumberFormatException numberformatexception) {
				if (errorhandler != null)
					errorhandler.error("Bad value for slant angle: '"
							+ string_1_ + "'; property ignored");
			}
		}
		String string_2_ = (String) hashtable.get("smallcaps-size");
		if (string_2_ != null) {
			makeSmallCaps = true;
			try {
				smallCapSizeRatio = new Double(string_2_).doubleValue();
			} catch (NumberFormatException numberformatexception) {
				if (errorhandler != null)
					errorhandler.error("Bad value for smallcap size ratio: '"
							+ string_1_ + "'; using default value of " + 0.8);
			}
		}
		String string_3_ = (String) hashtable.get("ligatures");
		if (string_3_ != null && string_3_.trim().length() > 0) {
			boolean bool = false;
			Array array = new Array();
			for (int i = 0; i < string_3_.length(); i++) {
				char c = string_3_.charAt(i);
				char[] cs = (char[]) UnicodeData.LIGATURES.get(c);
				if (cs != null && cs.length > 1) {
					StringBuffer stringbuffer = (StringBuffer) array
							.get(cs.length);
					if (stringbuffer == null)
						array.put(cs.length, stringbuffer = new StringBuffer());
					stringbuffer.append(c);
				}
			}
			StringBuffer stringbuffer = new StringBuffer();
			int i = array.length();
			while (i-- > 0) {
				StringBuffer stringbuffer_4_ = (StringBuffer) array.get(i);
				if (stringbuffer_4_ != null)
					stringbuffer.append((Object) stringbuffer_4_);
			}
			ligatures = new UnicodeTable();
			for (i = 0; i < stringbuffer.length(); i++) {
				char c = stringbuffer.charAt(i);
				char[] cs = (char[]) UnicodeData.LIGATURES.get(c);
				char[] cs_5_ = new char[cs.length];
				cs_5_[0] = c;
				for (int i_6_ = 1; i_6_ < cs.length; i_6_++)
					cs_5_[i_6_] = cs[i_6_];
				Array array_7_ = (Array) ligatures.get(cs[0]);
				if (array_7_ == null)
					array_7_ = new Array();
				array_7_.put(array_7_.length(), cs_5_);
				ligatures.put(cs[0], array_7_);
			}
		}
	}

	public synchronized Metric getMetric() {
		if (metric != null)
			return metric;
		try {
			switch (datatype) {
			case 2: {
				SeekableInput seekableinput = cache.openSeekableStream(
						metricfile, errHandler);
				if (seekableinput == null)
					break;
				try {
					metric = new TTMetric(seekableinput, subfont);
					break;
				} finally {
					seekableinput.close();
				}
			}
			case 1: {
				InputStream inputstream = cache.openStream(metricfile,
						errHandler);
				if (inputstream == null)
					break;
				try {
					do {
						if (glyphlistfile == null)
							metric = new AFMMetric(inputstream);
						else {
							InputStream inputstream_8_ = cache.openStream(
									glyphlistfile, errHandler);
							if (inputstream_8_ != null) {
								try {
									metric = (new AFMMetric(inputstream,
											new GlyphList(inputstream_8_)));
									break;
								} finally {
									inputstream_8_.close();
								}
							}
							metric = null;
						}
					} while (false);
					break;
				} finally {
					inputstream.close();
				}
			}
			default:
				throw new RuntimeException("Invalid font type: " + datatype);
			}
		} catch (FontFileFormatError fontfileformaterror) {
			error("Unsupported file format or broken file found reading font metric from "
					+ metricfile + ": " + fontfileformaterror.getMessage());
		} catch (IOException ioexception) {
			exception("Cannot read font metric from " + metricfile, ioexception);
		}
		if (metric == null)
			throw new BadFontDataException(
					"Could not obtain font metric for font family '"
							+ familyName + "'");
		if (embed) {
			if (!metric.embeddable()) {
				error("Font '"
						+ metric.fontName
						+ "' cannot be embedded because of license restrictions");
				embed = false;
			} else if (subset && !metric.subsettable()) {
				error("Font '"
						+ metric.fontName
						+ "' cannot be subsetted because of license restrictions");
				subset = false;
			}
		}
		if (metric.isCFF) {
			if (metric.isCIDFont)
				datatype = 4;
			else
				datatype = 3;
		}
		return metric;
	}

	public InputStream openFontStream() throws IOException {
		return (fontfile == null ? null : cache
				.openStream(fontfile, errHandler));
	}

	public SeekableInput openSeekableFontStream() throws IOException {
		return (fontfile == null ? null : cache.openSeekableStream(fontfile,
				errHandler));
	}

	private boolean getBooleanProperty(String string, boolean bool) {
		if (string == null || "auto".equalsIgnoreCase(string))
			return bool;
		if ("true".equalsIgnoreCase(string) || "yes".equalsIgnoreCase(string))
			return true;
		if ("false".equalsIgnoreCase(string) || "no".equalsIgnoreCase(string))
			return false;
		error("Invalid value '" + string + "' for a boolean property ignored");
		return bool;
	}

	public void warning(String string) {
		if (errHandler != null) {
			synchronized (errHandler) {
				errHandler.warning(string);
			}
		}
	}

	public void error(String string) {
		if (errHandler != null) {
			synchronized (errHandler) {
				errHandler.error(string);
			}
		}
	}

	public void exception(String string, Exception exception) {
		if (errHandler != null) {
			synchronized (errHandler) {
				errHandler.exception(string, exception);
			}
		}
	}
}