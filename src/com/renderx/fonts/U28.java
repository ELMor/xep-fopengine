/*
 * U28 - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.fonts;

import java.io.IOException;

import com.renderx.util.Array;
import com.renderx.util.Hashtable;

public abstract class U28 {
	public final Hashtable fontStateTable = new Hashtable();

	protected FontDescriptor curFont = null;

	protected boolean fontSpecified = false;

	protected int fontEncoding = 0;

	protected final FontCatalog catalog;

	protected Hashtable fontEncodingTable = new Hashtable();

	protected Hashtable usedGlyphsTable = new Hashtable();

	protected char[] txt = new char[32];

	protected int itxt = 0;

	public U28(FontCatalog fontcatalog) {
		catalog = fontcatalog;
	}

	protected abstract void flushTextBuffer() throws IOException;

	protected abstract boolean isOneByteText(FontRecord fontrecord);

	protected abstract boolean isProcessAsUnicode(FontRecord fontrecord);

	public void resetFont() {
		fontSpecified = false;
	}

	public void selectFont(String string, int i, String string_0_,
			String string_1_) {
		fontSpecified = false;
		fontEncoding = 0;
		FontRecord fontrecord = catalog.getFontRecord(string, i, string_0_,
				string_1_);
		curFont = (FontDescriptor) fontStateTable.get(fontrecord);
		if (curFont == null) {
			Array array = ((Array) fontEncodingTable
					.get(fontrecord.getMetric().fontName));
			if (array == null)
				array = new Array();
			UsedGlyphs usedglyphs = ((UsedGlyphs) usedGlyphsTable
					.get(fontrecord.getMetric().fontName));
			if (usedglyphs == null)
				usedglyphs = new UsedGlyphs();
			curFont = new FontDescriptor(fontrecord, array, usedglyphs);
			fontStateTable.put(fontrecord, curFont);
			fontEncodingTable.put(fontrecord.getMetric().fontName, array);
			usedGlyphsTable.put(fontrecord.getMetric().fontName, usedglyphs);
		}
	}

	public void processText(String string) throws IOException {
		if (isOneByteText(curFont.record))
			processOneByteText(string);
		else if (isProcessAsUnicode(curFont.record))
			processMultiByteTextUnicode(string);
		else
			processMultiByteTextCID(string);
	}

	private void processOneByteText(String string) throws IOException {
		if (txt.length < string.length())
			txt = new char[string.length()];
		Object object = null;
		Encoding encoding;
		if (!curFont.canExtend) {
			if (curFont.encodingTable.length() == 0)
				throw new RuntimeException(
						"Empty encoding table in font descriptor");
			encoding = (Encoding) curFont.encodingTable.get(fontEncoding);
		} else if (curFont.encodingTable.length() != 0) {
			try {
				encoding = (Encoding) curFont.encodingTable.get(fontEncoding);
			} catch (Exception exception) {
				throw new RuntimeException("Wrong encoding is specified");
			}
		} else {
			encoding = new Encoding();
			curFont.encodingTable.put(0, encoding);
		}
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			int i_2_ = encoding.getCode(c);
			CharMetrics charmetrics = curFont.record.getMetric().ucm(c);
			if (charmetrics == null) {
				charmetrics = curFont.record.getMetric().missingGlyph;
				if (charmetrics == null)
					continue;
				c = charmetrics.uc;
				i_2_ = encoding.getCode(c);
			}
			if (i_2_ == -1) {
				Encoding encoding_3_ = null;
				int i_4_;
				for (i_4_ = 0; i_4_ < curFont.encodingTable.length(); i_4_++) {
					encoding_3_ = (Encoding) curFont.encodingTable.get(i_4_);
					if (encoding_3_ == null)
						throw new RuntimeException(
								"Null encoding in font descriptor");
					i_2_ = encoding_3_.getCode(c);
					if (i_2_ != -1)
						break;
				}
				if (i_2_ == -1) {
					if (!curFont.canExtend) {
						curFont.hasUnmappedChars = true;
						i_4_ = fontEncoding;
						charmetrics = curFont.record.getMetric().ucm(' ');
						if (charmetrics == null) {
							charmetrics = curFont.record.getMetric().missingGlyph;
							if (charmetrics == null)
								continue;
						}
						c = charmetrics.uc;
						i_2_ = encoding.getCode(c);
					} else {
						if (encoding_3_.hasRoom())
							i_4_--;
						else {
							encoding_3_ = new Encoding();
							curFont.encodingTable.put(i_4_, encoding_3_);
						}
						i_2_ = encoding_3_.assignCode(c, charmetrics);
					}
				}
				if (i_4_ != fontEncoding) {
					flushTextBuffer();
					fontSpecified = false;
					fontEncoding = i_4_;
					encoding = encoding_3_;
				}
			}
			txt[itxt++] = (char) i_2_;
			curFont.used.markGlyph(c);
		}
		flushTextBuffer();
	}

	public void processMultiByteTextUnicode(String string) throws IOException {
		if (txt.length < 2 * string.length())
			txt = new char[2 * string.length()];
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			txt[itxt++] = (char) (c >> 8 & 0xff);
			txt[itxt++] = (char) (c & 0xff);
			curFont.used.markGlyph(c);
		}
		flushTextBuffer();
	}

	public void processMultiByteTextCID(String string) throws IOException {
		if (txt.length < 2 * string.length())
			txt = new char[2 * string.length()];
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			CharMetrics charmetrics = curFont.record.getMetric().ucm(c);
			int i_5_;
			if (charmetrics == null)
				i_5_ = 0;
			else {
				curFont.used.markGlyph(c);
				i_5_ = (char) charmetrics.c;
			}
			txt[itxt++] = (char) (i_5_ >> 8 & 0xff);
			txt[itxt++] = (char) (i_5_ & 0xff);
		}
		flushTextBuffer();
	}
}