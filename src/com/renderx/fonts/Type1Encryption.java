/*
 * Type1Encryption - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.fonts;

public class Type1Encryption {
	private static final int c1 = 52845;

	private static final int c2 = 22719;

	public static void encrypt(int i, byte[] is) {
		for (int i_0_ = 0; i_0_ < is.length; i_0_++) {
			byte i_1_ = (byte) ((is[i_0_] < 0 ? (int) (256 + is[i_0_])
					: is[i_0_]) ^ i >> 8);
			i = ((i_1_ < 0 ? (int) (256 + i_1_) : i_1_) + i) * 52845 + 22719;
			is[i_0_] = i_1_;
		}
	}

	public static byte[] decrypt(int i, byte[] is) {
		byte[] is_2_ = new byte[is.length];
		for (int i_3_ = 0; i_3_ < is.length; i_3_++) {
			int i_4_ = is[i_3_] < 0 ? (int) (256 + is[i_3_]) : is[i_3_];
			byte i_5_ = (byte) (i_4_ ^ i >> 8);
			i = (i_4_ + i) * 52845 + 22719;
			is_2_[i_3_] = i_5_;
		}
		return is_2_;
	}
}