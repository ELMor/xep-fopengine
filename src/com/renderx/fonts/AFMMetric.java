/*
 * AFMMetric - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.fonts;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.StringTokenizer;

import com.renderx.util.Hashtable;
import com.renderx.util.LineEnumerator;
import com.renderx.util.UnicodeTable;

public class AFMMetric extends Metric {
	private static final Hashtable KP = new Hashtable();

	private static final Hashtable K = new Hashtable();

	private static final Hashtable KW = new Hashtable();

	private static final short KP_ = 0;

	private static final short KP_H = 1;

	private static final short KP_X = 2;

	private static final short KP_Y = 3;

	private static final short K_C = 0;

	private static final short K_CH = 1;

	private static final short K_WX = 2;

	private static final short K_W0X = 3;

	private static final short K_W1X = 4;

	private static final short K_WY = 5;

	private static final short K_W0Y = 6;

	private static final short K_W1Y = 7;

	private static final short K_W = 8;

	private static final short K_W0 = 9;

	private static final short K_W1 = 10;

	private static final short K_VV = 11;

	private static final short K_N = 12;

	private static final short K_B = 13;

	private static final short K_L = 14;

	private static final short KW_FontName = 0;

	private static final short KW_FullName = 1;

	private static final short KW_FamilyName = 2;

	private static final short KW_Version = 3;

	private static final short KW_Notice = 4;

	private static final short KW_CharacterSet = 5;

	private static final short KW_EncodingScheme = 6;

	private static final short KW_Weight = 7;

	private static final short KW_ItalicAngle = 8;

	private static final short KW_UnderlinePosition = 9;

	private static final short KW_UnderlineThickness = 10;

	private static final short KW_CapHeight = 11;

	private static final short KW_XHeight = 12;

	private static final short KW_Ascender = 13;

	private static final short KW_Descender = 14;

	private static final short KW_CharWidth = 15;

	private static final short KW_StdHW = 16;

	private static final short KW_StdVW = 17;

	private static final short KW_FontBBox = 18;

	private static final short KW_IsFixedPitch = 19;

	private static final short KW_IsCIDFont = 20;

	private static final short KW_StartCharMetrics = 21;

	private static final short KW_StartKernPairs = 22;

	private static final short KW_StartTrackKern = 23;

	private static final short KW_EndFontMetrics = 24;

	public String encodingScheme = "";

	public String characterSet = "";

	public Encoding builtinEncoding = new Encoding();

	public AFMMetric(InputStream inputstream) throws IOException,
			FontFileFormatError {
		this(inputstream, GlyphList.dflt);
	}

	public AFMMetric(InputStream inputstream, GlyphList glyphlist)
			throws IOException, FontFileFormatError {
		LineEnumerator lineenumerator = new LineEnumerator(inputstream);
		while_1_: while (lineenumerator.hasMoreLines()) {
			String string = lineenumerator.nextLine();
			StringTokenizer stringtokenizer = new StringTokenizer(string);
			if (stringtokenizer.hasMoreTokens()) {
				Object object = KW.get(stringtokenizer.nextToken().trim());
				if (object instanceof Short) {
					switch (((Short) object).shortValue()) {
					case 0:
						fontName = parseName(string);
						break;
					case 1:
						fullName = parseName(string);
						break;
					case 2:
						family = parseName(string);
						break;
					case 3:
						version = parseName(string);
						break;
					case 4:
						notice = parseName(string);
						break;
					case 5:
						characterSet = parseName(string);
						break;
					case 6:
						encodingScheme = parseName(string);
						break;
					case 7:
						weight = parseName(string);
						break;
					case 8:
						italicAngle = parseFloat(stringtokenizer.nextToken());
						break;
					case 9:
						underlinePosition = parseFloat(stringtokenizer
								.nextToken());
						break;
					case 10:
						underlineThickness = parseFloat(stringtokenizer
								.nextToken());
						break;
					case 11:
						capHeight = parseFloat(stringtokenizer.nextToken());
						break;
					case 12:
						xHeight = parseFloat(stringtokenizer.nextToken());
						break;
					case 13:
						ascender = parseFloat(stringtokenizer.nextToken());
						break;
					case 14:
						descender = parseFloat(stringtokenizer.nextToken());
						break;
					case 16:
						stdHW = parseFloat(stringtokenizer.nextToken());
						break;
					case 17:
						stdVW = parseFloat(stringtokenizer.nextToken());
						break;
					case 18:
						fontBBox = parseFloatArray(stringtokenizer, 4);
						break;
					case 19:
						isFixedPitch = "true".equals(stringtokenizer
								.nextToken());
						break;
					case 20:
						isCIDFont = "true".equals(stringtokenizer.nextToken());
						break;
					case 15:
						wx = parseFloat(stringtokenizer.nextToken());
						wy = parseFloat(stringtokenizer.nextToken());
						isFixedPitch = true;
						break;
					case 21:
						readCharMetrics(lineenumerator, glyphlist);
						break;
					case 22:
						readKernPairs(lineenumerator, glyphlist);
						break;
					case 24:
						break while_1_;
					}
				}
			}
		}
		this.checkData();
	}

	private static final String parseName(String string)
			throws FontFileFormatError {
		int i = string.indexOf(" ");
		if (i == -1)
			i = string.indexOf("\t");
		if (i == -1)
			throw new FontFileFormatError(
					"Expect at least two tokens of the line");
		return string.substring(i + 1).trim();
	}

	private static final int parseHexInt(String string)
			throws FontFileFormatError {
		if (!string.startsWith("<") && !string.endsWith(">"))
			throw new FontFileFormatError(
					"Expected a hex number in angle brackets");
		return Integer.parseInt(string.substring(1, string.length() - 1), 16);
	}

	private static final float parseFloat(String string)
			throws NumberFormatException {
		return Float.valueOf(string).floatValue();
	}

	private static final float[] parseFloatArray(
			StringTokenizer stringtokenizer, int i) throws FontFileFormatError {
		float[] fs = new float[i];
		for (int i_0_ = 0; i_0_ < fs.length; i_0_++) {
			if (!stringtokenizer.hasMoreTokens())
				throw new FontFileFormatError(
						"Too few numbers in the array: expected " + i);
			fs[i_0_] = parseFloat(stringtokenizer.nextToken());
		}
		if (stringtokenizer.hasMoreTokens())
			throw new FontFileFormatError(
					"Too many numbers in the array: expected " + i);
		return fs;
	}

	private void readCharMetrics(LineEnumerator lineenumerator,
			GlyphList glyphlist) throws IOException, FontFileFormatError {
		Object object = null;
		String string;
		while (!"EndCharMetrics".equals(string = lineenumerator.nextLine()
				.trim())) {
			StringTokenizer stringtokenizer = new StringTokenizer(string, ";");
			if (stringtokenizer.hasMoreTokens()) {
				CharMetrics charmetrics = new CharMetrics();
				while (stringtokenizer.hasMoreTokens()) {
					StringTokenizer stringtokenizer_1_ = new StringTokenizer(
							stringtokenizer.nextToken().trim());
					Short var_short = (Short) K.get(stringtokenizer_1_
							.nextToken().trim());
					if (var_short != null) {
						switch (var_short.shortValue()) {
						case 0: {
							int i = Integer.parseInt(stringtokenizer_1_
									.nextToken());
							if (i != -1)
								charmetrics.c = i;
							break;
						}
						case 1: {
							int i = parseHexInt(stringtokenizer_1_.nextToken());
							if (i != -1)
								charmetrics.c = i;
							break;
						}
						case 2:
						case 3:
							charmetrics.wx = parseFloat(stringtokenizer_1_
									.nextToken());
							break;
						case 4: {
							float f = parseFloat(stringtokenizer_1_.nextToken());
							if (charmetrics.wx == 1.4E-45F)
								charmetrics.wx = f;
							break;
						}
						case 5:
						case 6:
							charmetrics.wy = parseFloat(stringtokenizer_1_
									.nextToken());
							break;
						case 7: {
							float f = parseFloat(stringtokenizer_1_.nextToken());
							if (charmetrics.wy == 1.4E-45F)
								charmetrics.wy = f;
							break;
						}
						case 8:
						case 9: {
							float[] fs = parseFloatArray(stringtokenizer_1_, 2);
							charmetrics.wx = fs[0];
							charmetrics.wy = fs[1];
							break;
						}
						case 10: {
							float[] fs = parseFloatArray(stringtokenizer_1_, 2);
							if (charmetrics.wx == 1.4E-45F)
								charmetrics.wx = fs[0];
							if (charmetrics.wy == 1.4E-45F)
								charmetrics.wy = fs[1];
							break;
						}
						case 12:
							charmetrics.n = stringtokenizer_1_.nextToken();
							break;
						case 13:
							charmetrics.b = parseFloatArray(stringtokenizer_1_,
									4);
							break;
						}
					}
				}
				if (charmetrics.n != null) {
					numGlyphs++;
					if (".notdef".equals(charmetrics.n) && missingGlyph == null)
						missingGlyph = charmetrics;
					if (charmetrics.wx == 1.4E-45F)
						charmetrics.wx = wx;
					if (charmetrics.wy == 1.4E-45F)
						charmetrics.wy = wy;
					if (charmetrics.b == null)
						charmetrics.b = fontBBox;
					else {
						if (capHeight == 1.4E-45F && charmetrics.uc == 'H')
							capHeight = charmetrics.b[3];
						if (xHeight == 1.4E-45F && charmetrics.uc == 'x')
							xHeight = charmetrics.b[3];
					}
					char[] cs = glyphlist.getUnicodes(charmetrics.n);
					if (cs != null) {
						charmetrics.uc = cs[0];
						for (int i = 0; i < cs.length; i++) {
							if (0 <= charmetrics.c && charmetrics.c < 256) {
								uniTable.put(cs[i], charmetrics);
								if ((charmetrics.c == 32) == (cs[i] == ' ')) {
									builtinEncoding.uni2byte.put(cs[i],
											new Integer(charmetrics.c));
									builtinEncoding.table[charmetrics.c] = charmetrics;
								}
							} else if (uniTable.get(cs[i]) == null)
								uniTable.put(cs[i], charmetrics);
						}
					}
				}
			}
		}
	}

	private void readKernPairs(LineEnumerator lineenumerator,
			GlyphList glyphlist) throws IOException, FontFileFormatError {
		Object object = null;
		String string;
		while (!"EndKernPairs"
				.equals(string = lineenumerator.nextLine().trim())) {
			double d = 0.0;
			double d_2_ = 0.0;
			Object object_3_ = null;
			Object object_4_ = null;
			StringTokenizer stringtokenizer = new StringTokenizer(string);
			if (stringtokenizer.hasMoreTokens()) {
				Short var_short = (Short) KP.get(stringtokenizer.nextToken()
						.trim());
				if (var_short != null && var_short.shortValue() != 1) {
					char[] cs = glyphlist.getUnicodes(stringtokenizer
							.nextToken());
					char[] cs_5_ = glyphlist.getUnicodes(stringtokenizer
							.nextToken());
					switch (var_short.shortValue()) {
					case 0:
						d = (double) parseFloat(stringtokenizer.nextToken());
						d_2_ = (double) parseFloat(stringtokenizer.nextToken());
						break;
					case 2:
						d = (double) parseFloat(stringtokenizer.nextToken());
						break;
					case 3:
						d_2_ = (double) parseFloat(stringtokenizer.nextToken());
						break;
					}
					if (cs != null && cs_5_ != null) {
						for (int i = 0; i < cs.length; i++) {
							CharMetrics charmetrics = this.ucm(cs[i]);
							if (charmetrics != null) {
								if (charmetrics.kernTable == null)
									charmetrics.kernTable = new UnicodeTable();
								for (int i_6_ = 0; i_6_ < cs_5_.length; i_6_++) {
									KernVector kernvector = new KernVector();
									kernvector.X = d;
									kernvector.Y = d_2_;
									charmetrics.kernTable.put(cs_5_[i_6_],
											kernvector);
								}
							}
						}
					}
				}
			}
		}
	}

	public void dump(PrintStream printstream) {
		super.dump(printstream);
		printstream.println("\nAFM-specific font data:");
		printstream.println("  Character set: " + characterSet);
		printstream.println("  Encoding scheme: " + encodingScheme);
		printstream.println("  Built-in encoding:");
		builtinEncoding.dump(printstream);
	}

	public static void main(String[] strings) throws Exception {
		GlyphList.init();
		switch (strings.length) {
		case 1:
			new AFMMetric(new FileInputStream(strings[0])).dump(System.out);
			break;
		case 2:
			new AFMMetric(new FileInputStream(strings[0]), new GlyphList(
					new FileInputStream(strings[1]))).dump(System.out);
			break;
		default:
			System.err
					.println("Usage: java com.renderx.fonts.AFMMetric <AFM filename> [<glyph list file name>]");
		}
	}

	static {
		KP.put("KP", new Short((short) 0));
		KP.put("KPH", new Short((short) 1));
		KP.put("KPX", new Short((short) 2));
		KP.put("KPY", new Short((short) 3));
		K.put("C", new Short((short) 0));
		K.put("CH", new Short((short) 1));
		K.put("WX", new Short((short) 2));
		K.put("W0X", new Short((short) 3));
		K.put("W1X", new Short((short) 4));
		K.put("WY", new Short((short) 5));
		K.put("W0Y", new Short((short) 6));
		K.put("W1Y", new Short((short) 7));
		K.put("W", new Short((short) 8));
		K.put("W0", new Short((short) 9));
		K.put("W1", new Short((short) 10));
		K.put("VV", new Short((short) 11));
		K.put("N", new Short((short) 12));
		K.put("B", new Short((short) 13));
		K.put("L", new Short((short) 14));
		KW.put("FontName", new Short((short) 0));
		KW.put("FullName", new Short((short) 1));
		KW.put("FamilyName", new Short((short) 2));
		KW.put("Version", new Short((short) 3));
		KW.put("Notice", new Short((short) 4));
		KW.put("CharacterSet", new Short((short) 5));
		KW.put("EncodingScheme", new Short((short) 6));
		KW.put("Weight", new Short((short) 7));
		KW.put("ItalicAngle", new Short((short) 8));
		KW.put("UnderlinePosition", new Short((short) 9));
		KW.put("UnderlineThickness", new Short((short) 10));
		KW.put("CapHeight", new Short((short) 11));
		KW.put("XHeight", new Short((short) 12));
		KW.put("Ascender", new Short((short) 13));
		KW.put("Descender", new Short((short) 14));
		KW.put("CharWidth", new Short((short) 15));
		KW.put("StdHW", new Short((short) 16));
		KW.put("StdVW", new Short((short) 17));
		KW.put("FontBBox", new Short((short) 18));
		KW.put("IsFixedPitch", new Short((short) 19));
		KW.put("IsCIDFont", new Short((short) 20));
		KW.put("StartCharMetrics", new Short((short) 21));
		KW.put("StartKernPairs", new Short((short) 22));
		KW.put("StartKernPairs0", new Short((short) 22));
		KW.put("StartKernPairs1", new Short((short) 22));
		KW.put("EndFontMetrics", new Short((short) 24));
	}
}