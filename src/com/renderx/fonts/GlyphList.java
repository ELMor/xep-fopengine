/*
 * GlyphList - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.fonts;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.StringTokenizer;

import com.renderx.util.Hashtable;
import com.renderx.util.LineEnumerator;
import com.renderx.util.URLSpec;
import com.renderx.util.User;

public final class GlyphList {
	private static final String ADOBE_GLYPH_LIST = "resource:com/renderx/fonts/agl.txt";

	public static final GlyphList dflt = new GlyphList();

	private Hashtable Name2Code = new Hashtable();

	private static boolean initialized = false;

	public static synchronized void init() {
		if (!initialized) {
			String string = null;
			try {
				string = User.getProperty("com.renderx.fonts.ADOBE_GLYPH_LIST");
				if (string == null)
					string = "resource:com/renderx/fonts/agl.txt";
				dflt.readGlyphList(new URLSpec(string).openStream());
			} catch (Exception exception) {
				throw new RuntimeException("cannot parse " + string + ": "
						+ exception.toString());
			}
			initialized = true;
		}
	}

	public GlyphList() {
		/* empty */
	}

	public GlyphList(InputStream inputstream) throws IOException,
			FontFileFormatError {
		this();
		readGlyphList(inputstream);
	}

	public void readGlyphList(InputStream inputstream) throws IOException,
			FontFileFormatError {
		LineEnumerator lineenumerator = new LineEnumerator(inputstream);
		int i = 0;
		while (lineenumerator.hasMoreLines()) {
			String string = lineenumerator.nextLine();
			i++;
			if (!string.startsWith("#", 0)) {
				StringTokenizer stringtokenizer = new StringTokenizer(string,
						";");
				if (stringtokenizer.hasMoreTokens()) {
					String string_0_ = stringtokenizer.nextToken().trim();
					if (stringtokenizer.hasMoreTokens()) {
						String string_1_ = stringtokenizer.nextToken().trim();
						registerGlyph(string_1_, (char) Integer.parseInt(
								string_0_, 16), i);
					}
				}
			}
		}
	}

	public void readCMap(InputStream inputstream) throws IOException,
			FontFileFormatError {
		LineEnumerator lineenumerator = new LineEnumerator(inputstream);
		if (!lineenumerator.hasMoreLines())
			throw new FontFileFormatError("Empty file");
		String string = lineenumerator.nextLine();
		if (!string.startsWith("%!PS-Adobe-3.0"))
			throw new FontFileFormatError("File does not appear to be a CMap");
		int i = 1;
		int i_2_ = 0;
		String string_3_ = null;
		String string_4_ = null;
		String string_5_ = null;
		while (lineenumerator.hasMoreLines()) {
			string = lineenumerator.nextLine();
			i++;
			int i_6_ = string.indexOf('%');
			if (i_6_ != -1)
				string = string.substring(0, i_6_);
			StringTokenizer stringtokenizer = new StringTokenizer(string, " <>");
			while (stringtokenizer.hasMoreTokens()) {
				String string_7_ = stringtokenizer.nextToken().trim();
				switch (i_2_) {
				case 0:
					if (string_7_ == "begincidchar") {
						i_2_ = 1;
						string_3_ = null;
					} else if (string_7_ == "begincidrange") {
						i_2_ = 2;
						string_4_ = string_5_ = null;
					}
					break;
				case 1:
					if (string_7_ == "endcidchar")
						i_2_ = 0;
					else if (string_3_ == null)
						string_3_ = string_7_;
					else {
						char c = (char) Integer.parseInt(string_3_, 16);
						registerGlyph(string_7_, c, i);
						string_3_ = null;
					}
					break;
				case 2:
					if (string_7_ == "endcidrange")
						i_2_ = 0;
					else if (string_4_ == null)
						string_4_ = string_7_;
					else if (string_5_ == null)
						string_5_ = string_7_;
					else {
						char c = (char) Integer.parseInt(string_4_, 16);
						char c_8_ = (char) Integer.parseInt(string_5_, 16);
						int i_9_ = Integer.parseInt(string_7_, 10);
						while (c <= c_8_) {
							GlyphList glyphlist_10_ = this;
							String string_11_ = Integer.toString(i_9_++);
							char c_12_ = c;
							c++;
							glyphlist_10_.registerGlyph(string_11_, c_12_, i);
						}
						string_4_ = null;
						string_5_ = null;
					}
					break;
				}
			}
		}
	}

	private void registerGlyph(String string, char c, int i)
			throws FontFileFormatError {
		if (c == 0)
			throw new FontFileFormatError("[Line " + i + "]: glyph '" + string
					+ "' has an invalid Unicode number");
		char[] cs = (char[]) Name2Code.get(string);
		Object object = null;
		char[] cs_13_;
		if (cs != null) {
			cs_13_ = new char[cs.length + 1];
			for (int i_14_ = 0; i_14_ < cs.length; i_14_++)
				cs_13_[i_14_ + 1] = cs[i_14_];
		} else
			cs_13_ = new char[1];
		cs_13_[0] = c;
		Name2Code.put(string, cs_13_);
	}

	public char[] getUserUnicode(String string) {
		Object object = Name2Code.get(string);
		return (char[]) object;
	}

	public char[] getDfltUnicode(String string) {
		Object object = dflt.Name2Code.get(string);
		return (char[]) object;
	}

	public char[] getUnicodes(String string) {
		int i = string.indexOf('.');
		if (i != -1)
			string = string.substring(0, i);
		char[] cs = getUserUnicode(string);
		if (cs != null)
			return cs;
		cs = getDfltUnicode(string);
		if (cs != null)
			return cs;
		if (string.startsWith("uni"))
			string = string.substring(3);
		else if (string.startsWith("u"))
			string = string.substring(1);
		else
			return null;
		if (string.length() != 4)
			return null;
		try {
			return new char[] { (char) Integer.parseInt(string, 16) };
		} catch (NumberFormatException numberformatexception) {
			return null;
		}
	}

	public static void main(String[] strings) throws Exception {
		if (strings.length == 0) {
			System.err
					.println("Usage: com.renderx.fonts.GlyphList [-cmap | -gl] glyph_list_file_name");
			System.exit(1);
		}
		init();
		System.err.println();
		System.err.println("Default Glyph list:");
		System.err.println("-------------------");
		Enumeration enumeration = dflt.Name2Code.keys();
		while (enumeration.hasMoreElements()) {
			String string = (String) enumeration.nextElement();
			char[] cs = (char[]) dflt.Name2Code.get(string);
			System.err.println(string + "   0x" + Integer.toHexString(cs[0]));
		}
		try {
			Object object = null;
			GlyphList glyphlist;
			if (strings.length == 1)
				glyphlist = new GlyphList(new FileInputStream(strings[0]));
			else {
				glyphlist = new GlyphList();
				if (strings[0].equals("-cmap"))
					glyphlist.readCMap(new FileInputStream(strings[1]));
				else
					glyphlist.readGlyphList(new FileInputStream(strings[1]));
			}
			System.err.println();
			System.err.println("Instance Glyph list:");
			System.err.println("--------------------");
			enumeration = glyphlist.Name2Code.keys();
			while (enumeration.hasMoreElements()) {
				String string = (String) enumeration.nextElement();
				char[] cs = (char[]) glyphlist.Name2Code.get(string);
				System.err.println(string + "   0x"
						+ Integer.toHexString(cs[0]));
			}
		} catch (IOException ioexception) {
			System.err.println(ioexception);
		}
		System.exit(0);
	}
}