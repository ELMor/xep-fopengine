/*
 * TTFont - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.fonts;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;

import com.renderx.util.Hashtable;
import com.renderx.util.SeekableFileInputStream;
import com.renderx.util.SeekableInput;
import com.renderx.util.User;

public final class TTFont {
	byte[] header = new byte[12];

	public Hashtable tableslist = null;

	public Hashtable tables = null;

	boolean isTTC = false;

	public UsedGlyphs used = new UsedGlyphs();

	public long[] glyphoffsets = null;

	public byte[] glyphdata = null;

	boolean longoffset = false;

	public int numGlyphs = -1;

	int numHMetrics = -1;

	int numVMetrics = -1;

	public byte[] hmtx;

	public byte[] vmtx;

	public byte[] glyf;

	public byte[] loca;

	public TTFont(SeekableInput seekableinput, int i) throws IOException {
		used.clearGlyphs();
		tableslist = new Hashtable();
		tables = new Hashtable();
		String string = User
				.getProperty("com.renderx.xep.TrueType.DropBitmaps");
		boolean bool = !"false".equals(string) && !"no".equals(string);
		int i_0_ = 0;
		seekableinput.readFully(header);
		if (header[0] == 116 && header[1] == 116 && header[2] == 99
				&& header[3] == 102) {
			isTTC = true;
			while (i-- > 0)
				i_0_ = seekableinput.readInt();
			seekableinput.seek((long) i_0_);
			seekableinput.readFully(header);
		}
		int i_1_ = ushort(header, 4);
		for (int i_2_ = 0; i_2_ < i_1_; i_2_++) {
			seekableinput.seek((long) (i_0_ + 12 + 16 * i_2_));
			byte[] is = new byte[4];
			seekableinput.readFully(is);
			String string_3_ = new String(is);
			if (!bool
					|| (!string_3_.equals("EBDT") && !string_3_.equals("EBLC")
							&& !string_3_.equals("EBSC")
							&& !string_3_.equals("hdmx") && !string_3_
							.equals("VDMX"))) {
				byte[] is_4_ = new byte[12];
				seekableinput.readFully(is_4_);
				tableslist.put(string_3_, is_4_);
				long l = (long) ulong(is_4_, 4);
				int i_5_ = ulong(is_4_, 8);
				byte[] is_6_ = new byte[i_5_];
				seekableinput.seek(l);
				seekableinput.readFully(is_6_);
				tables.put(string_3_, is_6_);
			}
		}
		Object object = null;
		byte[] is = (byte[]) tables.get("maxp");
		numGlyphs = ushort(is, 4);
		is = (byte[]) tables.get("head");
		longoffset = ushort(is, 50) != 0;
		is = (byte[]) tables.get("hhea");
		numHMetrics = ushort(is, 34);
		is = (byte[]) tables.get("vhea");
		if (is != null)
			numVMetrics = ushort(is, 34);
		if (!tables.containsKey("CFF ")) {
			is = (byte[]) tables.get("loca");
			glyphoffsets = new long[numGlyphs + 1];
			for (int i_7_ = 0; i_7_ < numGlyphs + 1; i_7_++)
				glyphoffsets[i_7_] = (longoffset ? (long) ulong(is, i_7_ * 4)
						: (long) (ushort(is, i_7_ * 2) * 2));
			glyphdata = (byte[]) tables.get("glyf");
			addGlyph(0);
		}
	}

	public void addGlyph(int i) {
		if (!used.checkGlyph((char) i)) {
			used.markGlyph((char) i);
			if (glyphoffsets[i] != glyphoffsets[i + 1]) {
				int i_8_ = (int) glyphoffsets[i];
				if ((ushort(glyphdata, i_8_) & 0x8000) != 0) {
					i_8_ += 10;
					for (;;) {
						addGlyph(ushort(glyphdata, i_8_ + 2));
						int i_9_ = ushort(glyphdata, i_8_);
						if ((i_9_ & 0x20) == 0)
							break;
						i_8_ = i_8_ + ((i_9_ & 0x1) > 0 ? 8 : 6);
						if ((i_9_ & 0x8) > 0)
							i_8_ += 2;
						else if ((i_9_ & 0x40) > 0)
							i_8_ += 4;
						else if ((i_9_ & 0x80) > 0)
							i_8_ += 8;
					}
				}
			}
		}
	}

	public void createSubsetTables() throws IOException {
		boolean bool = false;
		for (int i = 0; i < numGlyphs && !bool; i++)
			bool = (used.checkGlyph((char) i) && glyphoffsets[i + 1] != glyphoffsets[i]);
		if (!bool) {
			int i;
			for (i = 0; glyphoffsets[i + 1] != glyphoffsets[i] && i < numGlyphs; i++) {
				/* empty */
			}
			if (i < numGlyphs)
				addGlyph(i);
		}
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		ByteArrayOutputStream bytearrayoutputstream_10_ = new ByteArrayOutputStream();
		byte[] is = (byte[]) tables.get("hmtx");
		byte[] is_11_ = (byte[]) tables.get("vmtx");
		hmtx = null;
		if (is != null) {
			hmtx = new byte[is.length];
			for (int i = 0; i < hmtx.length; i++)
				hmtx[i] = (byte) 0;
			for (int i = 4 * (numHMetrics - 1); i >= 0 && i < 4 * numHMetrics; i++)
				hmtx[i] = is[i];
		}
		vmtx = null;
		if (is_11_ != null) {
			vmtx = new byte[is_11_.length];
			for (int i = 0; i < vmtx.length; i++)
				vmtx[i] = (byte) 0;
			for (int i = 4 * (numVMetrics - 1); i >= 0 && i < 4 * numVMetrics; i++)
				vmtx[i] = is_11_[i];
		}
		int i = 0;
		for (int i_12_ = 0; i_12_ < numGlyphs; i_12_++) {
			bytearrayoutputstream_10_.write(longoffset ? int2long(i)
					: int2short(i / 2));
			if (used.checkGlyph((char) i_12_)) {
				int i_13_ = (int) (glyphoffsets[i_12_ + 1] - glyphoffsets[i_12_]);
				bytearrayoutputstream.write(glyphdata,
						(int) glyphoffsets[i_12_], i_13_);
				i += i_13_;
				if (hmtx != null) {
					int i_14_ = i_12_ < numHMetrics ? 4 : 2;
					int i_15_ = (i_12_ < numHMetrics ? 4 * i_12_
							: 2 * (i_12_ + numHMetrics));
					for (int i_16_ = i_15_; i_16_ < i_15_ + i_14_; i_16_++)
						hmtx[i_16_] = is[i_16_];
				}
				if (vmtx != null) {
					int i_17_ = i_12_ < numVMetrics ? 4 : 2;
					int i_18_ = (i_12_ < numVMetrics ? 4 * i_12_
							: 2 * (i_12_ + numVMetrics));
					for (int i_19_ = i_18_; i_19_ < i_18_ + i_17_; i_19_++)
						vmtx[i_19_] = is_11_[i_19_];
				}
			}
		}
		bytearrayoutputstream_10_.write(longoffset ? int2long(i)
				: int2short(i / 2));
		glyf = bytearrayoutputstream.toByteArray();
		loca = bytearrayoutputstream_10_.toByteArray();
	}

	public byte[] createSubsetHeader() throws IOException {
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		bytearrayoutputstream.write(header, 0, 4);
		int i = tableslist.size();
		int i_20_ = 0;
		int i_21_ = 2;
		while (i_21_ <= i) {
			i_21_ *= 2;
			i_20_++;
		}
		i_21_ *= 8;
		bytearrayoutputstream.write(int2short(i));
		bytearrayoutputstream.write(int2short(i_20_));
		bytearrayoutputstream.write(int2short(i_21_));
		bytearrayoutputstream.write(int2short(i * 16 - i_21_));
		Enumeration enumeration = tableslist.keys();
		int i_22_ = 12 + tableslist.size() * 16;
		while (enumeration.hasMoreElements()) {
			for (/**/; (i_22_ & 0x3) > 0; i_22_++) {
				/* empty */
			}
			String string = (String) enumeration.nextElement();
			byte[] is = string.getBytes();
			bytearrayoutputstream.write(is);
			byte[] is_23_ = (byte[]) tableslist.get(string);
			if (string.equals("loca")) {
				bytearrayoutputstream.write(int2long(calcCheckSum(loca)));
				bytearrayoutputstream.write(int2long(i_22_));
				bytearrayoutputstream.write(int2long(loca.length));
				i_22_ += loca.length;
			} else if (string.equals("glyf")) {
				bytearrayoutputstream.write(int2long(calcCheckSum(glyf)));
				bytearrayoutputstream.write(int2long(i_22_));
				bytearrayoutputstream.write(int2long(glyf.length));
				i_22_ += glyf.length;
			} else if (string.equals("hmtx")) {
				bytearrayoutputstream.write(int2long(calcCheckSum(hmtx)));
				bytearrayoutputstream.write(int2long(i_22_));
				bytearrayoutputstream.write(int2long(hmtx.length));
				i_22_ += hmtx.length;
			} else if (string.equals("vmtx")) {
				bytearrayoutputstream.write(int2long(calcCheckSum(vmtx)));
				bytearrayoutputstream.write(int2long(i_22_));
				bytearrayoutputstream.write(int2long(vmtx.length));
				i_22_ += vmtx.length;
			} else {
				bytearrayoutputstream.write(is_23_, 0, 4);
				bytearrayoutputstream.write(int2long(i_22_));
				bytearrayoutputstream.write(is_23_, 8, 4);
				i_22_ += ulong(is_23_, 8);
			}
		}
		return bytearrayoutputstream.toByteArray();
	}

	public byte[] createSubset() throws IOException {
		createSubsetTables();
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		bytearrayoutputstream.write(createSubsetHeader());
		Enumeration enumeration = tableslist.keys();
		while (enumeration.hasMoreElements()) {
			while ((bytearrayoutputstream.size() & 0x3) > 0)
				bytearrayoutputstream.write(0);
			String string = (String) enumeration.nextElement();
			if (string.equals("loca"))
				bytearrayoutputstream.write(loca);
			else if (string.equals("glyf"))
				bytearrayoutputstream.write(glyf);
			else if (string.equals("hmtx"))
				bytearrayoutputstream.write(hmtx);
			else if (string.equals("vmtx"))
				bytearrayoutputstream.write(vmtx);
			else
				bytearrayoutputstream.write((byte[]) tables.get(string));
		}
		return bytearrayoutputstream.toByteArray();
	}

	int calcCheckSum(byte[] is) {
		int i = 0;
		for (int i_24_ = 0; i_24_ < 4; i_24_++) {
			for (int i_25_ = i_24_; i_25_ < is.length; i_25_ += 4)
				i += is[i_25_];
			if (i_24_ < 3)
				i *= 256;
		}
		return i;
	}

	int uoctet(byte i) {
		return i >= 0 ? (int) i : 256 + i;
	}

	int ushort(byte[] is, int i) {
		return 256 * uoctet(is[i]) + uoctet(is[i + 1]);
	}

	int ulong(byte[] is, int i) {
		return 65536 * ushort(is, i) + ushort(is, i + 2);
	}

	byte[] int2long(int i) {
		byte[] is = new byte[4];
		is[0] = (byte) (i >> 24 & 0xff);
		is[1] = (byte) (i >> 16 & 0xff);
		is[2] = (byte) (i >> 8 & 0xff);
		is[3] = (byte) (i & 0xff);
		return is;
	}

	byte[] int2short(int i) {
		byte[] is = new byte[2];
		is[0] = (byte) (i >> 8 & 0xff);
		is[1] = (byte) (i & 0xff);
		return is;
	}

	private String toHexString(int i) {
		String string = Integer.toHexString(i);
		for (i = string.length(); i < 4; i++)
			string = "0" + string;
		return string;
	}

	public static void main(String[] strings) throws Exception {
		TTFont ttfont = new TTFont(new SeekableFileInputStream(strings[0]), 1);
		ttfont.addGlyph(80);
		ttfont.addGlyph(90);
		ttfont.addGlyph(100);
		FileOutputStream fileoutputstream = new FileOutputStream(strings[1]);
		fileoutputstream.write(ttfont.createSubset());
		fileoutputstream.close();
	}
}