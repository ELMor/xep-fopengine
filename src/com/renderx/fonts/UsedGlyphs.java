/*
 * UsedGlyphs - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.fonts;

public class UsedGlyphs {
	protected byte[] usedGlyphs = new byte[8192];

	public UsedGlyphs() {
		clearGlyphs();
	}

	public void clearGlyphs() {
		for (int i = 0; i < usedGlyphs.length; i++)
			usedGlyphs[i] = (byte) 0;
	}

	public void markGlyph(char c) {
		usedGlyphs[(c & ~0x7) / 8] |= 1 << (c & 0x7);
	}

	public boolean checkGlyph(char c) {
		return (usedGlyphs[(c & ~0x7) / 8] & 1 << (c & 0x7)) != 0;
	}
}