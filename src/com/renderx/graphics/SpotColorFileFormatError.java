/*
 * SpotColorFileFormatError - Decompiled by JODE Visit
 * http://jode.sourceforge.net/
 */
package com.renderx.graphics;

public class SpotColorFileFormatError extends Exception {
	public SpotColorFileFormatError() {
		/* empty */
	}

	public SpotColorFileFormatError(String string) {
		super(string);
	}
}