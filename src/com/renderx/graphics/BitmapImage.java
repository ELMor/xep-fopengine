/*
 * BitmapImage - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics;

import java.io.IOException;
import java.io.OutputStream;

public abstract class BitmapImage extends Image {
	protected final double DEFAULT_RESOLUTION = 120.0;

	protected final double MINIMUM_RESOLUTION = 1.0;

	public int pxWidth = 0;

	public int pxHeight = 0;

	public double horzResolution = 120.0;

	public double vertResolution = 120.0;

	public static final short CM_NONE = 0;

	public static final short CM_RLE = 1;

	public static final short CM_FLATE = 2;

	public static final short CM_JPEG = 3;

	public static final short CM_CCITT3_1D = 4;

	public static final short CM_CCITT3_2D = 5;

	public static final short CM_CCITT4_2D = 6;

	public static final short CM_LZW = 7;

	public static final short CM_LZW_TIFF = 8;

	public short compressionMethod = 0;

	public static final short CS_UNDEFINED = 0;

	public static final short CS_RGB = 1;

	public static final short CS_CMYK = 2;

	public static final short CS_REVERSED_CMYK = 3;

	public static final short CS_GRAYSCALE = 4;

	public static final short CS_REVERSED_GRAYSCALE = 5;

	public static final short CS_INDEXED = 6;

	public static final short CS_HSB = 7;

	public static final short CS_ICCPROFILE = 8;

	public short colorSpace = 0;

	public static final short PREDICTOR_NONE = 0;

	public static final short PREDICTOR_TIFF = 1;

	public static final short PREDICTOR_PNG = 2;

	public short predictor = 0;

	public int bitsPerComponent = 0;

	public boolean usesBigEndian = true;

	public byte[] colorTable = null;

	public byte[] transparentColor = null;

	public boolean canCopyData = false;

	public boolean canExpandData = false;

	public abstract void parse() throws IOException, ImageFormatException;

	protected void setDimensions() {
		if (!(horzResolution >= 1.0))
			horzResolution = 120.0;
		if (!(vertResolution >= 1.0))
			vertResolution = 120.0;
		width = (double) pxWidth / horzResolution * 72.0;
		height = (double) pxHeight / vertResolution * 72.0;
	}

	public abstract void copyData(OutputStream outputstream)
			throws IOException, ImageFormatException;

	public abstract void expandData(OutputStream outputstream)
			throws IOException, ImageFormatException;

	public String toString() {
		return (super.toString() + "," + "pxWidth=" + pxWidth + ","
				+ "pxHeight=" + pxHeight + "," + "horzResolution="
				+ horzResolution + "," + "vertResolution=" + vertResolution);
	}
}