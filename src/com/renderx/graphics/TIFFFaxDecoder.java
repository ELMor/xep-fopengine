/*
 * TIFFFaxDecoder - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics;

public class TIFFFaxDecoder {
	private int bitPointer;

	private int bytePointer;

	private byte[] data;

	private int w;

	private int h;

	private int fillOrder;

	private int changingElemSize = 0;

	private int[] prevChangingElems;

	private int[] currChangingElems;

	private int lastChangingElement = 0;

	private int compression = 2;

	private int uncompressedMode = 0;

	private int fillBits = 0;

	private int oneD;

	static final int[] table1 = { 0, 1, 3, 7, 15, 31, 63, 127, 255 };

	static final int[] table2 = { 0, 128, 192, 224, 240, 248, 252, 254, 255 };

	static final byte[] flipTable = { 0, -128, 64, -64, 32, -96, 96, -32, 16,
			-112, 80, -48, 48, -80, 112, -16, 8, -120, 72, -56, 40, -88, 104,
			-24, 24, -104, 88, -40, 56, -72, 120, -8, 4, -124, 68, -60, 36,
			-92, 100, -28, 20, -108, 84, -44, 52, -76, 116, -12, 12, -116, 76,
			-52, 44, -84, 108, -20, 28, -100, 92, -36, 60, -68, 124, -4, 2,
			-126, 66, -62, 34, -94, 98, -30, 18, -110, 82, -46, 50, -78, 114,
			-14, 10, -118, 74, -54, 42, -86, 106, -22, 26, -102, 90, -38, 58,
			-70, 122, -6, 6, -122, 70, -58, 38, -90, 102, -26, 22, -106, 86,
			-42, 54, -74, 118, -10, 14, -114, 78, -50, 46, -82, 110, -18, 30,
			-98, 94, -34, 62, -66, 126, -2, 1, -127, 65, -63, 33, -95, 97, -31,
			17, -111, 81, -47, 49, -79, 113, -15, 9, -119, 73, -55, 41, -87,
			105, -23, 25, -103, 89, -39, 57, -71, 121, -7, 5, -123, 69, -59,
			37, -91, 101, -27, 21, -107, 85, -43, 53, -75, 117, -11, 13, -115,
			77, -51, 45, -83, 109, -19, 29, -99, 93, -35, 61, -67, 125, -3, 3,
			-125, 67, -61, 35, -93, 99, -29, 19, -109, 83, -45, 51, -77, 115,
			-13, 11, -117, 75, -53, 43, -85, 107, -21, 27, -101, 91, -37, 59,
			-69, 123, -5, 7, -121, 71, -57, 39, -89, 103, -25, 23, -105, 87,
			-41, 55, -73, 119, -9, 15, -113, 79, -49, 47, -81, 111, -17, 31,
			-97, 95, -33, 63, -65, 127, -1 };

	static final short[] white = { 6430, 6400, 6400, 6400, 3225, 3225, 3225,
			3225, 944, 944, 944, 944, 976, 976, 976, 976, 1456, 1456, 1456,
			1456, 1488, 1488, 1488, 1488, 718, 718, 718, 718, 718, 718, 718,
			718, 750, 750, 750, 750, 750, 750, 750, 750, 1520, 1520, 1520,
			1520, 1552, 1552, 1552, 1552, 428, 428, 428, 428, 428, 428, 428,
			428, 428, 428, 428, 428, 428, 428, 428, 428, 654, 654, 654, 654,
			654, 654, 654, 654, 1072, 1072, 1072, 1072, 1104, 1104, 1104, 1104,
			1136, 1136, 1136, 1136, 1168, 1168, 1168, 1168, 1200, 1200, 1200,
			1200, 1232, 1232, 1232, 1232, 622, 622, 622, 622, 622, 622, 622,
			622, 1008, 1008, 1008, 1008, 1040, 1040, 1040, 1040, 44, 44, 44,
			44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 396, 396, 396,
			396, 396, 396, 396, 396, 396, 396, 396, 396, 396, 396, 396, 396,
			1712, 1712, 1712, 1712, 1744, 1744, 1744, 1744, 846, 846, 846, 846,
			846, 846, 846, 846, 1264, 1264, 1264, 1264, 1296, 1296, 1296, 1296,
			1328, 1328, 1328, 1328, 1360, 1360, 1360, 1360, 1392, 1392, 1392,
			1392, 1424, 1424, 1424, 1424, 686, 686, 686, 686, 686, 686, 686,
			686, 910, 910, 910, 910, 910, 910, 910, 910, 1968, 1968, 1968,
			1968, 2000, 2000, 2000, 2000, 2032, 2032, 2032, 2032, 16, 16, 16,
			16, 10257, 10257, 10257, 10257, 12305, 12305, 12305, 12305, 330,
			330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330,
			330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330,
			330, 330, 330, 330, 330, 362, 362, 362, 362, 362, 362, 362, 362,
			362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362,
			362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 878, 878,
			878, 878, 878, 878, 878, 878, 1904, 1904, 1904, 1904, 1936, 1936,
			1936, 1936, -18413, -18413, -16365, -16365, -14317, -14317, -10221,
			-10221, 590, 590, 590, 590, 590, 590, 590, 590, 782, 782, 782, 782,
			782, 782, 782, 782, 1584, 1584, 1584, 1584, 1616, 1616, 1616, 1616,
			1648, 1648, 1648, 1648, 1680, 1680, 1680, 1680, 814, 814, 814, 814,
			814, 814, 814, 814, 1776, 1776, 1776, 1776, 1808, 1808, 1808, 1808,
			1840, 1840, 1840, 1840, 1872, 1872, 1872, 1872, 6157, 6157, 6157,
			6157, 6157, 6157, 6157, 6157, 6157, 6157, 6157, 6157, 6157, 6157,
			6157, 6157, -12275, -12275, -12275, -12275, -12275, -12275, -12275,
			-12275, -12275, -12275, -12275, -12275, -12275, -12275, -12275,
			-12275, 14353, 14353, 14353, 14353, 16401, 16401, 16401, 16401,
			22547, 22547, 24595, 24595, 20497, 20497, 20497, 20497, 18449,
			18449, 18449, 18449, 26643, 26643, 28691, 28691, 30739, 30739,
			-32749, -32749, -30701, -30701, -28653, -28653, -26605, -26605,
			-24557, -24557, -22509, -22509, -20461, -20461, 8207, 8207, 8207,
			8207, 8207, 8207, 8207, 8207, 72, 72, 72, 72, 72, 72, 72, 72, 72,
			72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72,
			72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72,
			72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72,
			72, 72, 72, 72, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104,
			104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104,
			104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104,
			104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104,
			104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104,
			104, 104, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107,
			4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107,
			4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107,
			4107, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266,
			266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266,
			266, 266, 266, 266, 266, 266, 266, 298, 298, 298, 298, 298, 298,
			298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298,
			298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298,
			524, 524, 524, 524, 524, 524, 524, 524, 524, 524, 524, 524, 524,
			524, 524, 524, 556, 556, 556, 556, 556, 556, 556, 556, 556, 556,
			556, 556, 556, 556, 556, 556, 136, 136, 136, 136, 136, 136, 136,
			136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136,
			136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136,
			136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136,
			136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136,
			136, 136, 136, 136, 136, 168, 168, 168, 168, 168, 168, 168, 168,
			168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168,
			168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168,
			168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168,
			168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168,
			168, 168, 168, 168, 460, 460, 460, 460, 460, 460, 460, 460, 460,
			460, 460, 460, 460, 460, 460, 460, 492, 492, 492, 492, 492, 492,
			492, 492, 492, 492, 492, 492, 492, 492, 492, 492, 2059, 2059, 2059,
			2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059,
			2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059,
			2059, 2059, 2059, 2059, 2059, 2059, 2059, 200, 200, 200, 200, 200,
			200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200,
			200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200,
			200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200,
			200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200,
			200, 200, 200, 200, 200, 200, 200, 232, 232, 232, 232, 232, 232,
			232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232,
			232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232,
			232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232,
			232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232,
			232, 232, 232, 232, 232, 232 };

	static final short[] additionalMakeup = { 28679, 28679, 31752, -32759,
			-31735, -30711, -29687, -28663, 29703, 29703, 30727, 30727, -27639,
			-26615, -25591, -24567 };

	static final short[] initBlack = { 3226, 6412, 200, 168, 38, 38, 134, 134,
			100, 100, 100, 100, 68, 68, 68, 68 };

	static final short[] twoBitBlack = { 292, 260, 226, 226 };

	static final short[] black = { 62, 62, 30, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3225,
			3225, 3225, 3225, 3225, 3225, 3225, 3225, 3225, 3225, 3225, 3225,
			3225, 3225, 3225, 3225, 3225, 3225, 3225, 3225, 3225, 3225, 3225,
			3225, 3225, 3225, 3225, 3225, 3225, 3225, 3225, 3225, 588, 588,
			588, 588, 588, 588, 588, 588, 1680, 1680, 20499, 22547, 24595,
			26643, 1776, 1776, 1808, 1808, -24557, -22509, -20461, -18413,
			1904, 1904, 1936, 1936, -16365, -14317, 782, 782, 782, 782, 814,
			814, 814, 814, -12269, -10221, 10257, 10257, 12305, 12305, 14353,
			14353, 16403, 18451, 1712, 1712, 1744, 1744, 28691, 30739, -32749,
			-30701, -28653, -26605, 2061, 2061, 2061, 2061, 2061, 2061, 2061,
			2061, 424, 424, 424, 424, 424, 424, 424, 424, 424, 424, 424, 424,
			424, 424, 424, 424, 424, 424, 424, 424, 424, 424, 424, 424, 424,
			424, 424, 424, 424, 424, 424, 424, 750, 750, 750, 750, 1616, 1616,
			1648, 1648, 1424, 1424, 1456, 1456, 1488, 1488, 1520, 1520, 1840,
			1840, 1872, 1872, 1968, 1968, 8209, 8209, 524, 524, 524, 524, 524,
			524, 524, 524, 556, 556, 556, 556, 556, 556, 556, 556, 1552, 1552,
			1584, 1584, 2000, 2000, 2032, 2032, 976, 976, 1008, 1008, 1040,
			1040, 1072, 1072, 1296, 1296, 1328, 1328, 718, 718, 718, 718, 456,
			456, 456, 456, 456, 456, 456, 456, 456, 456, 456, 456, 456, 456,
			456, 456, 456, 456, 456, 456, 456, 456, 456, 456, 456, 456, 456,
			456, 456, 456, 456, 456, 326, 326, 326, 326, 326, 326, 326, 326,
			326, 326, 326, 326, 326, 326, 326, 326, 326, 326, 326, 326, 326,
			326, 326, 326, 326, 326, 326, 326, 326, 326, 326, 326, 326, 326,
			326, 326, 326, 326, 326, 326, 326, 326, 326, 326, 326, 326, 326,
			326, 326, 326, 326, 326, 326, 326, 326, 326, 326, 326, 326, 326,
			326, 326, 326, 326, 358, 358, 358, 358, 358, 358, 358, 358, 358,
			358, 358, 358, 358, 358, 358, 358, 358, 358, 358, 358, 358, 358,
			358, 358, 358, 358, 358, 358, 358, 358, 358, 358, 358, 358, 358,
			358, 358, 358, 358, 358, 358, 358, 358, 358, 358, 358, 358, 358,
			358, 358, 358, 358, 358, 358, 358, 358, 358, 358, 358, 358, 358,
			358, 358, 358, 490, 490, 490, 490, 490, 490, 490, 490, 490, 490,
			490, 490, 490, 490, 490, 490, 4113, 4113, 6161, 6161, 848, 848,
			880, 880, 912, 912, 944, 944, 622, 622, 622, 622, 654, 654, 654,
			654, 1104, 1104, 1136, 1136, 1168, 1168, 1200, 1200, 1232, 1232,
			1264, 1264, 686, 686, 686, 686, 1360, 1360, 1392, 1392, 12, 12, 12,
			12, 12, 12, 12, 12, 390, 390, 390, 390, 390, 390, 390, 390, 390,
			390, 390, 390, 390, 390, 390, 390, 390, 390, 390, 390, 390, 390,
			390, 390, 390, 390, 390, 390, 390, 390, 390, 390, 390, 390, 390,
			390, 390, 390, 390, 390, 390, 390, 390, 390, 390, 390, 390, 390,
			390, 390, 390, 390, 390, 390, 390, 390, 390, 390, 390, 390, 390,
			390, 390, 390 };

	static final byte[] twoDCodes = { 80, 88, 23, 71, 30, 30, 62, 62, 4, 4, 4,
			4, 4, 4, 4, 4, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,
			11, 11, 11, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35,
			35, 35, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51,
			51, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41,
			41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41,
			41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41,
			41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41 };

	public TIFFFaxDecoder(int i, int i_0_, int i_1_) {
		fillOrder = i;
		w = i_0_;
		h = i_1_;
		bitPointer = 0;
		bytePointer = 0;
		prevChangingElems = new int[i_0_];
		currChangingElems = new int[i_0_];
	}

	public void decode1D(byte[] is, byte[] is_2_, int i, int i_3_)
			throws ImageFormatException {
		data = is_2_;
		int i_4_ = 0;
		int i_5_ = (w + 7) / 8;
		bitPointer = 0;
		bytePointer = 0;
		for (int i_6_ = 0; i_6_ < i_3_; i_6_++) {
			decodeNextScanline(is, i_4_, i);
			i_4_ += i_5_;
		}
	}

	public void decodeNextScanline(byte[] is, int i, int i_7_)
			throws ImageFormatException {
		boolean bool = false;
		boolean bool_8_ = false;
		boolean bool_9_ = false;
		boolean bool_10_ = true;
		boolean bool_11_ = false;
		changingElemSize = 0;
		while (i_7_ < w) {
			while (bool_10_) {
				int i_12_ = nextNBits(10);
				int i_13_ = white[i_12_];
				int i_14_ = i_13_ & 0x1;
				int i_15_ = i_13_ >>> 1 & 0xf;
				if (i_15_ == 12) {
					int i_16_ = nextLesserThan8Bits(2);
					i_12_ = i_12_ << 2 & 0xc | i_16_;
					i_13_ = additionalMakeup[i_12_];
					i_15_ = i_13_ >>> 1 & 0x7;
					int i_17_ = i_13_ >>> 4 & 0xfff;
					i_7_ += i_17_;
					updatePointer(4 - i_15_);
				} else {
					if (i_15_ == 0)
						throw new ImageFormatException(
								"Invalid code encountered");
					if (i_15_ == 15)
						throw new ImageFormatException(
								"EOL code word encountered in White run");
					int i_18_ = i_13_ >>> 5 & 0x7ff;
					i_7_ += i_18_;
					updatePointer(10 - i_15_);
					if (i_14_ == 0) {
						bool_10_ = false;
						currChangingElems[changingElemSize++] = i_7_;
					}
				}
			}
			if (i_7_ == w) {
				if (compression == 2)
					advancePointer();
				break;
			}
			while (!bool_10_) {
				int i_19_ = nextLesserThan8Bits(4);
				int i_20_ = initBlack[i_19_];
				int i_21_ = i_20_ & 0x1;
				int i_22_ = i_20_ >>> 1 & 0xf;
				int i_23_ = i_20_ >>> 5 & 0x7ff;
				if (i_23_ == 100) {
					i_19_ = nextNBits(9);
					i_20_ = black[i_19_];
					i_21_ = i_20_ & 0x1;
					i_22_ = i_20_ >>> 1 & 0xf;
					i_23_ = i_20_ >>> 5 & 0x7ff;
					if (i_22_ == 12) {
						updatePointer(5);
						i_19_ = nextLesserThan8Bits(4);
						i_20_ = additionalMakeup[i_19_];
						i_22_ = i_20_ >>> 1 & 0x7;
						i_23_ = i_20_ >>> 4 & 0xfff;
						setToBlack(is, i, i_7_, i_23_);
						i_7_ += i_23_;
						updatePointer(4 - i_22_);
					} else {
						if (i_22_ == 15)
							throw new ImageFormatException(
									"EOL code word encountered in Black run");
						setToBlack(is, i, i_7_, i_23_);
						i_7_ += i_23_;
						updatePointer(9 - i_22_);
						if (i_21_ == 0) {
							bool_10_ = true;
							currChangingElems[changingElemSize++] = i_7_;
						}
					}
				} else if (i_23_ == 200) {
					i_19_ = nextLesserThan8Bits(2);
					i_20_ = twoBitBlack[i_19_];
					i_23_ = i_20_ >>> 5 & 0x7ff;
					i_22_ = i_20_ >>> 1 & 0xf;
					setToBlack(is, i, i_7_, i_23_);
					i_7_ += i_23_;
					updatePointer(2 - i_22_);
					bool_10_ = true;
					currChangingElems[changingElemSize++] = i_7_;
				} else {
					setToBlack(is, i, i_7_, i_23_);
					i_7_ += i_23_;
					updatePointer(4 - i_22_);
					bool_10_ = true;
					currChangingElems[changingElemSize++] = i_7_;
				}
			}
			if (i_7_ == w) {
				if (compression == 2)
					advancePointer();
				break;
			}
		}
		currChangingElems[changingElemSize++] = i_7_;
	}

	public void decode2D(byte[] is, byte[] is_24_, int i, int i_25_, long l)
			throws ImageFormatException {
		data = is_24_;
		compression = 3;
		bitPointer = 0;
		bytePointer = 0;
		int i_26_ = (w + 7) / 8;
		int[] is_27_ = new int[2];
		boolean bool = false;
		oneD = (int) (l & 0x1L);
		uncompressedMode = (int) ((l & 0x2L) >> 1);
		fillBits = (int) ((l & 0x4L) >> 2);
		if (readEOL(true) != 1)
			throw new ImageFormatException("First scanline must be 1D encoded");
		int i_28_ = 0;
		decodeNextScanline(is, i_28_, i);
		i_28_ += i_26_;
		for (int i_29_ = 1; i_29_ < i_25_; i_29_++) {
			if (readEOL(false) == 0) {
				int[] is_30_ = prevChangingElems;
				prevChangingElems = currChangingElems;
				currChangingElems = is_30_;
				int i_31_ = 0;
				int i_32_ = -1;
				boolean bool_33_ = true;
				int i_34_ = i;
				lastChangingElement = 0;
				while (i_34_ < w) {
					getNextChangingElement(i_32_, bool_33_, is_27_);
					int i_35_ = is_27_[0];
					int i_36_ = is_27_[1];
					int i_37_ = nextLesserThan8Bits(7);
					i_37_ = twoDCodes[i_37_] & 0xff;
					int i_38_ = (i_37_ & 0x78) >>> 3;
					int i_39_ = i_37_ & 0x7;
					if (i_38_ == 0) {
						if (!bool_33_)
							setToBlack(is, i_28_, i_34_, i_36_ - i_34_);
						i_34_ = i_32_ = i_36_;
						updatePointer(7 - i_39_);
					} else if (i_38_ == 1) {
						updatePointer(7 - i_39_);
						if (bool_33_) {
							int i_40_ = decodeWhiteCodeWord();
							i_34_ += i_40_;
							currChangingElems[i_31_++] = i_34_;
							i_40_ = decodeBlackCodeWord();
							setToBlack(is, i_28_, i_34_, i_40_);
							i_34_ += i_40_;
							currChangingElems[i_31_++] = i_34_;
						} else {
							int i_41_ = decodeBlackCodeWord();
							setToBlack(is, i_28_, i_34_, i_41_);
							i_34_ += i_41_;
							currChangingElems[i_31_++] = i_34_;
							i_41_ = decodeWhiteCodeWord();
							i_34_ += i_41_;
							currChangingElems[i_31_++] = i_34_;
						}
						i_32_ = i_34_;
					} else if (i_38_ <= 8) {
						int i_42_ = i_35_ + (i_38_ - 5);
						currChangingElems[i_31_++] = i_42_;
						if (!bool_33_)
							setToBlack(is, i_28_, i_34_, i_42_ - i_34_);
						i_34_ = i_32_ = i_42_;
						bool_33_ = !bool_33_;
						updatePointer(7 - i_39_);
					} else
						throw new ImageFormatException(
								"Invalid code encountered while decoding 2D group 3 compressed data");
				}
				currChangingElems[i_31_++] = i_34_;
				changingElemSize = i_31_;
			} else
				decodeNextScanline(is, i_28_, i);
			i_28_ += i_26_;
		}
	}

	public synchronized void decodeT6(byte[] is, byte[] is_43_, int i,
			int i_44_, long l) throws ImageFormatException {
		data = is_43_;
		compression = 4;
		bitPointer = 0;
		bytePointer = 0;
		int i_45_ = (w + 7) / 8;
		boolean bool = false;
		int[] is_46_ = new int[2];
		uncompressedMode = (int) ((l & 0x2L) >> 1);
		int[] is_47_ = currChangingElems;
		changingElemSize = 0;
		is_47_[changingElemSize++] = w;
		is_47_[changingElemSize++] = w;
		int i_48_ = 0;
		for (int i_49_ = 0; i_49_ < i_44_; i_49_++) {
			int i_50_ = -1;
			boolean bool_51_ = true;
			int[] is_52_ = prevChangingElems;
			prevChangingElems = currChangingElems;
			is_47_ = currChangingElems = is_52_;
			int i_53_ = 0;
			int i_54_ = i;
			lastChangingElement = 0;
			while (i_54_ < w) {
				getNextChangingElement(i_50_, bool_51_, is_46_);
				int i_55_ = is_46_[0];
				int i_56_ = is_46_[1];
				int i_57_ = nextLesserThan8Bits(7);
				i_57_ = twoDCodes[i_57_] & 0xff;
				int i_58_ = (i_57_ & 0x78) >>> 3;
				int i_59_ = i_57_ & 0x7;
				if (i_58_ == 0) {
					if (!bool_51_)
						setToBlack(is, i_48_, i_54_, i_56_ - i_54_);
					i_54_ = i_50_ = i_56_;
					updatePointer(7 - i_59_);
				} else if (i_58_ == 1) {
					updatePointer(7 - i_59_);
					if (bool_51_) {
						int i_60_ = decodeWhiteCodeWord();
						i_54_ += i_60_;
						is_47_[i_53_++] = i_54_;
						i_60_ = decodeBlackCodeWord();
						setToBlack(is, i_48_, i_54_, i_60_);
						i_54_ += i_60_;
						is_47_[i_53_++] = i_54_;
					} else {
						int i_61_ = decodeBlackCodeWord();
						setToBlack(is, i_48_, i_54_, i_61_);
						i_54_ += i_61_;
						is_47_[i_53_++] = i_54_;
						i_61_ = decodeWhiteCodeWord();
						i_54_ += i_61_;
						is_47_[i_53_++] = i_54_;
					}
					i_50_ = i_54_;
				} else if (i_58_ <= 8) {
					int i_62_ = i_55_ + (i_58_ - 5);
					is_47_[i_53_++] = i_62_;
					if (!bool_51_)
						setToBlack(is, i_48_, i_54_, i_62_ - i_54_);
					i_54_ = i_50_ = i_62_;
					bool_51_ = !bool_51_;
					updatePointer(7 - i_59_);
				} else if (i_58_ == 11) {
					if (nextLesserThan8Bits(3) != 7)
						throw new ImageFormatException(
								"Invalid code encountered while decoding 2D group 4 compressed data");
					int i_63_ = 0;
					boolean bool_64_ = false;
					while (!bool_64_) {
						while (nextLesserThan8Bits(1) != 1)
							i_63_++;
						if (i_63_ > 5) {
							i_63_ -= 6;
							if (!bool_51_ && i_63_ > 0)
								is_47_[i_53_++] = i_54_;
							i_54_ += i_63_;
							if (i_63_ > 0)
								bool_51_ = true;
							if (nextLesserThan8Bits(1) == 0) {
								if (!bool_51_)
									is_47_[i_53_++] = i_54_;
								bool_51_ = true;
							} else {
								if (bool_51_)
									is_47_[i_53_++] = i_54_;
								bool_51_ = false;
							}
							bool_64_ = true;
						}
						if (i_63_ == 5) {
							if (!bool_51_)
								is_47_[i_53_++] = i_54_;
							i_54_ += i_63_;
							bool_51_ = true;
						} else {
							i_54_ += i_63_;
							is_47_[i_53_++] = i_54_;
							setToBlack(is, i_48_, i_54_, 1);
							i_54_++;
							bool_51_ = false;
						}
					}
				} else
					throw new ImageFormatException(
							"Invalid code encountered while decoding 2D group 4 compressed data");
			}
			is_47_[i_53_++] = i_54_;
			changingElemSize = i_53_;
			i_48_ += i_45_;
		}
	}

	private void setToBlack(byte[] is, int i, int i_65_, int i_66_) {
		int i_67_ = 8 * i + i_65_;
		int i_68_ = i_67_ + i_66_;
		int i_69_ = i_67_ >> 3;
		int i_70_ = i_67_ & 0x7;
		if (i_70_ > 0) {
			int i_71_ = 1 << 7 - i_70_;
			byte i_72_ = is[i_69_];
			for (/**/; i_71_ > 0 && i_67_ < i_68_; i_67_++) {
				i_72_ |= i_71_;
				i_71_ >>= 1;
			}
			is[i_69_] = i_72_;
		}
		i_69_ = i_67_ >> 3;
		for (/**/; i_67_ < i_68_ - 7; i_67_ += 8)
			is[i_69_++] = (byte) -1;
		for (/**/; i_67_ < i_68_; i_67_++) {
			i_69_ = i_67_ >> 3;
			is[i_69_] |= 1 << 7 - (i_67_ & 0x7);
		}
	}

	private int decodeWhiteCodeWord() throws ImageFormatException {
		int i = -1;
		int i_73_ = 0;
		boolean bool = true;
		while (bool) {
			int i_74_ = nextNBits(10);
			int i_75_ = white[i_74_];
			int i_76_ = i_75_ & 0x1;
			int i_77_ = i_75_ >>> 1 & 0xf;
			if (i_77_ == 12) {
				int i_78_ = nextLesserThan8Bits(2);
				i_74_ = i_74_ << 2 & 0xc | i_78_;
				i_75_ = additionalMakeup[i_74_];
				i_77_ = i_75_ >>> 1 & 0x7;
				i = i_75_ >>> 4 & 0xfff;
				i_73_ += i;
				updatePointer(4 - i_77_);
			} else {
				if (i_77_ == 0)
					throw new ImageFormatException("Invalid code encountered");
				if (i_77_ == 15)
					throw new ImageFormatException(
							"EOL code word encountered in White run");
				i = i_75_ >>> 5 & 0x7ff;
				i_73_ += i;
				updatePointer(10 - i_77_);
				if (i_76_ == 0)
					bool = false;
			}
		}
		return i_73_;
	}

	private int decodeBlackCodeWord() throws ImageFormatException {
		int i = -1;
		int i_79_ = 0;
		boolean bool = false;
		while (!bool) {
			int i_80_ = nextLesserThan8Bits(4);
			int i_81_ = initBlack[i_80_];
			int i_82_ = i_81_ & 0x1;
			int i_83_ = i_81_ >>> 1 & 0xf;
			i = i_81_ >>> 5 & 0x7ff;
			if (i == 100) {
				i_80_ = nextNBits(9);
				i_81_ = black[i_80_];
				i_82_ = i_81_ & 0x1;
				i_83_ = i_81_ >>> 1 & 0xf;
				i = i_81_ >>> 5 & 0x7ff;
				if (i_83_ == 12) {
					updatePointer(5);
					i_80_ = nextLesserThan8Bits(4);
					i_81_ = additionalMakeup[i_80_];
					i_83_ = i_81_ >>> 1 & 0x7;
					i = i_81_ >>> 4 & 0xfff;
					i_79_ += i;
					updatePointer(4 - i_83_);
				} else {
					if (i_83_ == 15)
						throw new ImageFormatException(
								"EOL code word encountered in Black run");
					i_79_ += i;
					updatePointer(9 - i_83_);
					if (i_82_ == 0)
						bool = true;
				}
			} else if (i == 200) {
				i_80_ = nextLesserThan8Bits(2);
				i_81_ = twoBitBlack[i_80_];
				i = i_81_ >>> 5 & 0x7ff;
				i_79_ += i;
				i_83_ = i_81_ >>> 1 & 0xf;
				updatePointer(2 - i_83_);
				bool = true;
			} else {
				i_79_ += i;
				updatePointer(4 - i_83_);
				bool = true;
			}
		}
		return i_79_;
	}

	private int readEOL(boolean bool) throws ImageFormatException {
		if (fillBits == 0) {
			int i = nextNBits(12);
			if (bool && i == 0 && nextNBits(4) == 1) {
				fillBits = 1;
				return 1;
			}
			if (i != 1)
				throw new ImageFormatException(
						"Scanline must begin with EOL code word");
		} else if (fillBits == 1) {
			int i = 8 - bitPointer;
			if (nextNBits(i) != 0)
				throw new ImageFormatException(
						"All fill bits preceding EOL code must be 0");
			if (i < 4 && nextNBits(8) != 0)
				throw new ImageFormatException(
						"All fill bits preceding EOL code must be 0");
			int i_84_;
			while ((i_84_ = nextNBits(8)) != 1) {
				if (i_84_ != 0)
					throw new ImageFormatException(
							"All fill bits preceding EOL code must be 0");
			}
		}
		if (oneD == 0)
			return 1;
		return nextLesserThan8Bits(1);
	}

	private void getNextChangingElement(int i, boolean bool, int[] is) {
		int[] is_85_ = prevChangingElems;
		int i_86_ = changingElemSize;
		int i_87_ = lastChangingElement > 0 ? lastChangingElement - 1 : 0;
		if (bool)
			i_87_ &= ~0x1;
		else
			i_87_ |= 0x1;
		int i_88_;
		for (i_88_ = i_87_; i_88_ < i_86_; i_88_ += 2) {
			int i_89_ = is_85_[i_88_];
			if (i_89_ > i) {
				lastChangingElement = i_88_;
				is[0] = i_89_;
				break;
			}
		}
		if (i_88_ + 1 < i_86_)
			is[1] = is_85_[i_88_ + 1];
	}

	private int nextNBits(int i) throws ImageFormatException {
		int i_90_ = data.length - 1;
		int i_91_ = bytePointer;
		int i_92_;
		int i_93_;
		int i_94_;
		if (fillOrder == 1) {
			i_92_ = data[i_91_];
			if (i_91_ == i_90_) {
				i_93_ = 0;
				i_94_ = 0;
			} else if (i_91_ + 1 == i_90_) {
				i_93_ = data[i_91_ + 1];
				i_94_ = 0;
			} else {
				i_93_ = data[i_91_ + 1];
				i_94_ = data[i_91_ + 2];
			}
		} else if (fillOrder == 2) {
			i_92_ = flipTable[data[i_91_] & 0xff];
			if (i_91_ == i_90_) {
				i_93_ = 0;
				i_94_ = 0;
			} else if (i_91_ + 1 == i_90_) {
				i_93_ = flipTable[data[i_91_ + 1] & 0xff];
				i_94_ = 0;
			} else {
				i_93_ = flipTable[data[i_91_ + 1] & 0xff];
				i_94_ = flipTable[data[i_91_ + 2] & 0xff];
			}
		} else
			throw new ImageFormatException(
					"FillOrder tag must be either 1 or 2");
		int i_95_ = 8 - bitPointer;
		int i_96_ = i - i_95_;
		int i_97_ = 0;
		if (i_96_ > 8) {
			i_97_ = i_96_ - 8;
			i_96_ = 8;
		}
		bytePointer++;
		int i_98_ = (i_92_ & table1[i_95_]) << i - i_95_;
		int i_99_ = (i_93_ & table2[i_96_]) >>> 8 - i_96_;
		boolean bool = false;
		if (i_97_ != 0) {
			i_99_ <<= i_97_;
			int i_100_ = (i_94_ & table2[i_97_]) >>> 8 - i_97_;
			i_99_ |= i_100_;
			bytePointer++;
			bitPointer = i_97_;
		} else if (i_96_ == 8) {
			bitPointer = 0;
			bytePointer++;
		} else
			bitPointer = i_96_;
		int i_101_ = i_98_ | i_99_;
		return i_101_;
	}

	private int nextLesserThan8Bits(int i) throws ImageFormatException {
		int i_102_ = data.length - 1;
		int i_103_ = bytePointer;
		int i_104_;
		int i_105_;
		if (fillOrder == 1) {
			i_104_ = data[i_103_];
			if (i_103_ == i_102_)
				i_105_ = 0;
			else
				i_105_ = data[i_103_ + 1];
		} else if (fillOrder == 2) {
			i_104_ = flipTable[data[i_103_] & 0xff];
			if (i_103_ == i_102_)
				i_105_ = 0;
			else
				i_105_ = flipTable[data[i_103_ + 1] & 0xff];
		} else
			throw new ImageFormatException(
					"FillOrder tag must be either 1 or 2");
		int i_106_ = 8 - bitPointer;
		int i_107_ = i - i_106_;
		int i_108_ = i_106_ - i;
		int i_109_;
		if (i_108_ >= 0) {
			i_109_ = (i_104_ & table1[i_106_]) >>> i_108_;
			bitPointer += i;
			if (bitPointer == 8) {
				bitPointer = 0;
				bytePointer++;
			}
		} else {
			i_109_ = (i_104_ & table1[i_106_]) << -i_108_;
			int i_110_ = (i_105_ & table2[i_107_]) >>> 8 - i_107_;
			i_109_ |= i_110_;
			bytePointer++;
			bitPointer = i_107_;
		}
		return i_109_;
	}

	private void updatePointer(int i) {
		int i_111_ = bitPointer - i;
		if (i_111_ < 0) {
			bytePointer--;
			bitPointer = 8 + i_111_;
		} else
			bitPointer = i_111_;
	}

	private boolean advancePointer() {
		if (bitPointer != 0) {
			bytePointer++;
			bitPointer = 0;
		}
		return true;
	}
}