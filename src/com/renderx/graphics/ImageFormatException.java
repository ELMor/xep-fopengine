/*
 * ImageFormatException - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics;

public class ImageFormatException extends Exception {
	public String mimetype = null;

	public ImageFormatException() {
		/* empty */
	}

	public ImageFormatException(String string) {
		super(string);
	}

	public ImageFormatException(String string, String string_0_) {
		super(string);
		mimetype = string_0_;
	}
}