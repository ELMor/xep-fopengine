/*
 * TIFFImage - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics;

import java.io.ByteArrayOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.renderx.util.Hashtable;
import com.renderx.util.LZWDecoderStream;
import com.renderx.util.SeekableInput;

public class TIFFImage extends BitmapImage {
	public long offsetAdjustment = 0L;

	private long t4options = 0L;

	private long t6options = 0L;

	private int fillOrder = 1;

	private int samplesPerPixel = 1;

	private boolean isTiled = false;

	private int tileWidth = 0;

	private int tileHeight = 0;

	private int rowsPerStrip = 0;

	public static final Integer TIFF_TAG_IMAGE_WIDTH = new Integer(256);

	public static final Integer TIFF_TAG_IMAGE_LENGTH = new Integer(257);

	public static final Integer TIFF_TAG_BITS_PER_SAMPLE = new Integer(258);

	public static final Integer TIFF_TAG_COMPRESSION = new Integer(259);

	public static final Integer TIFF_TAG_PHOTOMETRIC_INTERPRETATION = new Integer(
			262);

	public static final Integer TIFF_TAG_FILL_ORDER = new Integer(266);

	public static final Integer TIFF_TAG_STRIP_OFFSETS = new Integer(273);

	public static final Integer TIFF_TAG_ORIENTATION = new Integer(274);

	public static final Integer TIFF_TAG_SAMPLES_PER_PIXEL = new Integer(277);

	public static final Integer TIFF_TAG_ROWS_PER_STRIP = new Integer(278);

	public static final Integer TIFF_TAG_STRIP_BYTE_COUNTS = new Integer(279);

	public static final Integer TIFF_TAG_X_RESOLUTION = new Integer(282);

	public static final Integer TIFF_TAG_Y_RESOLUTION = new Integer(283);

	public static final Integer TIFF_TAG_PLANAR_CONFIGURATION = new Integer(284);

	public static final Integer TIFF_TAG_T4_OPTIONS = new Integer(292);

	public static final Integer TIFF_TAG_T6_OPTIONS = new Integer(293);

	public static final Integer TIFF_TAG_RESOLUTION_UNIT = new Integer(296);

	public static final Integer TIFF_TAG_PREDICTOR = new Integer(317);

	public static final Integer TIFF_TAG_COLORMAP = new Integer(320);

	public static final Integer TIFF_TAG_TILE_WIDTH = new Integer(322);

	public static final Integer TIFF_TAG_TILE_LENGTH = new Integer(323);

	public static final Integer TIFF_TAG_TILE_OFFSETS = new Integer(324);

	public static final Integer TIFF_TAG_TILE_BYTE_COUNTS = new Integer(325);

	public static final Integer TIFF_TAG_EXTRA_SAMPLES = new Integer(338);

	public static final Integer TIFF_TAG_SAMPLE_FORMAT = new Integer(339);

	public static final Integer TIFF_TAG_S_MIN_SAMPLE_VALUE = new Integer(340);

	public static final Integer TIFF_TAG_S_MAX_SAMPLE_VALUE = new Integer(341);

	public static final Hashtable tagNames = new Hashtable();

	public static final int TIFF_DATATYPE_BYTE = 1;

	public static final int TIFF_DATATYPE_ASCII = 2;

	public static final int TIFF_DATATYPE_SHORT = 3;

	public static final int TIFF_DATATYPE_LONG = 4;

	public static final int TIFF_DATATYPE_RATIONAL = 5;

	public static final int TIFF_DATATYPE_SBYTE = 6;

	public static final int TIFF_DATATYPE_UNDEFINED = 7;

	public static final int TIFF_DATATYPE_SSHORT = 8;

	public static final int TIFF_DATATYPE_SLONG = 9;

	public static final int TIFF_DATATYPE_SRATIONAL = 10;

	public static final int TIFF_DATATYPE_FLOAT = 11;

	public static final int TIFF_DATATYPE_DOUBLE = 12;

	public static final String[] dataTypeNames;

	public static final int TIFF_COMPRESSION_NONE = 1;

	public static final int TIFF_COMPRESSION_FAX_G3_1D = 2;

	public static final int TIFF_COMPRESSION_FAX_G3_2D = 3;

	public static final int TIFF_COMPRESSION_FAX_G4_2D = 4;

	public static final int TIFF_COMPRESSION_LZW = 5;

	public static final int TIFF_COMPRESSION_PACKBITS = 32773;

	private static final int TIFF_IMAGETYPE_REVERSE_MONOCHROME = 0;

	private static final int TIFF_IMAGETYPE_MONOCHROME = 1;

	private static final int TIFF_IMAGETYPE_RGB = 2;

	private static final int TIFF_IMAGETYPE_PALETTE = 3;

	private static final int TIFF_IMAGETYPE_TRANSPARENCY_MASK = 4;

	private static final int TIFF_IMAGETYPE_SEPARATED = 5;

	public static final String[] imageTypeNames;

	public static final int UNIT_NONE = 1;

	public static final int UNIT_IN = 2;

	public static final int UNIT_CM = 3;

	public static final int INTERLEAVED_PLANES = 1;

	public static final int SEPARATE_PLANES = 2;

	public static final int TIFF_PREDICTOR_NONE = 1;

	public static final int TIFF_PREDICTOR_DIFFERENCING = 2;

	public static final int TIFF_NORMAL_FILL_ORDER = 1;

	public static final int TIFF_REVERSED_FILL_ORDER = 2;

	Hashtable ifd = new Hashtable();

	protected class PredictorFilter extends FilterOutputStream {
		int code = 0;

		int codeMask = 0;

		int inBits = 0;

		int outBits = 0;

		int bitsPerPixel = 0;

		int inBitCount = 0;

		int outBitCount = 0;

		int fragWidth = 0;

		int xpos = 0;

		int[] components = null;

		PredictorFilter(OutputStream outputstream, int i) {
			super(outputstream);
			fragWidth = i;
			components = new int[samplesPerPixel];
			bitsPerPixel = samplesPerPixel * bitsPerComponent;
			codeMask = (1 << bitsPerComponent) - 1;
		}

		public void write(byte[] is) throws IOException {
			for (int i = 0; i < is.length; i++)
				write(is[i] & 0xff);
		}

		public void write(byte[] is, int i, int i_0_) throws IOException {
			for (int i_1_ = 0; i_1_ < i_0_; i_1_++)
				write(is[i + i_1_] & 0xff);
		}

		public void write(int i) throws IOException {
			inBits = inBits << 8 | i;
			inBitCount += 8;
			while (inBitCount >= bitsPerPixel) {
				for (int i_2_ = 0; i_2_ < samplesPerPixel; i_2_++) {
					code = inBits >> inBitCount - bitsPerComponent;
					inBitCount -= bitsPerComponent;
					inBits &= (1 << inBitCount) - 1;
					components[i_2_] = xpos == 0 ? code : components[i_2_]
							+ code;
					outBits |= outBits << bitsPerComponent | components[i_2_];
					outBitCount += bitsPerComponent;
					if (outBitCount == 8) {
						out.write(outBits);
						outBits = 0;
						outBitCount = 0;
					}
				}
				if (++xpos == fragWidth)
					xpos = 0;
			}
		}
	}

	class IFDEntry {
		int tag;

		int type;

		int count;

		long offset;

		byte[] value;

		public IFDEntry(int i, int i_3_, int i_4_, byte[] is) {
			tag = i;
			type = i_3_;
			count = i_4_;
			value = is;
			offset = (usesBigEndian ? (long) ((is[0] << 24 & ~0xffffff)
					+ (is[1] << 16 & 0xff0000) + (is[2] << 8 & 0xff00) + (is[3] & 0xff))
					: (long) ((is[3] << 24 & ~0xffffff)
							+ (is[2] << 16 & 0xff0000) + (is[1] << 8 & 0xff00) + (is[0] & 0xff)));
			offset &= 0xffffffffL;
			ifd.put(new Integer(i), this);
		}

		public long getInteger() throws ImageFormatException {
			if (count != 1)
				throw new ImageFormatException(
						"Array found instead of scalar in TIFF image "
								+ TIFFImage.this.toDisplayString() + ", tag "
								+ TIFFImage.tagNames.get(new Integer(tag))
								+ " [" + tag + "]");
			switch (type) {
			case 1:
				return (usesBigEndian ? offset >> 24 : offset) & 0xffL;
			case 3:
				return (usesBigEndian ? offset >> 16 : offset) & 0xffffL;
			case 4:
				return offset & 0xffffffffL;
			default:
				throw new ImageFormatException("Unexpected data type "
						+ TIFFImage.dataTypeNames[type] + " (" + type
						+ ") in TIFF image " + TIFFImage.this.toDisplayString()
						+ ", tag " + TIFFImage.tagNames.get(new Integer(tag))
						+ " [" + tag + "]");
			}
		}

		public double getRational(SeekableInput seekableinput)
				throws IOException, ImageFormatException {
			if (count != 1)
				throw new ImageFormatException(
						"Array found instead of scalar in TIFF image "
								+ TIFFImage.this.toDisplayString() + ", tag "
								+ TIFFImage.tagNames.get(new Integer(tag))
								+ " [" + tag + "]");
			if (type != 5)
				throw new ImageFormatException("Unexpected data type "
						+ TIFFImage.dataTypeNames[type] + " (" + type
						+ ") in TIFF image " + TIFFImage.this.toDisplayString()
						+ ", tag " + TIFFImage.tagNames.get(new Integer(tag))
						+ " [" + tag + "]");
			seekableinput.seek(offset + offsetAdjustment);
			long l = readUnsignedInt(seekableinput);
			long l_5_ = readUnsignedInt(seekableinput);
			if (l_5_ == 0L)
				throw new ImageFormatException(
						"Division by zero in RATIONAL data in TIFF image "
								+ TIFFImage.this.toDisplayString() + ", tag "
								+ TIFFImage.tagNames.get(new Integer(tag))
								+ " [" + tag + "]");
			return (double) l / (double) l_5_;
		}

		public int[] getIntegerArray(SeekableInput seekableinput)
				throws IOException, ImageFormatException {
			int[] is = new int[count];
			switch (type) {
			case 1:
				if (count <= 4) {
					for (int i = 0; i < count; i++)
						is[i] = value[i] & 0xff;
				} else {
					seekableinput.seek(offset + offsetAdjustment);
					for (int i = 0; i < count; i++)
						is[i] = seekableinput.readUnsignedByte();
				}
				break;
			case 3:
				if (count <= 2) {
					for (int i = 0; i < count; i++)
						is[i] = (usesBigEndian ? ((value[2 * i] << 8 & 0xff00) + (value[2 * i + 1] & 0xff))
								: ((value[2 * i + 1] << 8 & 0xff00) + (value[2 * i] & 0xff)));
				} else {
					seekableinput.seek(offset + offsetAdjustment);
					for (int i = 0; i < count; i++)
						is[i] = readUnsignedShort(seekableinput);
				}
				break;
			case 4:
				if (count == 1)
					is[0] = (int) offset;
				else {
					seekableinput.seek(offset + offsetAdjustment);
					for (int i = 0; i < count; i++)
						is[i] = readInt(seekableinput);
				}
				break;
			default:
				throw new ImageFormatException(
						"Not an integer data type in TIFF image "
								+ TIFFImage.this.toDisplayString() + ", tag "
								+ TIFFImage.tagNames.get(new Integer(tag))
								+ " [" + tag + "]");
			}
			return is;
		}
	}

	public TIFFImage() {
		mimetype = "image/tiff";
	}

	public TIFFImage(Image image, long l) {
		this();
		source = image.source;
		factory = image.factory;
		offsetAdjustment = l;
	}

	public void parse() throws IOException, ImageFormatException {
		SeekableInput seekableinput = this.openSeekableImageStream();
		try {
			seekableinput.seek(offsetAdjustment);
			byte[] is = new byte[2];
			seekableinput.readFully(is);
			if (is[0] == 73 && is[1] == 73)
				usesBigEndian = false;
			else if (is[0] == 77 && is[1] == 77)
				usesBigEndian = true;
			else
				throw new ImageFormatException(
						"Invalid byte order mark in TIFF image "
								+ this.toDisplayString());
			if (readShort(seekableinput) != 42)
				throw new ImageFormatException(
						"Invalid signature in TIFF image "
								+ this.toDisplayString());
			long l = readUnsignedInt(seekableinput);
			if (l == 0L)
				throw new ImageFormatException("No IFD in TIFF image "
						+ this.toDisplayString());
			seekableinput.seek(l + offsetAdjustment);
			int i = readUnsignedShort(seekableinput);
			while (i-- > 0) {
				int i_6_ = readUnsignedShort(seekableinput);
				int i_7_ = readUnsignedShort(seekableinput);
				int i_8_ = readInt(seekableinput);
				byte[] is_9_ = new byte[4];
				seekableinput.readFully(is_9_);
				new IFDEntry(i_6_, i_7_, i_8_, is_9_);
			}
			if (ifd.isEmpty())
				throw new ImageFormatException("No IFD entries in TIFF image "
						+ this.toDisplayString());
			IFDEntry ifdentry = (IFDEntry) ifd.get(TIFF_TAG_IMAGE_WIDTH);
			if (ifdentry == null)
				throw new ImageFormatException(
						"Cannot read image width in TIFF image "
								+ this.toDisplayString());
			pxWidth = (int) ifdentry.getInteger();
			ifdentry = (IFDEntry) ifd.get(TIFF_TAG_IMAGE_LENGTH);
			if (ifdentry == null)
				throw new ImageFormatException(
						"Cannot read image width in TIFF image "
								+ this.toDisplayString());
			pxHeight = (int) ifdentry.getInteger();
			double d = 0.0;
			ifdentry = (IFDEntry) ifd.get(TIFF_TAG_X_RESOLUTION);
			if (ifdentry != null)
				d = ifdentry.getRational(seekableinput);
			double d_10_ = 0.0;
			ifdentry = (IFDEntry) ifd.get(TIFF_TAG_Y_RESOLUTION);
			if (ifdentry != null)
				d_10_ = ifdentry.getRational(seekableinput);
			if (d == 0.0)
				d = d_10_;
			if (d_10_ == 0.0)
				d_10_ = d;
			if (d != 0.0 && d_10_ != 0.0) {
				int i_11_ = 2;
				ifdentry = (IFDEntry) ifd.get(TIFF_TAG_RESOLUTION_UNIT);
				if (ifdentry != null)
					i_11_ = (int) ifdentry.getInteger();
				switch (i_11_) {
				case 2:
					horzResolution = d;
					vertResolution = d_10_;
					break;
				case 3:
					horzResolution = 2.54 * d;
					vertResolution = 2.54 * d_10_;
					break;
				case 1: {
					double d_12_ = Math.sqrt(d_10_ / d);
					horzResolution /= d_12_;
					vertResolution *= d_12_;
					break;
				}
				default:
					throw new ImageFormatException(
							"Incorrect resolution unit in TIFF image "
									+ this.toDisplayString());
				}
			}
			this.setDimensions();
			ifdentry = (IFDEntry) ifd.get(TIFF_TAG_SAMPLE_FORMAT);
			if (ifdentry != null && ifdentry.getInteger() != 1L)
				throw new ImageFormatException(
						"Unsupported sample format in TIFF image "
								+ this.toDisplayString());
			ifdentry = (IFDEntry) ifd.get(TIFF_TAG_PHOTOMETRIC_INTERPRETATION);
			if (ifdentry == null)
				throw new ImageFormatException(
						"Cannot determine image type: no PhotometricInterpretation tag in TIFF image "
								+ this.toDisplayString());
			int i_13_ = (int) ifdentry.getInteger();
			switch (i_13_) {
			case 0:
				colorSpace = (short) 5;
				break;
			case 1:
				colorSpace = (short) 4;
				break;
			case 2:
				colorSpace = (short) 1;
				break;
			case 3:
				colorSpace = (short) 6;
				break;
			case 5:
				colorSpace = (short) 2;
				break;
			default:
				throw new ImageFormatException("Unsupported image type ("
						+ i_13_ + ") in TIFF image " + this.toDisplayString());
			}
			ifdentry = (IFDEntry) ifd.get(TIFF_TAG_ORIENTATION);
			if (ifdentry != null && ifdentry.getInteger() != 1L)
				throw new ImageFormatException(
						"Cannot handle data with orientation other than top-down left-right in TIFF image "
								+ this.toDisplayString());
			int i_14_ = 1;
			ifdentry = (IFDEntry) ifd.get(TIFF_TAG_COMPRESSION);
			if (ifdentry != null)
				i_14_ = (int) ifdentry.getInteger();
			switch (i_14_) {
			case 1:
				compressionMethod = (short) 0;
				break;
			case 32773:
				compressionMethod = (short) 1;
				break;
			case 2:
				compressionMethod = (short) 4;
				break;
			case 3:
				compressionMethod = (short) 5;
				ifdentry = (IFDEntry) ifd.get(TIFF_TAG_T4_OPTIONS);
				if (ifdentry != null)
					t4options = ifdentry.getInteger();
				break;
			case 4:
				compressionMethod = (short) 6;
				ifdentry = (IFDEntry) ifd.get(TIFF_TAG_T6_OPTIONS);
				if (ifdentry != null)
					t6options = ifdentry.getInteger();
				break;
			case 5:
				compressionMethod = (short) 8;
				ifdentry = (IFDEntry) ifd.get(TIFF_TAG_PREDICTOR);
				if (ifdentry != null && ifdentry.getInteger() == 2L)
					predictor = (short) 1;
				break;
			default:
				throw new ImageFormatException(
						"Unsupported compression scheme (" + i_14_
								+ ") in TIFF image " + this.toDisplayString());
			}
			int i_15_ = 1;
			ifdentry = (IFDEntry) ifd.get(TIFF_TAG_PLANAR_CONFIGURATION);
			if (ifdentry != null)
				i_15_ = (int) ifdentry.getInteger();
			ifdentry = (IFDEntry) ifd.get(TIFF_TAG_SAMPLES_PER_PIXEL);
			if (ifdentry != null)
				samplesPerPixel = (int) ifdentry.getInteger();
			switch (colorSpace) {
			case 1:
				if (samplesPerPixel != 3)
					throw new ImageFormatException("RGB image cannot have "
							+ samplesPerPixel
							+ " samples per pixel in TIFF image "
							+ this.toDisplayString());
				break;
			case 2:
				if (samplesPerPixel != 4)
					throw new ImageFormatException("Non-CMYK separated image ("
							+ samplesPerPixel
							+ " samples per pixel) in TIFF image "
							+ this.toDisplayString());
				break;
			default:
				if (samplesPerPixel != 1)
					throw new ImageFormatException("Don't know how to handle "
							+ imageTypeNames[i_13_] + " with "
							+ samplesPerPixel
							+ " samples per pixel in TIFF image "
							+ this.toDisplayString());
			}
			if (i_15_ == 2 && samplesPerPixel > 1)
				throw new ImageFormatException(
						"Separate color planes unsupported in TIFF image "
								+ this.toDisplayString());
			int i_16_ = 0;
			ifdentry = (IFDEntry) ifd.get(TIFF_TAG_EXTRA_SAMPLES);
			if (ifdentry != null)
				i_16_ = (int) ifdentry.getInteger();
			if (i_16_ == 1)
				throw new ImageFormatException(
						"Interleaved alpha channel unsupported in TIFF image "
								+ this.toDisplayString());
			bitsPerComponent = 1;
			ifdentry = (IFDEntry) ifd.get(TIFF_TAG_BITS_PER_SAMPLE);
			if (ifdentry == null) {
				if (colorSpace != 5 && colorSpace != 4)
					throw new ImageFormatException(
							"BitsPerSample tag is required for image type "
									+ imageTypeNames[i_13_] + " in TIFF image "
									+ this.toDisplayString());
			} else {
				if (ifdentry.count != samplesPerPixel)
					throw new ImageFormatException(
							"Number of BitsPerSample entries does not match SamplesPerPixel in TIFF image "
									+ this.toDisplayString());
				if (samplesPerPixel == 1) {
					bitsPerComponent = (int) ifdentry.getInteger();
					if (bitsPerComponent != 1 && bitsPerComponent != 2
							&& bitsPerComponent != 4 && bitsPerComponent != 8)
						throw new ImageFormatException(
								"Cannot handle data with " + bitsPerComponent
										+ " bits per sample in TIFF image "
										+ this.toDisplayString());
				} else {
					int[] is_17_ = ifdentry.getIntegerArray(seekableinput);
					for (int i_18_ = 0; i_18_ < is_17_.length; i_18_++) {
						if (is_17_[i_18_] != 8)
							throw new ImageFormatException(
									imageTypeNames[i_13_]
											+ " with bit depth other than 8 are not supported in TIFF image "
											+ this.toDisplayString());
					}
					bitsPerComponent = is_17_[0];
				}
			}
			fillOrder = 1;
			ifdentry = (IFDEntry) ifd.get(TIFF_TAG_FILL_ORDER);
			if (ifdentry != null)
				fillOrder = (int) ifdentry.getInteger();
			switch (fillOrder) {
			case 1:
				break;
			case 2:
				if (compressionMethod != 4 || compressionMethod != 5
						|| compressionMethod != 6)
					throw new ImageFormatException(
							"Non-standard FillOrder ("
									+ fillOrder
									+ ") is supported for CCITT compression schemes only; cannot process TIFF image "
									+ this.toDisplayString());
				break;
			default:
				throw new ImageFormatException("Invalid FillOrder value ("
						+ fillOrder + ") in TIFF image "
						+ this.toDisplayString());
			}
			if (compressionMethod == 4 || compressionMethod == 5
					|| compressionMethod == 6) {
				if (i_13_ != 1 && i_13_ != 0)
					throw new ImageFormatException(
							imageTypeNames[i_13_]
									+ " color model is incompatible with CCITT FAX compression; cannot process TIFF image "
									+ this.toDisplayString());
				if (bitsPerComponent != 1)
					throw new ImageFormatException(
							"CCITT FAX compression is incompatible with "
									+ bitsPerComponent
									+ "-bit color depth; cannot process TIFF image "
									+ this.toDisplayString());
			}
			if (colorSpace == 6) {
				ifdentry = (IFDEntry) ifd.get(TIFF_TAG_COLORMAP);
				if (ifdentry == null)
					throw new ImageFormatException(
							"No colormap for indexed color model in TIFF image "
									+ this.toDisplayString());
				if (ifdentry.count != 3 * (1 << bitsPerComponent))
					throw new ImageFormatException(
							"Incorrect colormap length in TIFF image "
									+ this.toDisplayString());
				colorTable = new byte[ifdentry.count];
				seekableinput.seek(ifdentry.offset + offsetAdjustment);
				boolean bool = false;
				for (int i_19_ = 0; i_19_ < ifdentry.count / 3; i_19_++)
					colorTable[i_19_ * 3] = (byte) (readUnsignedShort(seekableinput) >> 8);
				for (int i_20_ = 0; i_20_ < ifdentry.count / 3; i_20_++)
					colorTable[i_20_ * 3 + 1] = (byte) (readUnsignedShort(seekableinput) >> 8);
				for (int i_21_ = 0; i_21_ < ifdentry.count / 3; i_21_++)
					colorTable[i_21_ * 3 + 2] = (byte) (readUnsignedShort(seekableinput) >> 8);
			}
			if (ifd.get(TIFF_TAG_STRIP_OFFSETS) != null
					&& ifd.get(TIFF_TAG_STRIP_BYTE_COUNTS) != null
					&& ifd.get(TIFF_TAG_ROWS_PER_STRIP) != null) {
				isTiled = false;
				rowsPerStrip = (int) ((IFDEntry) ifd
						.get(TIFF_TAG_ROWS_PER_STRIP)).getInteger();
				if (rowsPerStrip < 0)
					rowsPerStrip = 2147483647;
			} else if (ifd.get(TIFF_TAG_TILE_WIDTH) != null
					&& ifd.get(TIFF_TAG_TILE_LENGTH) != null
					&& ifd.get(TIFF_TAG_TILE_OFFSETS) != null
					&& ifd.get(TIFF_TAG_TILE_BYTE_COUNTS) != null) {
				isTiled = true;
				tileWidth = (int) ((IFDEntry) ifd.get(TIFF_TAG_TILE_WIDTH))
						.getInteger();
				tileHeight = (int) ((IFDEntry) ifd.get(TIFF_TAG_TILE_LENGTH))
						.getInteger();
				if (tileWidth < 0)
					tileWidth = 2147483647;
				if (tileHeight < 0)
					tileHeight = 2147483647;
			} else
				throw new ImageFormatException(
						"Inconsistent set of strip/tile tags in TIFF image "
								+ this.toDisplayString());
			canExpandData = true;
			canCopyData = !isTiled && compressionMethod == 0;
		} finally {
			seekableinput.close();
		}
	}

	public void copyData(OutputStream outputstream) throws IOException,
			ImageFormatException {
		if (!canCopyData)
			throw new ImageFormatException("Copying data from TIFF image "
					+ this.toDisplayString() + " is not supported");
		SeekableInput seekableinput = this.openSeekableImageStream();
		try {
			seekableinput.seek(offsetAdjustment);
			switch (compressionMethod) {
			case 0:
				copyPlainStrips(outputstream, seekableinput);
				break;
			default:
				throw new ImageFormatException(
						"Internal error: cannot lazily copy data from TIFF image "
								+ this.toDisplayString());
			}
		} finally {
			seekableinput.close();
		}
	}

	private void copyPlainStrips(OutputStream outputstream,
			SeekableInput seekableinput) throws IOException,
			ImageFormatException {
		int[] is = ((IFDEntry) ifd.get(TIFF_TAG_STRIP_OFFSETS))
				.getIntegerArray(seekableinput);
		int[] is_22_ = ((IFDEntry) ifd.get(TIFF_TAG_STRIP_BYTE_COUNTS))
				.getIntegerArray(seekableinput);
		for (int i = 0; i < is.length; i++)
			copyChunk(outputstream, seekableinput, is[i], is_22_[i]);
	}

	public void expandData(OutputStream outputstream) throws IOException,
			ImageFormatException {
		SeekableInput seekableinput = this.openSeekableImageStream();
		try {
			seekableinput.seek(offsetAdjustment);
			if (isTiled)
				expandTiledData(outputstream, seekableinput);
			else
				expandStripData(outputstream, seekableinput);
		} finally {
			seekableinput.close();
		}
	}

	private void expandTiledData(OutputStream outputstream,
			SeekableInput seekableinput) throws IOException,
			ImageFormatException {
		int[] is = ((IFDEntry) ifd.get(TIFF_TAG_TILE_OFFSETS))
				.getIntegerArray(seekableinput);
		int[] is_23_ = ((IFDEntry) ifd.get(TIFF_TAG_TILE_BYTE_COUNTS))
				.getIntegerArray(seekableinput);
		int i = (pxWidth + tileWidth - 1) / tileWidth;
		int i_24_ = (pxHeight + tileHeight - 1) / tileHeight;
		int i_25_ = 1;
		switch (colorSpace) {
		case 1:
			i_25_ = 3;
			break;
		case 2:
			i_25_ = 4;
			break;
		}
		int i_26_ = (i_25_ == 1 ? (tileWidth * bitsPerComponent + 7) / 8
				: tileWidth * i_25_);
		int i_27_ = (i_25_ == 1 ? (pxWidth * bitsPerComponent + 7) / 8
				: pxWidth * i_25_);
		byte[][] is_28_ = new byte[tileHeight][i_27_];
		for (int i_29_ = 0; i_29_ < i_24_; i_29_++) {
			for (int i_30_ = 0; i_30_ < i; i_30_++) {
				int i_31_ = i_29_ * i + i_30_;
				ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
				OutputStream outputstream_32_ = (compressionMethod == 8
						&& predictor == 1 ? (OutputStream) (new PredictorFilter(
						bytearrayoutputstream, tileWidth))
						: bytearrayoutputstream);
				expandChunk(outputstream_32_, seekableinput, is[i_31_],
						is_23_[i_31_], tileHeight, i_26_ * tileHeight);
				byte[] is_33_ = bytearrayoutputstream.toByteArray();
				for (int i_34_ = 0; i_34_ < tileHeight; i_34_++) {
					for (int i_35_ = 0; i_35_ < i_26_; i_35_++) {
						int i_36_ = i_34_ * i_26_ + i_35_;
						int i_37_ = i_30_ * i_26_ + i_35_;
						if (i_37_ >= i_27_ || i_36_ >= is_33_.length)
							break;
						is_28_[i_34_][i_37_] = is_33_[i_36_];
					}
				}
			}
			for (int i_38_ = 0; i_38_ < tileHeight
					&& i_29_ * tileHeight + i_38_ < pxHeight; i_38_++)
				outputstream.write(is_28_[i_38_]);
		}
	}

	private void expandStripData(OutputStream outputstream,
			SeekableInput seekableinput) throws IOException,
			ImageFormatException {
		int[] is = ((IFDEntry) ifd.get(TIFF_TAG_STRIP_OFFSETS))
				.getIntegerArray(seekableinput);
		int[] is_39_ = ((IFDEntry) ifd.get(TIFF_TAG_STRIP_BYTE_COUNTS))
				.getIntegerArray(seekableinput);
		int i = (samplesPerPixel == 1 ? (pxWidth * bitsPerComponent + 7) / 8
				: pxWidth * samplesPerPixel);
		int i_40_ = pxHeight;
		for (int i_41_ = 0; i_41_ < is.length && i_40_ > 0; i_41_++) {
			OutputStream outputstream_42_ = (compressionMethod == 8
					&& predictor == 1 ? (OutputStream) new PredictorFilter(
					outputstream, pxWidth) : outputstream);
			int i_43_ = i_40_ < rowsPerStrip ? i_40_ : rowsPerStrip;
			expandChunk(outputstream_42_, seekableinput, is[i_41_],
					is_39_[i_41_], i_43_, i_43_ * i);
			i_40_ -= rowsPerStrip;
		}
	}

	private void expandChunk(OutputStream outputstream,
			SeekableInput seekableinput, int i, int i_44_, int i_45_, int i_46_)
			throws IOException, ImageFormatException {
		switch (compressionMethod) {
		case 0:
			copyChunk(outputstream, seekableinput, i, i_44_);
			break;
		case 1:
			expandPackBitChunk(outputstream, seekableinput, i, i_44_);
			break;
		case 8:
			expandLZWChunk(outputstream, seekableinput, i, i_44_, i_46_);
			break;
		case 4:
		case 5:
		case 6:
			expandFaxChunk(outputstream, seekableinput, i, i_44_, i_45_);
			break;
		default:
			throw new ImageFormatException(
					"Internal error: invalid compression type in TIFF image "
							+ this.toDisplayString());
		}
	}

	private void copyChunk(OutputStream outputstream,
			SeekableInput seekableinput, int i, int i_47_) throws IOException {
		byte[] is = new byte[8192];
		seekableinput.seek((long) i + offsetAdjustment);
		int i_48_;
		for (/**/; i_47_ > 0; i_47_ -= i_48_) {
			i_48_ = i_47_ > is.length ? is.length : i_47_;
			seekableinput.readFully(is, 0, i_48_);
			outputstream.write(is, 0, i_48_);
		}
	}

	private void expandPackBitStrips(OutputStream outputstream,
			SeekableInput seekableinput) throws IOException,
			ImageFormatException {
		int[] is = ((IFDEntry) ifd.get(TIFF_TAG_STRIP_OFFSETS))
				.getIntegerArray(seekableinput);
		int[] is_49_ = ((IFDEntry) ifd.get(TIFF_TAG_STRIP_BYTE_COUNTS))
				.getIntegerArray(seekableinput);
		for (int i = 0; i < is.length; i++)
			expandPackBitChunk(outputstream, seekableinput, is[i], is_49_[i]);
	}

	private void expandPackBitChunk(OutputStream outputstream,
			SeekableInput seekableinput, int i, int i_50_) throws IOException {
		byte[] is = new byte[256];
		seekableinput.seek((long) i + offsetAdjustment);
		while (i_50_ > 0) {
			int i_51_ = seekableinput.readByte();
			i_50_--;
			if (0 <= i_51_ && i_51_ <= 127) {
				i_51_++;
				seekableinput.readFully(is, 0, i_51_);
				i_50_ -= i_51_;
				outputstream.write(is, 0, i_51_);
			} else if (-128 <= i_51_ && i_51_ <= -1) {
				i_51_ = 1 - i_51_;
				byte i_52_ = seekableinput.readByte();
				i_50_--;
				while (i_51_-- > 0)
					outputstream.write(i_52_);
			}
		}
	}

	private void expandFaxChunk(OutputStream outputstream,
			SeekableInput seekableinput, int i, int i_53_, int i_54_)
			throws IOException, ImageFormatException {
		byte[] is = new byte[i_53_];
		seekableinput.seek((long) i + offsetAdjustment);
		seekableinput.readFully(is, 0, i_53_);
		int i_55_ = isTiled ? tileWidth : pxWidth;
		TIFFFaxDecoder tifffaxdecoder = new TIFFFaxDecoder(fillOrder, i_55_,
				i_54_);
		byte[] is_56_ = new byte[(i_55_ + 7) / 8 * i_54_];
		try {
			switch (compressionMethod) {
			case 4:
				tifffaxdecoder.decode1D(is_56_, is, 0, i_54_);
				break;
			case 5:
				tifffaxdecoder.decode2D(is_56_, is, 0, i_54_, t4options);
				break;
			case 6:
				tifffaxdecoder.decodeT6(is_56_, is, 0, i_54_, t6options);
				break;
			default:
				throw new ImageFormatException("Invalid compression method");
			}
		} catch (ImageFormatException imageformatexception) {
			throw new ImageFormatException("CCITT Decoder: "
					+ imageformatexception.getMessage() + " in TIFF file "
					+ this.toDisplayString());
		}
		outputstream.write(is_56_);
	}

	private void expandLZWChunk(OutputStream outputstream,
			SeekableInput seekableinput, int i, int i_57_, int i_58_)
			throws IOException {
		byte[] is = new byte[i_57_];
		LZWDecoderStream lzwdecoderstream = new LZWDecoderStream(outputstream,
				8, false, true, i_58_);
		seekableinput.seek((long) i + offsetAdjustment);
		seekableinput.readFully(is, 0, i_57_);
		try {
			lzwdecoderstream.write(is, 0, i_57_);
		} catch (IOException ioexception) {
			throw new IOException(ioexception.getMessage() + " in TIFF file "
					+ this.toDisplayString());
		}
		lzwdecoderstream.flush();
	}

	short readShort(SeekableInput seekableinput) throws IOException {
		if (usesBigEndian)
			return seekableinput.readShort();
		int i = seekableinput.readUnsignedByte();
		int i_59_ = seekableinput.readUnsignedByte();
		return (short) (i_59_ << 8 | i);
	}

	int readUnsignedShort(SeekableInput seekableinput) throws IOException {
		if (usesBigEndian)
			return seekableinput.readUnsignedShort();
		int i = seekableinput.readUnsignedByte();
		int i_60_ = seekableinput.readUnsignedByte();
		return i_60_ << 8 | i;
	}

	int readInt(SeekableInput seekableinput) throws IOException {
		if (usesBigEndian)
			return seekableinput.readInt();
		int i = seekableinput.readUnsignedByte();
		int i_61_ = seekableinput.readUnsignedByte();
		int i_62_ = seekableinput.readUnsignedByte();
		int i_63_ = seekableinput.readUnsignedByte();
		return (i_63_ << 24) + (i_62_ << 16) + (i_61_ << 8) + i;
	}

	long readUnsignedInt(SeekableInput seekableinput) throws IOException {
		if (usesBigEndian)
			return seekableinput.readUnsignedInt();
		return (long) readInt(seekableinput) & 0xffffffffL;
	}

	static {
		tagNames.put(TIFF_TAG_IMAGE_WIDTH, "ImageWidth");
		tagNames.put(TIFF_TAG_IMAGE_LENGTH, "ImageLength");
		tagNames.put(TIFF_TAG_BITS_PER_SAMPLE, "BitsPerSample");
		tagNames.put(TIFF_TAG_COMPRESSION, "Compression");
		tagNames.put(TIFF_TAG_PHOTOMETRIC_INTERPRETATION,
				"PhotometricInterpretation");
		tagNames.put(TIFF_TAG_FILL_ORDER, "FillOrder");
		tagNames.put(TIFF_TAG_STRIP_OFFSETS, "StripOffsets");
		tagNames.put(TIFF_TAG_ORIENTATION, "Orientation");
		tagNames.put(TIFF_TAG_SAMPLES_PER_PIXEL, "SamplesPerPixel");
		tagNames.put(TIFF_TAG_ROWS_PER_STRIP, "RowsPerStrip");
		tagNames.put(TIFF_TAG_STRIP_BYTE_COUNTS, "StripByteCounts");
		tagNames.put(TIFF_TAG_X_RESOLUTION, "XResolution");
		tagNames.put(TIFF_TAG_Y_RESOLUTION, "YResolution");
		tagNames.put(TIFF_TAG_PLANAR_CONFIGURATION, "PlanarConfiguration");
		tagNames.put(TIFF_TAG_T4_OPTIONS, "T4Options");
		tagNames.put(TIFF_TAG_T6_OPTIONS, "T6Options");
		tagNames.put(TIFF_TAG_RESOLUTION_UNIT, "ResolutionUnit");
		tagNames.put(TIFF_TAG_PREDICTOR, "Predictor");
		tagNames.put(TIFF_TAG_COLORMAP, "Colormap");
		tagNames.put(TIFF_TAG_TILE_WIDTH, "TileWidth");
		tagNames.put(TIFF_TAG_TILE_LENGTH, "TileLength");
		tagNames.put(TIFF_TAG_TILE_OFFSETS, "TileOffsets");
		tagNames.put(TIFF_TAG_TILE_BYTE_COUNTS, "TileByteCounts");
		tagNames.put(TIFF_TAG_EXTRA_SAMPLES, "ExtraSamples");
		tagNames.put(TIFF_TAG_SAMPLE_FORMAT, "SampleFormat");
		tagNames.put(TIFF_TAG_S_MIN_SAMPLE_VALUE, "SMinSampleValue");
		tagNames.put(TIFF_TAG_S_MAX_SAMPLE_VALUE, "SMaxSampleValue");
		dataTypeNames = new String[] { "Error", "BYTE", "ASCII", "SHORT",
				"LONG", "RATIONAL", "SBYTE", "UNDEFINED", "SSHORT", "SLONG",
				"SRATIONAL", "FLOAT", "DOUBLE" };
		imageTypeNames = new String[] { "Reverse Monochrome", "Monochrome",
				"RGB", "Palette", "Transparency Mask", "Separated (CMYK)" };
	}
}