/*
 * OpaqueColor - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics.vector;

import java.io.IOException;
import java.io.PrintStream;

public abstract class OpaqueColor extends PaintSpec {
	public static class Registration extends OpaqueColor {
		public double tint;

		public Registration(double d) {
			tint = d;
		}

		public void dump(PrintStream printstream, String string)
				throws IOException {
			printstream.println(string + "%<registration-color tint=\"" + tint
					+ "\">");
			printstream.println(string + "  " + (1.0 - tint) + " setgray");
			printstream.println(string + "%</registration-color>");
		}
	}

	public static class SpotColor extends OpaqueColor {
		public double tint;

		public OpaqueColor altcolor;

		public String colorant;

		public SpotColor(double d, String string, double d_0_, double d_1_,
				double d_2_, double d_3_) {
			colorant = string;
			tint = d;
			altcolor = new OpaqueColor.CMYK(d_0_, d_1_, d_2_, d_3_);
		}

		public SpotColor(double d, String string, double d_4_) {
			colorant = string;
			tint = d;
			altcolor = new OpaqueColor.Grayscale(d_4_);
		}

		public SpotColor(double d, String string) {
			colorant = string;
			tint = d;
			altcolor = new OpaqueColor.Grayscale(0.0);
		}

		public void dump(PrintStream printstream, String string)
				throws IOException {
			printstream.println(string + "%<spot-color colorant=\"" + colorant
					+ "\" tint=\"" + tint + "\">");
			printstream.println(string + "   0 0 0 " + tint + " setcmykcolor");
			printstream.println(string + "%</spot-color>");
		}
	}

	public static class CMYK extends OpaqueColor {
		public double c;

		public double m;

		public double y;

		public double k;

		public CMYK(double d, double d_5_, double d_6_, double d_7_) {
			c = d;
			m = d_5_;
			y = d_6_;
			k = d_7_;
		}

		public void dump(PrintStream printstream, String string)
				throws IOException {
			printstream.println(string + "%<cmyk-color cyan=\"" + c
					+ "\" magenta=\"" + m + "\" yellow=\"" + y + "\" black=\""
					+ k + "\">");
			printstream.println(string + "  " + c + " " + m + " " + y + " " + k
					+ " setcmykcolor");
			printstream.println(string + "%</cmyk-color>");
		}
	}

	public static class Grayscale extends OpaqueColor {
		public double g;

		public Grayscale(double d) {
			g = d;
		}

		public void dump(PrintStream printstream, String string)
				throws IOException {
			printstream.println(string + "%<grayscale gray=\"" + g + "\">");
			printstream.println(string + "  " + g + " setgray");
			printstream.println(string + "%</grayscale>");
		}
	}

	public static class RGB extends OpaqueColor {
		public double r;

		public double g;

		public double b;

		public RGB(double d, double d_8_, double d_9_) {
			r = d;
			g = d_8_;
			b = d_9_;
		}

		public void dump(PrintStream printstream, String string)
				throws IOException {
			printstream.println(string + "%<rgb-color red=\"" + r
					+ "\" green=\"" + g + "\" blue=\"" + b + "\">");
			printstream.println(string + "  " + r + " " + g + " " + b
					+ " setrgbcolor");
			printstream.println(string + "%</rgb-color>");
		}
	}
}