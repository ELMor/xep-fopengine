/*
 * Pattern - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics.vector;

import java.io.IOException;
import java.io.PrintStream;

public class Pattern extends PaintSpec {
	public GraphicObject content = null;

	public double xstep = 0.0;

	public double ystep = 0.0;

	public void dump(PrintStream printstream, String string) throws IOException {
		printstream.println(string + "%<pattern xstep=\"" + xstep
				+ "\" ystep=\"" + ystep + "\">");
		content.dump(printstream, "% " + string);
		printstream.println(string + "%</pattern>");
	}
}