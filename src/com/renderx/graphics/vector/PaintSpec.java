/*
 * PaintSpec - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics.vector;

import java.io.IOException;
import java.io.PrintStream;

public abstract class PaintSpec extends GraphicObject {
	public abstract void dump(PrintStream printstream, String string)
			throws IOException;
}