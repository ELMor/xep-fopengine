/*
 * GraphicGroup - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics.vector;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Enumeration;

import com.renderx.util.List;

public class GraphicGroup extends GraphicObject {
	public List children = new List();

	public GraphicGroup clippath = null;

	public double opacity = 1.0;

	public double[] matrix = { 1.0, 0.0, 0.0, 1.0, 0.0, 0.0 };

	public boolean isIdentityTransform() {
		return isIdentityTransform(matrix);
	}

	public boolean isIdentityTransform(double[] ds) {
		return (ds[0] == 1.0 && ds[1] == 0.0 && ds[2] == 0.0 && ds[3] == 1.0
				&& ds[4] == 0.0 && ds[5] == 0.0);
	}

	public double[] multipleMatrix(double[] ds, double[] ds_0_) {
		double[] ds_1_ = new double[6];
		ds_1_[0] = ds_0_[0] * ds[0] + ds_0_[1] * ds[2];
		ds_1_[1] = ds_0_[0] * ds[1] + ds_0_[1] * ds[3];
		ds_1_[2] = ds_0_[2] * ds[0] + ds_0_[3] * ds[2];
		ds_1_[3] = ds_0_[2] * ds[1] + ds_0_[3] * ds[3];
		ds_1_[4] = ds_0_[4] * ds[0] + ds_0_[5] * ds[2] + ds[4];
		ds_1_[5] = ds_0_[4] * ds[1] + ds_0_[5] * ds[3] + ds[5];
		return ds_1_;
	}

	public double[] transformCoordinates(double[] ds, double d, double d_2_) {
		double[] ds_3_ = new double[2];
		ds_3_[0] = ds[0] * d + ds[2] * d_2_ + ds[4];
		ds_3_[1] = ds[1] * d + ds[3] * d_2_ + ds[5];
		return ds_3_;
	}

	public double[] reverseMatrix(double[] ds) {
		double[] ds_4_ = new double[6];
		double d = ds[0];
		double d_5_ = ds[1];
		double d_6_ = ds[2];
		double d_7_ = ds[3];
		double d_8_ = ds[4];
		double d_9_ = ds[5];
		ds_4_[0] = d_7_ / (d * d_7_ - d_6_ * d_5_);
		ds_4_[1] = -d_5_ / (d * d_7_ - d_6_ * d_5_);
		ds_4_[2] = -d_6_ / (d * d_7_ - d_6_ * d_5_);
		ds_4_[3] = d / (d * d_7_ - d_6_ * d_5_);
		ds_4_[4] = (d_6_ * d_9_ - d_7_ * d_8_) / (d * d_7_ - d_6_ * d_5_);
		ds_4_[5] = (d_5_ * d_8_ - d * d_9_) / (d * d_7_ - d_6_ * d_5_);
		return ds_4_;
	}

	public double[] getReverseMatrix() {
		return reverseMatrix(matrix);
	}

	public void transform(double d, double d_10_, double d_11_, double d_12_,
			double d_13_, double d_14_) {
		double d_15_ = d * matrix[0] + d_10_ * matrix[2];
		double d_16_ = d * matrix[1] + d_10_ * matrix[3];
		double d_17_ = d_11_ * matrix[0] + d_12_ * matrix[2];
		double d_18_ = d_11_ * matrix[1] + d_12_ * matrix[3];
		double d_19_ = d_13_ * matrix[0] + d_14_ * matrix[2] + matrix[4];
		double d_20_ = d_13_ * matrix[1] + d_14_ * matrix[3] + matrix[5];
		matrix[0] = d_15_;
		matrix[1] = d_16_;
		matrix[2] = d_17_;
		matrix[3] = d_18_;
		matrix[4] = d_19_;
		matrix[5] = d_20_;
	}

	public void rotate(double d) {
		double d_21_ = Math.sin(d);
		double d_22_ = Math.cos(d);
		if (d_21_ == 1.0 || d_21_ == -1.0)
			d_22_ = 0.0;
		if (d_22_ == 1.0 || d_22_ == -1.0)
			d_21_ = 0.0;
		transform(d_22_, d_21_, -d_21_, d_22_, 0.0, 0.0);
	}

	public void translate(double d, double d_23_) {
		transform(1.0, 0.0, 0.0, 1.0, d, d_23_);
	}

	public void scale(double d, double d_24_) {
		transform(d, 0.0, 0.0, d_24_, 0.0, 0.0);
	}

	public void skew(double d, double d_25_) {
		transform(1.0, Math.tan(d_25_), Math.tan(d), 1.0, 0.0, 0.0);
	}

	public void clipRect(double d, double d_26_, double d_27_, double d_28_) {
		Path path = new Path();
		path.rectangle(d, d_26_, d_27_, d_28_);
		if (clippath == null)
			clippath = new GraphicGroup();
		clippath.append(path);
	}

	public void append(GraphicObject graphicobject) {
		children.append(graphicobject);
	}

	public void dump(PrintStream printstream, String string) throws IOException {
		printstream.println(string + "%<group opacity=\"" + opacity + "\" "
				+ "matrix=\"" + matrix[0] + " " + matrix[1] + " " + matrix[2]
				+ " " + matrix[3] + " " + matrix[4] + " " + matrix[5] + "\">");
		printstream.println(string + "  gsave");
		printstream.println(string + "  [" + matrix[0] + " " + matrix[1] + " "
				+ matrix[2] + " " + matrix[3] + " " + matrix[4] + " "
				+ matrix[5] + "] concat");
		if (clippath != null) {
			printstream.println(string + "  %<clippathref>");
			clippath.dump(printstream, string + "  ");
			printstream.println(string + "  %</clippathref>");
		}
		Enumeration enumeration = children.elements();
		while (enumeration.hasMoreElements())
			((GraphicObject) enumeration.nextElement()).dump(printstream,
					string + "  ");
		printstream.println(string + "  grestore");
		printstream.println(string + "%</group>");
	}
}