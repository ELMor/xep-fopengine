/*
 * SpotColorList - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.graphics;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.StringTokenizer;

import com.renderx.util.Hashtable;
import com.renderx.util.LineEnumerator;
import com.renderx.util.URLSpec;
import com.renderx.util.User;

public final class SpotColorList {
	private static final String SPOT_COLOR_LIST = "resource:com/renderx/graphics/PantoneColors.txt";

	public static final SpotColorList dflt = new SpotColorList();

	private final Hashtable exactName2CMYK = new Hashtable();

	private final Hashtable partialName2CMYK = new Hashtable();

	private static boolean initialized = false;

	public static synchronized void init() {
		if (!initialized) {
			String string = null;
			try {
				string = User
						.getProperty("com.renderx.graphics.SPOT_COLOR_LIST");
				if (string == null)
					string = "resource:com/renderx/graphics/PantoneColors.txt";
				dflt.readColorList(new URLSpec(string).openStream());
			} catch (Exception exception) {
				throw new RuntimeException("cannot parse " + string + ": "
						+ exception.toString());
			}
			initialized = true;
		}
	}

	public SpotColorList() {
		/* empty */
	}

	public SpotColorList(InputStream inputstream) throws IOException,
			SpotColorFileFormatError {
		this();
		readColorList(inputstream);
	}

	public void readColorList(InputStream inputstream) throws IOException,
			SpotColorFileFormatError {
		LineEnumerator lineenumerator = new LineEnumerator(inputstream);
		int i = 0;
		while (lineenumerator.hasMoreLines()) {
			String string = lineenumerator.nextLine();
			i++;
			if (!string.startsWith("#")) {
				StringTokenizer stringtokenizer = new StringTokenizer(string,
						";");
				if (stringtokenizer.hasMoreTokens()) {
					String string_0_ = normalizeColorName(stringtokenizer
							.nextToken());
					double[] ds = new double[4];
					int i_1_ = 0;
					try {
						while (stringtokenizer.hasMoreTokens()
								&& i_1_ < ds.length) {
							int i_2_ = Integer.parseInt(stringtokenizer
									.nextToken().trim());
							ds[i_1_++] = (double) i_2_ / 100.0;
						}
					} catch (Exception exception) {
						throw new SpotColorFileFormatError("[Line " + i
								+ "]: spot color '" + string_0_
								+ "' has an invalid CMYK equivalent");
					}
					if (string_0_.endsWith(" *"))
						partialName2CMYK.put(string_0_.substring(0, (string_0_
								.length() - 2)), ds);
					else
						exactName2CMYK.put(string_0_, ds);
				}
			}
		}
	}

	public double[] getCMYK(String string) {
		string = normalizeColorName(string);
		double[] ds = (double[]) exactName2CMYK.get(string);
		if (ds != null)
			return ds;
		int i = string.lastIndexOf(' ');
		if (i == -1)
			return null;
		return (double[]) partialName2CMYK.get(string.substring(0, i));
	}

	private static String normalizeColorName(String string) {
		StringTokenizer stringtokenizer = new StringTokenizer(string);
		StringBuffer stringbuffer = new StringBuffer();
		while (stringtokenizer.hasMoreTokens()) {
			stringbuffer.append(stringtokenizer.nextToken().toLowerCase());
			if (stringtokenizer.hasMoreTokens())
				stringbuffer.append(' ');
		}
		return stringbuffer.toString();
	}

	public static void main(String[] strings) throws Exception {
		if (strings.length == 0) {
			System.err
					.println("Usage: com.renderx.graphics.SpotColorList list_file_name");
			System.exit(1);
		}
		init();
		try {
			SpotColorList spotcolorlist = null;
			if (strings.length == 1)
				spotcolorlist = new SpotColorList(new FileInputStream(
						strings[0]));
			System.err.println();
			System.err.println("Instance SpotColor list:");
			System.err.println("--------------------");
			int i = 1;
			Enumeration enumeration = spotcolorlist.exactName2CMYK.keys();
			while (enumeration.hasMoreElements()) {
				String string = (String) enumeration.nextElement();
				double[] ds = spotcolorlist.getCMYK(string);
				System.err.println("[" + i + "] " + string + " = [" + ds[0]
						+ ", " + ds[1] + ", " + ds[2] + ", " + ds[3] + "]");
				i++;
			}
			enumeration = spotcolorlist.partialName2CMYK.keys();
			while (enumeration.hasMoreElements()) {
				String string = (String) enumeration.nextElement();
				double[] ds = spotcolorlist.getCMYK(string + " ABC");
				System.err.println("[" + i + "] " + string + " = [" + ds[0]
						+ ", " + ds[1] + ", " + ds[2] + ", " + ds[3] + "]");
				i++;
			}
		} catch (IOException ioexception) {
			System.err.println(ioexception);
		}
		System.exit(0);
	}
}