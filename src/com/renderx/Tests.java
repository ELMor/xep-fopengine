/*
 * Tests - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx;

import com.renderx.xep.pre.Parser;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class Tests extends TestCase {
	public static Test suite() {
		TestSuite testsuite = new TestSuite();
		testsuite.addTestSuite(com.renderx.util.Args.Test.class);
		testsuite.addTestSuite(com.renderx.util.MIMEType.Test.class);
		testsuite.addTestSuite(com.renderx.xep.pre.Bidi.Test.class);
		testsuite.addTestSuite(com.renderx.pdflib.Linearize.Test.class);
		testsuite
				.addTestSuite(com.renderx.pdflib.parser.PDFInstance.Test.class);
		testsuite.addTestSuite(com.renderx.pdflib.parser.XRefTable.Test.class);
		testsuite.addTestSuite(com.renderx.pslib.Image.Test.class);
		testsuite.addTestSuite(com.renderx.pslib.Images.Test.class);
		testsuite.addTest(Parser.Test.suite());
		testsuite.addTestSuite(com.renderx.fonts.CFFFont.Test.class);
		return testsuite;
	}
}