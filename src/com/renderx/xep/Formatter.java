/*
 * Formatter - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep;

import java.io.IOException;

import javax.xml.transform.Source;

import com.renderx.xep.lib.ConfigurationException;
import com.renderx.xep.lib.Logger;

import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

public interface Formatter {
	public void render(Source source, FOTarget fotarget) throws SAXException,
			IOException;

	public void render(Source source, FOTarget fotarget, Logger logger)
			throws SAXException, IOException;

	public ContentHandler createContentHandler(String string, FOTarget fotarget)
			throws ConfigurationException;

	public ContentHandler createContentHandler(String string,
			FOTarget fotarget, Logger logger) throws SAXException, IOException;
}