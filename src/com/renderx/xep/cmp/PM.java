/*
 * PM - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

import com.renderx.util.Hashtable;
import com.renderx.util.List;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;

public class PM extends Xattr {
	public int width;

	public int height;

	public int phi;

	public int[] pagerect;

	public List pilist = new List();

	public Hashtable regtab = new Hashtable();

	public void dimensions(int i, int i_0_) {
		width = i;
		height = i_0_;
		phi = this.get(Attn.$reference_orientation).count();
		if (this.get(Attn.$writing_mode) == Attr.rl_tb)
			pagerect = Rectangle.shrink(new int[] { 0, 0, i, i_0_ }, new int[] {
					this.get(Attn.$margin_end).length_or_ratio(i),
					this.get(Attn.$margin_after).length_or_ratio(i_0_),
					this.get(Attn.$margin_start).length_or_ratio(i),
					this.get(Attn.$margin_before).length_or_ratio(i_0_) });
		else
			pagerect = Rectangle.shrink(new int[] { 0, 0, i, i_0_ }, new int[] {
					this.get(Attn.$margin_start).length_or_ratio(i),
					this.get(Attn.$margin_after).length_or_ratio(i_0_),
					this.get(Attn.$margin_end).length_or_ratio(i),
					this.get(Attn.$margin_before).length_or_ratio(i_0_) });
		pagerect = Rectangle.rotate(pagerect, phi);
	}
}