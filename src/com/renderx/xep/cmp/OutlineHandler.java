/*
 * OutlineHandler - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

import com.renderx.util.Stack;
import com.renderx.xep.lib.Session;
import com.renderx.xep.pre.AttList;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.CompilerException;
import com.renderx.xep.pre.Elem;
import com.renderx.xep.pre.ElementHandler;

class OutlineHandler implements ElementHandler {
	private final Session session;

	private final Outline outline;

	private Stack bstack = new Stack();

	private Item.Bookmark b = null;

	OutlineHandler(Outline outline, Session session) {
		this.outline = outline;
		this.session = session;
	}

	public void startElement(short i, AttList attlist) throws CompilerException {
		switch (i) {
		case 103:
			bstack.push(b);
			b = new Item.Bookmark(attlist, b, session);
			outline.append(b);
			break;
		case 102:
			break;
		case 22:
			break;
		case 0: {
			StringBuffer stringbuffer = new StringBuffer();
			Item.Bookmark bookmark = b;
			bookmark.label = stringbuffer.append(bookmark.label).append(
					attlist.get(Attn.$TEXT).word()).toString();
			break;
		}
		default:
			throw new CompilerException("Invalid element " + Elem.getName(i)
					+ " in outline");
		}
	}

	public void endElement(short i) throws CompilerException {
		switch (i) {
		case 103:
			b = (Item.Bookmark) bstack.pop();
			break;
		case 102:
			break;
		case 22:
			break;
		case 0:
			break;
		default:
			throw new CompilerException("Invalid element " + Elem.getName(i)
					+ " in outline");
		}
	}
}