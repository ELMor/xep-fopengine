/*
 * ImageFactory - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

import java.net.MalformedURLException;

import com.renderx.fonts.FontCatalog;
import com.renderx.graphics.Image;
import com.renderx.util.ErrorHandler;
import com.renderx.util.URLCache;
import com.renderx.util.URLSpec;
import com.renderx.xep.lib.Session;
import com.renderx.xep.pre.Attr;

public class ImageFactory extends com.renderx.graphics.ImageFactory {
	final URLSpec brokenimage;

	public ImageFactory(FontCatalog fontcatalog, URLCache urlcache,
			ErrorHandler errorhandler, URLSpec urlspec) {
		super(fontcatalog, urlcache, errorhandler);
		brokenimage = urlspec;
	}

	public Image makeImage(Attr attr, Attr attr_0_, Attr attr_1_,
			Session session) {
		Object object = null;
		URLSpec urlspec;
		try {
			urlspec = (attr_0_ == null ? new URLSpec(attr.word())
					: new URLSpec(new URLSpec(attr_0_.word()), attr.word()));
		} catch (MalformedURLException malformedurlexception) {
			session.exception(("Failed to create image " + attr.word()
					+ " of type " + attr_1_), malformedurlexception);
			return makeImage(brokenimage, null, session);
		}
		return makeImage(urlspec, attr_1_.word(), session);
	}

	public Image makeImage(URLSpec urlspec, String string, Session session) {
		if ("auto".equals(string))
			string = null;
		try {
			return this.makeImage(urlspec, string);
		} catch (Exception exception) {
			session.exception(("Failed to create image " + urlspec
					+ " of type " + string), exception);
			try {
				return this.makeImage(brokenimage);
			} catch (Exception exception_2_) {
				session.exception(
						("Failed to process fallback image " + brokenimage),
						exception_2_);
				return null;
			}
		}
	}
}