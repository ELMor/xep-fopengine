/*
 * FlowHandler - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

import com.renderx.hyphen.Hyphenator;
import com.renderx.util.Hashtable;
import com.renderx.util.Stack;
import com.renderx.xep.lib.Session;
import com.renderx.xep.lib.Util;
import com.renderx.xep.pre.AttList;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Attributed;
import com.renderx.xep.pre.CompilerException;
import com.renderx.xep.pre.Elem;
import com.renderx.xep.pre.ElementHandler;

class FlowHandler implements ElementHandler {
	private Item.Block fakeblock;

	private Item.Span fakespan;

	private final Session session;

	private Stack contexts = new Stack();

	private Stack inlinecontexts = new Stack();

	private Stack captions = new Stack();

	private Stack tables = new Stack();

	private Stack lists = new Stack();

	private Stack exceptions = new Stack();

	private Stack ids = new Stack();

	private Stack keys = new Stack();

	private Hashtable keytab = new Hashtable();

	private Context context = null;

	private Item.ListBlock list = null;

	private Item.Table table = null;

	private Item.Block b = null;

	private Item.Span s = null;

	private Item.Footnote fn = null;

	private Tokenizer tokenizer = new Tokenizer();

	private Hyphenator hyphenator = null;

	private String currentLanguage = "";

	private boolean bow = true;

	private Item bos = null;

	private Attributed a0 = null;

	private Item.TextTraits tt0 = null;

	private Item.Id id;

	private Item.Key key;

	private static final Attr.Word uppercase = Attr.newWord("uppercase");

	private static final Attr.Word lowercase = Attr.newWord("lowercase");

	private static final Attr.Word capitalize = Attr.newWord("capitalize");

	private static class InlineContext {
		short tokenizer_a;

		Item bos;

		boolean bow;

		InlineContext(short i, Item item, boolean bool) {
			tokenizer_a = i;
			bos = item;
			bow = bool;
		}
	}

	private static class Context {
		Flow flow;

		Stack blocks;

		Stack spans;

		Context() {
			this(new Flow(), new Stack(), new Stack());
		}

		Context(Flow flow) {
			this(flow, new Stack(), new Stack());
		}

		Context(Flow flow, Stack stack, Stack stack_0_) {
			this.flow = flow;
			blocks = stack;
			spans = stack_0_;
		}
	}

	FlowHandler(Flow flow, Session session) {
		this.session = session;
		fakeblock = new Item.Block(AttList.empty, session);
		fakespan = new Item.Span(AttList.empty, session);
		id = new Item.Id(AttList.empty);
		id.desc_count = 0;
		ids.push(id);
		key = new Item.Key(AttList.empty, session);
		key.desc_count = 0;
		keys.push(key);
		pushContext(new Context(flow));
	}

	private void pushContext(Context context) {
		contexts.push(this.context = context);
		b = (context.blocks.isEmpty() ? fakeblock : (Item.Block) context.blocks
				.peek());
		s = (context.spans.isEmpty() ? fakespan : (Item.Span) context.spans
				.peek());
	}

	private void popContext() {
		contexts.pop();
		if (!contexts.isEmpty()) {
			context = (Context) contexts.peek();
			b = (context.blocks.isEmpty() ? fakeblock
					: (Item.Block) context.blocks.peek());
			s = (context.spans.isEmpty() ? fakespan : (Item.Span) context.spans
					.peek());
		} else {
			context = null;
			b = null;
			s = null;
		}
	}

	private void pushId(Attributed attributed) {
		id = new Item.Id(attributed);
		ids.push(id);
		id.desc_count = 0;
		context.flow.append(id);
	}

	private void popId() {
		Item.Lid lid = new Item.Lid(id.a, id.id);
		context.flow.append(lid);
		ids.pop();
		id = (Item.Id) ids.peek();
	}

	private void pushKey(Attributed attributed) {
		key = new Item.Key(attributed, session);
		keys.push(key);
		key.desc_count = 0;
		context.flow.append(key);
	}

	private void popKey() {
		Item.Endk endk = new Item.Endk(key.a, key.id);
		context.flow.append(endk);
		keys.pop();
		key = (Item.Key) keys.peek();
	}

	private void addZWLeader(AttList attlist) {
		/* empty */
	}

	private void pcdata(AttList attlist) {
		String string = attlist.get(Attn.$TEXT).word();
		boolean bool = b.wrap
				&& s.get(Attn.$keep_together_within_line) == Attr.auto;
		string = tokenizer.tokenize(attlist, string, b);
		if (bool && attlist.get(Attn.$hyphenate).bool()) {
			String string_1_ = attlist.get(Attn.$language).word();
			Attr attr = attlist.get(Attn.$country);
			if (attr != Attr.none)
				string_1_ += "-" + attr.word();
			if (!string_1_.equals(currentLanguage)) {
				hyphenator = session.config.hyphenFactory
						.getHyphenator(string_1_);
				currentLanguage = string_1_;
			}
			if (hyphenator != null)
				string = (hyphenator.hyphenate(string, attlist.get(
						Attn.$hyphenation_remain_character_count).count(),
						attlist.get(Attn.$hyphenation_push_character_count)
								.count()));
		}
		char[] cs = new char[string.length()];
		string.getChars(0, string.length(), cs, 0);
		short i = 0;
		int i_2_ = 0;
		for (int i_3_ = 0; i_3_ != cs.length; i_3_++) {
			char c = cs[i_3_];
			if (c == '\n') {
				if (i_2_ != 0) {
					addText(cs, i_2_, attlist, i);
					i_2_ = 0;
				}
				AttList attlist_4_ = (AttList) attlist.clone();
				attlist_4_.put(Attn.$leader_length_minimum, Attr.newLength(0));
				attlist_4_.put(Attn.$leader_length_optimum, Attr.newLength(0));
				attlist_4_.put(Attn.$leader_length_maximum, Attr.newLength(0));
				Item.Leader leader = new Item.Leader(attlist_4_, session);
				startSpan(leader.span = new Item.Span(attlist, session));
				context.flow.append(leader);
				endSpan();
				context.flow.append(Item.endl);
				i = (short) 0;
				bow = true;
			} else if (c == '\u00ad') {
				if (bool) {
					if (i_2_ != 0) {
						addText(cs, i_2_, attlist, (short) (i | 0x20));
						i_2_ = 0;
					}
					i = (short) 1;
				}
			} else if (c == '\u200b') {
				if (i_2_ != 0) {
					addText(cs, i_2_, attlist, i);
					i_2_ = 0;
				}
				if (bool)
					i = (short) 1;
				bow = true;
			} else {
				if (i_2_ != 0 && (cs[i_2_ - 1] == ' ') != (c == ' ')) {
					addText(cs, i_2_, attlist, i);
					i_2_ = 0;
					i = (short) 0;
				}
				cs[i_2_++] = c;
			}
		}
		if (i_2_ != 0)
			addText(cs, i_2_, attlist, i);
		attlist.remove(Attn.$TEXT);
	}

	private short bos(short i) {
		if (bos != null) {
			bos.setFlags(i & 0x1);
			i &= ~0x1;
			bos = null;
		}
		return i;
	}

	private void addText(char[] cs, int i, AttList attlist, short i_5_) {
		i_5_ = bos(i_5_);
		Attr attr = attlist.get(Attn.$text_transform);
		if (attr != Attr.none) {
			if (attr == uppercase) {
				for (int i_6_ = 0; i_6_ != i; i_6_++)
					cs[i_6_] = Character.toUpperCase(cs[i_6_]);
			} else if (attr == lowercase) {
				for (int i_7_ = 0; i_7_ != i; i_7_++)
					cs[i_7_] = Character.toLowerCase(cs[i_7_]);
			} else if (attr == capitalize) {
				for (int i_8_ = 0; i_8_ != i; i_8_++) {
					cs[i_8_] = (bow ? Character.toUpperCase(cs[i_8_])
							: Character.toLowerCase(cs[i_8_]));
					bow = !Character.isLetter(cs[i_8_]);
				}
			}
		}
		Item.Text text = new Item.Text(cs, i, attlist, session, a0, tt0);
		text.setFlags(i_5_);
		a0 = text.a;
		tt0 = text.tt;
		context.flow.append(text);
	}

	private void swap(Attributed attributed, Attn attn, Attn attn_9_) {
		if (attributed.containsKey(attn)) {
			if (attributed.containsKey(attn_9_)) {
				Attr attr = attributed.get(attn_9_);
				attributed.put(attn_9_, attributed.get(attn));
				attributed.put(attn, attr);
			} else
				attributed.put(attn_9_, attributed.remove(attn));
		} else if (attributed.containsKey(attn_9_))
			attributed.put(attn, attributed.remove(attn_9_));
	}

	private boolean rotated(Attributed attributed) {
		return ((Util.phi(attributed.get(Attn.$reference_orientation).count()) != 0) || Attr
				.dim_range(attributed,
						Attn.$block_progression_dimension_minimum,
						Attn.$block_progression_dimension_optimum,
						Attn.$block_progression_dimension_maximum, 0) != -2147483648);
	}

	private void arot(Attributed attributed, Attributed attributed_10_) {
		if (attributed.get(Attn.$reference_orientation).count() % 180 == 0) {
			Attn[] attns = { Attn.$reference_orientation, Attn.$writing_mode,
					Attn.$display_align,
					Attn.$block_progression_dimension_minimum,
					Attn.$block_progression_dimension_optimum,
					Attn.$block_progression_dimension_maximum };
			for (int i = 0; i != attns.length; i++) {
				Attn attn = attns[i];
				if (attributed.containsKey(attn))
					attributed_10_.put(attn, attributed.get(attn));
			}
		} else {
			Attn[] attns = { Attn.$reference_orientation, Attn.$writing_mode,
					Attn.$display_align,
					Attn.$inline_progression_dimension_minimum,
					Attn.$inline_progression_dimension_optimum,
					Attn.$inline_progression_dimension_maximum };
			for (int i = 0; i != attns.length; i++) {
				Attn attn = attns[i];
				if (attributed.containsKey(attn))
					attributed_10_.put(attn, attributed.get(attn));
			}
			Attn[] attns_11_ = { Attn.$block_progression_dimension_minimum,
					Attn.$inline_progression_dimension_minimum,
					Attn.$block_progression_dimension_optimum,
					Attn.$inline_progression_dimension_optimum,
					Attn.$block_progression_dimension_maximum,
					Attn.$inline_progression_dimension_maximum };
			for (int i = 0; i != attns_11_.length; i += 2)
				swap(attributed, attns_11_[i], attns_11_[i + 1]);
		}
	}

	private void startBlock(Item.Block block) {
		if (block instanceof Item.Frame
				&& block.get(Attn.$absolute_position) != Attr.auto) {
			startExpt((block.get(Attn.$absolute_position) == Attr
					.newWord("fixed")) ? (Item.Exception) new Item.Fixed(
					block.a) : new Item.Absolute(block.a));
			AttList attlist = (AttList) block.a.clone();
			Attn[] attns = { Attn.$start_indent, Attn.$end_indent,
					Attn.$margin_before, Attn.$margin_after,
					Attn.$margin_start, Attn.$margin_end,
					Attn.$border_before_width_length,
					Attn.$border_after_width_length,
					Attn.$border_start_width_length,
					Attn.$border_end_width_length, Attn.$padding_before_length,
					Attn.$padding_after_length, Attn.$padding_start_length,
					Attn.$padding_end_length,
					Attn.$inline_progression_dimension_minimum,
					Attn.$inline_progression_dimension_optimum,
					Attn.$inline_progression_dimension_maximum,
					Attn.$block_progression_dimension_minimum,
					Attn.$block_progression_dimension_optimum,
					Attn.$block_progression_dimension_maximum,
					Attn.$background_color, Attn.$background_image };
			for (int i = 0; i != attns.length; i++)
				attlist.remove(attns[i]);
			block.a = attlist;
			((Item.Frame) block).mode = (short) 2;
			context.flow.append(block);
			context.blocks.push(block);
			b = block;
		} else {
			if (block.get(Attn.$keep_with_previous_within_column) != Attr.auto
					|| (block.get(Attn.$keep_with_previous_within_page) != Attr.auto))
				context.flow.append(new Item.Keep((Attributed) block));
			if (block.get(Attn.$break_before) != Attr.auto)
				context.flow.append(new Item.Break(block, "before"));
			if (block.get(Attn.$clear) != Attr.none)
				context.flow.append(new Item.Clear(block.a));
			if (block.get(Attn.$span) == Attr.all)
				context.flow.append(new Item.Wide(block.a, session));
			context.flow.append(Item.empty);
			Item.Space.Display display = new Item.Space.Display(block,
					"space-before");
			if (!display.nil)
				context.flow.append(display);
			context.flow.append(block);
			context.blocks.push(block);
			if (rotated(block)) {
				AttList attlist = new AttList();
				block.collapse = false;
				arot(block.a = (AttList) block.a.clone(), attlist);
				startRotated(attlist);
			} else
				b = block;
		}
		bow = true;
	}

	private void endBlock() {
		if (b instanceof Item.Frame && ((Item.Frame) b).mode == 2) {
			context.flow.append(Item.endf);
			endExpt();
		} else {
			if (b instanceof Item.Frame && ((Item.Frame) b).mode == 1)
				endRotated();
			Item.Block block = b;
			context.blocks.pop();
			b = (context.blocks.isEmpty() ? fakeblock
					: (Item.Block) context.blocks.peek());
			context.flow
					.append(block instanceof Item.Frame ? (Item.Endb) Item.endf
							: block instanceof Item.ListItem ? (Item.Endb) Item.endli
									: Item.endb);
			if (block.get(Attn.$keep_with_next_within_column) != Attr.auto
					|| block.get(Attn.$keep_with_next_within_page) != Attr.auto)
				context.flow.append(new Item.Keep((Attributed) block));
			Item.Space.Display display = new Item.Space.Display(block,
					"space-after");
			if (!display.nil)
				context.flow.append(display);
			if (block.get(Attn.$span) == Attr.all)
				context.flow.append(Item.endw);
			if (block.get(Attn.$break_after) != Attr.auto)
				context.flow.append(new Item.Break(block, "after"));
		}
		bow = true;
	}

	private void startOuter(Item.Frame frame) {
		context.flow.append(frame);
		context.blocks.push(frame);
		b = frame;
	}

	private void endOuter() {
		context.blocks.pop();
		b = (context.blocks.isEmpty() ? fakeblock : (Item.Block) context.blocks
				.peek());
		context.flow.append(Item.endf);
	}

	private void startInner(Item.Frame frame) {
		if (frame.get(Attn.$keep_with_previous_within_column) != Attr.auto
				|| frame.get(Attn.$keep_with_previous_within_page) != Attr.auto)
			context.flow.append(new Item.Keep((Attributed) frame));
		if (frame.get(Attn.$break_before) != Attr.auto)
			context.flow.append(new Item.Break(frame, "before"));
		if (frame.get(Attn.$clear) != Attr.none)
			context.flow.append(new Item.Clear(frame.a));
		context.flow.append(Item.empty);
		context.flow.append(frame);
		context.blocks.push(frame);
		b = frame;
		bow = true;
	}

	private void endInner() {
		Item.Block block = b;
		context.blocks.pop();
		b = (context.blocks.isEmpty() ? fakeblock : (Item.Block) context.blocks
				.peek());
		context.flow.append(Item.endf);
		if (block.get(Attn.$keep_with_next_within_column) != Attr.auto
				|| block.get(Attn.$keep_with_next_within_page) != Attr.auto)
			context.flow.append(new Item.Keep((Attributed) block));
		if (block.get(Attn.$break_after) != Attr.auto)
			context.flow.append(new Item.Break(block, "after"));
		bow = true;
	}

	private void startRotated(Attributed attributed) {
		AttList attlist = (AttList) attributed.clone();
		Attn[] attns = { Attn.$reference_orientation,
				Attn.$inline_progression_dimension_minimum,
				Attn.$inline_progression_dimension_optimum,
				Attn.$inline_progression_dimension_maximum,
				Attn.$block_progression_dimension_minimum,
				Attn.$block_progression_dimension_optimum,
				Attn.$block_progression_dimension_maximum };
		for (int i = 0; i != attns.length; i++)
			attlist.remove(attns[i]);
		Item.Frame frame = new Item.Frame(attlist, session);
		frame.mode = (short) 1;
		context.flow.append(new Item.Rotated(attributed)).append(
				new Item.Jump("rotated:" + frame.bno)).append(frame);
		context.blocks.push(frame);
		b = frame;
	}

	private void endRotated() {
		Item.Block block = b;
		context.blocks.pop();
		b = (context.blocks.isEmpty() ? fakeblock : (Item.Block) context.blocks
				.peek());
		context.flow.append(Item.endf).append(Item.eofl).append(
				new Item.Label("rotated:" + block.bno));
	}

	private void startSpan(Item.Span span) {
		if (span.writing_mode != b.writing_mode) {
			Attn[] attns = { Attn.$border_end_color, Attn.$border_start_color,
					Attn.$border_end_style, Attn.$border_start_style,
					Attn.$border_end_width_conditionality,
					Attn.$border_start_width_conditionality,
					Attn.$border_end_width_length,
					Attn.$border_start_width_length,
					Attn.$padding_end_conditionality,
					Attn.$padding_start_conditionality,
					Attn.$padding_end_length, Attn.$padding_start_length,
					Attn.$margin_end, Attn.$margin_start };
			for (int i = 0; i != attns.length; i += 2)
				swap(span, attns[i], attns[i + 1]);
		}
		if (bos == null
				&& (s.get(Attn.$keep_together_within_line) != Attr.auto || (span
						.get(Attn.$keep_with_previous_within_line) != Attr.auto)))
			tokenizer.a = (short) -1;
		Item.Space.Inline inline = new Item.Space.Inline(span,
				(span.writing_mode == s.writing_mode ? "space-start"
						: "space-end"));
		if (!inline.nil) {
			context.flow.append(inline);
			if (bos == null)
				bos = inline;
		}
		context.flow.append(span);
		if (bos == null)
			bos = span;
		context.spans.push(span);
		s = span;
	}

	private void endSpan() {
		context.flow.append(Item.ends);
		Item.Span span = s;
		context.spans.pop();
		s = (context.spans.isEmpty() ? fakespan : (Item.Span) context.spans
				.peek());
		Item.Space.Inline inline = new Item.Space.Inline(span,
				(span.writing_mode == s.writing_mode ? "space-end"
						: "space-start"));
		if (!inline.nil)
			context.flow.append(inline);
		if (bos == null
				&& (s.get(Attn.$keep_together_within_line) != Attr.auto || span
						.get(Attn.$keep_with_next_within_line) != Attr.auto))
			tokenizer.a = (short) -1;
	}

	private void startExpt(Item.Exception exception) {
		bos(tokenizer.a >= 1 ? (short) 1 : (short) 0);
		tokenizer.a = (short) 0;
		inlinecontexts.push(new InlineContext(tokenizer.a, bos, bow));
		exceptions.push(exception);
		pushContext(new Context(exception.flow));
	}

	private void endExpt() {
		popContext();
		context.flow.append(exceptions.pop());
		InlineContext inlinecontext = (InlineContext) inlinecontexts.pop();
		tokenizer.a = inlinecontext.tokenizer_a;
		bos = inlinecontext.bos;
		bow = inlinecontext.bow;
	}

	private void startRowgroup(short i, AttList attlist) {
		Flow flow = new Flow();
		Flow flow_12_ = new Flow();
		contexts.push(new Context(flow_12_, context.blocks, context.spans))
				.push(
						context = new Context(flow, context.blocks,
								context.spans));
		startBlock(new Item.Block(attlist, session));
		popContext();
		table.context.startRowgroup(i, attlist, flow, flow_12_);
	}

	private void endRowgroup(short i) {
		endBlock();
		popContext();
		table.context.endRowgroup(i);
	}

	public void startElement(short i, AttList attlist) throws CompilerException {
		id.desc_count++;
		this.key.desc_count++;
		switch (i) {
		case 17:
		case 18:
		case 19:
			break;
		case 0:
			pcdata(attlist);
			break;
		case 20:
			startBlock(new Item.Block(attlist, session));
			break;
		case 21:
			startBlock(new Item.Frame(attlist, session));
			break;
		case 24:
			b.proplist.put(i, attlist);
			break;
		case 109:
			b.proplist.put(i, attlist);
			break;
		case 22:
		case 27:
			startSpan(new Item.Span(attlist, session));
			break;
		case 28:
			startSpan(new Item.Span(attlist, session));
			break;
		case 48:
			startSpan(new Item.Anchor(attlist, session));
			break;
		case 29: {
			short i_13_ = bos(tokenizer.a >= 1 ? (short) 1 : (short) 0);
			Item.Leader leader = new Item.Leader(attlist, session);
			startSpan(leader.span = new Item.Span(attlist, session));
			context.flow.append(leader.setFlags(i_13_));
			pushContext(new Context(new Flow(), context.blocks, context.spans));
			break;
		}
		case 25: {
			short i_14_ = bos(tokenizer.a >= 1 ? (short) 1 : (short) 0);
			Item.Span span = new Item.Span(attlist, session);
			Item.Inline inline;
			if (attlist.get(Attn.$src) != Attr.none) {
				inline = new Item.Image(attlist, session);
				((Item.Image) inline).span = span;
			} else {
				AttList attlist_15_ = (AttList) attlist.clone();
				attlist_15_.put(Attn.$leader_length_minimum, Attr.newLength(0));
				attlist_15_.put(Attn.$leader_length_optimum, Attr.newLength(0));
				attlist_15_.put(Attn.$leader_length_maximum, Attr.newLength(0));
				inline = new Item.Leader(attlist_15_, session);
				((Item.Leader) inline).span = span;
			}
			startSpan(span);
			context.flow.append(inline.setFlags(i_14_));
			tokenizer.a = (short) 0;
			endSpan();
			break;
		}
		case 46:
			fn = new Item.Footnote(attlist);
			break;
		case 47:
			startExpt(fn);
			startBlock(new Item.Block(attlist, session));
			break;
		case 30:
			startExpt(new Item.PageNumber(attlist));
			break;
		case 31:
			context.flow.append(new Item.PageId(attlist, session));
			break;
		case 201:
			context.flow.append(new Item.PageLid(attlist, session));
			break;
		case 108:
			startExpt(new Item.PageKey(attlist));
			break;
		case 110:
			((Item.PageKey) exceptions.car()).terms.snoc(new Item.PageKey.Term(
					attlist, session));
			break;
		case 41:
			startBlock(list = new Item.ListBlock(attlist, new ListContext(
					attlist, session)));
			lists.push(list);
			break;
		case 42: {
			Flow flow = new Flow();
			Flow flow_16_ = new Flow();
			contexts.push(new Context(flow_16_, context.blocks, context.spans))
					.push(
							context = new Context(flow, context.blocks,
									context.spans));
			startBlock(new Item.ListItem(attlist, session));
			list.context.startRow(attlist, flow, flow_16_);
			break;
		}
		case 43:
		case 44: {
			Flow flow = new Flow();
			pushContext(new Context(flow, context.blocks, context.spans));
			list.context.startCell(attlist, flow);
			startBlock(new Item.Block(attlist, session));
			break;
		}
		case 32: {
			startBlock(new Item.Block(attlist, session));
			Attr attr = b.get(Attn.$caption_side);
			if (attr == Attr.newWord("after") || attr == Attr.newWord("end"))
				captions.push(new Flow());
			break;
		}
		case 33: {
			Attr attr = b.get(Attn.$caption_side);
			if (attr == Attr.newWord("after") || attr == Attr.newWord("end")) {
				Flow flow = (Flow) captions.peek();
				pushContext(new Context(flow, context.blocks, context.spans));
				attlist
						.put(Attn.$keep_with_previous_within_column,
								Attr.always);
				captions.push(flow);
			} else
				attlist.put(Attn.$keep_with_next_within_column, Attr.always);
			startBlock(new Item.Frame(attlist, session));
			break;
		}
		case 34:
			attlist.put(Attn.$table_omit_footer_at_break, Attr.trueval);
			startBlock(table = new Item.Table(attlist, new TableContext(
					attlist, session)));
			tables.push(table);
			break;
		case 35:
			table.context.column(attlist);
			break;
		case 36:
			attlist.put(Attn.$keep_together_within_column, Attr.always);
			attlist.put(Attn.$keep_with_next_within_column, Attr.always);
			startRowgroup((short) 0, attlist);
			break;
		case 37:
			startRowgroup((short) 2, attlist);
			break;
		case 38:
			startRowgroup((short) 1, attlist);
			break;
		case 39:
			table.context.startRow(attlist);
			break;
		case 40: {
			Flow flow = new Flow();
			Flow flow_17_ = new Flow();
			Flow flow_18_ = new Flow();
			AttList attlist_19_ = new AttList();
			AttList attlist_20_ = (AttList) attlist.clone();
			attlist_19_.put(Attn.$writing_mode, table.get(Attn.$writing_mode));
			attlist_19_.put(Attn.$intrusion_displace, table
					.get(Attn.$intrusion_displace));
			Attr.Length length = (Attr.newLength(table.get(
					Attn.$border_separation_block_progression_direction)
					.length() / 2));
			attlist_20_.put(Attn.$margin_before, length);
			attlist_20_.put(Attn.$margin_after, length);
			attlist_20_.put(Attn.$margin_start, Attr.zerolength);
			attlist_20_.put(Attn.$margin_end, Attr.zerolength);
			attlist_20_.remove(Attn.$keep_together_within_column);
			attlist_20_.remove(Attn.$keep_together_within_page);
			pushContext(new Context(flow_17_, context.blocks, context.spans));
			context.flow = flow;
			Item.Frame frame;
			startOuter(frame = new Item.Frame(attlist_19_, session));
			Item.Frame frame_21_;
			if (rotated(attlist)) {
				AttList attlist_22_ = new AttList();
				arot(attlist_20_, attlist_22_);
				startInner(frame_21_ = new Item.Frame(attlist_20_, session));
				context.flow = flow_17_;
				pushContext(new Context(flow_18_, context.blocks, context.spans));
				startRotated(attlist_22_);
			} else {
				startInner(frame_21_ = new Item.Frame(attlist_20_, session));
				context.flow = flow_17_;
				pushContext(new Context(flow_18_, context.blocks, context.spans));
			}
			frame_21_.collapse = false;
			table.context.startCell(attlist, frame, frame_21_, flow, flow_18_,
					flow_17_);
			break;
		}
		case 49:
			break;
		case 50: {
			Flow flow = new Flow();
			Hashtable hashtable = (b.bno.intValue() > s.sno.intValue() ? b.markers != null ? b.markers
					: (b.markers = new Hashtable())
					: s.markers != null ? s.markers
							: (s.markers = new Hashtable()));
			Attr attr = attlist.get(Attn.$marker_class_name);
			hashtable.put(attr, flow);
			pushContext(new Context(flow));
			break;
		}
		case 51:
			startExpt(new Item.Remark(attlist));
			break;
		case 45: {
			AttList attlist_23_ = new AttList();
			if (attlist.get(Attn.$float) != Attr.none) {
				startExpt(new Item.Float(attlist));
				attlist_23_.put(Attn.$float, attlist.get(Attn.$float));
			}
			startBlock(new Item.Frame(attlist_23_, session));
			break;
		}
		case 101:
			attlist.put(Attn.$span, Attr.all);
			startBlock(new Item.Frame(attlist, session));
			break;
		case 107:
			context.flow.append(new Item.Pinpoint(attlist));
			break;
		case 111: {
			Item.Key key = new Item.Key((Attributed) attlist.clone(), session);
			context.flow.append(key);
			attlist.remove(Attn.$key);
			break;
		}
		case 112:
			context.flow.append(new Item.Endk(attlist, attlist
					.get(Attn.$ref_id)));
			break;
		case 23:
		case 26:
			pushContext(new Context(new Flow(), context.blocks, context.spans));
			break;
		default:
			throw new CompilerException("Invalid element " + Elem.getName(i));
		}
		if (attlist.containsKey(Attn.$id))
			pushId(attlist);
		if (attlist.containsKey(Attn.$key))
			pushKey(attlist);
	}

	public void endElement(short i) throws CompilerException {
		switch (i) {
		case 17:
		case 18:
		case 19:
			break;
		case 0:
			break;
		case 20:
			endBlock();
			break;
		case 21:
			endBlock();
			break;
		case 24:
			break;
		case 109:
			break;
		case 22:
		case 27:
		case 48:
			endSpan();
			break;
		case 28:
			endSpan();
			break;
		case 29:
			popContext();
			tokenizer.a = (short) 0;
			endSpan();
			bow = true;
			break;
		case 25:
			bow = true;
			break;
		case 46:
			break;
		case 47:
			endBlock();
			endExpt();
			break;
		case 30:
			endExpt();
			break;
		case 31:
			break;
		case 201:
			break;
		case 108:
			endExpt();
			break;
		case 110:
			break;
		case 41:
			list.context.gen(list, context.flow);
			lists.pop();
			list = lists.isEmpty() ? null : (Item.ListBlock) lists.peek();
			endBlock();
			break;
		case 42:
			popContext();
			endBlock();
			popContext();
			list.context.endRow();
			break;
		case 43:
		case 44:
			endBlock();
			popContext();
			list.context.endCell();
			break;
		case 32: {
			Attr attr = b.get(Attn.$caption_side);
			endBlock();
			if (attr == Attr.newWord("after") || attr == Attr.newWord("end")) {
				Flow flow = (Flow) captions.pop();
				context.flow.append(flow);
			}
			break;
		}
		case 33: {
			endBlock();
			Attr attr = b.get(Attn.$caption_side);
			if (attr == Attr.newWord("after") || attr == Attr.newWord("end"))
				popContext();
			break;
		}
		case 34:
			table.context.gen(table, context.flow);
			tables.pop();
			table = tables.isEmpty() ? null : (Item.Table) tables.peek();
			endBlock();
			break;
		case 35:
			break;
		case 36:
			endRowgroup((short) 0);
			break;
		case 37:
			endRowgroup((short) 2);
			break;
		case 38:
			endRowgroup((short) 1);
			break;
		case 39:
			table.context.endRow();
			break;
		case 40:
			if (((Item.Frame) b).mode == 1)
				endRotated();
			popContext();
			endInner();
			endOuter();
			popContext();
			table.context.endCell();
			break;
		case 49:
			break;
		case 50:
			popContext();
			break;
		case 51:
			endExpt();
			break;
		case 45: {
			Attr attr = b.get(Attn.$float);
			endBlock();
			if (attr != Attr.none)
				endExpt();
			break;
		}
		case 101:
			endBlock();
			break;
		case 107:
			break;
		case 111:
			break;
		case 112:
			break;
		case 23:
		case 26:
			popContext();
			break;
		default:
			throw new CompilerException("Invalid element " + Elem.getName(i));
		}
		if (id.desc_count == 0)
			popId();
		id.desc_count--;
		if (key.desc_count == 0)
			popKey();
		key.desc_count--;
	}
}