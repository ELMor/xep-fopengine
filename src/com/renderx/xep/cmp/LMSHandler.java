/*
 * LMSHandler - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

import com.renderx.util.Hashtable;
import com.renderx.xep.lib.InternalException;
import com.renderx.xep.lib.Session;
import com.renderx.xep.pre.AttList;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.CompilerException;
import com.renderx.xep.pre.ElementHandler;
import com.renderx.xep.pre.PI;
import com.renderx.xep.pre.PIHandler;

class LMSHandler implements ElementHandler, PIHandler {
	private final Session session;

	private final Hashtable masters;

	private Hashtable pmtab = new Hashtable();

	private PM pm = null;

	private AttList body = null;

	private AttList before = null;

	private AttList after = null;

	private AttList start = null;

	private AttList end = null;

	private int w;

	private int h;

	private SM sm = null;

	private SM.PMSet set = null;

	LMSHandler(Hashtable hashtable, Session session) {
		masters = hashtable;
		this.session = session;
	}

	public void startElement(short i, AttList attlist) throws CompilerException {
		switch (i) {
		case 10: {
			Attr attr = attlist.get(Attn.$master_name);
			this.pm = getpm(attr);
			session.openState("sequence-master");
			session.event("master-name", attr.word());
			sm = new SM(attlist);
			masters.put(attr, sm);
			set = new SM.PMSet();
			set.refs.append(new SM.PMRef(this.pm, -1));
			this.pm.bind(attlist);
			Area.fixBorderWidth(this.pm.a);
			Attr attr_0_ = this.pm.get(Attn.$page_width);
			Attr attr_1_ = this.pm.get(Attn.$page_height);
			if (attr_0_ instanceof Attr.Word) {
				if (attr_0_ == Attr.newWord("indefinite"))
					attr_0_ = Attr.newLength(session.config.MAX_PAGE_WIDTH);
				else if (attr_0_ == Attr.auto)
					attr_0_ = Attr.newLength(session.config.PAGE_WIDTH);
				else
					session.warning("page-width=" + attr_0_.word()
							+ " must be converted to length in the parser");
				this.pm.put(Attn.$page_width, attr_0_);
			}
			if (attr_1_ instanceof Attr.Word) {
				if (attr_1_ == Attr.newWord("indefinite"))
					attr_1_ = Attr.newLength(session.config.MAX_PAGE_HEIGHT);
				else if (attr_1_ == Attr.auto)
					attr_1_ = Attr.newLength(session.config.PAGE_HEIGHT);
				else
					session.warning("page-height=" + attr_1_.word()
							+ " must be converted to length in the parser");
				this.pm.put(Attn.$page_height, attr_1_);
			}
			w = attr_0_.length();
			h = attr_1_.length();
			if (w < session.config.MIN_PAGE_WIDTH)
				this.pm.put(Attn.$page_width, Attr
						.newLength(w = session.config.MIN_PAGE_WIDTH));
			if (h < session.config.MIN_PAGE_HEIGHT)
				this.pm.put(Attn.$page_height, Attr
						.newLength(h = session.config.MIN_PAGE_HEIGHT));
			if (w > session.config.MAX_PAGE_WIDTH)
				this.pm.put(Attn.$page_width, Attr
						.newLength(w = session.config.MAX_PAGE_WIDTH));
			if (h > session.config.MAX_PAGE_HEIGHT)
				this.pm.put(Attn.$page_height, Attr
						.newLength(h = session.config.MAX_PAGE_HEIGHT));
			String[] strings = { "before", "after", "start", "end" };
			for (int i_2_ = 0; i_2_ != strings.length; i_2_++) {
				Attn attn = Attn.oldName("margin-" + strings[i_2_]);
				if (this.pm.get(attn) == Attr.auto)
					this.pm.put(attn, Attr.zerolength);
			}
			this.pm.dimensions(w, h);
			break;
		}
		case 5: {
			Attr attr = attlist.get(Attn.$master_name);
			session.openState("sequence-master");
			session.event("master-name", attr.word());
			sm = new SM(attlist);
			masters.put(attr, sm);
			break;
		}
		case 6:
			set = new SM.PMSet(1);
			set.refs.append(new SM.PMRef(getpm(attlist
					.get(Attn.$master_reference)), -1));
			break;
		case 7:
			set = new SM.PMSet(attlist.get(Attn.$maximum_repeats).count());
			set.refs.append(new SM.PMRef(getpm(attlist
					.get(Attn.$master_reference)), -1));
			break;
		case 8:
			set = new SM.PMSet(attlist.get(Attn.$maximum_repeats).count());
			break;
		case 9: {
			PM pm = getpm(attlist.get(Attn.$master_reference));
			int i_3_ = 0;
			Attr attr = attlist.get(Attn.$page_position);
			if (attr == Attr.newWord("any"))
				i_3_ |= 0x7;
			else if (attr == Attr.newWord("first"))
				i_3_ |= 0x1;
			else if (attr == Attr.newWord("last"))
				i_3_ |= 0x2;
			else if (attr == Attr.newWord("rest"))
				i_3_ |= 0x4;
			else
				throw new InternalException("illegal value for page-position:"
						+ attr);
			Attr attr_4_ = attlist.get(Attn.$blank_or_not_blank);
			if (attr_4_ == Attr.newWord("any"))
				i_3_ |= 0x18;
			else if (attr_4_ == Attr.newWord("blank"))
				i_3_ |= 0x8;
			else if (attr_4_ == Attr.newWord("not-blank"))
				i_3_ |= 0x10;
			else
				throw new InternalException(
						"illegal value for blank-or-not-blank:" + attr_4_);
			Attr attr_5_ = attlist.get(Attn.$odd_or_even);
			if (attr_5_ == Attr.newWord("any"))
				i_3_ |= 0x60;
			else if (attr_5_ == Attr.newWord("odd"))
				i_3_ |= 0x20;
			else if (attr_5_ == Attr.newWord("even"))
				i_3_ |= 0x40;
			else
				throw new InternalException("illegal value for odd-or-even:"
						+ attr_5_);
			set.refs.append(new SM.PMRef(pm, i_3_));
			if ((i_3_ & 0x3) != 0) {
				i_3_ = i_3_ & ~0x3 | 0x80;
				set.refs.append(new SM.PMRef(pm, i_3_));
			}
			break;
		}
		case 11:
			body = attlist;
			break;
		case 12:
			before = attlist;
			break;
		case 13:
			after = attlist;
			break;
		case 14:
			start = attlist;
			break;
		case 15:
			end = attlist;
			break;
		default:
			throw new CompilerException("invalid element id: " + i);
		}
	}

	public void endElement(short i) throws CompilerException {
		switch (i) {
		case 10:
			sm.sets.append(set);
			set = null;
			sm = null;
			session.closeState("sequence-master");
			if (pm.get(Attn.$writing_mode) == Attr.rl_tb) {
				if (body != null) {
					Area.fixBorderWidth(body);
					pm.regtab.put(
							(body.containsKey(Attn.$region_name) ? (Attr) body
									.get(Attn.$region_name) : Attr
									.newWord("xsl-region-body")), new Region(
									pm.pagerect, body));
				}
				if (before != null) {
					Area.fixBorderWidth(before);
					pm.regtab
							.put(
									(before.containsKey(Attn.$region_name) ? (Attr) before
											.get(Attn.$region_name)
											: Attr.newWord("xsl-region-before")),
									new Region(
											(before.get(Attn.$precedence)
													.bool() ? (new int[] {
													pm.pagerect[0],
													(pm.pagerect[3] - before
															.get(Attn.$extent)
															.length_or_ratio(h)),
													pm.pagerect[2],
													pm.pagerect[3] })
													: new int[] {
															(end != null ? (pm.pagerect[0] + end
																	.get(
																			Attn.$extent)
																	.length_or_ratio(
																			h))
																	: pm.pagerect[0]),
															(pm.pagerect[3] - before
																	.get(
																			Attn.$extent)
																	.length_or_ratio(
																			h)),
															(start != null ? (pm.pagerect[2] - start
																	.get(
																			Attn.$extent)
																	.length_or_ratio(
																			w))
																	: pm.pagerect[2]),
															pm.pagerect[3] }),
											before));
				}
				if (after != null) {
					Area.fixBorderWidth(after);
					pm.regtab
							.put(
									(after.containsKey(Attn.$region_name) ? (Attr) after
											.get(Attn.$region_name)
											: Attr.newWord("xsl-region-after")),
									(new Region(
											(after.get(Attn.$precedence).bool() ? (new int[] {
													pm.pagerect[0],
													pm.pagerect[1],
													pm.pagerect[2],
													pm.pagerect[1]
															+ after
																	.get(
																			Attn.$extent)
																	.length_or_ratio(
																			h) })
													: new int[] {
															(end != null ? (pm.pagerect[0] + end
																	.get(
																			Attn.$extent)
																	.length_or_ratio(
																			h))
																	: pm.pagerect[0]),
															pm.pagerect[1],
															(start != null ? (pm.pagerect[2] - start
																	.get(
																			Attn.$extent)
																	.length_or_ratio(
																			h))
																	: pm.pagerect[2]),
															pm.pagerect[1]
																	+ after
																			.get(
																					Attn.$extent)
																			.length_or_ratio(
																					w) }),
											after)));
				}
				if (start != null) {
					Area.fixBorderWidth(start);
					pm.regtab
							.put(
									(start.containsKey(Attn.$region_name) ? (Attr) start
											.get(Attn.$region_name)
											: Attr.newWord("xsl-region-start")),
									new Region(
											(new int[] {
													(pm.pagerect[2] - start
															.get(Attn.$extent)
															.length_or_ratio(w)),
													((after != null && after
															.get(
																	Attn.$precedence)
															.bool()) ? (pm.pagerect[1] + after
															.get(Attn.$extent)
															.length_or_ratio(w))
															: pm.pagerect[1]),
													pm.pagerect[2],
													((before != null && before
															.get(
																	Attn.$precedence)
															.bool()) ? (pm.pagerect[3] - before
															.get(Attn.$extent)
															.length_or_ratio(w))
															: pm.pagerect[3]) }),
											start));
				}
				if (end != null) {
					Area.fixBorderWidth(end);
					pm.regtab
							.put(
									(end.containsKey(Attn.$region_name) ? (Attr) end
											.get(Attn.$region_name)
											: Attr.newWord("xsl-region-end")),
									new Region(
											(new int[] {
													pm.pagerect[0],
													((after != null && after
															.get(
																	Attn.$precedence)
															.bool()) ? (pm.pagerect[1] + after
															.get(Attn.$extent)
															.length_or_ratio(w))
															: pm.pagerect[1]),
													(pm.pagerect[0] + end.get(
															Attn.$extent)
															.length_or_ratio(w)),
													((before != null && before
															.get(
																	Attn.$precedence)
															.bool()) ? (pm.pagerect[3] - before
															.get(Attn.$extent)
															.length_or_ratio(w))
															: pm.pagerect[3]) }),
											end));
				}
			} else {
				if (body != null) {
					Area.fixBorderWidth(body);
					pm.regtab.put(
							(body.containsKey(Attn.$region_name) ? (Attr) body
									.get(Attn.$region_name) : Attr
									.newWord("xsl-region-body")), new Region(
									pm.pagerect, body));
				}
				if (before != null) {
					Area.fixBorderWidth(before);
					pm.regtab
							.put(
									(before.containsKey(Attn.$region_name) ? (Attr) before
											.get(Attn.$region_name)
											: Attr.newWord("xsl-region-before")),
									new Region(
											(before.get(Attn.$precedence)
													.bool() ? (new int[] {
													pm.pagerect[0],
													(pm.pagerect[3] - before
															.get(Attn.$extent)
															.length_or_ratio(h)),
													pm.pagerect[2],
													pm.pagerect[3] })
													: new int[] {
															(start != null ? (pm.pagerect[0] + start
																	.get(
																			Attn.$extent)
																	.length_or_ratio(
																			h))
																	: pm.pagerect[0]),
															(pm.pagerect[3] - before
																	.get(
																			Attn.$extent)
																	.length_or_ratio(
																			h)),
															(end != null ? (pm.pagerect[2] - end
																	.get(
																			Attn.$extent)
																	.length_or_ratio(
																			w))
																	: pm.pagerect[2]),
															pm.pagerect[3] }),
											before));
				}
				if (after != null) {
					Area.fixBorderWidth(after);
					pm.regtab
							.put(
									(after.containsKey(Attn.$region_name) ? (Attr) after
											.get(Attn.$region_name)
											: Attr.newWord("xsl-region-after")),
									(new Region(
											(after.get(Attn.$precedence).bool() ? (new int[] {
													pm.pagerect[0],
													pm.pagerect[1],
													pm.pagerect[2],
													pm.pagerect[1]
															+ after
																	.get(
																			Attn.$extent)
																	.length_or_ratio(
																			h) })
													: new int[] {
															(start != null ? (pm.pagerect[0] + start
																	.get(
																			Attn.$extent)
																	.length_or_ratio(
																			h))
																	: pm.pagerect[0]),
															pm.pagerect[1],
															(end != null ? (pm.pagerect[2] - end
																	.get(
																			Attn.$extent)
																	.length_or_ratio(
																			h))
																	: pm.pagerect[2]),
															pm.pagerect[1]
																	+ after
																			.get(
																					Attn.$extent)
																			.length_or_ratio(
																					w) }),
											after)));
				}
				if (start != null) {
					Area.fixBorderWidth(start);
					pm.regtab
							.put(
									(start.containsKey(Attn.$region_name) ? (Attr) start
											.get(Attn.$region_name)
											: Attr.newWord("xsl-region-start")),
									new Region(
											(new int[] {
													pm.pagerect[0],
													((after != null && after
															.get(
																	Attn.$precedence)
															.bool()) ? (pm.pagerect[1] + after
															.get(Attn.$extent)
															.length_or_ratio(w))
															: pm.pagerect[1]),
													(pm.pagerect[0] + start
															.get(Attn.$extent)
															.length_or_ratio(w)),
													((before != null && before
															.get(
																	Attn.$precedence)
															.bool()) ? (pm.pagerect[3] - before
															.get(Attn.$extent)
															.length_or_ratio(w))
															: pm.pagerect[3]) }),
											start));
				}
				if (end != null) {
					Area.fixBorderWidth(end);
					pm.regtab
							.put(
									(end.containsKey(Attn.$region_name) ? (Attr) end
											.get(Attn.$region_name)
											: Attr.newWord("xsl-region-end")),
									new Region(
											(new int[] {
													(pm.pagerect[2] - end.get(
															Attn.$extent)
															.length_or_ratio(w)),
													((after != null && after
															.get(
																	Attn.$precedence)
															.bool()) ? (pm.pagerect[1] + after
															.get(Attn.$extent)
															.length_or_ratio(w))
															: pm.pagerect[1]),
													pm.pagerect[2],
													((before != null && before
															.get(
																	Attn.$precedence)
															.bool()) ? (pm.pagerect[3] - before
															.get(Attn.$extent)
															.length_or_ratio(w))
															: pm.pagerect[3]) }),
											end));
				}
			}
			pm = null;
			body = before = after = start = end = null;
			break;
		case 5:
			sm = null;
			session.closeState("sequence-master");
			break;
		case 6:
			sm.sets.append(set);
			set = null;
			break;
		case 7:
			sm.sets.append(set);
			set = null;
			break;
		case 8:
			sm.sets.append(set);
			set = null;
			break;
		case 9:
			break;
		case 11:
			break;
		case 12:
			break;
		case 13:
			break;
		case 14:
			break;
		case 15:
			break;
		default:
			throw new CompilerException("invalid element id: " + i);
		}
	}

	public void processingInstruction(String string, String string_6_)
			throws CompilerException {
		if (pm != null)
			pm.pilist.append(new PI(string, string_6_));
	}

	private PM getpm(Attr attr) {
		PM pm = (PM) pmtab.get(attr);
		if (pm == null)
			pmtab.put(attr, pm = new PM());
		return pm;
	}
}