/*
 * SM - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

import java.util.Enumeration;

import com.renderx.util.List;
import com.renderx.xep.pre.Attributed;

public class SM extends Xattr {
	public static final short FIRST = 1;

	public static final short LAST = 2;

	public static final short REST = 4;

	public static final short BLANK = 8;

	public static final short FILLED = 16;

	public static final short ODD = 32;

	public static final short EVEN = 64;

	public static final short SINGLE = 128;

	public static final short ANY = -1;

	List sets = new List();

	private Enumeration eset;

	private int num;

	private int prenum;

	private PMSet cus = null;

	private PMSet pres = null;

	static class PMRef {
		PM pm;

		int guard;

		PMRef(PM pm, int i) {
			this.pm = pm;
			guard = i;
		}
	}

	static class PMSet {
		List refs;

		int max;

		PMSet() {
			refs = new List();
			max = -1;
		}

		PMSet(int i) {
			refs = new List();
			max = i;
		}
	}

	public SM(Attributed attributed) {
		super(attributed);
		sets.append(new PMSet(0));
	}

	public void rewind() {
		eset = sets.elements();
		cus = (PMSet) eset.nextElement();
	}

	public void retry() {
		eset = sets.elements();
		while (eset.hasMoreElements()) {
			cus = (PMSet) eset.nextElement();
			if (cus == pres)
				break;
		}
		num = prenum;
	}

	public PM pm(int i) throws NoPageMasterException {
		pres = cus;
		prenum = num;
		for (;;) {
			if (num != cus.max) {
				Enumeration enumeration = cus.refs.elements();
				while (enumeration.hasMoreElements()) {
					PMRef pmref = (PMRef) enumeration.nextElement();
					if ((i & pmref.guard) == i) {
						num++;
						return pmref.pm;
					}
				}
			}
			if (!eset.hasMoreElements())
				break;
			cus = (PMSet) eset.nextElement();
			num = 0;
		}
		throw new NoPageMasterException("state: " + i);
	}
}