/*
 * ExceptionDispatcher - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

public interface ExceptionDispatcher {
	public void process(Item.Exception exception);

	public void process(Item.Absolute absolute);

	public void process(Item.Fixed fixed);

	public void process(Item.Rotated rotated);

	public void process(Item.Footnote footnote);

	public void process(Item.FootnotesPending footnotespending);

	public void process(Item.PageNumber pagenumber);

	public void process(Item.PageKey pagekey);

	public void process(Item.Tag tag);

	public void process(Item.Id id);

	public void process(Item.Key key);

	public void process(Item.Endk endk);

	public void process(Item.Pinpoint pinpoint);

	public void process(Item.Keep keep);

	public void process(Item.Break var_break);

	public void process(Item.Remark remark);

	public void process(Item.Float var_float);

	public void process(Item.Clear clear);
}