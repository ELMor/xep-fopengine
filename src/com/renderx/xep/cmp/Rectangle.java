/*
 * Rectangle - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

public class Rectangle {
	private static int[][] RM = { { 1, 0, 0, 1 }, { 0, 1, -1, 0 },
			{ -1, 0, 0, -1 }, { 0, -1, 1, 0 } };

	public static int[] rotate(int[] is, int i) {
		int[] is_0_ = new int[4];
		int[] is_1_ = RM[i / 90];
		is_0_[0] = is[0] * is_1_[0] + is[1] * is_1_[1];
		is_0_[1] = is[0] * is_1_[2] + is[1] * is_1_[3];
		is_0_[2] = is[2] * is_1_[0] + is[3] * is_1_[1];
		is_0_[3] = is[2] * is_1_[2] + is[3] * is_1_[3];
		if (is_0_[0] > is_0_[2]) {
			int i_2_ = is_0_[2];
			is_0_[2] = is_0_[0];
			is_0_[0] = i_2_;
		}
		if (is_0_[1] > is_0_[3]) {
			int i_3_ = is_0_[3];
			is_0_[3] = is_0_[1];
			is_0_[1] = i_3_;
		}
		return is_0_;
	}

	public static int[] shift(int[] is, int i, int i_4_) {
		int[] is_5_ = { is[0] + i, is[1] + i_4_, is[2] + i, is[3] + i_4_ };
		return is_5_;
	}

	public static int[] shrink(int[] is, int[] is_6_) {
		int[] is_7_ = { is[0] + is_6_[0], is[1] + is_6_[1], is[2] - is_6_[2],
				is[3] - is_6_[3] };
		return is_7_;
	}
}