/*
 * Xattr - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

import com.renderx.xep.pre.AttList;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Attributed;

public abstract class Xattr implements Attributed, Cloneable {
	public Attributed a = null;

	private AttList b = null;

	public Xattr() {
		/* empty */
	}

	public void bind(Attributed attributed) {
		a = attributed;
	}

	public void bind(Xattr xattr_0_) {
		a = xattr_0_.b == null ? (Attributed) xattr_0_.a : xattr_0_;
	}

	public Xattr(Attributed attributed) {
		bind(attributed);
	}

	public Xattr(Xattr xattr_1_) {
		bind(xattr_1_);
	}

	public boolean containsKey(Attn attn) {
		return b != null && b.containsKey(attn) || a.containsKey(attn);
	}

	public Attr remove(Attn attn) {
		Attr attr = null;
		Object object = null;
		if (b != null)
			attr = b.remove(attn);
		Attr attr_2_ = a.remove(attn);
		return attr != null ? attr : attr_2_;
	}

	public Attr reset(Attn attn) {
		return b != null ? b.remove(attn) : null;
	}

	public void resetall() {
		b = null;
	}

	public final Attr get(Attn attn) {
		return b != null && b.containsKey(attn) ? b.get(attn) : a.get(attn);
	}

	public final void put(Attn attn, Attr attr) {
		if (b == null)
			b = new AttList();
		b.put(attn, attr);
	}

	public Object clone() {
		try {
			Xattr xattr_3_ = (Xattr) super.clone();
			if (b != null)
				xattr_3_.b = (AttList) b.clone();
			return xattr_3_;
		} catch (CloneNotSupportedException clonenotsupportedexception) {
			return null;
		}
	}
}