/*
 * Flow - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

import java.util.Enumeration;

import com.renderx.util.Hashtable;
import com.renderx.util.List;
import com.renderx.xep.lib.Session;

public class Flow extends List {
	public void ld(Session session) {
		terminate();
		Hashtable hashtable = new Hashtable();
		List list = new List();
		jtab(hashtable, list);
		Enumeration enumeration = list.elements();
		while (enumeration.hasMoreElements()) {
			Item.Fctl fctl = (Item.Fctl) enumeration.nextElement();
			if (hashtable.containsKey(fctl.id))
				fctl.adr = (Flow) hashtable.get(fctl.id);
			else
				session.error("undefined label " + fctl.id);
		}
	}

	private void jtab(Hashtable hashtable, List list) {
		Flow flow_0_ = this;
		while (!flow_0_.isEmpty()) {
			Item item = (Item) flow_0_.car();
			if (item instanceof Item.Exception) {
				if (((Item.Exception) item).flow != null) {
					((Item.Exception) item).flow.terminate();
					((Item.Exception) item).flow.jtab(hashtable, list);
				}
			} else if (item instanceof Item.Block) {
				if (((Item.Block) item).markers != null) {
					Enumeration enumeration = ((Item.Block) item).markers
							.elements();
					while (enumeration.hasMoreElements()) {
						Flow flow_1_ = (Flow) enumeration.nextElement();
						flow_1_.terminate();
						flow_1_.jtab(hashtable, list);
					}
				}
				if (item instanceof Item.Table) {
					((Item.Table) item).header.terminate();
					((Item.Table) item).header.jtab(hashtable, list);
					((Item.Table) item).footer.terminate();
					((Item.Table) item).footer.jtab(hashtable, list);
				}
			} else if (item instanceof Item.Span
					&& ((Item.Span) item).markers != null) {
				Enumeration enumeration = ((Item.Span) item).markers.elements();
				while (enumeration.hasMoreElements()) {
					Flow flow_2_ = (Flow) enumeration.nextElement();
					flow_2_.terminate();
					flow_2_.jtab(hashtable, list);
				}
			}
			flow_0_ = (Flow) flow_0_.cdr();
			if (item instanceof Item.Label)
				hashtable.put(((Item.Label) item).id, flow_0_);
			else if ((item instanceof Item.Jump || item instanceof Item.Fork)
					&& ((Item.Fctl) item).id != null)
				list.cons(item);
		}
	}

	private void terminate() {
		keepup();
		if (this.isEmpty() || !(this.last() instanceof Item.Eofl))
			this.append(Item.eofl);
	}

	private void keepup() {
		Flow flow_3_ = this;
		Item item = null;
		Flow flow_4_ = null;
		boolean bool = false;
		for (/**/; !flow_3_.isEmpty(); flow_3_ = (Flow) flow_3_.cdr()) {
			Item item_5_ = (Item) flow_3_.car();
			if (item != null && item_5_ instanceof Item.Keep) {
				if (!bool) {
					flow_4_.setCar(item_5_);
					for (;;) {
						flow_4_ = (Flow) flow_4_.cdr();
						Item item_6_ = (Item) flow_4_.car();
						flow_4_.setCar(item);
						if (item_6_ instanceof Item.Keep)
							break;
						item = item_6_;
					}
				}
				item = null;
				flow_4_ = null;
			} else {
				if ((item_5_ instanceof Item.Text && (((Item.Text) item_5_).nwsp != ((Item.Text) item_5_).chars.length))
						|| item_5_ instanceof Item.Ends) {
					bool = true;
					continue;
				}
				if (bool) {
					item = item_5_;
					flow_4_ = flow_3_;
				}
			}
			bool = false;
		}
	}
}