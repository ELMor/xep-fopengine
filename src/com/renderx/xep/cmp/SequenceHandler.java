/*
 * SequenceHandler - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

import com.renderx.util.Hashtable;
import com.renderx.xep.lib.Session;
import com.renderx.xep.pre.AttList;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.CompilerException;
import com.renderx.xep.pre.ElementHandler;
import com.renderx.xep.pre.PI;
import com.renderx.xep.pre.PIHandler;

class SequenceHandler implements ElementHandler, PIHandler {
	private final Sequence seq;

	private final Session session;

	private Flow sc = null;

	private ElementHandler subhandler = null;

	public SequenceHandler(Sequence sequence, Hashtable hashtable,
			Session session) throws CompilerException {
		seq = sequence;
		this.session = session;
		Attr attr = sequence.get(Attn.$master_reference);
		sequence.master = (SM) hashtable.get(attr);
		session.event("master-reference", attr.word());
		if (sequence.master == null)
			throw new NoMasterException(attr);
	}

	public void startElement(short i, AttList attlist) throws CompilerException {
		if (subhandler == null) {
			switch (i) {
			case 17:
				if (seq.title != null)
					session
							.error("invalid input: title may de defined only once");
				session.openState("title");
				subhandler = new FlowHandler(seq.title = new Flow(), session);
				subhandler.startElement(i, attlist);
				break;
			case 19:
				if (seq.flow != null)
					session
							.error("invalid input: flow may de defined only once");
				session.openState("flow");
				seq.flowname = attlist.get(Attn.$flow_name);
				session.event("flow-name", seq.flowname.word());
				subhandler = new FlowHandler(seq.flow = new Flow(), session);
				if (seq.containsKey(Attn.$id))
					seq.flow.cons(new Item.Id(
							(com.renderx.xep.pre.Attributed) seq));
				if (seq.containsKey(Attn.$key))
					seq.flow.cons(new Item.Key(seq, session));
				subhandler.startElement(i, attlist);
				break;
			case 18:
				session.openState("static-content");
				session.event("flow-name", attlist.get(Attn.$flow_name).word());
				subhandler = new FlowHandler(sc = new Flow(), session);
				seq.sctab.put(attlist.get(Attn.$flow_name), sc);
				subhandler.startElement(i, attlist);
				break;
			default:
				throw new CompilerException("invalid element id: " + i);
			}
		} else
			subhandler.startElement(i, attlist);
	}

	public void endElement(short i) throws CompilerException {
		if (subhandler == null) {
			switch (i) {
			default:
				throw new CompilerException("invalid element id: " + i);
			}
		}
		subhandler.endElement(i);
		switch (i) {
		case 17:
			seq.title.ld(session);
			session.closeState("title");
			subhandler = null;
			break;
		case 19:
			seq.flow.ld(session);
			session.closeState("flow");
			subhandler = null;
			break;
		case 18:
			sc.ld(session);
			session.closeState("static-content");
			subhandler = null;
			break;
		}
	}

	public void processingInstruction(String string, String string_0_)
			throws CompilerException {
		if (subhandler == null)
			seq.pilist.append(new PI(string, string_0_));
		else if (subhandler instanceof PIHandler)
			((PIHandler) subhandler).processingInstruction(string, string_0_);
	}
}