/*
 * Region - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.cmp;

import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Attributed;

public class Region extends Area {
	public int phi = a.get(Attn.$reference_orientation).count();

	public int[] rc;

	public Region(int[] is, Attributed attributed) {
		super(attributed);
		String[] strings = { "before", "after", "start", "end" };
		for (int i = 0; i != strings.length; i++) {
			Attn attn = Attn.oldName("margin-" + strings[i]);
			if (a.get(attn) == Attr.auto)
				this.put(attn, Attr.zerolength);
		}
		if (attributed.get(Attn.$writing_mode) == Attr.rl_tb) {
			is = (int[]) is.clone();
			int i = is[2];
			is[2] = -is[0];
			is[0] = -i;
		}
		int i = a.get(Attn.$margin_start).length_or_ratio(is[2] - is[0]);
		int i_0_ = a.get(Attn.$margin_after).length_or_ratio(is[3] - is[1]);
		int i_1_ = a.get(Attn.$margin_end).length_or_ratio(is[2] - is[0]);
		int i_2_ = a.get(Attn.$margin_before).length_or_ratio(is[3] - is[1]);
		super.init(Rectangle.shrink(is, new int[] { i, i_0_, i_1_, i_2_ }));
		rc = (int[]) r[2].clone();
		if (attributed.get(Attn.$writing_mode) == Attr.rl_tb) {
			int i_3_ = rc[2];
			rc[2] = -rc[0];
			rc[0] = -i_3_;
		}
		rc = Rectangle.rotate(rc, phi);
		if (a.get(Attn.$display_align) == Attr.auto)
			this.put(Attn.$display_align, Attr.newWord("before"));
	}
}