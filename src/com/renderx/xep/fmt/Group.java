/*
 * Group - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.fmt;

class Group {
	Integer bno;

	MTF.SavedState sast;

	static final Group nogroup = new Group(null, null);

	Group(Integer integer, MTF.SavedState savedstate) {
		bno = integer;
		sast = savedstate;
	}
}