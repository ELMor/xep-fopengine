/*
 * InstructionDispatcher - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.fmt;

import com.renderx.xep.cmp.Item;

public interface InstructionDispatcher {
	public void process(Line line);

	public void process(BPB bpb);

	public void process(Cntl.Rotate rotate);

	public void process(Cntl.Translate translate);

	public void process(Item.Text text);

	public void process(Item.Leader leader);

	public void process(Item.Image image);

	public void process(Item.Washer.Inline inline);

	public void process(Item.Filled filled);

	public void process(Item.Id id);

	public void process(Item.Bookmark bookmark);

	public void process(Item.Pinpoint pinpoint);
}