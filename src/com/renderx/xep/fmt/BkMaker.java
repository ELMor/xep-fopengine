/*
 * BkMaker - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.fmt;

import java.util.Enumeration;

import com.renderx.util.Applicator;
import com.renderx.util.Array;
import com.renderx.util.Hashtable;
import com.renderx.util.List;
import com.renderx.xep.cmp.ExceptionDispatcher;
import com.renderx.xep.cmp.Flow;
import com.renderx.xep.cmp.Item;
import com.renderx.xep.cmp.NoPageMasterException;
import com.renderx.xep.cmp.Outline;
import com.renderx.xep.cmp.PM;
import com.renderx.xep.cmp.Rectangle;
import com.renderx.xep.cmp.Region;
import com.renderx.xep.cmp.Sequence;
import com.renderx.xep.lib.InternalException;
import com.renderx.xep.lib.Session;
import com.renderx.xep.lib.Util;
import com.renderx.xep.pre.AttList;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Attributed;

public class BkMaker implements InstructionDispatcher {
	private List sequences;

	private Outline outline;

	private List book;

	private Sequence seq;

	private List pages;

	private Markers seqmarkers;

	Page page;

	List topfloats;

	List footnotes;

	List bodynotes;

	boolean bodywide;

	private Region reg;

	private Path path;

	private Path fnpath;

	private short recovery_step = 0;

	private final Session session;

	private static final short FC_BEFORE = 0;

	private static final short FC_IN = 1;

	private static final short FC_AFTER = 2;

	private short firstcol;

	private Path emptypath = new Path();

	private Hashtable idtab;

	private Hashtable iktab;

	private KeyTable keytab;

	private int initial_page_number = 1;

	private int passno;

	private final int[][] TM = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };

	private final int[][][] RM = { { { 0, 1, 0 }, { -1, 0, 0 }, { 0, 0, 1 } },
			{ { -1, 0, 0 }, { 0, -1, 0 }, { 0, 0, 1 } },
			{ { 0, -1, 0 }, { 1, 0, 0 }, { 0, 0, 1 } } };

	private int[][] M;

	private Ptr ptr;

	private List strm;

	private short state;

	private short fnstate;

	private short flstate;

	private MTF.SavedState bfsstate;

	private Object bfsmark;

	public class ExceptionHandler implements ExceptionDispatcher {
		private MTF fmt;

		ExceptionHandler(MTF mtf) {
			fmt = mtf;
		}

		public void process(Item.Exception exception) {
			fmt.cur.flow = (Flow) fmt.cur.flow.cdr();
		}

		public void process(Item.Absolute absolute) {
			List list = new List();
			list.append(reg).append(new Cntl.Translate(reg.rc[0], reg.rc[1]));
			absolute(list, fmt, absolute, reg.rc[2] - reg.rc[0], reg.rc[3]
					- reg.rc[1]);
			list.append(new Cntl.Translate(-reg.rc[0], -reg.rc[1]));
			page.strmtab.snoc(list);
			fmt.cur.flow = (Flow) fmt.cur.flow.cdr();
		}

		public void process(Item.Fixed fixed) {
			List list = new List();
			absolute(list, fmt, fixed, page.master.width, page.master.height);
			page.strmtab.snoc(list);
			fmt.cur.flow = (Flow) fmt.cur.flow.cdr();
		}

		public void process(Item.Rotated rotated) {
			Flow flow = (Flow) fmt.cur.flow.cdr().cdr();
			int i = rotated.get(Attn.$reference_orientation).count();
			int i_0_ = fmt.cur.x;
			int i_1_ = fmt.cur.x + fmt.cur.width;
			int i_2_ = fmt.cur.y0;
			int i_3_ = fmt.cur.y;
			int i_4_ = i_1_ - i_0_;
			int i_5_ = i_3_ - i_2_;
			if (rotated.get(Attn.$writing_mode) == Attr.rl_tb) {
				int i_6_ = -i_0_;
				i_0_ = -i_1_;
				i_1_ = i_6_;
			}
			boolean bool = true;
			if (i % 180 == 0) {
				int i_7_ = Attr.dim_range(rotated,
						Attn.$block_progression_dimension_minimum,
						Attn.$block_progression_dimension_optimum,
						Attn.$block_progression_dimension_maximum, i_5_);
				if (i_7_ != -2147483648) {
					i_2_ = i_3_ - i_7_;
					if (i_2_ < fmt.cur.y0)
						bool = false;
				} else {
					Region region = reg;
					reg = new Region(new int[] { i_0_, i_2_, i_1_, i_3_ },
							rotated.a);
					Path path = new Path(reg.rc);
					List list = new List();
					Ptr ptr = new Ptr(fmt.cur, new TD(flow, session));
					ptr.newColumn(true);
					MTF mtf = auxMTF(list, path, new Path(), ptr);
					if (i == 0) {
						mtf.start_edge = fmt.cur.start_edge;
						mtf.end_edge = fmt.cur.end_edge;
					}
					MTF.SavedState savedstate = mtf.sast;
					if (mkStream(mtf) != 1)
						bool = false;
					else if (!path.isEmpty()) {
						int[] is = (int[]) path.last();
						i_2_ += is[3] - is[1];
					}
					savedstate.restore();
					reg = region;
				}
			} else {
				int i_8_ = (Attr.dim_range(rotated,
						Attn.$inline_progression_dimension_minimum,
						Attn.$inline_progression_dimension_optimum,
						Attn.$inline_progression_dimension_maximum, i_5_));
				if (i_8_ != -2147483648) {
					i_2_ = i_3_ - i_8_;
					if (i_2_ < fmt.cur.y0)
						bool = false;
				}
			}
			if (bool) {
				Region region = reg;
				reg = new Region(new int[] { i_0_, i_2_, i_1_, i_3_ },
						rotated.a);
				Path path = new Path(reg.rc);
				List list = fmt.strm;
				Ptr ptr = new Ptr(fmt.cur, new TD(flow, session));
				list.append(new BPB(reg.a, reg.r[0], session));
				if (reg.phi != 0)
					list.append(new Cntl.Rotate(reg.phi));
				List.Mark mark = list.mark();
				ptr.newColumn(true);
				MTF mtf = auxMTF(list, path, new Path(), ptr);
				if (i == 0) {
					mtf.start_edge = fmt.cur.start_edge;
					mtf.end_edge = fmt.cur.end_edge;
				}
				int i_9_ = footnotes.length();
				MTF.SavedState savedstate = mtf.sast;
				mtf.sast = null;
				mtf.keep = (short) 2;
				Attr attr = rotated.get(Attn.$overflow);
				mtf.runoff = (attr != Attr.newWord("hidden") && attr != Attr
						.newWord("error-if-overflow"));
				if (mkStream(mtf) != 1
						&& attr == Attr.newWord("error-if-overflow"))
					session
							.warning("overflow in a table-cell or block-container");
				int i_10_;
				if (i % 180 == 0)
					i_10_ = mtf.dW;
				else if (!mtf.path.isEmpty()) {
					int[] is = (int[]) path.last();
					i_10_ = is[1] - is[3];
				} else
					i_10_ = 0;
				i_10_ += fmt.dW0;
				if (i_10_ > fmt.dW)
					fmt.dW = i_10_;
				if (!mtf.path.isEmpty()) {
					i_10_ = BkMaker.this.vshift(reg, mtf.path);
					if (Util.gt0(i_10_)) {
						list.insert(mark, new Cntl.Translate(0, -i_10_));
						list.append(new Cntl.Translate(0, i_10_));
					}
				}
				BkMaker.this.appendAbsolute(list, reg);
				if (reg.phi != 0)
					list.append(new Cntl.Rotate(360 - reg.phi));
				if (fmt.mode == 0 && i_9_ != footnotes.length()) {
					if (bodywide)
						bool = footnotes.last() != null;
					else
						fmt.footnotes_pending = true;
				}
				if (bool) {
					if (fmt.cur.b.bpb.first_line)
						fmt.cur.b.bpb.first_line = false;
					fmt.cur.container_edge = false;
					fmt.cur.empty = mtf.cur == null ? (short) 0 : mtf.cur.empty;
					fmt.cur.y = i_2_;
					fmt.cur.flow = (Flow) fmt.cur.flow.cdr();
				} else {
					savedstate.restore();
					fmt.cur.empty = (short) 2;
					fmt.cur.waiting = (short) 2;
				}
				reg = region;
			} else {
				fmt.cur.empty = (short) 2;
				fmt.cur.waiting = (short) 2;
			}
		}

		public void process(Item.Footnote footnote) {
			if (bodywide) {
				switch (fmt.mode) {
				case 0:
					if (bodynotes.isPair()) {
						footnotes.snoc(bodynotes.shift());
						fmt.cur.flow = (Flow) fmt.cur.flow.cdr();
					} else {
						fmt.cur.empty = (short) 2;
						fmt.cur.waiting = (short) 3;
					}
					break;
				case 1:
					footnotes.snoc(bodynotes.isPair() ? bodynotes.shift()
							: null);
					fmt.cur.flow = (Flow) fmt.cur.flow.cdr();
					break;
				case 2:
					fmt.cur.flow = (Flow) fmt.cur.flow.cdr();
					break;
				default:
					throw new InternalException("invalid fmt mode: " + fmt.mode);
				}
			} else {
				Flow flow = footnote.flow;
				switch (fmt.mode) {
				case 0:
				case 1:
					footnotes.append(new Ptr(new TD(flow, session)));
					if (session.config.PAGEWIDE_FOOTNOTES)
						bodynotes.append(((Ptr) footnotes.last()).ln());
					fmt.cur.flow = (Flow) fmt.cur.flow.cdr();
					if (fmt.mode == 0)
						fmt.footnotes_pending = true;
					break;
				case 2:
					fmt.cur.flow = (Flow) fmt.cur.flow.cdr();
					break;
				default:
					throw new InternalException("invalid fmt mode: " + fmt.mode);
				}
			}
		}

		public void process(Item.FootnotesPending footnotespending) {
			boolean bool = true;
			fmt.cur.flow = (Flow) fmt.cur.flow.cdr();
			Path path = ((Path) fmt.path.clone()).add(fnpath);
			MTF mtf = (oolMTF(strm, (Path) path.clone(), new Ptr(new TD(
					(BkMaker.this.rule(Attr.newWord("xsl-footnote-separator"),
							0)), session))));
			MTF.SavedState savedstate = mtf.sast;
			mkStream(mtf);
			Enumeration enumeration = footnotes.elements();
			Ptr ptr;
			while_15_: do {
				do {
					ptr = (Ptr) enumeration.nextElement();
					if (!enumeration.hasMoreElements())
						break while_15_;
					mtf = oolMTF(strm, mtf.path, ptr.ln());
				} while (mkStream(mtf) == 1);
				bool = false;
			} while (false);
			if (bool) {
				mtf = oolMTF(strm, mtf.path, ptr.ln());
				mkStream(mtf);
				if (mtf.ptr.empties())
					bool = false;
			}
			savedstate.restore();
			if (bool) {
				BkMaker.this.fnSpace(path);
				fmt.cur.flow.cons(Item.footnotesAppended);
			} else {
				fmt.cur.empty = (short) 2;
				fmt.cur.waiting = (short) 3;
			}
		}

		public void process(Item.PageNumber pagenumber) {
			if (fmt.phrase == null)
				fmt.phrase = new LineBuffer.Phrase(fmt);
			Item.Text text = new Item.Text(page.id, pagenumber.a, session);
			fmt.cur.flow = (Flow) fmt.cur.flow.cdr().cons(text);
		}

		public void process(Item.PageKey pagekey) {
			Flow flow = new Flow();
			Item.Text text = new Item.Text(pagekey.get(Attn.$list_separator)
					.word(), pagekey.a, session);
			boolean bool = pagekey.get(Attn.$keep_together_within_line) == Attr.auto;
			Enumeration enumeration = pagekey.terms.elements();
			while (enumeration.hasMoreElements()) {
				Item.PageKey.Term term = (Item.PageKey.Term) enumeration
						.nextElement();
				term.ranges = keytab.get(term.ref_key, term.merge);
			}
			int i = 0;
			while_16_: for (;;) {
				KeyTable.Range range = null;
				Item.PageKey.Term term = null;
				Enumeration enumeration_11_ = pagekey.terms.elements();
				while (enumeration_11_.hasMoreElements()) {
					Item.PageKey.Term term_12_ = (Item.PageKey.Term) enumeration_11_
							.nextElement();
					if (!term_12_.ranges.isEmpty()) {
						KeyTable.Range range_13_ = (KeyTable.Range) term_12_.ranges
								.car();
						if (range == null) {
							range = range_13_;
							term = term_12_;
						} else if (range_13_.key.page.no < range.key.page.no
								|| ((range_13_.key.page.no == range.key.page.no) && (range_13_.endk.page.no < range.endk.page.no))) {
							range = range_13_;
							term = term_12_;
						} else if (range_13_.key.page.no == range.key.page.no
								&& (range_13_.endk.page.no == range.endk.page.no))
							term_12_.ranges.shift();
					}
				}
				if (range == null)
					break;
				term.ranges.shift();
				Item.Span span;
				if (term.link) {
					AttList attlist = (AttList) term.a.clone();
					attlist.put(Attn.$internal_destination, range.key.id);
					span = new Item.Anchor(attlist, session);
				} else
					span = new Item.Span(term.a, session);
				Item.Space.Inline inline = new Item.Space.Inline(
						span,
						((span.writing_mode == fmt.cur.s.writing_mode) ? "space-start"
								: "space-end"));
				if (!inline.nil)
					flow.append(inline.setFlags(i)).append(span);
				else
					flow.append(span.setFlags(i));
				if (bool)
					i = 1;
				flow.append(new Item.Text(range.key.page.id, term.a, session));
				if (range.key.page != range.endk.page) {
					int i_14_ = ((term.get(Attn.$keep_together_within_line) == Attr.auto) ? 1
							: 0);
					flow.append(term.RS).append(
							new Item.Text(range.endk.page.id, term.a, session)
									.setFlags(i_14_));
				}
				flow.append(Item.ends);
				inline = new Item.Space.Inline(
						span,
						((span.writing_mode == fmt.cur.s.writing_mode) ? "space-end"
								: "space-start"));
				if (!inline.nil)
					flow.append(inline);
				Enumeration enumeration_15_ = pagekey.terms.elements();
				do {
					if (!enumeration_15_.hasMoreElements())
						break while_16_;
				} while (((Item.PageKey.Term) enumeration_15_.nextElement()).ranges
						.isEmpty());
				flow.append(text);
			}
			flow.append(new Item.Jump((Flow) fmt.cur.flow.cdr()));
			fmt.cur.flow = flow;
		}

		public void process(Item.Tag tag) {
			tag.page = page;
			fmt.cur.flow = (Flow) fmt.cur.flow.cdr();
		}

		public void process(Item.Id id) {
			process((Item.Tag) id);
			id.x = fmt.cur.x;
			id.y = fmt.cur.y;
			fmt.strm.append(id);
			idtab.put(id.id, id);
		}

		public void process(Item.Key key) {
			process((Item.Tag) key);
			keytab.put(key);
			Object object;
			if ((object = iktab.get(key.id)) instanceof Item.Endk) {
				((Item.Endk) object).put(Attn.$key, key.get(Attn.$key));
				keytab.put((Item.Endk) object);
			}
			iktab.put(key.id, key.get(Attn.$key));
			if (!key.a.containsKey(Attn.$id)) {
				AttList attlist = (AttList) key.a.clone();
				attlist.put(Attn.$id, key.id);
				process((Item.Id) fmt.cur.flow.cons(new Item.Id(attlist)).car());
			}
		}

		public void process(Item.Endk endk) {
			process((Item.Tag) endk);
			Object object = iktab.get(endk.id);
			if (object instanceof Attr) {
				endk.put(Attn.$key, (Attr) object);
				keytab.put(endk);
			} else
				iktab.put(endk.id, endk);
		}

		public void process(Item.Pinpoint pinpoint) {
			pinpoint.x = fmt.cur.x;
			pinpoint.y = fmt.cur.y;
			fmt.strm.append(pinpoint);
			fmt.cur.flow = (Flow) fmt.cur.flow.cdr();
		}

		public void process(Item.Keep keep) {
			fmt.cur.flow = (Flow) fmt.cur.flow.cdr();
			if ((fmt.keep & 0x2) != 0)
				fmt.cur.empty = (short) 1;
		}

		public void process(Item.Break var_break) {
			fmt.cur.flow = (Flow) fmt.cur.flow.cdr();
			if (fmt.cur.group != Group.nogroup) {
				Group group = fmt.cur.group;
				Enumeration enumeration = fmt.ptr.tab.elements();
				while (enumeration.hasMoreElements()) {
					TD td = (TD) enumeration.nextElement();
					if (td.group == group)
						td.group = Group.nogroup;
				}
			}
			switch (var_break.context) {
			case 0:
				session.error("break with context 'auto'");
				break;
			case 1:
				if (fmt.cur.empty != 4) {
					fmt.cur.empty = (short) 0;
					fmt.cur.waiting = (short) 3;
				}
				break;
			case 2:
				if (fmt.cur.empty != 4 || firstcol != 1) {
					fmt.cur.empty = (short) 0;
					fmt.cur.waiting = (short) 4;
				}
				break;
			case 3:
				if (fmt.cur.empty != 4 || firstcol != 1 || page.number % 2 != 1) {
					fmt.cur.empty = (short) 0;
					fmt.cur.waiting = (short) 5;
				}
				break;
			case 4:
				if (fmt.cur.empty != 4 || firstcol != 1 || page.number % 2 != 0) {
					fmt.cur.empty = (short) 0;
					fmt.cur.waiting = (short) 6;
				}
				break;
			default:
				throw new InternalException("illegal break context:"
						+ var_break.context);
			}
		}

		public void process(Item.Remark remark) {
			fmt.cur.flow = (Flow) fmt.cur.flow.cdr();
			Flow flow = null;
			Marker marker = (Marker) page.markers.get(remark.name);
			if (marker != null)
				flow = marker.get(remark.pos);
			if (flow == null && remark.boundary != 0) {
				flow = (Flow) seqmarkers.get(remark.name);
				if (flow == null && remark.boundary == 2) {
					Attr.Word word = Attr.Word.create("[ghost]"
							+ remark.name.word());
					marker = (Marker) page.markers.get(word);
					if (marker != null)
						flow = marker.get(remark.pos);
					if (flow == null)
						flow = (Flow) seqmarkers.get(word);
				}
			}
			if (flow != null) {
				flow.setLast(new Item.Jump(fmt.cur.flow));
				fmt.cur.flow = flow;
			}
		}

		public void process(Item.Float var_float) {
			short i = (var_float.edge == 4 ? page.number % 2 == 1 ? (short) 2
					: (short) 3
					: var_float.edge == 5 ? page.number % 2 == 0 ? (short) 2
							: (short) 3 : var_float.edge);
			switch (i) {
			case 0:
				fmt.cur.flow = (Flow) fmt.cur.flow.cdr();
				var_float.flow.setLast(new Item.Jump(fmt.cur.flow));
				fmt.cur.flow = var_float.flow;
				break;
			case 1:
				switch (fmt.mode) {
				case 0:
				case 1:
					topfloats.append(new Ptr(new TD(var_float.flow, session)));
				/* fall through */
				case 2:
					fmt.cur.flow = (Flow) fmt.cur.flow.cdr();
					break;
				default:
					throw new InternalException("invalid fmt mode: " + fmt.mode);
				}
				break;
			case 2:
			case 3:
				switch (fmt.mode) {
				case 0:
				case 1: {
					int i_16_ = fmt.cur.x;
					int i_17_ = fmt.cur.x + fmt.cur.width;
					int i_18_ = fmt.cur.y0;
					int i_19_ = fmt.cur.y;
					if (!(fmt.cur.b instanceof Item.Frame)) {
						i_16_ -= fmt.cur.b.start_indent;
						i_17_ += fmt.cur.b.end_indent;
					}
					int i_20_ = i_17_ - i_16_;
					int i_21_ = i_19_ - i_18_;
					Item.Block block = (fmt.cur.b instanceof Item.Frame ? fmt.cur.b
							: !fmt.cur.frstack.isEmpty() ? (Item.Block) fmt.cur.frstack
									.car()
									: null);
					Edge edge = fmt.cur.start_edge;
					Edge edge_22_ = fmt.cur.end_edge;
					boolean bool = fmt.cur.b.writing_mode == Attr.rl_tb;
					if (bool) {
						int i_23_ = -i_16_;
						i_16_ = -i_17_;
						i_17_ = i_23_;
						Edge edge_24_ = edge;
						edge = edge_22_;
						edge_22_ = edge_24_;
					}
					if (fmt.ptr.lb != null) {
						if (fmt.phrase != null) {
							fmt.ptr.lb.size(fmt.phrase, false);
							i_19_ -= (fmt.phrase.height_before
									+ fmt.phrase.height_after + fmt.cur.b.after_indent);
						} else
							i_19_ -= (fmt.ptr.lb.height_before
									+ fmt.ptr.lb.height_after + fmt.cur.b.after_indent);
					}
					i_19_ = clear(fmt,
							new Item.Clear((Attributed) var_float).edge, i_19_);
					Region region = reg;
					while_17_: do {
						Path path;
						MTF mtf;
						MTF.SavedState savedstate;
						for (;;) {
							reg = new Region(new int[] { i_16_, i_18_, i_17_,
									i_19_ }, var_float.a);
							path = new Path(reg.rc);
							mtf = oolMTF(new List(), path, new Ptr(fmt.cur,
									new TD(var_float.flow, session)));
							savedstate = mtf.sast;
							mtf.start_edge = edge;
							mtf.end_edge = edge_22_;
							if (mkStream(mtf) != 1) {
								savedstate.restore();
								fmt.cur.empty = (short) 2;
								fmt.cur.waiting = (short) 2;
								break while_17_;
							}
							if (mtf.dW <= 0)
								break;
							int i_25_ = edge.y0();
							int i_26_ = edge_22_.y0();
							int i_27_ = (i_25_ > i_19_ ? i_26_
									: i_26_ > i_19_ ? i_25_
											: i_25_ > i_26_ ? i_25_ : i_26_);
							if (i_27_ > i_19_)
								break;
							i_19_ = i_27_ - 1;
						}
						if (!path.isEmpty()) {
							int[] is = (int[]) path.last();
							i_18_ += is[3] - is[1];
						}
						int i_28_ = 0;
						if ((i == 2) == bool && mtf.dWs - 1 < -1)
							i_28_ += mtf.dWs;
						if ((i == 3) == bool && mtf.dWe - 1 < -1)
							i_28_ += mtf.dWe;
						if (mtf.dW != -2147483648 && mtf.dW < 0) {
							if ((i == 3) == bool)
								i_17_ += mtf.dW;
							else
								i_16_ -= mtf.dW;
						}
						savedstate.restore();
						reg = new Region(
								new int[] { i_16_, i_18_, i_17_, i_19_ },
								var_float.a);
						Ptr ptr = new Ptr(fmt.cur, new TD(var_float.flow,
								session));
						mtf = oolMTF(fmt.strm, new Path(reg.rc), ptr);
						mtf.start_edge = edge;
						mtf.end_edge = edge_22_;
						mtf.sast = null;
						mtf.keep = (short) 0;
						Attr attr = var_float.get(Attn.$overflow);
						mtf.runoff = (attr != Attr.newWord("hidden") && attr != Attr
								.newWord("error-if-overflow"));
						if (mkStream(mtf) != 1
								&& attr == Attr.newWord("error-if-overflow"))
							session.warning("overflow in a float");
						BkMaker.this.appendAbsolute(strm, reg);
						int i_29_ = i_17_ - i_16_ + i_28_;
						if (i == 2) {
							if (block != null) {
								BPB.Frame frame = (BPB.Frame) block.bpb;
								fmt.cur.start_edge = frame.start_edge = frame.start_edge
										.add(i_18_, i_19_, i_29_);
								if (i_18_ < frame.y0s)
									frame.y0s = i_18_;
							} else
								fmt.cur.start_edge = fmt.start_edge = fmt.start_edge
										.add(i_18_, i_19_, i_29_);
						} else if (block != null) {
							BPB.Frame frame = (BPB.Frame) block.bpb;
							fmt.cur.end_edge = frame.end_edge = frame.end_edge
									.add(i_18_, i_19_, i_29_);
							if (i_18_ < frame.y0e)
								frame.y0e = i_18_;
						} else
							fmt.cur.end_edge = fmt.end_edge = fmt.end_edge.add(
									i_18_, i_19_, i_29_);
						if (fmt.Y > i_18_)
							fmt.Y = i_18_;
						fmt.cur.flow = (Flow) fmt.cur.flow.cdr();
					} while (false);
					reg = region;
					break;
				}
				case 2:
					fmt.cur.flow = (Flow) fmt.cur.flow.cdr();
					var_float.flow.setLast(new Item.Jump(fmt.cur.flow));
					fmt.cur.flow = var_float.flow;
					break;
				default:
					throw new InternalException("invalid fmt mode: " + fmt.mode);
				}
				break;
			default:
				throw new InternalException("illegal value for float.edge: "
						+ var_float.edge);
			}
		}

		public void process(Item.Clear clear) {
			short i = clear.edge;
			fmt.cur.y = clear(fmt, i, fmt.cur.y);
			fmt.cur.flow = (Flow) fmt.cur.flow.cdr();
		}

		private void absolute(List list, MTF mtf, Item.Exception exception,
				int i, int i_30_) {
			Flow flow = exception.flow;
			int i_31_ = exception.get(Attn.$reference_orientation).count();
			Attr attr = exception.get(Attn.$left);
			Attr attr_32_ = exception.get(Attn.$bottom);
			Attr attr_33_ = exception.get(Attn.$right);
			Attr attr_34_ = exception.get(Attn.$top);
			int i_35_ = attr.length_or_ratio(i);
			int i_36_ = attr_32_.length_or_ratio(i_30_);
			int i_37_ = attr_33_.length_or_ratio(i);
			int i_38_ = attr_34_.length_or_ratio(i_30_);
			int i_39_;
			int i_40_;
			if (i_31_ % 180 == 0) {
				i_39_ = (Attr.dim_range(exception,
						Attn.$inline_progression_dimension_minimum,
						Attn.$inline_progression_dimension_optimum,
						Attn.$inline_progression_dimension_maximum, i));
				i_40_ = Attr.dim_range(exception,
						Attn.$block_progression_dimension_minimum,
						Attn.$block_progression_dimension_optimum,
						Attn.$block_progression_dimension_maximum, i_30_);
			} else {
				i_39_ = Attr.dim_range(exception,
						Attn.$block_progression_dimension_minimum,
						Attn.$block_progression_dimension_optimum,
						Attn.$block_progression_dimension_maximum, i);
				i_40_ = (Attr.dim_range(exception,
						Attn.$inline_progression_dimension_minimum,
						Attn.$inline_progression_dimension_optimum,
						Attn.$inline_progression_dimension_maximum, i_30_));
			}
			if (i_39_ == -2147483648) {
				if (attr == Attr.auto)
					i_35_ = 0;
				if (attr_33_ == Attr.auto)
					i_37_ = 0;
				i_39_ = i - i_37_ - i_35_;
			} else if (attr_33_ == Attr.auto) {
				if (attr == Attr.auto)
					i_35_ = 0;
				i_37_ = i - i_35_ - i_39_;
			} else if (attr == Attr.auto)
				i_35_ = i - i_37_ - i_39_;
			else
				i_39_ = i - i_37_ - i_35_;
			if (i_40_ == -2147483648) {
				if (attr_34_ == Attr.auto)
					i_38_ = 0;
				if (attr_32_ == Attr.auto)
					i_36_ = 0;
				i_40_ = i_30_ - i_38_ - i_36_;
			} else if (attr_32_ == Attr.auto) {
				if (attr_34_ == Attr.auto)
					i_38_ = 0;
				i_36_ = i_30_ - i_38_ - i_40_;
			} else if (attr_34_ == Attr.auto)
				i_38_ = i_30_ - i_36_ - i_40_;
			else
				i_40_ = i - i_36_ - i_38_;
			Region region = reg;
			reg = new Region(
					(Rectangle
							.shrink(
									new int[] { 0, 0, i, i_30_ },
									(new int[] {
											(i_35_
													- exception
															.get(
																	Attn.$border_start_width_length)
															.length()
													- exception
															.get(
																	Attn.$padding_start_length)
															.length_or_ratio(i) - exception
													.get(Attn.$margin_start)
													.length_or_ratio(i)),
											(i_36_
													- exception
															.get(
																	Attn.$border_after_width_length)
															.length()
													- exception
															.get(
																	Attn.$padding_after_length)
															.length_or_ratio(
																	i_30_) - exception
													.get(Attn.$margin_after)
													.length_or_ratio(i_30_)),
											(i_37_
													- exception
															.get(
																	Attn.$border_end_width_length)
															.length()
													- exception
															.get(
																	Attn.$padding_end_length)
															.length_or_ratio(i) - exception
													.get(Attn.$margin_end)
													.length_or_ratio(i)),
											(i_38_
													- exception
															.get(
																	Attn.$border_before_width_length)
															.length()
													- exception
															.get(
																	Attn.$padding_before_length)
															.length_or_ratio(
																	i_30_) - exception
													.get(Attn.$margin_before)
													.length_or_ratio(i_30_)) }))),
					exception.a);
			Path path = new Path(reg.rc);
			Ptr ptr = new Ptr(new TD(flow, session));
			list.append(new BPB(reg.a, reg.r[0], session));
			if (i_31_ != 0)
				list.append(new Cntl.Rotate(i_31_));
			List.Mark mark = list.mark();
			ptr.newColumn(true);
			MTF mtf_41_ = oolMTF(list, path, ptr);
			mtf_41_.sast = null;
			mtf_41_.keep = (short) 0;
			Attr attr_42_ = exception.get(Attn.$overflow);
			mtf_41_.runoff = (attr_42_ != Attr.newWord("hidden") && attr_42_ != Attr
					.newWord("error-if-overflow"));
			if (mkStream(mtf_41_) != 1
					&& attr_42_ == Attr.newWord("error-if-overflow"))
				session.warning("overflow in "
						+ (exception instanceof Item.Absolute ? "absolute"
								: "fixed") + " block-container");
			if (!mtf_41_.path.isEmpty()) {
				int i_43_ = BkMaker.this.vshift(reg, mtf_41_.path);
				if (Util.gt0(i_43_)) {
					list.insert(mark, new Cntl.Translate(0, -i_43_));
					list.append(new Cntl.Translate(0, i_43_));
				}
			}
			BkMaker.this.appendAbsolute(list, reg);
			if (i_31_ != 0)
				list.append(new Cntl.Rotate(360 - i_31_));
			reg = region;
		}

		private int clear(MTF mtf, short i, int i_44_) {
			Item.Block block = (mtf.cur.b instanceof Item.Frame ? mtf.cur.b
					: !mtf.cur.frstack.isEmpty() ? (Item.Block) mtf.cur.frstack
							.car() : null);
			int i_45_;
			int i_46_;
			if (block != null) {
				BPB.Frame frame = (BPB.Frame) block.bpb;
				i_45_ = frame.y0s;
				i_46_ = frame.y0e;
			} else {
				i_45_ = mtf.start_edge.y0();
				i_46_ = mtf.end_edge.y0();
			}
			if ((i & 0x4) != 0)
				i = (short) (i | (page.number % 2 == 1 ? 1 : 2));
			if ((i & 0x8) != 0)
				i = (short) (i | (page.number % 2 == 0 ? 1 : 2));
			if ((i & 0x1) != 0 && i_44_ >= i_45_)
				i_44_ = i_45_ - 1;
			if ((i & 0x2) != 0 && i_44_ >= i_46_)
				i_44_ = i_46_ - 1;
			return i_44_;
		}
	}

	private class LastPageException extends Exception {
		private LastPageException() {
			/* empty */
		}
	}

	class Forkpoint {
		private Ptr ptr = null;

		private List topfloats = null;

		private List footnotes = null;

		private List bodynotes = null;

		private List strmtab = null;

		private List.Mark mark = null;

		private short firstcol = 0;

		void save() {
			ptr = BkMaker.this.ptr.ln();
			topfloats = (List) BkMaker.this.topfloats.clone();
			footnotes = (List) BkMaker.this.footnotes.clone();
			bodynotes = (List) BkMaker.this.bodynotes.clone();
			strmtab = (List) page.strmtab.clone();
			mark = strm.mark();
			firstcol = BkMaker.this.firstcol;
		}

		void restore() {
			BkMaker.this.ptr.restore(ptr.ln());
			BkMaker.this.topfloats.clear().append(topfloats);
			BkMaker.this.footnotes.clear().append(footnotes);
			BkMaker.this.bodynotes.clear().append(bodynotes);
			page.strmtab.clear().append(strmtab);
			strm.cut(mark);
			BkMaker.this.firstcol = firstcol;
		}
	}

	class Bodypoint {
		Ptr ptr;

		short state;

		List strmtab;

		List.Mark mark;

		void save() {
			ptr = BkMaker.this.ptr.ln();
			state = BkMaker.this.state;
			topfloats = (List) topfloats.clone();
			mark = strm.mark();
			strmtab = (List) page.strmtab.clone();
		}

		void restore() {
			BkMaker.this.ptr = ptr.ln();
			BkMaker.this.state = state;
			topfloats.clear().append(topfloats);
			strm.cut(mark);
			page.strmtab.clear().append(strmtab);
		}
	}

	public static class Page {
		public int no;

		public int number;

		public String id;

		public List strmtab;

		public PM master;

		Markers markers;

		Ptr ptr;

		List topfloats;

		List footnotes;

		short state;

		public Page(int i) {
			strmtab = new List();
			markers = new Markers();
			no = i;
			id = "?";
		}

		Page(BkMaker bkmaker, int i, Sequence sequence, int i_47_)
				throws NoPageMasterException {
			strmtab = new List();
			markers = new Markers();
			no = bkmaker.book.length() + i + 1;
			number = bkmaker.initial_page_number + i;
			i_47_ = i_47_ | (number % 2 == 0 ? 64 : 32);
			id = sequence.numstr.format(number);
			master = sequence.master.pm(i_47_);
		}
	}

	public BkMaker(List list, Outline outline, Session session) {
		sequences = list;
		this.outline = outline;
		this.session = session;
	}

	MTF colMTF() {
		MTF mtf = new MTF(strm, path, fnpath, ptr, this, session);
		switch (firstcol) {
		case 0:
			mtf.sast = null;
			firstcol = (short) 1;
			break;
		case 1:
			firstcol = (short) 2;
			break;
		case 2:
			break;
		default:
			throw new InternalException("illegal value for firstcol: "
					+ firstcol);
		}
		if (recovery_step == 1)
			mtf.keep = (short) 0;
		mtf.mode = (short) 0;
		return mtf;
	}

	MTF auxMTF(List list, Path path, Path path_48_, Ptr ptr) {
		MTF mtf = new MTF(list, path, path_48_, ptr, this, session);
		mtf.mode = (short) 1;
		return mtf;
	}

	MTF oolMTF(List list, Path path, Ptr ptr) {
		emptypath.clear();
		MTF mtf = new MTF(list, path, emptypath, ptr, this, session);
		mtf.mode = (short) 2;
		return mtf;
	}

	private boolean uneven(int i, Attr attr) {
		if (attr == Attr.newWord("even"))
			return i % 2 == 0;
		if (attr == Attr.newWord("odd"))
			return i % 2 == 1;
		if (attr == Attr.newWord("end-on-even"))
			return (initial_page_number + i) % 2 == 1;
		if (attr == Attr.newWord("end-on-odd"))
			return (initial_page_number + i) % 2 == 0;
		if (attr == Attr.newWord("no-force"))
			return false;
		if (attr == Attr.auto)
			throw new InternalException(
					"force-page-count==auto should have been resolved in compiler");
		throw new InternalException("invalid value for force-page-count: "
				+ attr);
	}

	private void feedPage() {
		strm = new List();
		page.strmtab.snoc(strm);
		Enumeration enumeration = page.master.regtab.elements();
		while (enumeration.hasMoreElements()) {
			reg = (Region) enumeration.nextElement();
			strm.append(new BPB(reg.a, reg.r[0], session));
		}
	}

	private Flow rule(Attr attr, int i) {
		Flow flow = (Flow) seq.sctab.get(attr);
		if (flow == null) {
			flow = new Flow();
			flow.ld(session);
			seq.sctab.put(attr, flow);
		}
		return (i == 0 ? flow : ((Flow) new Flow().cons(
				new Item.Washer.Display(i)).append(flow)));
	}

	private void fnSpace(Path path) {
		int i = 100;
		short i_49_ = -1;
		for (;;) {
			MTF mtf = (oolMTF(strm, path.flipX(), new Ptr(new TD(rule(Attr
					.newWord("xsl-footnote-separator"), i), session))));
			MTF.SavedState savedstate = mtf.sast;
			mkStream(mtf);
			Enumeration enumeration = footnotes.elements();
			while (enumeration.hasMoreElements()) {
				mtf = oolMTF(strm, mtf.path, ((Ptr) enumeration.nextElement())
						.ln());
				i_49_ = mkStream(mtf);
			}
			fnpath.clear().append((Path) path.clone());
			if (i_49_ == 2) {
				savedstate.restore();
				break;
			}
			Enumeration enumeration_50_ = mtf.path.flipX().elements();
			while (enumeration_50_.hasMoreElements())
				fnpath.apply((short) 0, (int[]) enumeration_50_.nextElement());
			mtf = (oolMTF(strm, (Path) fnpath.clone(), new Ptr(new TD(rule(Attr
					.newWord("xsl-footnote-separator"), 0), session))));
			mkStream(mtf);
			Enumeration enumeration_51_ = footnotes.elements();
			while (enumeration_51_.hasMoreElements()) {
				mtf = oolMTF(strm, mtf.path, ((Ptr) enumeration_51_
						.nextElement()).ln());
				i_49_ = mkStream(mtf);
			}
			savedstate.restore();
			if (i_49_ == 1)
				break;
			i *= 2;
		}
		Enumeration enumeration = fnpath.elements();
		while (enumeration.hasMoreElements())
			this.path.apply((short) 0, (int[]) enumeration.nextElement());
	}

	private void fmtList(List list, Path path, List list_52_, boolean bool) {
		if (!list_52_.isEmpty()) {
			((Ptr) list_52_.car()).newColumn(true);
			MTF mtf = oolMTF(list, path, (Ptr) list_52_.car());
			if (bool)
				mtf.sast = null;
			while_11_: for (;;) {
				switch (mkStream(mtf)) {
				case 2:
					break while_11_;
				case 1:
					break;
				default:
					continue;
				}
				list_52_.shift();
				if (list_52_.isEmpty())
					break;
				mtf = oolMTF(list, path, (Ptr) list_52_.car());
			}
		}
	}

	short mkStream(MTF mtf) {
		int i = -1;
		ExceptionHandler exceptionhandler = new ExceptionHandler(mtf);
		short i_53_;
		while_12_: for (;;) {
			switch (i_53_ = mtf.fmt()) {
			case 0:
				((Item.Exception) mtf.cur.flow.car()).pass(exceptionhandler);
				break;
			case 1:
			case 2:
			case 3:
			case 4:
				break while_12_;
			default:
				throw new InternalException(
						"illegal return code from MTF.fmt: " + i_53_);
			}
		}
		return i_53_;
	}

	private int vshift(Attributed attributed, Path path) {
		int i = 0;
		int[] is = (int[]) path.last();
		int i_54_ = is[3] - is[1];
		Attr attr = attributed.get(Attn.$display_align);
		if (attr != Attr.newWord("before")) {
			if (attr == Attr.newWord("center"))
				i = i_54_ / 2;
			else if (attr == Attr.newWord("after"))
				i = i_54_;
			else
				session.warning("display-align='" + attr
						+ "' not supported for area-containers");
		}
		return i;
	}

	private void fillStartingIncludingCarryover() {
		Enumeration enumeration = ptr.tab.elements();
		while (enumeration.hasMoreElements()) {
			TD td = (TD) enumeration.nextElement();
			if (!td.bstack.isEmpty()) {
				Enumeration enumeration_55_ = td.bstack.elements();
				Item.Block block = td.b;
				do {
					if (block.markers != null)
						page.markers.First(block.markers);
					block = (Item.Block) enumeration_55_.nextElement();
				} while (enumeration_55_.hasMoreElements());
			}
			if (!td.sstack.isEmpty()) {
				Enumeration enumeration_56_ = td.sstack.elements();
				Item.Span span = td.s;
				do {
					if (span.markers != null)
						page.markers.First(span.markers);
					span = (Item.Span) enumeration_56_.nextElement();
				} while (enumeration_56_.hasMoreElements());
			}
		}
	}

	private int mkFloats() {
		bfsstate = null;
		bfsmark = null;
		int i;
		if (!topfloats.isEmpty()) {
			Path path = new Path(reg.rc);
			fmtList(strm, path, topfloats, true);
			flstate = topfloats.isEmpty() ? (short) 1 : (short) 2;
			if (isFilled()) {
				Ptr ptr = (new Ptr(new TD(rule(Attr
						.newWord("xsl-before-float-separator"), 0), session)));
				ptr.newColumn(true);
				MTF mtf = oolMTF(strm, path, ptr);
				bfsstate = mtf.sast;
				mkStream(mtf);
				Object object = null;
				bfsmark = strm.last();
				i = path.isPair() ? ((int[]) path.car())[3] : reg.rc[1];
			} else
				i = reg.rc[3];
		} else {
			flstate = (short) 1;
			i = reg.rc[3];
		}
		return i;
	}

	private int measureBodynotes(int i, boolean bool) {
		List list = bodynotes.map(new List(), new Applicator() {
			public Object f(Object object) {
				return ((Ptr) object).ln();
			}
		});
		fnpath = new Path(new int[] { reg.rc[0], -1073741824, reg.rc[2], 0 });
		Ptr ptr = new Ptr(new TD(
				rule(Attr.newWord("xsl-footnote-separator"), 0), session));
		ptr.newColumn(true);
		MTF mtf = oolMTF(strm, fnpath, ptr);
		Object object = null;
		MTF.SavedState savedstate = mtf.sast;
		mkStream(mtf);
		Object object_58_ = null;
		fmtList(strm, fnpath, list, bool);
		i = reg.rc[1] - ((int[]) fnpath.car())[3];
		savedstate.restore();
		if (!footnotes.isEmpty()) {
			fnpath = new Path(
					new int[] { reg.rc[0], -1073741824, reg.rc[2], 0 });
			fmtList(strm, fnpath, footnotes, false);
			i += ((int[]) fnpath.car())[3];
			list = new List();
			Enumeration enumeration = bodynotes.elements();
			for (;;) {
				Ptr ptr_59_ = (Ptr) enumeration.nextElement();
				if (!enumeration.hasMoreElements())
					break;
				list.snoc(ptr_59_.ln());
			}
			fnpath = new Path(new int[] { reg.rc[0], reg.rc[1], reg.rc[2], i });
			ptr = new Ptr(new TD(
					rule(Attr.newWord("xsl-footnote-separator"), 0), session));
			ptr.newColumn(true);
			mtf = oolMTF(strm, fnpath, ptr);
			object = null;
			savedstate = mtf.sast;
			mkStream(mtf);
			object_58_ = null;
			fmtList(strm, fnpath, list, bool);
			int i_60_ = 0;
			int i_61_ = ((int[]) fnpath.car())[3];
			for (;;) {
				Ptr ptr_62_ = ((Ptr) bodynotes.last()).ln();
				fnpath = new Path(new int[] { reg.rc[0], reg.rc[1], reg.rc[2],
						i_61_ + i_60_ });
				mtf = oolMTF(strm, fnpath, ptr_62_);
				mkStream(mtf);
				object_58_ = null;
				if (!ptr_62_.empties())
					break;
				i_60_ = 2 * i_60_ + 50;
				Object object_63_ = null;
			}
			i += i_60_;
			savedstate.restore();
		}
		return i;
	}

	private void mkBodynotes(int i, boolean bool) {
		if (!bodynotes.isEmpty()) {
			List list = footnotes.map(new List(), new Applicator() {
				public Object f(Object object) {
					return ((Ptr) object).ln();
				}
			});
			fnpath = new Path(
					new int[] { reg.rc[0], -1073741824, reg.rc[2], 0 });
			Ptr ptr = new Ptr(new TD(rule(Attr
					.newWord("xsl-footnote-separator"), 0), session));
			ptr.newColumn(true);
			MTF mtf = oolMTF(strm, fnpath, ptr);
			Object object = null;
			MTF.SavedState savedstate = mtf.sast;
			mkStream(mtf);
			Object object_65_ = null;
			fmtList(strm, fnpath, list, bool);
			i = reg.rc[1] - ((int[]) fnpath.car())[3];
			savedstate.restore();
		}
		fnpath = new Path(new int[] { reg.rc[0], reg.rc[1], reg.rc[2], i });
		Ptr ptr = new Ptr(new TD(
				rule(Attr.newWord("xsl-footnote-separator"), 0), session));
		ptr.newColumn(true);
		MTF mtf = oolMTF(strm, fnpath, ptr);
		Object object = null;
		mkStream(mtf);
		Object object_66_ = null;
		fmtList(strm, fnpath, footnotes, bool);
		fnstate = footnotes.isEmpty() ? (short) 1 : (short) 2;
	}

	private void mkBody() {
		int i = reg.rc[1];
		int i_67_ = mkFloats();
		List.Mark mark = null;
		int i_68_ = -2147483648;
		Bodypoint bodypoint = new Bodypoint();
		bodypoint.save();
		bodynotes.clear();
		int i_69_ = footnotes.length();
		if (i_69_ != 0) {
			Enumeration enumeration = footnotes.elements();
			while (enumeration.hasMoreElements())
				bodynotes.snoc(((Ptr) enumeration.nextElement()).ln());
		}
		bodywide = false;
		for (;;) {
			com.renderx.xep.cmp.Xattr xattr = null;
			Forkpoint forkpoint = new Forkpoint();
			int i_70_ = i;
			int i_71_ = i_67_;
			int i_72_ = 1;
			int i_73_ = 0;
			int i_74_ = 0;
			int i_75_ = 0;
			int i_76_ = 0;
			boolean bool = false;
			boolean bool_77_ = false;
			path = new Path();
			fnpath = new Path();
			if (bodywide)
				mark = strm.mark();
			do {
				if (i_73_ == 0) {
					i_68_ = 2147483647;
					if (!bool) {
						forkpoint.save();
						xattr = (this.ptr.atwide ? (com.renderx.xep.cmp.Xattr) this.ptr.wide
								: reg);
						i_72_ = xattr.get(Attn.$column_count).count();
						i_75_ = xattr.get(Attn.$column_gap).length_or_ratio(
								reg.rc[2] - reg.rc[0]);
						i_76_ = ((reg.rc[2] - reg.rc[0] - (i_72_ - 1) * i_75_) / i_72_);
						bool_77_ = bool_77_ || i_72_ != 1;
					}
				}
				int i_78_;
				if (xattr.get(Attn.$writing_mode) == Attr.rl_tb)
					i_78_ = reg.rc[0] + (i_76_ + i_75_) * (i_72_ - 1 - i_73_);
				else
					i_78_ = reg.rc[0] + (i_76_ + i_75_) * i_73_;
				int i_79_ = i_78_ + i_76_;
				path.clear().append(
						new Path(new int[] { i_78_, i_70_, i_79_, i_71_ }));
				if (!bodywide) {
					fnpath.clear();
					if (!footnotes.isEmpty())
						fnSpace(path);
				}
				if (state != 1) {
					if (!bodywide)
						mark = strm.mark();
					this.ptr.newColumn(i_71_ == i_67_);
					MTF mtf = colMTF();
					state = mkStream(mtf);
					switch (state) {
					case 1:
						break;
					case 2:
						if (bool) {
							if (i_73_ == i_72_ - 1) {
								forkpoint.restore();
								i_70_ -= (i_71_ - i_70_) / session.config.CJF;
								if (i_71_ - i_70_ < session.config.CJF)
									i_70_--;
								if (i_70_ < i)
									i_70_ = i;
								i_73_ = 0;
								continue;
							}
						} else {
							i_74_ += i_71_ - i_70_;
							if (!path.isEmpty()) {
								int[] is = (int[]) path.car();
								int[] is_80_ = (int[]) path.last();
								i_74_ -= is_80_[3] - is_80_[1];
							}
						}
						break;
					case 3:
					case 4: {
						TD td = (TD) this.ptr.tab.car();
						bool_77_ = true;
						if (state == 3)
							this.ptr.wide = (Item.Wide) td.flow.car();
						td.flow = (Flow) td.flow.cdr();
						if (bool) {
							bool = false;
							this.ptr.atwide = state == 3;
							i_73_ = -1;
							i_71_ = i_70_;
							i_70_ = i;
							i_74_ = 0;
						} else {
							i_74_ += i_71_ - i_70_;
							if (!path.isEmpty()) {
								int[] is = (int[]) path.car();
								int[] is_81_ = (int[]) path.last();
								i_74_ -= is_81_[3] - is_81_[1];
							}
							i_70_ = i_71_ - i_74_ / i_72_;
							forkpoint.restore();
							i_73_ = 0;
							bool = true;
							continue;
						}
						break;
					}
					default:
						throw new InternalException(
								"illegal state from mkStream");
					}
					Object object = null;
				}
				if (bfsstate != null && strm.last() == bfsmark)
					bfsstate.restore();
				if (!path.isEmpty()) {
					int i_82_ = vshift(xattr, path);
					if (Util.gt0(i_82_)) {
						if (bool_77_) {
							if (i_68_ > i_82_)
								i_68_ = i_82_;
						} else if (!bodywide) {
							strm.insert(mark, new Cntl.Translate(0, -i_82_));
							strm.append(new Cntl.Translate(0, i_82_));
							int[] is = (int[]) path.last();
							is[3] -= i_82_;
						}
					}
				}
				if (bodywide) {
					fnstate = (short) 1;
					if (i_68_ != 2147483647) {
						strm.insert(mark, new Cntl.Translate(0, -i_68_));
						strm.append(new Cntl.Translate(0, i_68_));
					}
				} else if (!footnotes.isEmpty()) {
					boolean bool_83_ = !isFilled();
					Ptr ptr = (new Ptr(new TD(rule(Attr
							.newWord("xsl-footnote-separator"), 0), session)));
					ptr.newColumn(true);
					MTF mtf = oolMTF(strm, fnpath, ptr);
					Object object = null;
					mkStream(mtf);
					Object object_84_ = null;
					fmtList(strm, fnpath, footnotes, bool_83_);
					fnstate = footnotes.isEmpty() ? (short) 1 : (short) 2;
				} else
					fnstate = (short) 1;
			} while (++i_73_ != i_72_);
			if (session.config.PAGEWIDE_FOOTNOTES && bool_77_) {
				boolean bool_85_ = !isFilled();
				if (bodywide) {
					if (!footnotes.isEmpty())
						mkBodynotes(i, bool_85_);
				} else if (!bodynotes.isEmpty()) {
					i = measureBodynotes(i, bool_85_);
					bodypoint.restore();
					footnotes.clear();
					for (int i_86_ = 0; i_86_ != i_69_; i_86_++)
						footnotes.snoc(bodynotes.shift());
					bodywide = true;
					continue;
				}
			}
			if (bodywide || !bool_77_ || i_68_ == 2147483647)
				break;
			bodypoint.restore();
			bodywide = true;
		}
	}

	private void fillEndingIncludingCarryover() {
		Enumeration enumeration = ptr.tab.elements();
		while (enumeration.hasMoreElements()) {
			TD td = (TD) enumeration.nextElement();
			if (!td.bstack.isEmpty()) {
				Enumeration enumeration_87_ = td.bstack.elements();
				Item.Block block = td.b;
				do {
					if (block.markers != null)
						page.markers.Last(block.markers);
					block = (Item.Block) enumeration_87_.nextElement();
				} while (enumeration_87_.hasMoreElements());
			}
			if (!td.sstack.isEmpty()) {
				Enumeration enumeration_88_ = td.sstack.elements();
				Item.Span span = td.s;
				do {
					if (span.markers != null)
						page.markers.Last(span.markers);
					span = (Item.Span) enumeration_88_.nextElement();
				} while (enumeration_88_.hasMoreElements());
			}
		}
	}

	private void appendAbsolute(List list, Region region) {
		List list_89_ = new List();
		Enumeration enumeration = page.strmtab.elements();
		while (enumeration.hasMoreElements()) {
			List list_90_ = (List) enumeration.nextElement();
			if (list_90_.isPair() && list_90_.car() == region)
				list.append(list_90_.cdr());
			else
				list_89_.snoc(list_90_);
		}
		page.strmtab.clear().append(list_89_);
	}

	private boolean skipBigInline() {
		Enumeration enumeration = ptr.tab.elements();
		while (enumeration.hasMoreElements()) {
			TD td = (TD) enumeration.nextElement();
			Item item = (Item) td.flow.car();
			Flow flow = td.flow;
			List list = new List();
			int i = 0;
			for (/**/; (item instanceof Item.Span || item instanceof Item.Ends
					|| item instanceof Item.Inline
					|| item instanceof Item.Space.Inline
					|| item instanceof Item.Washer.Inline || item instanceof Item.Exception); item = (Item) flow
					.car()) {
				if (item instanceof Item.Span)
					i++;
				else if (item instanceof Item.Ends)
					i--;
				list.cons(flow);
				if (i == 0) {
					while (list.isPair()) {
						Flow flow_91_ = (Flow) list.shift();
						item = (Item) ((Item) flow_91_.car()).clone();
						item.skip();
						flow_91_.setCar(item);
					}
					return true;
				}
				flow = (Flow) flow.cdr();
			}
		}
		return false;
	}

	private int cutBlankTail() {
		int i = 0;
		for (;;) {
			if (pages.isEmpty())
				return i;
			page = (Page) pages.car();
			Enumeration enumeration = page.strmtab.elements();
			while (enumeration.hasMoreElements()) {
				Enumeration enumeration_92_ = ((List) enumeration.nextElement())
						.elements();
				while (enumeration_92_.hasMoreElements()) {
					Object object = enumeration_92_.nextElement();
					if (object instanceof Line || object instanceof Item.Filled)
						return i;
				}
			}
			i++;
			pages.shift();
		}
	}

	private void mkFlow() {
		session.openState("flow");
		try {
			Page page = null;
			int i = -1;
			try {
				seq.master.rewind();
				pages = new List();
				ptr = new Ptr(new TD(seq.flow, session));
				seq.flow = null;
				topfloats.clear();
				footnotes.clear();
				flstate = (short) -1;
				state = (short) -1;
				fnstate = (short) -1;
				int i_93_ = 0;
				i = 0;
				while_14_: for (/**/; true; i++) {
					this.page = new Page(this, i, seq, 0x10 | (i == 0 ? 1 : 4));
					if (recovery_step != 0)
						recovery_step--;
					Enumeration enumeration = ptr.tab.elements();
					while (enumeration.hasMoreElements()) {
						TD td = (TD) enumeration.nextElement();
						if (td.waiting == 4
								|| td.waiting == (this.page.number % 2 == 0 ? 6
										: 5))
							td.waiting = (short) 3;
					}
					this.page.ptr = ptr.ln();
					this.page.state = state;
					this.page.topfloats = new List();
					this.page.footnotes = new List();
					Enumeration enumeration_94_ = topfloats.elements();
					while (enumeration_94_.hasMoreElements())
						this.page.topfloats.snoc(((Ptr) enumeration_94_
								.nextElement()).ln());
					Enumeration enumeration_95_ = footnotes.elements();
					while (enumeration_95_.hasMoreElements())
						this.page.footnotes.snoc(((Ptr) enumeration_95_
								.nextElement()).ln());
					boolean bool = false;
					page = null;
					for (;;) {
						session.event("page-number", this.page.id);
						pages.cons(this.page);
						feedPage();
						if (!this.page.master.regtab.containsKey(seq.flowname)) {
							if (bool) {
								session.error("No region " + seq.flowname
										+ " on last page #" + this.page.id);
								throw new LastPageException();
							}
							if (++i_93_ == 2)
								session.error("No region " + seq.flowname
										+ " on page #" + this.page.id);
							else
								continue while_14_;
							break while_14_;
						}
						i_93_ = 0;
						fillStartingIncludingCarryover();
						this.page.strmtab.snoc(strm = new List());
						reg = ((Region) this.page.master.regtab
								.get(seq.flowname));
						firstcol = (short) 0;
						if (reg.phi != 0)
							strm.append(new Cntl.Rotate(reg.phi));
						mkBody();
						appendAbsolute(strm, reg);
						if (reg.phi != 0)
							strm.append(new Cntl.Rotate(360 - reg.phi));
						fillEndingIncludingCarryover();
						this.page.markers.Reconcile();
						if (flstate != 1 || !topfloats.isEmpty() || state != 1
								|| fnstate != 1)
							break;
						if (bool)
							break while_14_;
						i -= cutBlankTail();
						if (uneven(i, seq.get(Attn.$force_page_count))) {
							pages
									.cons(this.page = new Page(this, ++i, seq,
											10));
							feedPage();
							break while_14_;
						}
						if (pages.isEmpty())
							break while_14_;
						page = (Page) pages.car();
						seq.master.retry();
						this.page = new Page(this, i, seq, 0x10 | (i == 0 ? 128
								: 2));
						if (this.page.master == page.master)
							break while_14_;
						pages.shift();
						ptr = page.ptr;
						state = page.state;
						topfloats = page.topfloats;
						footnotes = page.footnotes;
						bool = true;
					}
					if (bool)
						throw new LastPageException();
					if (!isFilled()
							|| (!ptr.movedSince(this.page.ptr)
									&& (!this.page.footnotes.isPair() || ((footnotes
											.length() >= this.page.footnotes
											.length()) && !(((Ptr) footnotes
											.car())
											.movedSince((Ptr) this.page.footnotes
													.car())))) && (!this.page.topfloats
									.isPair() || ((topfloats.length() >= this.page.topfloats
									.length()) && !(((Ptr) topfloats.car())
									.movedSince((Ptr) this.page.topfloats.car())))))) {
						Enumeration enumeration_96_ = ptr.tab.elements();
						while (enumeration_96_.hasMoreElements()) {
							TD td = (TD) enumeration_96_.nextElement();
							if (td.waiting == (this.page.number % 2 == 0 ? 5
									: 6)) {
								if (!isFilled()) {
									Page page_97_ = this.page;
									seq.master.retry();
									this.page = new Page(this, i, seq,
											0x8 | (i == 0 ? 1 : 4));
									if (this.page.master != page_97_.master) {
										pages.setCar(this.page);
										feedPage();
									}
								}
								continue while_14_;
							}
						}
						pages.shift();
						i--;
						if (flstate == 2) {
							session
									.warning("no space for a float, some data will be lost");
							topfloats = topfloats.cdr();
						}
						if (state == 2) {
							session
									.error("no space for an element, trying to recover");
							if (recovery_step == 1) {
								if (!skipBigInline()) {
									session
											.error("failed to recover, truncating");
									state = (short) 1;
								}
							} else
								recovery_step = (short) 2;
						}
						if (fnstate == 2) {
							session
									.warning("no place for a footnote, some data will be lost");
							footnotes.shift();
						}
					}
				}
			} catch (LastPageException lastpageexception) {
				session
						.warning("last page's content does not fit into the last page, reverting to 'rest'");
				pages.setCar(page);
				if (uneven(i + 1, seq.get(Attn.$force_page_count))) {
					pages.cons(this.page = new Page(this, ++i, seq, 12));
					feedPage();
				}
				pages.cons(this.page = new Page(this, ++i, seq, 10));
				feedPage();
			}
		} catch (NoPageMasterException nopagemasterexception) {
			session.error(nopagemasterexception.toString());
		} finally {
			session.closeState("flow");
		}
	}

	private void mkStatic() {
		session.openState("static-content");
		try {
			Enumeration enumeration = pages.reverse().elements();
			while (enumeration.hasMoreElements()) {
				page = (Page) enumeration.nextElement();
				session.event("page-number", page.id);
				page.ptr = null;
				page.topfloats = null;
				page.footnotes = null;
				Array array = new Array();
				Enumeration enumeration_98_ = page.master.regtab.keys();
				int i = 0;
				while (enumeration_98_.hasMoreElements()) {
					Object object = enumeration_98_.nextElement();
					boolean bool = false;
					int i_99_;
					for (i_99_ = 0; i_99_ < i; i_99_++) {
						if (array.get(i_99_).toString().compareTo(
								object.toString()) > 0)
							break;
					}
					for (int i_100_ = i; i_100_ > i_99_; i_100_--)
						array.put(i_100_, array.get(i_100_ - 1));
					array.put(i_99_, object);
					i++;
				}
				for (int i_101_ = 0; i_101_ < array.length(); i_101_++) {
					Attr attr = (Attr) array.get(i_101_);
					Flow flow = (Flow) seq.sctab.get(attr);
					if (flow != null) {
						session.event("region-name", attr.toString());
						reg = (Region) page.master.regtab.get(attr);
						firstcol = (short) 0;
						strm = new List();
						page.strmtab.snoc(strm);
						this.ptr = new Ptr(new TD(flow, session));
						topfloats.clear();
						footnotes.clear();
						state = (short) -1;
						fnstate = (short) -1;
						if (reg.phi != 0)
							strm.append(new Cntl.Rotate(reg.phi));
						path = new Path(reg.rc);
						fnpath = new Path();
						if (state != 1) {
							List.Mark mark = strm.mark();
							this.ptr.newColumn(true);
							MTF mtf = colMTF();
							mtf.runoff = true;
							state = mkStream(mtf);
							if (!path.isEmpty()) {
								int i_102_ = vshift(reg, path);
								if (Util.gt0(i_102_)) {
									strm.insert(mark, new Cntl.Translate(0,
											-i_102_));
									strm.append(new Cntl.Translate(0, i_102_));
									int[] is = (int[]) path.last();
									is[3] -= i_102_;
								}
							}
						}
						if (!footnotes.isEmpty()) {
							boolean bool = !isFilled();
							Ptr ptr = (new Ptr(new TD(rule((Attr
									.newWord("xsl-footnote-separator")), 0),
									session)));
							ptr.newColumn(true);
							MTF mtf = oolMTF(strm, fnpath, ptr);
							Object object = null;
							mkStream(mtf);
							Object object_103_ = null;
							fmtList(strm, fnpath, footnotes, bool);
							fnstate = footnotes.isEmpty() ? (short) 1
									: (short) 2;
						} else
							fnstate = (short) 1;
						appendAbsolute(strm, reg);
						if (reg.phi != 0)
							strm.append(new Cntl.Rotate(360 - reg.phi));
					}
				}
				Enumeration enumeration_104_ = page.markers.keys();
				while (enumeration_104_.hasMoreElements()) {
					Attr attr = (Attr) enumeration_104_.nextElement();
					Flow flow = ((Marker) page.markers.get(attr)).get(3);
					if (flow != null)
						seqmarkers.put(attr, flow);
				}
				page.markers = null;
			}
		} finally {
			session.closeState("static-content");
		}
	}

	public List mkBook() {
		passno = 0;
		book = new List();
		idtab = new Hashtable();
		iktab = new Hashtable();
		keytab = new KeyTable(session);
		seqmarkers = new Markers();
		topfloats = new List();
		footnotes = new List();
		bodynotes = new List();
		Enumeration enumeration = sequences.elements();
		while (enumeration.hasMoreElements()) {
			seq = (Sequence) enumeration.nextElement();
			session.openState("sequence");
			try {
				session.event("master-reference", seq.get(
						Attn.$master_reference).word());
				if (seq.get(Attn.$initial_page_number) instanceof Attr.Count) {
					initial_page_number = seq.get(Attn.$initial_page_number)
							.count();
					if (initial_page_number < 1)
						initial_page_number = 1;
				} else if (initial_page_number % 2 == 1 ? (seq
						.get(Attn.$initial_page_number) == Attr
						.newWord("auto-even")) : (seq
						.get(Attn.$initial_page_number) == Attr
						.newWord("auto-odd")))
					initial_page_number++;
				mkFlow();
				if (pages.isPair() && seq.lids.isPair()) {
					Enumeration enumeration_105_ = seq.lids.elements();
					while (enumeration_105_.hasMoreElements()) {
						Item.Lid lid = new Item.Lid(seq,
								(Attr) enumeration_105_.nextElement());
						lid.page = (Page) pages.car();
						lid.x = lid.y = 0;
						idtab.put(lid.id, lid);
					}
				}
				mkStatic();
				book.append(pages.reverse());
				initial_page_number += pages.length();
				seqmarkers.clear();
			} finally {
				session.closeState("sequence");
			}
		}
		seqmarkers = null;
		topfloats = footnotes = null;
		path = fnpath = null;
		reg = null;
		emptypath = null;
		passno = 1;
		Enumeration enumeration_106_ = outline.elements();
		while (enumeration_106_.hasMoreElements())
			((Instruction) enumeration_106_.nextElement()).pass(this);
		Enumeration enumeration_107_ = book.elements();
		while (enumeration_107_.hasMoreElements()) {
			page = (Page) enumeration_107_.nextElement();
			Enumeration enumeration_108_ = page.strmtab.elements();
			while (enumeration_108_.hasMoreElements()) {
				Enumeration enumeration_109_ = ((List) enumeration_108_
						.nextElement()).elements();
				while (enumeration_109_.hasMoreElements())
					((Instruction) enumeration_109_.nextElement()).pass(this);
			}
		}
		idtab = null;
		keytab = null;
		passno = 2;
		Enumeration enumeration_110_ = book.elements();
		while (enumeration_110_.hasMoreElements()) {
			int[][] is = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
			M = is;
			page = (Page) enumeration_110_.nextElement();
			if (page.master.phi != 0)
				M = mul(RM[page.master.phi / 90 - 1], M, 3, 3, 3);
			Enumeration enumeration_111_ = page.strmtab.elements();
			while (enumeration_111_.hasMoreElements()) {
				Enumeration enumeration_112_ = ((List) enumeration_111_
						.nextElement()).elements();
				while (enumeration_112_.hasMoreElements())
					((Instruction) enumeration_112_.nextElement()).pass(this);
			}
		}
		return book;
	}

	private boolean isFilled() {
		Enumeration enumeration = strm.elements();
		while (enumeration.hasMoreElements()) {
			Object object = enumeration.nextElement();
			if (object instanceof Line || object instanceof Item.Filled)
				return true;
		}
		return false;
	}

	public void process(Line line) {
		switch (passno) {
		case 1:
			if (line.bpb != null) {
				line.bpb.shift();
				line.x += line.bpb.shiftx;
				line.y += line.bpb.shifty;
				line.bpb = null;
			}
			if (line.pageids) {
				List list = new List();
				Enumeration enumeration = line.words.elements();
				while (enumeration.hasMoreElements()) {
					Item item = (Item) enumeration.nextElement();
					if (item instanceof Item.PageId) {
						Item.PageId pageid = (Item.PageId) item;
						Attr attr = pageid.get(Attn.$ref_id);
						if (!idtab.containsKey(attr)) {
							session
									.warning("unresolved page-number-citation: '"
											+ attr + "'");
							idtab.put(attr, Item.Id.UNRESOLVED);
						}
						Item.Text text = new Item.Text((((Item.Id) idtab
								.get(attr)).page.id), pageid.a, session);
						text.offset = pageid.offset;
						item = text;
						line.width += ((Item.Text) item).width - pageid.width;
					}
					list.append(item);
				}
				line.words = list;
			}
			switch (line.align) {
			case 5:
				line.align = page.number % 2 == 0 ? (short) 0 : (short) 2;
				break;
			case 4:
				line.align = page.number % 2 == 1 ? (short) 0 : (short) 2;
				break;
			}
			line.align();
			break;
		case 2:
			break;
		default:
			throw new InternalException("illegal pass number");
		}
	}

	public void process(BPB bpb) {
		switch (passno) {
		case 1:
			if (bpb.par != null) {
				bpb.par.shift();
				if (bpb instanceof BPB.Block)
					((BPB.Block) bpb).shift();
				int i = bpb.par.get(Attn.$reference_orientation).count();
				int i_113_;
				int i_114_;
				switch (i / 90) {
				case 0:
					i_113_ = bpb.par.shiftx;
					i_114_ = bpb.par.shifty;
					break;
				case 1:
					i_113_ = bpb.par.shifty;
					i_114_ = -bpb.par.shiftx;
					break;
				case 2:
					i_113_ = -bpb.par.shiftx;
					i_114_ = -bpb.par.shifty;
					break;
				case 3:
					i_113_ = -bpb.par.shifty;
					i_114_ = bpb.par.shiftx;
					break;
				default:
					throw new InternalException(
							"illegal value of reference-orientation: "
									+ bpb.get(Attn.$reference_orientation));
				}
				bpb.r[0] += i_113_;
				bpb.r[2] += i_113_;
				bpb.r[1] += i_114_;
				bpb.r[3] += i_114_;
				bpb.par = null;
			}
			if (bpb instanceof BPB.Anchor) {
				BPB.Anchor anchor = (BPB.Anchor) bpb;
				if (anchor.containsKey(Attn.$internal_destination)) {
					Attr attr = anchor.get(Attn.$internal_destination);
					if (!idtab.containsKey(attr)) {
						session.warning("unresolved internal destination: '"
								+ attr + "'");
						idtab.put(attr, Item.Id.UNRESOLVED);
					}
					anchor.id = (Item.Id) idtab.get(attr);
				}
			}
			break;
		case 2:
			if (bpb instanceof BPB.Anchor) {
				if (bpb.writing_mode == Attr.rl_tb) {
					int i = -bpb.r[0];
					bpb.r[0] = -bpb.r[2];
					bpb.r[2] = i;
				}
				BPB.Anchor anchor = (BPB.Anchor) bpb;
				int[] is = {
						(M[0][0] * anchor.r[0] + M[1][0] * anchor.r[1] + M[2][0]),
						(M[0][1] * anchor.r[0] + M[1][1] * anchor.r[1] + M[2][1]),
						(M[0][0] * anchor.r[2] + M[1][0] * anchor.r[3] + M[2][0]),
						(M[0][1] * anchor.r[2] + M[1][1] * anchor.r[3] + M[2][1]) };
				anchor.rabs = is;
			}
			break;
		default:
			throw new InternalException("illegal pass number");
		}
	}

	public void process(Cntl.Rotate rotate) {
		switch (passno) {
		case 1:
			break;
		case 2:
			M = mul(RM[rotate.phi / 90 - 1], M, 3, 3, 3);
			break;
		default:
			throw new InternalException("illegal pass number");
		}
	}

	public void process(Cntl.Translate translate) {
		switch (passno) {
		case 1:
			break;
		case 2:
			TM[2][0] = translate.x;
			TM[2][1] = translate.y;
			M = mul(TM, M, 3, 3, 3);
			break;
		default:
			throw new InternalException("illegal pass number");
		}
	}

	public void process(Item.Text text) {
		/* empty */
	}

	public void process(Item.Leader leader) {
		/* empty */
	}

	public void process(Item.Image image) {
		/* empty */
	}

	public void process(Item.Washer.Inline inline) {
		/* empty */
	}

	public void process(Item.Filled filled) {
		/* empty */
	}

	public void process(Item.Id id) {
		switch (passno) {
		case 1:
			break;
		case 2: {
			if (id.writing_mode == Attr.rl_tb)
				id.x = -id.x;
			int i = id.x;
			int i_115_ = id.y;
			id.x = M[0][0] * i + M[1][0] * i_115_ + M[2][0];
			id.y = M[0][1] * i + M[1][1] * i_115_ + M[2][1];
			id.x = (int) Math.round((double) id.x
					* session.config.SHIFT_TARGET_X);
			id.y = (int) Math
					.round(((double) id.y * session.config.SHIFT_TARGET_Y)
							+ ((double) id.page.master.height * (1.0 - session.config.SHIFT_TARGET_Y)));
			break;
		}
		default:
			throw new InternalException("illegal pass number");
		}
	}

	public void process(Item.Pinpoint pinpoint) {
		switch (passno) {
		case 1:
			break;
		case 2: {
			if (pinpoint.writing_mode == Attr.rl_tb)
				pinpoint.x = -pinpoint.x;
			int i = pinpoint.x;
			int i_116_ = pinpoint.y;
			pinpoint.x = M[0][0] * i + M[1][0] * i_116_ + M[2][0];
			pinpoint.y = M[0][1] * i + M[1][1] * i_116_ + M[2][1];
			break;
		}
		default:
			throw new InternalException("illegal pass number");
		}
	}

	public void process(Item.Bookmark bookmark) {
		if (bookmark.containsKey(Attn.$internal_destination)) {
			Attr attr = bookmark.get(Attn.$internal_destination);
			if (!idtab.containsKey(attr)) {
				session.warning("unresolved internal destination: '" + attr
						+ "'");
				idtab.put(attr, Item.Id.UNRESOLVED);
			}
			bookmark.id = (Item.Id) idtab.get(attr);
		}
	}

	private final int[][] mul(int[][] is, int[][] is_117_, int i, int i_118_,
			int i_119_) {
		int[][] is_120_ = new int[i][i_119_];
		for (int i_121_ = 0; i_121_ != i; i_121_++) {
			for (int i_122_ = 0; i_122_ != i_119_; i_122_++) {
				is_120_[i_121_][i_122_] = 0;
				for (int i_123_ = 0; i_123_ != i_118_; i_123_++)
					is_120_[i_121_][i_122_] += is[i_121_][i_123_]
							* is_117_[i_123_][i_122_];
			}
		}
		return is_120_;
	}
}