/*
 * Path - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.fmt;

import java.util.Enumeration;

import com.renderx.util.Applicator;
import com.renderx.util.List;
import com.renderx.xep.lib.InternalException;

final class Path extends List {
	public static final short SUB = 0;

	public static final short MUL = 1;

	Path() {
		/* empty */
	}

	Path(int[] is) {
		add(is);
	}

	Path(int[][] is) {
		for (int i = 0; i != is.length; i++)
			add(is[i]);
	}

	Path add(int[] is) {
		if (this.isPair()) {
			int[] is_0_ = (int[]) this.last();
			if (is_0_[0] == is[0] && is_0_[2] == is[2] && is_0_[1] == is[3])
				is_0_[1] = is[1];
			else
				this.snoc(is.clone());
		} else
			this.snoc(is.clone());
		return this;
	}

	Path add(Path path_1_) {
		Enumeration enumeration = path_1_.elements();
		while (enumeration.hasMoreElements())
			add((int[]) enumeration.nextElement());
		return this;
	}

	public Object clone() {
		Path path_2_ = new Path();
		Enumeration enumeration = this.elements();
		while (enumeration.hasMoreElements())
			path_2_.snoc(((int[]) enumeration.nextElement()).clone());
		return path_2_;
	}

	public String toString() {
		return this.map(new List(), new Applicator() {
			public Object f(Object object) {
				int[] is = (int[]) object;
				int i = 0;
				String string = "[";
				for (;;) {
					string += is[i];
					if (++i == is.length)
						break;
					string += " ";
				}
				string += "]";
				return string;
			}
		}).toString();
	}

	Path flipX() {
		Path path_4_ = new Path();
		Enumeration enumeration = this.elements();
		while (enumeration.hasMoreElements()) {
			int[] is = (int[]) enumeration.nextElement();
			is = (int[]) is.clone();
			int i = is[1];
			is[1] = -is[3];
			is[3] = -i;
			path_4_.cons(is);
		}
		return path_4_;
	}

	boolean covers(Path path_5_) {
		path_5_ = (Path) path_5_.clone();
		Enumeration enumeration = this.elements();
		while (enumeration.hasMoreElements())
			path_5_.apply((short) 0, (int[]) enumeration.nextElement());
		return !path_5_.isPair();
	}

	Path apply(short i, int[] is) {
		Path path_6_ = new Path();
		Enumeration enumeration = this.elements();
		while (enumeration.hasMoreElements()) {
			int[] is_7_ = (int[]) enumeration.nextElement();
			if (is_7_[1] < is[3] && is[3] < is_7_[3]) {
				int[] is_8_ = (int[]) is_7_.clone();
				int[] is_9_ = (int[]) is_7_.clone();
				is_8_[1] = is[3];
				is_9_[3] = is[3];
				path_6_.snoc(is_8_).snoc(is_9_);
			} else
				path_6_.snoc(is_7_);
		}
		Path path_10_ = path_6_;
		path_6_ = new Path();
		enumeration = path_10_.elements();
		while (enumeration.hasMoreElements()) {
			int[] is_11_ = (int[]) enumeration.nextElement();
			if (is_11_[1] < is[1] && is[1] < is_11_[3]) {
				int[] is_12_ = (int[]) is_11_.clone();
				int[] is_13_ = (int[]) is_11_.clone();
				is_12_[1] = is[1];
				is_13_[3] = is[1];
				path_6_.snoc(is_12_).snoc(is_13_);
			} else
				path_6_.snoc(is_11_);
		}
		this.clear();
		switch (i) {
		case 0:
			enumeration = path_6_.elements();
			while (enumeration.hasMoreElements()) {
				int[] is_14_ = (int[]) enumeration.nextElement();
				if (is[1] < is_14_[3] && is_14_[3] <= is[3]) {
					if (is_14_[0] < is[0] && is[0] < is_14_[2]) {
						int[] is_15_ = (int[]) is_14_.clone();
						is_15_[2] = is[0];
						add(is_15_);
					}
					if (is_14_[0] < is[2] && is[2] < is_14_[2]) {
						int[] is_16_ = (int[]) is_14_.clone();
						is_16_[0] = is[2];
						add(is_16_);
					}
					if (is_14_[0] >= is[2] || is[0] >= is_14_[2])
						add(is_14_);
				} else
					this.snoc(is_14_);
			}
			break;
		case 1:
			enumeration = path_6_.elements();
			while (enumeration.hasMoreElements()) {
				int[] is_17_ = (int[]) enumeration.nextElement();
				if (is[1] < is_17_[3] && is_17_[3] <= is[3]) {
					if (is_17_[0] < is[0] && is[0] < is_17_[2]) {
						int[] is_18_ = (int[]) is_17_.clone();
						is_18_[0] = is[0];
						if (is[2] < is_17_[2])
							is_18_[2] = is[2];
						add(is_18_);
					} else if (is_17_[0] < is[2] && is[2] < is_17_[2]) {
						int[] is_19_ = (int[]) is_17_.clone();
						is_19_[2] = is[2];
						this.snoc(is_19_);
					}
				}
			}
		/* fall through */
		default:
			throw new InternalException("illegal operation on path: " + i);
		}
		return this;
	}
}