/*
 * TD - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.fmt;

import java.util.Enumeration;

import com.renderx.util.Applicator;
import com.renderx.util.Stack;
import com.renderx.xep.cmp.Flow;
import com.renderx.xep.cmp.Item;
import com.renderx.xep.lib.InternalException;
import com.renderx.xep.lib.Session;
import com.renderx.xep.pre.AttList;

final class TD implements Cloneable {
	Flow flow;

	int y;

	int y0;

	int x;

	int width;

	Edge start_edge;

	Edge end_edge;

	Stack bstack = new Stack();

	Stack sstack = new Stack();

	Stack frstack = new Stack();

	Item.Block b;

	Item.Span s;

	Item.Space.Display daccu = new Item.Space.Display();

	static final short EMPTY_NONE = 0;

	static final short EMPTY_KEEP = 1;

	static final short EMPTY_LOCAL = 2;

	static final short EMPTY_AREA = 3;

	static final short EMPTY_COLUMN = 4;

	short empty = 4;

	boolean filled = false;

	boolean container_edge = false;

	int lineno = -2147483648;

	Group group = Group.nogroup;

	static final short WAIT_NOTHING = 0;

	static final short WAIT_SYNC = 1;

	static final short WAIT_AREA = 2;

	static final short WAIT_COLUMN = 3;

	static final short WAIT_PAGE = 4;

	static final short WAIT_ODD_PAGE = 5;

	static final short WAIT_EVEN_PAGE = 6;

	short waiting = 0;

	int tno;

	private final Session session;

	private static final Copier copier = new Copier();

	private static class Copier implements Applicator {
		private Copier() {
			/* empty */
		}

		public Object f(Object object) {
			return ((Item) object).clone();
		}
	}

	TD(Flow flow, Session session) {
		this.flow = flow;
		this.session = session;
		tno = session.nextno();
		b = new Item.Block(AttList.empty, session);
		s = new Item.Span(AttList.empty, session);
	}

	public Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException clonenotsupportedexception) {
			throw new InternalException("TD is not cloneable: "
					+ clonenotsupportedexception.getMessage());
		}
	}

	TD ln() {
		TD td_0_ = (TD) clone();
		td_0_.bstack = (Stack) bstack.map(new Stack(), copier);
		td_0_.frstack = (Stack) frstack.map(new Stack(), copier);
		td_0_.sstack = (Stack) sstack.map(new Stack(), copier);
		td_0_.b = (Item.Block) b.clone();
		td_0_.s = (Item.Span) s.clone();
		td_0_.daccu = (Item.Space.Display) daccu.clone();
		return td_0_;
	}

	TD cp() {
		TD td_1_ = ln();
		Enumeration enumeration = td_1_.bstack.elements();
		while (enumeration.hasMoreElements()) {
			Item.Block block = (Item.Block) enumeration.nextElement();
			if (block.bpb != null)
				block.bpb = new BPB.Block(block, (int[]) block.bpb.r.clone(),
						session);
		}
		if (b.bpb != null)
			td_1_.b.bpb = new BPB.Block(b, (int[]) b.bpb.r.clone(), session);
		return td_1_;
	}

	TD dup(Flow flow) {
		TD td_2_ = (TD) clone();
		td_2_.tno = session.nextno();
		td_2_.flow = flow;
		td_2_.empty = (short) 2;
		td_2_.filled = false;
		td_2_.bstack = (Stack) bstack.clone();
		td_2_.frstack = (Stack) frstack.clone();
		td_2_.sstack = (Stack) sstack.clone();
		if (td_2_.sstack.isEmpty())
			td_2_.s = (Item.Span) s.clone();
		else
			td_2_.sstack.setLast(((Item.Span) sstack.last()).clone());
		td_2_.daccu = (Item.Space.Display) daccu.clone();
		return td_2_;
	}

	public String toString() {
		return Integer.toString(tno);
	}
}