/*
 * MTF - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.fmt;

import java.util.Enumeration;

import com.renderx.util.Hashtable;
import com.renderx.util.List;
import com.renderx.xep.cmp.Flow;
import com.renderx.xep.cmp.Item;
import com.renderx.xep.cmp.ItemDispatcher;
import com.renderx.xep.cmp.TableContext;
import com.renderx.xep.lib.InternalException;
import com.renderx.xep.lib.Session;
import com.renderx.xep.lib.Util;
import com.renderx.xep.pre.AttList;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Attributed;

final class MTF implements ItemDispatcher {
	static final short COL = 0;

	static final short AUX = 1;

	static final short OOL = 2;

	short mode;

	static final short KEEPT = 1;

	static final short KEEPW = 2;

	short keep = 3;

	boolean runoff = false;

	List strm;

	Path path;

	Path fnpath;

	Ptr ptr;

	BkMaker bkm;

	boolean footnotes_pending = false;

	Session session;

	Edge start_edge = Edge.empty;

	Edge end_edge = Edge.empty;

	int[] a = null;

	private Hashtable breg = null;

	int Y = -2147483648;

	int dW = -2147483648;

	int dWs = -2147483648;

	int dWe = -2147483648;

	int dW0;

	TD cur = null;

	private Hashtable lindtab = null;

	LineBuffer.Phrase phrase = null;

	private Attributed a0 = null;

	private Attributed fla = null;

	private Attributed fla0 = null;

	private Item.TextTraits tt0 = null;

	private short action = -1;

	private static final short NEXT = 0;

	private static final short SWITCH = 1;

	private static final short RETURN = 2;

	private short retcode = -1;

	static final short EXCEPTION = 0;

	static final short END_OF_FLOW = 1;

	static final short END_OF_PATH = 2;

	static final short BEG_OF_WIDE = 3;

	static final short END_OF_WIDE = 4;

	SavedState sast;

	private static final short BA_NEWPA = 0;

	private static final short BA_BOBA = 1;

	private static final short BA_EOBA = 2;

	private class NotATopLevelWide extends Exception {
		NotATopLevelWide(String string) {
			super(string);
		}
	}

	final class SavedState {
		private final List.Mark strmmark = strm.mark();

		private final Path path = (Path) MTF.this.path.clone();

		private final Path fnpath = (Path) MTF.this.fnpath.clone();

		private final Ptr ptr = MTF.this.ptr.ln();

		private final List topfloats = (List) bkm.topfloats.clone();

		private final List footnotes = (List) bkm.footnotes.clone();

		private final List bodynotes = (List) bkm.bodynotes.clone();

		private final List strmtab = (List) bkm.page.strmtab.clone();

		private final Markers markers = (bkm.page.markers.isEmpty() ? new Markers()
				: (Markers) bkm.page.markers.clone());

		private final int[] a = (int[]) MTF.this.a.clone();

		private final Edge start_edge = MTF.this.start_edge;

		private final Edge end_edge = MTF.this.end_edge;

		private final int Y = MTF.this.Y;

		private final int dW0 = MTF.this.dW0;

		private final int dW = MTF.this.dW;

		private final int dWs = MTF.this.dWs;

		private final int dWe = MTF.this.dWe;

		private final dBPB[] dbreg = new dBPB[breg.size()];

		SavedState() {
			int i = 0;
			Enumeration enumeration = breg.elements();
			while (enumeration.hasMoreElements()) {
				BPB.Block block = (BPB.Block) enumeration.nextElement();
				if (block instanceof BPB.Frame)
					dbreg[i++] = new dfBPB(block.bno, (BPB.Frame) block);
				else
					dbreg[i++] = new dBPB(block.bno, block);
			}
		}

		void restore() {
			strm.cut(strmmark);
			MTF.this.path.clear().append(path);
			MTF.this.fnpath.clear().append(fnpath);
			MTF.this.ptr.restore(ptr);
			bkm.topfloats.clear().append(topfloats);
			bkm.footnotes.clear().append(footnotes);
			bkm.bodynotes.clear().append(bodynotes);
			footnotes_pending = false;
			bkm.page.strmtab.clear().append(strmtab);
			bkm.page.markers.clear();
			Enumeration enumeration = markers.keys();
			while (enumeration.hasMoreElements()) {
				Object object = enumeration.nextElement();
				bkm.page.markers.put(object, markers.get(object));
			}
			MTF.this.a = a;
			MTF.this.start_edge = start_edge;
			MTF.this.end_edge = end_edge;
			breg = new Hashtable();
			for (int i = 0; i != dbreg.length; i++) {
				dBPB var_dBPB = dbreg[i];
				breg.put(var_dBPB.bno, var_dBPB.BPB());
			}
			MTF.this.Y = Y;
			MTF.this.dW0 = dW0;
			MTF.this.dW = dW;
			MTF.this.dWs = dWs;
			MTF.this.dWe = dWe;
			cur = (TD) MTF.this.ptr.tab.car();
			phrase = null;
		}
	}

	private static final class dfBPB extends dBPB {
		final int y0s;

		final int y0e;

		final Edge start_edge;

		final Edge end_edge;

		dfBPB(Integer integer, BPB.Frame frame) {
			super(integer, (BPB.Block) frame);
			y0s = frame.y0s;
			y0e = frame.y0e;
			start_edge = frame.start_edge;
			end_edge = frame.end_edge;
		}

		BPB.Block BPB() {
			BPB.Frame frame = (BPB.Frame) super.BPB();
			frame.y0s = y0s;
			frame.y0e = y0e;
			frame.start_edge = start_edge;
			frame.end_edge = end_edge;
			return frame;
		}
	}

	private static class dBPB {
		final Integer bno;

		final BPB.Block bpb;

		final int r_1;

		final int r1;

		final boolean draw;

		dBPB(Integer integer, BPB.Block block) {
			bno = integer;
			bpb = block;
			r_1 = block.r[1];
			r1 = block.r1;
			draw = block.draw;
		}

		BPB.Block BPB() {
			bpb.r[1] = r_1;
			bpb.r1 = r1;
			bpb.draw = draw;
			return bpb;
		}
	}

	MTF(List list, Path path, Path path_0_, Ptr ptr, BkMaker bkmaker,
			Session session) {
		strm = list;
		this.path = path;
		fnpath = path_0_;
		this.ptr = ptr;
		bkm = bkmaker;
		this.session = session;
		Enumeration enumeration = ptr.tab.elements();
		while (enumeration.hasMoreElements()) {
			cur = (TD) enumeration.nextElement();
			if (cur.waiting == 3)
				cur.waiting = (short) 2;
		}
		if (!path.isEmpty() && !ptr.tab.isEmpty()) {
			pathArea();
			cur = (TD) ptr.tab.car();
		}
	}

	private void pathArea() {
		a = (int[]) path.shift();
		Y = a[3];
		breg = new Hashtable();
		lindtab = new Hashtable();
		Enumeration enumeration = ptr.tab.elements();
		while (enumeration.hasMoreElements()) {
			cur = (TD) enumeration.nextElement();
			if (cur.waiting == 2)
				cur.waiting = (short) 0;
			if (!cur.filled && cur.empty < 3)
				cur.empty = (short) 3;
			if (cur.container_edge)
				cur.daccu = new Item.Space.Display();
			cur.frstack.clear();
			bAreas();
			if (cur.y < Y)
				Y = cur.y;
		}
		sast = new SavedState();
	}

	short fmt() {
		if (ptr.tab.isEmpty()) {
			action = (short) 2;
			retcode = (short) 1;
		} else if (a == null) {
			action = (short) 2;
			retcode = (short) 2;
		} else {
			if (!path.isEmpty()) {
				int[] is = (int[]) path.car();
				if (a[1] == is[3] && a[0] == is[0] && a[2] == is[2]) {
					a[1] = is[1];
					path.shift();
				}
			}
			Enumeration enumeration = ptr.tab.elements();
			while (enumeration.hasMoreElements()) {
				cur = (TD) enumeration.nextElement();
				cur.y0 = a[1];
				Enumeration enumeration_1_ = cur.frstack.elements();
				while (enumeration_1_.hasMoreElements()) {
					Item.Frame frame = (Item.Frame) enumeration_1_
							.nextElement();
					cur.y0 += frame.after_indent;
				}
				cur.y0 += cur.b.after_indent;
			}
			cur = (TD) ptr.tab.car();
			action = cur.waiting == 0 ? (short) 0 : (short) 1;
		}
		while_21_: for (;;) {
			switch (action) {
			case 0:
				break;
			case 1: {
				if (cur.y < Y)
					Y = cur.y;
				boolean bool = cur.group == Group.nogroup;
				boolean bool_2_ = false;
				boolean bool_3_ = false;
				while_20_: for (;;) {
					TD td = cur;
					TD td_4_ = null;
					int i = -2147483648;
					for (;;) {
						ptr.nxt();
						cur = (TD) ptr.tab.car();
						if (bool || td.group == cur.group) {
							switch (cur.waiting) {
							case 0:
								if (cur.flow.car() instanceof Item.Eofl)
									action = (short) 0;
								else {
									if (cur.y > i) {
										td_4_ = cur;
										i = cur.y;
									}
									break;
								}
								continue while_21_;
							case 2:
								bool_2_ = true;
								break;
							case 3:
							case 4:
							case 5:
							case 6:
								bool_3_ = true;
								break;
							case 1:
								break;
							default:
								throw new InternalException(
										"illegal wait condition");
							}
							if (cur == td) {
								if (td_4_ != null) {
									do {
										ptr.nxt();
										cur = (TD) ptr.tab.car();
									} while (td_4_ != cur);
									action = (short) 0;
								} else {
									if (!bool) {
										cur = td;
										waitForPath();
										bool = true;
									} else {
										if (bool_2_ || bool_3_) {
											if (sast != null && ptr.empties()) {
												sast.restore();
												sast = null;
												forceWait(bool_2_ ? (short) 2
														: (short) 3);
												action = (short) 1;
											} else {
												Enumeration enumeration = ptr.tab
														.elements();
												while (enumeration
														.hasMoreElements()) {
													cur = ((TD) enumeration
															.nextElement());
													bpbOuts();
												}
												if (bool_2_ && !path.isEmpty()) {
													pathArea();
													action = (short) 1;
												} else {
													action = (short) 2;
													retcode = (short) 2;
												}
												cur = (TD) ptr.tab.car();
											}
										} else {
											String string = "";
											Enumeration enumeration = ptr.syn
													.keys();
											while (enumeration
													.hasMoreElements())
												string += " "
														+ ((String) (enumeration
																.nextElement()));
											throw new InternalException(
													"dead lock:" + string);
										}
										break while_20_;
									}
									break;
								}
								break while_20_;
							}
						}
					}
				}
				continue;
			}
			case 2:
				if (a != null) {
					int[] is = (int[]) a.clone();
					is[3] = a[1] = cur != null && cur.y < Y ? cur.y : Y;
					path.cons(is);
				}
				break while_21_;
			default:
				throw new InternalException("unknown action");
			}
			action = (short) -1;
			Item item = (Item) cur.flow.car();
			if (item.isSkipped()) {
				cur.flow = (Flow) cur.flow.cdr();
				action = (short) 0;
			} else {
				if (!item.keepsFilled())
					cur.filled = false;
				if (item.breaksPhrase() && phrase != null) {
					boolean bool = false;
					ptr.lb.size(phrase, true);
					boolean bool_5_ = (!phrase.empty && (phrase.width > phrase.max_width
							- cur.s.end_indent + 50));
					boolean bool_6_ = !runoff
							&& (cur.y - phrase.height_before
									- phrase.height_after + 50) < cur.y0;
					if (bool_5_ && cur.b.wrap || bool_6_) {
						if (bool_6_) {
							phrase.restore();
							bool = true;
							SavedState savedstate = sast;
							short i = cur.empty != 0 ? cur.empty : (short) 2;
							flushLine(false, false);
							cur.empty = i;
							sast = savedstate;
							waitForPath();
						} else if (ptr.lb.empty) {
							if (cur.start_edge.isEmpty()
									&& cur.end_edge.isEmpty()) {
								ptr.lb.add(phrase);
								action = (short) 0;
							} else {
								Item.Block block = (cur.b instanceof Item.Frame ? cur.b
										: !cur.frstack.isEmpty() ? (Item.Block) cur.frstack
												.car()
												: null);
								int i;
								int i_7_;
								if (block != null) {
									BPB.Frame frame = (BPB.Frame) block.bpb;
									i = frame.y0s;
									i_7_ = frame.y0e;
								} else {
									i = start_edge.y0();
									i_7_ = end_edge.y0();
								}
								if (cur.start_edge.isEmpty())
									i = 2147483647;
								if (cur.end_edge.isEmpty())
									i_7_ = 2147483647;
								int i_8_ = (i > cur.y ? i_7_ : i_7_ > cur.y ? i
										: i > i_7_ ? i : i_7_);
								if (i_8_ <= cur.y) {
									phrase.restore();
									bool = true;
									flushLine(false, false);
									cur.y = i_8_ - 1;
									TAME();
								} else {
									ptr.lb.add(phrase);
									action = (short) 0;
								}
							}
						} else if ((ptr.lb.hyphenated && !phrase.hyphenated || ((ptr.lb.hyphenated || !phrase.hyphenated) && (phrase.width
								- phrase.max_width < (phrase.max_width - ptr.lb.width))))
								&& (((phrase.width + cur.s.end_indent - phrase.max_width) * session.config.LTF) < phrase.max_width)) {
							ptr.lb.add(phrase);
							action = (short) 0;
						} else {
							phrase.restore();
							bool = true;
							flushLine(false, false);
							TAME();
						}
					} else {
						ptr.lb.add(phrase);
						action = (short) 0;
					}
					phrase = null;
					if (bool || action != 0)
						continue;
				}
				if (item.breaksLine()) {
					flushLine(true, item.breaksBlock());
					item = (Item) cur.flow.car();
				}
				if (item.breaksDisplaySpace())
					cur.daccu = new Item.Space.Display();
				item.pass(this);
			}
		}
		return retcode;
	}

	private void TAME() {
		action = (cur.group == Group.nogroup && cur.empty == 0
				&& ptr.tab.length() > 1 && cur.y <= Y) ? (short) 1 : (short) 0;
	}

	private int tableHeader(Attr attr, int[] is, Flow flow) {
		int i = 0;
		if (flow.isPair()) {
			if (attr == Attr.rl_tb) {
				int i_9_ = -is[2];
				is[2] = -is[0];
				is[0] = i_9_;
			}
			i = is[3] - is[1];
			int i_10_ = bkm.footnotes.length();
			MTF mtf_11_ = bkm.auxMTF(strm, new Path(is), fnpath, new Ptr(
					new TD(flow, session)));
			short i_12_ = bkm.mkStream(mtf_11_);
			if (i_10_ != bkm.footnotes.length() && !bkm.bodywide)
				footnotes_pending = true;
			if (i_12_ == 1 && mtf_11_.path.isPair()) {
				is = (int[]) mtf_11_.path.car();
				i -= is[3] - is[1];
			}
		}
		return i;
	}

	private void bAreas() {
		bArea((short) 0);
	}

	private void ibArea() {
		bArea((short) 1);
	}

	private void obArea() {
		bArea((short) 2);
	}

	private void bArea(short i) {
		int i_13_ = a[0];
		int i_14_ = a[2];
		int i_15_ = a[3];
		int i_16_ = a[1];
		Attr attr = Attr.lr_tb;
		int i_17_ = 0;
		int i_18_ = 0;
		dW0 = 0;
		cur.start_edge = start_edge;
		cur.end_edge = end_edge;
		if (!cur.bstack.isEmpty()) {
			Enumeration enumeration = cur.bstack.reverse().elements();
			Item.Block block = (Item.Block) enumeration.nextElement();
			BPB.Frame frame = null;
			Item.ListBlock listblock = null;
			boolean bool = false;
			for (;;) {
				Item.Block block_19_ = ((Item.Block) (enumeration
						.hasMoreElements() ? (Object) enumeration.nextElement()
						: cur.b));
				boolean bool_20_ = i == 0 || i == 1 && block_19_ == cur.b;
				if (block_19_.writing_mode != attr) {
					int i_21_ = i_13_;
					Edge edge = cur.start_edge;
					i_13_ = -i_14_;
					cur.start_edge = cur.end_edge;
					i_14_ = -i_21_;
					cur.end_edge = edge;
					attr = block_19_.writing_mode;
				}
				int i_22_;
				if (bool_20_) {
					i_22_ = cur.y + block_19_.before_indent;
					if (!(block instanceof Item.Frame))
						i_22_ -= block.before_indent;
				} else
					i_22_ = block_19_.bpb.r[3];
				int i_23_ = i_13_;
				int i_24_ = i_14_;
				int i_25_ = i_17_;
				int i_26_ = i_18_;
				int i_27_ = dWs;
				int i_28_ = dWe;
				Edge edge = cur.start_edge;
				Edge edge_29_ = cur.end_edge;
				for (;;) {
					if (bool_20_)
						block_19_.base = -2147483648;
					block_19_.dimensions(block, listblock, lindtab, i_14_
							- i_13_);
					if (!cur.start_edge.isEmpty()) {
						switch (block_19_.displace) {
						case 0:
							cur.start_edge = Edge.empty;
							break;
						case 1:
						case 2:
							if (block_19_ instanceof Item.Frame)
								cur.start_edge = cur.start_edge
										.dip(block_19_.start_indent);
							break;
						case 3: {
							int i_30_ = cur.start_edge.depth(i_16_, i_22_);
							if (i_30_ > block_19_.start_indent) {
								int i_31_ = i_30_ - block_19_.start_indent;
								i_17_ -= i_31_;
								if (i_17_ > dWs)
									dWs = i_17_;
								if (block_19_ instanceof Item.Frame) {
									i_13_ += i_31_;
									block_19_.dimensions(block, listblock,
											lindtab, i_14_ - i_13_);
								} else {
									block_19_.start_indent += i_31_;
									block_19_.abs_margin[2] += i_31_;
								}
							}
							if (block_19_ instanceof Item.Frame)
								cur.start_edge = Edge.empty;
							break;
						}
						default:
							throw new InternalException(
									"illegal displace mode "
											+ block_19_.displace);
						}
					}
					if (!cur.end_edge.isEmpty()) {
						switch (block_19_.displace) {
						case 0:
							cur.end_edge = Edge.empty;
							break;
						case 1:
						case 2:
							if (block_19_ instanceof Item.Frame)
								cur.end_edge = cur.end_edge
										.dip(block_19_.end_indent);
							break;
						case 3: {
							int i_32_ = cur.end_edge.depth(i_16_, i_22_);
							if (i_32_ > block_19_.end_indent) {
								int i_33_ = i_32_ - block_19_.end_indent;
								i_18_ -= i_33_;
								if (i_18_ > dWe)
									dWe = i_18_;
								if (block_19_ instanceof Item.Frame) {
									i_14_ -= i_33_;
									block_19_.dimensions(block, listblock,
											lindtab, i_14_ - i_13_);
								} else {
									block_19_.end_indent += i_33_;
									block_19_.abs_margin[3] += i_33_;
								}
							}
							if (block_19_ instanceof Item.Frame)
								cur.end_edge = Edge.empty;
							break;
						}
						default:
							throw new InternalException(
									"illegal displace mode "
											+ block_19_.displace);
						}
					}
					if (!bool_20_
							|| block_19_.displace != 3
							|| edge.isEmpty()
							&& edge_29_.isEmpty()
							|| (block_19_.base > 50 && (block_19_.content_width == -2147483648 || (block_19_.base
									- block_19_.content_width >= -50))))
						break;
					int i_34_;
					int i_35_;
					if (frame != null) {
						i_34_ = frame.y0s;
						i_35_ = frame.y0e;
					} else {
						i_34_ = start_edge.y0();
						i_35_ = end_edge.y0();
					}
					if (edge.isEmpty())
						i_34_ = 2147483647;
					if (edge_29_.isEmpty())
						i_35_ = 2147483647;
					int i_36_ = (i_34_ > i_22_ ? i_35_ : i_35_ > i_22_ ? i_34_
							: i_34_ > i_35_ ? i_34_ : i_35_);
					if (i_36_ > i_22_)
						break;
					i_22_ = i_36_ - 1;
					cur.y = i_22_ - block_19_.before_indent;
					if (!(block instanceof Item.Frame))
						cur.y += block.before_indent;
					i_13_ = i_23_;
					i_14_ = i_24_;
					i_17_ = i_25_;
					i_18_ = i_26_;
					dWs = i_27_;
					dWe = i_28_;
					cur.start_edge = edge;
					cur.end_edge = edge_29_;
				}
				if (block_19_ instanceof Item.Table) {
					TableContext tablecontext = ((Item.Table) block_19_).context;
					i_24_ = block_19_.content_width;
					if (i_24_ == -2147483648)
						i_24_ = (i_14_ - i_13_ - block_19_.start_indent - block_19_.end_indent);
					tablecontext.pin(i_24_, false);
					block_19_.gap += i_24_ - tablecontext.table_width;
					block_19_.content_width = tablecontext.table_width;
					block_19_.end_indent = (i_14_ - i_13_
							- block_19_.start_indent - block_19_.content_width);
				}
				if (block_19_.content_width != -2147483648) {
					dW0 -= block_19_.gap;
					if (dW0 > dW)
						dW = dW0;
				}
				if (bool_20_) {
					if (i == 0) {
						if (breg.containsKey(block_19_.bno)) {
							block_19_.bpb = (BPB.Block) breg.get(block_19_.bno);
							if (block_19_ instanceof Item.Frame) {
								cur.frstack.push(block_19_);
								frame = (BPB.Frame) block_19_.bpb;
								cur.start_edge = frame.start_edge;
								cur.end_edge = frame.end_edge;
							}
							strm.append(block_19_.bpb);
							if (block_19_ instanceof Item.Table
									&& !block_19_.get(
											Attn.$table_omit_header_at_break)
											.bool()
									&& ((BPB.Table) block_19_.bpb).dy != 0) {
								i_15_ -= ((BPB.Table) block_19_.bpb).dy;
								bool = true;
							}
						} else {
							newBpb(i_15_, i_13_, i_14_, block_19_, block.bpb,
									false);
							if (!bool || session.config.DISCARD_AFTER_HEADER) {
								if ((block_19_.bpb
										.get(Attn.$border_before_width_conditionality)) == Attr.discard) {
									block_19_.bpb.put(
											Attn.$border_before_width_length,
											Attr.zerolength);
									block_19_.bpb
											.put(
													Attn.$border_before_width_collapsed,
													Attr.zerolength);
								}
								if ((block_19_.bpb
										.get(Attn.$padding_before_conditionality)) == Attr.discard)
									block_19_.bpb.put(
											Attn.$padding_before_length,
											Attr.zerolength);
							}
							if (block_19_ instanceof Item.Frame) {
								cur.frstack.push(block_19_);
								frame = (BPB.Frame) block_19_.bpb;
								frame.start_edge = cur.start_edge;
								frame.end_edge = cur.end_edge;
							}
							strm.append(block_19_.bpb);
							if (block_19_ instanceof Item.Table
									&& !block_19_.get(
											Attn.$table_omit_header_at_break)
											.bool()
									&& ((((BPB.Table) block_19_.bpb).dy = (tableHeader(
											attr,
											(new int[] {
													i_13_
															+ block_19_.start_indent,
													i_16_
															+ block_19_.after_indent,
													i_14_
															- block_19_.end_indent,
													i_15_
															- block_19_.before_indent }),
											((Item.Table) block_19_).header))) != 0)) {
								i_15_ -= ((BPB.Table) block_19_.bpb).dy;
								bool = true;
							}
						}
					} else if (i == 1) {
						newBpb(cur.y + block_19_.before_indent, i_13_, i_14_,
								block_19_, block.bpb, true);
						if (block_19_ instanceof Item.Frame) {
							frame = (BPB.Frame) block_19_.bpb;
							frame.start_edge = cur.start_edge;
							frame.end_edge = cur.end_edge;
						}
						strm.append(block_19_.bpb);
						if (block_19_ instanceof Item.Table
								&& !block_19_.get(
										Attn.$table_omit_initial_header).bool())
							cur.y -= (tableHeader(attr, (new int[] {
									i_13_ + block_19_.start_indent,
									i_16_ + block_19_.after_indent,
									i_14_ - block_19_.end_indent, cur.y }),
									((Item.Table) block_19_).header));
					}
				} else if (block_19_ instanceof Item.Frame) {
					frame = (BPB.Frame) block_19_.bpb;
					cur.start_edge = frame.start_edge;
					cur.end_edge = frame.end_edge;
				}
				if (block_19_ == cur.b)
					break;
				block = block_19_;
				if (block instanceof Item.Frame) {
					i_13_ += block.start_indent;
					i_14_ -= block.end_indent;
					i_15_ -= block.before_indent;
					i_16_ += block.after_indent;
				}
				if (block instanceof Item.ListBlock)
					listblock = (Item.ListBlock) block;
			}
		}
		if (i == 0)
			cur.y = i_15_ - cur.b.before_indent;
		cur.y0 = i_16_ + cur.b.after_indent;
		cur.x = i_13_ + cur.b.start_indent;
		cur.width = i_14_ - cur.b.end_indent - cur.x;
		cur.lineno = 0;
		if (!cur.sstack.isEmpty()) {
			Enumeration enumeration = cur.sstack.reverse().elements();
			Item.Span span = (Item.Span) enumeration.nextElement();
			for (;;) {
				Item.Span span_37_ = ((Item.Span) (enumeration
						.hasMoreElements() ? (Object) enumeration.nextElement()
						: cur.s));
				span_37_.dimensions(span, cur.width);
				if (span_37_ == cur.s)
					break;
				span = span_37_;
			}
			((Item.Span) cur.sstack.last()).tt = cur.b.tt;
		} else
			cur.s.tt = cur.b.tt;
	}

	private void newBpb(int i, int i_38_, int i_39_, Item.Block block,
			BPB.Block block_40_, boolean bool) {
		int[] is = {
				(i_38_ + block.start_indent
						- block.get(Attn.$padding_start_length).length() - block
						.get(Attn.$border_start_width_length).length()),
				2147483647,
				(i_39_ - block.end_indent
						+ block.get(Attn.$padding_end_length).length() + block
						.get(Attn.$border_end_width_length).length()),
				(i - block.before_indent
						+ block.get(Attn.$padding_before_length).length() + block
						.get(Attn.$border_before_width_length).length()) };
		block.bpb = (block instanceof Item.Frame ? (BPB.Block) (block instanceof Item.Table ? (BPB.Frame) new BPB.Table(
				(Item.Table) block, is, session)
				: new BPB.Frame((Item.Frame) block, is, session))
				: block instanceof Item.ListItem ? (BPB.Block) new BPB.ListItem(
						(Item.ListItem) block, is, session)
						: new BPB.Block(block, is, session));
		breg.put(block.bno, block.bpb);
		block.bpb.draw = !block.collapse;
		block.bpb.first = bool;
		block.bpb.par = block_40_;
	}

	private void closeBpb(int i, Item.Block block) {
		int i_41_ = (i - block.get(Attn.$padding_after_length).length() - block
				.get(Attn.$border_after_width_length).length());
		if (i_41_ < block.bpb.r1) {
			block.bpb.r1 = i_41_;
			if (i_41_ < block.bpb.r[1]) {
				block.bpb.r[1] = i_41_;
				if (block.level != null) {
					Enumeration enumeration = block.level.bnos.elements();
					while (enumeration.hasMoreElements()) {
						Integer integer = (Integer) enumeration.nextElement();
						if (breg.containsKey(integer)) {
							BPB.Block block_42_ = (BPB.Block) breg.get(integer);
							if (i_41_ < block_42_.r[1])
								block_42_.r[1] = i_41_;
						}
					}
				}
			}
		}
	}

	private void relastore(int i, Item.Block block) {
		BPB.Block block_43_ = block.bpb;
		BPB.Block block_44_ = block_43_;
		block_44_.baseline = i;
		do {
			if (block_43_ instanceof BPB.ListItem) {
				BPB.ListItem listitem = (BPB.ListItem) block_43_;
				if (listitem.alt == null)
					listitem.alt = block_44_;
				else
					listitem.desc = block_44_;
				block_44_ = listitem;
			} else
				block_43_.desc = block_44_;
			block_43_ = block_43_.par;
			if (block_43_ == null)
				break;
		} while (block_43_.desc == null);
	}

	private int relashift(int i, Item.ListItem listitem) {
		BPB.ListItem listitem_45_ = (BPB.ListItem) listitem.bpb;
		if (listitem_45_.desc != null) {
			int i_46_ = listitem_45_.desc.baseline - listitem_45_.alt.baseline;
			if (listitem_45_.get(Attn.$relative_align) == Attr.baseline) {
				BPB.Block block;
				BPB.Block block_47_;
				if (i_46_ > 0) {
					block = listitem_45_.desc;
					block_47_ = listitem_45_.alt;
				} else {
					i_46_ = -i_46_;
					block = listitem_45_.alt;
					block_47_ = listitem_45_.desc;
				}
				listitem_45_.baseline = block_47_.baseline;
				for (/**/; block.par != listitem_45_; block = block.par) {
					/* empty */
				}
				block.shifty -= i_46_;
				if (block.r[1] - i_46_ < i)
					i = block.r[1] - i_46_;
				int i_48_ = a[1] + listitem.after_indent;
				if (i < i_48_) {
					int i_49_ = i_48_ - i;
					i = i_48_;
					block.shifty += i_49_;
					block_47_.shifty += i_49_;
					listitem_45_.baseline += i_49_;
				}
			} else
				listitem_45_.baseline = (i_46_ > 0 ? listitem_45_.desc.baseline
						: listitem_45_.alt.baseline);
		} else if (listitem_45_.alt != null)
			listitem_45_.baseline = listitem_45_.alt.baseline;
		listitem_45_.desc = listitem_45_.alt = null;
		return i;
	}

	private void bpbOut() {
		if (cur.b instanceof Item.Frame) {
			BPB.Frame frame = (BPB.Frame) cur.b.bpb;
			if (cur.y > frame.y0s)
				cur.y = frame.y0s;
			if (cur.y > frame.y0e)
				cur.y = frame.y0e;
		}
		if (cur.b instanceof Item.ListItem)
			cur.y = relashift(cur.y, (Item.ListItem) cur.b);
		closeBpb(cur.y, cur.b);
		if (cur.b.bpb.first)
			cur.b.bpb.draw = true;
		if (cur.bstack.length() > 1) {
			Item.Block block = (Item.Block) cur.bstack.car();
			if (cur.b.bpb.draw)
				block.bpb.draw = true;
		}
	}

	private void bpbOuts() {
		if (!cur.bstack.isEmpty()) {
			Enumeration enumeration = cur.bstack.elements();
			Item.Block block = cur.b;
			for (;;) {
				if (block instanceof Item.Frame) {
					BPB.Frame frame = (BPB.Frame) block.bpb;
					if (cur.y > frame.y0s)
						cur.y = frame.y0s;
					if (cur.y > frame.y0e)
						cur.y = frame.y0e;
				}
				if (block instanceof Item.ListItem)
					cur.y = relashift(cur.y, (Item.ListItem) block);
				closeBpb(cur.y, block);
				cur.y -= block.after_indent;
				if (block.bpb.get(Attn.$padding_after_conditionality) == Attr.discard)
					block.bpb.put(Attn.$padding_after_length, Attr.zerolength);
				if (block.bpb.get(Attn.$border_after_width_conditionality) == Attr.discard) {
					block.bpb.put(Attn.$border_after_width_length,
							Attr.zerolength);
					block.bpb.put(Attn.$border_after_width_collapsed,
							Attr.zerolength);
				}
				boolean bool = block.bpb.draw;
				block = (Item.Block) enumeration.nextElement();
				if (!enumeration.hasMoreElements())
					break;
				if (!(block instanceof Item.Frame))
					cur.y += block.after_indent;
				if (bool)
					block.bpb.draw = true;
			}
		}
	}

	private void flushLine(boolean bool, boolean bool_50_) {
		if (ptr.lb != null && !ptr.lb.empty) {
			if (ptr.lb.words.isPair()) {
				List list = new List();
				Enumeration enumeration = ptr.lb.words.elements();
				while (enumeration.hasMoreElements()) {
					Item item = (Item) enumeration.nextElement();
					if ((cur.b.wst & 0x2) == 2
							&& ((item instanceof Item.Text && (((Item.Text) item).nwsp == ((Item.Text) item).chars.length)) || (item instanceof Item.Leader
									&& ((Item.Leader) item).leader_pattern == 0 && ((Item.Leader) item).len_max == 0)))
						ptr.lb.width -= ((Item.Inline) item).width;
					else if (item instanceof Item.Space.Inline
							&& !((Item.Space.Inline) item).retain)
						ptr.lb.width -= ((Item.Space.Inline) item).optimum;
					else {
						list.append(item);
						if (!(item instanceof Item.Span)
								&& !(item instanceof Item.Ends)
								&& item instanceof Item.Inline)
							break;
					}
				}
				while (enumeration.hasMoreElements())
					list.append(enumeration.nextElement());
				ptr.lb.words = list;
			}
			cur.y -= ptr.lb.height_before;
			strm.append(new Line(strm, ptr.lb, bool || bool_50_, session));
			if (cur.b.bpb.first_line) {
				relastore(cur.y, cur.b);
				cur.b.bpb.first_line = false;
			}
			cur.y -= ptr.lb.height_after;
			cur.container_edge = false;
			cur.lineno++;
			while_22_: do {
				int i;
				for (;;) {
					if (bool_50_) {
						cur.empty = (short) 0;
						break while_22_;
					}
					i = countLines(cur.b.widows);
					if (i != 0)
						break;
					bool_50_ = true;
				}
				if (i >= cur.b.widows) {
					if (cur.lineno >= cur.b.orphans)
						cur.empty = (short) 0;
					else
						cur.empty = (short) 2;
				} else
					cur.empty = (short) 2;
			} while (false);
			if (!cur.bstack.isEmpty())
				cur.b.bpb.draw = true;
			if (!bool_50_ && !ptr.empties()) {
				if (footnotes_pending) {
					cur.flow.cons(Item.footnotesPending);
					footnotes_pending = false;
				} else
					sast = new SavedState();
			}
			int i = ptr.lb.width - ptr.lb.max_width;
			if (i > 0)
				i = 0;
			i += dW0;
			if (i > dW)
				dW = i;
		}
		ptr.lb = null;
	}

	private int countLines(int i) {
		TD td = cur.cp();
		int i_51_ = 0;
		LineCounter.Phrase phrase = null;
		LineCounter linecounter = null;
		while (i_51_ != i && !td.flow.isEmpty()) {
			Item item = (Item) td.flow.car();
			if (item.isSkipped())
				td.flow = (Flow) td.flow.cdr();
			else if (item.breaksPhrase() && phrase != null) {
				if (linecounter == null)
					linecounter = new LineCounter(td, td.b.bpb.first_line
							&& i_51_ == 0);
				linecounter.size(phrase);
				boolean bool = (((td.b.wst & 0x4) != 4 || !phrase.empty) && phrase.width > (phrase.max_width
						- td.s.end_indent + 50));
				if (bool && td.b.wrap) {
					if (linecounter.empty) {
						linecounter.add(phrase);
						action = (short) 0;
					} else if ((linecounter.hyphenated && !phrase.hyphenated || ((linecounter.hyphenated || !phrase.hyphenated) && (phrase.width
							- phrase.max_width < (phrase.max_width - linecounter.width))))
							&& ((phrase.width + td.s.end_indent - phrase.max_width)
									* session.config.LTF < phrase.max_width))
						linecounter.add(phrase);
					else {
						phrase.restore();
						linecounter = null;
					}
				} else
					linecounter.add(phrase);
				phrase = null;
			} else {
				if (item.breaksLine()) {
					linecounter = null;
					if (item.breaksBlock())
						break;
				}
				if (phrase == null)
					phrase = new LineCounter.Phrase(td);
				item.dimensions(td.width);
				phrase.words.append(item);
				if (phrase.empty
						&& ((item instanceof Item.Text && ((((Item.Text) item).nwsp != ((Item.Text) item).chars.length) || td.b.wst == 0))
								|| item instanceof Item.Leader || item instanceof Item.Image)) {
					phrase.empty = false;
					if (linecounter == null || linecounter.empty)
						i_51_++;
				}
				td.flow = (Flow) td.flow.cdr();
			}
		}
		return i_51_;
	}

	private void waitForPath() {
		if (cur.group != Group.nogroup)
			cur.group.sast.restore();
		cur.waiting = (short) 2;
		action = (short) 1;
	}

	void forceWait(short i) {
		if (ptr.lb != null) {
			ptr.lb.size(phrase, true);
			ptr.lb.add(phrase);
			phrase = null;
			Item item = (Item) cur.flow.car();
			if (item.breaksLine() || sast == null)
				flushLine(item.breaksLine(), item.breaksBlock());
			else {
				sast.restore();
				sast = null;
			}
		}
		Enumeration enumeration = ptr.tab.elements();
		while (enumeration.hasMoreElements()) {
			cur = (TD) enumeration.nextElement();
			if (cur.waiting == 0)
				cur.waiting = i;
		}
		cur = (TD) ptr.tab.car();
	}

	public void process(Item.Block block) {
		if (block.keep_together && cur.group == Group.nogroup
				&& (keep & 0x1) != 0)
			cur.group = new Group(block.bno, new SavedState());
		cur.flow = (Flow) cur.flow.cdr();
		cur.bstack.push(cur.b);
		if (cur.b instanceof Item.Frame)
			cur.frstack.push(cur.b);
		if (cur.b instanceof Item.ListItem || cur.b instanceof Item.Frame)
			cur.container_edge = true;
		Item.Block block_52_ = cur.b;
		cur.b = block;
		int i = (block.get(Attn.$margin_before).length_or_ratio(cur.width)
				+ block.get(Attn.$border_before_width_length).length() + block
				.get(Attn.$padding_before_length).length_or_ratio(cur.width));
		if (Util.ne0(i)) {
			cur.y -= i;
			cur.daccu = new Item.Space.Display();
			cur.container_edge = false;
		}
		ibArea();
		cur.b.bpb.first_line = true;
		if (cur.b.markers != null)
			bkm.page.markers.Starting(cur.b.markers);
		TAME();
	}

	public void process(Item.ListBlock listblock) {
		process((Item.Block) listblock);
	}

	public void process(Item.Frame frame) {
		process((Item.Block) frame);
	}

	public void process(Item.Table table) {
		process((Item.Frame) table);
	}

	public void process(Item.Endb endb) {
		if (cur.b.markers != null)
			bkm.page.markers.Ending(cur.b.markers);
		bpbOut();
		if (cur.b.bno == cur.group.bno)
			cur.group = Group.nogroup;
		cur.flow = (Flow) cur.flow.cdr();
		Item.Block block = cur.b;
		cur.b = (Item.Block) cur.bstack.pop();
		int i = block.after_indent;
		if (!(cur.b instanceof Item.Frame))
			i -= cur.b.after_indent;
		if (Util.ne0(i)) {
			cur.y -= i;
			cur.daccu = new Item.Space.Display();
			cur.container_edge = false;
		}
		if (cur.b instanceof Item.Frame)
			cur.frstack.pop();
		obArea();
		if (((Item) cur.flow.car()).ss())
			action = (short) 0;
		else {
			if (!ptr.empties()) {
				if (footnotes_pending) {
					cur.flow.cons(Item.footnotesPending);
					footnotes_pending = false;
					action = (short) 0;
					return;
				}
				sast = new SavedState();
			}
			TAME();
		}
	}

	public void process(Item.Endl endl) {
		cur.flow = (Flow) cur.flow.cdr();
		TAME();
	}

	private boolean firstLine(Attributed attributed) {
		if (cur.b.bpb.first_line && cur.b.proplist.contains((short) 24)) {
			AttList attlist = cur.b.proplist.get0((short) 24);
			if (attributed != a0) {
				a0 = attributed;
				fla = (Attributed) a0.clone();
				Enumeration enumeration = attlist.keys();
				while (enumeration.hasMoreElements()) {
					Attn attn = (Attn) enumeration.nextElement();
					fla.put(attn, attlist.get(attn));
				}
			}
			return true;
		}
		return false;
	}

	private void addToPhrase(Item item) {
		if (phrase == null) {
			phrase = new LineBuffer.Phrase(this);
			if (ptr.lb == null)
				ptr.lb = new LineBuffer(this);
		}
		phrase.words.append(item.cow());
		cur.flow = (Flow) cur.flow.cdr();
		action = (short) 0;
	}

	public void process(Item.Span span) {
		addToPhrase(span);
		if (span.markers != null)
			bkm.page.markers.Starting(span.markers);
	}

	public void process(Item.Anchor anchor) {
		process((Item.Span) anchor);
	}

	public void process(Item.Ends ends) {
		addToPhrase(ends);
		if (cur.s.markers != null)
			bkm.page.markers.Ending(cur.s.markers);
	}

	public void process(Item.Text text) {
		if (text.nwsp == text.chars.length && (cur.b.wst & 0x4) == 4
				&& (ptr.lb == null || ptr.lb.empty)
				&& (phrase == null || phrase.empty)) {
			cur.flow = (Flow) cur.flow.cdr();
			action = (short) 0;
		} else {
			if (firstLine(text.a)) {
				text = new Item.Text(text.chars, text.chars.length, fla,
						session, fla0, tt0);
				fla0 = text.a;
				tt0 = text.tt;
			}
			addToPhrase(text);
			if (text.nwsp != text.chars.length || cur.b.wst == 0)
				phrase.empty = false;
		}
	}

	public void process(Item.PageId pageid) {
		process((Item.Text) pageid);
	}

	public void process(Item.Leader leader) {
		if (firstLine(leader.a))
			leader = new Item.Leader(fla, session);
		leader.dimensions(cur.width);
		addToPhrase(leader);
		phrase.empty = false;
	}

	public void process(Item.Image image) {
		if (firstLine(image.a))
			image = new Item.Image(fla, session);
		image.dimensions(cur.width);
		addToPhrase(image);
		phrase.empty = false;
	}

	public void process(Item.Washer.Inline inline) {
		addToPhrase(inline);
	}

	public void process(Item.Space.Inline inline) {
		inline.dimensions(cur.width);
		if (!inline.retain && (ptr.lb == null || ptr.lb.empty)
				&& (phrase == null || phrase.empty)) {
			cur.flow = (Flow) cur.flow.cdr();
			action = (short) 0;
		} else
			addToPhrase(inline);
	}

	public void process(Item.Washer.Display display) {
		if (cur.y - display.thickness >= cur.y0) {
			cur.flow = (Flow) cur.flow.cdr();
			cur.y -= display.thickness;
			TAME();
		} else
			waitForPath();
	}

	public void process(Item.Space.Display display) {
		display.dimensions(cur.width);
		if ((Util.ne0(display.optimum) || display.precedence != 0)
				&& (display.retain || !cur.container_edge)) {
			Item.Space space = cur.daccu.merge(display);
			if (cur.y - space.minimum >= cur.y0) {
				cur.y -= space.optimum;
				if (cur.y < cur.y0)
					cur.y = cur.y0;
				cur.flow = (Flow) cur.flow.cdr();
				TAME();
			} else
				waitForPath();
		} else {
			cur.flow = (Flow) cur.flow.cdr();
			action = (short) 0;
		}
	}

	public void process(Item.Exception exception) {
		action = (short) 2;
		retcode = (short) 0;
	}

	public void process(Item.Fork fork) {
		cur.flow = (Flow) cur.flow.cdr();
		ptr.add(cur.dup(fork.adr));
		action = (short) 0;
	}

	public void process(Item.Jump jump) {
		cur.flow = jump.adr;
		action = (short) 0;
	}

	public void process(Item.Label label) {
		cur.flow = (Flow) cur.flow.cdr();
		action = (short) 0;
	}

	public void process(Item.Sync sync) {
		if (ptr.syn.containsKey(sync.id)) {
			cur.flow = (Flow) cur.flow.cdr();
			TD td = (TD) ptr.syn.get(sync.id);
			ptr.syn.remove(sync.id);
			td.waiting = (short) 0;
			td.flow = (Flow) td.flow.cdr();
			if (!(sync instanceof Item.Softsync)) {
				if (td.empty > cur.empty)
					cur.empty = td.empty;
				else if (cur.empty > td.empty)
					td.empty = cur.empty;
				if (cur.container_edge != td.container_edge)
					cur.container_edge = td.container_edge = false;
				if (td.y < cur.y)
					cur.y = td.y;
				else
					td.y = cur.y;
			}
		} else {
			ptr.syn.put(sync.id, cur);
			cur.waiting = (short) 1;
		}
		action = (short) 1;
	}

	public void process(Item.Softsync softsync) {
		process((Item.Sync) softsync);
	}

	public void process(Item.Empty empty) {
		if (!ptr.empties()) {
			if (footnotes_pending) {
				cur.flow.cons(Item.footnotesPending);
				footnotes_pending = false;
				action = (short) 0;
				return;
			}
			sast = new SavedState();
		}
		if (cur.empty < 2)
			cur.empty = (short) 2;
		cur.flow = (Flow) cur.flow.cdr();
		action = (short) 0;
	}

	public void process(Item.Filled filled) {
		cur.flow = (Flow) cur.flow.cdr();
		if (cur.empty != 1) {
			cur.empty = (short) 0;
			cur.filled = true;
			cur.container_edge = false;
			strm.append(filled);
			if (((Item) cur.flow.car()).ss())
				action = (short) 0;
			else {
				if (!ptr.empties()) {
					if (footnotes_pending) {
						cur.flow.cons(Item.footnotesPending);
						footnotes_pending = false;
						action = (short) 0;
						return;
					}
					sast = new SavedState();
				}
				TAME();
			}
		} else
			action = (short) 0;
	}

	public void process(Item.Eofl eofl) {
		if (ptr.tab.length() == 1 && footnotes_pending) {
			cur.flow.cons(Item.footnotesPending);
			footnotes_pending = false;
			action = (short) 0;
		} else {
			if (!cur.bstack.isEmpty()) {
				Enumeration enumeration = cur.bstack.elements();
				Item.Block block = cur.b;
				for (;;) {
					closeBpb(cur.y, block);
					cur.y -= block.after_indent;
					boolean bool = block.bpb.draw;
					block = (Item.Block) enumeration.nextElement();
					if (!enumeration.hasMoreElements())
						break;
					if (!(block instanceof Item.Frame))
						cur.y += block.after_indent;
					if (bool)
						block.bpb.draw = true;
				}
			}
			if (cur.y < Y)
				Y = cur.y;
			ptr.del();
			if (ptr.tab.isEmpty()) {
				action = (short) 2;
				retcode = (short) 1;
			} else {
				cur = (TD) ptr.tab.car();
				action = (short) 1;
			}
		}
	}

	private void WiEw(Item item) {
		try {
			if (ptr.tab.length() != 1)
				throw new InternalException(
						"more than one thread on a column span boundary");
			if (cur.bstack.length() != 1)
				throw new NotATopLevelWide(
						"span='all' may only be specified on a child of fo:flow");
			if (cur.y < Y)
				Y = cur.y;
			action = (short) 2;
			retcode = item instanceof Item.Wide ? (short) 3 : (short) 4;
		} catch (NotATopLevelWide notatoplevelwide) {
			session.error(notatoplevelwide.getMessage());
			cur.flow = (Flow) cur.flow.cdr();
			action = (short) 0;
		}
	}

	public void process(Item.Wide wide) {
		WiEw(wide);
	}

	public void process(Item.Endw endw) {
		WiEw(endw);
	}

	public void process(Item.FootnotesAppended footnotesappended) {
		cur.flow = (Flow) cur.flow.cdr();
		if (((Item) cur.flow.car()).ss())
			action = (short) 0;
		else {
			if (!ptr.empties())
				sast = new SavedState();
			TAME();
		}
	}

	public void process(Item.Level level) {
		Enumeration enumeration = level.bnos.elements();
		while (enumeration.hasMoreElements()) {
			Integer integer = (Integer) enumeration.nextElement();
			if (breg.containsKey(integer)) {
				BPB.Block block = (BPB.Block) breg.get(integer);
				if (cur.y < block.r[1] - block.margin_after)
					block.r[1] = cur.y + block.margin_after;
			}
		}
		cur.container_edge = false;
		cur.flow = (Flow) cur.flow.cdr();
		action = (short) 0;
	}

	public void process(Item.Bookmark bookmark) {
		/* empty */
	}
}