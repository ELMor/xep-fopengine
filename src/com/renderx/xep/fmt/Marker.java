/*
 * Marker - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.fmt;

import com.renderx.xep.cmp.Flow;

class Marker implements Cloneable {
	private Flow[] flows = new Flow[6];

	private int lastset;

	Marker() {
		for (int i = 0; i != flows.length; i++)
			flows[i] = null;
		lastset = -1;
	}

	public Object clone() {
		try {
			Marker marker_0_ = (Marker) super.clone();
			marker_0_.flows = new Flow[flows.length];
			System.arraycopy(flows, 0, marker_0_.flows, 0, flows.length);
			return marker_0_;
		} catch (CloneNotSupportedException clonenotsupportedexception) {
			return null;
		}
	}

	final Flow put(int i, Flow flow) {
		lastset = i;
		return flows[i] = flow;
	}

	final Flow get(int i) {
		return flows[i];
	}

	final Flow putStarting(Flow flow) {
		if (flows[1] == null)
			return put(1, flow);
		if (lastset != 1 && lastset != 3)
			return put(3, flow);
		return null;
	}

	final Flow putEnding(Flow flow) {
		if (flows[2] == null && lastset == 2)
			return put(2, flow);
		if (lastset != 4)
			return put(4, flow);
		return null;
	}

	final void reconcile() {
		if (flows[0] == null)
			flows[0] = flows[1];
		else if (flows[1] == null)
			flows[1] = flows[0];
		if (flows[4] == null)
			flows[4] = flows[2];
		if (flows[5] == null)
			flows[5] = flows[4];
		else if (flows[4] == null)
			flows[4] = flows[5];
		if (flows[3] == null)
			flows[3] = flows[1];
		if (flows[2] == null)
			flows[2] = flows[4];
	}
}