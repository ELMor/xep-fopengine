/*
 * KeyTable - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.fmt;

import java.util.Enumeration;

import com.renderx.util.Hashtable;
import com.renderx.util.List;
import com.renderx.xep.cmp.Item;
import com.renderx.xep.lib.InternalException;
import com.renderx.xep.lib.Session;
import com.renderx.xep.pre.AttList;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;

class KeyTable {
	private final Session session;

	private final Range aguard = new Range();

	private final Range bguard = new Range();

	private Hashtable tab = new Hashtable();

	static class Range implements Cloneable {
		Item.Key key = null;

		Item.Endk endk = null;

		public Range copy() {
			try {
				return (Range) super.clone();
			} catch (CloneNotSupportedException clonenotsupportedexception) {
				throw new InternalException(clonenotsupportedexception
						.getMessage());
			}
		}
	}

	public KeyTable(Session session) {
		this.session = session;
		aguard.key = new Item.Key(AttList.empty, session);
		aguard.key.page = new BkMaker.Page(-2147483648);
		bguard.key = new Item.Key(AttList.empty, session);
		bguard.key.page = new BkMaker.Page(2147483647);
	}

	private Range getRange(Attr attr, Attr attr_0_) {
		Hashtable hashtable = (Hashtable) tab.get(attr);
		if (hashtable == null)
			tab.put(attr, hashtable = new Hashtable());
		Range range = (Range) hashtable.get(attr_0_);
		if (range == null)
			hashtable.put(attr_0_, range = new Range());
		return range;
	}

	void put(Item.Key key) {
		getRange(key.get(Attn.$key), key.id).key = key;
	}

	void put(Item.Endk endk) {
		getRange(endk.get(Attn.$key), endk.id).endk = endk;
	}

	List get(Attr attr, boolean bool) {
		List list = new List();
		Hashtable hashtable = (Hashtable) tab.get(attr);
		if (hashtable == null)
			session.error("no entries for index key '" + attr + "'");
		else {
			List list_1_ = new List();
			List list_2_ = new List();
			Range range = aguard;
			Range range_3_ = bguard;
			list_1_.cons(range);
			list_2_.cons(range_3_);
			Enumeration enumeration = hashtable.elements();
			while (enumeration.hasMoreElements()) {
				Range range_4_ = (Range) enumeration.nextElement();
				if (range_4_.endk == null) {
					range_4_.endk = new Item.Endk(range_4_.key.a,
							range_4_.key.id);
					range_4_.endk.page = range_4_.key.page;
					range_4_.endk.x = range_4_.key.x;
					range_4_.endk.y = range_4_.key.y;
					session.warning("unbalanced start key '" + attr
							+ "' on page " + range_4_.key.page.id);
				} else if (range_4_.key == null) {
					range_4_.key = new Item.Key(range_4_.endk.a, session);
					range_4_.key.id = range_4_.endk.id;
					range_4_.key.page = range_4_.endk.page;
					range_4_.key.x = range_4_.endk.x;
					range_4_.key.y = range_4_.endk.y;
					session.warning("unbalanced end key '" + attr
							+ "' on page " + range_4_.key.page.id);
				}
				for (;;) {
					if (range_4_.key.page.no > range_3_.key.page.no) {
						range = range_3_;
						list_2_ = list_2_.cdr();
						range_3_ = (Range) list_2_.car();
						list_1_.cons(range);
					} else {
						if (range_4_.key.page.no >= range.key.page.no)
							break;
						range_3_ = range;
						list_1_ = list_1_.cdr();
						range = (Range) list_1_.car();
						list_2_.cons(range_3_);
					}
				}
				range_3_ = range_4_;
				list_2_.cons(range_3_);
			}
			for (;;) {
				Range range_5_ = (Range) list_1_.car();
				list_1_ = list_1_.cdr();
				if (list_1_.isEmpty())
					break;
				list_2_.cons(range_5_);
			}
			Object object = null;
			if (list_2_.length() != 1) {
				Range range_6_ = ((Range) list_2_.car()).copy();
				list_2_ = list_2_.cdr();
				list.snoc(range_6_);
				while (list_2_.length() != 1) {
					Range range_7_ = ((Range) list_2_.car()).copy();
					list_2_ = list_2_.cdr();
					if (range_7_.key.page.no > range_6_.endk.page.no
							+ (bool ? 1 : 0)) {
						range_6_ = range_7_;
						list.snoc(range_6_);
					} else if (range_7_.endk.page.no > range_6_.endk.page.no)
						range_6_.endk = range_7_.endk;
				}
			}
		}
		return list;
	}
}