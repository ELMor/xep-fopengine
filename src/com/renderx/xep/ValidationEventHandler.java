/*
 * ValidationEventHandler - Decompiled by JODE Visit
 * http://jode.sourceforge.net/
 */
package com.renderx.xep;

public interface ValidationEventHandler {
	public void warning(String string, String string_0_, String string_1_,
			int i, int i_2_);

	public void error(String string, String string_3_, String string_4_, int i,
			int i_5_);
}