/* PSLibInternalError - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.gen.backends;

public class PSLibInternalError extends RuntimeException
{
    public PSLibInternalError() {
	/* empty */
    }
    
    public PSLibInternalError(Exception exception) {
	super(exception.toString());
    }
    
    public PSLibInternalError(String string) {
	super(string);
    }
}
