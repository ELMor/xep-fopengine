/* PDFLibInternalError - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.gen.backends;

public class PDFLibInternalError extends RuntimeException
{
    public PDFLibInternalError() {
	/* empty */
    }
    
    public PDFLibInternalError(Exception exception) {
	super(exception.toString());
    }
    
    public PDFLibInternalError(String string) {
	super(string);
    }
}
