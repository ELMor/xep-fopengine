/* EmbeddedImageFilter - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.gen.backends;
import com.renderx.util.Hashtable;
import com.renderx.xep.gen.H4base;
import com.renderx.xep.lib.Logger;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

public class EmbeddedImageFilter implements ContentHandler
{
    private static final String XEPURI = "http://www.renderx.com/XEP/xep";
    private final ContentHandler parent;
    private final Logger logger;
    private Hashtable imagetable = new Hashtable();
    
    public EmbeddedImageFilter(ContentHandler contenthandler, Logger logger) {
	parent = contenthandler;
	this.logger = logger;
    }
    
    public void startDocument() throws SAXException {
	logger.openState("generate");
	if (parent instanceof H4base)
	    logger.event("output-format", ((H4base) parent).outFormat());
	parent.startDocument();
    }
    
    public void endDocument() throws SAXException {
	parent.endDocument();
	logger.closeState("generate");
    }
    
    public void startElement(String string, String string_0_, String string_1_,
			     Attributes attributes) throws SAXException {
	if ("http://www.renderx.com/XEP/xep".equals(string)) {
	    if (string_0_.equals("page"))
		logger.event("page-number",
			     attributes.getValue("page-number"));
	    else if (string_0_.equals("image")) {
		String string_2_ = attributes.getValue("id");
		String string_3_ = attributes.getValue("id-ref");
		if (string_2_ != null)
		    imagetable.put(string_2_, attributes.getValue("src"));
		else if (string_3_ != null) {
		    String string_4_ = (String) imagetable.get(string_3_);
		    if (string_4_ == null)
			logger.error("Unresolved image reference: id-ref=\""
				     + string_3_ + "\"");
		    AttributesImpl attributesimpl
			= new AttributesImpl(attributes);
		    attributesimpl.addAttribute("", "src", "src", "CDATA",
						string_4_);
		    attributes = attributesimpl;
		}
	    }
	}
	parent.startElement("http://www.renderx.com/XEP/xep", string_0_,
			    string_1_, attributes);
    }
    
    public void characters(char[] cs, int i, int i_5_) throws SAXException {
	parent.characters(cs, i, i_5_);
    }
    
    public void endElement
	(String string, String string_6_, String string_7_)
	throws SAXException {
	parent.endElement(string, string_6_, string_7_);
    }
    
    public void startPrefixMapping(String string, String string_8_)
	throws SAXException {
	parent.startPrefixMapping(string, string_8_);
    }
    
    public void endPrefixMapping(String string) throws SAXException {
	parent.endPrefixMapping(string);
    }
    
    public void ignorableWhitespace(char[] cs, int i, int i_9_)
	throws SAXException {
	parent.ignorableWhitespace(cs, i, i_9_);
    }
    
    public void processingInstruction(String string, String string_10_)
	throws SAXException {
	parent.processingInstruction(string, string_10_);
    }
    
    public void skippedEntity(String string) throws SAXException {
	parent.skippedEntity(string);
    }
    
    public void setDocumentLocator(Locator locator) {
	parent.setDocumentLocator(locator);
    }
}
