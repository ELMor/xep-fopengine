/*
 * Attn - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre;

import com.renderx.util.Hashtable;
import com.renderx.xep.lib.InternalException;
import com.renderx.xep.pre.att.AbsoluteSide;
import com.renderx.xep.pre.att.AscenderDescender;
import com.renderx.xep.pre.att.Background;
import com.renderx.xep.pre.att.BackgroundPosition;
import com.renderx.xep.pre.att.Bool;
import com.renderx.xep.pre.att.Border;
import com.renderx.xep.pre.att.BorderSide;
import com.renderx.xep.pre.att.BorderSpacing;
import com.renderx.xep.pre.att.BorderWidth;
import com.renderx.xep.pre.att.BpIp;
import com.renderx.xep.pre.att.Clip;
import com.renderx.xep.pre.att.ColorProperty;
import com.renderx.xep.pre.att.ColorSpec;
import com.renderx.xep.pre.att.ColumnWidth;
import com.renderx.xep.pre.att.ContentType;
import com.renderx.xep.pre.att.Direction;
import com.renderx.xep.pre.att.Enumerated;
import com.renderx.xep.pre.att.FontFamily;
import com.renderx.xep.pre.att.FontSize;
import com.renderx.xep.pre.att.FontSpec;
import com.renderx.xep.pre.att.FontStretch;
import com.renderx.xep.pre.att.FourSides;
import com.renderx.xep.pre.att.HeightWidth;
import com.renderx.xep.pre.att.ImageURL;
import com.renderx.xep.pre.att.KeepSpec;
import com.renderx.xep.pre.att.LangSpec;
import com.renderx.xep.pre.att.LineHeight;
import com.renderx.xep.pre.att.LinkURL;
import com.renderx.xep.pre.att.Literal;
import com.renderx.xep.pre.att.MarginSide;
import com.renderx.xep.pre.att.MinOptMax;
import com.renderx.xep.pre.att.NoOp;
import com.renderx.xep.pre.att.PageBreak;
import com.renderx.xep.pre.att.PageBreakInside;
import com.renderx.xep.pre.att.PageSize;
import com.renderx.xep.pre.att.Position;
import com.renderx.xep.pre.att.ProgressionDimension;
import com.renderx.xep.pre.att.ReferenceOrientation;
import com.renderx.xep.pre.att.Synonym;
import com.renderx.xep.pre.att.TextDecoration;
import com.renderx.xep.pre.att.TextShadow;
import com.renderx.xep.pre.att.Typed;
import com.renderx.xep.pre.att.UnicodeBidi;
import com.renderx.xep.pre.att.VerticalAlign;
import com.renderx.xep.pre.att.WhiteSpace;
import com.renderx.xep.pre.att.WritingModeSpec;

public abstract class Attn {
	private static final Hashtable nametab = new Hashtable();

	private static int idcounter = 0;

	public final int id;

	public String name;

	public Attr defaultValue = null;

	public boolean inheritable = false;

	public int turn = 15;

	public static final int INTERNAL_ATTRIBUTE_TURN = -1;

	public static final int TABLE_STRUCTURE_TURN = 0;

	public static final int COLUMN_NUMBER_TURN = 1;

	public static final int FONT_SHORTHAND_TURN = 2;

	public static final int CONTEXT_ELEMENTS_TURN = 3;

	public static final int INDENTS_TURN = 4;

	public static final int BORDER_SHORTHAND_TURN = 5;

	public static final int BORDER_PROPERTY_SHORTHAND_TURN = 6;

	public static final int BORDER_SIDE_SHORTHAND_TURN = 7;

	public static final int LINE_HEIGHT_COMPOUND_TURN = 8;

	public static final int LINE_HEIGHT_TURN = 9;

	public static final int OTHER_SHORTHAND_TURN = 10;

	public static final int OTHER_COMPOUND_TURN = 11;

	public static final int BORDER_RELATIVE_SIDE_TURN = 12;

	public static final int BORDER_ABSOLUTE_SIDE_TURN = 13;

	public static final int IMAGE_TURN = 14;

	public static final int LAST_TURN = 15;

	public static Attn $absolute_position = null;

	public static Attn $after_indent = null;

	public static Attn $alignment_adjust = null;

	public static Attn $alignment_baseline = null;

	public static Attn $background_attachment = null;

	public static Attn $background_color = null;

	public static Attn $background_image = null;

	public static Attn $background_position_horizontal = null;

	public static Attn $background_position_vertical = null;

	public static Attn $background_repeat = null;

	public static Attn $baseline_shift = null;

	public static Attn $before_indent = null;

	public static Attn $blank_or_not_blank = null;

	public static Attn $block_progression_dimension_maximum = null;

	public static Attn $block_progression_dimension_minimum = null;

	public static Attn $block_progression_dimension_optimum = null;

	public static Attn $border_after_precedence = null;

	public static Attn $border_after_color = null;

	public static Attn $border_after_style = null;

	public static Attn $border_after_width_conditionality = null;

	public static Attn $border_after_width_length = null;

	public static Attn $border_before_precedence = null;

	public static Attn $border_before_color = null;

	public static Attn $border_before_style = null;

	public static Attn $border_before_width_conditionality = null;

	public static Attn $border_before_width_length = null;

	public static Attn $border_collapse = null;

	public static Attn $border_end_precedence = null;

	public static Attn $border_end_color = null;

	public static Attn $border_end_style = null;

	public static Attn $border_end_width_conditionality = null;

	public static Attn $border_end_width_length = null;

	public static Attn $border_separation_block_progression_direction = null;

	public static Attn $border_separation_inline_progression_direction = null;

	public static Attn $border_start_precedence = null;

	public static Attn $border_start_color = null;

	public static Attn $border_start_style = null;

	public static Attn $border_start_width_conditionality = null;

	public static Attn $border_start_width_length = null;

	public static Attn $bottom = null;

	public static Attn $break_after = null;

	public static Attn $break_before = null;

	public static Attn $caption_side = null;

	public static Attn $character = null;

	public static Attn $clear = null;

	public static Attn $clip = null;

	public static Attn $color = null;

	public static Attn $color_profile_name = null;

	public static Attn $column_count = null;

	public static Attn $column_gap = null;

	public static Attn $column_number = null;

	public static Attn $column_width_fixed = null;

	public static Attn $column_width_proportional = null;

	public static Attn $content_height = null;

	public static Attn $content_type = null;

	public static Attn $content_width = null;

	public static Attn $country = null;

	public static Attn $destination_placement_offset = null;

	public static Attn $direction = null;

	public static Attn $display_align = null;

	public static Attn $dominant_baseline = null;

	public static Attn $empty_cells = null;

	public static Attn $end_indent = null;

	public static Attn $ends_row = null;

	public static Attn $extent = null;

	public static Attn $external_destination = null;

	public static Attn $float = null;

	public static Attn $flow_name = null;

	public static Attn $font_family = null;

	public static Attn $font_size = null;

	public static Attn $font_size_adjust = null;

	public static Attn $font_selection_strategy = null;

	public static Attn $font_stretch = null;

	public static Attn $font_style = null;

	public static Attn $font_variant = null;

	public static Attn $font_weight = null;

	public static Attn $force_page_count = null;

	public static Attn $format = null;

	public static Attn $glyph_orientation_horizontal = null;

	public static Attn $glyph_orientation_vertical = null;

	public static Attn $grouping_separator = null;

	public static Attn $grouping_size = null;

	public static Attn $hyphenate = null;

	public static Attn $hyphenation_character = null;

	public static Attn $hyphenation_keep = null;

	public static Attn $hyphenation_ladder_count = null;

	public static Attn $hyphenation_push_character_count = null;

	public static Attn $hyphenation_remain_character_count = null;

	public static Attn $id = null;

	public static Attn $indicate_destination = null;

	public static Attn $initial_page_number = null;

	public static Attn $inline_progression_dimension_maximum = null;

	public static Attn $inline_progression_dimension_minimum = null;

	public static Attn $inline_progression_dimension_optimum = null;

	public static Attn $internal_destination = null;

	public static Attn $intrusion_displace = null;

	public static Attn $keep_together_within_column = null;

	public static Attn $keep_together_within_line = null;

	public static Attn $keep_together_within_page = null;

	public static Attn $keep_with_next_within_column = null;

	public static Attn $keep_with_next_within_line = null;

	public static Attn $keep_with_next_within_page = null;

	public static Attn $keep_with_previous_within_column = null;

	public static Attn $keep_with_previous_within_line = null;

	public static Attn $keep_with_previous_within_page = null;

	public static Attn $key = null;

	public static Attn $language = null;

	public static Attn $last_line_end_indent = null;

	public static Attn $leader_alignment = null;

	public static Attn $leader_length_maximum = null;

	public static Attn $leader_length_minimum = null;

	public static Attn $leader_length_optimum = null;

	public static Attn $leader_pattern = null;

	public static Attn $leader_pattern_width = null;

	public static Attn $left = null;

	public static Attn $letter_spacing_conditionality = null;

	public static Attn $letter_spacing_maximum = null;

	public static Attn $letter_spacing_minimum = null;

	public static Attn $letter_spacing_optimum = null;

	public static Attn $letter_spacing_precedence = null;

	public static Attn $letter_value = null;

	public static Attn $line_height_conditionality = null;

	public static Attn $line_height_maximum = null;

	public static Attn $line_height_minimum = null;

	public static Attn $line_height_optimum = null;

	public static Attn $line_height_precedence = null;

	public static Attn $line_height_shift_adjustment = null;

	public static Attn $line_stacking_strategy = null;

	public static Attn $linefeed_treatment = null;

	public static Attn $link_back = null;

	public static Attn $list_separator = null;

	public static Attn $margin_after = null;

	public static Attn $margin_before = null;

	public static Attn $margin_end = null;

	public static Attn $margin_start = null;

	public static Attn $marker_class_name = null;

	public static Attn $master_name = null;

	public static Attn $master_reference = null;

	public static Attn $maximum_repeats = null;

	public static Attn $media_usage = null;

	public static Attn $merge_subsequent_page_numbers = null;

	public static Attn $number_columns_repeated = null;

	public static Attn $number_columns_spanned = null;

	public static Attn $number_rows_spanned = null;

	public static Attn $odd_or_even = null;

	public static Attn $orphans = null;

	public static Attn $overflow = null;

	public static Attn $padding_after_conditionality = null;

	public static Attn $padding_after_length = null;

	public static Attn $padding_before_conditionality = null;

	public static Attn $padding_before_length = null;

	public static Attn $padding_end_conditionality = null;

	public static Attn $padding_end_length = null;

	public static Attn $padding_start_conditionality = null;

	public static Attn $padding_start_length = null;

	public static Attn $page_height = null;

	public static Attn $page_position = null;

	public static Attn $page_width = null;

	public static Attn $precedence = null;

	public static Attn $provisional_distance_between_starts = null;

	public static Attn $provisional_label_separation = null;

	public static Attn $range_separator = null;

	public static Attn $ref_id = null;

	public static Attn $ref_key = null;

	public static Attn $reference_orientation = null;

	public static Attn $region_name = null;

	public static Attn $relative_align = null;

	public static Attn $relative_position = null;

	public static Attn $rendering_intent = null;

	public static Attn $retrieve_boundary = null;

	public static Attn $retrieve_class_name = null;

	public static Attn $retrieve_position = null;

	public static Attn $right = null;

	public static Attn $role = null;

	public static Attn $rule_style = null;

	public static Attn $rule_thickness = null;

	public static Attn $scaling = null;

	public static Attn $scaling_method = null;

	public static Attn $score_spaces = null;

	public static Attn $script = null;

	public static Attn $show_destination = null;

	public static Attn $source_document = null;

	public static Attn $space_after_conditionality = null;

	public static Attn $space_after_maximum = null;

	public static Attn $space_after_minimum = null;

	public static Attn $space_after_optimum = null;

	public static Attn $space_after_precedence = null;

	public static Attn $space_before_conditionality = null;

	public static Attn $space_before_maximum = null;

	public static Attn $space_before_minimum = null;

	public static Attn $space_before_optimum = null;

	public static Attn $space_before_precedence = null;

	public static Attn $space_end_conditionality = null;

	public static Attn $space_end_maximum = null;

	public static Attn $space_end_minimum = null;

	public static Attn $space_end_optimum = null;

	public static Attn $space_end_precedence = null;

	public static Attn $space_start_conditionality = null;

	public static Attn $space_start_maximum = null;

	public static Attn $space_start_minimum = null;

	public static Attn $space_start_optimum = null;

	public static Attn $space_start_precedence = null;

	public static Attn $span = null;

	public static Attn $src = null;

	public static Attn $start_indent = null;

	public static Attn $starts_row = null;

	public static Attn $suppress_at_line_break = null;

	public static Attn $table_layout = null;

	public static Attn $table_omit_footer_at_break = null;

	public static Attn $table_omit_header_at_break = null;

	public static Attn $target_presentation_context = null;

	public static Attn $target_processing_context = null;

	public static Attn $target_stylesheet = null;

	public static Attn $text_align = null;

	public static Attn $text_align_last = null;

	public static Attn $text_altitude = null;

	public static Attn $text_decoration = null;

	public static Attn $text_depth = null;

	public static Attn $text_indent = null;

	public static Attn $text_shadow = null;

	public static Attn $text_transform = null;

	public static Attn $top = null;

	public static Attn $treat_as_word_space = null;

	public static Attn $unicode_bidi = null;

	public static Attn $visibility = null;

	public static Attn $white_space_collapse = null;

	public static Attn $white_space_treatment = null;

	public static Attn $widows = null;

	public static Attn $word_spacing_conditionality = null;

	public static Attn $word_spacing_maximum = null;

	public static Attn $word_spacing_minimum = null;

	public static Attn $word_spacing_optimum = null;

	public static Attn $word_spacing_precedence = null;

	public static Attn $wrap_option = null;

	public static Attn $writing_mode = null;

	public static Attn $z_index = null;

	public static Attn $background_content_type = null;

	public static Attn $background_content_height = null;

	public static Attn $background_content_width = null;

	public static Attn $background_scaling = null;

	public static Attn $initial_destination = null;

	public static Attn $collapse_subtree = null;

	public static Attn $leader_dot = null;

	public static Attn $leader_content = null;

	public static Attn $system_id = null;

	public static Attn $background_system_id = null;

	public static Attn $instream_image = null;

	public static Attn $background_instream_image = null;

	public static Attn $table_omit_initial_header = null;

	public static Attn $table_omit_final_footer = null;

	public static Attn $name = null;

	public static Attn $value = null;

	public static Attn $TEXT = null;

	public static Attn $BIDI_CLASS = null;

	public static Attn $border_right_color = null;

	public static Attn $border_right_style = null;

	public static Attn $border_right_width = null;

	public static Attn $border_left_color = null;

	public static Attn $border_left_style = null;

	public static Attn $border_left_width = null;

	public static Attn $border_top_color = null;

	public static Attn $border_top_style = null;

	public static Attn $border_top_width = null;

	public static Attn $border_bottom_color = null;

	public static Attn $border_bottom_style = null;

	public static Attn $border_bottom_width = null;

	public static Attn $padding_top = null;

	public static Attn $padding_bottom = null;

	public static Attn $padding_right = null;

	public static Attn $padding_left = null;

	public static Attn $margin_top = null;

	public static Attn $margin_bottom = null;

	public static Attn $margin_right = null;

	public static Attn $margin_left = null;

	public static Attn $padding_before = null;

	public static Attn $padding_after = null;

	public static Attn $padding_start = null;

	public static Attn $padding_end = null;

	public static Attn $border_before_width = null;

	public static Attn $border_after_width = null;

	public static Attn $border_start_width = null;

	public static Attn $border_end_width = null;

	public static Attn $line_height = null;

	public static Attn $space_before = null;

	public static Attn $space_after = null;

	public static Attn $space_start = null;

	public static Attn $space_end = null;

	public static Attn $leader_length = null;

	public static Attn $letter_spacing = null;

	public static Attn $word_spacing = null;

	public static Attn $inline_progression_dimension = null;

	public static Attn $block_progression_dimension = null;

	public static Attn $border_separation = null;

	public static Attn $keep_with_next = null;

	public static Attn $keep_with_previous = null;

	public static Attn $keep_together = null;

	public static Attn $column_width = null;

	public static Attn $width = null;

	public static Attn $max_width = null;

	public static Attn $min_width = null;

	public static Attn $height = null;

	public static Attn $max_height = null;

	public static Attn $min_height = null;

	public static Attn $border_top = null;

	public static Attn $border_bottom = null;

	public static Attn $border_right = null;

	public static Attn $border_left = null;

	public static Attn $border_width = null;

	public static Attn $border_style = null;

	public static Attn $border_color = null;

	public static Attn $padding = null;

	public static Attn $margin = null;

	public static Attn $border = null;

	public static Attn $background = null;

	public static Attn $background_position = null;

	public static Attn $page_break_after = null;

	public static Attn $page_break_before = null;

	public static Attn $page_break_inside = null;

	public static Attn $font = null;

	public static Attn $position = null;

	public static Attn $white_space = null;

	public static Attn $size = null;

	public static Attn $vertical_align = null;

	public static Attn $xml_lang = null;

	public static Attn $border_spacing = null;

	public static Attn $active_state = null;

	public static Attn $auto_restore = null;

	public static Attn $starting_state = null;

	public static Attn $switch_to = null;

	public static Attn $case_name = null;

	public static Attn $case_title = null;

	public static Attn $asimuth = null;

	public static Attn $cue = null;

	public static Attn $cue_after = null;

	public static Attn $cue_before = null;

	public static Attn $elevation = null;

	public static Attn $pause = null;

	public static Attn $pause_after = null;

	public static Attn $pause_before = null;

	public static Attn $pitch_range = null;

	public static Attn $play_during = null;

	public static Attn $richness = null;

	public static Attn $speak = null;

	public static Attn $speak_header = null;

	public static Attn $speak_numeral = null;

	public static Attn $speak_punctuation = null;

	public static Attn $speech_rate = null;

	public static Attn $stress = null;

	public static Attn $voice_family = null;

	public static Attn $volume = null;

	public static Attn $border_after_width_collapsed = null;

	public static Attn $border_before_width_collapsed = null;

	public static Attn $border_start_width_collapsed = null;

	public static Attn $border_end_width_collapsed = null;

	public static Attn $table_identifier = null;

	public static Attn $table_element_class = null;

	public static Attn $body_number = null;

	public static Attn $row_number = null;

	public static Attn $image_base_url = null;

	private static boolean initialized = false;

	protected Attn(String string) {
		name = string;
		nametab.put(string, this);
		id = ++idcounter;
	}

	public abstract void process(String string, ParserContext parsercontext)
			throws ParserException;

	public Attr postprocess(Attr attr, ParserContext parsercontext)
			throws ParserException {
		return attr;
	}

	public String toString() {
		return name;
	}

	public static Attn byName(String string) throws ParserException {
		Attn attn = (Attn) nametab.get(string);
		if (attn == null)
			throw new ParserException("illegal attribute name: " + string);
		return attn;
	}

	public static Attn oldName(String string) {
		return (Attn) nametab.get(string);
	}

	public static synchronized void init() throws InternalException {
		if (!initialized) {
			Attr.init();
			try {
				$absolute_position = new Enumerated("absolute-position",
						"auto", new String[] { "auto", "absolute", "fixed" });
				$after_indent = new Typed("after-indent", "0pt", 44);
				$after_indent.inheritable = true;
				$after_indent.turn = 4;
				$margin_after = new MarginSide("margin-after", "0pt",
						"after-indent");
				$margin_after.turn = 12;
				$alignment_adjust = new Typed("alignment-adjust", "auto", 12,
						(new String[] { "auto", "baseline", "middle",
								"central", "top", "text-top", "before-edge",
								"text-before-edge", "bottom", "text-bottom",
								"after-edge", "text-after-edge", "ideographic",
								"alphabetic", "hanging", "mathematical" }));
				$alignment_baseline = new Enumerated("alignment-baseline",
						"auto", (new String[] { "auto", "baseline", "middle",
								"central", "top", "text-top", "before-edge",
								"text-before-edge", "bottom", "text-bottom",
								"after-edge", "text-after-edge", "ideographic",
								"alphabetic", "hanging", "mathematical" }));
				$background_attachment = new Enumerated(
						"background-attachment", "scroll", new String[] {
								"scroll", "fixed" });
				$background_color = new ColorSpec("background-color",
						"transparent");
				$background_content_type = new ContentType(
						"background-content-type", "auto");
				$background_image = new ImageURL("background-image",
						"background-content-type");
				$background_image.turn = 14;
				$background_position_horizontal = new Typed(
						"background-position-horizontal", "0pt", 12);
				((Typed) $background_position_horizontal)
						.addAlias("left", "0%");
				((Typed) $background_position_horizontal).addAlias("center",
						"50%");
				((Typed) $background_position_horizontal).addAlias("right",
						"100%");
				$background_position_vertical = new Typed(
						"background-position-vertical", "0pt", 12);
				((Typed) $background_position_vertical).addAlias("top", "0%");
				((Typed) $background_position_vertical).addAlias("center",
						"50%");
				((Typed) $background_position_vertical).addAlias("bottom",
						"100%");
				$background_repeat = new Enumerated("background-repeat",
						"repeat", new String[] { "repeat", "repeat-x",
								"repeat-y", "no-repeat" });
				$baseline_shift = new Typed("baseline-shift", "0pt", 12,
						new String[] { "baseline", "sub", "super" });
				$before_indent = new Typed("before-indent", "0pt", 44);
				$before_indent.inheritable = true;
				$before_indent.turn = 4;
				$margin_before = new MarginSide("margin-before", "0pt",
						"before-indent");
				$margin_before.turn = 12;
				$blank_or_not_blank = new Enumerated("blank-or-not-blank",
						"any", new String[] { "blank", "not-blank", "any" });
				$block_progression_dimension_maximum = (new ProgressionDimension(
						"block-progression-dimension.maximum", "auto"));
				$block_progression_dimension_minimum = (new ProgressionDimension(
						"block-progression-dimension.minimum", "auto"));
				$block_progression_dimension_optimum = (new ProgressionDimension(
						"block-progression-dimension.optimum", "auto"));
				$inline_progression_dimension_maximum = (new ProgressionDimension(
						"inline-progression-dimension.maximum", "auto"));
				$inline_progression_dimension_minimum = (new ProgressionDimension(
						"inline-progression-dimension.minimum", "auto"));
				$inline_progression_dimension_optimum = (new ProgressionDimension(
						"inline-progression-dimension.optimum", "auto"));
				((ProgressionDimension) $block_progression_dimension_minimum).perpendicular = $inline_progression_dimension_minimum;
				((ProgressionDimension) $block_progression_dimension_optimum).perpendicular = $inline_progression_dimension_optimum;
				((ProgressionDimension) $block_progression_dimension_maximum).perpendicular = $inline_progression_dimension_maximum;
				((ProgressionDimension) $inline_progression_dimension_minimum).perpendicular = $block_progression_dimension_minimum;
				((ProgressionDimension) $inline_progression_dimension_optimum).perpendicular = $block_progression_dimension_optimum;
				((ProgressionDimension) $inline_progression_dimension_maximum).perpendicular = $block_progression_dimension_maximum;
				$border_after_precedence = new Typed("border-after-precedence",
						"auto", 1, new String[] { "auto", "force" });
				$border_after_color = new ColorSpec("border-after-color",
						"black");
				$border_after_color.turn = 12;
				$border_after_style = new Enumerated("border-after-style",
						"none", (new String[] { "none", "hidden", "solid",
								"double", "dashed", "dotted", "groove",
								"ridge", "inset", "outset" }));
				$border_after_style.turn = 12;
				$border_after_width_conditionality = new Enumerated(
						"border-after-width.conditionality", "discard",
						new String[] { "discard", "retain" });
				$border_after_width_conditionality.turn = 12;
				$border_after_width_length = new BorderWidth(
						"border-after-width.length", "0pt",
						"border-after-color");
				$border_after_width_length.turn = 12;
				$border_before_precedence = new Typed(
						"border-before-precedence", "auto", 1, new String[] {
								"auto", "force" });
				$border_before_color = new ColorSpec("border-before-color",
						"black");
				$border_before_color.turn = 12;
				$border_before_style = new Enumerated("border-before-style",
						"none", (new String[] { "none", "hidden", "solid",
								"double", "dashed", "dotted", "groove",
								"ridge", "inset", "outset" }));
				$border_before_style.turn = 12;
				$border_before_width_conditionality = new Enumerated(
						"border-before-width.conditionality", "discard",
						new String[] { "discard", "retain" });
				$border_before_width_conditionality.turn = 12;
				$border_before_width_length = new BorderWidth(
						"border-before-width.length", "0pt",
						"border-before-color");
				$border_before_width_length.turn = 12;
				$border_collapse = new Enumerated("border-collapse",
						"collapse", new String[] { "collapse",
								"collapse-with-precedence", "separate" });
				$border_collapse.inheritable = true;
				$border_collapse.turn = 0;
				$border_end_precedence = new Typed("border-end-precedence",
						"auto", 1, new String[] { "auto", "force" });
				$border_end_color = new ColorSpec("border-end-color", "black");
				$border_end_color.turn = 12;
				$border_end_style = new Enumerated("border-end-style", "none",
						(new String[] { "none", "hidden", "solid", "double",
								"dashed", "dotted", "groove", "ridge", "inset",
								"outset" }));
				$border_end_style.turn = 12;
				$border_end_width_conditionality = new Enumerated(
						"border-end-width.conditionality", "discard",
						new String[] { "discard", "retain" });
				$border_end_width_conditionality.turn = 12;
				$border_end_width_length = new BorderWidth(
						"border-end-width.length", "0pt", "border-end-color");
				$border_end_width_length.turn = 12;
				$border_separation_block_progression_direction = (new Typed(
						"border-separation.block-progression-direction", "0pt",
						8));
				$border_separation_block_progression_direction.inheritable = true;
				$border_separation_inline_progression_direction = (new Typed(
						"border-separation.inline-progression-direction",
						"0pt", 8));
				$border_separation_inline_progression_direction.inheritable = true;
				$border_start_precedence = new Typed("border-start-precedence",
						"auto", 1, new String[] { "auto", "force" });
				$border_start_color = new ColorSpec("border-start-color",
						"black");
				$border_start_color.turn = 12;
				$border_start_style = new Enumerated("border-start-style",
						"none", (new String[] { "none", "hidden", "solid",
								"double", "dashed", "dotted", "groove",
								"ridge", "inset", "outset" }));
				$border_start_style.turn = 12;
				$border_start_width_conditionality = new Enumerated(
						"border-start-width.conditionality", "discard",
						new String[] { "discard", "retain" });
				$border_start_width_conditionality.turn = 12;
				$border_start_width_length = new BorderWidth(
						"border-start-width.length", "0pt",
						"border-start-color");
				$border_start_width_length.turn = 12;
				$bottom = new Typed("bottom", "auto", 44);
				$break_after = new Enumerated("break-after", "auto",
						new String[] { "auto", "column", "page", "even-page",
								"odd-page" });
				$break_before = new Enumerated("break-before", "auto",
						new String[] { "auto", "column", "page", "even-page",
								"odd-page" });
				$caption_side = new Enumerated("caption-side", "before",
						new String[] { "before", "after", "start", "end",
								"top", "bottom", "left", "right" });
				$caption_side.inheritable = true;
				$character = new Literal("character");
				$clear = new Enumerated("clear", "none", new String[] { "none",
						"start", "end", "left", "right", "both", "inside",
						"outside" });
				((Enumerated) $clear).addAlias("left", "start");
				((Enumerated) $clear).addAlias("right", "end");
				$clip = new Clip("clip", "auto");
				$color = new ColorProperty("color", "black");
				$color.inheritable = true;
				$color.turn = 3;
				$color_profile_name = new Literal("color-profile-name");
				$column_count = new Typed("column-count", "1", 1);
				$column_gap = new Typed("column-gap", "12pt", 12);
				$column_number = new Typed("column-number", "-1", 1);
				$column_number.turn = 1;
				$column_width_fixed = new Typed("column-width-fixed", "0pt", 12);
				$column_width_proportional = new Typed(
						"column-width-proportional", "auto", 2);
				$content_height = new Typed("content-height", "auto", 12,
						new String[] { "auto", "scale-to-fit" });
				$content_width = new Typed("content-width", "auto", 12,
						new String[] { "auto", "scale-to-fit" });
				$country = new Typed("country", "none", 32);
				$country.inheritable = true;
				$destination_placement_offset = new Typed(
						"destination-placement-offset", "0pt", 8);
				$direction = new Direction("direction", "ltr");
				$direction.inheritable = true;
				$display_align = new Enumerated("display-align", "auto",
						new String[] { "before", "center", "after" });
				((Enumerated) $display_align).addAlias("auto", "before");
				$display_align.inheritable = true;
				$dominant_baseline = new Enumerated("dominant-baseline",
						"auto", (new String[] { "auto", "use-script",
								"no-change", "reset-size", "ideographic",
								"alphabetic", "hanging", "mathematical",
								"central", "middle", "text-after-edge",
								"text-before-edge" }));
				$empty_cells = new Enumerated("empty-cells", "show",
						new String[] { "show", "hide" });
				$empty_cells.inheritable = true;
				$end_indent = new Typed("end-indent", "0pt", 44);
				$end_indent.inheritable = true;
				$end_indent.turn = 4;
				$margin_end = new MarginSide("margin-end", "0pt", "end-indent");
				$margin_end.turn = 12;
				$ends_row = new Bool("ends-row", "false");
				$ends_row.turn = 0;
				$extent = new Typed("extent", "0pt", 12);
				$external_destination = new LinkURL("external-destination");
				$float = new Enumerated("float", "none", new String[] {
						"before", "start", "end", "left", "right", "none",
						"inside", "outside" });
				$flow_name = new Literal("flow-name");
				$font_family = new FontFamily("font-family", "");
				$font_family.inheritable = true;
				$font_size = new FontSize("font-size", "12pt");
				$font_size.inheritable = true;
				$font_size.turn = 3;
				$font_size_adjust = new Typed("font-size-adjust", "none", 2,
						new String[] { "none" });
				$font_size_adjust.inheritable = true;
				$font_selection_strategy = new Enumerated(
						"font-selection-strategy", "auto", (new String[] {
								"auto", "character-by-character" }));
				$font_selection_strategy.inheritable = true;
				$font_stretch = new FontStretch("font-stretch", "normal");
				$font_stretch.inheritable = true;
				$font_style = new Enumerated("font-style", "normal",
						new String[] { "normal", "italic", "oblique",
								"backslant" });
				$font_style.inheritable = true;
				$font_variant = new Enumerated("font-variant", "normal",
						new String[] { "normal", "small-caps" });
				$font_variant.inheritable = true;
				$font_weight = new Typed("font-weight", "normal", 1);
				((Typed) $font_weight).addAlias("normal", "400");
				((Typed) $font_weight).addAlias("bold", "700");
				((Typed) $font_weight).addAlias("lighter", "400");
				((Typed) $font_weight).addAlias("bolder", "700");
				$font_weight.inheritable = true;
				$force_page_count = new Enumerated("force-page-count", "auto",
						(new String[] { "auto", "even", "odd", "end-on-even",
								"end-on-odd", "no-force" }));
				$format = new Literal("format", "1");
				$glyph_orientation_horizontal = new Typed(
						"glyph-orientation-horizontal", "0deg", 16);
				$glyph_orientation_horizontal.inheritable = true;
				$glyph_orientation_vertical = new Typed(
						"glyph-orientation-vertical", "auto", 16,
						new String[] { "auto" });
				$glyph_orientation_vertical.inheritable = true;
				$grouping_separator = new Literal("grouping-separator", "");
				$grouping_size = new Typed("grouping-size", "9999999999", 1);
				$hyphenate = new Bool("hyphenate", "false");
				$hyphenate.inheritable = true;
				$hyphenation_character = new Literal("hyphenation-character",
						"-");
				$hyphenation_character.inheritable = true;
				$hyphenation_keep = new Enumerated("hyphenation-keep", "auto",
						new String[] { "auto", "column", "page" });
				$hyphenation_keep.inheritable = true;
				$hyphenation_ladder_count = new Typed(
						"hyphenation-ladder-count", "none", 1,
						new String[] { "none" });
				$hyphenation_ladder_count.inheritable = true;
				$hyphenation_push_character_count = new Typed(
						"hyphenation-push-character-count", "2", 1);
				$hyphenation_push_character_count.inheritable = true;
				$hyphenation_remain_character_count = new Typed(
						"hyphenation-remain-character-count", "2", 1);
				$hyphenation_remain_character_count.inheritable = true;
				$id = new Literal("id");
				$indicate_destination = new Bool("indicate-destination",
						"false");
				$initial_page_number = new Typed("initial-page-number", "auto",
						1, new String[] { "auto", "auto-odd", "auto-even" });
				$internal_destination = new Literal("internal-destination");
				$intrusion_displace = new Enumerated("intrusion-displace",
						"auto", new String[] { "auto", "none", "line",
								"indent", "block" });
				$intrusion_displace.inheritable = true;
				$keep_together_within_column = new Typed(
						"keep-together.within-column", "auto", 1, new String[] {
								"auto", "always" });
				$keep_together_within_column.inheritable = true;
				$keep_together_within_line = new Typed(
						"keep-together.within-line", "auto", 1, new String[] {
								"auto", "always" });
				$keep_together_within_line.inheritable = true;
				$keep_together_within_page = new Typed(
						"keep-together.within-page", "auto", 1, new String[] {
								"auto", "always" });
				$keep_together_within_page.inheritable = true;
				$keep_with_next_within_column = new Typed(
						"keep-with-next.within-column", "auto", 1,
						new String[] { "auto", "always" });
				$keep_with_next_within_line = new Typed(
						"keep-with-next.within-line", "auto", 1, new String[] {
								"auto", "always" });
				$keep_with_next_within_page = new Typed(
						"keep-with-next.within-page", "auto", 1, new String[] {
								"auto", "always" });
				$keep_with_previous_within_column = new Typed(
						"keep-with-previous.within-column", "auto", 1,
						new String[] { "auto", "always" });
				$keep_with_previous_within_line = new Typed(
						"keep-with-previous.within-line", "auto", 1,
						new String[] { "auto", "always" });
				$keep_with_previous_within_page = new Typed(
						"keep-with-previous.within-page", "auto", 1,
						new String[] { "auto", "always" });
				$key = new Literal("key");
				$language = new Typed("language", "none", 32);
				$language.inheritable = true;
				$last_line_end_indent = new Typed("last-line-end-indent",
						"0pt", 12);
				$last_line_end_indent.inheritable = true;
				$leader_alignment = new Enumerated("leader-alignment", "none",
						new String[] { "none", "reference-area", "page" });
				$leader_alignment.inheritable = true;
				$leader_length_maximum = new Typed("leader-length.maximum",
						"100%", 12);
				$leader_length_maximum.inheritable = true;
				$leader_length_minimum = new Typed("leader-length.minimum",
						"0pt", 12);
				$leader_length_minimum.inheritable = true;
				$leader_length_optimum = new Typed("leader-length.optimum",
						"12pt", 12);
				$leader_length_optimum.inheritable = true;
				$leader_pattern = new Enumerated("leader-pattern", "space",
						new String[] { "space", "rule", "dots", "use-content" });
				$leader_pattern.inheritable = true;
				$leader_pattern_width = new Typed("leader-pattern-width",
						"use-font-metrics", 12);
				((Typed) $leader_pattern_width).addAlias("use-font-metrics",
						"0pt");
				$leader_pattern_width.inheritable = true;
				$left = new Typed("left", "auto", 44);
				$letter_spacing_conditionality = new Enumerated(
						"letter-spacing.conditionality", "discard",
						new String[] { "discard", "retain" });
				$letter_spacing_conditionality.inheritable = true;
				$letter_spacing_maximum = new Typed("letter-spacing.maximum",
						"auto", 8, new String[] { "auto" });
				((Typed) $letter_spacing_maximum).addAlias("normal", "auto");
				$letter_spacing_maximum.inheritable = true;
				$letter_spacing_minimum = new Typed("letter-spacing.minimum",
						"auto", 8, new String[] { "auto" });
				((Typed) $letter_spacing_minimum).addAlias("normal", "auto");
				$letter_spacing_minimum.inheritable = true;
				$letter_spacing_optimum = new Typed("letter-spacing.optimum",
						"auto", 8, new String[] { "auto" });
				((Typed) $letter_spacing_optimum).addAlias("normal", "auto");
				$letter_spacing_optimum.inheritable = true;
				$letter_spacing_precedence = new Typed(
						"letter-spacing.precedence", "0", 1,
						new String[] { "force" });
				$letter_spacing_precedence.inheritable = true;
				$letter_value = new Enumerated("letter-value", "auto",
						new String[] { "auto", "alphabetic", "traditional" });
				$line_height_conditionality = new Enumerated(
						"line-height.conditionality", "retain", new String[] {
								"discard", "retain" });
				$line_height_conditionality.inheritable = true;
				$line_height_conditionality.turn = 9;
				$line_height_maximum = new LineHeight("line-height.maximum",
						"normal");
				$line_height_maximum.inheritable = true;
				$line_height_maximum.turn = 9;
				$line_height_minimum = new LineHeight("line-height.minimum",
						"normal");
				$line_height_minimum.inheritable = true;
				$line_height_minimum.turn = 9;
				$line_height_optimum = new LineHeight("line-height.optimum",
						"normal");
				$line_height_optimum.inheritable = true;
				$line_height_optimum.turn = 9;
				$line_height_precedence = new Typed("line-height.precedence",
						"force", 1, new String[] { "force" });
				$line_height_precedence.inheritable = true;
				$line_height_precedence.turn = 9;
				$line_height_shift_adjustment = new Enumerated(
						"line-height-shift-adjustment", "consider-shifts",
						new String[] { "consider-shifts", "disregard-shifts" });
				$line_height_shift_adjustment.inheritable = true;
				$line_stacking_strategy = new Enumerated(
						"line-stacking-strategy", "max-height", new String[] {
								"line-height", "font-height", "max-height" });
				$line_stacking_strategy.inheritable = true;
				$linefeed_treatment = new Enumerated("linefeed-treatment",
						"treat-as-space",
						(new String[] { "ignore", "preserve", "treat-as-space",
								"treat-as-zero-width-space" }));
				$linefeed_treatment.inheritable = true;
				$link_back = new Bool("link-back", "false");
				$link_back.inheritable = true;
				$list_separator = new Literal("list-separator", ", ");
				$list_separator.inheritable = true;
				$marker_class_name = new Literal("marker-class-name");
				$master_name = new Literal("master-name");
				$master_reference = new Literal("master-reference");
				$maximum_repeats = new Typed("maximum-repeats", "no-limit", 1);
				((Typed) $maximum_repeats).addAlias("no-limit", "-1");
				$media_usage = new Enumerated("media-usage", "auto",
						new String[] { "auto", "paginate",
								"bounded-in-one-dimension", "unbounded" });
				$merge_subsequent_page_numbers = new Bool(
						"merge-subsequent-page-numbers", "false");
				$merge_subsequent_page_numbers.inheritable = true;
				$number_columns_repeated = new Typed("number-columns-repeated",
						"1", 1);
				$number_columns_spanned = new Typed("number-columns-spanned",
						"1", 1);
				$number_rows_spanned = new Typed("number-rows-spanned", "1", 1);
				$odd_or_even = new Enumerated("odd-or-even", "any",
						new String[] { "odd", "even", "any" });
				$orphans = new Typed("orphans", "2", 1);
				$orphans.inheritable = true;
				$overflow = new Enumerated("overflow", "visible",
						(new String[] { "visible", "hidden", "scroll",
								"error-if-overflow", "auto" }));
				((Enumerated) $overflow).addAlias("auto", "visible");
				$padding_after_conditionality = new Enumerated(
						"padding-after.conditionality", "discard",
						new String[] { "discard", "retain" });
				$padding_after_conditionality.turn = 12;
				$padding_after_length = new Typed("padding-after.length",
						"0pt", 12);
				$padding_after_length.turn = 12;
				$padding_before_conditionality = new Enumerated(
						"padding-before.conditionality", "discard",
						new String[] { "discard", "retain" });
				$padding_before_conditionality.turn = 12;
				$padding_before_length = new Typed("padding-before.length",
						"0pt", 12);
				$padding_before_length.turn = 12;
				$padding_end_conditionality = new Enumerated(
						"padding-end.conditionality", "discard", new String[] {
								"discard", "retain" });
				$padding_end_conditionality.turn = 12;
				$padding_end_length = new Typed("padding-end.length", "0pt", 12);
				$padding_end_length.turn = 12;
				$padding_start_conditionality = new Enumerated(
						"padding-start.conditionality", "discard",
						new String[] { "discard", "retain" });
				$padding_start_conditionality.turn = 12;
				$padding_start_length = new Typed("padding-start.length",
						"0pt", 12);
				$padding_start_length.turn = 12;
				$page_height = new Typed("page-height", "auto", 8,
						new String[] { "auto", "indefinite" });
				$page_position = new Enumerated("page-position", "any",
						new String[] { "first", "last", "rest", "any" });
				$page_width = new Typed("page-width", "auto", 8, new String[] {
						"auto", "indefinite" });
				$precedence = new Bool("precedence", "false");
				$provisional_distance_between_starts = new Typed(
						"provisional-distance-between-starts", "24pt", 12);
				$provisional_distance_between_starts.inheritable = true;
				$provisional_label_separation = new Typed(
						"provisional-label-separation", "6pt", 12);
				$provisional_label_separation.inheritable = true;
				$range_separator = new Literal("range-separator", "\u2013");
				$range_separator.inheritable = true;
				$ref_id = new Literal("ref-id");
				$ref_key = new Literal("ref-key");
				$reference_orientation = new ReferenceOrientation(
						"reference-orientation");
				$reference_orientation.turn = 3;
				$region_name = new Literal("region-name");
				$relative_align = new Enumerated("relative-align", "before",
						new String[] { "before", "baseline" });
				$relative_align.inheritable = true;
				$relative_position = new Enumerated("relative-position",
						"static", new String[] { "static", "relative" });
				$rendering_intent = new Enumerated("rendering-intent", "auto",
						new String[] { "auto", "perceptual", "saturation",
								"relative-colorimetric",
								"absolute-colorimetric" });
				$retrieve_boundary = new Enumerated("retrieve-boundary",
						"page-sequence", new String[] { "page",
								"page-sequence", "document" });
				$retrieve_class_name = new Literal("retrieve-class-name");
				$retrieve_position = new Enumerated("retrieve-position",
						"first-starting-within-page", (new String[] {
								"first-starting-within-page",
								"first-including-carryover",
								"last-starting-within-page",
								"last-ending-within-page" }));
				$right = new Typed("right", "auto", 44);
				$role = new Literal("role", "none");
				$rule_style = new Enumerated("rule-style", "solid",
						new String[] { "none", "dotted", "dashed", "solid",
								"double", "groove", "ridge" });
				$rule_style.inheritable = true;
				$rule_thickness = new Typed("rule-thickness", "1pt", 8);
				$rule_thickness.inheritable = true;
				$scaling = new Enumerated("scaling", "uniform", new String[] {
						"uniform", "non-uniform" });
				$scaling_method = new Enumerated("scaling-method", "auto",
						new String[] { "auto", "integer-pixels",
								"resample-any-method" });
				$score_spaces = new Bool("score-spaces", "false");
				$score_spaces.inheritable = true;
				$script = new Typed("script", "none", 32);
				$script.inheritable = true;
				$show_destination = new Enumerated("show-destination",
						"replace", new String[] { "replace", "new" });
				$source_document = new Literal("source-document", "none");
				$space_after_conditionality = new Enumerated(
						"space-after.conditionality", "discard", new String[] {
								"discard", "retain" });
				$space_after_maximum = new Typed("space-after.maximum", "0pt",
						8);
				$space_after_minimum = new Typed("space-after.minimum", "0pt",
						8);
				$space_after_optimum = new Typed("space-after.optimum", "0pt",
						8);
				$space_after_precedence = new Typed("space-after.precedence",
						"0", 1, new String[] { "force" });
				$space_before_conditionality = new Enumerated(
						"space-before.conditionality", "discard", new String[] {
								"discard", "retain" });
				$space_before_maximum = new Typed("space-before.maximum",
						"0pt", 8);
				$space_before_minimum = new Typed("space-before.minimum",
						"0pt", 8);
				$space_before_optimum = new Typed("space-before.optimum",
						"0pt", 8);
				$space_before_precedence = new Typed("space-before.precedence",
						"0", 1, new String[] { "force" });
				$space_end_conditionality = new Enumerated(
						"space-end.conditionality", "discard", new String[] {
								"discard", "retain" });
				$space_end_maximum = new Typed("space-end.maximum", "0pt", 12);
				$space_end_minimum = new Typed("space-end.minimum", "0pt", 12);
				$space_end_optimum = new Typed("space-end.optimum", "0pt", 12);
				$space_end_precedence = new Typed("space-end.precedence", "0",
						1, new String[] { "force" });
				$space_start_conditionality = new Enumerated(
						"space-start.conditionality", "discard", new String[] {
								"discard", "retain" });
				$space_start_maximum = new Typed("space-start.maximum", "0pt",
						12);
				$space_start_minimum = new Typed("space-start.minimum", "0pt",
						12);
				$space_start_optimum = new Typed("space-start.optimum", "0pt",
						12);
				$space_start_precedence = new Typed("space-start.precedence",
						"0", 1, new String[] { "force" });
				$span = new Enumerated("span", "none", new String[] { "all",
						"none" });
				$content_type = new ContentType("content-type", "auto");
				$src = new ImageURL("src", "content-type");
				$src.turn = 14;
				$start_indent = new Typed("start-indent", "0pt", 44);
				$start_indent.inheritable = true;
				$start_indent.turn = 4;
				$margin_start = new MarginSide("margin-start", "0pt",
						"start-indent");
				$margin_start.turn = 12;
				$starts_row = new Bool("starts-row", "false");
				$starts_row.turn = 0;
				$suppress_at_line_break = new Enumerated(
						"suppress-at-line-break", "auto", new String[] {
								"auto", "suppress", "retain" });
				$table_layout = new Enumerated("table-layout", "auto",
						new String[] { "auto", "fixed" });
				$table_omit_footer_at_break = new Bool(
						"table-omit-footer-at-break", "false");
				$table_omit_header_at_break = new Bool(
						"table-omit-header-at-break", "false");
				$table_omit_initial_header = new Bool(
						"table-omit-initial-header", "false");
				$table_omit_final_footer = new Bool("table-omit-final-footer",
						"false");
				$target_presentation_context = new Literal(
						"target-presentation-context",
						"use-target-processing-context");
				$target_processing_context = new Literal(
						"target-processing-context", "document-root");
				$target_stylesheet = new Literal("target-stylesheet",
						"use-normal-stylesheet");
				$text_align = new Typed("text-align", "start", 32,
						new String[] { "start", "center", "end", "justify",
								"inside", "outside", "left", "right" });
				$text_align.inheritable = true;
				((Typed) $text_align).addAlias("left", "start");
				((Typed) $text_align).addAlias("right", "end");
				$text_align_last = new Enumerated("text-align-last",
						"relative", new String[] { "relative", "start",
								"center", "end", "justify", "inside",
								"outside", "left", "right" });
				$text_align_last.inheritable = true;
				$text_altitude = new AscenderDescender("text-altitude",
						"use-font-metrics");
				$text_altitude.inheritable = true;
				$text_decoration = new TextDecoration("text-decoration", "none");
				$text_decoration.inheritable = true;
				$text_depth = new AscenderDescender("text-depth",
						"use-font-metrics");
				$text_depth.inheritable = true;
				$text_indent = new Typed("text-indent", "0pt", 12);
				$text_indent.inheritable = true;
				$text_shadow = new TextShadow("text-shadow", "none");
				$text_shadow.inheritable = true;
				$text_transform = new Enumerated("text-transform", "none",
						new String[] { "capitalize", "uppercase", "lowercase",
								"none" });
				$text_transform.inheritable = true;
				$top = new Typed("top", "auto", 44);
				$treat_as_word_space = new Bool("treat-as-word-space", "auto",
						new String[] { "auto" });
				$unicode_bidi = new UnicodeBidi("unicode-bidi", "normal");
				$visibility = new Enumerated("visibility", "visible",
						new String[] { "visible", "hidden", "collapse" });
				$visibility.inheritable = true;
				$white_space_collapse = new Bool("white-space-collapse", "true");
				$white_space_collapse.inheritable = true;
				$white_space_treatment = new Enumerated(
						"white-space-treatment",
						"ignore-if-surrounding-linefeed", (new String[] {
								"ignore", "preserve",
								"ignore-if-surrounding-linefeed",
								"ignore-if-before-linefeed",
								"ignore-if-after-linefeed" }));
				$white_space_treatment.inheritable = true;
				$widows = new Typed("widows", "2", 1);
				$widows.inheritable = true;
				$word_spacing_conditionality = new Enumerated(
						"word-spacing.conditionality", "retain", new String[] {
								"discard", "retain" });
				$word_spacing_conditionality.inheritable = true;
				$word_spacing_maximum = new Typed("word-spacing.maximum",
						"auto", 8, new String[] { "auto" });
				((Typed) $word_spacing_maximum).addAlias("normal", "auto");
				$word_spacing_maximum.inheritable = true;
				$word_spacing_minimum = new Typed("word-spacing.minimum",
						"auto", 8, new String[] { "auto" });
				((Typed) $word_spacing_minimum).addAlias("normal", "auto");
				$word_spacing_minimum.inheritable = true;
				$word_spacing_optimum = new Typed("word-spacing.optimum",
						"auto", 8, new String[] { "auto" });
				((Typed) $word_spacing_optimum).addAlias("normal", "auto");
				$word_spacing_optimum.inheritable = true;
				$word_spacing_precedence = new Typed("word-spacing.precedence",
						"0", 1, new String[] { "force" });
				$word_spacing_precedence.inheritable = true;
				$wrap_option = new Enumerated("wrap-option", "wrap",
						new String[] { "wrap", "no-wrap" });
				$wrap_option.inheritable = true;
				$writing_mode = new WritingModeSpec("writing-mode", "lr-tb");
				$writing_mode.inheritable = true;
				$writing_mode.turn = 3;
				$z_index = new Typed("z-index", "auto", 1,
						new String[] { "auto" });
				$background_content_height = new Typed(
						"background-content-height", "auto", 12,
						new String[] { "auto" });
				$background_content_width = new Typed(
						"background-content-width", "auto", 12,
						new String[] { "auto" });
				$background_scaling = new Enumerated("background-scaling",
						"uniform", new String[] { "uniform", "non-uniform" });
				$initial_destination = new Literal("initial-destination");
				$collapse_subtree = new Bool("collapse-subtree", "true");
				$leader_dot = new Literal("leader-dot", ".");
				$leader_dot.inheritable = true;
				$leader_content = new Literal("leader-content", "");
				$leader_content.inheritable = true;
				$name = new Literal("name");
				$value = new Literal("value");
				$TEXT = new Literal("TEXT");
				$TEXT.turn = -1;
				$BIDI_CLASS = new Literal("BIDI-CLASS");
				$BIDI_CLASS.turn = -1;
				$border_top_color = new AbsoluteSide("border-top-color", 2,
						"border-*-color");
				$border_top_color.turn = 13;
				$border_top_style = new AbsoluteSide("border-top-style", 2,
						"border-*-style");
				$border_top_style.turn = 13;
				$border_top_width = new AbsoluteSide("border-top-width", 2,
						"border-*-width.length");
				$border_top_width.turn = 13;
				$padding_top = new AbsoluteSide("padding-top", 2,
						"padding-*.length");
				$padding_top.turn = 13;
				$margin_top = new AbsoluteSide("margin-top", 2, "margin-*");
				$margin_top.turn = 13;
				$border_bottom_color = new AbsoluteSide("border-bottom-color",
						3, "border-*-color");
				$border_bottom_color.turn = 13;
				$border_bottom_style = new AbsoluteSide("border-bottom-style",
						3, "border-*-style");
				$border_bottom_style.turn = 13;
				$border_bottom_width = new AbsoluteSide("border-bottom-width",
						3, "border-*-width.length");
				$border_bottom_width.turn = 13;
				$padding_bottom = new AbsoluteSide("padding-bottom", 3,
						"padding-*.length");
				$padding_bottom.turn = 13;
				$margin_bottom = new AbsoluteSide("margin-bottom", 3,
						"margin-*");
				$margin_bottom.turn = 13;
				$border_left_color = new AbsoluteSide("border-left-color", 0,
						"border-*-color");
				$border_left_color.turn = 13;
				$border_left_style = new AbsoluteSide("border-left-style", 0,
						"border-*-style");
				$border_left_style.turn = 13;
				$border_left_width = new AbsoluteSide("border-left-width", 0,
						"border-*-width.length");
				$border_left_width.turn = 13;
				$padding_left = new AbsoluteSide("padding-left", 0,
						"padding-*.length");
				$padding_left.turn = 13;
				$margin_left = new AbsoluteSide("margin-left", 0, "margin-*");
				$margin_left.turn = 13;
				$border_right_color = new AbsoluteSide("border-right-color", 1,
						"border-*-color");
				$border_right_color.turn = 13;
				$border_right_style = new AbsoluteSide("border-right-style", 1,
						"border-*-style");
				$border_right_style.turn = 13;
				$border_right_width = new AbsoluteSide("border-right-width", 1,
						"border-*-width.length");
				$border_right_width.turn = 13;
				$padding_right = new AbsoluteSide("padding-right", 1,
						"padding-*.length");
				$padding_right.turn = 13;
				$margin_right = new AbsoluteSide("margin-right", 1, "margin-*");
				$margin_right.turn = 13;
				$padding_before = new Synonym("padding-before",
						"padding-before.length");
				$padding_before.turn = 11;
				$padding_after = new Synonym("padding-after",
						"padding-after.length");
				$padding_after.turn = 11;
				$padding_start = new Synonym("padding-start",
						"padding-start.length");
				$padding_start.turn = 11;
				$padding_end = new Synonym("padding-end", "padding-end.length");
				$padding_end.turn = 11;
				$border_before_width = new Synonym("border-before-width",
						"border-before-width.length");
				$border_before_width.turn = 11;
				$border_after_width = new Synonym("border-after-width",
						"border-after-width.length");
				$border_after_width.turn = 11;
				$border_start_width = new Synonym("border-start-width",
						"border-start-width.length");
				$border_start_width.turn = 11;
				$border_end_width = new Synonym("border-end-width",
						"border-end-width.length");
				$border_end_width.turn = 11;
				$line_height = new MinOptMax("line-height");
				$line_height.turn = 8;
				$space_before = new MinOptMax("space-before");
				$space_before.turn = 11;
				$space_after = new MinOptMax("space-after");
				$space_after.turn = 11;
				$space_start = new MinOptMax("space-start");
				$space_start.turn = 11;
				$space_end = new MinOptMax("space-end");
				$space_end.turn = 11;
				$leader_length = new MinOptMax("leader-length");
				$leader_length.turn = 11;
				$letter_spacing = new MinOptMax("letter-spacing");
				$letter_spacing.turn = 11;
				$word_spacing = new MinOptMax("word-spacing");
				$word_spacing.turn = 11;
				$inline_progression_dimension = new MinOptMax(
						"inline-progression-dimension");
				$inline_progression_dimension.turn = 11;
				$block_progression_dimension = new MinOptMax(
						"block-progression-dimension");
				$block_progression_dimension.turn = 11;
				$border_separation = new BpIp("border-separation");
				$border_separation.turn = 11;
				$keep_with_next = new KeepSpec("keep-with-next");
				$keep_with_next.turn = 11;
				$keep_with_previous = new KeepSpec("keep-with-previous");
				$keep_with_previous.turn = 11;
				$keep_together = new KeepSpec("keep-together");
				$keep_together.turn = 11;
				$column_width = new ColumnWidth("column-width");
				$width = new HeightWidth("width",
						"inline-progression-dimension",
						"block-progression-dimension");
				$width.turn = 10;
				$max_width = new HeightWidth("max-width",
						"inline-progression-dimension.maximum",
						"block-progression-dimension.maximum");
				$max_width.turn = 10;
				$min_width = new HeightWidth("min-width",
						"inline-progression-dimension.minimum",
						"block-progression-dimension.minimum");
				$min_width.turn = 10;
				$height = new HeightWidth("height",
						"block-progression-dimension",
						"inline-progression-dimension");
				$height.turn = 10;
				$max_height = new HeightWidth("max-height",
						"block-progression-dimension.maximum",
						"inline-progression-dimension.maximum");
				$max_height.turn = 10;
				$min_height = new HeightWidth("min-height",
						"block-progression-dimension.minimum",
						"inline-progression-dimension.minimum");
				$min_height.turn = 10;
				$border_top = new BorderSide("border-top");
				$border_top.turn = 7;
				$border_bottom = new BorderSide("border-bottom");
				$border_bottom.turn = 7;
				$border_left = new BorderSide("border-left");
				$border_left.turn = 7;
				$border_right = new BorderSide("border-right");
				$border_right.turn = 7;
				$border_width = new FourSides("border-width", "border-*-width");
				$border_width.turn = 6;
				$border_style = new FourSides("border-style", "border-*-style");
				$border_style.turn = 6;
				$border_color = new FourSides("border-color", "border-*-color");
				$border_color.turn = 6;
				$padding = new FourSides("padding", "padding-*");
				$padding.turn = 6;
				$margin = new FourSides("margin", "margin-*");
				$margin.turn = 6;
				$border = new Border("border");
				$border.turn = 5;
				$background_position = new BackgroundPosition(
						"background-position");
				$background_position.turn = 10;
				$background = new Background("background");
				$background.turn = 5;
				$page_break_after = new PageBreak("page-break-after",
						"break-after", "keep-with-next.within-column");
				$page_break_after.turn = 10;
				$page_break_before = new PageBreak("page-break-before",
						"break-before", "keep-with-previous.within-column");
				$page_break_before.turn = 10;
				$page_break_inside = new PageBreakInside("page-break-inside");
				$page_break_inside.turn = 10;
				$font = new FontSpec("font");
				$font.turn = 2;
				$position = new Position("position");
				$position.turn = 10;
				$white_space = new WhiteSpace("white-space");
				$white_space.turn = 10;
				$size = new PageSize("size");
				$size.turn = 10;
				$vertical_align = new VerticalAlign("vertical-align");
				$vertical_align.turn = 10;
				$xml_lang = new LangSpec("xml:lang");
				$xml_lang.turn = 10;
				$border_spacing = new BorderSpacing("border-spacing");
				$border_spacing.turn = 10;
				$border_after_width_collapsed = new Typed(
						"border-after-width.collapsed", "0pt", 8);
				$border_after_width_collapsed.turn = -1;
				$border_before_width_collapsed = new Typed(
						"border-before-width.collapsed", "0pt", 8);
				$border_before_width_collapsed.turn = -1;
				$border_start_width_collapsed = new Typed(
						"border-start-width.collapsed", "0pt", 8);
				$border_start_width_collapsed.turn = -1;
				$border_end_width_collapsed = new Typed(
						"border-end-width.collapsed", "0pt", 8);
				$border_end_width_collapsed.turn = -1;
				$table_identifier = new Typed("table-identifier", "-1", 1);
				$table_identifier.turn = -1;
				$table_element_class = new Enumerated("table-element-class",
						"none", new String[] { "table", "body", "header",
								"footer", "row", "column", "cell", "none" });
				$table_element_class.turn = -1;
				$body_number = new Typed("body-number", "-1", 1, new String[] {
						"header", "footer" });
				$body_number.turn = -1;
				$row_number = new Typed("row-number", "-1", 1);
				$row_number.turn = -1;
				$image_base_url = new Literal("image-base-url");
				$image_base_url.turn = -1;
				$active_state = new NoOp("active-state");
				$auto_restore = new NoOp("auto-restore");
				$starting_state = new NoOp("starting-state");
				$switch_to = new NoOp("switch-to");
				$case_name = new NoOp("case-name");
				$case_title = new NoOp("case-title");
				$asimuth = new NoOp("asimuth");
				$cue = new NoOp("cue");
				$cue_after = new NoOp("cue-after");
				$cue_before = new NoOp("cue-before");
				$elevation = new NoOp("elevation");
				$pause = new NoOp("pause");
				$pause_after = new NoOp("pause-after");
				$pause_before = new NoOp("pause-before");
				$pitch_range = new NoOp("pitch-range");
				$play_during = new NoOp("play-during");
				$richness = new NoOp("richness");
				$speak = new NoOp("speak");
				$speak_header = new NoOp("speak-header");
				$speak_numeral = new NoOp("speak-numeral");
				$speak_punctuation = new NoOp("speak-punctuation");
				$speech_rate = new NoOp("speech-rate");
				$stress = new NoOp("stress");
				$voice_family = new NoOp("voice-family");
				$volume = new NoOp("volume");
			} catch (ParserException parserexception) {
				throw new InternalException(
						"Failed to initialize attribute descriptors: "
								+ parserexception.getMessage());
			}
			initialized = true;
		}
	}
}