/*
 * Parser - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;

import com.renderx.graphics.SpotColorList;
import com.renderx.sax.InputSource;
import com.renderx.sax.NullSource;
import com.renderx.sax.SAXStorage;
import com.renderx.sax.Serializer;
import com.renderx.sax.XMLReaderFactory;
import com.renderx.util.Hashtable;
import com.renderx.util.URLSpec;
import com.renderx.util.URLUtil;
import com.renderx.util.User;
import com.renderx.xep.cmp.FO;
import com.renderx.xep.cmp.RootHandler;
import com.renderx.xep.lib.Conf;
import com.renderx.xep.lib.ConfigurationException;
import com.renderx.xep.lib.Session;
import com.renderx.xep.pre.helpers.ElementHandlerSerializer;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.AttributesImpl;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLFilterImpl;

public class Parser {
	protected final Session session;

	private static boolean initialized = false;

	public static class Test extends TestSuite {
		static XMLReader reader = null;

		static Hashtable defaults = null;

		static boolean debugmode = false;

		static Conf conf = null;

		static class ParserMarkerTest extends TestCase {
			StringBuffer xmlsource = new StringBuffer();

			Vector objs = null;

			boolean debugmode = false;

			class MyElementHandler implements ElementHandler {
				String name = null;

				boolean found = false;

				public MyElementHandler() {
					/* empty */
				}

				public void endElement(short i) {
					/* empty */
				}

				public void startElement(short i, AttList attlist) {
					if (Elem.getName(i).equals("marker"))
						found = true;
				}
			}

			ParserMarkerTest(String string) {
				super(string);
			}

			public void runTest() {
				testMarker();
			}

			protected void setUp() {
				objs = new Vector();
				try {
					BufferedReader bufferedreader = (new BufferedReader(
							new InputStreamReader(
									ClassLoader
											.getSystemResourceAsStream("com/renderx/xep/pre/TEST_DATA/marker-locations.fo"))));
					String string;
					while ((string = bufferedreader.readLine()) != null)
						xmlsource.append(string);
					bufferedreader = (new BufferedReader(
							new InputStreamReader(
									ClassLoader
											.getSystemResourceAsStream("com/renderx/xep/pre/TEST_DATA/marker-locations.txt"))));
					while ((string = bufferedreader.readLine()) != null)
						objs.add(string);
				} catch (Exception exception) {
					System.out.println("Error while reading source..."
							+ exception);
				}
			}

			public void testMarker() {
				Object object = null;
				String string = "";
				for (int i = 0; i < objs.size(); i++) {
					String string_0_ = (String) objs.elementAt(i);
					int i_1_ = xmlsource.toString().indexOf("<fo:flow");
					int i_2_ = xmlsource.substring(i_1_).indexOf(
							"<fo:" + string_0_);
					int i_3_ = xmlsource.substring(i_1_ + i_2_).indexOf(">");
					String string_4_ = (xmlsource.substring(0, i_1_ + i_2_
							+ i_3_ + 1)
							+ "<fo:marker marker-class-name=\"test\">Test</fo:marker>" + xmlsource
							.substring(i_1_ + i_2_ + i_3_ + 1));
					InputSource inputsource = new InputSource(new StringReader(
							string_4_));
					MyElementHandler myelementhandler = new MyElementHandler();
					myelementhandler.name = string_0_;
					try {
						Parser parser = new Parser(new Session(Test.conf));
						if (debugmode) {
							ElementHandlerSerializer elementhandlerserializer = (new ElementHandlerSerializer(
									new BufferedOutputStream(
											new FileOutputStream(string_0_
													+ ".parsed"))));
							parser.parse(Test.reader, inputsource,
									elementhandlerserializer);
							elementhandlerserializer.flush();
						} else
							parser.parse(Test.reader, inputsource,
									myelementhandler);
					} catch (Exception exception) {
						System.out.println("Error while parsing " + exception);
					}
					if (!debugmode && !myelementhandler.found)
						string += ("\nError! Element fo:"
								+ myelementhandler.name + " doesn't support markers...");
				}
				if (!string.equals(""))
					Assert.fail(string);
				Assert.assertTrue(true);
			}
		}

		static class ParserInheritanceTest extends TestCase {
			boolean debugmode = false;

			String xml = null;

			String name = null;

			String value = null;

			String inherited = null;

			class InheritanceElementHandler implements ElementHandler {
				String name = null;

				String val = null;

				boolean found = false;

				public InheritanceElementHandler() {
					/* empty */
				}

				public void endElement(short i) {
					/* empty */
				}

				public void startElement(short i, AttList attlist) {
					Enumeration enumeration = attlist.keys();
					while (enumeration.hasMoreElements()) {
						Attn attn = (Attn) enumeration.nextElement();
						if (!attn.name.equals("column-number")
								&& !attn.name.equals("float")
								&& !attn.name.equals("number-columns-spanned")
								&& !attn.name.equals("number-rows-spanned")
								&& !attn.name.equals("src")
								&& (!Elem.getName(i).equals("root") || !attn.name
										.equals("media-usage"))
								&& (attn.name.equals(name) || attn.name
										.startsWith(name + "."))
								&& (!attn.name.startsWith("border-")
										|| !attn.name.endsWith("-width.length")
										|| Test.defaults.get(attn.name) == null || !Test.defaults
										.get(attn.name).equals(
												attlist.get(attn).toString()))
								&& (!attn.name.equals("content-type")
										|| !Elem.getName(i).equals(
												"external-graphic") || attlist
										.get(attn).toString().equals(val)))
							found = true;
					}
				}
			}

			ParserInheritanceTest(String string) {
				super(string);
			}

			public void runTest() {
				testInheritance();
			}

			public void testInheritance() {
				Object object = null;
				String string = "";
				InputSource inputsource = new InputSource(new StringReader(xml));
				try {
					inputsource.setSystemId(URLUtil.toURL(new File("."))
							.toExternalForm());
				} catch (Exception exception) {
					System.out.println("Cannot set source document SystemID "
							+ exception);
				}
				InheritanceElementHandler inheritanceelementhandler = new InheritanceElementHandler();
				inheritanceelementhandler.name = name;
				inheritanceelementhandler.val = value;
				try {
					Parser parser = new Parser(new Session(Test.conf));
					if (debugmode) {
						System.err.println("Dumping parsed file to " + name
								+ ".parsed");
						FileOutputStream fileoutputstream = new FileOutputStream(
								name + ".parsed");
						BufferedOutputStream bufferedoutputstream = new BufferedOutputStream(
								fileoutputstream);
						ElementHandlerSerializer elementhandlerserializer = (new ElementHandlerSerializer(
								bufferedoutputstream));
						parser.parse(org.xml.sax.helpers.XMLReaderFactory
								.createXMLReader(), inputsource,
								elementhandlerserializer);
						elementhandlerserializer.flush();
						bufferedoutputstream.flush();
						fileoutputstream.close();
					} else
						parser.parse(org.xml.sax.helpers.XMLReaderFactory
								.createXMLReader(), inputsource,
								inheritanceelementhandler);
				} catch (Exception exception) {
					System.out.println("Error while parsing " + exception);
					exception.printStackTrace(System.err);
				}
				if (!debugmode && inheritanceelementhandler.found
						&& inherited.equals("no"))
					Assert.fail("Extra inheritance error! Property " + name
							+ " was inherited, but it violates the spec!");
				if (!debugmode && !inheritanceelementhandler.found
						&& inherited.equals("yes"))
					Assert
							.fail("Missing inheritance error! Property "
									+ name
									+ " was not inherited, but it must be inherited according to spec!");
				Assert.assertTrue(true);
			}
		}

		static class ParserAttributesTest extends TestCase {
			Hashtable defaults = null;

			boolean debugmode = false;

			SAXStorage template = null;

			TestAttribute attr = null;

			String testName = null;

			class MyElementHandler implements ElementHandler {
				TestAttribute attr = null;

				boolean scipflag = true;

				public MyElementHandler() {
					/* empty */
				}

				public void endElement(short i) {
					/* empty */
				}

				public void startElement(short i, AttList attlist) {
					if (Elem.getName(i).equals("block") && scipflag == true)
						scipflag = false;
					else if (Elem.getName(i).equals(attr.element)) {
						String string = ("\nElement: " + Elem.getName(i)
								+ " Source attribute: " + attr.name + "=\""
								+ attr.value + "\"\nProperties found: "
								+ attlist.toString() + "\nExpected properties:\n ");
						for (int i_5_ = 0; i_5_ < attr.props.size(); i_5_++) {
							String[] strings = (String[]) attr.props
									.elementAt(i_5_);
							string += strings[0] + "=\"" + strings[1]
									+ "\";\n ";
							try {
								Attn attn = Attn.byName(strings[0]);
								if (attlist.containsKey(attn)) {
									if (strings[0].equals("background-image")) {
										String string_6_ = strings[1];
										String string_7_ = attlist.get(attn)
												.toString();
										File file = new File(string_6_);
										File file_8_ = new File(string_7_);
										try {
											string_6_ = file.getCanonicalPath();
											string_7_ = file_8_
													.getCanonicalPath();
										} catch (Exception exception) {
											Assert
													.fail("Can't calculate canonical path...");
										}
										Assert
												.assertEquals(
														("Property "
																+ strings[0]
																+ " corresponding to attribute "
																+ attr.name
																+ "="
																+ attr.value + " found in output, but has wrong value.\n"),
														string_6_, string_7_);
									} else
										Assert
												.assertEquals(
														("Property "
																+ strings[0]
																+ " corresponding to attribute "
																+ attr.name
																+ "="
																+ attr.value + " found in output, but has wrong value.\n"),
														strings[1], attlist
																.get(attn)
																.toString());
								} else if (defaults.get(strings[0]) == null
										|| !defaults.get(strings[0]).equals(
												strings[1]))
									Assert.fail("Property " + strings[0]
											+ " (value " + strings[1]
											+ ") corresponding to attribute "
											+ attr.name + "=" + attr.value
											+ " wasn't found in output."
											+ string + "\n");
							} catch (ParserException parserexception) {
								Assert.fail("Invalid attribute name! "
										+ parserexception);
							}
						}
					}
				}
			}

			class MyXMLFilter extends XMLFilterImpl {
				TestAttribute attr = null;

				AttributesImpl newatts = null;

				public void startElement(String string, String string_9_,
						String string_10_, Attributes attributes)
						throws SAXException {
					if (string_9_.equals(attr.element)) {
						newatts = new AttributesImpl();
						for (int i = 0; i < attributes.getLength(); i++) {
							String string_11_ = attributes.getQName(i);
							if (string_11_ == null
									|| !string_11_.startsWith("xmlns:"))
								newatts.addAttribute(attributes.getURI(i),
										attributes.getLocalName(i), attributes
												.getQName(i), attributes
												.getType(i), attributes
												.getValue(i));
						}
						newatts.addAttribute("", attr.name, attr.name, "CDATA",
								attr.value);
						super.startElement(string, string_9_, string_10_,
								newatts);
					} else
						super.startElement(string, string_9_, string_10_,
								attributes);
				}
			}

			ParserAttributesTest(String string) {
				super(string);
				testName = string;
			}

			public void runTest() {
				testAttributes();
			}

			public void testAttributes() {
				MyElementHandler myelementhandler = new MyElementHandler();
				MyXMLFilter myxmlfilter = new MyXMLFilter();
				myxmlfilter.attr = attr;
				myelementhandler.attr = attr;
				myxmlfilter.setParent(template);
				Parser parser = null;
				try {
					parser = new Parser(new Session(Test.conf));
				} catch (ConfigurationException configurationexception) {
					System.out
							.println("Configuration problems while initializing parser"
									+ configurationexception);
				}
				try {
					if (debugmode) {
						String string = testName + ".parsed";
						if (string.indexOf(":") != -1)
							string = string.substring(string.indexOf(":") + 1);
						Serializer serializer = new Serializer(string
								+ "-source");
						myxmlfilter.setContentHandler(serializer);
						myxmlfilter.parse(new NullSource());
						ElementHandlerSerializer elementhandlerserializer = (new ElementHandlerSerializer(
								new BufferedOutputStream(new FileOutputStream(
										string))));
						parser.parse(myxmlfilter, new NullSource(),
								elementhandlerserializer);
						elementhandlerserializer.flush();
					} else
						parser.parse(myxmlfilter, new NullSource(),
								myelementhandler);
				} catch (Exception exception) {
					System.out.println("Error while parsing " + exception);
					exception.printStackTrace(System.out);
					Assert.fail("Error while parsing " + exception);
				}
			}
		}

		static class TestAttribute {
			String name;

			String value;

			String element;

			Vector props;

			TestAttribute() {
				name = value = element = "";
				props = new Vector();
			}
		}

		static class MyContentHandler extends DefaultHandler {
			Vector attributes = new Vector();

			TestAttribute attr = null;

			public void startElement(String string, String string_12_,
					String string_13_, Attributes attributes)
					throws SAXException {
				if (string_12_.equals("attribute")) {
					attr = new TestAttribute();
					attr.name = attributes.getValue("name");
					attr.value = attributes.getValue("value");
					attr.element = attributes.getValue("element");
				} else if (string_12_.equals("property"))
					attr.props.add(new String[] { attributes.getValue("name"),
							attributes.getValue("value") });
			}

			public void endElement(String string, String string_14_,
					String string_15_) throws SAXException {
				if (string_14_.equals("attribute"))
					attributes.add(attr);
			}
		}

		public static void main(String[] strings) {
			if (strings.length == 1 && strings[0].equals("-debug"))
				debugmode = true;
			TestRunner.run(suite());
		}

		public static junit.framework.Test suite() {
			SAXStorage saxstorage = null;
			Vector vector = null;
			Object object = null;
			Hashtable hashtable = new Hashtable();
			hashtable.put("margin.xml", "Margin shorthand TestSuite");
			hashtable.put("padding.xml", "Padding shorthand TestSuite");
			hashtable.put("border.xml", "Border shorthands TestSuite");
			hashtable.put("pagebreak.xml", "Page-break shorthands TestSuite");
			hashtable.put("whitespace.xml", "White-space shorthand TestSuite");
			hashtable.put("font.xml", "Font shorthand TestSuite");
			hashtable.put("background.xml", "Background shorthand TestSuite");
			hashtable.put("expressions.xml", "Basic expressions TestSuite");
			hashtable.put("composite.xml", "Composite properties TestSuite");
			hashtable.put("shorthands.xml", "Basic shorthands TestSuite");
			TestSuite testsuite = new TestSuite("Parser TestSuite");
			try {
				initParser();
				saxstorage = parseTemplate("testing.fo");
			} catch (Exception exception) {
				System.out.println("Error during setup! " + exception);
			}
			try {
				conf = new Conf();
			} catch (ConfigurationException configurationexception) {
				System.out
						.println("Configuration problems while initializing parser"
								+ configurationexception);
			}
			Enumeration enumeration = hashtable.keys();
			while (enumeration.hasMoreElements()) {
				String string = (String) enumeration.nextElement();
				try {
					vector = parseAttributes(string);
				} catch (Exception exception) {
					System.out.println("Error during parsing test data! File: "
							+ string + exception);
				}
				TestSuite testsuite_16_ = new TestSuite((String) hashtable
						.get(string));
				for (int i = 0; i < vector.size(); i++) {
					TestAttribute testattribute = (TestAttribute) vector
							.elementAt(i);
					ParserAttributesTest parserattributestest = new ParserAttributesTest(
							testattribute.name + "#" + i);
					parserattributestest.debugmode = debugmode;
					parserattributestest.defaults = defaults;
					parserattributestest.attr = testattribute;
					parserattributestest.template = saxstorage;
					testsuite_16_.addTest(parserattributestest);
				}
				testsuite.addTest(testsuite_16_);
			}
			StringBuffer stringbuffer = new StringBuffer();
			Object object_17_ = null;
			TestSuite testsuite_18_ = new TestSuite(
					"Parser Inheritance TestSuite");
			Vector vector_19_ = new Vector();
			try {
				BufferedReader bufferedreader = (new BufferedReader(
						new InputStreamReader(
								ClassLoader
										.getSystemResourceAsStream("com/renderx/xep/pre/TEST_DATA/testing.fo"))));
				String string;
				while ((string = bufferedreader.readLine()) != null)
					stringbuffer.append(string);
				bufferedreader = (new BufferedReader(
						new InputStreamReader(
								ClassLoader
										.getSystemResourceAsStream("com/renderx/xep/pre/TEST_DATA/props.txt"))));
				while ((string = bufferedreader.readLine()) != null) {
					String string_20_ = string
							.substring(string.indexOf(" ") + 1);
					vector_19_.add(new String[] {
							string.substring(0, string.indexOf(" ")),
							string_20_.substring(string_20_.indexOf(" ") + 1),
							string_20_.substring(0, string_20_.indexOf(" ")) });
				}
			} catch (Exception exception) {
				System.out
						.println("Error! Can't find properties! " + exception);
			}
			for (int i = 0; i < vector_19_.size(); i++) {
				String[] strings = (String[]) vector_19_.elementAt(i);
				String string = stringbuffer.toString();
				string = (string.substring(0, string.indexOf("<fo:root") + 9)
						+ strings[0] + "=\"" + strings[1] + "\" " + string
						.substring(string.indexOf("<fo:root") + 9));
				ParserInheritanceTest parserinheritancetest = new ParserInheritanceTest(
						strings[0]);
				parserinheritancetest.debugmode = debugmode;
				parserinheritancetest.xml = string;
				parserinheritancetest.name = strings[0];
				parserinheritancetest.value = strings[1];
				parserinheritancetest.inherited = strings[2];
				testsuite_18_.addTest(parserinheritancetest);
			}
			testsuite.addTest(testsuite_18_);
			ParserMarkerTest parsermarkertest = new ParserMarkerTest(
					"Parser Marker Test");
			parsermarkertest.debugmode = debugmode;
			testsuite.addTest(parsermarkertest);
			return testsuite;
		}

		static void initParser() throws SAXException,
				ParserConfigurationException, IOException {
			if (User.getProperty("org.xml.sax.driver") == null) {
				Properties properties = User.getProperties();
				properties.put("org.xml.sax.driver",
						"com.icl.saxon.aelfred.SAXDriver");
				User.setProperties(properties);
			}
			reader = org.xml.sax.helpers.XMLReaderFactory.createXMLReader();
			Vector vector = parseAttributes("defaults.xml");
			defaults = new Hashtable();
			for (int i = 0; i < vector.size(); i++) {
				TestAttribute testattribute = (TestAttribute) vector
						.elementAt(i);
				defaults.put(testattribute.name, testattribute.value);
			}
		}

		static SAXStorage parseTemplate(String string) throws SAXException,
				IOException {
			SAXStorage saxstorage = new SAXStorage();
			reader.setContentHandler(saxstorage);
			reader.parse(new org.xml.sax.InputSource(ClassLoader
					.getSystemResourceAsStream("com/renderx/xep/pre/TEST_DATA/"
							+ string)));
			return saxstorage;
		}

		static Vector parseAttributes(String string) throws SAXException,
				IOException {
			MyContentHandler mycontenthandler = new MyContentHandler();
			reader.setContentHandler(mycontenthandler);
			reader.parse(new org.xml.sax.InputSource(ClassLoader
					.getSystemResourceAsStream("com/renderx/xep/pre/TEST_DATA/"
							+ string)));
			return mycontenthandler.attributes;
		}
	}

	public Parser() throws ConfigurationException {
		this(new Session());
	}

	public Parser(Session session) throws ConfigurationException {
		this.session = session;
		init();
	}

	public static synchronized void init() throws ConfigurationException {
		if (!initialized) {
			SpotColorList.init();
			Bidi.init();
			Attr.init();
			Attr.init();
			AttributeParser.init();
			Attn.init();
			Elem.init();
			WritingMode.init();
			initialized = true;
		}
	}

	void parse(XMLReader xmlreader, org.xml.sax.InputSource inputsource,
			ElementHandler elementhandler) throws SAXException, IOException {
		xmlreader.setContentHandler(createContentHandler(inputsource
				.getSystemId(), elementhandler));
		xmlreader.parse(inputsource);
	}

	ContentHandler createContentHandler(String string,
			ElementHandler elementhandler) {
		return new FOPreprocessor(
				new FOHandler(string, elementhandler, session), session);
	}

	public ContentHandler createContentHandler(String string, FO fo) {
		return createContentHandler(string, new RootHandler(fo, session));
	}

	public static void main(String[] strings) throws Exception {
		String string = null;
		if (strings.length == 1) {
			string = strings[0] + ".parsed";
			System.out.println("Dumping parser output to " + string);
		} else if (strings.length == 2)
			string = strings[1];
		else {
			System.out
					.println("Usage: com.renderx.xep.pre.Parser <input file name> [<output file name>]");
			System.exit(0);
		}
		init();
		Parser parser = new Parser();
		XMLReader xmlreader = XMLReaderFactory.createXMLReader();
		InputSource inputsource = new InputSource(new URLSpec(strings[0]));
		ElementHandlerSerializer elementhandlerserializer = (new ElementHandlerSerializer(
				new BufferedOutputStream(new FileOutputStream(string))));
		try {
			parser.parse(xmlreader, inputsource, elementhandlerserializer);
		} finally {
			elementhandlerserializer.flush();
		}
	}
}