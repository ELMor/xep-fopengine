/*
 * Token - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre;

import java.io.PrintStream;

public class Token {
	public int type = 0;

	public Attr value = null;

	public static final int INVALID_TYPE = 0;

	public static final int END_OF_DATA = 1;

	public static final int LEFT_PARENTHESIS = 2;

	public static final int RIGHT_PARENTHESIS = 4;

	public static final int COMMA = 8;

	public static final int PLUS = 16;

	public static final int MINUS = 32;

	public static final int ASTERISK = 64;

	public static final int SLASH = 128;

	public static final int DIVIDE = 256;

	public static final int MODULO = 512;

	public static final int UNARY_PLUS = 1024;

	public static final int UNARY_MINUS = 2048;

	public static final int FUNCTOR = 4096;

	public static final int EXPRESSION = 8192;

	public Token() {
		/* empty */
	}

	public Token(int i) {
		type = i;
	}

	public Token(int i, Attr attr) {
		type = i;
		value = attr;
	}

	public String getTypeName() {
		switch (type) {
		case 0:
			return "<INVALID TYPE>";
		case 1:
			return "END OF DATA";
		case 4096:
			return "FUNCTOR";
		case 2:
			return "LEFT PARENTHESIS";
		case 4:
			return "RIGHT PARENTHESIS";
		case 8:
			return "COMMA";
		case 16:
			return "PLUS";
		case 32:
			return "MINUS";
		case 64:
			return "ASTERISK";
		case 128:
			return "SLASH";
		case 256:
			return "DIVIDE";
		case 512:
			return "MODULO";
		case 1024:
			return "UNARY PLUS";
		case 2048:
			return "UNARY MINUS";
		case 8192:
			return value.getTypeName();
		default:
			return "<BAD EXPRESSION TYPE>";
		}
	}

	public void Dump(PrintStream printstream) {
		Dump(printstream, "");
	}

	public void Dump(PrintStream printstream, String string) {
		printstream.print(string + getTypeName());
		if (type == 8192 || type == 4096)
			printstream.print(": " + value.toString());
		printstream.println();
	}
}