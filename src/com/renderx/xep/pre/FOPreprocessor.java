/*
 * FOPreprocessor - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.Enumeration;

import com.renderx.sax.InputSource;
import com.renderx.sax.SAXStorage;
import com.renderx.sax.Serializer;
import com.renderx.sax.XMLReaderFactory;
import com.renderx.util.Base64OutputStream;
import com.renderx.util.DefaultErrorHandler;
import com.renderx.util.ErrorHandler;
import com.renderx.util.Hashtable;
import com.renderx.xep.lib.InternalException;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.AttributesImpl;

public class FOPreprocessor implements ContentHandler {
	public static final String GHOST_MARKER_PREFIX = "[ghost]";

	private final ContentHandler parent;

	private ContentHandler handler = null;

	private final ErrorHandler errorHandler;

	private int skip = 0;

	private int treeDepth = 0;

	private final StringBuffer pcdata = new StringBuffer();

	private int storeMarker = 0;

	private SAXStorage markerData = null;

	private final Hashtable markerTable = new Hashtable();

	private final Hashtable markerHistory = new Hashtable();

	private String markerClassName = null;

	private int storeImage = 0;

	private Serializer imageData = null;

	private StringWriter imageWriter = null;

	private Base64OutputStream imageFilter = null;

	private AttributesImpl imageAttributes = null;

	private int storeLeader = 0;

	private StringBuffer leaderData = null;

	private AttributesImpl leaderAttributes = null;

	private boolean multiSwitch = false;

	private boolean emptyRowGroup = false;

	private boolean emptyPageIndex = false;

	private String refKey = null;

	private static final Attributes emptyAttributes = new AttributesImpl();

	private static final Attributes ltrOverrideAttributes = new AttributesImpl();

	private static boolean initialized;

	private static final String generateId(int i) {
		return "[" + i + "]";
	}

	public static synchronized void init() throws InternalException {
		if (!initialized) {
			AttributeParser.init();
			Elem.init();
			initialized = true;
		}
	}

	public FOPreprocessor(ContentHandler contenthandler,
			ErrorHandler errorhandler) {
		handler = parent = contenthandler;
		errorHandler = errorhandler;
	}

	private String makeQName(String string, String string_0_) {
		if (string_0_ == null)
			return null;
		int i = string_0_.indexOf(':');
		if (i == -1)
			return string;
		return string_0_.substring(0, i + 1) + string;
	}

	private void flushText() throws SAXException {
		int i = pcdata.length();
		if (i > 0) {
			char[] cs = new char[i];
			pcdata.getChars(0, i, cs, 0);
			handler.characters(cs, 0, i);
			pcdata.setLength(0);
		}
	}

	public void startElement(String string, String string_1_, String string_2_,
			Attributes attributes) throws SAXException {
		treeDepth++;
		if (skip > 0)
			skip++;
		else if (storeLeader > 0)
			storeLeader++;
		else if (storeImage > 0) {
			storeImage++;
			imageData.startElement(string, string_1_, string_2_, attributes);
		} else {
			flushText();
			if (string == null || string.length() == 0) {
				errorHandler
						.warning("Invalid element in anonymous namespace: '"
								+ string_1_ + "'; element skipped");
				skip = 1;
			} else {
				Elem.Descriptor descriptor = Elem.getDescriptor(string_1_);
				if (descriptor == null || !string.equals(descriptor.nsuri)) {
					if ("http://www.w3.org/1999/XSL/Format".equals(string))
						errorHandler.warning("Unknown XSL-FO element: '"
								+ string_1_ + "'; element skipped");
					else if ("http://www.renderx.com/XSL/Extensions"
							.equals(string))
						errorHandler
								.warning("Invalid RenderX extension element: '"
										+ string_1_ + "'; element skipped");
					skip = 1;
				} else {
					switch (descriptor.id) {
					case 55:
						skip = 1;
						break;
					case 53:
						if (!multiSwitch)
							skip = 1;
						else
							handler.startElement(
									"http://www.w3.org/1999/XSL/Format",
									"wrapper", makeQName("wrapper", string_2_),
									attributes);
						break;
					case 52:
					case 54:
						handler.startElement(
								"http://www.w3.org/1999/XSL/Format", "wrapper",
								makeQName("wrapper", string_2_), attributes);
						break;
					case 23: {
						String string_3_ = attributes.getValue("character");
						if (string_3_ != null && string_3_.length() == 1) {
							handler.startElement(
									"http://www.w3.org/1999/XSL/Format",
									"inline", makeQName("inline", string_2_),
									attributes);
							handler.characters(string_3_.toCharArray(), 0,
									string_3_.length());
							handler.endElement(
									"http://www.w3.org/1999/XSL/Format",
									"inline", makeQName("inline", string_2_));
						}
						skip = 1;
						break;
					}
					case 51: {
						String string_4_ = attributes
								.getValue("retrieve-class-name");
						if (string_4_ == null)
							skip = 1;
						else {
							Integer integer = (Integer) markerTable
									.get(string_4_);
							if (integer == null)
								integer = new Integer(0);
							markerTable.put(string_4_, new Integer(integer
									.intValue() + 1));
							String string_5_ = generateId(integer.intValue())
									+ string_4_;
							AttributesImpl attributesimpl = new AttributesImpl(
									attributes);
							attributesimpl
									.setValue(attributes
											.getIndex("retrieve-class-name"),
											string_5_);
							handler.startElement(
									"http://www.w3.org/1999/XSL/Format",
									string_1_, string_2_, attributesimpl);
							handler.endElement(
									"http://www.w3.org/1999/XSL/Format",
									string_1_, string_2_);
							skip = 1;
						}
						break;
					}
					case 50:
						if (storeMarker > 0)
							skip = 1;
						else {
							markerClassName = attributes
									.getValue("marker-class-name");
							if (markerClassName == null)
								skip = 1;
							else {
								storeMarker = 1;
								handler = markerData = new SAXStorage();
								handler.startElement(
										"http://www.w3.org/1999/XSL/Format",
										"wrapper", makeQName("wrapper",
												string_2_), attributes);
							}
						}
						break;
					case 17:
					case 18:
					case 19: {
						handler.startElement(
								"http://www.w3.org/1999/XSL/Format", string_1_,
								string_2_, attributes);
						handler.startElement(
								"http://www.w3.org/1999/XSL/Format", "block",
								makeQName("block", string_2_), emptyAttributes);
						Enumeration enumeration = markerHistory.keys();
						while (enumeration.hasMoreElements()) {
							String string_6_ = (String) enumeration
									.nextElement();
							SAXStorage saxstorage = (SAXStorage) markerHistory
									.get(string_6_);
							proliferateMarker(string_6_, makeQName("marker",
									string_2_), saxstorage, true);
						}
						break;
					}
					case 108:
						handler.startElement(
								"http://www.w3.org/1999/XSL/Format", "inline",
								makeQName("inline", string_2_), attributes);
						handler.startElement(
								"http://www.w3.org/1999/XSL/Format", string_1_,
								string_2_, emptyAttributes);
						refKey = attributes.getValue("ref-key");
						break;
					case 30:
						handler.startElement(
								"http://www.w3.org/1999/XSL/Format",
								"bidi-override", makeQName("bidi-override",
										string_2_), ltrOverrideAttributes);
						handler.startElement(
								"http://www.w3.org/1999/XSL/Format", "inline",
								makeQName("inline", string_2_), attributes);
						handler.startElement(
								"http://www.w3.org/1999/XSL/Format", string_1_,
								string_2_, emptyAttributes);
						handler.endElement("http://www.w3.org/1999/XSL/Format",
								string_1_, string_2_);
						handler.endElement("http://www.w3.org/1999/XSL/Format",
								"inline", makeQName("inline", string_2_));
						handler.endElement("http://www.w3.org/1999/XSL/Format",
								"bidi-override", makeQName("bidi-override",
										string_2_));
						skip = 1;
						break;
					case 31:
					case 201: {
						handler.startElement(
								"http://www.w3.org/1999/XSL/Format",
								"bidi-override", makeQName("bidi-override",
										string_2_), ltrOverrideAttributes);
						handler.startElement(
								"http://www.w3.org/1999/XSL/Format", "inline",
								makeQName("inline", string_2_), attributes);
						String string_7_ = attributes.getValue("ref-id");
						if (string_7_ == null)
							handler.startElement(
									"http://www.w3.org/1999/XSL/Format",
									string_1_, string_2_, emptyAttributes);
						else {
							AttributesImpl attributesimpl = new AttributesImpl();
							attributesimpl.addAttribute("", "ref-id", "ref-id",
									"CDATA", string_7_);
							handler.startElement(
									"http://www.w3.org/1999/XSL/Format",
									string_1_, string_2_, attributesimpl);
						}
						handler.endElement("http://www.w3.org/1999/XSL/Format",
								string_1_, string_2_);
						handler.endElement("http://www.w3.org/1999/XSL/Format",
								"inline", makeQName("inline", string_2_));
						handler.endElement("http://www.w3.org/1999/XSL/Format",
								"bidi-override", makeQName("bidi-override",
										string_2_));
						skip = 1;
						break;
					}
					case 29:
						leaderData = new StringBuffer();
						leaderAttributes = new AttributesImpl(attributes);
						storeLeader = 1;
						break;
					case 26:
						imageWriter = new StringWriter();
						imageFilter = new Base64OutputStream(imageWriter);
						try {
							imageData = new Serializer(imageFilter);
						} catch (IOException ioexception) {
							throw new InternalException(
									"Internal error: cannot create a serializer for an embedded image: "
											+ ioexception.toString());
						}
						imageAttributes = new AttributesImpl(attributes);
						storeImage = 1;
						imageData.startDocument();
						break;
					case 6:
					case 7:
					case 9:
					case 11:
					case 12:
					case 13:
					case 14:
					case 15:
					case 24:
					case 25:
					case 35:
					case 105:
					case 107:
					case 109:
					case 110:
					case 111:
					case 112:
						handler.startElement(
								"http://www.w3.org/1999/XSL/Format", string_1_,
								string_2_, attributes);
						handler.endElement("http://www.w3.org/1999/XSL/Format",
								string_1_, string_2_);
						skip = 1;
						break;
					default:
						handler.startElement(
								"http://www.w3.org/1999/XSL/Format", string_1_,
								string_2_, attributes);
					}
					multiSwitch = descriptor.id == 52;
					emptyRowGroup = (descriptor.id == 38 || descriptor.id == 36 || descriptor.id == 37);
					emptyPageIndex = descriptor.id == 108;
					if (storeMarker > 0 && descriptor.id != 50 && skip == 0
							&& storeLeader == 0 && storeImage == 0)
						storeMarker++;
				}
			}
		}
	}

	public void endElement(String string, String string_8_, String string_9_)
			throws SAXException {
		treeDepth--;
		if (skip > 0)
			skip--;
		else if (storeLeader > 0) {
			storeLeader--;
			if (storeLeader == 0) {
				String string_10_ = leaderData.toString();
				if (string_10_ != null && string_10_.length() > 0)
					leaderAttributes.addAttribute("", "leader-content",
							"leader-content", "CDATA", string_10_);
				handler.startElement("http://www.w3.org/1999/XSL/Format",
						string_8_, string_9_, leaderAttributes);
				handler.endElement("http://www.w3.org/1999/XSL/Format",
						string_8_, string_9_);
				leaderData = null;
				leaderAttributes = null;
			}
		} else if (storeImage > 0) {
			storeImage--;
			if (storeImage == 0) {
				imageData.endDocument();
				try {
					imageFilter.close();
					imageWriter.close();
				} catch (IOException ioexception) {
					throw new InternalException(ioexception.toString());
				}
				imageAttributes
						.addAttribute("", "src", "src", "CDATA",
								("data:image/svg+xml;base64," + imageWriter
										.toString()));
				handler.startElement("http://www.w3.org/1999/XSL/Format",
						"external-graphic", makeQName("external-graphic",
								string_9_), imageAttributes);
				handler.endElement("http://www.w3.org/1999/XSL/Format",
						"external-graphic", makeQName("external-graphic",
								string_9_));
				imageData = null;
				imageFilter = null;
				imageWriter = null;
			} else
				imageData.endElement(string, string_8_, string_9_);
		} else {
			flushText();
			if (storeMarker > 0) {
				storeMarker--;
				if (storeMarker == 0) {
					handler.endElement("http://www.w3.org/1999/XSL/Format",
							"wrapper", makeQName("wrapper", string_9_));
					handler = parent;
					markerHistory.put(markerClassName, markerData);
					proliferateMarker(markerClassName, string_9_, markerData,
							false);
					markerData = null;
					markerClassName = null;
					return;
				}
			}
			Elem.Descriptor descriptor = Elem.getDescriptor(string_8_);
			if (descriptor == null)
				throw new InternalException(
						"Nesting violation in preprocessor: encountered closing tag for element '"
								+ string_8_ + "', namespace '" + string + "'");
			switch (descriptor.id) {
			case 17:
			case 18:
			case 19:
				handler.endElement("http://www.w3.org/1999/XSL/Format",
						"block", makeQName("block", string_9_));
				handler.endElement("http://www.w3.org/1999/XSL/Format",
						string_8_, string_9_);
				break;
			case 108:
				if (emptyPageIndex && refKey != null) {
					AttributesImpl attributesimpl = new AttributesImpl();
					attributesimpl.addAttribute("", "ref-key", "ref-key",
							"CDATA", refKey);
					handler.startElement("http://www.w3.org/1999/XSL/Format",
							"index-item", makeQName("index-item", string_9_),
							attributesimpl);
					handler.endElement("http://www.w3.org/1999/XSL/Format",
							"index-item", makeQName("index-item", string_9_));
				}
				handler.endElement("http://www.w3.org/1999/XSL/Format",
						string_8_, string_9_);
				handler.endElement("http://www.w3.org/1999/XSL/Format",
						"inline", makeQName("inline", string_9_));
				break;
			case 50:
			case 52:
			case 53:
			case 54:
				handler.endElement("http://www.w3.org/1999/XSL/Format",
						"wrapper", makeQName("wrapper", string_9_));
				handler.endElement("http://www.w3.org/1999/XSL/Format",
						string_8_, string_9_);
				break;
			case 16:
				markerTable.clear();
				handler.endElement("http://www.w3.org/1999/XSL/Format",
						string_8_, string_9_);
				break;
			case 36:
			case 37:
			case 38:
				if (emptyRowGroup) {
					handler.startElement("http://www.w3.org/1999/XSL/Format",
							"table-row", makeQName("table-row", string_9_),
							emptyAttributes);
					handler.startElement("http://www.w3.org/1999/XSL/Format",
							"table-cell", makeQName("table-cell", string_9_),
							emptyAttributes);
					handler.startElement("http://www.w3.org/1999/XSL/Format",
							"block", makeQName("block", string_9_),
							emptyAttributes);
					handler.endElement("http://www.w3.org/1999/XSL/Format",
							"block", makeQName("block", string_9_));
					handler.endElement("http://www.w3.org/1999/XSL/Format",
							"table-cell", makeQName("table-cell", string_9_));
					handler.endElement("http://www.w3.org/1999/XSL/Format",
							"table-row", makeQName("table-row", string_9_));
					emptyRowGroup = false;
				}
				handler.endElement("http://www.w3.org/1999/XSL/Format",
						string_8_, string_9_);
				break;
			default:
				handler.endElement("http://www.w3.org/1999/XSL/Format",
						string_8_, string_9_);
			}
		}
	}

	public void characters(char[] cs, int i, int i_11_) throws SAXException {
		if (skip <= 0 && treeDepth != 0) {
			if (storeLeader > 0)
				leaderData.append(cs, i, i_11_);
			else if (storeImage > 0)
				imageData.characters(cs, i, i_11_);
			else
				pcdata.append(cs, i, i_11_);
		}
	}

	public void startDocument() throws SAXException {
		handler.startDocument();
		pcdata.setLength(0);
	}

	public void endDocument() throws SAXException {
		parent.endDocument();
	}

	public void processingInstruction(String string, String string_12_)
			throws SAXException {
		if (skip <= 0) {
			if (storeImage > 0)
				imageData.processingInstruction(string, string_12_);
			else if (storeLeader <= 0) {
				flushText();
				handler.processingInstruction(string, string_12_);
			}
		}
	}

	public void ignorableWhitespace(char[] cs, int i, int i_13_)
			throws SAXException {
		if (skip <= 0) {
			if (storeLeader > 0)
				leaderData.append(cs, i, i_13_);
			else if (storeImage > 0)
				imageData.ignorableWhitespace(cs, i, i_13_);
			else {
				flushText();
				handler.ignorableWhitespace(cs, i, i_13_);
			}
		}
	}

	public void skippedEntity(String string) throws SAXException {
		if (skip <= 0) {
			if (storeImage > 0)
				imageData.skippedEntity(string);
			else if (storeLeader <= 0) {
				flushText();
				handler.skippedEntity(string);
			}
		}
	}

	public void setDocumentLocator(Locator locator) {
		handler.setDocumentLocator(locator);
	}

	public void startPrefixMapping(String string, String string_14_)
			throws SAXException {
		if (skip <= 0) {
			if (storeImage > 0)
				imageData.startPrefixMapping(string, string_14_);
			else if (storeLeader <= 0) {
				flushText();
				if ("http://www.renderx.com/XSL/Extensions".equals(string_14_))
					string_14_ = "http://www.w3.org/1999/XSL/Format";
				handler.startPrefixMapping(string, string_14_);
			}
		}
	}

	public void endPrefixMapping(String string) throws SAXException {
		if (skip <= 0) {
			if (storeImage > 0)
				imageData.endPrefixMapping(string);
			else if (storeLeader <= 0) {
				flushText();
				handler.endPrefixMapping(string);
			}
		}
	}

	private void proliferateMarker(String string, String string_15_,
			SAXStorage saxstorage, boolean bool) throws SAXException {
		Integer integer = (Integer) markerTable.get(string);
		if (integer != null) {
			int i = integer.intValue();
			saxstorage.setContentHandler(handler);
			String string_16_ = bool ? "[ghost]" : "";
			for (int i_17_ = 0; i_17_ < integer.intValue(); i_17_++) {
				AttributesImpl attributesimpl = new AttributesImpl();
				attributesimpl.addAttribute("", "marker-class-name",
						"marker-class-name", "CDATA", (string_16_
								+ generateId(i_17_) + string));
				handler.startElement("http://www.w3.org/1999/XSL/Format",
						"marker", string_15_, attributesimpl);
				saxstorage.play();
				handler.endElement("http://www.w3.org/1999/XSL/Format",
						"marker", string_15_);
			}
		}
	}

	public static void main(String[] strings) {
		OutputStream outputstream = System.out;
		if (strings.length < 1) {
			System.err
					.println("call: java com.renderx.xep.pre.FOPreprocessor source_xml [target_xml]\n");
			System.exit(1);
		}
		try {
			init();
			if (strings.length > 1)
				outputstream = new FileOutputStream(strings[1]);
			XMLReader xmlreader = XMLReaderFactory.createXMLReader();
			xmlreader.setContentHandler(new FOPreprocessor(new Serializer(
					outputstream), new DefaultErrorHandler()));
			xmlreader.parse(new InputSource(new FileInputStream(strings[0])));
		} catch (Exception exception) {
			System.out.println(exception.toString());
			exception.printStackTrace();
		}
	}

	static {
		((AttributesImpl) ltrOverrideAttributes).addAttribute("",
				"unicode-bidi", "unicode-bidi", "CDATA", "embed");
		((AttributesImpl) ltrOverrideAttributes).addAttribute("", "direction",
				"direction", "CDATA", "ltr");
		initialized = false;
	}
}