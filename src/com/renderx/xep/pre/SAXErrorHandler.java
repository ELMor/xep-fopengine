/*
 * SAXErrorHandler - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre;

import com.renderx.util.ErrorHandler;
import com.renderx.xep.lib.Logger;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class SAXErrorHandler implements org.xml.sax.ErrorHandler {
	int errorCount = 0;

	int warningCount = 0;

	private final ErrorHandler logger;

	SAXErrorHandler(Logger logger) {
		this.logger = logger;
		reset();
	}

	public void reset() {
		errorCount = 0;
		warningCount = 0;
	}

	private static String formatMessage(SAXParseException saxparseexception) {
		int i = saxparseexception.getLineNumber();
		int i_0_ = saxparseexception.getColumnNumber();
		if (i <= 0)
			return saxparseexception.getMessage();
		if (i_0_ <= 0)
			return "line " + i + ": " + saxparseexception.getMessage();
		return ("line " + i + ", column " + i_0_ + ": " + saxparseexception
				.getMessage());
	}

	public void warning(SAXParseException saxparseexception)
			throws SAXException {
		logger.warning(formatMessage(saxparseexception));
		warningCount++;
	}

	public void error(SAXParseException saxparseexception) throws SAXException {
		logger.error(formatMessage(saxparseexception));
		errorCount++;
	}

	public void fatalError(SAXParseException saxparseexception)
			throws SAXException {
		throw saxparseexception;
	}
}