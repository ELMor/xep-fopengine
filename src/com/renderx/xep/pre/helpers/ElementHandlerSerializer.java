/* ElementHandlerSerializer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.helpers;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Enumeration;

import com.renderx.xep.pre.AttList;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.CompilerException;
import com.renderx.xep.pre.Elem;
import com.renderx.xep.pre.ElementHandler;
import com.renderx.xep.pre.PIHandler;

public class ElementHandlerSerializer extends PrintStream
    implements ElementHandler, PIHandler
{
    private int level = 0;
    private boolean opentag = false;
    
    public ElementHandlerSerializer(OutputStream outputstream) {
	super(outputstream);
    }
    
    public void startElement(short i, AttList attlist)
	throws CompilerException {
	try {
	    if (opentag)
		this.print(">");
	    printIndent();
	    this.print("<");
	    printQuoted(Elem.getName(i));
	    opentag = true;
	    int i_0_ = attlist.size();
	    if (i_0_ > 0) {
		Attn[] attns = new Attn[i_0_];
		Enumeration enumeration = attlist.keys();
		int i_1_ = 0;
		while (enumeration.hasMoreElements()) {
		    Attn attn = (Attn) enumeration.nextElement();
		    int i_2_;
		    for (i_2_ = 0;
			 (i_2_ < i_1_
			  && attns[i_2_].name.compareTo(attn.name) < 0);
			 i_2_++) {
			/* empty */
		    }
		    for (int i_3_ = i_1_; i_3_ > i_2_; i_3_--)
			attns[i_3_] = attns[i_3_ - 1];
		    i_1_++;
		    attns[i_2_] = attn;
		}
		for (int i_4_ = 0; i_4_ < i_0_; i_4_++) {
		    Attn attn = attns[i_4_];
		    this.print(" ");
		    printQuoted(attn.name);
		    this.print("=\"");
		    printQuoted(attlist.get(attn).toString());
		    this.print("\"");
		}
	    }
	} catch (IOException ioexception) {
	    throw new CompilerException("Serializer error: "
					+ ioexception.getMessage());
	}
	level++;
    }
    
    public void endElement(short i) throws CompilerException {
	level--;
	try {
	    if (opentag) {
		this.print("/>");
		opentag = false;
	    } else {
		printIndent();
		this.print("</");
		printQuoted(Elem.getName(i));
		this.print(">");
	    }
	} catch (IOException ioexception) {
	    throw new CompilerException("Serializer error: "
					+ ioexception.getMessage());
	}
    }
    
    public void processingInstruction(String string, String string_5_)
	throws CompilerException {
	try {
	    if (opentag) {
		this.print(">");
		opentag = false;
	    }
	    printIndent();
	    this.print("<?" + string + ' ' + string_5_ + "?>");
	} catch (IOException ioexception) {
	    throw new CompilerException("Serializer error: "
					+ ioexception.getMessage());
	}
    }
    
    private void printIndent() throws IOException {
	this.println();
	for (int i = 0; i < level; i++)
	    this.print("  ");
    }
    
    private void printQuoted(String string) throws IOException {
	for (int i = 0; i < string.length(); i++) {
	    char c = string.charAt(i);
	    if (c == '<')
		this.print("&lt;");
	    else if (c == '>')
		this.print("&gt;");
	    else if (c == '&')
		this.print("&amp;");
	    else if (c == '\'')
		this.print("&apos;");
	    else if (c == '\"')
		this.print("&quot;");
	    else if (' ' <= c && c < '\u0080')
		this.print(c);
	    else
		this.print("&#x" + Integer.toHexString(c) + ";");
	}
    }
}
