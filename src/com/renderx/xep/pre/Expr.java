/*
 * Expr - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre;

public abstract class Expr extends Attr {
	public abstract String toString();

	public String getTypeName() {
		return "EXPRESSION";
	}

	public Attr cast(int i) {
		return this;
	}

	public abstract Attr initialize(Attr[] attrs) throws ParserException;

	public abstract Attr preevaluate() throws ParserException;

	public abstract Attr evaluate(ParserContext parsercontext, Attn attn)
			throws ParserException;
}