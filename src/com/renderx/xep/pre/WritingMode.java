/*
 * WritingMode - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre;

import com.renderx.util.Hashtable;
import com.renderx.xep.lib.InternalException;

public class WritingMode {
	private static final Hashtable nametab = new Hashtable();

	static WritingMode $defaultmode = null;

	public static final int LEFT = 0;

	public static final int RIGHT = 1;

	public static final int TOP = 2;

	public static final int BOTTOM = 3;

	public static final int START = 0;

	public static final int END = 1;

	public static final int BEFORE = 2;

	public static final int AFTER = 3;

	public final int[] sides = new int[4];

	public boolean isHorizontal = true;

	public Attr direction;

	public Attr attribute;

	private static boolean initialized = false;

	public static WritingMode byName(String string) throws ParserException {
		Object object = nametab.get(string);
		if (object == null)
			throw new ParserException("Unrecognized writing mode: " + string);
		return (WritingMode) object;
	}

	public static synchronized void init() throws InternalException {
		if (!initialized) {
			Attn.init();
			Bidi.init();
			new WritingMode("lr-tb", 0, 1, 2, 3);
			new WritingMode("rl-tb", 1, 0, 2, 3);
			new WritingMode("tb-rl", 2, 3, 1, 0);
			new WritingMode("tb-lr", 2, 3, 0, 1);
			new WritingMode("lr-bt", 0, 1, 3, 2);
			new WritingMode("rl-bt", 1, 0, 3, 2);
			new WritingMode("bt-rl", 3, 2, 1, 0);
			new WritingMode("bt-lr", 3, 2, 0, 1);
			$defaultmode = (WritingMode) nametab
					.get(Attn.$writing_mode.defaultValue.word());
			if ($defaultmode == null)
				throw new InternalException(
						"Invalid value for default writing mode");
			initialized = true;
		}
	}

	private WritingMode(String string, int i, int i_0_, int i_1_, int i_2_) {
		nametab.put(string, this);
		sides[i] = 0;
		sides[i_0_] = 1;
		sides[i_1_] = 2;
		sides[i_2_] = 3;
		isHorizontal = sides[0] == 0 || sides[0] == 1;
		direction = sides[0] == 1 ? Bidi.RIGHTTOLEFT : Bidi.LEFTTORIGHT;
		attribute = Attr.Word.create(string);
	}
}