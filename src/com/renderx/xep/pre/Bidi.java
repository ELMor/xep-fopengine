/*
 * Bidi - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre;

import com.renderx.xep.lib.InternalException;

import junit.framework.Assert;
import junit.framework.TestCase;

public class Bidi {
	public static final Attr[] classes = new Attr[65536];

	public static final char[] mirrorChars = new char[65536];

	public static final char[] initialShape;

	public static final char[] medialShape;

	public static final char[] finalShape;

	public static final boolean[] transparentShape;

	public static Attr LEFTTORIGHT;

	public static Attr RIGHTTOLEFT;

	public static Attr NEUTRAL;

	private static boolean initialized;

	public static class Test extends TestCase {
		protected void setUp() {
			init();
		}

		public void testBasic() {
			Assert.assertEquals(shape("", false), "");
			Assert.assertEquals(shape(" ", false), " ");
			Assert.assertEquals(shape("This is a text", false),
					"This is a text");
		}

		public void testAlef() {
			Assert.assertEquals(shape("\u0627", false), "\u0627");
		}

		public void testBehAlef() {
			Assert.assertEquals(shape("\u0628\u0627", false), "\ufe91\ufe8e");
		}

		public void testBehSpaceAlef() {
			Assert.assertEquals(shape("\u0628 \u0627", false), "\u0628 \u0627");
		}

		public void testAlefBehAlef() {
			Assert.assertEquals(shape("\u0627\u0628\u0627", false),
					"\u0627\ufe91\ufe8e");
		}

		public void testJeemBehAlef() {
			Assert.assertEquals(shape("\u062c\u0628\u0627", false),
					"\ufe9f\ufe92\ufe8e");
		}

		public void testBehTransparentAlef() {
			Assert.assertEquals(shape("\u0628\u064b\u064c\u064d\u0627", false),
					"\ufe91\u064b\u064c\u064d\ufe8e");
		}

		public void testLamAlef() {
			Assert.assertEquals(shape("\u0644\u0627", false), "\ufefb");
		}

		public void testBehLamAlef() {
			Assert.assertEquals(shape("\u0628\u0644\u0627", false),
					"\ufe91\ufefc");
		}

		public void testLamAlefLamAlefMadda() {
			Assert.assertEquals(shape("\u0644\u0627\u0644\u0622", false),
					"\ufefb\ufef5");
		}

		public void testMirrorON() {
			Assert.assertEquals(shape("( [ {", true), ") ] }");
		}

		public void testMirrorOFF() {
			Assert.assertEquals(shape("( [ {", false), "( [ {");
		}
	}

	private static void set(int i, Attr attr) {
		classes[i] = attr;
	}

	private static void setRange(int i, int i_0_, Attr attr) {
		for (int i_1_ = i; i_1_ <= i_0_; i_1_++)
			set(i_1_, attr);
	}

	private static void setMirror(int i, char c) {
		mirrorChars[i] = c;
	}

	public static String mirror(String string) {
		int i;
		for (i = 0; i < string.length() && mirrorChars[string.charAt(i)] == 0; i++) {
			/* empty */
		}
		if (i == string.length())
			return string;
		char[] cs = string.toCharArray();
		for (/**/; i < string.length(); i++) {
			char c = mirrorChars[string.charAt(i)];
			if (c != 0)
				cs[i] = c;
		}
		return new String(cs);
	}

	public static String shape(String string, boolean bool) {
		int i;
		for (i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			if (bool && mirrorChars[c] != 0 || initialShape[c] != 0
					|| medialShape[c] != 0 || finalShape[c] != 0)
				break;
		}
		if (i == string.length())
			return string;
		char[] cs = string.toCharArray();
		int i_2_ = i;
		boolean bool_3_ = false;
		while (i < cs.length) {
			char c = string.charAt(i++);
			if (bool) {
				char c_4_ = mirrorChars[c];
				if (c_4_ != 0)
					c = c_4_;
			}
			if (c == '\u0644' && i < cs.length) {
				switch (string.charAt(i)) {
				case '\u0622':
					c = '\ufef5';
					i++;
					break;
				case '\u0623':
					c = '\ufef7';
					i++;
					break;
				case '\u0625':
					c = '\ufef9';
					i++;
					break;
				case '\u0627':
					c = '\ufefb';
					i++;
					break;
				}
			}
			int i_5_ = i;
			for (/**/; i < cs.length && transparentShape[string.charAt(i)]; i++) {
				/* empty */
			}
			if (i == cs.length || finalShape[string.charAt(i)] == 0) {
				if (bool_3_)
					c = finalShape[c];
				bool_3_ = false;
			} else {
				char c_6_ = bool_3_ ? medialShape[c] : initialShape[c];
				if (c_6_ == 0) {
					if (bool_3_)
						c = finalShape[c];
					bool_3_ = false;
				} else {
					c = c_6_;
					bool_3_ = true;
				}
			}
			cs[i_2_++] = c;
			while (i_5_ != i)
				cs[i_2_++] = string.charAt(i_5_++);
		}
		return new String(cs, 0, i_2_);
	}

	public static synchronized void init() throws InternalException {
		if (!initialized) {
			LEFTTORIGHT = Attr.Word.create("ltr");
			RIGHTTOLEFT = Attr.Word.create("rtl");
			NEUTRAL = Attr.Word.create("neutral");
			setRange(0, 65535, LEFTTORIGHT);
			setRange(0, 31, NEUTRAL);
			setRange(128, 159, NEUTRAL);
			setRange(768, 879, NEUTRAL);
			setRange(8192, 8303, NEUTRAL);
			setRange(8400, 8447, NEUTRAL);
			setRange(8592, 8703, NEUTRAL);
			setRange(9472, 10175, NEUTRAL);
			setRange(65520, 65535, NEUTRAL);
			setRange(1424, 1983, RIGHTTOLEFT);
			setRange(64285, 65023, RIGHTTOLEFT);
			setRange(65136, 65279, RIGHTTOLEFT);
			setRange(1632, 1641, LEFTTORIGHT);
			set(9, NEUTRAL);
			set(10, NEUTRAL);
			set(13, NEUTRAL);
			set(32, NEUTRAL);
			set(160, NEUTRAL);
			setRange(8192, 8203, NEUTRAL);
			set(8232, NEUTRAL);
			set(8239, NEUTRAL);
			setRange(33, 34, NEUTRAL);
			setRange(39, 46, NEUTRAL);
			setRange(58, 63, NEUTRAL);
			setRange(91, 96, NEUTRAL);
			setRange(123, 126, NEUTRAL);
			set(161, NEUTRAL);
			set(166, NEUTRAL);
			set(168, NEUTRAL);
			set(171, NEUTRAL);
			set(173, NEUTRAL);
			set(175, NEUTRAL);
			set(180, NEUTRAL);
			setRange(182, 184, NEUTRAL);
			set(187, NEUTRAL);
			set(191, NEUTRAL);
			set(215, NEUTRAL);
			set(247, NEUTRAL);
			setRange(1643, 1644, NEUTRAL);
			set(8206, LEFTTORIGHT);
			set(8234, LEFTTORIGHT);
			set(8237, LEFTTORIGHT);
			set(8207, RIGHTTOLEFT);
			set(8235, RIGHTTOLEFT);
			set(8238, RIGHTTOLEFT);
			setMirror(40, ')');
			setMirror(41, '(');
			setMirror(60, '>');
			setMirror(62, '<');
			setMirror(91, ']');
			setMirror(93, '[');
			setMirror(123, '}');
			setMirror(125, '{');
			setMirror(171, '\u00bb');
			setMirror(187, '\u00ab');
			setMirror(8249, '\u203a');
			setMirror(8250, '\u2039');
			setMirror(8261, '\u2046');
			setMirror(8262, '\u2045');
			setMirror(8317, '\u207e');
			setMirror(8318, '\u207d');
			setMirror(8333, '\u208e');
			setMirror(8334, '\u208d');
			setMirror(8712, '\u220b');
			setMirror(8713, '\u220c');
			setMirror(8714, '\u220d');
			setMirror(8715, '\u2208');
			setMirror(8716, '\u2209');
			setMirror(8717, '\u220a');
			setMirror(8725, '\u29f5');
			setMirror(8764, '\u223d');
			setMirror(8765, '\u223c');
			setMirror(8771, '\u22cd');
			setMirror(8786, '\u2253');
			setMirror(8787, '\u2252');
			setMirror(8788, '\u2255');
			setMirror(8789, '\u2254');
			setMirror(8804, '\u2265');
			setMirror(8805, '\u2264');
			setMirror(8806, '\u2267');
			setMirror(8807, '\u2266');
			setMirror(8808, '\u2269');
			setMirror(8809, '\u2268');
			setMirror(8810, '\u226b');
			setMirror(8811, '\u226a');
			setMirror(8814, '\u226f');
			setMirror(8815, '\u226e');
			setMirror(8816, '\u2271');
			setMirror(8817, '\u2270');
			setMirror(8818, '\u2273');
			setMirror(8819, '\u2272');
			setMirror(8820, '\u2275');
			setMirror(8821, '\u2274');
			setMirror(8822, '\u2277');
			setMirror(8823, '\u2276');
			setMirror(8824, '\u2279');
			setMirror(8825, '\u2278');
			setMirror(8826, '\u227b');
			setMirror(8827, '\u227a');
			setMirror(8828, '\u227d');
			setMirror(8829, '\u227c');
			setMirror(8830, '\u227f');
			setMirror(8831, '\u227e');
			setMirror(8832, '\u2281');
			setMirror(8833, '\u2280');
			setMirror(8834, '\u2283');
			setMirror(8835, '\u2282');
			setMirror(8836, '\u2285');
			setMirror(8837, '\u2284');
			setMirror(8838, '\u2287');
			setMirror(8839, '\u2286');
			setMirror(8840, '\u2289');
			setMirror(8841, '\u2288');
			setMirror(8842, '\u228b');
			setMirror(8843, '\u228a');
			setMirror(8847, '\u2290');
			setMirror(8848, '\u228f');
			setMirror(8849, '\u2292');
			setMirror(8850, '\u2291');
			setMirror(8856, '\u29b8');
			setMirror(8866, '\u22a3');
			setMirror(8867, '\u22a2');
			setMirror(8870, '\u2ade');
			setMirror(8872, '\u2ae4');
			setMirror(8873, '\u2ae3');
			setMirror(8875, '\u2ae5');
			setMirror(8880, '\u22b1');
			setMirror(8881, '\u22b0');
			setMirror(8882, '\u22b3');
			setMirror(8883, '\u22b2');
			setMirror(8884, '\u22b5');
			setMirror(8885, '\u22b4');
			setMirror(8886, '\u22b7');
			setMirror(8887, '\u22b6');
			setMirror(8905, '\u22ca');
			setMirror(8906, '\u22c9');
			setMirror(8907, '\u22cc');
			setMirror(8908, '\u22cb');
			setMirror(8909, '\u2243');
			setMirror(8912, '\u22d1');
			setMirror(8913, '\u22d0');
			setMirror(8918, '\u22d7');
			setMirror(8919, '\u22d6');
			setMirror(8920, '\u22d9');
			setMirror(8921, '\u22d8');
			setMirror(8922, '\u22db');
			setMirror(8923, '\u22da');
			setMirror(8924, '\u22dd');
			setMirror(8925, '\u22dc');
			setMirror(8926, '\u22df');
			setMirror(8927, '\u22de');
			setMirror(8928, '\u22e1');
			setMirror(8929, '\u22e0');
			setMirror(8930, '\u22e3');
			setMirror(8931, '\u22e2');
			setMirror(8932, '\u22e5');
			setMirror(8933, '\u22e4');
			setMirror(8934, '\u22e7');
			setMirror(8935, '\u22e6');
			setMirror(8936, '\u22e9');
			setMirror(8937, '\u22e8');
			setMirror(8938, '\u22eb');
			setMirror(8939, '\u22ea');
			setMirror(8940, '\u22ed');
			setMirror(8941, '\u22ec');
			setMirror(8944, '\u22f1');
			setMirror(8945, '\u22f0');
			setMirror(8946, '\u22fa');
			setMirror(8947, '\u22fb');
			setMirror(8948, '\u22fc');
			setMirror(8950, '\u22fd');
			setMirror(8951, '\u22fe');
			setMirror(8954, '\u22f2');
			setMirror(8955, '\u22f3');
			setMirror(8956, '\u22f4');
			setMirror(8957, '\u22f6');
			setMirror(8958, '\u22f7');
			setMirror(8968, '\u2309');
			setMirror(8969, '\u2308');
			setMirror(8970, '\u230b');
			setMirror(8971, '\u230a');
			setMirror(9001, '\u232a');
			setMirror(9002, '\u2329');
			setMirror(10088, '\u2769');
			setMirror(10089, '\u2768');
			setMirror(10090, '\u276b');
			setMirror(10091, '\u276a');
			setMirror(10092, '\u276d');
			setMirror(10093, '\u276c');
			setMirror(10094, '\u276f');
			setMirror(10095, '\u276e');
			setMirror(10096, '\u2771');
			setMirror(10097, '\u2770');
			setMirror(10098, '\u2773');
			setMirror(10099, '\u2772');
			setMirror(10100, '\u2775');
			setMirror(10101, '\u2774');
			setMirror(10197, '\u27d6');
			setMirror(10198, '\u27d5');
			setMirror(10205, '\u27de');
			setMirror(10206, '\u27dd');
			setMirror(10210, '\u27e3');
			setMirror(10211, '\u27e2');
			setMirror(10212, '\u27e5');
			setMirror(10213, '\u27e4');
			setMirror(10214, '\u27e7');
			setMirror(10215, '\u27e6');
			setMirror(10216, '\u27e9');
			setMirror(10217, '\u27e8');
			setMirror(10218, '\u27eb');
			setMirror(10219, '\u27ea');
			setMirror(10627, '\u2984');
			setMirror(10628, '\u2983');
			setMirror(10629, '\u2986');
			setMirror(10630, '\u2985');
			setMirror(10631, '\u2988');
			setMirror(10632, '\u2987');
			setMirror(10633, '\u298a');
			setMirror(10634, '\u2989');
			setMirror(10635, '\u298c');
			setMirror(10636, '\u298b');
			setMirror(10637, '\u2990');
			setMirror(10638, '\u298f');
			setMirror(10639, '\u298e');
			setMirror(10640, '\u298d');
			setMirror(10641, '\u2992');
			setMirror(10642, '\u2991');
			setMirror(10643, '\u2994');
			setMirror(10644, '\u2993');
			setMirror(10645, '\u2996');
			setMirror(10646, '\u2995');
			setMirror(10647, '\u2998');
			setMirror(10648, '\u2997');
			setMirror(10680, '\u2298');
			setMirror(10688, '\u29c1');
			setMirror(10689, '\u29c0');
			setMirror(10692, '\u29c5');
			setMirror(10693, '\u29c4');
			setMirror(10703, '\u29d0');
			setMirror(10704, '\u29cf');
			setMirror(10705, '\u29d2');
			setMirror(10706, '\u29d1');
			setMirror(10708, '\u29d5');
			setMirror(10709, '\u29d4');
			setMirror(10712, '\u29d9');
			setMirror(10713, '\u29d8');
			setMirror(10714, '\u29db');
			setMirror(10715, '\u29da');
			setMirror(10741, '\u2215');
			setMirror(10744, '\u29f9');
			setMirror(10745, '\u29f8');
			setMirror(10748, '\u29fd');
			setMirror(10749, '\u29fc');
			setMirror(10795, '\u2a2c');
			setMirror(10796, '\u2a2b');
			setMirror(10797, '\u2a2c');
			setMirror(10798, '\u2a2d');
			setMirror(10804, '\u2a35');
			setMirror(10805, '\u2a34');
			setMirror(10812, '\u2a3d');
			setMirror(10813, '\u2a3c');
			setMirror(10852, '\u2a65');
			setMirror(10853, '\u2a64');
			setMirror(10873, '\u2a7a');
			setMirror(10874, '\u2a79');
			setMirror(10877, '\u2a7e');
			setMirror(10878, '\u2a7d');
			setMirror(10879, '\u2a80');
			setMirror(10880, '\u2a7f');
			setMirror(10881, '\u2a82');
			setMirror(10882, '\u2a81');
			setMirror(10883, '\u2a84');
			setMirror(10884, '\u2a83');
			setMirror(10891, '\u2a8c');
			setMirror(10892, '\u2a8b');
			setMirror(10897, '\u2a92');
			setMirror(10898, '\u2a91');
			setMirror(10899, '\u2a94');
			setMirror(10900, '\u2a93');
			setMirror(10901, '\u2a96');
			setMirror(10902, '\u2a95');
			setMirror(10903, '\u2a98');
			setMirror(10904, '\u2a97');
			setMirror(10905, '\u2a9a');
			setMirror(10906, '\u2a99');
			setMirror(10907, '\u2a9c');
			setMirror(10908, '\u2a9b');
			setMirror(10913, '\u2aa2');
			setMirror(10914, '\u2aa1');
			setMirror(10918, '\u2aa7');
			setMirror(10919, '\u2aa6');
			setMirror(10920, '\u2aa9');
			setMirror(10921, '\u2aa8');
			setMirror(10922, '\u2aab');
			setMirror(10923, '\u2aaa');
			setMirror(10924, '\u2aad');
			setMirror(10925, '\u2aac');
			setMirror(10927, '\u2ab0');
			setMirror(10928, '\u2aaf');
			setMirror(10931, '\u2ab4');
			setMirror(10932, '\u2ab3');
			setMirror(10939, '\u2abc');
			setMirror(10940, '\u2abb');
			setMirror(10941, '\u2abe');
			setMirror(10942, '\u2abd');
			setMirror(10943, '\u2ac0');
			setMirror(10944, '\u2abf');
			setMirror(10945, '\u2ac2');
			setMirror(10946, '\u2ac1');
			setMirror(10947, '\u2ac4');
			setMirror(10948, '\u2ac3');
			setMirror(10949, '\u2ac6');
			setMirror(10950, '\u2ac5');
			setMirror(10957, '\u2ace');
			setMirror(10958, '\u2acd');
			setMirror(10959, '\u2ad0');
			setMirror(10960, '\u2acf');
			setMirror(10961, '\u2ad2');
			setMirror(10962, '\u2ad1');
			setMirror(10963, '\u2ad4');
			setMirror(10964, '\u2ad3');
			setMirror(10965, '\u2ad6');
			setMirror(10966, '\u2ad5');
			setMirror(10974, '\u22a6');
			setMirror(10979, '\u22a9');
			setMirror(10980, '\u22a8');
			setMirror(10981, '\u22ab');
			setMirror(10988, '\u2aed');
			setMirror(10989, '\u2aec');
			setMirror(10999, '\u2af8');
			setMirror(11000, '\u2af7');
			setMirror(11001, '\u2afa');
			setMirror(11002, '\u2af9');
			setMirror(12296, '\u3009');
			setMirror(12297, '\u3008');
			setMirror(12298, '\u300b');
			setMirror(12299, '\u300a');
			setMirror(12300, '\u300d');
			setMirror(12301, '\u300c');
			setMirror(12302, '\u300f');
			setMirror(12303, '\u300e');
			setMirror(12304, '\u3011');
			setMirror(12305, '\u3010');
			setMirror(12308, '\u3015');
			setMirror(12309, '\u3014');
			setMirror(12310, '\u3017');
			setMirror(12311, '\u3016');
			setMirror(12312, '\u3019');
			setMirror(12313, '\u3018');
			setMirror(12314, '\u301b');
			setMirror(12315, '\u301a');
			setMirror(65288, '\uff09');
			setMirror(65289, '\uff08');
			setMirror(65308, '\uff1e');
			setMirror(65310, '\uff1c');
			setMirror(65339, '\uff3d');
			setMirror(65341, '\uff3b');
			setMirror(65371, '\uff5d');
			setMirror(65373, '\uff5b');
			setMirror(65375, '\uff60');
			setMirror(65376, '\uff5f');
			setMirror(65378, '\uff63');
			setMirror(65379, '\uff62');
			finalShape[1649] = '\ufb51';
			finalShape[1659] = '\ufb53';
			initialShape[1659] = '\ufb54';
			medialShape[1659] = '\ufb55';
			finalShape[1662] = '\ufb57';
			initialShape[1662] = '\ufb58';
			medialShape[1662] = '\ufb59';
			finalShape[1664] = '\ufb5b';
			initialShape[1664] = '\ufb5c';
			medialShape[1664] = '\ufb5d';
			finalShape[1658] = '\ufb5f';
			initialShape[1658] = '\ufb60';
			medialShape[1658] = '\ufb61';
			finalShape[1663] = '\ufb63';
			initialShape[1663] = '\ufb64';
			medialShape[1663] = '\ufb65';
			finalShape[1657] = '\ufb67';
			initialShape[1657] = '\ufb68';
			medialShape[1657] = '\ufb69';
			finalShape[1700] = '\ufb6b';
			initialShape[1700] = '\ufb6c';
			medialShape[1700] = '\ufb6d';
			finalShape[1702] = '\ufb6f';
			initialShape[1702] = '\ufb70';
			medialShape[1702] = '\ufb71';
			finalShape[1668] = '\ufb73';
			initialShape[1668] = '\ufb74';
			medialShape[1668] = '\ufb75';
			finalShape[1667] = '\ufb77';
			initialShape[1667] = '\ufb78';
			medialShape[1667] = '\ufb79';
			finalShape[1670] = '\ufb7b';
			initialShape[1670] = '\ufb7c';
			medialShape[1670] = '\ufb7d';
			finalShape[1671] = '\ufb7f';
			initialShape[1671] = '\ufb80';
			medialShape[1671] = '\ufb81';
			finalShape[1677] = '\ufb83';
			finalShape[1676] = '\ufb85';
			finalShape[1678] = '\ufb87';
			finalShape[1672] = '\ufb89';
			finalShape[1688] = '\ufb8b';
			finalShape[1681] = '\ufb8d';
			finalShape[1705] = '\ufb8f';
			initialShape[1705] = '\ufb90';
			medialShape[1705] = '\ufb91';
			finalShape[1711] = '\ufb93';
			initialShape[1711] = '\ufb94';
			medialShape[1711] = '\ufb95';
			finalShape[1715] = '\ufb97';
			initialShape[1715] = '\ufb98';
			medialShape[1715] = '\ufb99';
			finalShape[1713] = '\ufb9b';
			initialShape[1713] = '\ufb9c';
			medialShape[1713] = '\ufb9d';
			finalShape[1722] = '\ufb9f';
			finalShape[1723] = '\ufba1';
			initialShape[1723] = '\ufba2';
			medialShape[1723] = '\ufba3';
			finalShape[1728] = '\ufba5';
			finalShape[1729] = '\ufba7';
			initialShape[1729] = '\ufba8';
			medialShape[1729] = '\ufba9';
			finalShape[1726] = '\ufbab';
			initialShape[1726] = '\ufbac';
			medialShape[1726] = '\ufbad';
			finalShape[1746] = '\ufbaf';
			finalShape[1747] = '\ufbb1';
			finalShape[1709] = '\ufbd4';
			initialShape[1709] = '\ufbd5';
			medialShape[1709] = '\ufbd6';
			finalShape[1735] = '\ufbd8';
			finalShape[1734] = '\ufbda';
			finalShape[1736] = '\ufbdc';
			finalShape[1739] = '\ufbdf';
			finalShape[1733] = '\ufbe1';
			finalShape[1737] = '\ufbe3';
			finalShape[1744] = '\ufbe5';
			initialShape[1744] = '\ufbe6';
			medialShape[1744] = '\ufbe7';
			initialShape[1609] = '\ufbe8';
			medialShape[1609] = '\ufbe9';
			finalShape[1740] = '\ufbfd';
			initialShape[1740] = '\ufbfe';
			medialShape[1740] = '\ufbff';
			finalShape[1570] = '\ufe82';
			finalShape[1571] = '\ufe84';
			finalShape[1572] = '\ufe86';
			finalShape[1573] = '\ufe88';
			finalShape[1574] = '\ufe8a';
			initialShape[1574] = '\ufe8b';
			medialShape[1574] = '\ufe8c';
			finalShape[1575] = '\ufe8e';
			finalShape[1576] = '\ufe90';
			initialShape[1576] = '\ufe91';
			medialShape[1576] = '\ufe92';
			finalShape[1577] = '\ufe94';
			finalShape[1578] = '\ufe96';
			initialShape[1578] = '\ufe97';
			medialShape[1578] = '\ufe98';
			finalShape[1579] = '\ufe9a';
			initialShape[1579] = '\ufe9b';
			medialShape[1579] = '\ufe9c';
			finalShape[1580] = '\ufe9e';
			initialShape[1580] = '\ufe9f';
			medialShape[1580] = '\ufea0';
			finalShape[1581] = '\ufea2';
			initialShape[1581] = '\ufea3';
			medialShape[1581] = '\ufea4';
			finalShape[1582] = '\ufea6';
			initialShape[1582] = '\ufea7';
			medialShape[1582] = '\ufea8';
			finalShape[1583] = '\ufeaa';
			finalShape[1584] = '\ufeac';
			finalShape[1585] = '\ufeae';
			finalShape[1586] = '\ufeb0';
			finalShape[1587] = '\ufeb2';
			initialShape[1587] = '\ufeb3';
			medialShape[1587] = '\ufeb4';
			finalShape[1588] = '\ufeb6';
			initialShape[1588] = '\ufeb7';
			medialShape[1588] = '\ufeb8';
			finalShape[1589] = '\ufeba';
			initialShape[1589] = '\ufebb';
			medialShape[1589] = '\ufebc';
			finalShape[1590] = '\ufebe';
			initialShape[1590] = '\ufebf';
			medialShape[1590] = '\ufec0';
			finalShape[1591] = '\ufec2';
			initialShape[1591] = '\ufec3';
			medialShape[1591] = '\ufec4';
			finalShape[1592] = '\ufec6';
			initialShape[1592] = '\ufec7';
			medialShape[1592] = '\ufec8';
			finalShape[1593] = '\ufeca';
			initialShape[1593] = '\ufecb';
			medialShape[1593] = '\ufecc';
			finalShape[1594] = '\ufece';
			initialShape[1594] = '\ufecf';
			medialShape[1594] = '\ufed0';
			finalShape[1601] = '\ufed2';
			initialShape[1601] = '\ufed3';
			medialShape[1601] = '\ufed4';
			finalShape[1602] = '\ufed6';
			initialShape[1602] = '\ufed7';
			medialShape[1602] = '\ufed8';
			finalShape[1603] = '\ufeda';
			initialShape[1603] = '\ufedb';
			medialShape[1603] = '\ufedc';
			finalShape[1604] = '\ufede';
			initialShape[1604] = '\ufedf';
			medialShape[1604] = '\ufee0';
			finalShape[1605] = '\ufee2';
			initialShape[1605] = '\ufee3';
			medialShape[1605] = '\ufee4';
			finalShape[1606] = '\ufee6';
			initialShape[1606] = '\ufee7';
			medialShape[1606] = '\ufee8';
			finalShape[1607] = '\ufeea';
			initialShape[1607] = '\ufeeb';
			medialShape[1607] = '\ufeec';
			finalShape[1608] = '\ufeee';
			finalShape[1609] = '\ufef0';
			finalShape[1610] = '\ufef2';
			initialShape[1610] = '\ufef3';
			medialShape[1610] = '\ufef4';
			finalShape[65269] = '\ufef6';
			finalShape[65271] = '\ufef8';
			finalShape[65273] = '\ufefa';
			finalShape[65275] = '\ufefc';
			finalShape[1600] = initialShape[1600] = medialShape[1600] = '\u0640';
			finalShape[8205] = initialShape[8205] = medialShape[8205] = '\u200d';
			transparentShape[1611] = true;
			transparentShape[1612] = true;
			transparentShape[1613] = true;
			transparentShape[1614] = true;
			transparentShape[1615] = true;
			transparentShape[1616] = true;
			transparentShape[1617] = true;
			transparentShape[1618] = true;
			transparentShape[1619] = true;
			transparentShape[1620] = true;
			transparentShape[1621] = true;
			transparentShape[1648] = true;
			transparentShape[1750] = true;
			transparentShape[1751] = true;
			transparentShape[1752] = true;
			transparentShape[1753] = true;
			transparentShape[1754] = true;
			transparentShape[1755] = true;
			transparentShape[1756] = true;
			transparentShape[1758] = true;
			transparentShape[1759] = true;
			transparentShape[1760] = true;
			transparentShape[1761] = true;
			transparentShape[1762] = true;
			transparentShape[1763] = true;
			transparentShape[1764] = true;
			transparentShape[1767] = true;
			transparentShape[1768] = true;
			transparentShape[1770] = true;
			transparentShape[1771] = true;
			transparentShape[1772] = true;
			transparentShape[1773] = true;
			initialized = true;
		}
	}

	static {
		for (int i = 0; i < mirrorChars.length; i++)
			mirrorChars[i] = '\0';
		initialShape = new char[65536];
		medialShape = new char[65536];
		finalShape = new char[65536];
		for (int i = 0; i < initialShape.length; i++)
			initialShape[i] = medialShape[i] = finalShape[i] = '\0';
		transparentShape = new boolean[65536];
		for (int i = 0; i < transparentShape.length; i++)
			transparentShape[i] = false;
		initialized = false;
	}
}