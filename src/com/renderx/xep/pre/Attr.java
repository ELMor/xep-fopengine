/*
 * Attr - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre;

import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

import com.renderx.xep.lib.InternalException;
import com.renderx.xep.pre.att.ResolvablePercents;

public abstract class Attr {
	private Object key;

	public static final Bool trueval = Bool.create(true);

	public static final Bool falseval = Bool.create(false);

	public static final Length zerolength = Length.create(0);

	public static Word all;

	public static Word always;

	public static Word auto;

	public static Word baseline;

	public static Word consider_shifts;

	public static Word discard;

	public static Word font_height;

	public static Word force;

	public static Word hidden;

	public static Word ignore;

	public static Word max_height;

	public static Word none;

	public static Word relative;

	public static Word retain;

	public static Word treat_as_space;

	public static Word wrap;

	public static Word ltr;

	public static Word lr_tb;

	public static Word rl_tb;

	public static Word header;

	public static Word footer;

	public static Count count0;

	public static Ratio ratio1;

	private static boolean initialized = false;

	public static final class Percentage extends Numeric implements Scalable {
		public final double percentage;

		private static final Attr.ValueCache valueCache = new Attr.ValueCache();

		public Percentage(double d) {
			this.percentage = d;
		}

		public String toString() {
			return String.valueOf(this.percentage) + "%";
		}

		public final String getTypeName() {
			return "Percentage";
		}

		public static synchronized Attr.Percentage create(double d) {
			Double var_double = new Double(d);
			Object object = valueCache.get(var_double);
			if (object == null)
				valueCache.put(var_double, object = new Attr.Percentage(d));
			return (Attr.Percentage) object;
		}

		public Attr cast(int i) {
			return ((i & 0x4) > 0 ? Attr.Ratio.create(this.percentage / 100.0)
					: null);
		}

		public Attr evaluate(ParserContext parsercontext, Attn attn)
				throws ParserException {
			if (attn instanceof ResolvablePercents) {
				Attr.Scalable scalable = ((ResolvablePercents) attn)
						.getPercentBase(parsercontext);
				if (scalable != null)
					return scalable.scale(this.percentage / 100.0);
			}
			return this;
		}

		public Attr scale(double d) {
			return create(this.percentage * d);
		}

		public Attr.Numeric minus() {
			return create(-this.percentage);
		}

		public Attr.Numeric abs() {
			return this.percentage >= 0.0 ? (Attr.Numeric) this : minus();
		}

		public boolean greater(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Percentage)
				return this.percentage > numeric.percentage();
			throw new ParserException("Cannot compare Percentage to "
					+ numeric.getTypeName());
		}

		public Attr.Numeric add(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Percentage)
				return create(this.percentage + numeric.percentage());
			throw new ParserException("Cannot add " + numeric.getTypeName()
					+ " to Percentage");
		}

		public Attr.Numeric subtract(Attr.Numeric numeric)
				throws ParserException {
			if (numeric instanceof Attr.Percentage)
				return create(this.percentage - numeric.percentage());
			throw new ParserException("Cannot subtract "
					+ numeric.getTypeName() + " from Percentage");
		}

		public Attr.Numeric multiply(Attr.Numeric numeric)
				throws ParserException {
			if (numeric instanceof Attr.Count)
				return create(this.percentage * (double) numeric.count());
			if (numeric instanceof Attr.Ratio)
				return create(this.percentage * numeric.ratio());
			throw new ParserException("Cannot multiply Percentage by "
					+ numeric.getTypeName());
		}

		public Attr.Numeric divide(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Count || numeric instanceof Attr.Ratio) {
				double d = (numeric instanceof Attr.Count ? (double) numeric
						.count() : numeric.ratio());
				if (d == 0.0)
					throw new ParserException("Divide by zero in expression");
				return create(this.percentage / d);
			}
			if (numeric instanceof Attr.Percentage) {
				double d = numeric.percentage();
				if (d == 0.0)
					throw new ParserException("Divide by zero in expression");
				return Attr.Ratio.create(this.percentage / d);
			}
			throw new ParserException("Cannot divide Percentage by "
					+ numeric.getTypeName());
		}

		public Attr.Numeric modulo(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Percentage) {
				double d = numeric.percentage();
				if (d == 0.0)
					throw new ParserException("Divide by zero in expression");
				return create(this.percentage % d);
			}
			throw new ParserException("Cannot calculate Percentage modulo "
					+ numeric.getTypeName());
		}
	}

	public static final class EMLength extends Numeric implements Scalable {
		public final double emlength;

		private static final Attr.ValueCache valueCache = new Attr.ValueCache();

		public EMLength(double d) {
			this.emlength = d;
		}

		public String toString() {
			return String.valueOf(this.emlength) + "em";
		}

		public final String getTypeName() {
			return "Length (em)";
		}

		public static synchronized Attr.EMLength create(double d) {
			Double var_double = new Double(d);
			Object object = valueCache.get(var_double);
			if (object == null)
				valueCache.put(var_double, object = new Attr.EMLength(d));
			return (Attr.EMLength) object;
		}

		public Attr evaluate(ParserContext parsercontext, Attn attn)
				throws ParserException {
			return parsercontext.fontSize.scale(this.emlength);
		}

		public Attr scale(double d) {
			return create(this.emlength * d);
		}

		public Attr.Numeric minus() {
			return create(-this.emlength);
		}

		public Attr.Numeric abs() {
			return this.emlength >= 0.0 ? (Attr.Numeric) this : minus();
		}

		public boolean greater(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.EMLength)
				return this.emlength > numeric.emlength();
			throw new ParserException("Cannot compare Length (em) to "
					+ numeric.getTypeName());
		}

		public Attr.Numeric add(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.EMLength)
				return create(this.emlength + numeric.emlength());
			throw new ParserException("Cannot add " + numeric.getTypeName()
					+ " to Length (em)");
		}

		public Attr.Numeric subtract(Attr.Numeric numeric)
				throws ParserException {
			if (numeric instanceof Attr.EMLength)
				return create(this.emlength - numeric.emlength());
			throw new ParserException("Cannot subtract "
					+ numeric.getTypeName() + " from Length (em)");
		}

		public Attr.Numeric multiply(Attr.Numeric numeric)
				throws ParserException {
			if (numeric instanceof Attr.Count)
				return create(this.emlength * (double) numeric.count());
			if (numeric instanceof Attr.Ratio)
				return create(this.emlength * numeric.ratio());
			throw new ParserException("Cannot multiply Length (em) by "
					+ numeric.getTypeName());
		}

		public Attr.Numeric divide(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Count || numeric instanceof Attr.Ratio) {
				double d = (numeric instanceof Attr.Count ? (double) numeric
						.count() : numeric.ratio());
				if (d == 0.0)
					throw new ParserException("Divide by zero in expression");
				return create(this.emlength / d);
			}
			if (numeric instanceof Attr.EMLength) {
				double d = numeric.emlength();
				if (d == 0.0)
					throw new ParserException("Divide by zero in expression");
				return Attr.Ratio.create(this.emlength / d);
			}
			throw new ParserException("Cannot divide Length (em) by "
					+ numeric.getTypeName());
		}

		public Attr.Numeric modulo(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.EMLength) {
				double d = numeric.emlength();
				if (d == 0.0)
					throw new ParserException("Divide by zero in expression");
				return create(this.emlength % d);
			}
			throw new ParserException("Cannot calculate Length (em) modulo "
					+ numeric.getTypeName());
		}
	}

	public static final class Composite extends Attr {
		public final Attr[] vector;

		public Composite(Attr[] attrs) {
			vector = attrs;
		}

		public String toString() {
			String string = "[";
			for (int i = 0; i < vector.length; i++) {
				if (i > 0)
					string += ", ";
				string = string
						+ (vector[i] == null ? "NULL" : vector[i].toString());
			}
			string += "]";
			return string;
		}

		public final String getTypeName() {
			return "Attribute vector";
		}
	}

	public abstract static class Color extends Attr {
		public static Transparent transparent = Transparent.create();

		public static final class CMYK extends Opaque {
			private static final Attr.ValueCache valueCache = new Attr.ValueCache();

			public final long cmyk;

			public final double[] c_m_y_k() {
				return (new double[] {
						(double) (this.cmyk >> 48 & 0xffffL) / 65535.0,
						(double) (this.cmyk >> 32 & 0xffffL) / 65535.0,
						(double) (this.cmyk >> 16 & 0xffffL) / 65535.0,
						(double) (this.cmyk & 0xffffL) / 65535.0 });
			}

			public CMYK(long l) {
				this.cmyk = l;
			}

			public final boolean equals(Object object) {
				return (object instanceof Attr.Color.CMYK && ((Attr.Color.CMYK) object).cmyk == this.cmyk);
			}

			public String toString() {
				double d = (double) (this.cmyk >> 48 & 0xffffL) / 65535.0;
				double d_0_ = (double) (this.cmyk >> 32 & 0xffffL) / 65535.0;
				double d_1_ = (double) (this.cmyk >> 16 & 0xffffL) / 65535.0;
				double d_2_ = (double) (this.cmyk & 0xffffL) / 65535.0;
				double d_3_ = 1.0 - d_2_;
				double d_4_ = d_3_ > d ? d_3_ - d : 0.0;
				double d_5_ = d_3_ > d_0_ ? d_3_ - d_0_ : 0.0;
				double d_6_ = d_3_ > d_1_ ? d_3_ - d_1_ : 0.0;
				return ("rgb-icc(" + d_4_ * 255.0 + ", " + d_5_ * 255.0 + ", "
						+ d_6_ * 255.0 + ", #CMYK, " + d + ", " + d_0_ + ", "
						+ d_1_ + ", " + d_2_ + ")");
			}

			public final String getTypeName() {
				return "CMYK color";
			}

			public static final Attr.Color.CMYK create(double d, double d_7_,
					double d_8_, double d_9_) {
				if (d < 0.0)
					d = 0.0;
				else if (d > 1.0)
					d = 1.0;
				if (d_7_ < 0.0)
					d_7_ = 0.0;
				else if (d_7_ > 1.0)
					d_7_ = 1.0;
				if (d_8_ < 0.0)
					d_8_ = 0.0;
				else if (d_8_ > 1.0)
					d_8_ = 1.0;
				if (d_9_ < 0.0)
					d_9_ = 0.0;
				else if (d_9_ > 1.0)
					d_9_ = 1.0;
				long l = Math.round(d * 65535.0);
				l <<= 16;
				l |= Math.round(d_7_ * 65535.0);
				l <<= 16;
				l |= Math.round(d_8_ * 65535.0);
				l <<= 16;
				l |= Math.round(d_9_ * 65535.0);
				return create(l);
			}

			private static final synchronized Attr.Color.CMYK create(long l) {
				Long var_long = new Long(l);
				Object object = valueCache.get(var_long);
				if (object == null)
					valueCache.put(var_long, object = new Attr.Color.CMYK(l));
				return (Attr.Color.CMYK) object;
			}

			public Attr.Color.Opaque darkerColor() {
				double[] ds = c_m_y_k();
				double d = 1.0 - ds[0];
				double d_10_ = 1.0 - ds[1];
				double d_11_ = 1.0 - ds[2];
				double d_12_ = 1.0 - ds[3];
				double d_13_ = (d + d_10_ + d_11_) / 3.0;
				double d_14_ = d_13_ < 0.5 ? d_13_ : 1.0 - 0.25 / d_13_;
				return create(1.0 - d * d_14_, 1.0 - d_10_ * d_14_, 1.0 - d_11_
						* d_14_, 1.0 - (d_12_ < 0.5 ? d_12_ * d_12_
						: d_12_ - 0.25));
			}

			public Attr.Color.Opaque lighterColor() {
				double[] ds = c_m_y_k();
				double d = ds[0];
				double d_15_ = ds[1];
				double d_16_ = ds[2];
				double d_17_ = ds[3];
				double d_18_ = (d + d_15_ + d_16_) / 3.0;
				double d_19_ = d_18_ < 0.5 ? d_18_ : 1.0 - 0.25 / d_18_;
				return create(d * d_19_, d_15_ * d_19_, d_16_ * d_19_,
						d_17_ < 0.5 ? d_17_ * d_17_ : d_17_ - 0.25);
			}
		}

		public static final class RGB extends Opaque {
			private static final Attr.ValueCache valueCache = new Attr.ValueCache();

			public final long rgb;

			public final double[] r_g_b() {
				return (new double[] {
						(double) (this.rgb >> 32 & 0xffffL) / 65535.0,
						(double) (this.rgb >> 16 & 0xffffL) / 65535.0,
						(double) (this.rgb & 0xffffL) / 65535.0 });
			}

			public RGB(long l) {
				this.rgb = l;
			}

			public final boolean equals(Object object) {
				return (object instanceof Attr.Color.RGB && ((Attr.Color.RGB) object).rgb == this.rgb);
			}

			public String toString() {
				double d = (double) (this.rgb >> 32 & 0xffffL) / 65535.0;
				double d_20_ = (double) (this.rgb >> 16 & 0xffffL) / 65535.0;
				double d_21_ = (double) (this.rgb & 0xffffL) / 65535.0;
				return ("rgb(" + d * 255.0 + ", " + d_20_ * 255.0 + ", "
						+ d_21_ * 255.0 + ")");
			}

			public final String getTypeName() {
				return "RGB color";
			}

			public static final Attr.Color.RGB create(double d, double d_22_,
					double d_23_) {
				if (d < 0.0)
					d = 0.0;
				else if (d > 1.0)
					d = 1.0;
				if (d_22_ < 0.0)
					d_22_ = 0.0;
				else if (d_22_ > 1.0)
					d_22_ = 1.0;
				if (d_23_ < 0.0)
					d_23_ = 0.0;
				else if (d_23_ > 1.0)
					d_23_ = 1.0;
				long l = Math.round(d * 65535.0);
				l <<= 16;
				l |= Math.round(d_22_ * 65535.0);
				l <<= 16;
				l |= Math.round(d_23_ * 65535.0);
				return create(l);
			}

			private static final synchronized Attr.Color.RGB create(long l) {
				Long var_long = new Long(l);
				Object object = valueCache.get(var_long);
				if (object == null)
					valueCache.put(var_long, object = new Attr.Color.RGB(l));
				return (Attr.Color.RGB) object;
			}

			public Attr.Color.Opaque darkerColor() {
				double[] ds = r_g_b();
				double d = ds[0];
				double d_24_ = ds[1];
				double d_25_ = ds[2];
				double d_26_ = (d + d_24_ + d_25_) / 3.0;
				double d_27_ = d_26_ < 0.5 ? d_26_ : 1.0 - 0.25 / d_26_;
				return create(d * d_27_, d_24_ * d_27_, d_25_ * d_27_);
			}

			public Attr.Color.Opaque lighterColor() {
				double[] ds = r_g_b();
				double d = 1.0 - ds[0];
				double d_28_ = 1.0 - ds[1];
				double d_29_ = 1.0 - ds[2];
				double d_30_ = (d + d_28_ + d_29_) / 3.0;
				double d_31_ = d_30_ < 0.5 ? d_30_ : 1.0 - 0.25 / d_30_;
				return create(1.0 - d * d_31_, 1.0 - d_28_ * d_31_, 1.0 - d_29_
						* d_31_);
			}
		}

		public static final class SpotColor extends Opaque {
			private static final Attr.ValueCache valueCache = new Attr.ValueCache();

			public final int tint;

			public final String colorant;

			public final Attr.Color.Opaque altColor;

			public final double tint() {
				return (double) (tint & 0xffff) / 65535.0;
			}

			public SpotColor(String string, Attr.Color.Opaque opaque, int i) {
				colorant = string;
				altColor = opaque;
				tint = i;
			}

			public final boolean equals(Object object) {
				if (object instanceof Attr.Color.SpotColor) {
					Attr.Color.SpotColor spotcolor_32_ = (Attr.Color.SpotColor) object;
					return (spotcolor_32_.tint == tint
							&& spotcolor_32_.colorant.equals(colorant) && spotcolor_32_.altColor
							.equals(altColor));
				}
				return false;
			}

			public String toString() {
				double d = (double) tint / 65535.0;
				return ("rgb-icc(" + d * 255.0 + ", " + d * 255.0 + ", " + d
						* 255.0 + ", #SpotColor, '" + colorant + "', "
						+ altColor.toString() + ", " + d + ")");
			}

			public final String getTypeName() {
				return "Spot color";
			}

			public static final Attr.Color.SpotColor create(String string,
					Attr.Color.Opaque opaque, double d) {
				if (d < 0.0)
					d = 0.0;
				else if (d > 1.0)
					d = 1.0;
				return create(string, opaque, (int) Math.round(d * 65535.0));
			}

			private static final synchronized Attr.Color.SpotColor create(
					String string, Attr.Color.Opaque opaque, int i) {
				String string_33_ = "" + i + " " + opaque.hashCode() + " "
						+ string;
				Object object = valueCache.get(string_33_);
				if (object == null)
					valueCache
							.put(string_33_, object = new Attr.Color.SpotColor(
									string, opaque, i));
				return (Attr.Color.SpotColor) object;
			}

			public Attr.Color.Opaque darkerColor() {
				double d = 1.0 - tint();
				return create(colorant, altColor, 1.0 - (d < 0.5 ? d * d
						: d - 0.25));
			}

			public Attr.Color.Opaque lighterColor() {
				double d = tint();
				return create(colorant, altColor, d < 0.5 ? d * d : d - 0.25);
			}
		}

		public static final class Registration extends Opaque {
			private static final Attr.ValueCache valueCache = new Attr.ValueCache();

			public final int tint;

			public final double tint() {
				return (double) (tint & 0xffff) / 65535.0;
			}

			public Registration(int i) {
				tint = i;
			}

			public final boolean equals(Object object) {
				return (object instanceof Attr.Color.Registration && ((Attr.Color.Registration) object).tint == tint);
			}

			public String toString() {
				double d = (double) tint / 65535.0;
				return ("rgb-icc(" + d * 255.0 + ", " + d * 255.0 + ", " + d
						* 255.0 + ", #Registration, " + d + ")");
			}

			public final String getTypeName() {
				return "Registration color";
			}

			public static final Attr.Color.Registration create(double d) {
				if (d < 0.0)
					d = 0.0;
				else if (d > 1.0)
					d = 1.0;
				return create((int) Math.round(d * 65535.0));
			}

			private static final synchronized Attr.Color.Registration create(
					int i) {
				Integer integer = new Integer(i);
				Object object = valueCache.get(integer);
				if (object == null)
					valueCache.put(integer,
							object = new Attr.Color.Registration(i));
				return (Attr.Color.Registration) object;
			}

			public Attr.Color.Opaque darkerColor() {
				double d = tint();
				return create(d < 0.5 ? d * d : d - 0.25);
			}

			public Attr.Color.Opaque lighterColor() {
				double d = 1.0 - tint();
				return create(1.0 - (d < 0.5 ? d * d : d - 0.25));
			}
		}

		public static final class Grayscale extends Opaque {
			private static final Attr.ValueCache valueCache = new Attr.ValueCache();

			public final int gray;

			public final double gray() {
				return (double) (gray & 0xffff) / 65535.0;
			}

			public Grayscale(int i) {
				gray = i;
			}

			public final boolean equals(Object object) {
				return (object instanceof Attr.Color.Grayscale && ((Attr.Color.Grayscale) object).gray == gray);
			}

			public String toString() {
				double d = (double) gray / 65535.0;
				return ("rgb-icc(" + d * 255.0 + ", " + d * 255.0 + ", " + d
						* 255.0 + ", #Grayscale, " + d + ")");
			}

			public final String getTypeName() {
				return "Grayscale color";
			}

			public static final Attr.Color.Grayscale create(double d) {
				if (d < 0.0)
					d = 0.0;
				else if (d > 1.0)
					d = 1.0;
				return create((int) Math.round(d * 65535.0));
			}

			private static final synchronized Attr.Color.Grayscale create(int i) {
				Integer integer = new Integer(i);
				Object object = valueCache.get(integer);
				if (object == null)
					valueCache.put(integer,
							object = new Attr.Color.Grayscale(i));
				return (Attr.Color.Grayscale) object;
			}

			public Attr.Color.Opaque darkerColor() {
				double d = gray();
				return create(d < 0.5 ? d * d : d - 0.25);
			}

			public Attr.Color.Opaque lighterColor() {
				double d = 1.0 - gray();
				return create(1.0 - (d < 0.5 ? d * d : d - 0.25));
			}
		}

		public abstract static class Opaque extends Attr.Color {
			public static final int BIT_DEPTH = 16;

			public static final int BASE = 65536;

			public static final int MAXVALUE = 65535;

			public abstract Attr.Color.Opaque lighterColor();

			public abstract Attr.Color.Opaque darkerColor();
		}

		private static final class Transparent extends Attr.Color {
			private static Attr.Color.Transparent the_only_instance = new Attr.Color.Transparent();

			private Transparent() {
				/* empty */
			}

			public boolean equals(Object object) {
				return object instanceof Attr.Color.Transparent;
			}

			public String toString() {
				return "transparent";
			}

			public final String getTypeName() {
				return "Transparent color";
			}

			public static Attr.Color.Transparent create() {
				return the_only_instance;
			}
		}
	}

	public static final class Bool extends Attr {
		public final boolean bool;

		private static Attr.Bool true_instance = new Attr.Bool(true);

		private static Attr.Bool false_instance = new Attr.Bool(false);

		private Bool(boolean bool_34_) {
			this.bool = bool_34_;
		}

		public String toString() {
			return this.bool ? "true" : "false";
		}

		public final String getTypeName() {
			return "Boolean";
		}

		public static Attr.Bool create(boolean bool) {
			return bool ? true_instance : false_instance;
		}
	}

	public static final class Ratio extends Numeric implements Scalable {
		public final double ratio;

		private static final Attr.ValueCache valueCache = new Attr.ValueCache();

		public Ratio(double d) {
			this.ratio = d;
		}

		public String toString() {
			return String.valueOf(this.ratio);
		}

		public final String getTypeName() {
			return "Real number";
		}

		public static synchronized Attr.Ratio create(double d) {
			Double var_double = new Double(d);
			Object object = valueCache.get(var_double);
			if (object == null)
				valueCache.put(var_double, object = new Attr.Ratio(d));
			return (Attr.Ratio) object;
		}

		public Attr cast(int i) {
			if ((i & 0x2) > 0)
				return this;
			if ((i & 0x1) > 0)
				return Attr.Count.create((int) Math.round(this.ratio));
			if (this.ratio == 0.0) {
				if ((i & 0x8) > 0)
					return Attr.Length.create(0);
				if ((i & 0x10) > 0)
					return Attr.Angle.create(0.0);
			}
			return null;
		}

		public Attr scale(double d) {
			return create(this.ratio * d);
		}

		public Attr.Numeric minus() {
			return create(-this.ratio);
		}

		public Attr.Numeric abs() {
			return this.ratio >= 0.0 ? (Attr.Numeric) this : minus();
		}

		public boolean greater(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Count)
				return this.ratio > (double) numeric.count();
			if (numeric instanceof Attr.Ratio)
				return this.ratio > numeric.ratio();
			throw new ParserException("Cannot compare Real number to "
					+ numeric.getTypeName());
		}

		public Attr.Numeric add(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Count)
				return create(this.ratio + (double) numeric.count());
			if (numeric instanceof Attr.Ratio)
				return create(this.ratio + numeric.ratio());
			throw new ParserException("Cannot add " + numeric.getTypeName()
					+ " to Real number");
		}

		public Attr.Numeric subtract(Attr.Numeric numeric)
				throws ParserException {
			if (numeric instanceof Attr.Count)
				return create(this.ratio - (double) numeric.count());
			if (numeric instanceof Attr.Ratio)
				return create(this.ratio - numeric.ratio());
			throw new ParserException("Cannot subtract "
					+ numeric.getTypeName() + " from Real number");
		}

		public Attr.Numeric multiply(Attr.Numeric numeric)
				throws ParserException {
			if (numeric instanceof Attr.Count)
				return create(this.ratio * (double) numeric.count());
			if (numeric instanceof Attr.Ratio)
				return create(this.ratio * numeric.ratio());
			return numeric.multiply(this);
		}

		public Attr.Numeric divide(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Count || numeric instanceof Attr.Ratio) {
				double d = (numeric instanceof Attr.Count ? (double) numeric
						.count() : numeric.ratio());
				if (d == 0.0)
					throw new ParserException("Divide by zero in expression");
				return create(this.ratio / d);
			}
			throw new ParserException("Cannot divide Real number by "
					+ numeric.getTypeName());
		}

		public Attr.Numeric modulo(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Count || numeric instanceof Attr.Ratio) {
				double d = (numeric instanceof Attr.Count ? (double) numeric
						.count() : numeric.ratio());
				if (d == 0.0)
					throw new ParserException("Divide by zero in expression");
				return create(this.ratio % d);
			}
			throw new ParserException("Cannot calculate Real number modulo "
					+ numeric.getTypeName());
		}
	}

	public static final class Count extends Numeric {
		public final int count;

		private static final Attr.ValueCache valueCache = new Attr.ValueCache();

		public Count(int i) {
			this.count = i;
		}

		public String toString() {
			return String.valueOf(this.count);
		}

		public final String getTypeName() {
			return "Integer";
		}

		public static synchronized Attr.Count create(int i) {
			Integer integer = new Integer(i);
			Object object = valueCache.get(integer);
			if (object == null)
				valueCache.put(integer, object = new Attr.Count(i));
			return (Attr.Count) object;
		}

		public Attr cast(int i) {
			if ((i & 0x1) > 0)
				return this;
			if ((i & 0x2) > 0)
				return Attr.Ratio.create((double) this.count);
			if (this.count == 0) {
				if ((i & 0x8) > 0)
					return Attr.Length.create(0);
				if ((i & 0x10) > 0)
					return Attr.Angle.create(0.0);
			}
			return null;
		}

		public Attr.Numeric minus() {
			return create(-this.count);
		}

		public Attr.Numeric abs() {
			return this.count >= 0 ? (Attr.Numeric) this : minus();
		}

		public boolean greater(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Count)
				return this.count > numeric.count();
			if (numeric instanceof Attr.Ratio)
				return (double) this.count > numeric.ratio();
			throw new ParserException("Cannot compare Integer to "
					+ numeric.getTypeName());
		}

		public Attr.Numeric add(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Count)
				return create(this.count + numeric.count());
			if (numeric instanceof Attr.Ratio)
				return Attr.Ratio.create((double) this.count + numeric.ratio());
			throw new ParserException("Cannot add " + numeric.getTypeName()
					+ " to Integer");
		}

		public Attr.Numeric subtract(Attr.Numeric numeric)
				throws ParserException {
			if (numeric instanceof Attr.Count)
				return create(this.count - numeric.count());
			if (numeric instanceof Attr.Ratio)
				return Attr.Ratio.create((double) this.count - numeric.ratio());
			throw new ParserException("Cannot subtract "
					+ numeric.getTypeName() + " from Integer");
		}

		public Attr.Numeric multiply(Attr.Numeric numeric)
				throws ParserException {
			if (numeric instanceof Attr.Count)
				return create(this.count * numeric.count());
			if (numeric instanceof Attr.Ratio)
				return Attr.Ratio.create((double) this.count * numeric.ratio());
			return numeric.multiply(this);
		}

		public Attr.Numeric divide(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Count) {
				int i = numeric.count();
				if (i == 0)
					throw new ParserException("Divide by zero in expression");
				if (this.count % i == 0)
					return create(this.count / i);
				return Attr.Ratio.create((double) this.count / (double) i);
			}
			if (numeric instanceof Attr.Ratio) {
				double d = numeric.ratio();
				if (d == 0.0)
					throw new ParserException("Divide by zero in expression");
				return Attr.Ratio.create((double) this.count / d);
			}
			throw new ParserException("Cannot divide Integer by "
					+ numeric.getTypeName());
		}

		public Attr.Numeric modulo(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Count) {
				int i = numeric.count();
				if (i == 0)
					throw new ParserException("Divide by zero in expression");
				return create(this.count % i);
			}
			if (numeric instanceof Attr.Ratio) {
				double d = numeric.ratio();
				if (d == 0.0)
					throw new ParserException("Divide by zero in expression");
				return Attr.Ratio.create((double) this.count % d);
			}
			throw new ParserException("Cannot calculate Integer modulo "
					+ numeric.getTypeName());
		}
	}

	public static final class Angle extends Numeric implements Scalable {
		public final double angle;

		private static final Attr.ValueCache valueCache = new Attr.ValueCache();

		public Angle(double d) {
			this.angle = d;
		}

		public String toString() {
			return String.valueOf(this.angle) + "deg";
		}

		public final String getTypeName() {
			return "Angle";
		}

		public static synchronized Attr.Angle create(double d) {
			Double var_double = new Double(d);
			Object object = valueCache.get(var_double);
			if (object == null)
				valueCache.put(var_double, object = new Attr.Angle(d));
			return (Attr.Angle) object;
		}

		public Attr cast(int i) {
			return (i & 0x10) > 0 ? this : null;
		}

		public Attr scale(double d) {
			return create(this.angle * d);
		}

		public Attr.Numeric minus() {
			return create(-this.angle);
		}

		public Attr.Numeric abs() {
			return this.angle >= 0.0 ? (Attr.Numeric) this : minus();
		}

		public boolean greater(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Angle)
				return this.angle > numeric.angle();
			throw new ParserException("Cannot compare Angle to "
					+ numeric.getTypeName());
		}

		public Attr.Numeric add(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Angle)
				return create(this.angle + numeric.angle());
			throw new ParserException("Cannot add " + numeric.getTypeName()
					+ " to Angle");
		}

		public Attr.Numeric subtract(Attr.Numeric numeric)
				throws ParserException {
			if (numeric instanceof Attr.Angle)
				return create(this.angle - numeric.angle());
			throw new ParserException("Cannot subtract "
					+ numeric.getTypeName() + " from Angle");
		}

		public Attr.Numeric multiply(Attr.Numeric numeric)
				throws ParserException {
			if (numeric instanceof Attr.Count)
				return create(this.angle * (double) numeric.count());
			if (numeric instanceof Attr.Ratio)
				return create(this.angle * numeric.ratio());
			throw new ParserException("Cannot multiply Angle by "
					+ numeric.getTypeName());
		}

		public Attr.Numeric divide(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Count || numeric instanceof Attr.Ratio) {
				double d = (numeric instanceof Attr.Count ? (double) numeric
						.count() : numeric.ratio());
				if (d == 0.0)
					throw new ParserException("Divide by zero in expression");
				return create(this.angle / d);
			}
			if (numeric instanceof Attr.Angle) {
				double d = numeric.angle();
				if (d == 0.0)
					throw new ParserException("Divide by zero in expression");
				return Attr.Ratio.create(this.angle / d);
			}
			throw new ParserException("Cannot divide Angle by "
					+ numeric.getTypeName());
		}

		public Attr.Numeric modulo(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Angle) {
				double d = numeric.angle();
				if (d == 0.0)
					throw new ParserException("Divide by zero in expression");
				return create(this.angle % d);
			}
			throw new ParserException("Cannot calculate Angle modulo "
					+ numeric.getTypeName());
		}
	}

	public static final class Length extends Numeric implements Scalable {
		public final int length;

		private static final Attr.ValueCache valueCache = new Attr.ValueCache();

		public Length(int i) {
			this.length = i;
		}

		public String toString() {
			return String.valueOf((double) this.length / 1000.0) + "pt";
		}

		public final String getTypeName() {
			return "Length";
		}

		public static synchronized Attr.Length create(int i) {
			Integer integer = new Integer(i);
			Object object = valueCache.get(integer);
			if (object == null)
				valueCache.put(integer, object = new Attr.Length(i));
			return (Attr.Length) object;
		}

		public Attr cast(int i) {
			return (i & 0x8) > 0 ? this : null;
		}

		public Attr scale(double d) {
			return create((int) Math.round((double) this.length * d));
		}

		public Attr.Numeric minus() {
			return create(-this.length);
		}

		public Attr.Numeric abs() {
			return this.length >= 0 ? (Attr.Numeric) this : minus();
		}

		public boolean greater(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Length)
				return this.length > numeric.length();
			throw new ParserException("Cannot compare Length to "
					+ numeric.getTypeName());
		}

		public Attr.Numeric add(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Length)
				return create(this.length + numeric.length());
			throw new ParserException("Cannot add " + numeric.getTypeName()
					+ " to Length");
		}

		public Attr.Numeric subtract(Attr.Numeric numeric)
				throws ParserException {
			if (numeric instanceof Attr.Length)
				return create(this.length - numeric.length());
			throw new ParserException("Cannot subtract "
					+ numeric.getTypeName() + " from Length");
		}

		public Attr.Numeric multiply(Attr.Numeric numeric)
				throws ParserException {
			if (numeric instanceof Attr.Count)
				return create(this.length * numeric.count());
			if (numeric instanceof Attr.Ratio)
				return create((int) Math.round((double) this.length
						* numeric.ratio()));
			throw new ParserException("Cannot multiply Length by "
					+ numeric.getTypeName());
		}

		public Attr.Numeric divide(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Count || numeric instanceof Attr.Ratio) {
				double d = (numeric instanceof Attr.Count ? (double) numeric
						.count() : numeric.ratio());
				if (d == 0.0)
					throw new ParserException("Divide by zero in expression");
				return create((int) Math.round((double) this.length / d));
			}
			if (numeric instanceof Attr.Length) {
				double d = (double) numeric.length();
				if (d == 0.0)
					throw new ParserException("Divide by zero in expression");
				return Attr.Ratio.create((double) this.length / d);
			}
			throw new ParserException("Cannot divide Length by "
					+ numeric.getTypeName());
		}

		public Attr.Numeric modulo(Attr.Numeric numeric) throws ParserException {
			if (numeric instanceof Attr.Length) {
				int i = numeric.length();
				if (i == 0)
					throw new ParserException("Divide by zero in expression");
				return create(this.length % i);
			}
			throw new ParserException("Cannot calculate Length modulo "
					+ numeric.getTypeName());
		}
	}

	public static final class Word extends Attr {
		public final String word;

		private static final Attr.ValueCache valueCache = new Attr.ValueCache();

		public Word(String string) {
			this.word = string;
		}

		public String toString() {
			return this.word;
		}

		public final String getTypeName() {
			return "Word";
		}

		public static synchronized Attr.Word create(String string) {
			Object object = valueCache.get(string);
			if (object == null)
				valueCache.put(string, object = new Attr.Word(string));
			return (Attr.Word) object;
		}

		public static Attr.Word createUncached(String string) {
			return new Attr.Word(string);
		}

		public Attr cast(int i) {
			return (i & 0x20) > 0 ? this : null;
		}
	}

	public static interface Scalable {
		public Attr scale(double d);
	}

	public abstract static class Numeric extends Attr {
		public abstract boolean greater(Attr.Numeric numeric_35_)
				throws ParserException;

		public abstract Attr.Numeric abs() throws ParserException;

		public abstract Attr.Numeric minus() throws ParserException;

		public abstract Attr.Numeric add(Attr.Numeric numeric_36_)
				throws ParserException;

		public abstract Attr.Numeric subtract(Attr.Numeric numeric_37_)
				throws ParserException;

		public abstract Attr.Numeric multiply(Attr.Numeric numeric_38_)
				throws ParserException;

		public abstract Attr.Numeric divide(Attr.Numeric numeric_39_)
				throws ParserException;

		public abstract Attr.Numeric modulo(Attr.Numeric numeric_40_)
				throws ParserException;
	}

	private static class ValueCache {
		private WeakHashMap t = new WeakHashMap();

		private ValueCache() {
			/* empty */
		}

		Object get(Object object) {
			Object object_41_ = t.get(object);
			return (object_41_ == null ? null : ((WeakReference) object_41_)
					.get());
		}

		void put(Object object, Object object_42_) {
			((Attr) object_42_).key = object;
			t.put(object, new WeakReference(object_42_));
		}
	}

	public abstract String toString();

	public abstract String getTypeName();

	public Attr cast(int i) {
		return null;
	}

	public Attr evaluate(ParserContext parsercontext, Attn attn)
			throws ParserException {
		return this;
	}

	public Attr preevaluate() throws ParserException {
		return this;
	}

	public final int length_or_ratio(int i) {
		if (this instanceof Length)
			return length();
		if (this instanceof Ratio)
			return (int) Math.round(ratio() * (double) i);
		if (this == auto)
			return i;
		throw new InternalException(this.getClass().getName()
				+ " is illegal type for length_or_ratio()");
	}

	public final int length() {
		return ((Length) this).length;
	}

	public final int count() {
		return ((Count) this).count;
	}

	public final double ratio() {
		return ((Ratio) this).ratio;
	}

	public final boolean bool() {
		return ((Bool) this).bool;
	}

	public final String word() {
		return ((Word) this).word;
	}

	public final Attr[] vector() {
		return ((Composite) this).vector;
	}

	public final double percentage() {
		return ((Percentage) this).percentage;
	}

	public final double emlength() {
		return ((EMLength) this).emlength;
	}

	public final double angle() {
		return ((Angle) this).angle;
	}

	public static Word newWord(String string) {
		return Word.create(string);
	}

	public static Length newLength(int i) {
		return Length.create(i);
	}

	public static Ratio newRatio(double d) {
		return Ratio.create(d);
	}

	public static Count newCount(int i) {
		return Count.create(i);
	}

	public static int dim_range(Attributed attributed, Attn attn,
			Attn attn_43_, Attn attn_44_, int i) {
		Attr attr = attributed.get(attn);
		Attr attr_45_ = attributed.get(attn_43_);
		Attr attr_46_ = attributed.get(attn_44_);
		return (attr == auto && attr_45_ == auto && attr_46_ == auto ? -2147483648
				: a_range(attributed, attn, attn_43_, attn_44_, attr, attr_45_,
						attr_46_, i));
	}

	public static int length_range(Attributed attributed, Attn attn,
			Attn attn_47_, Attn attn_48_, int i) {
		return a_range(attributed, attn, attn_47_, attn_48_, attributed
				.get(attn), attributed.get(attn_47_), attributed.get(attn_48_),
				i);
	}

	private static int a_range(Attributed attributed, Attn attn, Attn attn_49_,
			Attn attn_50_, Attr attr, Attr attr_51_, Attr attr_52_, int i) {
		int i_53_ = attr.length_or_ratio(i);
		int i_54_ = attr_51_.length_or_ratio(i);
		int i_55_ = attr_52_.length_or_ratio(i);
		if (i_53_ > i_55_) {
			if (attributed.containsKey(attn_50_))
				i_53_ = i_55_;
			else
				i_55_ = i_53_;
		}
		if (!attributed.containsKey(attn_49_))
			i_54_ = i_53_;
		return i_54_;
	}

	public static synchronized void init() {
		if (!initialized) {
			all = Word.create("all");
			always = Word.create("always");
			auto = Word.create("auto");
			baseline = Word.create("baseline");
			consider_shifts = Word.create("consider-shifts");
			discard = Word.create("discard");
			font_height = Word.create("font-height");
			force = Word.create("force");
			hidden = Word.create("hidden");
			ignore = Word.create("ignore");
			max_height = Word.create("max-height");
			none = Word.create("none");
			relative = Word.create("relative");
			retain = Word.create("retain");
			treat_as_space = Word.create("treat-as-space");
			wrap = Word.create("wrap");
			ltr = Word.create("ltr");
			lr_tb = Word.create("lr-tb");
			rl_tb = Word.create("rl-tb");
			header = Word.create("header");
			footer = Word.create("footer");
			count0 = Count.create(0);
			ratio1 = Ratio.create(1.0);
			initialized = true;
		}
	}
}