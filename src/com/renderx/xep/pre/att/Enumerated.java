/* Enumerated - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class Enumerated extends Parsed
{
    public Enumerated(String string, String string_0_, String[] strings)
	throws ParserException {
	super(string);
	defaultValue = Attr.Word.create(string_0_);
	this.addKeywords(strings);
    }
    
    protected Attr normalize(Attr attr, ParserContext parsercontext)
	throws ParserException {
	if (attr instanceof Attr.Word)
	    throw new ParserException("Invalid attribute value: " + name
				      + "=\"" + attr + "\"");
	throw new ParserException("Attribute type mismatch: " + name
				  + " cannot have value '" + attr
				  + "' of type " + attr.getTypeName());
    }
}
