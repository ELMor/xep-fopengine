/* VerticalAlign - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.AttributeParser;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class VerticalAlign extends Attn
{
    private final Attn alignAdjust = Attn.$alignment_adjust;
    private final Attn alignBase = Attn.$alignment_baseline;
    private final Attn baseShift = Attn.$baseline_shift;
    private final Attn domBase = Attn.$dominant_baseline;
    
    public VerticalAlign(String string) throws ParserException {
	super(string);
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	Attr attr
	    = AttributeParser.parse(string).evaluate(parsercontext, this);
	if (attr instanceof Attr.Word) {
	    String string_0_ = attr.word();
	    if (string_0_.equals("baseline")) {
		alignBase.process("baseline", parsercontext);
		alignAdjust.process("auto", parsercontext);
		baseShift.process("baseline", parsercontext);
		domBase.process("auto", parsercontext);
	    } else if (string_0_.equals("top")) {
		alignBase.process("before-edge", parsercontext);
		alignAdjust.process("auto", parsercontext);
		baseShift.process("baseline", parsercontext);
		domBase.process("auto", parsercontext);
	    } else if (string_0_.equals("text-top")) {
		alignBase.process("text-before-edge", parsercontext);
		alignAdjust.process("auto", parsercontext);
		baseShift.process("baseline", parsercontext);
		domBase.process("auto", parsercontext);
	    } else if (string_0_.equals("middle")) {
		alignBase.process("middle", parsercontext);
		alignAdjust.process("auto", parsercontext);
		baseShift.process("baseline", parsercontext);
		domBase.process("auto", parsercontext);
	    } else if (string_0_.equals("bottom")) {
		alignBase.process("after-edge", parsercontext);
		alignAdjust.process("auto", parsercontext);
		baseShift.process("baseline", parsercontext);
		domBase.process("auto", parsercontext);
	    } else if (string_0_.equals("text-bottom")) {
		alignBase.process("text-after-edge", parsercontext);
		alignAdjust.process("auto", parsercontext);
		baseShift.process("baseline", parsercontext);
		domBase.process("auto", parsercontext);
	    } else if (string_0_.equals("sub")) {
		alignBase.process("baseline", parsercontext);
		alignAdjust.process("auto", parsercontext);
		baseShift.process("sub", parsercontext);
		domBase.process("auto", parsercontext);
	    } else if (string_0_.equals("super")) {
		alignBase.process("baseline", parsercontext);
		alignAdjust.process("auto", parsercontext);
		baseShift.process("super", parsercontext);
		domBase.process("auto", parsercontext);
	    } else
		throw new ParserException("Invalid keyword in property value: "
					  + name + "=\"" + string + "\"");
	} else if (attr instanceof Attr.Length
		   || attr instanceof Attr.Percentage) {
	    alignBase.process("baseline", parsercontext);
	    alignAdjust.process(attr.toString(), parsercontext);
	    baseShift.process("baseline", parsercontext);
	    domBase.process("auto", parsercontext);
	} else
	    throw new ParserException("Invalid property value: " + name + "=\""
				      + string + "\"");
    }
}
