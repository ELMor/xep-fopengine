/* KeepSpec - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class KeepSpec extends Attn implements Alias
{
    private Attn line;
    private Attn column;
    private Attn page;
    
    public Attn resolveAlias(ParserContext parsercontext) {
	return column;
    }
    
    public KeepSpec(String string) throws ParserException {
	super(string);
	line = Attn.byName(string + ".within-line");
	column = Attn.byName(string + ".within-column");
	page = Attn.byName(string + ".within-page");
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	line.process(string, parsercontext);
	column.process(string, parsercontext);
	page.process(string, parsercontext);
    }
}
