/* NoOp - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.ParserContext;

public class NoOp extends Attn
{
    public NoOp(String string) {
	super(string);
    }
    
    public void process(String string, ParserContext parsercontext) {
	/* empty */
    }
}
