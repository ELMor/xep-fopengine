/* LineHeight - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class LineHeight extends Typed implements ResolvablePercents
{
    public LineHeight(String string, String string_0_) throws ParserException {
	super(string, string_0_, 10);
	this.addAlias("normal", "1.2");
    }
    
    public Attr.Scalable getPercentBase(ParserContext parsercontext) {
	return parsercontext.fontSize;
    }
    
    public Attr postprocess(Attr attr, ParserContext parsercontext)
	throws ParserException {
	return (attr instanceof Attr.Ratio
		? parsercontext.fontSize.scale(attr.ratio()) : attr);
    }
}
