/* PageBreak - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.AttributeParser;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class PageBreak extends Attn
{
    private final Attn keepProp;
    private final Attn breakProp;
    
    public PageBreak(String string, String string_0_, String string_1_)
	throws ParserException {
	super(string);
	keepProp = Attn.byName(string_1_);
	breakProp = Attn.byName(string_0_);
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	if (string.trim().equals("inherit")) {
	    keepProp.process("inherit", parsercontext);
	    breakProp.process("inherit", parsercontext);
	}
	Attr attr
	    = AttributeParser.parse(string).evaluate(parsercontext, this);
	if (!(attr instanceof Attr.Word))
	    throw new ParserException("Invalid property value: " + name + "=\""
				      + string + "\"");
	String string_2_ = attr.word();
	if (string_2_.equals("always"))
	    breakProp.process("page", parsercontext);
	else if (string_2_.equals("avoid"))
	    keepProp.process("always", parsercontext);
	else if (string_2_.equals("left"))
	    breakProp.process("even-page", parsercontext);
	else if (string_2_.equals("right"))
	    breakProp.process("odd-page", parsercontext);
	else if (!string_2_.equals("auto"))
	    throw new ParserException("Invalid keyword in property value: "
				      + name + "=\"" + string + "\"");
    }
}
