/* BorderSide - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import java.util.Enumeration;

import com.renderx.util.Hashtable;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.AttributeParser;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;
import com.renderx.xep.pre.Token;

public class BorderSide extends Attn
{
    private AbsoluteSide widthComponent;
    private AbsoluteSide colorComponent;
    private AbsoluteSide styleComponent;
    private Hashtable widths = new Hashtable();
    private Hashtable styles = new Hashtable();
    
    public BorderSide(String string) throws ParserException {
	super(string);
	widthComponent = (AbsoluteSide) Attn.byName(string + "-width");
	colorComponent = (AbsoluteSide) Attn.byName(string + "-color");
	styleComponent = (AbsoluteSide) Attn.byName(string + "-style");
	widths.put("thin", new Boolean(true));
	widths.put("medium", new Boolean(true));
	widths.put("thick", new Boolean(true));
	styles.put("none", new Boolean(true));
	styles.put("hidden", new Boolean(true));
	styles.put("solid", new Boolean(true));
	styles.put("double", new Boolean(true));
	styles.put("dashed", new Boolean(true));
	styles.put("dotted", new Boolean(true));
	styles.put("inset", new Boolean(true));
	styles.put("outset", new Boolean(true));
	styles.put("groove", new Boolean(true));
	styles.put("ridge", new Boolean(true));
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	boolean bool = false;
	boolean bool_0_ = false;
	boolean bool_1_ = false;
	java.util.Stack stack = AttributeParser.parseShorthand(string);
	if (stack.size() > 3)
	    throw new ParserException("Too many tokens in shorthand: " + name
				      + "=\"" + string + "\"");
	Enumeration enumeration = stack.elements();
	while (enumeration.hasMoreElements()) {
	    Token token = (Token) enumeration.nextElement();
	    if (token.type != 8192)
		throw new ParserException("Incorrect shorthand structure: "
					  + name + "=\"" + string + "\"");
	    Attr attr = token.value;
	    if (attr instanceof Attr.Color) {
		if (bool_0_)
		    throw new ParserException
			      ("Duplicated color specification in shorthand: "
			       + name + "=\"" + string + "\"");
		bool_0_ = true;
		colorComponent.process(attr, parsercontext);
	    } else if (attr instanceof Attr.Length) {
		if (bool_1_)
		    throw new ParserException
			      ("Duplicated width specification in shorthand: "
			       + name + "=\"" + string + "\"");
		bool_1_ = true;
		widthComponent.process(attr, parsercontext);
	    } else if (attr instanceof Attr.Word) {
		if (ColorSpec.predefinedColors.containsKey(attr.word())) {
		    if (bool_0_)
			throw new ParserException
				  ("Duplicated color specification in shorthand: "
				   + name + "=\"" + string + "\"");
		    bool_0_ = true;
		    colorComponent.process(attr, parsercontext);
		} else if (styles.containsKey(attr.word())) {
		    if (bool)
			throw new ParserException
				  ("Duplicated color specification in shorthand: "
				   + name + "=\"" + string + "\"");
		    bool = true;
		    styleComponent.process(attr, parsercontext);
		} else if (widths.containsKey(attr.word())) {
		    if (bool_1_)
			throw new ParserException
				  ("Duplicated color specification in shorthand: "
				   + name + "=\"" + string + "\"");
		    bool_1_ = true;
		    widthComponent.process(attr, parsercontext);
		} else
		    throw new ParserException("Unrecognized token "
					      + attr.word() + " in shorthand: "
					      + name + "=\"" + string + "\"");
	    } else
		throw new ParserException("Incorrect token type "
					  + attr.getTypeName()
					  + " in shorthand: " + name + "=\""
					  + string + "\"");
	}
    }
}
