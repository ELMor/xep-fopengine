/* LangSpec - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class LangSpec extends Attn
{
    public LangSpec(String string) throws ParserException {
	super(string);
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	if (string.trim().equals("inherit")) {
	    Attn.$language.process("inherit", parsercontext);
	    Attn.$country.process("inherit", parsercontext);
	} else {
	    string = string.trim();
	    int i = string.indexOf('-');
	    if (i == -1) {
		Attn.$language.process(string, parsercontext);
		parsercontext.removeAttribute(Attn.$country);
	    } else {
		Attn.$language.process(string.substring(0, i), parsercontext);
		Attn.$country.process(string.substring(i + 1), parsercontext);
	    }
	}
    }
}
