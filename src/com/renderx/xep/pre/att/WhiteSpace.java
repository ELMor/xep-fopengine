/* WhiteSpace - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.AttributeParser;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class WhiteSpace extends Attn
{
    private final Attn whitespaceCollapse = Attn.$white_space_collapse;
    private final Attn whitespaceTreatment = Attn.$white_space_treatment;
    private final Attn linefeedTreatment = Attn.$linefeed_treatment;
    private final Attn wrapOption = Attn.$wrap_option;
    
    public WhiteSpace(String string) throws ParserException {
	super(string);
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	if (string.trim().equals("inherit")) {
	    linefeedTreatment.process("inherit", parsercontext);
	    whitespaceCollapse.process("inherit", parsercontext);
	    whitespaceTreatment.process("inherit", parsercontext);
	    wrapOption.process("inherit", parsercontext);
	} else {
	    Attr attr
		= AttributeParser.parse(string).evaluate(parsercontext, this);
	    if (!(attr instanceof Attr.Word))
		throw new ParserException("Invalid property value: " + name
					  + "=\"" + string + "\"");
	    String string_0_ = attr.word();
	    if (string_0_.equals("normal")) {
		linefeedTreatment.process("treat-as-space", parsercontext);
		whitespaceCollapse.process("true", parsercontext);
		whitespaceTreatment.process("ignore-if-surrounding-linefeed",
					    parsercontext);
		wrapOption.process("wrap", parsercontext);
	    } else if (string_0_.equals("nowrap")) {
		linefeedTreatment.process("treat-as-space", parsercontext);
		whitespaceCollapse.process("true", parsercontext);
		whitespaceTreatment.process("ignore-if-surrounding-linefeed",
					    parsercontext);
		wrapOption.process("no-wrap", parsercontext);
	    } else if (string_0_.equals("pre")) {
		linefeedTreatment.process("preserve", parsercontext);
		whitespaceCollapse.process("false", parsercontext);
		whitespaceTreatment.process("preserve", parsercontext);
		wrapOption.process("no-wrap", parsercontext);
	    } else
		throw new ParserException("Invalid keyword in property value: "
					  + name + "=\"" + string + "\"");
	}
    }
}
