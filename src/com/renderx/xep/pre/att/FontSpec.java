/* FontSpec - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import java.util.Enumeration;

import com.renderx.util.Hashtable;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.AttributeParser;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;
import com.renderx.xep.pre.Token;

public class FontSpec extends Attn
{
    static final Hashtable sysFonts = new Hashtable();
    
    public FontSpec(String string) throws ParserException {
	super(string);
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	if (!string.trim().equals("inherit")) {
	    String string_0_ = (String) sysFonts.get(string.trim());
	    if (string_0_ != null)
		process(string_0_, parsercontext);
	    else {
		Attn.$font_size_adjust.process("none", parsercontext);
		Attn.$font_stretch.process("normal", parsercontext);
		Attn.$font_style.process("normal", parsercontext);
		Attn.$font_weight.process("normal", parsercontext);
		Attn.$font_variant.process("normal", parsercontext);
		Attn.$line_height.process("normal", parsercontext);
		java.util.Stack stack = AttributeParser.parseShorthand(string);
		Enumeration enumeration = stack.elements();
		Object object = null;
		Token token;
		for (;;) {
		    if (!enumeration.hasMoreElements())
			throw new ParserException
				  ("Invalid shorthand structure - cannot find font size specifier: "
				   + name + "=\"" + string + "\"");
		    token = (Token) enumeration.nextElement();
		    if (token.type != 8192)
			throw new ParserException
				  ("Invalid shorthand structure - unexpected token type "
				   + token.getTypeName() + " in shorthand '"
				   + name + "'");
		    if (token.value instanceof Attr.Word) {
			String string_1_ = token.value.word();
			if (!string_1_.equals("normal")) {
			    if (string_1_.equals("italic")
				|| string_1_.equals("oblique")
				|| string_1_.equals("backslant"))
				((Parsed) Attn.$font_style)
				    .process(token.value, parsercontext);
			    else if (string_1_.equals("bold")
				     || string_1_.equals("lighter")
				     || string_1_.equals("bolder"))
				((Parsed) Attn.$font_weight)
				    .process(token.value, parsercontext);
			    else {
				if (!string_1_.equals("small-caps"))
				    break;
				((Parsed) Attn.$font_variant)
				    .process(token.value, parsercontext);
			    }
			}
		    } else {
			if (!(token.value instanceof Attr.Count))
			    break;
			int i = token.value.count();
			if (i != 100 && i != 200 && i != 300 && i != 400
			    && i != 500 && i != 600 && i != 700 && i != 800
			    && i != 900)
			    break;
			((Parsed) Attn.$font_weight).process(token.value,
							     parsercontext);
		    }
		}
		((Parsed) Attn.$font_size).process(token.value, parsercontext);
		if (!enumeration.hasMoreElements())
		    throw new ParserException
			      ("Invalid shorthand structure - two or more tokens expected: "
			       + name + "=\"" + string + "\"");
		token = (Token) enumeration.nextElement();
		if (token.type == 128) {
		    if (!enumeration.hasMoreElements())
			throw new ParserException
				  ("Invalid shorthand structure - slash should be followed by a line height specifier: "
				   + name + "=\"" + string + "\"");
		    token = (Token) enumeration.nextElement();
		    ((Parsed) Attn.$line_height_optimum)
			.process(token.value, parsercontext);
		    ((Parsed) Attn.$line_height_minimum)
			.process(token.value, parsercontext);
		    ((Parsed) Attn.$line_height_maximum)
			.process(token.value, parsercontext);
		    if (!enumeration.hasMoreElements())
			throw new ParserException
				  ("Invalid shorthand structure - cannot find font family definition: "
				   + name + "=\"" + string + "\"");
		    token = (Token) enumeration.nextElement();
		}
		String string_2_ = token.value.toString();
		while (enumeration.hasMoreElements()) {
		    token = (Token) enumeration.nextElement();
		    if (token.type == 8)
			string_2_ += ", ";
		    else if (token.value != null)
			string_2_ += " " + token.value.toString();
		}
		Attn.$font_family.process(string_2_, parsercontext);
	    }
	}
    }
    
    static {
	sysFonts.put("caption", "bold 10pt Helvetica");
	sysFonts.put("icon", "8pt Helvetica");
	sysFonts.put("menu", "8pt Helvetica");
	sysFonts.put("message-box", "9pt Helvetica");
	sysFonts.put("small-caption", "bold 8pt Helvetica");
	sysFonts.put("status-bar", "8pt Helvetica");
    }
}
