/* URLAttr - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public abstract class URLAttr extends Attn
{
    protected URLAttr(String string) {
	super(string);
	defaultValue = Attr.Word.create("none");
    }
    
    public abstract void process
	(String string, ParserContext parsercontext) throws ParserException;
    
    protected static String parseURL(String string) {
	String string_0_ = string.trim();
	if (string_0_.startsWith("url(")
	    && string_0_.charAt(string_0_.length() - 1) == ')')
	    string_0_ = string_0_.substring(4, string_0_.length() - 1).trim();
	if (string_0_.length() >= 2
	    && ((string_0_.charAt(0) == '\''
		 && string_0_.charAt(string_0_.length() - 1) == '\'')
		|| (string_0_.charAt(0) == '\"'
		    && string_0_.charAt(string_0_.length() - 1) == '\"')))
	    string_0_ = string_0_.substring(1, string_0_.length() - 1);
	return string_0_;
    }
}
