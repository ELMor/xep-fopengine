/* UnicodeBidi - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class UnicodeBidi extends Enumerated
{
    public UnicodeBidi(String string, String string_0_)
	throws ParserException {
	super(string, string_0_,
	      new String[] { "normal", "embed", "bidi-override" });
    }
    
    public void process(Attr attr, ParserContext parsercontext)
	throws ParserException {
	attr = attr.evaluate(parsercontext, this);
	if (attr instanceof Expr)
	    throw new ParserException("Could not evaluate expression "
				      + attr.toString() + " in attribute "
				      + name);
	if (keywords.containsKey(attr)) {
	    parsercontext.addAttribute(this, attr);
	    if (parsercontext.element.id == 22)
		parsercontext.bidiOverride
		    = attr.word().equals("bidi-override");
	} else
	    normalize(attr, parsercontext);
    }
    
    protected Attr normalize(Attr attr, ParserContext parsercontext)
	throws ParserException {
	attr = super.normalize(attr, parsercontext);
	return attr;
    }
}
