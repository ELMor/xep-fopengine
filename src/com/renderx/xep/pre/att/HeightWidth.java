/* HeightWidth - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class HeightWidth extends Attn implements Alias
{
    private Attn substHorz;
    private Attn substVert;
    
    public Attn resolveAlias(ParserContext parsercontext) {
	return parsercontext.activeMode.isHorizontal ? substHorz : substVert;
    }
    
    public HeightWidth(String string, String string_0_, String string_1_)
	throws ParserException {
	super(string);
	substHorz = Attn.byName(string_0_);
	substVert = Attn.byName(string_1_);
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	resolveAlias(parsercontext).process(string, parsercontext);
    }
}
