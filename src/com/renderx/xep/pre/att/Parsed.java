/* Parsed - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.util.Hashtable;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.AttributeParser;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public abstract class Parsed extends Attn
{
    protected Hashtable keywords = null;
    protected Hashtable aliases = null;
    
    protected Parsed(String string) {
	super(string);
    }
    
    public void addAlias(String string, String string_0_)
	throws ParserException {
	addAlias(AttributeParser.parse(string),
		 AttributeParser.parse(string_0_));
    }
    
    public void addAlias(String string, Attr attr) throws ParserException {
	addAlias(AttributeParser.parse(string), attr);
    }
    
    public void addAlias(Attr attr, String string) throws ParserException {
	addAlias(attr, AttributeParser.parse(string));
    }
    
    public void addAlias(Attr attr, Attr attr_1_) {
	if (aliases == null)
	    aliases = new Hashtable();
	aliases.put(attr, attr_1_);
	if (defaultValue == attr)
	    defaultValue = attr_1_;
    }
    
    public void addKeyword(String string) {
	if (keywords == null)
	    keywords = new Hashtable();
	keywords.put(Attr.Word.create(string), new Boolean(true));
    }
    
    public void addKeywords(String[] strings) {
	for (int i = 0; i < strings.length; i++)
	    addKeyword(strings[i]);
    }
    
    public final void process(String string, ParserContext parsercontext)
	throws ParserException {
	process(AttributeParser.parse(string), parsercontext);
    }
    
    public void process(Attr attr, ParserContext parsercontext)
	throws ParserException {
	attr = attr.evaluate(parsercontext, this);
	if (aliases != null) {
	    Attr attr_2_ = (Attr) aliases.get(attr);
	    if (attr_2_ != null)
		attr = attr_2_.evaluate(parsercontext, this);
	}
	if (attr instanceof Expr)
	    throw new ParserException("Could not evaluate expression "
				      + attr.toString() + " in attribute "
				      + name);
	if (keywords != null && keywords.containsKey(attr))
	    parsercontext.addAttribute(this, attr);
	else
	    parsercontext.addAttribute(this, normalize(attr, parsercontext));
    }
    
    protected abstract Attr normalize
	(Attr attr, ParserContext parsercontext) throws ParserException;
}
