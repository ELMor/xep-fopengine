/* Background - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import java.util.Enumeration;

import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.AttributeParser;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;
import com.renderx.xep.pre.Token;

public class Background extends Attn
{
    private Attn bgcolor = Attn.$background_color;
    private Attn bgimage = Attn.$background_image;
    private Attn bgrepeat = Attn.$background_repeat;
    private Attn bgattach = Attn.$background_attachment;
    private Attn bgpos = Attn.$background_position;
    
    public Background(String string) throws ParserException {
	super(string);
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	if (string.trim().equals("inherit")) {
	    bgcolor.process("inherit", parsercontext);
	    bgimage.process("inherit", parsercontext);
	    bgrepeat.process("inherit", parsercontext);
	    bgattach.process("inherit", parsercontext);
	    bgpos.process("inherit", parsercontext);
	} else {
	    java.util.Stack stack = AttributeParser.parseShorthand(string);
	    Enumeration enumeration = stack.elements();
	    String string_0_ = null;
	    String string_1_ = null;
	    String string_2_ = null;
	    String string_3_ = null;
	    String string_4_ = null;
	    boolean bool = false;
	    while (enumeration.hasMoreElements()) {
		Token token = (Token) enumeration.nextElement();
		if (token.type == 8192) {
		    if (token.value instanceof Attr.Word) {
			String string_5_ = token.value.word();
			if (string_5_.equals("transparent")
			    || (ColorSpec.predefinedColors.get(string_5_)
				!= null)) {
			    bool = false;
			    if (string_0_ != null)
				parsercontext.session.warning
				    ("Color defined twice in '" + name
				     + "' shorthand: second definition ignored");
			    else
				string_0_ = string_5_;
			} else if (string_5_.equals("repeat")
				   || string_5_.equals("no-repeat")
				   || string_5_.equals("repeat-x")
				   || string_5_.equals("repeat-y")) {
			    bool = false;
			    if (string_2_ != null)
				parsercontext.session.warning
				    ("Tiling defined twice in '" + name
				     + "' shorthand: second definition ignored");
			    else
				string_2_ = string_5_;
			} else if (string_5_.equals("scroll")
				   || string_5_.equals("fixed")) {
			    bool = false;
			    if (string_3_ != null)
				parsercontext.session.warning
				    ("Attachment defined twice in '" + name
				     + "' shorthand: second definition ignored");
			    else
				string_3_ = string_5_;
			} else if (string_5_.equals("none")
				   || string_5_.startsWith("url(")) {
			    bool = false;
			    if (string_1_ != null)
				parsercontext.session.warning
				    ("Image URL defined twice in '" + name
				     + "' shorthand: second definition ignored");
			    else
				string_1_ = string_5_;
			} else if (string_5_.equals("top")
				   || string_5_.equals("bottom")
				   || string_5_.equals("right")
				   || string_5_.equals("left")
				   || string_5_.equals("center")) {
			    if (string_4_ == null) {
				bool = true;
				string_4_ = string_5_;
			    } else if (bool) {
				bool = false;
				string_4_ += " " + (String) string_5_;
			    } else
				parsercontext.session.warning
				    ("Misplaced token in '" + name
				     + "' shorthand: " + string_5_);
			} else
			    parsercontext.session.warning
				("Ignored unexpected token in '" + name
				 + "' shorthand: " + string_5_);
		    } else if (token.value instanceof Attr.Length
			       || token.value instanceof Attr.Percentage) {
			if (string_4_ == null) {
			    bool = true;
			    string_4_ = token.value.toString();
			} else if (bool) {
			    bool = false;
			    string_4_ += " " + token.value.toString();
			} else
			    parsercontext.session.warning
				("Ignored unexpected token in '" + name
				 + "' shorthand: " + token.value.toString());
		    } else if (token.value instanceof Attr.Color) {
			bool = false;
			if (string_0_ != null)
			    parsercontext.session.warning
				("Color defined twice in '" + name
				 + "' shorthand: second definition ignored");
			else
			    string_0_ = token.value.toString();
		    }
		} else {
		    bool = false;
		    parsercontext.session.warning("Unexpected token type in '"
						  + name + "' shorthand: "
						  + token.getTypeName()
						  + ", token ignored");
		}
	    }
	    bgcolor.process(string_0_ == null ? "transparent" : string_0_,
			    parsercontext);
	    bgimage.process(string_1_ == null ? "none" : string_1_,
			    parsercontext);
	    bgrepeat.process(string_2_ == null ? "repeat" : string_2_,
			     parsercontext);
	    bgattach.process(string_3_ == null ? "scroll" : string_3_,
			     parsercontext);
	    bgpos.process(string_4_ == null ? "0% 0%" : string_4_,
			  parsercontext);
	}
    }
}
