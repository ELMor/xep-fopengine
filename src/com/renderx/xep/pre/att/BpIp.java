/* BpIp - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class BpIp extends Attn implements Alias
{
    private Attn bp;
    private Attn ip;
    
    public BpIp(String string) throws ParserException {
	super(string);
	bp = Attn.byName(string + ".block-progression-direction");
	ip = Attn.byName(string + ".inline-progression-direction");
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	bp.process(string, parsercontext);
	ip.process(string, parsercontext);
    }
    
    public Attn resolveAlias(ParserContext parsercontext) {
	return bp;
    }
}
