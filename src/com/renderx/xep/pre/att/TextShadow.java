/* TextShadow - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import java.util.Enumeration;
import java.util.Stack;

import com.renderx.util.Array;
import com.renderx.util.Hashtable;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.AttributeParser;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;
import com.renderx.xep.pre.Token;

public class TextShadow extends Literal
{
    private Hashtable colors = new Hashtable();
    
    class ShadowDescriptor
    {
	Attr[] value;
	int lcount;
	
	ShadowDescriptor(ParserContext parsercontext) {
	    value = new Attr[4];
	    lcount = 0;
	    value[0] = parsercontext.color;
	    value[1] = value[2] = null;
	    value[3] = Attr.zerolength;
	}
	
	ShadowDescriptor(Attr[] attrs) {
	    value = new Attr[4];
	    lcount = 0;
	    value = attrs;
	}
    }
    
    public TextShadow(String string, String string_0_) throws ParserException {
	super(string, string_0_);
	colors.put("black", AttributeParser.parse("#000000"));
	colors.put("gray", AttributeParser.parse("#808080"));
	colors.put("silver", AttributeParser.parse("#C0C0C0"));
	colors.put("white", AttributeParser.parse("#FFFFFF"));
	colors.put("maroon", AttributeParser.parse("#800000"));
	colors.put("red", AttributeParser.parse("#FF0000"));
	colors.put("purple", AttributeParser.parse("#800080"));
	colors.put("fuchsia", AttributeParser.parse("#FF00FF"));
	colors.put("green", AttributeParser.parse("#008000"));
	colors.put("lime", AttributeParser.parse("#00FF00"));
	colors.put("olive", AttributeParser.parse("#808000"));
	colors.put("yellow", AttributeParser.parse("#FFFF00"));
	colors.put("navy", AttributeParser.parse("#000080"));
	colors.put("blue", AttributeParser.parse("#0000FF"));
	colors.put("teal", AttributeParser.parse("#008080"));
	colors.put("aqua", AttributeParser.parse("#00FFFF"));
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	Array array = new Array();
	ShadowDescriptor shadowdescriptor = null;
	Stack stack = AttributeParser.parseShorthand(string);
	if (stack.size() == 1) {
	    Token token = (Token) stack.pop();
	    if (token.type == 8192) {
		Attr attr = token.value.evaluate(parsercontext, this);
		if (attr == Attr.none || attr instanceof Attr.Composite) {
		    parsercontext.addAttribute(this, attr);
		    return;
		}
	    }
	    parsercontext.session.warning("Invalid value of " + name
					  + " attribute: "
					  + token.value.toString()
					  + "; shadow set to none.");
	    parsercontext.addAttribute(this, Attr.none);
	}
	Enumeration enumeration = stack.elements();
	while (enumeration.hasMoreElements()) {
	    Token token = (Token) enumeration.nextElement();
	    if (token.type == 8) {
		if (shadowdescriptor != null) {
		    if (shadowdescriptor.lcount >= 2)
			array.put(array.length(), shadowdescriptor);
		    else
			parsercontext.session.warning
			    ("Invalid structure of " + name
			     + " attribute: too few consecutive lengths");
		    shadowdescriptor = null;
		}
	    } else if (token.type != 8192)
		parsercontext.session.warning("Unexpected token in " + name
					      + ": " + token.getTypeName());
	    else {
		Attr attr = token.value.evaluate(parsercontext, this);
		if (attr instanceof Attr.Color) {
		    if (shadowdescriptor == null)
			shadowdescriptor = new ShadowDescriptor(parsercontext);
		    if (shadowdescriptor.value[0] == null)
			shadowdescriptor.value[0] = attr;
		    else
			parsercontext.session.warning
			    ("Repeated color specification in " + name
			     + " ignored");
		} else if (attr instanceof Attr.Length) {
		    if (shadowdescriptor == null)
			shadowdescriptor = new ShadowDescriptor(parsercontext);
		    if (shadowdescriptor.lcount < 3)
			shadowdescriptor.value[++shadowdescriptor.lcount]
			    = attr;
		    else
			parsercontext.session.warning
			    ("Too many length specifiers in " + name
			     + ": extra tokens ignored");
		} else if (attr instanceof Attr.Word) {
		    if (stack.size() == 1 && "none".equals(attr.word())) {
			parsercontext.inherited.remove(this);
			return;
		    }
		    if (colors.containsKey(attr.word())) {
			if (shadowdescriptor == null)
			    shadowdescriptor
				= new ShadowDescriptor(parsercontext);
			shadowdescriptor.value[0]
			    = (Attr) colors.get(attr.word());
		    } else
			parsercontext.session.warning("Invalid keyword in "
						      + name + ": "
						      + attr.word());
		} else if (attr instanceof Attr.Composite) {
		    Attr[] attrs = ((Attr.Composite) attr).vector;
		    for (int i = 0; i < attrs.length; i++) {
			if (attrs[i] != null
			    && attrs[i] instanceof Attr.Composite) {
			    Attr[] attrs_1_
				= ((Attr.Composite) attrs[i]).vector;
			    if (attrs_1_.length == 4
				&& (attrs_1_[0] == null
				    || attrs_1_[0] instanceof Attr.Color)
				&& attrs_1_[1] instanceof Attr.Length
				&& attrs_1_[2] instanceof Attr.Length
				&& (attrs_1_[3] == null
				    || attrs_1_[3] instanceof Attr.Length))
				array.put(array.length(),
					  new ShadowDescriptor(attrs_1_));
			}
		    }
		} else
		    parsercontext.session.warning("Invalid token type in "
						  + name + ": "
						  + attr.getTypeName());
	    }
	}
	if (shadowdescriptor != null) {
	    if (shadowdescriptor.lcount >= 2)
		array.put(array.length(), shadowdescriptor);
	    else
		parsercontext.session.warning
		    ("Invalid structure of " + name
		     + " attribute: too few consecutive lengths");
	    Object object = null;
	}
	Attr[] attrs = new Attr[array.length()];
	for (int i = 0; i < attrs.length; i++)
	    attrs[i]
		= new Attr.Composite(((ShadowDescriptor) array.get(i)).value);
	parsercontext.addAttribute(this, new Attr.Composite(attrs));
    }
}
