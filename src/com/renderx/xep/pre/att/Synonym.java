/* Synonym - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class Synonym extends Attn implements Alias
{
    private Attn substitution;
    
    public Attn resolveAlias(ParserContext parsercontext) {
	return substitution;
    }
    
    public Synonym(String string, String string_0_) throws ParserException {
	super(string);
	substitution = Attn.byName(string_0_);
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	substitution.process(string, parsercontext);
    }
}
