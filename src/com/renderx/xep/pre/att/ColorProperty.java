/* ColorProperty - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class ColorProperty extends ColorSpec
{
    public ColorProperty(String string, String string_0_)
	throws ParserException {
	super(string, string_0_);
    }
    
    protected Attr normalize(Attr attr, ParserContext parsercontext)
	throws ParserException {
	parsercontext.color
	    = (Attr.Color) super.normalize(attr, parsercontext);
	return parsercontext.color;
    }
}
