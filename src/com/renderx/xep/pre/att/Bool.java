/* Bool - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class Bool extends Parsed
{
    public Bool(String string, String string_0_) throws ParserException {
	super(string);
	defaultValue = Attr.Word.create(string_0_);
	this.addAlias("true", Attr.trueval);
	this.addAlias("false", Attr.falseval);
    }
    
    public Bool(String string, String string_1_, String[] strings)
	throws ParserException {
	this(string, string_1_);
	this.addKeywords(strings);
    }
    
    protected Attr normalize(Attr attr, ParserContext parsercontext)
	throws ParserException {
	if (attr instanceof Attr.Bool)
	    return attr;
	throw new ParserException("Attribute type mismatch: " + name
				  + " cannot have value of type "
				  + attr.getTypeName());
    }
}
