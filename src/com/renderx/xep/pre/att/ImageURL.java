/*
 * ImageURL - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;

import com.renderx.util.URLSpec;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class ImageURL extends URLAttr {
	private Attn contentTypeAttr;

	public ImageURL(String string, String string_0_) throws ParserException {
		super(string);
		contentTypeAttr = Attn.byName(string_0_);
	}

	public void process(String string, ParserContext parsercontext)
			throws ParserException {
		parsercontext.addAttribute(this, Attr.Word.create("none"));
		if (!"none".equals(string)) {
			String string_1_ = URLAttr.parseURL(string);
			Object object = null;
			URLSpec urlspec = null;
			String string_2_;
			try {
				urlspec = new URLSpec(parsercontext.baseURL, string_1_);
				string_2_ = parsercontext.session.config.cache.getContentType(
						urlspec, parsercontext.session);
			} catch (Exception exception) {
				parsercontext.session.warning("Could not retrieve image from '"
						+ urlspec + "': " + exception.toString());
				URLSpec urlspec_3_ = parsercontext.session.config.BROKENIMAGE;
				if (urlspec_3_ != null) {
					String string_4_ = urlspec_3_.toString();
					if (!string_4_.equals(string)) {
						process(string_4_, parsercontext);
						return;
					}
					return;
				}
				return;
			}
			parsercontext.addAttribute(this, Attr.Word
					.createUncached(string_1_));
			if (parsercontext.baseURL != null)
				parsercontext.addAttribute(Attn.$image_base_url, Attr.Word
						.createUncached(parsercontext.baseURL.toString()));
			if (string_2_ != null)
				parsercontext.addAttribute(contentTypeAttr, Attr.Word
						.create(string_2_));
		}
	}
}