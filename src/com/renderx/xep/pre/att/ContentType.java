/* ContentType - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class ContentType extends Literal
{
    public ContentType(String string, String string_0_)
	throws ParserException {
	super(string, string_0_);
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	if (!string.equals("auto")) {
	    string = string.trim();
	    if (string.startsWith("namespace-prefix:"))
		throw new ParserException("\"namespace-prefix:\" notation in "
					  + name + " is not supported");
	    if (string.startsWith("content-type:"))
		string = string.substring(13).trim();
	    parsercontext.addAttribute(this, Attr.Word.create(string));
	}
    }
}
