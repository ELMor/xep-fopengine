/* ColorSpec - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import java.util.Enumeration;

import com.renderx.util.Hashtable;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.AttributeParser;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class ColorSpec extends Parsed
{
    public static final Hashtable predefinedColors = new Hashtable();
    
    public ColorSpec(String string, String string_0_) throws ParserException {
	super(string);
	defaultValue = AttributeParser.parse(string_0_);
	this.addAlias("transparent", Attr.Color.transparent);
	Enumeration enumeration = predefinedColors.keys();
	while (enumeration.hasMoreElements()) {
	    String string_1_ = (String) enumeration.nextElement();
	    String string_2_ = (String) predefinedColors.get(string_1_);
	    this.addAlias(string_1_, string_2_);
	}
    }
    
    protected Attr normalize(Attr attr, ParserContext parsercontext)
	throws ParserException {
	if (attr instanceof Attr.Color)
	    return attr;
	if (attr instanceof Attr.Word)
	    throw new ParserException("Type mismatch for '" + name
				      + "': cannot interpret token \""
				      + attr.word() + "\" as a color name.");
	throw new ParserException("Type mismatch for '" + name + "': "
				  + attr.getTypeName()
				  + " cannot be converted to Color");
    }
    
    static {
	predefinedColors.put("aliceblue", "#F0F8FF");
	predefinedColors.put("antiquewhite", "#FAEBD7");
	predefinedColors.put("aqua", "#00FFFF");
	predefinedColors.put("aquamarine", "#7FFFD4");
	predefinedColors.put("azure", "#F0FFFF");
	predefinedColors.put("beige", "#F5F5DC");
	predefinedColors.put("bisque", "#FFE4C4");
	predefinedColors.put("black", "#000000");
	predefinedColors.put("blanchedalmond", "#FFEBCD");
	predefinedColors.put("blue", "#0000FF");
	predefinedColors.put("blueviolet", "#8A2BE2");
	predefinedColors.put("brown", "#A52A2A");
	predefinedColors.put("burlywood", "#DEB887");
	predefinedColors.put("cadetblue", "#5F9EA0");
	predefinedColors.put("chartreuse", "#7FFF00");
	predefinedColors.put("chocolate", "#D2691E");
	predefinedColors.put("coral", "#FF7F50");
	predefinedColors.put("cornflowerblue", "#6495ED");
	predefinedColors.put("cornsilk", "#FFF8DC");
	predefinedColors.put("crimson", "#DC143C");
	predefinedColors.put("cyan", "#00FFFF");
	predefinedColors.put("darkblue", "#00008B");
	predefinedColors.put("darkcyan", "#008B8B");
	predefinedColors.put("darkgoldenrod", "#B8860B");
	predefinedColors.put("darkgray", "#A9A9A9");
	predefinedColors.put("darkgreen", "#006400");
	predefinedColors.put("darkgrey", "#A9A9A9");
	predefinedColors.put("darkkhaki", "#BDB76B");
	predefinedColors.put("darkmagenta", "#8B008B");
	predefinedColors.put("darkolivegreen", "#556B2F");
	predefinedColors.put("darkorange", "#FF8C00");
	predefinedColors.put("darkorchid", "#9932CC");
	predefinedColors.put("darkred", "#8B0000");
	predefinedColors.put("darksalmon", "#E9967A");
	predefinedColors.put("darkseagreen", "#8FBC8F");
	predefinedColors.put("darkslateblue", "#483D8B");
	predefinedColors.put("darkslategray", "#2F4F4F");
	predefinedColors.put("darkslategrey", "#2F4F4F");
	predefinedColors.put("darkturquoise", "#00CED1");
	predefinedColors.put("darkviolet", "#9400D3");
	predefinedColors.put("deeppink", "#FF1493");
	predefinedColors.put("deepskyblue", "#00BFFF");
	predefinedColors.put("dimgray", "#696969");
	predefinedColors.put("dimgrey", "#696969");
	predefinedColors.put("dodgerblue", "#1E90FF");
	predefinedColors.put("firebrick", "#B22222");
	predefinedColors.put("floralwhite", "#FFFAF0");
	predefinedColors.put("forestgreen", "#228B22");
	predefinedColors.put("fuchsia", "#FF00FF");
	predefinedColors.put("gainsboro", "#DCDCDC");
	predefinedColors.put("ghostwhite", "#F8F8FF");
	predefinedColors.put("gold", "#FFD700");
	predefinedColors.put("goldenrod", "#DAA520");
	predefinedColors.put("gray", "#808080");
	predefinedColors.put("grey", "#808080");
	predefinedColors.put("green", "#008000");
	predefinedColors.put("greenyellow", "#ADFF2F");
	predefinedColors.put("honeydew", "#F0FFF0");
	predefinedColors.put("hotpink", "#FF69B4");
	predefinedColors.put("indianred", "#CD5C5C");
	predefinedColors.put("indigo", "#4B0082");
	predefinedColors.put("ivory", "#FFFFF0");
	predefinedColors.put("khaki", "#F0E68C");
	predefinedColors.put("lavender", "#E6E6FA");
	predefinedColors.put("lavenderblush", "#FFF0F5");
	predefinedColors.put("lawngreen", "#7CFC00");
	predefinedColors.put("lemonchiffon", "#FFFACD");
	predefinedColors.put("lightblue", "#ADD8E6");
	predefinedColors.put("lightcoral", "#F08080");
	predefinedColors.put("lightcyan", "#E0FFFF");
	predefinedColors.put("lightgoldenrodyellow", "#FAFAD2");
	predefinedColors.put("lightgray", "#D3D3D3");
	predefinedColors.put("lightgreen", "#90EE90");
	predefinedColors.put("lightgrey", "#D3D3D3");
	predefinedColors.put("lightpink", "#FFB6C1");
	predefinedColors.put("lightsalmon", "#FFA07A");
	predefinedColors.put("lightseagreen", "#20B2AA");
	predefinedColors.put("lightskyblue", "#87CEFA");
	predefinedColors.put("lightslategray", "#778899");
	predefinedColors.put("lightslategrey", "#778899");
	predefinedColors.put("lightsteelblue", "#B0C4DE");
	predefinedColors.put("lightyellow", "#FFFFE0");
	predefinedColors.put("lime", "#00FF00");
	predefinedColors.put("limegreen", "#32CD32");
	predefinedColors.put("linen", "#FAF0E6");
	predefinedColors.put("magenta", "#FF00FF");
	predefinedColors.put("maroon", "#800000");
	predefinedColors.put("mediumaquamarine", "#66CDAA");
	predefinedColors.put("mediumblue", "#0000CD");
	predefinedColors.put("mediumorchid", "#BA55D3");
	predefinedColors.put("mediumpurple", "#9370DB");
	predefinedColors.put("mediumseagreen", "#3CB371");
	predefinedColors.put("mediumslateblue", "#7B68EE");
	predefinedColors.put("mediumspringgreen", "#00FA9A");
	predefinedColors.put("mediumturquoise", "#48D1CC");
	predefinedColors.put("mediumvioletred", "#C71585");
	predefinedColors.put("midnightblue", "#191970");
	predefinedColors.put("mintcream", "#F5FFFA");
	predefinedColors.put("mistyrose", "#FFE4E1");
	predefinedColors.put("moccasin", "#FFE4B5");
	predefinedColors.put("navajowhite", "#FFDEAD");
	predefinedColors.put("navy", "#000080");
	predefinedColors.put("oldlace", "#FDF5E6");
	predefinedColors.put("olive", "#808000");
	predefinedColors.put("olivedrab", "#6B8E23");
	predefinedColors.put("orange", "#FFA500");
	predefinedColors.put("orangered", "#FF4500");
	predefinedColors.put("orchid", "#DA70D6");
	predefinedColors.put("palegoldenrod", "#EEE8AA");
	predefinedColors.put("palegreen", "#98FB98");
	predefinedColors.put("paleturquoise", "#AFEEEE");
	predefinedColors.put("palevioletred", "#DB7093");
	predefinedColors.put("papayawhip", "#FFEFD5");
	predefinedColors.put("peachpuff", "#FFDAB9");
	predefinedColors.put("peru", "#CD853F");
	predefinedColors.put("pink", "#FFC0CB");
	predefinedColors.put("plum", "#DDA0DD");
	predefinedColors.put("powderblue", "#B0E0E6");
	predefinedColors.put("purple", "#800080");
	predefinedColors.put("red", "#FF0000");
	predefinedColors.put("rosybrown", "#BC8F8F");
	predefinedColors.put("royalblue", "#4169E1");
	predefinedColors.put("saddlebrown", "#8B4513");
	predefinedColors.put("salmon", "#FA8072");
	predefinedColors.put("sandybrown", "#F4A460");
	predefinedColors.put("seagreen", "#2E8B57");
	predefinedColors.put("seashell", "#FFF5EE");
	predefinedColors.put("sienna", "#A0522D");
	predefinedColors.put("silver", "#C0C0C0");
	predefinedColors.put("skyblue", "#87CEEB");
	predefinedColors.put("slateblue", "#6A5ACD");
	predefinedColors.put("slategray", "#708090");
	predefinedColors.put("slategrey", "#708090");
	predefinedColors.put("snow", "#FFFAFA");
	predefinedColors.put("springgreen", "#00FF7F");
	predefinedColors.put("steelblue", "#4682B4");
	predefinedColors.put("tan", "#D2B48C");
	predefinedColors.put("teal", "#008080");
	predefinedColors.put("thistle", "#D8BFD8");
	predefinedColors.put("tomato", "#FF6347");
	predefinedColors.put("turquoise", "#40E0D0");
	predefinedColors.put("violet", "#EE82EE");
	predefinedColors.put("wheat", "#F5DEB3");
	predefinedColors.put("white", "#FFFFFF");
	predefinedColors.put("whitesmoke", "#F5F5F5");
	predefinedColors.put("yellow", "#FFFF00");
	predefinedColors.put("yellowgreen", "#9ACD32");
    }
}
