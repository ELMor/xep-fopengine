/* Position - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.AttributeParser;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class Position extends Attn
{
    public Position(String string) throws ParserException {
	super(string);
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	if (string.trim().equals("inherit")) {
	    Attn.$relative_position.process("inherit", parsercontext);
	    Attn.$absolute_position.process("inherit", parsercontext);
	} else {
	    Attr attr
		= AttributeParser.parse(string).evaluate(parsercontext, this);
	    if (!(attr instanceof Attr.Word))
		throw new ParserException("Invalid property value: " + name
					  + "=\"" + string + "\"");
	    String string_0_ = attr.word();
	    if (!string_0_.equals("static")) {
		if (string_0_.equals("relative"))
		    Attn.$relative_position.process(string_0_, parsercontext);
		else if (string_0_.equals("fixed")
			 || string_0_.equals("absolute"))
		    Attn.$absolute_position.process(string_0_, parsercontext);
		else
		    throw new ParserException
			      ("Invalid keyword in property value: " + name
			       + "=\"" + string + "\"");
	    }
	}
    }
}
