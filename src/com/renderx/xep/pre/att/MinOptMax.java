/* MinOptMax - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class MinOptMax extends Attn implements Alias
{
    private Attn min;
    private Attn opt;
    private Attn max;
    
    public Attn resolveAlias(ParserContext parsercontext) {
	return opt;
    }
    
    public MinOptMax(String string) throws ParserException {
	super(string);
	min = Attn.byName(string + ".minimum");
	opt = Attn.byName(string + ".optimum");
	max = Attn.byName(string + ".maximum");
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	max.process(string, parsercontext);
	min.process(string, parsercontext);
	opt.process(string, parsercontext);
    }
}
