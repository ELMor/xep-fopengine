/* ColumnWidth - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class ColumnWidth extends Attn implements Alias
{
    public ColumnWidth(String string) {
	super(string);
    }
    
    public Attn resolveAlias(ParserContext parsercontext) {
	return Attn.$column_width_fixed;
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	parsercontext.proportionalColumnWidth = Attr.Ratio.create(0.0);
	Attn.$column_width_fixed.process(string, parsercontext);
	Attr attr = parsercontext.proportionalColumnWidth.cast(2);
	parsercontext.addAttribute(Attn.$column_width_proportional, attr);
    }
}
