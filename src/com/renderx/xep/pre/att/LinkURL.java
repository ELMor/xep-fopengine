/* LinkURL - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class LinkURL extends URLAttr
{
    public LinkURL(String string) {
	super(string);
    }
    
    public void process(String string, ParserContext parsercontext)
	throws ParserException {
	parsercontext.addAttribute
	    (this, Attr.Word.createUncached(URLAttr.parseURL(string)));
    }
}
