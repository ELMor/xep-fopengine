/* FontStretch - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.att;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class FontStretch extends Typed implements ResolvablePercents
{
    public FontStretch(String string, String string_0_)
	throws ParserException {
	super(string, string_0_, 2);
	this.addAlias("ultra-condensed", "0.482");
	this.addAlias("extra-condensed", "0.579");
	this.addAlias("condensed", "0.694");
	this.addAlias("semi-condensed", "0.833");
	this.addAlias("normal", "1.0");
	this.addAlias("semi-expanded", "1.2");
	this.addAlias("expanded", "1.44");
	this.addAlias("extra-expanded", "1.728");
	this.addAlias("ultra-expanded", "2.074");
	this.addAlias("wider", "120%");
	this.addAlias("narrower", "83.3%");
    }
    
    public Attr.Scalable getPercentBase(ParserContext parsercontext) {
	Attr.Ratio ratio = (Attr.Ratio) parsercontext.inherited.get(this);
	return ratio == null ? (Attr.Ratio) defaultValue : ratio;
    }
}
