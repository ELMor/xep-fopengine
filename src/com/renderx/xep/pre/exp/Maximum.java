/* Maximum - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserException;

public final class Maximum extends BinaryArithmetic
{
    public Maximum() {
	/* empty */
    }
    
    public Maximum(Attr attr, Attr attr_0_) {
	super(attr, attr_0_);
    }
    
    public String toString() {
	return "max(" + arg1 + ", " + arg2 + ")";
    }
    
    protected Attr calculate(Attr attr, Attr attr_1_) throws ParserException {
	if (attr instanceof Expr || attr_1_ instanceof Expr)
	    return (attr == arg1 && attr_1_ == arg2 ? this
		    : new Maximum(attr, attr_1_));
	if (attr instanceof Attr.Numeric && attr_1_ instanceof Attr.Numeric) {
	    if ((attr instanceof Attr.Percentage
		 && !(attr_1_ instanceof Attr.Percentage))
		|| (!(attr instanceof Attr.Percentage)
		    && attr_1_ instanceof Attr.Percentage)
		|| (attr instanceof Attr.Length
		    && attr_1_ instanceof Attr.EMLength)
		|| (attr_1_ instanceof Attr.Length
		    && attr instanceof Attr.EMLength))
		return (attr == arg1 && attr_1_ == arg2 ? this
			: new Maximum(attr, attr_1_));
	    return (((Attr.Numeric) attr).greater((Attr.Numeric) attr_1_)
		    ? attr : attr_1_);
	}
	throw new ParserException("Cannot compare " + attr.getTypeName()
				  + " to " + attr_1_.getTypeName());
    }
}
