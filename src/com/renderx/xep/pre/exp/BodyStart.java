/* BodyStart - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public final class BodyStart extends ZeroArityExpr
{
    public String toString() {
	return "body-start()";
    }
    
    public Attr evaluate(ParserContext parsercontext, Attn attn)
	throws ParserException {
	return Attr.Word.create("body-start:" + parsercontext.listid);
    }
}
