/* LabelEnd - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public final class LabelEnd extends ZeroArityExpr
{
    public String toString() {
	return "label-end()";
    }
    
    public Attr evaluate(ParserContext parsercontext, Attn attn)
	throws ParserException {
	return Attr.Word.create("label-end:" + parsercontext.listid);
    }
}
