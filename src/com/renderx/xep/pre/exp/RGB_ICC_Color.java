/* RGB_ICC_Color - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public final class RGB_ICC_Color extends Expr
{
    protected Attr[] rgbData = null;
    protected Attr profile = null;
    protected Attr[] colorData = null;
    
    public String toString() {
	String string = ("rgb-icc(" + rgbData[0] + ", " + rgbData[1] + ", "
			 + rgbData[2] + ", " + profile);
	if (colorData != null) {
	    for (int i = 0; i < colorData.length; i++)
		string += ", " + colorData[i];
	}
	string += ")";
	return string;
    }
    
    public RGB_ICC_Color() {
	/* empty */
    }
    
    public RGB_ICC_Color(Attr[] attrs, Attr attr, Attr[] attrs_0_) {
	rgbData = attrs;
	profile = attr;
	colorData = attrs_0_;
    }
    
    public Attr initialize(Attr[] attrs) throws ParserException {
	if (attrs == null)
	    throw new ParserException
		      ("rgb-icc() function requires 5 or more arguments");
	if (attrs.length < 5)
	    throw new ParserException
		      ("rgb-icc() function requires 5 or more arguments");
	rgbData = new Attr[3];
	rgbData[0] = attrs[0];
	rgbData[1] = attrs[1];
	rgbData[2] = attrs[2];
	profile = attrs[3];
	colorData = new Attr[attrs.length - 4];
	for (int i = 4; i < attrs.length; i++)
	    colorData[i - 4] = attrs[i];
	return preevaluate();
    }
    
    public final Attr preevaluate() throws ParserException {
	Attr attr = selectColorSpace(profile.preevaluate());
	if (attr instanceof RGB_ICC_Color)
	    return attr;
	return attr.preevaluate();
    }
    
    public final Attr evaluate(ParserContext parsercontext, Attn attn)
	throws ParserException {
	Attr attr = selectColorSpace(profile.evaluate(parsercontext, attn));
	if (attr instanceof RGB_ICC_Color)
	    return attr;
	return attr.evaluate(parsercontext, attn);
    }
    
    protected final Attr selectColorSpace(Attr attr) throws ParserException {
	if (attr instanceof Expr)
	    return (attr == profile ? this
		    : new RGB_ICC_Color(rgbData, attr, colorData));
	if (attr instanceof Attr.Word) {
	    if ("#Grayscale".equals(attr.word())) {
		if (colorData == null || colorData.length != 1)
		    throw new ParserException
			      ("Built-in #Grayscale profile takes 1 argument");
		return new Grayscale_Color().initialize(colorData);
	    }
	    if ("#CMYK".equals(attr.word())) {
		if (colorData == null || colorData.length != 4)
		    throw new ParserException
			      ("Built-in #CMYK profile takes 4 arguments");
		return new CMYK_Color().initialize(colorData);
	    }
	    if ("#SpotColor".equals(attr.word())) {
		if (colorData == null || colorData.length < 2)
		    throw new ParserException
			      ("Built-in #SpotColor profile should take 2 or more arguments");
		return new Spot_Color().initialize(colorData);
	    }
	    if ("#Registration".equals(attr.word())) {
		if (colorData == null || colorData.length != 1)
		    throw new ParserException
			      ("Built-in #Registration profile takes 1 argument");
		return new Registration_Color().initialize(colorData);
	    }
	    return new RGB_Color().initialize(rgbData);
	}
	throw new ParserException
		  ("Color profile name in rgb-icc() function cannot be of type "
		   + attr.getTypeName());
    }
}
