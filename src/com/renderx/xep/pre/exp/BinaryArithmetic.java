/* BinaryArithmetic - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public abstract class BinaryArithmetic extends Expr
{
    protected Attr arg1 = null;
    protected Attr arg2 = null;
    
    public BinaryArithmetic() {
	this(null, null);
    }
    
    public BinaryArithmetic(Attr attr, Attr attr_0_) {
	arg1 = attr;
	arg2 = attr_0_;
    }
    
    public abstract String toString();
    
    public final Attr initialize(Attr[] attrs) throws ParserException {
	if (attrs == null)
	    throw new ParserException
		      ("A binary arithmetic expression must have two arguments");
	if (attrs.length != 2)
	    throw new ParserException
		      ("Invalid number of arguments in a binary arithmetic expression");
	arg1 = attrs[0];
	arg2 = attrs[1];
	return preevaluate();
    }
    
    public final Attr preevaluate() throws ParserException {
	return calculate(arg1.preevaluate(), arg2.preevaluate());
    }
    
    public final Attr evaluate(ParserContext parsercontext, Attn attn)
	throws ParserException {
	return calculate(arg1.evaluate(parsercontext, attn),
			 arg2.evaluate(parsercontext, attn));
    }
    
    protected abstract Attr calculate(Attr attr, Attr attr_1_)
	throws ParserException;
}
