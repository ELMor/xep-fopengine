/* PropertyValue - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;
import com.renderx.xep.pre.att.Alias;

public abstract class PropertyValue extends Expr
{
    protected Attr arg = null;
    protected Attn name = null;
    
    public PropertyValue() {
	this(null);
    }
    
    public PropertyValue(Attr attr) {
	arg = attr;
    }
    
    public final String toString() {
	return getName() + "(" + (arg == null ? "" : arg.toString()) + ")";
    }
    
    protected abstract String getName();
    
    public final Attr initialize(Attr[] attrs) throws ParserException {
	if (attrs == null)
	    arg = null;
	else if (attrs.length == 0)
	    arg = null;
	else if (attrs.length == 1)
	    arg = attrs[0];
	else
	    throw new ParserException("Property-value function " + getName()
				      + " accepts zero or one arguments");
	return preevaluate();
    }
    
    public final Attr preevaluate() throws ParserException {
	if (arg != null) {
	    Attr attr = arg.preevaluate();
	    if (attr instanceof Attr.Word)
		name = Attn.byName(attr.word());
	}
	return this;
    }
    
    public final Attr evaluate(ParserContext parsercontext, Attn attn)
	throws ParserException {
	Attn attn_0_ = attn;
	if (name != null)
	    attn_0_ = name;
	else if (arg != null) {
	    Attr attr = arg.evaluate(parsercontext, attn);
	    if (!(attr instanceof Attr.Word))
		return this;
	    attn_0_ = Attn.byName(attr.word());
	}
	if (attn_0_ instanceof Alias)
	    attn_0_ = ((Alias) attn_0_).resolveAlias(parsercontext);
	Attr attr = calculate(parsercontext, attn_0_);
	if (attr == null)
	    throw new ParserException("Cannot resolve expression "
				      + toString());
	return attr;
    }
    
    protected abstract Attr calculate
	(ParserContext parsercontext, Attn attn) throws ParserException;
}
