/* RGB_Color - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class RGB_Color extends Expr
{
    protected Attr R = null;
    protected Attr G = null;
    protected Attr B = null;
    
    public String toString() {
	return "rgb(" + R + ", " + G + ", " + B + ")";
    }
    
    public RGB_Color() {
	/* empty */
    }
    
    public RGB_Color(Attr attr, Attr attr_0_, Attr attr_1_) {
	R = attr;
	G = attr_0_;
	B = attr_1_;
    }
    
    public Attr initialize(Attr[] attrs) throws ParserException {
	if (attrs == null)
	    throw new ParserException("rgb() function requires 3 arguments");
	if (attrs.length != 3)
	    throw new ParserException("rgb() function requires 3 arguments");
	R = attrs[0];
	G = attrs[1];
	B = attrs[2];
	return preevaluate();
    }
    
    public final Attr preevaluate() throws ParserException {
	return calculate(R.preevaluate(), G.preevaluate(), B.preevaluate());
    }
    
    public final Attr evaluate(ParserContext parsercontext, Attn attn)
	throws ParserException {
	return calculate(R.evaluate(parsercontext, attn),
			 G.evaluate(parsercontext, attn),
			 B.evaluate(parsercontext, attn));
    }
    
    protected final Attr calculate(Attr attr, Attr attr_2_, Attr attr_3_)
	throws ParserException {
	if (attr instanceof Expr || attr_2_ instanceof Expr
	    || attr_3_ instanceof Expr)
	    return (attr == R && attr_2_ == G && attr_3_ == B ? this
		    : new RGB_Color(attr, attr_2_, attr_3_));
	double d = extractColorComponent(attr);
	double d_4_ = extractColorComponent(attr_2_);
	double d_5_ = extractColorComponent(attr_3_);
	return Attr.Color.RGB.create(d, d_4_, d_5_);
    }
    
    private double extractColorComponent(Attr attr) throws ParserException {
	double d = 0.0;
	if (attr instanceof Attr.Percentage)
	    d = attr.percentage() / 100.0;
	else if (attr instanceof Attr.Ratio)
	    d = attr.ratio() / 255.0;
	else if (attr instanceof Attr.Count)
	    d = (double) attr.count() / 255.0;
	else
	    throw new ParserException
		      ("Argument of rgb() function cannot be of type "
		       + attr.getTypeName());
	if (d < 0.0)
	    d = 0.0;
	if (d > 1.0)
	    d = 1.0;
	return d;
    }
}
