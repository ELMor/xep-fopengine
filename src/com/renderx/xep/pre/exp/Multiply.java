/* Multiply - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserException;

public final class Multiply extends BinaryArithmetic
{
    public Multiply() {
	/* empty */
    }
    
    public Multiply(Attr attr, Attr attr_0_) {
	super(attr, attr_0_);
    }
    
    public String toString() {
	return "(" + arg1 + " * " + arg2 + ")";
    }
    
    protected Attr calculate(Attr attr, Attr attr_1_) throws ParserException {
	if (attr instanceof Expr || attr_1_ instanceof Expr)
	    return (attr == arg1 && attr_1_ == arg2 ? this
		    : new Multiply(attr, attr_1_));
	if (attr instanceof Attr.Numeric && attr_1_ instanceof Attr.Numeric)
	    return ((Attr.Numeric) attr).multiply((Attr.Numeric) attr_1_);
	throw new ParserException("Cannot multiply type " + attr.getTypeName()
				  + " by type " + attr_1_.getTypeName());
    }
}
