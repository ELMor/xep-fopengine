/* Registration_Color - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class Registration_Color extends Expr
{
    protected Attr G = null;
    
    public String toString() {
	return "grayscale(" + G + ")";
    }
    
    public Registration_Color() {
	/* empty */
    }
    
    public Registration_Color(Attr attr) {
	G = attr;
    }
    
    public Attr initialize(Attr[] attrs) throws ParserException {
	if (attrs == null)
	    throw new ParserException
		      ("grayscale() function requires 1 argument");
	if (attrs.length != 1)
	    throw new ParserException
		      ("grayscale() function requires 1 argument");
	G = attrs[0];
	return preevaluate();
    }
    
    public final Attr preevaluate() throws ParserException {
	return calculate(G.preevaluate());
    }
    
    public final Attr evaluate(ParserContext parsercontext, Attn attn)
	throws ParserException {
	return calculate(G.evaluate(parsercontext, attn));
    }
    
    protected final Attr calculate(Attr attr) throws ParserException {
	if (attr instanceof Expr)
	    return attr == G ? this : new Registration_Color(attr);
	return Attr.Color.Registration.create(extractColorComponent(attr));
    }
    
    private double extractColorComponent(Attr attr) throws ParserException {
	double d = 0.0;
	if (attr instanceof Attr.Percentage)
	    d = attr.percentage() / 100.0;
	else if (attr instanceof Attr.Ratio)
	    d = attr.ratio();
	else if (attr instanceof Attr.Count)
	    d = (double) attr.count();
	else
	    throw new ParserException
		      ("Argument of registration() function cannot be of type "
		       + attr.getTypeName());
	if (d < 0.0)
	    d = 0.0;
	if (d > 1.0)
	    d = 1.0;
	return d;
    }
}
