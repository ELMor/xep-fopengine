/* Ceiling - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserException;

public final class Ceiling extends UnaryArithmetic
{
    public Ceiling() {
	/* empty */
    }
    
    public Ceiling(Attr attr) {
	super(attr);
    }
    
    public String toString() {
	return "ceiling(" + arg + ")";
    }
    
    protected Attr calculate(Attr attr) throws ParserException {
	if (attr instanceof Expr)
	    return attr == arg ? this : new Ceiling(attr);
	if (attr instanceof Attr.Count)
	    return attr;
	if (attr instanceof Attr.Ratio)
	    return Attr.Count.create((int) Math.ceil(attr.ratio()));
	throw new ParserException("Cannot take ceiling() of type "
				  + attr.getTypeName());
    }
}
