/* Absolute - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserException;

public final class Absolute extends UnaryArithmetic
{
    public Absolute() {
	/* empty */
    }
    
    public Absolute(Attr attr) {
	super(attr);
    }
    
    public String toString() {
	return "abs(" + arg + ")";
    }
    
    protected Attr calculate(Attr attr) throws ParserException {
	if (attr instanceof Expr)
	    return attr == arg ? this : new Absolute(attr);
	if (attr instanceof Attr.Numeric)
	    return ((Attr.Numeric) attr).abs();
	throw new ParserException("Cannot take absolute value of type "
				  + attr.getTypeName());
    }
}
