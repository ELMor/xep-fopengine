/* FromParent - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public class FromParent extends PropertyValue
{
    public FromParent() {
	/* empty */
    }
    
    public FromParent(Attr attr) {
	super(attr);
    }
    
    protected String getName() {
	return "from-parent";
    }
    
    protected Attr calculate(ParserContext parsercontext, Attn attn)
	throws ParserException {
	ParserContext parsercontext_0_ = parsercontext.prev;
	if (parsercontext_0_ == null)
	    return attn.defaultValue;
	Attr attr = (Attr) parsercontext_0_.specified.get(attn);
	if (attr == null)
	    attr = (Attr) parsercontext_0_.inherited.get(attn);
	return attr == null ? attn.defaultValue : attr;
    }
}
