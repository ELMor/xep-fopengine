/* Modulo - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserException;

public final class Modulo extends BinaryArithmetic
{
    public Modulo() {
	/* empty */
    }
    
    public Modulo(Attr attr, Attr attr_0_) {
	super(attr, attr_0_);
    }
    
    public String toString() {
	return "(" + arg1 + " mod " + arg2 + ")";
    }
    
    protected Attr calculate(Attr attr, Attr attr_1_) throws ParserException {
	if (attr instanceof Expr || attr_1_ instanceof Expr)
	    return (attr == arg1 && attr_1_ == arg2 ? this
		    : new Modulo(attr, attr_1_));
	if (attr instanceof Attr.Numeric && attr_1_ instanceof Attr.Numeric) {
	    if ((attr instanceof Attr.Percentage
		 && !(attr_1_ instanceof Attr.Percentage))
		|| (!(attr instanceof Attr.Percentage)
		    && attr_1_ instanceof Attr.Percentage)
		|| (attr instanceof Attr.Length
		    && attr_1_ instanceof Attr.EMLength)
		|| (attr_1_ instanceof Attr.Length
		    && attr instanceof Attr.EMLength))
		return (attr == arg1 && attr_1_ == arg2 ? this
			: new Modulo(attr, attr_1_));
	    return ((Attr.Numeric) attr).modulo((Attr.Numeric) attr_1_);
	}
	throw new ParserException("Cannot get a remainder of "
				  + attr.getTypeName() + " modulo "
				  + attr_1_.getTypeName());
    }
}
