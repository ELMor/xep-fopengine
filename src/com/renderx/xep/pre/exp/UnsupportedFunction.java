/* UnsupportedFunction - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.Expr;
import com.renderx.xep.pre.ParserContext;
import com.renderx.xep.pre.ParserException;

public abstract class UnsupportedFunction extends Expr
{
    public String toString() {
	return getName() + "(...) [unsupported]";
    }
    
    protected abstract String getName();
    
    public final Attr initialize(Attr[] attrs) throws ParserException {
	throw new ParserException("Function " + getName() + " unsupported");
    }
    
    public final Attr preevaluate() throws ParserException {
	throw new ParserException("Function " + getName() + " unsupported");
    }
    
    public final Attr evaluate(ParserContext parsercontext, Attn attn)
	throws ParserException {
	throw new ParserException("Function " + getName() + " unsupported");
    }
}
