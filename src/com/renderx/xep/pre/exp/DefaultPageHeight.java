/* DefaultPageHeight - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre.exp;
import com.renderx.xep.pre.Attn;
import com.renderx.xep.pre.Attr;
import com.renderx.xep.pre.ParserContext;

public final class DefaultPageHeight extends ZeroArityExpr
{
    public String toString() {
	return "default-page-height()";
    }
    
    public Attr preevaluate() {
	return this;
    }
    
    public Attr evaluate(ParserContext parsercontext, Attn attn) {
	return Attr.Length.create(parsercontext.session.config.PAGE_HEIGHT);
    }
}
