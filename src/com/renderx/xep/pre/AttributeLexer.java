/*
 * AttributeLexer - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre;

import java.io.IOException;
import java.io.InputStreamReader;

import com.renderx.xep.lib.InternalException;

public class AttributeLexer {
	public static final String blanks = " \t\r\n";

	public static final String token_delimiters = " \t\r\n'\"()+,*/";

	public static final String single_chars = "-()+,*/";

	public static final String quotes = "'\"";

	public static final String digits = "0123456789";

	public static final String hexdigits = "0123456789ABCDEFabcdef";

	private char[] data = null;

	private int length = 0;

	private int cursor = 0;

	private boolean isShorthand = false;

	private boolean expectUnaryOp = true;

	public AttributeLexer() {
		/* empty */
	}

	public AttributeLexer(boolean bool) {
		isShorthand = bool;
	}

	public AttributeLexer(String string, boolean bool) {
		this(bool);
		setSource(string);
	}

	public AttributeLexer(char[] cs, boolean bool) {
		this(bool);
		setSource(cs);
	}

	public void setSource(String string) {
		setSource(string.toCharArray());
	}

	public void setSource(char[] cs) {
		setSource(cs, 0, cs.length);
	}

	public void setSource(char[] cs, int i, int i_0_) {
		data = cs;
		cursor = i;
		length = i + i_0_;
		expectUnaryOp = true;
	}

	private void skip_whitespace() {
		for (/**/; cursor < length; cursor++) {
			if (" \t\r\n".indexOf(data[cursor]) == -1)
				break;
		}
	}

	public Token nextToken() throws ParserException {
		Token token = new Token();
		nextToken(token);
		return token;
	}

	public int nextToken(Token token) throws ParserException {
		skip_whitespace();
		token.value = null;
		if (cursor >= length)
			return token.type = 1;
		if ("-()+,*/".indexOf(data[cursor]) != -1) {
			switch (data[cursor++]) {
			case '(':
				token.type = 2;
				break;
			case ')':
				token.type = 4;
				break;
			case ',':
				token.type = 8;
				break;
			case '+':
				token.type = expectUnaryOp || isShorthand ? 1024 : 16;
				break;
			case '-':
				token.type = expectUnaryOp || isShorthand ? 2048 : 32;
				break;
			case '*':
				token.type = 64;
				break;
			case '/':
				token.type = 128;
				break;
			default:
				throw new InternalException(
						"Lexer internal error: unhandled single-char token "
								+ data[cursor - 1]);
			}
			expectUnaryOp = token.type != 4;
			return token.type;
		}
		expectUnaryOp = false;
		token.type = 8192;
		if ("'\"".indexOf(data[cursor]) != -1) {
			char c = data[cursor];
			int i = ++cursor;
			do {
				if (cursor == length)
					throw new ParserException("Unterminated quoted string");
			} while (data[cursor++] != c);
			token.value = Attr.Word.create(new String(data, i, cursor - i - 1));
			return token.type;
		}
		int i = cursor;
		for (/**/; cursor < length; cursor++) {
			if (" \t\r\n'\"()+,*/".indexOf(data[cursor]) != -1)
				break;
		}
		if (data[i] == '#') {
			if (cursor - i == 7
					&& "0123456789ABCDEFabcdef".indexOf(data[i + 1]) != -1
					&& "0123456789ABCDEFabcdef".indexOf(data[i + 2]) != -1
					&& "0123456789ABCDEFabcdef".indexOf(data[i + 3]) != -1
					&& "0123456789ABCDEFabcdef".indexOf(data[i + 4]) != -1
					&& "0123456789ABCDEFabcdef".indexOf(data[i + 5]) != -1
					&& "0123456789ABCDEFabcdef".indexOf(data[i + 6]) != -1) {
				int i_1_ = Integer.parseInt(new String(data, i + 1, 2), 16);
				int i_2_ = Integer.parseInt(new String(data, i + 3, 2), 16);
				int i_3_ = Integer.parseInt(new String(data, i + 5, 2), 16);
				if (i_1_ == i_2_ && i_2_ == i_3_)
					token.value = Attr.Color.Grayscale
							.create((double) i_2_ / 255.0);
				else
					token.value = Attr.Color.RGB.create((double) i_1_ / 255.0,
							(double) i_2_ / 255.0, (double) i_3_ / 255.0);
				return token.type;
			}
			if (cursor - i == 4
					&& "0123456789ABCDEFabcdef".indexOf(data[i + 1]) != -1
					&& "0123456789ABCDEFabcdef".indexOf(data[i + 2]) != -1
					&& "0123456789ABCDEFabcdef".indexOf(data[i + 3]) != -1) {
				int i_4_ = Integer.parseInt(new String(data, i + 1, 1), 16) * 17;
				int i_5_ = Integer.parseInt(new String(data, i + 2, 1), 16) * 17;
				int i_6_ = Integer.parseInt(new String(data, i + 3, 1), 16) * 17;
				if (i_4_ == i_5_ && i_5_ == i_6_)
					token.value = Attr.Color.Grayscale
							.create((double) i_5_ / 255.0);
				else
					token.value = Attr.Color.RGB.create((double) i_4_ / 255.0,
							(double) i_5_ / 255.0, (double) i_6_ / 255.0);
				return token.type;
			}
		}
		if ("0123456789".indexOf(data[i]) == -1
				&& (data[i] != '.' || i >= length - 1 || "0123456789"
						.indexOf(data[i + 1]) == -1)) {
			if (cursor - i == 3) {
				if (data[i] == 'd' && data[i + 1] == 'i' && data[i + 2] == 'v'
						&& !isShorthand) {
					expectUnaryOp = true;
					return token.type = 256;
				}
				if (data[i] == 'm' && data[i + 1] == 'o' && data[i + 2] == 'd'
						&& !isShorthand) {
					expectUnaryOp = true;
					return token.type = 512;
				}
				if (data[i] == 'u' && data[i + 1] == 'r' && data[i + 2] == 'l') {
					int i_7_ = cursor;
					skip_whitespace();
					if (data[cursor++] != '(')
						cursor = i_7_;
					else {
						skip_whitespace();
						i = cursor;
						int i_8_ = cursor;
						Token token_9_ = new Token();
						while_25_: for (;;) {
							switch (nextToken(token_9_)) {
							case 1:
								throw new ParserException(
										"Unclosed bracket in an URI specifier");
							case 4:
								break while_25_;
							default:
								i_8_ = cursor;
							}
						}
						if ("'\"".indexOf(data[i]) != -1 && i_8_ > i + 1
								&& data[i] == data[i_8_ - 1]) {
							i++;
							i_8_--;
						}
						token.value = Attr.Word.create("url("
								+ new String(data, i, i_8_ - i) + ")");
						return token.type;
					}
				}
			}
			token.value = Attr.Word.create(new String(data, i, cursor - i));
			skip_whitespace();
			if (cursor < length && data[cursor] == '(') {
				cursor++;
				expectUnaryOp = true;
				token.type = 4096;
			}
			return token.type;
		}
		int i_10_ = i;
		boolean bool = false;
		double d = 1.0;
		double d_11_ = 0.0;
		int i_12_ = 0;
		for (/**/; i_10_ < cursor; i_10_++) {
			if ("0123456789".indexOf(data[i_10_]) != -1) {
				int i_13_ = data[i_10_] - 48;
				if (bool) {
					d /= 10.0;
					d_11_ += d * (double) i_13_;
				} else {
					i_12_ *= 10.0;
					i_12_ += i_13_;
					d_11_ = (double) i_12_;
				}
			} else {
				if (data[i_10_] != '.' || bool)
					break;
				bool = true;
			}
		}
		switch (cursor - i_10_) {
		case 0:
			if (bool) {
				token.value = Attr.Ratio.create(d_11_);
				return token.type;
			}
			token.value = Attr.Count.create(i_12_);
			return token.type;
		case 1:
			if (data[i_10_] == '%') {
				token.value = Attr.Percentage.create(d_11_);
				return token.type;
			}
			break;
		case 2:
			if (data[i_10_] == 'e' && data[i_10_ + 1] == 'm') {
				token.value = Attr.EMLength.create(d_11_);
				return token.type;
			}
			if (data[i_10_] == 'p' && data[i_10_ + 1] == 't') {
				token.value = Attr.Length.create((int) Math
						.round(d_11_ * 1000.0));
				return token.type;
			}
			if (data[i_10_] == 'i' && data[i_10_ + 1] == 'n') {
				token.value = Attr.Length.create((int) Math
						.round(d_11_ * 72.0 * 1000.0));
				return token.type;
			}
			if (data[i_10_] == 'p' && data[i_10_ + 1] == 'c') {
				token.value = Attr.Length.create((int) Math
						.round(d_11_ * 12.0 * 1000.0));
				return token.type;
			}
			if (data[i_10_] == 'p' && data[i_10_ + 1] == 'x') {
				token.value = Attr.Length.create((int) Math
						.round(d_11_ * 0.6 * 1000.0));
				return token.type;
			}
			if (data[i_10_] == 'c' && data[i_10_ + 1] == 'm') {
				token.value = Attr.Length.create((int) Math
						.round(d_11_ * 28.346456692913385 * 1000.0));
				return token.type;
			}
			if (data[i_10_] == 'm' && data[i_10_ + 1] == 'm') {
				token.value = Attr.Length.create((int) Math
						.round(d_11_ * 2.834645669291339 * 1000.0));
				return token.type;
			}
			break;
		case 3:
			if (data[i_10_] == 'd' && data[i_10_ + 1] == 'e'
					&& data[i_10_ + 2] == 'g') {
				token.value = Attr.Angle.create(d_11_);
				return token.type;
			}
			if (data[i_10_] == 'r' && data[i_10_ + 1] == 'a'
					&& data[i_10_ + 2] == 'd') {
				token.value = Attr.Angle.create(d_11_ * 180.0 / 3.14159265);
				return token.type;
			}
		/* fall through */
		case 4:
			if (data[i_10_] == 'g' && data[i_10_ + 1] == 'r'
					&& data[i_10_ + 2] == 'a' && data[i_10_ + 3] == 'd') {
				token.value = Attr.Angle.create(d_11_ * 0.9);
				return token.type;
			}
			break;
		}
		throw new ParserException("Incorrect structure of a numeric token: "
				+ new String(data, i, cursor - i));
	}

	public static void main(String[] strings) {
		char[] cs = new char[8192];
		int i = 0;
		AttributeLexer attributelexer = new AttributeLexer();
		Token token = new Token();
		for (;;) {
			try {
				InputStreamReader inputstreamreader = new InputStreamReader(
						System.in);
				i = inputstreamreader.read(cs);
			} catch (IOException ioexception) {
				System.err.println(ioexception);
				System.exit(1);
			}
			if (i < 0)
				System.exit(0);
			attributelexer.setSource(cs, 0, i);
			try {
				do {
					attributelexer.nextToken(token);
					token.Dump(System.out);
				} while (token.type != 1);
			} catch (ParserException parserexception) {
				System.err.println(parserexception.toString());
			} catch (InternalException internalexception) {
				System.err.println(internalexception.toString());
			}
			System.out.println();
		}
	}
}