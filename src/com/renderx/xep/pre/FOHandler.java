/*
 * FOHandler - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.pre;

import java.net.MalformedURLException;
import java.util.Enumeration;

import com.renderx.util.Hashtable;
import com.renderx.util.URLSpec;
import com.renderx.xep.lib.InternalException;
import com.renderx.xep.lib.Session;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;

public class FOHandler implements ContentHandler {
	private final Session session;

	private final ElementHandler handler;

	private final URLSpec baseURL;

	private final BidiResolver bidiFilter;

	private final AttributeSieve sieve = new AttributeSieve();

	private Locator locator = null;

	private ParserContext context = null;

	private int skipcount = 0;

	private int listid = 0;

	private int tableid = 0;

	public void setDocumentLocator(Locator locator) {
		this.locator = locator;
	}

	FOHandler(String string, ElementHandler elementhandler, Session session) {
		this.session = session;
		URLSpec urlspec = null;
		if (string != null && string.trim().length() != 0) {
			String string_0_ = string.trim();
			int i = string_0_.lastIndexOf('/');
			if (i != -1)
				string_0_ = string_0_.substring(0, i + 1);
			try {
				urlspec = new URLSpec(urlspec, string_0_);
			} catch (Exception exception) {
				this.session.warning("System ID '" + string
						+ "' cannot be used as base URL: " + exception);
			}
		} else if (!this.session.config.ALLOW_NULL_SYSTEM_ID)
			this.session
					.warning("Null system ID: relative URLs may behave badly");
		baseURL = urlspec;
		if (this.session.config.ENABLE_BIDI) {
			bidiFilter = new BidiResolver(elementhandler, session);
			handler = bidiFilter;
		} else {
			bidiFilter = null;
			handler = elementhandler;
		}
	}

	public void ignorableWhitespace(char[] cs, int i, int i_1_) {
		/* empty */
	}

	public void startPrefixMapping(String string, String string_2_) {
		/* empty */
	}

	public void endPrefixMapping(String string) {
		/* empty */
	}

	public void skippedEntity(String string) {
		/* empty */
	}

	public void processingInstruction(String string, String string_3_) {
		if (skipcount <= 0) {
			if (handler instanceof PIHandler) {
				if (string_3_ != null && string_3_.trim().startsWith("url")) {
					String string_4_ = string_3_.trim().substring(3).trim();
					if (string_4_.length() > 2 && string_4_.charAt(0) == '('
							&& string_4_.charAt(string_4_.length() - 1) == ')') {
						string_4_ = string_4_.substring(1,
								string_4_.length() - 1).trim();
						if (string_4_.length() > 2
								&& ((string_4_.charAt(0) == '\'' && (string_4_
										.charAt(string_4_.length() - 1) == '\'')) || (string_4_
										.charAt(0) == '\"' && string_4_
										.charAt(string_4_.length() - 1) == '\"')))
							string_4_ = string_4_.substring(1, string_4_
									.length() - 1);
						URLSpec urlspec = context == null ? baseURL
								: context.baseURL;
						try {
							string_3_ = ("url('"
									+ new URLSpec(urlspec, string_4_)
											.toString() + "')");
						} catch (MalformedURLException malformedurlexception) {
							/* empty */
						}
					}
				}
				try {
					((PIHandler) handler).processingInstruction(string,
							string_3_);
				} catch (CompilerException compilerexception) {
					session.warning("Processing instruction ignored: "
							+ compilerexception.getMessage());
				}
			}
		}
	}

	public void startDocument() {
		skipcount = 0;
		listid = 0;
		tableid = 0;
		bidiFilter.reset();
	}

	public void endDocument() {
		/* empty */
	}

	public void startElement(String string, String string_5_, String string_6_,
			Attributes attributes) {
		if (skipcount > 0)
			skipcount++;
		else {
			if (context == null) {
				context = new ParserContext(null, session);
				context.baseURL = baseURL;
			} else if (context.next == null)
				context = new ParserContext(context, session);
			else
				context = context.next;
			Object object = null;
			Elem.Descriptor descriptor;
			try {
				descriptor = Elem.getDescriptor(string_5_);
				if (descriptor == null)
					throw new ParserException("Unknown element name: '"
							+ string_5_ + "'");
				context.initialize(descriptor,
						(descriptor.id == 50 ? attributes
								.getValue("marker-class-name") : null));
				sieve.clear();
			} catch (ParserException parserexception) {
				session.warning(parserexception.toString());
				skipcount = 1;
				return;
			}
			if (descriptor.id == 41)
				context.listid = ++listid;
			for (int i = 0; i < attributes.getLength(); i++) {
				try {
					String string_7_ = attributes.getURI(i);
					if (string_7_ == null
							|| string_7_.length() == 0
							|| string_7_
									.equals("http://www.w3.org/1999/XSL/Format")
							|| (string_7_
									.equals("http://www.renderx.com/XSL/Extensions")))
						sieve.put(attributes.getLocalName(i), attributes
								.getValue(i));
					else if (string_7_
							.equals("http://www.w3.org/XML/1998/namespace")) {
						String string_8_ = attributes.getLocalName(i);
						if ("base".equals(string_8_)) {
							String string_9_ = attributes.getValue(i);
							try {
								context.baseURL = new URLSpec(context.baseURL,
										string_9_);
							} catch (Exception exception) {
								session.exception(("Cannot convert '"
										+ string_9_ + "' to a URL: "),
										exception);
							}
						} else if ("lang".equals(string_8_))
							sieve.put("xml:lang", attributes.getValue(i));
					}
				} catch (ParserException parserexception) {
					session.warning("Attribute ignored: "
							+ parserexception.getMessage());
				}
			}
			for (int i = 0; i <= 15; i++) {
				AttributeSieve.NameAndValue[] nameandvalues = sieve.turns[i];
				int i_10_ = sieve.turnCounts[i];
				while (i_10_-- > 0) {
					try {
						nameandvalues[i_10_].att.process(
								nameandvalues[i_10_].value, context);
					} catch (ParserException parserexception) {
						session.warning("Bad attribute "
								+ nameandvalues[i_10_].att.name + ": "
								+ parserexception.getMessage());
					}
				}
				if (i == 1 && descriptor.id == 40) {
					try {
						ComputeCellColumnNumber();
					} catch (ParserException parserexception) {
						session.warning(parserexception.toString());
						skipcount = 1;
						return;
					}
				}
			}
			try {
				descriptor.checkRequired(context.specified);
			} catch (ParserException parserexception) {
				session.error(parserexception.toString());
				skipcount = 1;
				return;
			}
			Attr attr = (Attr) context.specified.get(Attn.$span);
			if (attr != null
					&& attr.word().equals("all")
					&& (descriptor.id == 20 || descriptor.id == 21
							|| descriptor.id == 41 || descriptor.id == 34 || descriptor.id == 32)) {
				ParserContext parsercontext;
				for (parsercontext = context.prev; parsercontext != null
						&& parsercontext.element.id == 49; parsercontext = parsercontext.prev) {
					/* empty */
				}
				if (parsercontext != null && parsercontext.element.id == 20)
					parsercontext = parsercontext.prev;
				if (parsercontext == null || parsercontext.element.id != 19)
					session
							.warning("'span' attribute on "
									+ descriptor.name
									+ " ignored because the element is not a direct child of a flow");
				else {
					context.extraFlowSection = true;
					AttList attlist = new AttList();
					attlist.put(Attn.$column_count, Attr.Count.create(1));
					callStartHandler((short) 101, attlist);
				}
			}
			try {
				switch (descriptor.id) {
				case 51:
					saveMarkerContext();
					break;
				case 34:
					startTable();
					setCollapsePrecedence(6);
					context.addAttribute(Attn.$table_element_class, Attr.Word
							.create("table"));
					halfCollapsedBorders();
					break;
				case 38:
					setCollapsePrecedence(2);
					context.addAttribute(Attn.$table_element_class, Attr.Word
							.create("body"));
					context.addAttribute(Attn.$body_number, Attr.Count
							.create(++context.prev.bodyNumber));
					startTableRowGroup();
					zeroCollapsedBorders();
					break;
				case 36:
					setCollapsePrecedence(1);
					context.addAttribute(Attn.$table_element_class, Attr.Word
							.create("header"));
					context.addAttribute(Attn.$body_number, Attr.header);
					startTableRowGroup();
					zeroCollapsedBorders();
					break;
				case 37:
					setCollapsePrecedence(0);
					context.addAttribute(Attn.$table_element_class, Attr.Word
							.create("footer"));
					startTableRowGroup();
					zeroCollapsedBorders();
					context.addAttribute(Attn.$body_number, Attr.footer);
					break;
				case 35:
					setCollapsePrecedence(4);
					context.addAttribute(Attn.$table_element_class, Attr.Word
							.create("column"));
					saveTableColumnContext();
					break;
				case 39:
					setCollapsePrecedence(3);
					context.addAttribute(Attn.$table_element_class, Attr.Word
							.create("row"));
					startTableRow();
					break;
				case 40:
					setCollapsePrecedence(5);
					context.addAttribute(Attn.$table_element_class, Attr.Word
							.create("cell"));
					startTableCell();
					halfCollapsedBorders();
					break;
				}
			} catch (ParserException parserexception) {
				session.info(parserexception.toString());
				skipcount = 1;
				return;
			}
			if (descriptor.id != 35)
				callStartHandler();
		}
	}

	private void callStartHandler() {
		AttList attlist = new AttList();
		FillAttributeList(attlist, context.inherited, context.element);
		FillAttributeList(attlist, context.specified, context.element);
		if (context.element.useWritingMode)
			attlist.put(Attn.$writing_mode, context.activeMode.attribute);
		if (context.element.usedProps.get(Attn.$direction) != null)
			attlist.put(Attn.$direction, context.direction);
		callStartHandler(context.element.id, attlist);
	}

	private void callStartHandler(short i, AttList attlist) {
		try {
			handler.startElement(i, attlist);
		} catch (CompilerException compilerexception) {
			session.error("Error while opening element "
					+ Elem.getDescriptor(i).name + ": "
					+ compilerexception.toString());
			skipcount = 1;
		}
	}

	public void endElement(String string, String string_11_, String string_12_) {
		if (context == null)
			throw new InternalException("Context stack underflow");
		if (skipcount > 0) {
			if (--skipcount == 0) {
				context.clear();
				context = context.prev;
			}
		} else {
			if (context.extraTableRow)
				callEndHandler((short) 39);
			if (context.element.id == 38 || context.element.id == 36
					|| context.element.id == 37) {
				int i = 0;
				for (int i_13_ = 0; i_13_ < context.busyCells.length(); i_13_++) {
					Integer integer = (Integer) context.busyCells.get(i_13_);
					if (integer != null) {
						int i_14_ = integer.intValue();
						if (i_14_ > i)
							i = i_14_;
					}
				}
				while (i-- > 0) {
					callStartHandler((short) 39, AttList.empty);
					callEndHandler((short) 39);
					context.prev.rowNumber++;
				}
			}
			if (context.element.id != 35)
				callEndHandler();
			if (context.extraFlowSection)
				callEndHandler((short) 101);
			if (context.endsRow) {
				callEndHandler((short) 39);
				context.prev.extraTableRow = false;
				context.prev.nextColumn = 1;
			}
			context.clear();
			context = context.prev;
		}
	}

	private void callEndHandler() {
		callEndHandler(context.element.id);
	}

	private void callEndHandler(short i) {
		try {
			handler.endElement(i);
		} catch (CompilerException compilerexception) {
			session.error("Error while closing element "
					+ Elem.getDescriptor(i).name + ": "
					+ compilerexception.toString());
		}
	}

	private void FillAttributeList(AttList attlist, Hashtable hashtable,
			Elem.Descriptor descriptor) {
		Enumeration enumeration = hashtable.keys();
		while (enumeration.hasMoreElements()) {
			Attn attn = (Attn) enumeration.nextElement();
			if (descriptor.usedProps.get(attn) != null) {
				if (context.collapseBorders) {
					if ((descriptor.id == 34 && ((attn == (Attn.$border_separation_block_progression_direction)) || (attn == (Attn.$border_separation_inline_progression_direction))))
							|| (!context.useCollapsePrecedence && (attn == Attn.$border_before_precedence
									|| attn == Attn.$border_after_precedence
									|| attn == Attn.$border_start_precedence || attn == Attn.$border_end_precedence)))
						continue;
				} else if ((descriptor.id == 39 || descriptor.id == 35
						|| descriptor.id == 38 || descriptor.id == 36 || descriptor.id == 37)
						&& (attn == Attn.$border_before_width_length
								|| attn == Attn.$border_after_width_length
								|| attn == Attn.$border_start_width_length
								|| attn == Attn.$border_end_width_length
								|| (attn == Attn.$border_before_width_conditionality)
								|| (attn == Attn.$border_after_width_conditionality)
								|| (attn == Attn.$border_start_width_conditionality)
								|| attn == Attn.$border_end_width_conditionality
								|| attn == Attn.$border_before_color
								|| attn == Attn.$border_after_color
								|| attn == Attn.$border_start_color
								|| attn == Attn.$border_end_color
								|| attn == Attn.$border_before_style
								|| attn == Attn.$border_after_style
								|| attn == Attn.$border_start_style || attn == Attn.$border_end_style))
					continue;
				try {
					attlist.put(attn, attn.postprocess((Attr) hashtable
							.get(attn), context));
				} catch (ParserException parserexception) {
					session.error("Cannot store attribute " + attn.name
							+ " in attribute list: "
							+ parserexception.toString());
				}
			}
		}
	}

	public void characters(char[] cs, int i, int i_15_) {
		if (skipcount <= 0 && i_15_ != 0 && context.element.enableTextChildren) {
			AttList attlist = new AttList();
			FillAttributeList(attlist, context.inherited, Elem.$pcdata);
			if (!session.config.ENABLE_BIDI)
				callCharacterHandler(new String(cs, i, i_15_), attlist);
			else if (context.bidiOverride)
				bidiFilter.characters(new String(cs, i, i_15_), attlist,
						Bidi.NEUTRAL, context);
			else {
				int i_16_ = i + i_15_;
				int i_17_ = i;
				int i_18_;
				for (i_18_ = i; i_18_ < i_16_
						&& Bidi.classes[cs[i_18_]] == Bidi.NEUTRAL; i_18_++) {
					/* empty */
				}
				if (i_18_ > i) {
					bidiFilter.characters(new String(cs, i, i_18_ - i),
							attlist, Bidi.NEUTRAL, context);
					i = i_17_ = i_18_;
				}
				for (/**/; i < i_16_; i = i_17_ = i_18_) {
					Attr attr = Bidi.classes[cs[i]];
					while (i_18_ < i_16_ && Bidi.classes[cs[i_18_]] == attr) {
						for (i_17_ = i_18_; i_17_ < i_16_
								&& Bidi.classes[cs[i_17_]] == attr; i_17_++) {
							/* empty */
						}
						for (i_18_ = i_17_; (i_18_ < i_16_ && Bidi.classes[cs[i_18_]] == Bidi.NEUTRAL); i_18_++) {
							/* empty */
						}
					}
					if (i_17_ > i)
						bidiFilter.characters(new String(cs, i, i_17_ - i),
								attlist, attr, context);
					if (i_18_ > i_17_)
						bidiFilter.characters(new String(cs, i_17_, i_18_
								- i_17_), attlist, Bidi.NEUTRAL, context);
				}
			}
		}
	}

	private void callCharacterHandler(String string, AttList attlist) {
		attlist.put(Attn.$TEXT, Attr.Word.createUncached(string));
		try {
			handler.startElement((short) 0, attlist);
			handler.endElement((short) 0);
		} catch (CompilerException compilerexception) {
			session.error("Error while processing text node: "
					+ compilerexception);
		}
	}

	private void saveMarkerContext() throws ParserException {
		Attr attr = (Attr) context.specified.get(Attn.$retrieve_class_name);
		if (attr == null)
			throw new ParserException(
					"fo:retrieve-marker element has no retrieve-class-name attribute; element ignored");
		ParserContext parsercontext = context.prev;
		while (parsercontext.element.id != 16) {
			if ((parsercontext = parsercontext.prev) == null)
				throw new ParserException(
						"fo:retrieve-marker element outside fo:page-sequence; element ignored");
		}
		parsercontext.markerInfo.put(attr.word(), context.clone());
	}

	private void saveTableColumnContext() throws ParserException {
		ParserContext parsercontext = context.prev;
		if (parsercontext == null)
			throw new ParserException("Misplaced fo:table-column ignored");
		if (parsercontext.element.id != 34)
			throw new ParserException("Misplaced fo:table-column ignored");
		int i = parsercontext.nextColumn;
		Attr attr = (Attr) context.specified.get(Attn.$column_number);
		if (attr == null) {
			for (/**/; parsercontext.tableColumns.get(i) != null; i++) {
				/* empty */
			}
		} else
			i = attr.count();
		if (i <= 0)
			throw new ParserException("Invalid column number " + i
					+ "; column descriptor ignored");
		Attr attr_19_ = (Attr) context.specified
				.get(Attn.$number_columns_spanned);
		int i_20_ = attr_19_ == null ? 1 : attr_19_.count();
		if (i_20_ <= 0) {
			session.warning("Invalid value number-columns-spanned=\"" + i_20_
					+ "\"; defaulted to 1");
			i_20_ = 1;
		}
		Attr attr_21_ = (Attr) context.specified
				.get(Attn.$number_columns_repeated);
		int i_22_ = attr_21_ == null ? 1 : attr_21_.count();
		if (i_22_ <= 0) {
			session.warning("Invalid value number-columns-repeated=\"" + i_22_
					+ "\"; defaulted to 1");
			i_22_ = 1;
		}
		context.specified.remove(Attn.$number_columns_spanned);
		context.specified.remove(Attn.$number_columns_repeated);
		while (i_22_-- > 0) {
			if (parsercontext.tableColumns.get(i) != null)
				throw new ParserException(
						"Column descriptors repeat or overlap: descriptor ignored");
			context.specified.put(Attn.$column_number, Attr.Count.create(i));
			parsercontext.tableColumns.put(i, new ParserContext.TableColumn(
					i_20_, context));
			context.specified.put(Attn.$column_number, Attr.Count.create(i));
			callStartHandler();
			callEndHandler();
			i += i_20_;
			parsercontext.nextColumn = i;
		}
	}

	private void startTable() throws ParserException {
		Attr attr = (Attr) context.inherited.get(Attn.$border_collapse);
		if (attr == null)
			attr = Attn.$border_collapse.defaultValue;
		context.setCollapsedBorderModel(attr.word());
		context.tableid = ++tableid;
		context.bodyNumber = 0;
		context.specified.put(Attn.$table_identifier, Attr.Count
				.create(context.tableid));
	}

	private void startTableRowGroup() throws ParserException {
		context.rowNumber = 0;
		context.specified.put(Attn.$table_identifier, Attr.Count
				.create(context.prev.tableid));
	}

	private void startTableRow() throws ParserException {
		ParserContext parsercontext = context.prev;
		if (parsercontext == null)
			throw new ParserException("Misplaced table row element ignored");
		if (parsercontext.extraTableRow) {
			callEndHandler((short) 39);
			parsercontext.nextColumn = 1;
			parsercontext.extraTableRow = false;
		}
		parsercontext.decreaseBusyCells();
		parsercontext.rowNumber++;
	}

	private void ComputeCellColumnNumber() throws ParserException {
		ParserContext parsercontext = context.prev;
		if (parsercontext == null)
			throw new ParserException("Misplaced table cell element ignored");
		ParserContext parsercontext_23_ = parsercontext.element.id == 39 ? parsercontext
				: null;
		ParserContext parsercontext_24_ = (parsercontext_23_ == null ? parsercontext
				: parsercontext_23_.prev);
		if (parsercontext_24_ == null)
			throw new ParserException("Misplaced table cell element ignored");
		if (parsercontext_24_.busyCells == null)
			throw new ParserException("Misplaced table cell element ignored");
		if (parsercontext_23_ == null) {
			Attr attr = (Attr) context.specified.get(Attn.$starts_row);
			Attr attr_25_ = (Attr) context.specified.get(Attn.$ends_row);
			boolean bool = attr != null && attr.bool();
			if (!parsercontext_24_.extraTableRow || bool) {
				startTableRow();
				callStartHandler((short) 39, AttList.empty);
				parsercontext_24_.extraTableRow = true;
			}
			context.endsRow = attr_25_ != null && attr_25_.bool();
		}
		context.columnNumber = parsercontext.nextColumn;
		Attr attr = (Attr) context.specified.get(Attn.$column_number);
		if (attr == null) {
			for (/**/; parsercontext_24_.busyCells.get(context.columnNumber) != null; context.columnNumber++) {
				/* empty */
			}
		} else {
			context.columnNumber = attr.count();
			if (parsercontext_24_.busyCells.get(context.columnNumber) != null)
				throw new ParserException("Overlapping cells: cell ignored");
		}
		if (context.columnNumber <= 0)
			throw new ParserException("Invalid column number "
					+ context.columnNumber + ": cell ignored");
	}

	private void startTableCell() throws ParserException {
		ParserContext parsercontext = context.prev;
		if (parsercontext == null)
			throw new ParserException("Misplaced table cell element ignored");
		ParserContext parsercontext_26_ = parsercontext.element.id == 39 ? parsercontext
				: null;
		ParserContext parsercontext_27_ = (parsercontext_26_ == null ? parsercontext
				: parsercontext_26_.prev);
		if (parsercontext_27_ == null)
			throw new ParserException("Misplaced table cell element ignored");
		if (parsercontext_27_.busyCells == null)
			throw new ParserException("Misplaced table cell element ignored");
		ParserContext parsercontext_28_ = parsercontext_27_.prev;
		if (parsercontext_28_ == null)
			throw new ParserException("Misplaced table cell element ignored");
		if (parsercontext_28_.element.id != 34)
			throw new ParserException("Misplaced table cell element ignored");
		Attr attr = (Attr) context.specified.get(Attn.$number_columns_spanned);
		int i = 1;
		if (attr != null)
			i = attr.count();
		if (i <= 0) {
			session.warning("Invalid value number-columns-spanned=\"" + i
					+ "\"; defaulted to 1");
			i = 1;
		}
		Attr attr_29_ = (Attr) context.specified.get(Attn.$number_rows_spanned);
		int i_30_ = 1;
		if (attr_29_ != null)
			i_30_ = attr_29_.count();
		for (int i_31_ = 0; i_31_ < i; i_31_++) {
			if (parsercontext_27_.busyCells.get(context.columnNumber + i_31_) != null) {
				session
						.warning("Bad table structure: overlapping cells may be truncated or skipped");
				i = i_31_;
				break;
			}
			parsercontext_27_.busyCells.put(context.columnNumber + i_31_,
					new Integer(i_30_));
		}
		context.specified.put(Attn.$column_number, Attr.Count
				.create(context.columnNumber));
		context.specified.put(Attn.$row_number, Attr.Count
				.create(parsercontext_27_.rowNumber));
		context.specified.put(Attn.$number_columns_spanned, Attr.Count
				.create(i));
		context.specified.put(Attn.$number_rows_spanned, Attr.Count
				.create(i_30_));
		context.specified.put(Attn.$table_identifier, Attr.Count
				.create(parsercontext_28_.tableid));
		switch (parsercontext_27_.element.id) {
		case 36:
			context.specified.put(Attn.$body_number, Attr.header);
			break;
		case 37:
			context.specified.put(Attn.$body_number, Attr.footer);
			break;
		case 38:
			context.specified.put(Attn.$body_number, Attr.Count
					.create(parsercontext_28_.bodyNumber));
			break;
		}
		parsercontext.nextColumn = context.columnNumber + i;
		ParserContext parsercontext_32_ = getTableLayerContext(Attn.$background_color);
		if (parsercontext_32_ != null)
			copyProperty(parsercontext_32_, Attn.$background_color);
		ParserContext parsercontext_33_ = getTableLayerContext(Attn.$background_image);
		if (parsercontext_33_ != null) {
			copyProperty(parsercontext_33_, Attn.$background_image);
			copyProperty(parsercontext_33_, Attn.$background_repeat);
			copyProperty(parsercontext_33_, Attn.$background_attachment);
			copyProperty(parsercontext_33_,
					Attn.$background_position_horizontal);
			copyProperty(parsercontext_33_, Attn.$background_position_vertical);
			copyProperty(parsercontext_33_, Attn.$background_content_width);
			copyProperty(parsercontext_33_, Attn.$background_content_height);
			copyProperty(parsercontext_33_, Attn.$background_scaling);
		}
		if (context.collapseBorders) {
			if (parsercontext_26_ != null) {
				overrideBorder("before", parsercontext_26_);
				overrideBorder("after", parsercontext_26_);
			}
			ParserContext.TableColumn tablecolumn = ((ParserContext.TableColumn) parsercontext_28_.tableColumns
					.get(context.columnNumber));
			if (tablecolumn != null)
				overrideBorder("start", tablecolumn.context);
			ParserContext.TableColumn tablecolumn_34_ = (i == 1 ? tablecolumn
					: ((ParserContext.TableColumn) parsercontext_28_.tableColumns
							.get(context.columnNumber + i - 1)));
			if (tablecolumn_34_ != null)
				overrideBorder("end", tablecolumn_34_.context);
		}
	}

	private ParserContext getTableLayerContext(Attn attn) {
		if (context.specified.get(attn) != null)
			return null;
		ParserContext parsercontext = context.prev;
		ParserContext parsercontext_35_ = parsercontext.element.id == 39 ? parsercontext
				: null;
		ParserContext parsercontext_36_ = (parsercontext_35_ == null ? parsercontext
				: parsercontext_35_.prev);
		ParserContext parsercontext_37_ = parsercontext_36_.prev;
		if (parsercontext_35_ != null
				&& parsercontext_35_.specified.get(attn) != null)
			return parsercontext_35_;
		if (parsercontext_36_.specified.get(attn) != null)
			return parsercontext_36_;
		ParserContext.TableColumn tablecolumn = ((ParserContext.TableColumn) parsercontext_37_.tableColumns
				.get(context.columnNumber));
		if (tablecolumn != null
				&& tablecolumn.context.specified.get(attn) != null)
			return tablecolumn.context;
		return null;
	}

	private void copyProperty(ParserContext parsercontext, Attn attn) {
		Attr attr = (Attr) parsercontext.specified.get(attn);
		if (attr == null)
			context.specified.remove(attn);
		else
			context.specified.put(attn, attr);
	}

	private void setCollapsePrecedence(int i) throws ParserException {
		if (context.useCollapsePrecedence) {
			Attr.Count count = Attr.Count.create(i);
			if (context.specified.get(Attn.$border_before_precedence) == null)
				context.addAttribute(Attn.$border_before_precedence, count);
			if (context.specified.get(Attn.$border_after_precedence) == null)
				context.addAttribute(Attn.$border_after_precedence, count);
			if (context.specified.get(Attn.$border_start_precedence) == null)
				context.addAttribute(Attn.$border_start_precedence, count);
			if (context.specified.get(Attn.$border_end_precedence) == null)
				context.addAttribute(Attn.$border_end_precedence, count);
		}
	}

	private void copyCollapsedBorders() {
		if (context.collapseBorders) {
			context.copySpecifiedValue(Attn.$border_before_width_length,
					Attn.$border_before_width_collapsed);
			context.copySpecifiedValue(Attn.$border_after_width_length,
					Attn.$border_after_width_collapsed);
			context.copySpecifiedValue(Attn.$border_start_width_length,
					Attn.$border_start_width_collapsed);
			context.copySpecifiedValue(Attn.$border_end_width_length,
					Attn.$border_end_width_collapsed);
		}
	}

	private void zeroCollapsedBorders() {
		if (context.collapseBorders) {
			copyCollapsedBorders();
			context.specified.put(Attn.$border_before_width_length,
					Attr.zerolength);
			context.specified.put(Attn.$border_after_width_length,
					Attr.zerolength);
			context.specified.put(Attn.$border_start_width_length,
					Attr.zerolength);
			context.specified.put(Attn.$border_end_width_length,
					Attr.zerolength);
		}
	}

	private void halfCollapsedBorders() {
		if (context.collapseBorders) {
			copyCollapsedBorders();
			divideByTwo(Attn.$border_before_width_length);
			divideByTwo(Attn.$border_after_width_length);
			divideByTwo(Attn.$border_start_width_length);
			divideByTwo(Attn.$border_end_width_length);
		}
	}

	private void divideByTwo(Attn attn) {
		Attr attr = (Attr) context.specified.get(attn);
		if (attr != null && attr instanceof Attr.Length)
			context.specified.put(attn, Attr.Length.create(attr.length() / 2));
	}

	private void overrideBorder(String string, ParserContext parsercontext)
			throws ParserException {
		Attn attn = Attn.oldName("border-" + string + "-width.length");
		Attn attn_38_ = Attn.oldName("border-" + string
				+ "-width.conditionality");
		Attn attn_39_ = Attn.oldName("border-" + string + "-color");
		Attn attn_40_ = Attn.oldName("border-" + string + "-style");
		Attn attn_41_ = Attn.oldName("border-" + string + "-precedence");
		if (compareBorderAttributes(parsercontext, attn_41_, attn_40_, attn)) {
			copyProperty(parsercontext, attn);
			copyProperty(parsercontext, attn_38_);
			copyProperty(parsercontext, attn_39_);
			copyProperty(parsercontext, attn_40_);
			copyProperty(parsercontext, attn_41_);
			copyProperty(parsercontext, Attn.$table_element_class);
		}
	}

	private boolean compareBorderAttributes(ParserContext parsercontext,
			Attn attn, Attn attn_42_, Attn attn_43_) {
		if (context.useCollapsePrecedence) {
			Attr attr = (Attr) context.specified.get(attn);
			Attr attr_44_ = (Attr) parsercontext.specified.get(attn);
			int i = BorderComparator.comparePrecedence(attr, attr_44_);
			if (i < 0)
				return true;
			if (i > 0)
				return false;
		}
		Attr attr = (Attr) context.specified.get(attn_43_);
		Attr attr_45_ = (Attr) parsercontext.specified.get(attn_43_);
		Attr attr_46_ = (Attr) context.specified.get(attn_42_);
		Attr attr_47_ = (Attr) parsercontext.specified.get(attn_42_);
		return (BorderComparator.compareWidthAndStyle(attr, attr_46_, attr_45_,
				attr_47_) < 0);
	}
}