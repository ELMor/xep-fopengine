/*
 * ValidationException - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep;

import org.xml.sax.SAXParseException;

class ValidationException extends SAXParseException {
	ValidationException(String string) {
		super(string, (org.xml.sax.Locator) null);
	}
}