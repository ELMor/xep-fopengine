/*
 * DebugLogger - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.lib;

import com.renderx.util.DebugEventHandler;
import com.renderx.util.TLS;
import com.renderx.util.TLSInstance;

public class DebugLogger extends TLSInstance implements DebugEventHandler {
	private static final TLS tls = new TLS(DebugLogger.class);

	public static synchronized DebugLogger logger() {
		try {
			return (DebugLogger) tls.instance();
		} catch (InstantiationException instantiationexception) {
			throw new RuntimeException("should not happen: "
					+ instantiationexception.toString());
		} catch (IllegalAccessException illegalaccessexception) {
			throw new RuntimeException("should not happen: "
					+ illegalaccessexception.toString());
		}
	}

	public static synchronized void release() {
		tls.release();
	}

	public synchronized void message(String string) {
		/* empty */
	}

	public synchronized void debug(String string, String string_0_) {
		/* empty */
	}

	public boolean isDebugged(String string) {
		return false;
	}
}