/*
 * LicenseException - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.lib;

public class LicenseException extends ConfigurationException {
	public LicenseException() {
		/* empty */
	}

	public LicenseException(String string) {
		super(string);
	}

	public LicenseException(Exception exception) {
		super(exception);
	}

	public LicenseException(String string, Exception exception) {
		super(string, exception);
	}
}