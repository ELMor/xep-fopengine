/*
 * DefaultLogger - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.lib;

import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;

public class DefaultLogger implements Logger {
	private final PrintWriter out;

	private String indent = "";

	private boolean newline = true;

	public DefaultLogger() {
		this(new OutputStreamWriter(System.err));
	}

	public DefaultLogger(Writer writer) {
		out = new PrintWriter(writer, true);
	}

	public synchronized void openDocument() {
		if (!newline)
			out.println();
		out.print("(document ");
		indent = "  ";
		newline = false;
		out.flush();
	}

	public synchronized void closeDocument() {
		out.println(")");
		indent = "";
		newline = false;
		out.flush();
	}

	public synchronized void event(String string, String string_0_) {
		if (newline)
			out.print(indent);
		if ("page-number".equals(string))
			out.print("[" + string_0_ + "]");
		else if ("region-name".equals(string))
			out.print("|" + string_0_ + "|");
		else
			out.print("[" + string + " " + string_0_ + "]");
		newline = false;
		out.flush();
	}

	public synchronized void openState(String string) {
		if (!newline)
			out.println();
		out.print(indent + "(" + string + " ");
		StringBuffer stringbuffer = new StringBuffer();
		DefaultLogger defaultlogger_1_ = this;
		defaultlogger_1_.indent = stringbuffer.append(defaultlogger_1_.indent)
				.append("  ").toString();
		newline = false;
		out.flush();
	}

	public synchronized void closeState(String string) {
		if (indent.length() >= 2)
			indent = indent.substring(2);
		if (newline)
			out.print(indent);
		out.print(")");
		newline = false;
		out.flush();
	}

	public synchronized void info(String string) {
		if (!newline)
			out.println();
		out.println(indent + string);
		newline = true;
		out.flush();
	}

	public synchronized void warning(String string) {
		if (!newline)
			out.println();
		out.println(indent + "[warning] " + string);
		newline = true;
		out.flush();
	}

	public synchronized void error(String string) {
		if (!newline)
			out.println();
		out.println(indent + "[error] " + string);
		newline = true;
		out.flush();
	}

	public synchronized void exception(String string, Exception exception) {
		error(string);
		error(exception.toString());
	}
}