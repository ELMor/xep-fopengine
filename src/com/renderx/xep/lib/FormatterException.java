/*
 * FormatterException - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.lib;

import org.xml.sax.SAXException;

public class FormatterException extends SAXException {
	public FormatterException() {
		super("");
	}

	public FormatterException(String string) {
		super(string);
	}

	public FormatterException(Exception exception) {
		super(exception);
	}

	public FormatterException(String string, Exception exception) {
		super(string, exception);
	}
}