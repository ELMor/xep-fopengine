/*
 * ConfigurationException - Decompiled by JODE Visit
 * http://jode.sourceforge.net/
 */
package com.renderx.xep.lib;

public class ConfigurationException extends FormatterException {
	public ConfigurationException() {
		/* empty */
	}

	public ConfigurationException(String string) {
		super(string);
	}

	public ConfigurationException(Exception exception) {
		super(exception);
	}

	public ConfigurationException(String string, Exception exception) {
		super(string, exception);
	}
}