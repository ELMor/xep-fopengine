/*
 * EventLogger - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep.lib;

import com.renderx.util.Stack;
import com.renderx.util.TLS;
import com.renderx.util.TLSInstance;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import org.xml.sax.helpers.DefaultHandler;

public class EventLogger extends TLSInstance implements Logger {
	private static final TLS tls = new TLS(EventLogger.class);

	public static final String uri = "http://www.renderx.com/XEP/log";

	private static final String pfx = "log";

	private Stack sst = new Stack();

	private static ContentHandler fallbackHandler = new FallbackHandler();

	private ContentHandler handler = fallbackHandler;

	private AttributesImpl attrs = new AttributesImpl();

	private String[] prevmsg = new String[8];

	private static final class FallbackHandler extends DefaultHandler {
		private FallbackHandler() {
			/* empty */
		}

		public synchronized void startDocument() throws SAXException {
			System.err.println("(document");
		}

		public synchronized void endDocument() throws SAXException {
			System.err.println(")");
		}

		public synchronized void startElement(String string, String string_0_,
				String string_1_, Attributes attributes) throws SAXException {
			if ("http://www.renderx.com/XEP/log".equals(string)) {
				if (string_0_.equals("state"))
					System.err.print("(" + attributes.getValue("class") + " ");
				else if (string_0_.equals("event"))
					System.err.print("[" + attributes.getValue("class") + " ");
				else if (string_0_.equals("error")
						|| string_0_.equals("warning"))
					System.err.print("\n{"
							+ (string_0_.equals("error") ? "!" : "?"));
				else
					throw new SAXException("invalid element " + string_0_
							+ " passed to log handler");
			}
		}

		public synchronized void endElement(String string, String string_2_,
				String string_3_) throws SAXException {
			if ("http://www.renderx.com/XEP/log".equals(string)) {
				if (string_2_.equals("state"))
					System.err.print(")");
				else if (string_2_.equals("event"))
					System.err.print("]");
				else if (string_2_.equals("error")
						|| string_2_.equals("warning"))
					System.err.print("}\n");
				else
					throw new SAXException("invalid element " + string_2_
							+ " passed to log handler");
			}
		}

		public synchronized void characters(char[] cs, int i, int i_4_)
				throws SAXException {
			System.err.print(new String(cs, i, i_4_));
		}

		public synchronized void ignorableWhitespace(char[] cs, int i, int i_5_)
				throws SAXException {
			/* empty */
		}

		public synchronized void processingInstruction(String string,
				String string_6_) throws SAXException {
			/* empty */
		}
	}

	private void addAttribute(String string, String string_7_, String string_8_) {
		attrs.addAttribute("", string, string, string_7_, string_8_);
	}

	private void startElement(String string, Attributes attributes)
			throws SAXException {
		handler.startElement("http://www.renderx.com/XEP/log", string, "log:"
				+ string, attributes);
	}

	private void endElement(String string) throws SAXException {
		handler.endElement("http://www.renderx.com/XEP/log", string, "log:"
				+ string);
	}

	public static synchronized EventLogger logger() {
		try {
			return (EventLogger) tls.instance();
		} catch (InstantiationException instantiationexception) {
			throw new RuntimeException("should not happen: "
					+ instantiationexception.toString());
		} catch (IllegalAccessException illegalaccessexception) {
			throw new RuntimeException("should not happen: "
					+ illegalaccessexception.toString());
		}
	}

	public static synchronized void release() {
		tls.release();
	}

	public void setContentHandler(ContentHandler contenthandler) {
		handler = contenthandler != null ? contenthandler : fallbackHandler;
	}

	public synchronized void openDocument() {
		try {
			handler.startDocument();
		} catch (SAXException saxexception) {
			throw new InternalException("problem in the logger");
		}
	}

	public synchronized void closeDocument() {
		try {
			handler.endDocument();
		} catch (SAXException saxexception) {
			throw new InternalException("problem in the logger");
		}
	}

	public synchronized void event(String string, String string_9_) {
		try {
			attrs.clear();
			addAttribute("class", "CDATA", string);
			startElement("event", attrs);
			info(string_9_);
			endElement("event");
		} catch (SAXException saxexception) {
			throw new InternalException("problem in the logger");
		}
	}

	public synchronized void openState(String string) {
		try {
			attrs.clear();
			addAttribute("class", "CDATA", string);
			startElement("state", attrs);
			sst.push(string);
		} catch (SAXException saxexception) {
			throw new InternalException("problem in the logger");
		}
	}

	public synchronized void closeState(String string) {
		try {
			String string_10_ = (String) sst.pop();
			if (!string.equals(string_10_))
				throw new InternalException("openState(\"" + string_10_
						+ "\")/closeState(\"" + string + "\") do not match");
			endElement("state");
		} catch (SAXException saxexception) {
			throw new InternalException("problem in the logger");
		}
	}

	public synchronized void info(String string) {
		try {
			char[] cs = new char[string.length()];
			string.getChars(0, cs.length, cs, 0);
			handler.characters(cs, 0, cs.length);
		} catch (SAXException saxexception) {
			throw new InternalException("problem in the logger");
		}
	}

	private boolean dejavu(String string) {
		for (int i = 0; i != prevmsg.length; i++) {
			if (string.equals(prevmsg[i]))
				return true;
		}
		System.arraycopy(prevmsg, 0, prevmsg, 1, prevmsg.length - 1);
		prevmsg[0] = string;
		return false;
	}

	public synchronized void warning(String string) {
		if (!dejavu(string)) {
			try {
				attrs.clear();
				startElement("warning", attrs);
				info(string);
				endElement("warning");
			} catch (SAXException saxexception) {
				throw new InternalException("problem in the logger");
			}
		}
	}

	public synchronized void error(String string) {
		if (!dejavu(string)) {
			try {
				attrs.clear();
				startElement("error", attrs);
				info(string);
				endElement("error");
			} catch (SAXException saxexception) {
				throw new InternalException("problem in the logger");
			}
		}
	}

	public synchronized void exception(String string, Exception exception) {
		error(string + ": " + exception.getMessage());
	}
}