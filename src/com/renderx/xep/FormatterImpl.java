/*
 * FormatterImpl - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.renderx.xep;

import java.io.IOException;
import java.util.Properties;

import javax.xml.transform.Source;

import com.renderx.sax.JAXPSourceConverter;
import com.renderx.xep.lib.Conf;
import com.renderx.xep.lib.ConfigurationException;
import com.renderx.xep.lib.DefaultLogger;
import com.renderx.xep.lib.Logger;

import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

public class FormatterImpl implements Formatter {
	private final FormatterCore core;

	public FormatterImpl() throws ConfigurationException {
		this(new Properties());
	}

	public FormatterImpl(Logger logger) throws ConfigurationException {
		this(new Properties(), logger);
	}

	public FormatterImpl(Properties properties) throws ConfigurationException {
		this(properties, new DefaultLogger());
	}

	public FormatterImpl(Properties properties, Logger logger)
			throws ConfigurationException {
		this(new Conf(properties, logger));
	}

	public FormatterImpl(Source source) throws ConfigurationException {
		this(source, new Properties());
	}

	public FormatterImpl(Source source, Logger logger)
			throws ConfigurationException {
		this(source, new Properties(), logger);
	}

	public FormatterImpl(Source source, Properties properties)
			throws ConfigurationException {
		this(source, properties, new DefaultLogger());
	}

	public FormatterImpl(Source source, Properties properties, Logger logger)
			throws ConfigurationException {
		this(createConfig(source, properties, logger));
	}

	private static final Conf createConfig(Source source,
			Properties properties, Logger logger) throws ConfigurationException {
		try {
			JAXPSourceConverter jaxpsourceconverter = new JAXPSourceConverter(
					source);
			try {
				return new Conf(jaxpsourceconverter.source,
						jaxpsourceconverter.parser, properties, logger);
			} finally {
				if (jaxpsourceconverter.stream != null)
					jaxpsourceconverter.stream.close();
			}
		} catch (IOException ioexception) {
			throw new ConfigurationException(ioexception);
		} catch (SAXException saxexception) {
			throw new ConfigurationException(saxexception);
		}
	}

	protected FormatterImpl(Conf conf) throws ConfigurationException {
		core = new FormatterCore(conf);
	}

	public void cleanup() {
		core.cleanup();
	}

	public void render(Source source, FOTarget fotarget) throws SAXException,
			IOException {
		render(source, fotarget, core.config.logger);
	}

	public void render(Source source, FOTarget fotarget, Logger logger)
			throws SAXException, IOException {
		render(source, createGenerator(fotarget, logger), logger);
	}

	public ContentHandler createContentHandler(String string, FOTarget fotarget)
			throws ConfigurationException {
		return createContentHandler(string, fotarget, core.config.logger);
	}

	public ContentHandler createContentHandler(String string,
			FOTarget fotarget, Logger logger) throws ConfigurationException {
		return createContentHandler(string, createGenerator(fotarget, logger),
				logger);
	}

	public void render(Source source, ContentHandler contenthandler)
			throws SAXException, IOException {
		render(source, contenthandler, core.config.logger);
	}

	public void render(Source source, ContentHandler contenthandler,
			Logger logger) throws SAXException, IOException {
		JAXPSourceConverter jaxpsourceconverter = new JAXPSourceConverter(
				source);
		try {
			core.render(jaxpsourceconverter.source, jaxpsourceconverter.parser,
					contenthandler, logger);
		} finally {
			if (jaxpsourceconverter.stream != null)
				jaxpsourceconverter.stream.close();
		}
	}

	public ContentHandler createContentHandler(String string,
			ContentHandler contenthandler) throws ConfigurationException {
		return createContentHandler(string, contenthandler, core.config.logger);
	}

	public ContentHandler createContentHandler(String string,
			ContentHandler contenthandler, Logger logger)
			throws ConfigurationException {
		return core.createContentHandler(string, contenthandler, logger);
	}

	public ContentHandler createGenerator(FOTarget fotarget)
			throws ConfigurationException {
		return createGenerator(fotarget, core.config.logger);
	}

	public ContentHandler createGenerator(FOTarget fotarget, Logger logger)
			throws ConfigurationException {
		return core.createGenerator(fotarget.stream, fotarget.format,
				fotarget.options, logger);
	}
}