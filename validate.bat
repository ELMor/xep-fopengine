@echo off
rem   This batch file encapsulates a standard XEP call. 

set CP=C:\tmp\xep\lib\xep.jar;C:\tmp\xep\lib\saxon.jar;C:\tmp\xep\lib\xt.jar

if x%OS%==xWindows_NT goto WINNT
"c:\java\j2sdk1.4.2_05\jre\bin\java" -classpath "%CP%" "-Dcom.renderx.xep.CONFIG=C:\tmp\xep\xep.xml" com.renderx.xep.Validator %1 %2 %3 %4 %5 %6 %7 %8 %9
goto END

:WINNT
"c:\java\j2sdk1.4.2_05\jre\bin\java" -classpath "%CP%" "-Dcom.renderx.xep.CONFIG=C:\tmp\xep\xep.xml" com.renderx.xep.Validator %*

:END

set CP=
